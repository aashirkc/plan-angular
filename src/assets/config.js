window.wpms_config = {
    // dist: 'sainamaina',
    // dist: '',
    dist: 'musikot',
    // dist: 'kshireshwornath',
    // dist: 'aathbiskot',
    // dist: 'bodebarsain',
    // dist: 'bideha',
    monitoringCommitteeDescription: [
        { name: "memberName", heading: "नाम", placeholder: "ENTER_NAME" },
        { name: "memberPost", disabled: true, heading: "पद", placeholder: "पद" },
        { name: "committeAddress", heading: "ठेगाना", placeholder: "ठेगाना" },
        { name: "citizenshipNumber", heading: "ना.प्र.प.नं.", placeholder: "ना.प्र.प.नं." },
        { name: "issueDistrict", heading: "जारी गर्ने जिल्ला", placeholder: "जारी गर्ने जिल्ला" },
        { name: "issueDate", heading: "जारी मिति", placeholder: "जारी मिति" },
        { name: "phoneNumber", heading: "सम्पर्क नं", placeholder: "सम्पर्क नं" },
        { name: "committeId", hidden: true },
        { name: "administrativeId", hidden: true },
        { name: "scanCopy", hidden: true },
    ]

    // dist: 'kotahimai',
    // monitoringCommitteeDescription: [
    //     { name: "memberPost", disabled: true, heading: "पद", placeholder: "पद" },
    //     { name: "memberName", heading: "नाम", placeholder: "ENTER_NAME" },
    //     { name: "memberFather", heading: "पति/पिताको नाम", placeholder: "पति/पिताको नाम" },
    //     { name: "memberGrandFather", heading: "बाजे/ससुराको नाम", placeholder: "बाजे/ससुराको नाम" },
    //     { name: "citizenshipNumber", heading: "ना.प्र.प.नं.", placeholder: "ना.प्र.प.नं." },
    //     { name: "committeAddress", heading: "ठेगाना", placeholder: "ठेगाना" },
    //     { name: "issueDistrict", heading: "जारी गर्ने जिल्ला", placeholder: "जारी गर्ने जिल्ला" },
    //     { name: "issueDate", heading: "जारी मिति", placeholder: "जारी मिति" },
    //     { name: "phoneNumber", heading: "सम्पर्क नं", placeholder: "सम्पर्क नं" },
    //     { name: "committeId", hidden: true },
    //     { name: "administrativeId", hidden: true },
    //     { name: "scanCopy", hidden: true },
    // ]
};
