import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe, DecimalPipe } from '@angular/common';
import {
    HttpClientModule,
    HttpClient,
    HttpClientXsrfModule,
    HTTP_INTERCEPTORS
} from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { FileUploadModule } from 'ng2-file-upload';
// import { print } from 'print-js/src/index'

import {
    AllReportService,
    AuthenticateService,
    TfmHttpInterceptorService,
    UserService,
    PermissionService,
    CurrentUserService,
    MyFileUploadService,
    PlanningYearService,
    ActivityMasterService,
    AgencyTypeService,
    InclusionGroupService,
    PlantUnitService,
    ProgressReportingTippaniAadeshService,
    ActivityFocusAreaMasterService,
    ActivityDetailService,
    ActivityOutputService,
    ServiceDetailsService,
    CapacityBuildingService,
    WorkOutputAssetService,
    AdministrativeApprovalService,
    ProgressReportingService,
    ServiceTypeMasterService,
    SetupsService,
    AssetsDetailTypeService,
    TrainingCategoryMasterService,
    DashboardServiceService,
    UnicodeTranslateService,
    OrganizedByMasterService,
    AssetsDetailsCategoryService,
    AssetsDetailsSubCategoryService,
    KataSanchalanService,
    BudgetTransferService,
    EstimateAbstractOfCostService,
    MeasurementBookAndBillOfQuantityService,
    ProgramTransferService,
    EstimateDateConverterService,
    ToggleSidebarService,
    NepaliNumberToWordService,
    AnugamanSamitiMasterServiceService,
    PrintingService
} from './shared';
import {
    AuthGuard,
    CanNavigateRouteGuard,
    NavigateGuard,
    DateConverterService
} from './shared';
import { SharedPipesModule } from './shared';
import {
    NgxPermissionsModule,
    NgxPermissionsService,
    NgxPermissionsStore,
    NgxRolesStore,
    NgxPermissionsGuard
} from 'ngx-permissions';
import { DataService } from './shared/data-service';
import { ProgramsService } from './shared/services/programs.service';
import { KeyboardLayoutService } from './shared/services/keyboard-layout.service';
import { DivisionServiceService } from './shared/services/division-service.service';
import { FormHelperModule } from './shared/form-helper/form-helper.module';
// import { TestComponentComponent } from './test-component/test-component.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    // for development
    // return new TranslateHttpLoader(http, '/start-angular/SB-Admin-BS4-Angular-4/master/dist/assets/i18n/', '.json');
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        HttpClientXsrfModule,
        AppRoutingModule,
        NgxBarcodeModule,
        SharedPipesModule,
        MatSidenavModule,
        MatListModule,
        FileUploadModule,
        NgxPermissionsModule.forChild(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [
        AuthGuard,
        NavigateGuard,
        CanNavigateRouteGuard,
        NgxPermissionsService,
        NgxPermissionsStore,
        NgxRolesStore,
        NgxPermissionsGuard,
        DatePipe,
        DecimalPipe,
        AuthenticateService,
        AllReportService,
        TfmHttpInterceptorService,
        UserService,
        PermissionService,
        CurrentUserService,
        TranslateService,
        DateConverterService,
        MyFileUploadService,
        PlanningYearService,
        ActivityMasterService,
        AgencyTypeService,
        InclusionGroupService,
        PlantUnitService,
        ActivityFocusAreaMasterService,
        ActivityDetailService,
        ActivityOutputService,
        ServiceDetailsService,
        CapacityBuildingService,
        WorkOutputAssetService,
        DataService,
        AdministrativeApprovalService,
        ProgressReportingService,
        ServiceTypeMasterService,
        AssetsDetailsSubCategoryService,
        AssetsDetailsCategoryService,
        OrganizedByMasterService,
        SetupsService,
        AssetsDetailTypeService,
        TrainingCategoryMasterService,
        DashboardServiceService,
        ProgressReportingTippaniAadeshService,
        UnicodeTranslateService,
        ProgramsService,
        ProgressReportingTippaniAadeshService,
        KataSanchalanService,
        BudgetTransferService,
        EstimateAbstractOfCostService,
        MeasurementBookAndBillOfQuantityService,
        ProgramTransferService,
        EstimateDateConverterService,
        ToggleSidebarService,
        KeyboardLayoutService,
        AnugamanSamitiMasterServiceService,
        PrintingService,

        DivisionServiceService,
        NepaliNumberToWordService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TfmHttpInterceptorService,
            multi: true
        },
        {
            provide: 'ENABLE_ADMINISTRATIVE_APPROVAL_DISABLE_FEATURE',
            useValue: false
        },
        {
            provide: 'DIST_TYPE',
            useValue: window['wpms_config']['dist']
        },

        // { provide: 'API_URL', useValue: 'http://crazziee:8084/Planning/api/' },
        // { provide: 'API_URL_DOC', useValue: 'http://crazziee:8084/PlanningDocument/' },
        // { provide: 'LoginUrl', useValue: 'http://crazziee:8084/Planning/' },

        //  { provide: 'API_URL', useValue: 'http://Aruns-MacBook-Pro/Planning/api/' },
        // { provide: 'API_URL_DOC', useValue: 'http://Aruns-MacBook-Pro/PlanningDocument/' },
        // { provide: 'LoginUrl', useValue: 'http://Aruns-MacBook-Pro/Planning/' },

        // { provide: 'API_URL', useValue: 'http://localhost:8080/Planning/api/' },
        // { provide: 'API_URL_DOC', useValue: 'http://localhost:8080/PlanningDocument/' },
        // { provide: 'LoginUrl', useValue: 'http://localhost:8080/Planning/' },

        // {
        //     provide: 'API_URL',
        //     useValue: 'http://192.168.100.199:8080/Planning/api/'
        // },
        // {
        //     provide: 'API_URL_DOC',
        //     useValue: 'http://192.168.100.199:8080/PlanningDocument/'
        // },
        // {
        //     provide: 'LoginUrl',
        //     useValue: 'http://192.168.100.199:8080/Planning/'
        // }

        // {
        //     provide: 'API_URL',
        //     useValue: 'http://stg.phoenixsolutions.com.np:8080/BidehaPlanning/api/'
        // },
        // {
        //     provide: 'API_URL_DOC',
        //     useValue:
        //         'http://stg.phoenixsolutions.com.np:8080/BidehaPlanningDocument/'
        // },
        // {
        //     provide: 'LoginUrl',
        //     useValue: 'http://stg.phoenixsolutions.com.np:8080/BidehaPlanning/'
        // }

        // { provide: 'API_URL', useValue: 'http://103.235.199.37:8088/SainamainaPlanning/api/' },
        // { provide: 'API_URL_DOC', useValue: 'http://103.235.199.37:8088/SainamainaPlanningDocument/' },
        // { provide: 'LoginUrl', useValue: 'http://103.235.199.37:8088/SainamainaPlanning/' },

        // { provide: 'API_URL', useValue: 'http://stg.phoenixsolutions.com.np:8080/KshireshwornathPlanning/api/' },
        // { provide: 'API_URL_DOC', useValue: 'http://stg.phoenixsolutions.com.np:8080/KshireshwornathKshireshwornathDocument/' },
        // { provide: 'LoginUrl', useValue: 'http://stg.phoenixsolutions.com.np:8080/KshireshwornathPlanning/' },

        // { provide: 'API_URL', useValue: 'http://stg.phoenixsolutions.com.np:8080/SainamainaPlanning/api/' },
        // { provide: 'API_URL_DOC', useValue: 'http://stg.phoenixsolutions.com.np:8080/SainamainaPlanningDocument/' },
        // { provide: 'LoginUrl', useValue: 'http://stg.phoenixsolutions.com.np:8080/SainamainaPlanning/' },

        {
            provide: 'API_URL',
            useValue:
                window.location.pathname.substring(
                    0,
                    window.location.pathname.indexOf('/', 2)
                ) + '/api/'
        },
        {
            provide: 'API_URL_DOC',
            useValue:
                window.location.pathname.substring(
                    0,
                    window.location.pathname.indexOf('/', 2)
                ) + 'Document/'
        },
        {
            provide: 'LoginUrl',
            useValue:
                window.location.pathname.substring(
                    0,
                    window.location.pathname.indexOf('/', 2)
                ) + '/'
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
