import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ListFilterPipe } from './../pipes/list-filter.pipe';
import { DateSplitPipe } from './../pipes/date-split.pipe';
import { NepaliInputComponent } from './../components/nepali-input/nepali-input.component';
import { NepaliTextareaComponent } from './../components/nepali-textarea/nepali-textarea.component';
import { LetterheadComponent } from './../components/letterhead/letterhead.component';

import { TranslateModule } from '@ngx-translate/core';

import { jqxPanelComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxpanel';
import { jqxInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxinput';
import { jqxTextAreaComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxtextarea';
import { jqxButtonComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxbuttons';
import { jqxNumberInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnumberinput';
import { jqxTreeComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxtree';
import { jqxDataTableComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxdatatable';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { jqxDropDownListComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxValidatorComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxvalidator';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxDateTimeInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxdatetimeinput';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxTooltipComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxtooltip';
import { jqxCheckBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcheckbox';
import { jqxRadioButtonComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxradiobutton';
import { jqxListBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxlistbox';
import { jqxPasswordInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxpasswordinput';
import { jqxExpanderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxexpander';
import { jqxTreeGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxtreegrid';
import { jqxMaskedInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxmaskedinput';
import { jqxTabsComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxtabs';
import { jqxLayoutComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxlayout';
import { jqxDockingComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxdocking';
import { jqxChartComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxchart';
import { NepaliRankPipe } from '../pipes/nepali-rank.pipe';
import { DisplayNepaliDatePipe } from '../pipes/display-nepali-date.pipe';
import { FileUploadModule } from 'ng2-file-upload';
import { NpdigitTextPipe } from '../pipes/npdigit-text.pipe';
import { DigitCommasPipe } from '../pipes/digit-commas.pipe';
import { FormHelperModule } from '../form-helper/form-helper.module';

// import { NgxPermissionsModule } from 'ngx-permissions';

// import {
//   TreeviewModule,
//   DropdownTreeviewComponent,
//   TreeviewConfig,
//   TreeviewI18nDefault,
//   TreeviewI18n,
//   DefaultTreeviewEventParser,
//   TreeviewEventParser
// } from 'ngx-treeview';

@NgModule({
    declarations: [
        jqxPanelComponent,
        jqxInputComponent,
        jqxTextAreaComponent,
        jqxButtonComponent,
        jqxWindowComponent,
        jqxTreeComponent,
        jqxDataTableComponent,
        jqxGridComponent,
        jqxValidatorComponent,
        jqxNumberInputComponent,
        jqxDropDownListComponent,
        jqxComboBoxComponent,
        jqxDateTimeInputComponent,
        jqxLoaderComponent,
        jqxNotificationComponent,
        jqxTooltipComponent,
        jqxCheckBoxComponent,
        jqxRadioButtonComponent,
        jqxListBoxComponent,
        jqxPasswordInputComponent,
        jqxExpanderComponent,
        jqxTreeGridComponent,
        jqxMaskedInputComponent,
        jqxTabsComponent,
        ListFilterPipe,
        DateSplitPipe,
        jqxLayoutComponent,
        jqxDockingComponent,
        jqxChartComponent,
        NepaliInputComponent,
        LetterheadComponent,
        NepaliTextareaComponent,
        NepaliRankPipe,
        DisplayNepaliDatePipe,
        DigitCommasPipe,
        NpdigitTextPipe
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        FileUploadModule,
        // NgxPermissionsModule
        // TreeviewModule
    ],
    providers: [
        // TreeviewConfig,
        // { provide: TreeviewI18n, useClass: TreeviewI18nDefault },
        // { provide: TreeviewEventParser, useClass: DefaultTreeviewEventParser }
    ],
    exports: [
        FormsModule,
        CommonModule,
        FileUploadModule,
        ReactiveFormsModule,
        TranslateModule,
        jqxPanelComponent,
        jqxInputComponent,
        jqxTextAreaComponent,
        jqxButtonComponent,
        jqxTreeComponent,
        jqxDataTableComponent,
        jqxGridComponent,
        jqxWindowComponent,
        jqxDropDownListComponent,
        jqxValidatorComponent,
        jqxNumberInputComponent,
        jqxComboBoxComponent,
        jqxDateTimeInputComponent,
        jqxLoaderComponent,
        jqxNotificationComponent,
        jqxTooltipComponent,
        jqxCheckBoxComponent,
        jqxRadioButtonComponent,
        jqxListBoxComponent,
        jqxPasswordInputComponent,
        jqxExpanderComponent,
        jqxTreeGridComponent,
        jqxMaskedInputComponent,
        jqxTabsComponent,
        ListFilterPipe,
        DateSplitPipe,
        jqxLayoutComponent,
        jqxDockingComponent,
        jqxChartComponent,
        NepaliInputComponent,
        NepaliTextareaComponent,
        NepaliRankPipe,
        LetterheadComponent,
        DisplayNepaliDatePipe,
        DigitCommasPipe,
        NpdigitTextPipe
        // NgxPermissionsModule
        // TreeviewModule
    ]
})
export class SharedModule { }
