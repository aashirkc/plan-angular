import { TestBed, async, inject } from '@angular/core/testing';

import { CanNavigateRouteGuard } from './can-navigate-route.guard';

describe('CanNavigateRouteGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanNavigateRouteGuard]
    });
  });

  it('should ...', inject([CanNavigateRouteGuard], (guard: CanNavigateRouteGuard) => {
    expect(guard).toBeTruthy();
  }));
});
