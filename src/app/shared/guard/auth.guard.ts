import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthenticateService } from '../services/authenticate.service';
import { CurrentUserService } from '../services/current-user.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

    jwtHelper: JwtHelperService = new JwtHelperService();
    callTime: number = 0;
    constructor(
        private router: Router,
        private authService: AuthenticateService,
        private cus:CurrentUserService
    ) { }

    canActivate() {
        let result = this.checkUser();
        return result;
    }

    canActivateChild() {
        let result = this.checkUser();
        return result;
    }

    checkUser() {
        let currentToken = localStorage.getItem('pcToken') || null;
        // let currentToken = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJNQU5PSiIsImlhdCI6MTUzNDY2NjcwMCwic3ViIjoiTUFOT0oiLCJpc3MiOiJNQU5PSiIsImV4cCI6MTU1OTI0MDEwMH0.rDIzR8a9_aEs430COk_ftEHQE-b7hBzaByb69JkDbWo";
        this.callTime++;
        if (currentToken && !this.jwtHelper.isTokenExpired(currentToken)) {
            let tokenTime = new Date(this.jwtHelper.getTokenExpirationDate(currentToken)).getTime();
            let currentDate = new Date().getTime();
            let timeRemaining = (tokenTime - currentDate);
            let diffmin = Math.round(((timeRemaining % 86400000) % 3600000) / 60000);
            if (this.callTime == 1) {
                if (diffmin < 20 && diffmin >= 0) {
                    let currentUser = this.cus.getTokenData();
                    let id = currentUser['user']['userId'];
                    this.authService.loginTokenUpdate(id).subscribe((res) => {
                        if (res['token']) {
                            localStorage.removeItem('pcToken');
                            localStorage.setItem('pcToken', res['token']);
                        }
                    }, (error) => {
                        this.authService.logout();
                    });
                }
            }
            return true;
        }
        if (currentToken && this.jwtHelper.isTokenExpired(currentToken)) {
            // console.log('Token Expired check in Auth Guard.');
            this.removeData();
            this.router.navigate(['/login'], { queryParams: { type: 'error', msg: 'Session Expired Please login to continue' } });
            return false;
        }
        if (currentToken) {
            return true;
        }
        this.removeData();
        this.router.navigate(['/login']);
        return false;
    }

    removeData() {
        localStorage.removeItem('pcToken');
    }
}
