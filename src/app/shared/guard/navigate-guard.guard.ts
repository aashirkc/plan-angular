import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild } from '@angular/router';

@Injectable()
export class NavigateGuard implements CanActivate, CanActivateChild {

  canActivate() {
    console.log('i am checking to see if you are logged in');
    var t = confirm('Do You Want To Leave');
    if(t== true){
      return true;
    }
    return false;
  }

  canActivateChild() {
    console.log('i am checking to see if you are logged in');
      var t = confirm('Do You Want To Leave');
      if(t== true){
        return true;
      }
      return false;
  }

}