import { FormArray, FormControl, FormGroup, ValidationErrors } from '@angular/forms';

export class CustomValidators {

  static monthDay(c: FormControl): ValidationErrors {
    const numValue = c.value;
    let isValid = false;
    let message = {};

    if (c.value && typeof c.value == 'string') {
      let dayValue = parseInt('0' + c.value);
      if (typeof dayValue == 'number') {
        (dayValue >= 0 && dayValue < 30) ? isValid = true : isValid = false;
      } else {
        isValid = false;
      }
    }
    message = {
      'number': 'Must be between 0 and 29',
    };

    return isValid ? null : message;
  }

  static checkDate(c: FormControl): ValidationErrors {
    // First check for the pattern
    let dateString = c.value;
    let isValid = false;
    let message = {
      'number': 'Invalid Date Format',
    };

    let pattern = /([12]\d{3}-(0[1-9]|1[0-2])-\b(0[1-9]|[12]\d|3[0-2])\b)/;
    if (!pattern.test(dateString)) {
      return isValid ? null : message;
    }

    // Parse the date parts to integers
    var parts = dateString.split("-");
    var day = parseInt(parts[2], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[0], 10);

    // Check the ranges of month and year
    if (year < 1000 || year > 3000 || month == 0 || month > 12) {
      return isValid ? null : message;
    }


    isValid = true;
    return isValid ? null : message;
  }
}