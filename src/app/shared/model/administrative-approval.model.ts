export class AdministrativeApproval {
    issuingAuthority: string;
    // prasasanikSakha: string;
    workOrderDate: string;
    workOrderNo: string;
    workServiceType: string;
    name: string;
    address: string;
    contactPerson: string;
    contactNumber: string;
    contractDate: string;
    wadaRepresentative: string;

    befinitedOrganizations: string;
    benifitedHouses: string;
    benifitedPopulation: string;
    consumerCommitteAddress: string;
    consumerCommitteName: string;
    grantedFromConsumerCommitte: string;
    grantedFromIngo: string;
    grantedFromLocalLevel: string;
    grantedFromNgo: string;
    grantedFromOthers: string;
    grantedFromState: string;
    grantedFromUnion: string;
    othersBenifited: string;

    formationDate: string;
    bankName: string;
    acNo: string;
    agreementDate: string;
    estimatedWorkCompletionDate: string;
    malePopulation: string;
    femalePopulation: string;
    dalitPopulation: string;
    janajatiPopulation: string;
    othersPopulation: string;
    contractBankAcNo: string;
    contractBankName: string;
    contractBankAcHolder: string;
    contractAmount: string;

    constructor(obj?: any) {
        this.issuingAuthority = (obj && obj.issuingAuthority) || '';
        // this.prasasanikSakha = obj && obj.prasasanikSakha || '';
        this.workOrderDate = (obj && obj.workOrderDate) || '';
        this.workOrderNo = (obj && obj.workOrderNo) || '';
        this.name = (obj && obj.name) || '';
        this.address = (obj && obj.address) || '';
        this.contactPerson = (obj && obj.contactPerson) || '';
        this.contactNumber = (obj && obj.contactNumber) || '';
        this.contractDate = (obj && obj.contractDate) || '';
        this.workServiceType = (obj && obj.workServiceType) || '';
        this.wadaRepresentative = (obj && obj.wadaRepresentative) || '';

        this.contractBankAcNo = (obj && obj.contractBankAcNo) || '';
        this.contractBankName = (obj && obj.contractBankName) || '';
        this.contractBankAcHolder = (obj && obj.contractBankAcHolder) || '';
        this.contractAmount = (obj && obj.contractAmount) || '';

        this.befinitedOrganizations = (obj && obj.befinitedOrganizations) || '';
        this.benifitedHouses = (obj && obj.benifitedHouses) || '';
        this.benifitedPopulation = (obj && obj.benifitedPopulation) || '';
        this.consumerCommitteAddress =
            (obj && obj.consumerCommitteAddress) || '';
        this.consumerCommitteName = (obj && obj.consumerCommitteName) || '';
        this.grantedFromConsumerCommitte =
            (obj && obj.grantedFromConsumerCommitte) || '';
        this.grantedFromIngo = (obj && obj.grantedFromIngo) || '';
        this.grantedFromLocalLevel = (obj && obj.grantedFromLocalLevel) || '';
        this.grantedFromNgo = (obj && obj.grantedFromNgo) || '';
        this.grantedFromOthers = (obj && obj.grantedFromOthers) || '';
        this.grantedFromState = (obj && obj.grantedFromState) || '';
        this.grantedFromUnion = (obj && obj.grantedFromUnion) || '';
        this.othersBenifited = (obj && obj.othersBenifited) || '';

        this.formationDate = (obj && obj.formationDate) || '';
        this.bankName = (obj && obj.bankName) || '';
        this.acNo = (obj && obj.acNo) || '';
        this.agreementDate = (obj && obj.agreementDate) || '';
        this.estimatedWorkCompletionDate =
            (obj && obj.estimatedWorkCompletionDate) || '';
        this.malePopulation = (obj && obj.malePopulation) || '';
        this.femalePopulation = (obj && obj.femalePopulation) || '';
        this.dalitPopulation = (obj && obj.dalitPopulation) || '';
        this.janajatiPopulation = (obj && obj.janajatiPopulation) || '';
        this.othersPopulation = (obj && obj.othersPopulation) || '';
    }
}
