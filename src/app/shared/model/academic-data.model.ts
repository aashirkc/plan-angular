export class AcademicData {
    board:string;
    institute:string;
    level:string;
    majorSubjects:string;
    obtainedMarks:string;
    obtainedType:string;
    passedYear:string;
    program:string;
    regNo:string;
  
   constructor(obj?: any) {
    this.board = obj && obj.board || '';
    this.institute = obj && obj.institute || '';
    this.level = obj && obj.level || '';
    this.majorSubjects = obj && obj.majorSubjects || '';
    this.obtainedMarks = obj && obj.obtainedMarks || '';
    this.obtainedType = obj && obj.obtainedType || '';
    this.passedYear = obj && obj.passedYear || '';
    this.program = obj && obj.program || '';
    this.regNo = obj && obj.regNo || '';
   
   }
}
