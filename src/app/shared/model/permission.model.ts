export class Permission {

    id: string;
    faculty: string;
    userCode:string;
    constructor(obj?: any) {
        this.id = obj && obj.id || '';
        this.faculty = obj && obj.faculty || '';
        this.userCode = obj && obj.userCode || '';
    }
}
