export class FinancialProgress {
    enterDate: string;
    issueDate: string;
    issueAmount: string;
    billedAmount: string;
    activity: string;
    id: string;
    workCompletionPercentage: string;
    voucherNo: string;
    // investigationStage: string;

    constructor(obj?: any) {
        this.enterDate = obj && obj.enterDate || '';
        this.activity = obj && obj.activity || '';
        this.id = obj && obj.id || '';
        this.issueAmount = obj && obj.issueAmount || 0;
        this.billedAmount = obj && obj.billedAmount || 0;
        this.issueDate = obj && obj.issueDate || 0;
        this.workCompletionPercentage = obj && obj.workCompletionPercentage || 0;
        this.voucherNo = obj && obj.voucherNo || 0;
        // this.investigationStage = obj && obj.investigationStage && obj.investigationStage.id || 0;
    }
}