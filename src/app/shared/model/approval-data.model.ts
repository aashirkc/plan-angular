export class ApprovalData {
    orderIssuing: string;
    proposedCost: number;
    techCost: number;
    techOrderNo: number;
    techDate: Date;
    // yojanaSakha: string;

    constructor(obj?: any) {
        this.orderIssuing = obj && obj.orderIssuing || '';
        this.proposedCost = obj && obj.proposedCost || '';
        this.techCost = obj && obj.techCost || '';
        this.techOrderNo = obj && obj.techOrderNo || '';
        this.techDate = obj && obj.techDate;
        // this.yojanaSakha = obj && obj.yojanaSakha || '';
        
    }
}