import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { jqxMenuComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxmenu';
import { jqxTreeComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxtree';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    pushRightClass: string = 'push-right';
    activityDatas:any={};
    userData:any;
    activityStatus:any='';
    constructor(private translate: TranslateService, public router: Router,) {
        // this.router.events.subscribe((val) => {
        //     if (val instanceof NavigationEnd && window.innerWidth <= 992 && this.isToggled()) {
        //         this.toggleSidebar();
        //     }
        // });
    }
    ngOnInit(): void {
        // this.ts.currentMessage.subscribe((data) => {
        //     if(data=="on")
        //     {this.activityDatas=JSON.parse(localStorage.getItem('ActivityDatas'));
        //     if (this.activityDatas['executionReportStatus'] == 'Not Started') {
        //         // return this.transData['NOT_STARTED'];
        //         this.activityStatus="सुरु भएको छैन"
        //       } else if (this.activityDatas['executionReportStatus'] == 'Ongoing') {
        //         // return this.transData['ONGOINGE'];
    
        //         this.activityStatus="चलिरहेको योजना"
        //       } else if (this.activityDatas['executionReportStatus'] == 'Completed') {
        //         // return this.transData['COMPLETEDE'];
        //         this.activityStatus="पुर्ण भएको योजना"
        //       } else if (this.activityDatas['executionReportStatus'] == 'Suspended') {
        //         // return this.transData['SUSPENDED'];
        //         this.activityStatus="निलम्बित"
        //       } else if (this.activityDatas['executionReportStatus'] == 'Spill Over') {
        //         // return this.transData['SPILLOVERE'];
        //         this.activityStatus="समयावधि थपिएको"
        //       } else if (this.activityDatas['executionReportStatus'] == 'Abandoned') {
        //         // return this.transData['ABANDONEDE'];
        //         this.activityStatus="त्यागिएको"
        //       }}
        //     if(data=="off")
        //     {

        //     }
        //   });
        
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.

    }
    @ViewChild('myTree') myTree: jqxTreeComponent;
    // @ViewChild('myMenu') myMenu: jqxMenuComponent;
    myTreeOnInitialized(): void {
        this.myTree.selectItem(document.getElementById('home'));
        this.myTree.expandItem(document.getElementById('solutions'));
        // document.addEventListener('contextmenu', event => {
        //     event.preventDefault();

            // if ((event.target).classList.contains('jqx-tree-item')) {
            //     this.myTree.selectItem(event.target);
            //     let scrollTop = window.scrollY;
            //     let scrollLeft = window.scrollX;
            //     this.myMenu.open(event.clientX + 5 + scrollLeft, event.clientY + 5 + scrollTop);
            //     return false;
            // } else {
            //     this.myMenu.close();
            // }


        // });
    }

    ngAfterViewInit() {
        // this.myTree.elementRef.nativeElement.firstChild.style.border = 'none';
    }
    // select(event: any): void {
    //     this.ContentPanel.nativeElement.innerHTML = "<div style='margin: 10px;'>" + event.args.element.id + "</div>";
    // }

    ItemClick(event: any): void {
        let userDetalils = JSON.parse(localStorage.getItem('pcUser'));
        this.userData = userDetalils['userType'];
        if (!Number(event.args.element.id)) {
            // this.router.navigate([event.args.element.id]);
            let nav=event.args.element.id;
            this.activityDatas=JSON.parse(localStorage.getItem('ActivityDatas'));
            console.log(this.activityDatas);
            if(nav==='technical-approval'){
                
               
                if (this.activityDatas['outputType'] == null || this.activityDatas['outputType'] == 'N') {
                   
                    window.alert('कृपया पहिले गतिविधि परिभाषित गर्नुहोस्!!');
                    this.router.navigate(["/activity/activity-output"]);
                  
                } else {
                    if (this.userData.toLowerCase() == 'activity manager' || this.userData.toLowerCase() == 'admin') {
                        if (this.activityDatas['id']) {
                            if (this.activityDatas['administrativeApproval'] == 'N') {
                                if (this.activityDatas['taStatus'] == 'N') {
                                    this.router.navigate(["/approval/technical-approval/view", { id: this.activityDatas["id"], name: this.activityDatas["activityName"], cost: this.activityDatas["totalEstimatedCost"] }]);
                                } else {      
                                   ///////////////
                                   if (this.userData.toLowerCase() == 'super user') {
                                    if (this.activityDatas['id']) {
                                      
                                        this.router.navigate(['/approval/technical-approval/edit/', this.activityDatas['id']]);
                                    }
                                } else if (this.activityDatas.t_ApprovedBy > 0) {
                                   
                                //    window.alert("स्विकृत भएको");
                                this.router.navigate(["/approval/technical-approval/data-view", { id: this.activityDatas["id"], name: this.activityDatas["activityName"] }]);
                                   
                   
                                } else if (this.activityDatas.t_ApprovedBy == null) {
                                    alert(this.activityDatas.t_ApprovedBy);
                                } else if (this.activityDatas.t_ApprovedBy == 0) {
                                    if (this.activityDatas['executionReportStatus'] == 'Not Started') {
                                    
                                            if (this.userData.toLowerCase() == 'technical approver' || this.userData.toLowerCase() == 'activity manager' || this.userData.toLowerCase() == 'admin') {
                                                if (this.activityDatas['id']) {
                                                   
                                                    this.router.navigate(["/approval/technical-approval/edit", { id: this.activityDatas["id"], name: this.activityDatas["activityName"], cost: this.activityDatas["totalEstimatedCost"] }]);
                                                }
                                            } else {
                                               
                                                window.alert('तपाईलार्इ याे कार्य गर्न स्वीकृती दिएकाे छैन !!');
                                              
                                            }
                                        
                                    } else {
                                        
                                        // window.alert('कार्य पहिले नै सुरु भएको छ !!');
                                        this.router.navigate(["/approval/technical-approval/data-view", { id: this.activityDatas["id"], name: this.activityDatas["activityName"] }]);
                                    }
                                }
                                    //////////////////
                                }
                            } else {
                                // window.alert('या तपाईले पहिलेनै स्वीकृति गर्नु भएको छ वा प्रशासनिक अनुमोदन गरेको छ?!!');
                                this.router.navigate(["/approval/technical-approval/data-view", { id: this.activityDatas["id"], name: this.activityDatas["activityName"] }]);
                            }
                        }
                    } else {
                        // window.alert('तपाईलार्इ याे कार्य गर्न स्वीकृती दिएकाे छैन !!');
                        this.router.navigate(["/approval/technical-approval/data-view", { id: this.activityDatas["id"], name: this.activityDatas["activityName"] }]);
                       
                    }
                }
            }

            // if(nav==="technical-approval-entry"){
            //     if (this.activityDatas['outputType'] == null || this.activityDatas['outputType'] == 'N') {
                   
            //                 window.alert('कृपया पहिले गतिविधि परिभाषित गर्नुहोस्!!');
            //                 this.router.navigate(["/activity/activity-output"]);
                          
            //             } else {
            //                 if (this.userData.toLowerCase() == 'activity manager' || this.userData.toLowerCase() == 'admin') {
            //                     if (this.activityDatas['id']) {
            //                         if (this.activityDatas['administrativeApproval'] == 'N') {
            //                             if (this.activityDatas['taStatus'] == 'N') {
            //                                 this.router.navigate(["/approval/technical-approval/view", { id: this.activityDatas["id"], name: this.activityDatas["activityName"], cost: this.activityDatas["totalEstimatedCost"] }]);
            //                             } else {      
            //                                ///////////////
            //                                window.alert('तपाईले पहिलेनै स्वीकृति गर्नु भएको छ !!');
            //                                 //////////////////
            //                             }
            //                         } else {
            //                             window.alert('या तपाईले पहिलेनै स्वीकृति गर्नु भएको छ वा प्रशासनिक अनुमोदन गरेको छ?!!');
                                       
            //                         }
            //                     }
            //                 } else {
            //                     window.alert('तपाईलार्इ याे कार्य गर्न स्वीकृती दिएकाे छैन !!');
                               
            //                 }
            //             }
            // }
            // if(nav==="technical-approval-edit"){
            //     if (this.userData.toLowerCase() == 'super user') {
            //                                 if (this.activityDatas['id']) {
                                              
            //                                     this.router.navigate(['/approval/technical-approval/edit/', this.activityDatas['id']]);
            //                                 }
            //                             } else if (this.activityDatas.t_ApprovedBy > 0) {
                                           
            //                                window.alert("स्विकृत भएको");
            //                             // this.router.navigate(["/approval/technical-approval/data-view", { id: this.activityDatas["id"], name: this.activityDatas["activityName"] }]);
                                           
                           
            //                             } else if (this.activityDatas.t_ApprovedBy == null) {
            //                                 alert(this.activityDatas.t_ApprovedBy);
            //                             } else if (this.activityDatas.t_ApprovedBy == 0) {
            //                                 if (this.activityDatas['executionReportStatus'] == 'Not Started') {
                                            
            //                                         if (this.userData.toLowerCase() == 'technical approver' || this.userData.toLowerCase() == 'activity manager' || this.userData.toLowerCase() == 'admin') {
            //                                             if (this.activityDatas['id']) {
                                                           
            //                                                 this.router.navigate(["/approval/technical-approval/edit", { id: this.activityDatas["id"], name: this.activityDatas["activityName"], cost: this.activityDatas["totalEstimatedCost"] }]);
            //                                             }
            //                                         } else {
                                                       
            //                                             window.alert('तपाईलार्इ याे कार्य गर्न स्वीकृती दिएकाे छैन !!');
                                                      
            //                                         }
                                                
            //                                 } else {
                                                
            //                                     window.alert('कार्य पहिले नै सुरु भएको छ !!');
                                                
            //                                 }
            //                             }
            // }
            
            // if(nav==="technical-approval-view"){
            //     if (this.activityDatas['taStatus'] == 'N') {
                   
            //    window.alert('कृपया पहिले अनुमोदन गर्नुहाेस्');
                
            //     }
            //     else {
            //         this.router.navigate(["/approval/technical-approval/data-view", { id: this.activityDatas["id"], name: this.activityDatas["activityName"] }]);
            //     }
            // }


            if(nav==='administrative-approval'){

                if (this.activityDatas.t_ApprovedBy > 0) {
                    if (this.activityDatas.taStatus == "N") {
                    
                   window.alert('प्राविधिक अनुमोदन गरिएको छैन');

            
                    } else {             
                      if (this.activityDatas['executionReportStatus'] == 'Not Started') {
                        if (this.activityDatas['outputType'] == null) {
                    
                       window.alert('कृपया पहिले गतिविधि परिभाषित गर्नुहोस्!!');
                       this.router.navigate(["/activity/activity-output"]);
                        } else {
                          if (this.userData.toLowerCase() == 'program preparer' || this.userData.toLowerCase() == 'admin') {
                            if (this.activityDatas['id']) {
                              if (this.activityDatas['administrativeApproval'] == 'N') {
                                this.router.navigate(['/approval/administrative-approval/', this.activityDatas['id']]);

                              } else {
                               
                            //  window.alert('तपाईले पहिलेनै अनुमोदन गर्नुभएको छ !!');
                            ////////////
                            if (this.userData.toLowerCase() == 'super user') {
                                if (this.activityDatas['id']) {
                           
                                  this.router.navigate(['/approval/administrative-approval/edit/', this.activityDatas['id']]);
                                }
                              } else if (this.activityDatas.a_ApprovedBy > 0) {
                                
                                // window.alert('स्विकृत भएको');
                                this.router.navigate(['/approval/administrative-approval/view/', this.userData['id']]);
                              } else {
                                  if (this.userData.toLowerCase() == 'program approver' || this.userData.toLowerCase() == 'program preparer' || this.userData.toLowerCase() == 'admin') {
                                    if (this.activityDatas['id']) {
                                   
                                      this.router.navigate(['/approval/administrative-approval/edit/', this.activityDatas['id']]);
                                    }
                                  }
                                  else {
                                
                                    window.alert('तपाईलार्इ याे कार्य गर्न स्वीकृती दिएकाे छैन !!');
                                  
                    
                                  }
                                
                              }
                              ////////////
                              }
                            }
        
                          } else {
                           
                         window.alert('तपाईलार्इ याे कार्य गर्न स्वीकृती दिएकाे छैन !!');
                          
                          }
                        }
                      } else {
                        
                    //  window.alert('कार्य पहिले नै सुरु भयो');
                    this.router.navigate(['/approval/administrative-approval/view/', this.activityDatas['id']]);
                     
                      }
                    }
        
                  } else {
             
                 window.alert('प्राविधिक स्वीकृत भएको छैन');
                    
                  }
            }
            if(nav==='samjauta'){
                this.router.navigate(['/approval/administrtive-approval-aggrement', this.activityDatas['id']]);
            }
            if(nav==='karyadesh'){
                if (this.activityDatas['a_ApprovedBy'] <= 0) {
                   
                   window.alert('स्विकृत नभएको');
             
                  }else {
                  this.router.navigate(['/approval/administrative-approval-karyadesh/', this.activityDatas['id']]);
                  }
            }
            if(nav==='tippani'){
                if (this.activityDatas['a_ApprovedBy'] <= 0) {
                   
                   window.alert('स्विकृत नभएको');
             
                  }else {
                //   this.router.navigate(['/approval/administrative-approval-karyadesh/', this.activityDatas['id']]);
                  this.router.navigate(['/approval/tipani-adesh/', this.activityDatas['id']]);
                  }
            }

            if(nav==='khata-open'){
                this.router.navigate(["/khata-sanchalan/about-financial-operation"]);
            }

            if(nav==='khata-close'){
                this.router.navigate(["/khata-sanchalan/about-account-close"]);
            }
            if(nav==='khata-edit'){
                this.router.navigate(["/khata-sanchalan/account-edit"]);
            }
            if(nav==='khata-name-change'){
                this.router.navigate(["/khata-sanchalan/name-change"]);
            }
            if(nav==='khata-pharpharak'){
                this.router.navigate(["/khata-sanchalan/farfaarak"]);
            }

            if(nav==='anusuchi3'){
                this.router.navigate(["/report/anusuchi3"]);
            }
            if(nav==='anusuchi4'){
                this.router.navigate(["/report/anusuchi4"]);
            }
            if(nav==='anusuchi5'){
                this.router.navigate(["/report/anusuchi5"]);
            }
            if(nav==='anusuchi6'){
                this.router.navigate(["/report/anusuchi6"]);
            }

            if(nav==='public-exam'){
                this.router.navigate(["/report/sarbajanik-faram"]);
            }

            if(nav==='bhoutik'){
                if (this.activityDatas['executionReportStatus'] == "Not Started") {
                   
               window.alert('योजना सुरु भएको छैन');
                   
                  } else {
              
                    if (this.activityDatas['outputType'] == null || this.activityDatas['outputType'] == 'N') {
                  
                   window.alert('Activity has not been defined!!');
                   
                    } else {
                      this.router.navigate(['/progress-reporting/progress-reporting-physical-progress/', this.activityDatas['id'], { outputType: this.activityDatas['outputType'] }]);
                    }
                  }
            }

            if(nav==='aarthik'){
                if (this.activityDatas['executionReportStatus'] == "Not Started") {
                   
                  window.alert('योजना सुरु भएको छैन');
                   
                  } else {
                
                    if (this.activityDatas['executionReportStatus'] != 'Not Started') {
                      
                      if (this.activityDatas['id']) {
                       
                        this.router.navigate(['/progress-reporting/progress-reporting-financial-progress/', this.activityDatas['id']]);
                      }
                    } else {
                     
                    window.alert('Work Not Started');
                      
                    }
                  }
            }

            if(nav==='completion'){
                if (this.activityDatas['executionReportStatus'] == "Completed") {

                   
                    this.router.navigate(['/progress-reporting/progress-reporting-completion-certificate/', this.activityDatas['id'], { outputType: this.activityDatas['outputType'] }]);
                  } else {
                
                  window.alert("योजना पुरा भएको छैन");
                   
                  }
            }
            if(nav==='generate'){
                this.router.navigate(['generate-all-report']);
            }
            if(nav==='get-all-files'){
                this.router.navigate(['get-all-files']);
            }


           

        }
    }

}
