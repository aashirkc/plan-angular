import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-quickaccess',
  templateUrl: './quickaccess.component.html',
  styleUrls: ['./quickaccess.component.scss']
})
export class QuickaccessComponent implements OnInit {

  public CurrentPath:string;
  
  links:any = [];

  searchText:string;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    this.router = router;
    this.CurrentPath = this.router.parseUrl(this.router.url).root.children.primary.segments[0].path;
  
  }

  ngOnInit() {
    this.getCurrentLinks();
  }

  getCurrentLinks(){
    let currentModule:string = this.CurrentPath; 
    switch(currentModule) { 
       case "inventory": { 
          this.links = [
            { name: 'Product Class Master' ,path:'/inventory/product-class-master' },
            { name:'Product Group Master', path:'/inventory./product-group-master'},
            { name:'Product Category Master' , path:'/inventory/product-category-master'},
            { name:'Product Sub Category Master', path:'/inventory/product-subcategory-master'},
            { name:'Item Master', path:'/inventory/item-master'},
            { name:'Unit Master', path:'/inventory/unit-master', },
            { name:'Welfare Item Master', path:'/inventory/welfare-item-master'},
            { name:'Po Requisition Detail Master', path:'/inventory/po-requisition-detail-master'},
            { name:'Requisition Detail Master', path:'/inventory/requisition-detail-master'},
            { name:'GRN Po Master', path:'/inventory/grn-po-master'},
            { name:'Direct Po Master', path:'/inventory/direct-po-master'},
            { name:'Direct Po Item Master', path:'/inventory/direct-po-item-master'},
            { name:'Garden Po Master', path:'/inventory/garden-po-master'},
            { name:'Garden Po Item Master', path:'/inventory/garden-po-item-master'},
            { name:'Material Issue', path:'/inventory/material-issue'},
            { name:'Account Head', path:'/inventory/account-head'},
            { name:'Vendor', path:'/inventory/vendor'},
            { name:'Store', path:'/inventory/store'},
            { name:'TE-Direct Grn', path:'/inventory/te-direct-grn'},
            { name:'TE-PO Grn', path:'/inventory/te-po-grn'},
          ]
          break; 
       } 
       case "setup": { 
        this.links = [
          { name:'Company Master' ,path:'/setup/company-master' },
          { name:'Garden Master', path:'/setup/garden-master'},
          { name:'Division Master' , path:'/setup/division-master'},
          { name:'Department Master' , path:'/setup/department-master'},
          { name:'Designation Master' , path:'/setup/designation-master'},
          { name:'Paybook Master' , path:'/setup/paybook-master'},
          { name:'Holiday Master' , path:'/setup/holiday-master'},
          { name:'Leave Type Master' , path:'/setup/leave-type-master'},
          { name:'Cost Center Master' , path:'/setup/cost-center-master'},
        ]
          break; 
       } 
       case "sectional": {
        this.links = [
          { name:'Section Master' ,path:'/sectional/section-master' },
        ]
          break;    
       } 
       case "factory-master": { 
        this.links = [
          { name:'Dryer' ,path:'/factory-master/dryer' },
          { name:'Tea Made Process' ,path:'/factory-master/tea-made-process' },
          { name:'Excess Tea Made' ,path:'/factory-master/excess-teamade' },
          { name:'Tea Grade Master' ,path:'/factory-master/tea-grade' },
          { name:'Tea Grade Sorting' ,path:'/factory-master/tea-grade-sorting' },
          { name:'Tea Grade Transfer' ,path:'/factory-master/tea-grade-transfer' },
          { name:'Sister Garden Master' ,path:'/factory-master/sister-garden' },
          { name:'Daily Supplier Bought Leaf' ,path:'/factory-master/daily-bought-leaf' },
          { name:'Supplier' ,path:'/factory-master/supplier' },
          { name:'Sister Garden Daily Bought Leaf' ,path:'/factory-master/sister-garden-daily-bought-leaf' },
          { name:'Daily Leaf - Own Garden' ,path:'/factory-master/garden-daily-leaf' },
          { name:'Lot Number', path:'/factory-master/lot-number'},
          { name:'Tough Number', path:'/factory-master/tough-number'},
          { name:'Production Stock', path:'/factory-master/production-stock'},
        ]
          break; 
       } 
       case "field-module": {
        this.links = [
          { name:'Shade Tree Type' ,path:'/field-module/shade-tree-type' },
          { name:'Bush Type' ,path:'/field-module/bush-type' },
          { name:'Bush Variety Type' ,path:'/field-module/bush-variety' },
          { name:'Sectional Information History' ,path:'/field-module/section-history' },
          { name:'Sectional Infilling History' ,path:'/field-module/sectional-infilling-history' },
          { name:'Section Field Activity' ,path:'/field-module/section-field-activity' },
          { name:'Irrigation Unit' ,path:'/field-module/irrigation-unit' },
          { name:'Irrigation Activity' ,path:'/field-module/irrigation-activity' },
          { name:'Sectional Shade Tree History' ,path:'/field-module/sectional-shade-tree' },
          { name:'Sectional Plucking History' ,path:'/field-module/sectional-plucking-history' },
          { name:'Sectional Weather History' ,path:'/field-module/sectional-weather-history' },
          { name:'Seed/Clone Master' ,path:'/field-module/seed-clone-master' },
          { name:'Nursery Master' ,path:'/field-module/nursery-master' },
        ]
          break;    
       }  
       case "pruning-master": {
        this.links = [
          { name:'Prune Plan' ,path:'/pruning-master/prune-plan' },
          { name:'Prune Style' ,path:'/pruning-master/prune-style' },
        ]
          break;    
       } 
       case "pay-roll": {
        this.links = [
          { name:'Staff Master' ,path:'/pay-roll/staff-master' },
          { name:'Allowance Master' ,path:'/pay-roll/earning-master' },
          { name:'Staff Attendence Master' ,path:'/pay-roll/staff-attendence-master' },
          { name:'Leave Master' ,path:'/pay-roll/leave-master' },
          { name:'Holiday Master' ,path:'/pay-roll/holiday-master' },
          { name:'Deduction Master' ,path:'/pay-roll/deduction-master' },
          { name:'Worker Wages Calculation' ,path:'/pay-roll/worker-wages-calculation' },
        ]
          break;    
       } 
       case "budgets": {
        this.links = [
          { name:'Plucking Mandays Calculation' ,path:'/budgets/plucking-mandays-calculation' },
          { name:'Activity Mandays Estimation' ,path:'/budgets/activity-mandays-estimation' },
          { name:'Activity Monthly Budget' ,path:'/budgets/activity-monthly-budget' },
        ]
          break;    
       }
       case "activity": {
        this.links = [
          { name:'Activity Master' ,path:'/activity/activity-master' },
          { name:'Worker Master' ,path:'/activity/worker-master' },
        ]
          break;    
       }
       case "reports": {
        this.links = [
          { name:'Daily Green Leaf Report' ,path:'/reports/daily-green-leaf-report' },
          { name:'Supplier Wise Bought' ,path:'/reports/supplier-wise-bought-leaf-report' },
          { name:'Dryer Details Log Report' ,path:'/reports/dryer-details-log-report' },
          { name:'Sectionwise Green Leaf Report',path:'/reports/section-wise-green-leaf-report'},
          { name:'Production Analysis Report',path:'/reports/production-analysis-report'},
          { name:'Tea Grade Sorting Report',path:'/reports/tea-grade-sorting-report'},
          { name:'Garden Weather Report',path:'/reports/garden-weather-report'},
          { name:'Daily Factory Report',path:'/reports/daily-factory-report'},
          { name:'Irrigation Progress Report',path:'/reports/irrigation-progress-report'},
          { name:'Daily Packing Report',path:'/reports/daily-packing-report'},
          { name:'Yield Report',path:'/reports/section-yield-report'},
          { name:'Nursery Plant Distribution Report',path:'/reports/nursery-plant-distribution-report'},
          { name:'Nursery Stock Summary Report',path:'/reports/nursery-stock-summary-report'},
          { name:'Section History Report',path:'/reports/section-history-report'},
          { name:'Activity Wise Kamjari Report',path:'/reports/activity-wise-kamjari-report'},
          { name:'Daily Control Sheet Permanent Report',path:'/reports/daily-control-sheet-permanent-report'},
          { name:'Daily Control Sheet Temporary Report',path:'/reports/daily-control-sheet-temporary-report'},
          { name:'Fortnight Attendance Sheet Report',path:'/reports/fortnight-attendance-sheet-report'},
          { name:'Paybook Wise Worker Details Report',path:'/reports/paybook-wise-worker-details-report'},
          { name:'Worker Leave Status Report',path:'/reports/worker-leave-status-report'},
          { name:'Activity Master Checklist Report',path:'/reports/activity-master-checklist-report'},
          { name:'Daily Wages Register Report',path:'/reports/daily-wages-register-report'},
          { name:'Worker Bonus Earning Register Report',path:'/reports/worker-bonus-earning-register-report'},
          { name:'Work Force Summary Report',path:'/reports/work-force-summary-report'},
          { name:'Staff Salary Register Report',path:'/reports/staff-salary-register-report'},
          { name:'Worker Fortnightly PF Leadger Report',path:'/reports/worker-fortnightly-pf-ledger-report'},
          { name:'PF Deduction Register Report',path:'/reports/pf-deduction-register-report'},
          { name:'Leave With Wages Register Report',path:'/reports/leave-with-wages-register-report'},
          { name:'Wage Summary Report',path:'/reports/wage-summary-report'},
          { name:'OverTime Earning Report',path:'/reports/over-time-earning-report'},
          { name:'Fortnight Summary Report',path:'/reports/fortnight-summary-report'},
          { name:'Item Category Summary',path:'/reports/item-category-summary-report'},
          { name:'GRNs for which Purchase bills Pending',path:'/reports/grns-for-which-purchase-bills-pending-report'},
          { name:'Opening Balance Reports',path:'/reports/opening-balance-report'},
          { name:'Issue Register Date & Item Category Wise',path:'/reports/issue-register-date-and-item-category-wise-report'},
          { name:'Store Consumption Summary',path:'/reports/store-consumption-summary'},
          { name:'Bill Register (Item Wise)',path:'/reports/bill-register-item-wise-report'},
          { name:'Item Wise Store Receipt Register',path:'/reports/item-wise-store-receipt-register-report'},
          { name:'Item Stock Balance Report',path:'/reports/item-stock-balance-report'},
          { name:'Purchase Order Checklist',path:'/reports/purchase-order-checklist-report'},
          { name:'Requisition Checklist Reports',path:'/reports/requisition-checklist-reports'},
          { name:'Purchase Order Supplier Wise',path:'/reports/purchase-order-supplier-wise-report'},
          { name:'Store Ledger Report',path:'/reports/store-ledger-report'},
          { name:'Daily Factory Kamjari Report',path:'/reports/daily-factory-kamjari-report'},
          { name:'Daily Garden Kamjari Report',path:'/reports/daily-garden-kamjari-report'},
          { name:'Daily Store Log Report',path:'/reports/daily-store-log-report'},
          { name:'Goods Receipt Report',path:'/reports/goods-receipt-report'},
          { name:'Material Issue Report',path:'/reports/material-issue-report'},
          { name:'Po With Requisition Report',path:'/reports/po-with-requisition-report'},
          { name:'Po Direct Report',path:'/reports/po-direct'},
          { name:'Tea Dispatch Report',path:'/reports/tea-dispatch-report'},
          { name:'Sample Tea Dispatch Report',path:'/reports/sample-tea-dispatch-report'},
        ]
          break;    
       }
       case "user-master": {
        this.links = [
          { name:'Manage Users' ,path:'/user-master/users' },
          { name:'Worker Roles' ,path:'/user-master/roles' },
          { name:'Worker Permissions' ,path:'/user-master/permissions' },
        ]
          break;    
       }
       default: { 
          console.log("Invalid choice"); 
          break;              
       } 
    }
  }



}
