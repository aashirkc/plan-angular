import { Component, OnInit, Inject } from '@angular/core';
// import {  AllReportService } from './shared';
import { AllReportService } from '../../services/all-report.service';

@Component({
    selector: 'app-letterhead',
    templateUrl: './letterhead.component.html',
    styleUrls: ['./letterhead.component.scss']
})
export class LetterheadComponent implements OnInit {
    heading: any;
    imgSrc: any = 'assets/images/logo.jpg';
    distType: string;

    constructor(
        private report: AllReportService,
        @Inject('DIST_TYPE') distType: string
    ) {
        this.distType = distType;
    }

    ngOnInit() {
        this.report.getOrg().subscribe(
            result => {
                if (result.length == 1 && result[0].error) {
                    this.heading = [];
                } else {
                    this.heading = result[0];
                }
            },
            error => {
                console.info(error);
            }
        );
    }
}
