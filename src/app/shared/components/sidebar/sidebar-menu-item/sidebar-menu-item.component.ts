import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidebar-menu-item, [app-sidebar-menu-item]',
  templateUrl: './sidebar-menu-item.component.html',
  styleUrls: ['./sidebar-menu-item.component.scss']
})
export class SidebarMenuItemComponent implements OnInit {

  @Input() menus = [];
  @Output() closeBar = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  anchorClicked($event) {
    this.closeBar.emit($event);
  }
}
