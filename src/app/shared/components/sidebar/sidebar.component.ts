import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { jqxMenuComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxmenu';
import { AuthenticateService } from '../../services/authenticate.service';
import { TranslateService } from '@ngx-translate/core';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { ActivityMasterService } from '../../services/activity-master.service';
import { ToggleSidebarService, } from '../../services';
import { KeyboardLayoutService } from 'app/shared/services/keyboard-layout.service';


@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {

    @ViewChild('keyboardWindow') keyboardWindow: jqxWindowComponent;
    @ViewChild('jqxMenu1') jqxMenu1: jqxMenuComponent;
    @ViewChild('jqxMenu2') jqxMenu2: jqxMenuComponent;

    fy: string;
    uname: string; // User Name
    bname: string //Branch Name
    currentLang: string = 'NE';
    lnth: any
    tog: boolean = true;
    lay = 'romanized';
    userType: string = ' ';//User Type
    check: boolean = false;
    checkAdmin: boolean = false;

    constructor(
        private as: AuthenticateService,
        private acts: ActivityMasterService,
        private translate: TranslateService,
        public router: Router,
        private ts: ToggleSidebarService,
        private kl: KeyboardLayoutService,
    ) {
        let userData = JSON.parse(window.localStorage.getItem('pcUser'));
        this.uname = userData['userId'];
        let savedLang = localStorage.getItem('pcLang');
        this.currentLang = savedLang.toUpperCase();
        window.localStorage.removeItem('ActivityDatas');
        if (userData['userType']) {
            this.userType = userData['userType'];
        }
    }
    ngOnInit(): void {



        this.acts.remIndex({}).subscribe((res) => {
            if (res && res[0]) {
                this.lnth = res.length;
            }
        }, (error) => {
            console.info(error);
        });
        this.ts.currentMessage.subscribe((data) => {
            if (data == "off") {
                this.check = false;
            }
            else {
                this.check = true;
            }
        });
    }
    ngAfterViewInit(): void {
        if (this.userType.toLowerCase() == 'admin' || this.userType.toLowerCase() == 'super admin') {
            this.jqxMenu1.setItemOpenDirection('userInfo', 'left', 'down');
            this.checkAdmin = true;
        }
        if (this.userType.toLowerCase() != 'admin' && this.userType.toLowerCase() != 'super admin') {
            this.jqxMenu2.setItemOpenDirection('userInfo', 'left', 'down');
        }
        // this.jqxMenu.setItemOpenDirection('UserMaster', 'left', 'down');
    }
    menuClicked($event) {
        let itemVal = $event.args['innerText'];
        if (itemVal == 'Nepali' || itemVal == 'नेपाली') {
            this.translate.use('ne');
            this.currentLang = 'NE';
            localStorage.setItem('pcLang', 'ne');
            window.location.reload();
        }
        if (itemVal == 'English' || itemVal == 'अंग्रेजी') {
            this.translate.use('en');
            this.currentLang = 'EN';
            localStorage.setItem('pcLang', 'en');
            window.location.reload();
        }

    }

    isActive = false;
    showMenu = '';
    eventCalled() {
        this.isActive = !this.isActive;
    }
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
    logOut() {
        this.as.logout().subscribe((res) => {
            localStorage.removeItem('pcToken');
            localStorage.removeItem('pcUser');
            this.router.navigate(['/login']);
        }, (error) => {
            localStorage.removeItem('pcToken');
            localStorage.removeItem('pcUser');
            this.router.navigate(['/login']);
        });
    }

    showKeyboard() {
        this.keyboardWindow.open();
    }

    closeBar() {
        // console.log('hello')
        if (this.check) {
            this.ts.changeMessage("off");
            window.localStorage.removeItem('ActivityDatas')
            this.keyboardWindow.close();
        }
    }

    changeKeyboard() {

        if (this.tog) {
            this.kl.changeMessage("traditional");
            this.lay = "traditional";
            console.log('trad');

        } else {
            this.kl.changeMessage("romanized");
            this.lay = "romanized";
            console.log('rom');
        }
        this.tog = !this.tog;
    }
}
