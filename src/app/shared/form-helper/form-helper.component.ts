import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-form-helper, [app-form-helper]',
  templateUrl: './form-helper.component.html',
  styleUrls: ['./form-helper.component.scss'],
})
export class FormHelperComponent implements OnInit {


  @Input() formModel: any;
  @Input() formConfig: any;
  constructor(private _doms: DomSanitizer) {
  }

  bypassStyle(style: string) {
    return this._doms.bypassSecurityTrustStyle(style);
  }

  ngOnInit() {
    console.log(this.formModel);
  }


}
