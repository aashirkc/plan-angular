import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormHelperComponent } from './form-helper.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule
  ],
  declarations: [FormHelperComponent],
  exports: [
    FormHelperComponent
  ]
})
export class FormHelperModule { }
