import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutHelperComponent } from './layout-helper.component';
import { FormHelperModule } from '../form-helper/form-helper.module';

@NgModule({
  imports: [
    CommonModule,
    FormHelperModule
  ],
  declarations: [LayoutHelperComponent],
  exports: [
    LayoutHelperComponent
  ]
})
export class LayoutHelperModule { }
