import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutHelperComponent } from './layout-helper.component';

describe('LayoutHelperComponent', () => {
  let component: LayoutHelperComponent;
  let fixture: ComponentFixture<LayoutHelperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutHelperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutHelperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
