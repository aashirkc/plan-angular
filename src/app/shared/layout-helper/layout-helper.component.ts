import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-layout-helper',
  templateUrl: './layout-helper.component.html',
  styleUrls: ['./layout-helper.component.scss']
})
export class LayoutHelperComponent implements OnInit {

  @Input() templateString: string;
  @Input() formModel: any = {};
  formConfig: any = {};
  constructor() { }

  ngOnInit() {
    this.formConfig = this.parse(this.templateString);
    console.log(this.formModel);
  }

  parse(template: string) {
    try {
      template = template.replace(/\[\[[^{\]\]}]*\]\]/g, (x) => {
        const resp = x.substring(2, x.length - 2);
        const [name, type, cssClass] = resp.split('|');
        if (name in this.formModel) {
          return '^^^' + JSON.stringify({
            name, type, class: cssClass
          }) + '^^^';
        }
        return x;
      });
      const configArray = template.split('^^^');
      const arr = [];
      for (const data of configArray) {
        try {
          const p = JSON.parse(data);
          arr.push(p);
        } catch (err) {
          arr.push({
            type: 'span',
            value: data
          })
        }
      }
      console.log(arr)
      return arr;
    } catch (err) {
      console.log(err);
    }
  }
}
