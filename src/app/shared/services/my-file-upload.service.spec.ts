import { TestBed, inject } from '@angular/core/testing';

import { MyFileUploadService } from './my-file-upload.service';

describe('MyFileUploadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyFileUploadService]
    });
  });

  it('should be created', inject([MyFileUploadService], (service: MyFileUploadService) => {
    expect(service).toBeTruthy();
  }));
});
