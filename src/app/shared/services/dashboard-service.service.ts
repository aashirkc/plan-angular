import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// declare var google: any;

@Injectable()
export class DashboardServiceService {

  apiUrl: string;
  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl;

    // google.load("elements", "1", { packages: "transliteration" });

        //Load the Language API.
      // google.load("language", "1", { packages: "transliteration" });

  }

  getchartData(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/Dashboard', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  getGraph(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/Dashboard/NewGraph', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  getWardByWardCode(code,planYear) {
    let Params = new HttpParams();
    for (let key in planYear) {
      if (planYear.hasOwnProperty(key)) {
        Params = Params.append(key, planYear[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/Dashboard/' + code,{params:Params})
      .map(
        (response) => response,
        (error) => error
      );

  }

  allCosts(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/IssueApproval', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }

  getInclusionGroup(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/InclusionChart', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }

  getImplementationArea(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/ImplementationArea', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }


  setTranslation(idArray){

    // let func = (ids) => {
    //   var options = {
    //       sourceLanguage: 'en',
    //       // or google.elements.transliteration.LanguageCode.ENGLISH,
    //       destinationLanguage: ['ne'],
    //       // or [google.elements.transliteration.LanguageCode.HINDI],
    //       shortcutKey: 'ctrl+g',
    //       transliterationEnabled: true
    //   };
    //   var control = new google.elements.transliteration.TransliterationControl(options);
    //   // Enable transliteration in the textfields with the given ids.
    //   var ids = ids;
    //   control.makeTransliteratable(ids);
    //   // Show the transliteration control which can be used to toggle between
    //   // English and Nepali.
    //   control.showControl('translControl');
    // }

    // var callback = () => func(idArray);
    // google.setOnLoadCallback(callback);

  }

  getTranslation(input){
    // // console.log(input);
    //   //Call google.language.transliterate()
    //   let func2 = (val) => {
    //     console.log(val);
        // google.language.transliterate(["Namaste"], "en", "ne", function(result) {
        //   if (!result.error) {
        //     console.log(result);
        //     // var container = document.getElementById("transliteration");
        //     // if (result.transliterations && result.transliterations.length > 0 &&
        //     //     result.transliterations[0].transliteratedWords.length > 0) {
        //     //   container.innerHTML = result.transliterations[0].transliteratedWords[0];
        //     // }
        //   }
        // });
    //   }

    //   let callback2 = () => func2(input);

    //   google.setOnLoadCallback(callback2);


  }

}
