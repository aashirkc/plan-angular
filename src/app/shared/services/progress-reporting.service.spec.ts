/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProgressReportingService } from './progress-reporting.service';

describe('Service: ProgressReporting', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProgressReportingService]
    });
  });

  it('should ...', inject([ProgressReportingService], (service: ProgressReportingService) => {
    expect(service).toBeTruthy();
  }));
});
