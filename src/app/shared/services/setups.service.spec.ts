import { TestBed, inject } from '@angular/core/testing';

import { SetupsService } from './setups.service';

describe('SetupsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SetupsService]
    });
  });

  it('should be created', inject([SetupsService], (service: SetupsService) => {
    expect(service).toBeTruthy();
  }));
});
