/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MeasurementBookAndBillOfQuantityService } from './measurement-book-and-bill-of-quantity.service';

describe('Service: MeasurementBookAndBillOfQuantity', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeasurementBookAndBillOfQuantityService]
    });
  });

  it('should ...', inject([MeasurementBookAndBillOfQuantityService], (service: MeasurementBookAndBillOfQuantityService) => {
    expect(service).toBeTruthy();
  }));
});
