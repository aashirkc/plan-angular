import { TestBed, inject } from '@angular/core/testing';

import { ActivityDetailService } from './activity-detail.service';

describe('ActivityDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityDetailService]
    });
  });

  it('should be created', inject([ActivityDetailService], (service: ActivityDetailService) => {
    expect(service).toBeTruthy();
  }));
});
