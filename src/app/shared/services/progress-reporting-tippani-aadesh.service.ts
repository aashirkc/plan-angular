import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProgressReportingTippaniAadeshService {

    apiUrl: string;
    constructor(
        private http: HttpClient,
        @Inject('API_URL') apiUrl: string
    ) {
        this.apiUrl = apiUrl;
    }
    index(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/Mandate', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )

    }
    store(post) {
        return this.http.post(this.apiUrl + 'Report/Mandate', post)
            .map(
                (response) => response,
                (error) => error
            );

    }
    get(id,post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/Mandate/' + id, { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )

    }
    getOrganizationName(): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        return this.http.get(this.apiUrl + 'setup/OrganizationsMaster/1', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )

    }
    destroy(id) {
        return this.http.delete(this.apiUrl + 'Report/Mandate/' + id).map(
            (response: Response) => response,
            (error) => error)
    }
    update(id, post): Observable<any[]> {
        return this.http.put(this.apiUrl + 'Report/Mandate/' + id, post)
            .map(
                (response) => <any[]>response,
                (error) => error
            );
    }
    storeTippani(id,post) {
        return this.http.post(this.apiUrl + 'Progress/TippaniOrder/'+id, post)
            .map(
                (response) => response,
                (error) => error
            );

    }
    getTippani(id,post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Progress/TippaniOrder/' + id, { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )

    }

}
