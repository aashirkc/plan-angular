import { TestBed, inject } from '@angular/core/testing';

import { AllReportService } from './all-report.service';

describe('AllReportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AllReportService]
    });
  });

  it('should be created', inject([AllReportService], (service: AllReportService) => {
    expect(service).toBeTruthy();
  }));
});
