/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProgramTransferService } from './program-transfer.service';

describe('Service: ProgramTransfer', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProgramTransferService]
    });
  });

  it('should ...', inject([ProgramTransferService], (service: ProgramTransferService) => {
    expect(service).toBeTruthy();
  }));
});
