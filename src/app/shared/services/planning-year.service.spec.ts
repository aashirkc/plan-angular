/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PlanningYearService } from './planning-year.service';

describe('Service: PlanningYear', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlanningYearService]
    });
  });

  it('should ...', inject([PlanningYearService], (service: PlanningYearService) => {
    expect(service).toBeTruthy();
  }));
});
