import { TestBed, inject } from '@angular/core/testing';

import { TfmHttpInterceptorService } from './tfm-http-interceptor.service';

describe('TfmHttpInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TfmHttpInterceptorService]
    });
  });

  it('should be created', inject([TfmHttpInterceptorService], (service: TfmHttpInterceptorService) => {
    expect(service).toBeTruthy();
  }));
});
