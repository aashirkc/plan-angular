import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MyFileUploadService {

  apiUrl: string;
  apiUrl2: string;
  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl;
  }

  public uploadFile(fileToUpload: File, data, id) {
    //note: no HttpHeaders passed as 3d param to POST!
    //So no Content-Type constructed manually.
    //Angular 4.3/4.x does it automatically.
    const _formData = new FormData();
    _formData.append('bankVoucharAttachment', fileToUpload, fileToUpload.name);
    for (let key in data) {
      _formData.append(key, data[key]);
    }
    return this.http.put(this.apiUrl + 'Home/UploadBankVoucher/' + id, _formData);
  }
  public uploadCourseExamRegisterFile(fileToUpload: File, data) {
    //note: no HttpHeaders passed as 3d param to POST!
    //So no Content-Type constructed manually.
    //Angular 4.3/4.x does it automatically.
    const _formData = new FormData();
    _formData.append('bankVoucharAttachment', fileToUpload, fileToUpload.name);
    for (let key in data) {
      _formData.append(key, data[key]);
    }
    return this.http.post(this.apiUrl + 'Exam/StudentRegistration', _formData);
  }

  public uploadCourseRegisterFile(fileToUpload: File, data) {
    //note: no HttpHeaders passed as 3d param to POST!
    //So no Content-Type constructed manually.
    //Angular 4.3/4.x does it automatically.
    const _formData = new FormData();
    _formData.append('bankVoucharAttachment', fileToUpload, fileToUpload.name);
    for (let key in data) {
      _formData.append(key, data[key]);
    }
    return this.http.put(this.apiUrl + 'CourseRegistration/Course/' + data['courseId'], _formData);
  }

  public uploadStudentPhotoFile(fileToUpload: File, data, id) {
    //note: no HttpHeaders passed as 3d param to POST!
    //So no Content-Type constructed manually.
    //Angular 4.3/4.x does it automatically.
    const _formData = new FormData();
    _formData.append('studentPhoto', fileToUpload, fileToUpload.name);
    //   for (let key in data) {
    //   _formData.append(key, data[key]);
    // }
    return this.http.put(this.apiUrl + 'Home/UploadStudentPhoto/' + id, _formData);
  }
  public uploadFileAcademic(fileToUpload: File, type, year, levelCode, id) {
    //note: no HttpHeaders passed as 3d param to POST!
    //So no Content-Type constructed manually.
    //Angular 4.3/4.x does it automatically.
    const _formData = new FormData();
    _formData.append('image', fileToUpload, fileToUpload.name);
    return this.http.patch(this.apiUrl + 'Admission/StudentPreEducation?fileType=' + type + '&id=' + id + '&levelCode=' + levelCode + '&year=' + year, _formData);
  }
  public uploadFileAcademicPost(fileToUpload: File, type, year, levelCode, id) {
    //note: no HttpHeaders passed as 3d param to POST!
    //So no Content-Type constructed manually.
    //Angular 4.3/4.x does it automatically.
    const _formData = new FormData();
    _formData.append('image', fileToUpload, fileToUpload.name);
    return this.http.patch(this.apiUrl + 'Admission/StudentPreEducation?fileType=' + type + '&id=' + id + '&levelCode=' + levelCode + '&year=' + year, _formData);
  }

  public uploadFileCreate(fileStu: File, fileFath: File, fileMath: File, id) {
    //note: no HttpHeaders passed as 3d param to POST!
    //So no Content-Type constructed manually.
    //Angular 4.3/4.x does it automatically.
    const _formData = new FormData();
    _formData.append('studentPhoto', fileStu, fileStu.name);
    _formData.append('fatherPhoto', fileFath, fileFath.name);
    _formData.append('motherPhoto', fileMath, fileMath.name);
    return this.http.post(this.apiUrl + 'students/studentphotodetailsSave?studentId=' + id, _formData);
  }

}
