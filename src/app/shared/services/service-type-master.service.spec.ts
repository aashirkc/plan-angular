/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ServiceTypeMasterService } from './service-type-master.service';

describe('Service: ServiceTypeMaster', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServiceTypeMasterService]
    });
  });

  it('should ...', inject([ServiceTypeMasterService], (service: ServiceTypeMasterService) => {
    expect(service).toBeTruthy();
  }));
});
