import { TestBed, inject } from '@angular/core/testing';

import { DivisionServiceService } from './division-service.service';

describe('DivisionServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DivisionServiceService]
    });
  });

  it('should be created', inject([DivisionServiceService], (service: DivisionServiceService) => {
    expect(service).toBeTruthy();
  }));
});
