/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { KataSanchalanService } from './kata-sanchalan.service';

describe('Service: KataSanchalan', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KataSanchalanService]
    });
  });

  it('should ...', inject([KataSanchalanService], (service: KataSanchalanService) => {
    expect(service).toBeTruthy();
  }));
});
