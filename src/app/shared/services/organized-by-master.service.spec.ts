import { TestBed, inject } from '@angular/core/testing';

import { OrganizedByMasterService } from './organized-by-master.service';

describe('OrganizedByMasterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrganizedByMasterService]
    });
  });

  it('should be created', inject([OrganizedByMasterService], (service: OrganizedByMasterService) => {
    expect(service).toBeTruthy();
  }));
});
