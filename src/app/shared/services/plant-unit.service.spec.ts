/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PlantUnitService } from './plant-unit.service';

describe('Service: PlantUnit', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlantUnitService]
    });
  });

  it('should ...', inject([PlantUnitService], (service: PlantUnitService) => {
    expect(service).toBeTruthy();
  }));
});
