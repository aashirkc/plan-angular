import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProgressReportingService {
    apiUrl: string;
    constructor(private http: HttpClient, @Inject('API_URL') apiUrl: string) {
        this.apiUrl = apiUrl;
    }

    index(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'Activity/ActivityDetails', {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }

    indexList(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'Activity/ActivityDetails/0/0', {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }

    store(post) {
        return this.http
            .post(this.apiUrl + 'Activity/ActivityDetails', post)
            .map(response => response, error => error);
    }

    destroy(id) {
        return this.http
            .delete(this.apiUrl + 'Activity/ActivityDetails/' + id)
            .map((response: Response) => response, error => error);
    }

    show(searchString) {
        return this.http
            .get(this.apiUrl + 'Activity/ActivityDetails/' + searchString)
            .map((response: Response) => response, error => error);
    }
    showActivity(searchString) {
        return this.http
            .get(this.apiUrl + 'Activity/ActivityDetails/' + searchString)
            .map((response: Response) => response, error => error);
    }

    update(id, post): Observable<any[]> {
        return this.http
            .put(this.apiUrl + 'Activity/ActivityDetails/' + id, post)
            .map(response => <any[]>response, error => error);
    }
    updateActivity(id, post): Observable<any[]> {
        return this.http
            .patch(this.apiUrl + 'Activity/ActivityDetails/' + id, post)
            .map(response => <any[]>response, error => error);
    }

    showChild(id) {
        return this.http
            .get(this.apiUrl + 'Activity/ActivityDetails/' + id)
            .map((response: Response) => response, error => error);
    }
    showItem(id) {
        return this.http
            .get(this.apiUrl + 'Activity/ActivityDetails/' + id)
            .map((response: Response) => response, error => error);
    }

    addTechnicalApproval(data) {
        return this.http
            .post(this.apiUrl + 'TechnicalApproval/TechnicalApproval', data)
            .map((response: Response) => response, error => error);
    }

    getFinancialProgress(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'Progress/FinancialProgress', {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }
    getFinancialProgressSingle(id): Observable<any[]> {
        return this.http
            .get(this.apiUrl + 'Progress/FinancialProgress/' + id)
            .map(response => <any[]>response, error => error);
    }
    getPhysicalProgressSingle(id): Observable<any[]> {
        return this.http
            .get(this.apiUrl + 'Progress/ProgressService/' + id)
            .map(response => <any[]>response, error => error);
    }
    getSD(id): Observable<any[]> {
        return this.http
            .get(this.apiUrl + 'Report/ServiceDetails/' + id)
            .map(response => <any[]>response, error => error);
    }
    getWOA(id): Observable<any[]> {
        return this.http
            .get(this.apiUrl + 'Report/ProgressPhysicalAsset/' + id)
            .map(response => <any[]>response, error => error);
    }
    getCB(id): Observable<any[]> {
        return this.http
            .get(this.apiUrl + 'Report/ProgressCapacityBuilding/' + id)
            .map(response => <any[]>response, error => error);
    }
    getFinancialProgressProgramDetails(id): Observable<any[]> {
        return this.http
            .get(this.apiUrl + 'Progress/FinancialProgress/Bill/' + id)
            .map(response => <any[]>response, error => error);
    }

    financialProgressStore(post) {
        return this.http
            .post(this.apiUrl + 'Progress/FinancialProgress', post)
            .map(response => response, error => error);
    }
    getOrganization(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'setup/OrganizationsMaster', {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }
    financialProgressUpdate(post, id) {
        return this.http
            .post(this.apiUrl + 'Progress/FinancialProgress/' + id, post)
            .map(response => response, error => error);
    }
    getPhysicalProgress(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'Progress/ProgressCapacityBuilding', {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }
    storePhysicalProgress(post) {
        return this.http
            .post(this.apiUrl + 'Progress/ProgressCapacityBuilding', post)
            .map(response => response, error => error);
    }
    deletePhysicalProgress(post) {
        return this.http
            .delete(
                this.apiUrl +
                    'Progress/ProgressCapacityBuilding?imageName=' +
                    post
            )
            .map(response => response, error => error);
    }

    deleteFinancialProgress(imageUrl) {
        return this.http
            .delete(
                this.apiUrl + 'Progress/FinancialProgress?imageName=' + imageUrl
            )
            .map(response => response, error => error);
    }

    getServiceProgress(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'Progress/ProgressService', {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }
    storeServiceProgress(post) {
        return this.http
            .post(this.apiUrl + 'Progress/ProgressService', post)
            .map(response => response, error => error);
    }
    deleteServiceProgress(post) {
        return this.http
            .delete(this.apiUrl + 'Progress/ProgressService?imageName=' + post)
            .map(response => response, error => error);
    }
    getPhysicalAssetProgress(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'Progress/ProgressPhysicalAsset', {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }
    storePhysicalAssetProgress(post) {
        return this.http
            .post(this.apiUrl + 'Progress/ProgressPhysicalAsset', post)
            .map(response => response, error => error);
    }
    deletePhysicalAssetProgress(post) {
        return this.http
            .delete(
                this.apiUrl + 'Progress/ProgressPhysicalAsset?imageName=' + post
            )
            .map(response => response, error => error);
    }

    workCompletionCertificate(activityId, post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'Approve/WorkCompletion/' + activityId, {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }
    loadCb(post): Observable<any[]> {
        return this.http
            .get(this.apiUrl + 'Progress/ProgressCapacityBuilding/' + post)
            .map(response => <any[]>response, error => error);
    }
    loadSd(post): Observable<any[]> {
        return this.http
            .get(this.apiUrl + 'Progress/ProgressService/' + post)
            .map(response => <any[]>response, error => error);
    }
    loadWoa(post): Observable<any[]> {
        return this.http
            .get(this.apiUrl + 'Progress/ProgressPhysicalAsset/' + post)
            .map(response => <any[]>response, error => error);
    }
}
