import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ActivityDetailService {

  apiUrl: string;

  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl;
  }
  
  index(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/ActivityDetails', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  store(post) {
    return this.http.post(this.apiUrl + 'Activity/ActivityDetails', post)
      .map(
        (response) => response,
        (error) => error
      );

  }

  show(id) {
    return this.http.get(this.apiUrl + 'Activity/ActivityDetails/' + id)
      .map(
        (response) => response,
        (error) => error
      );
  }
  
  destroy(id) {
    return this.http.delete(this.apiUrl + 'Activity/ActivityDetails/' + id).map(
      (response: Response) => response,
      (error) => error)
  }
  
  update(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Activity/ActivityDetails/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  getYearlyPrograms() {
    return this.http.get(this.apiUrl + 'Activity/YearlyPrograms')
      .map(
        (response) => response,
        (error) => error
      );
  }

  getYearlyProgramsByFocusArea(id) {
    return this.http.get(this.apiUrl + 'Activity/YearlyPrograms/'+id)
      .map(
        (response) => response,
        (error) => error
      );
  }
  storeRules(id,post) {
    return this.http.post(this.apiUrl + 'Activity/Rule/'+ id, post)
      .map(
        (response) => response,
        (error) => error
      );

  }
}
