import { Injectable, Inject } from '@angular/core';

@Injectable()
export class NepaliNumberToWordService {
    check: any;
    constructor() {}

    convertNepaliDigitToEnglish(str: string) {
        if (str) {
            const mapping = {
                '०': '0',
                '१': '1',
                '२': '2',
                '३': '3',
                '४': '4',
                '५': '5',
                '६': '6',
                '७': '7',
                '८': '8',
                '९': '9'
            };
            let retStr = '';
            for (const ch of str) {
                if (ch in mapping) {
                    retStr += mapping[ch].toString();
                } else {
                    retStr += ch.toString();
                }
            }
            return retStr;
        }
        return '';
    }
    getNepaliWord(number) {
        var numberTemp,
            point,
            hundred,
            numberLen,
            i,
            str,
            follower,
            divider,
            counter,
            result,
            points,
            nepaliwords;

        number = number.toString().split('.');
        numberTemp = this.convertNepaliDigitToEnglish(number[0]);
        point = this.convertNepaliDigitToEnglish(number[1]);
        if (parseInt(point) === 0) {
            point = null;
        }
        hundred = '';
        numberLen = numberTemp.length;
        i = 0;
        str = new Array();
        nepaliwords = [
            'एक',
            'दुई',
            'तिन',
            'चार',
            'पाँच',
            'छ',
            'सात',
            'आठ',
            'नाै',
            'दस',
            'एघार',
            'बाह्र',
            'तेह्र',
            'चाैँध',
            'पन्ध्र',
            'शोह्र',
            'सत्र',
            'अठार',
            'उन्नाईस',
            'बिस',
            'एक्काइस',
            'बाइस',
            'तेइस',
            'चाेबिस',
            'पच्चीस',
            'छब्बीस',
            'सत्ताइस',
            'अठ्ठाइस',
            'उनन्तिस',
            'तिस',
            'एकतिस',
            'बत्तिस',
            'तेत्तिस',
            'चाैँतिस',
            'पैँतिस',
            'छत्तिस',
            'सैँतीस',
            'अठतीस',
            'उनन्चालीस',
            'चालिस',
            'एकचालीस',
            'बयालीस',
            'त्रियालीस',
            'चवालीस',
            'पैँतालीस',
            ' छयालीस',
            'सच्चालीस',
            'अठचालीस',
            'उनन्चास',
            'पचास',
            'एकाउन्न	',
            'बाउन्न',
            'त्रिपन्न',
            'चाैँउन्न',
            'पचपन्न',
            'छपन्न',
            'सन्ताउन्न',
            'अन्ठाउन्न',
            'उनन्साठी',
            'साठी',
            'एकसट्ठी',
            'बयसट्ठी',
            'त्रिसट्ठी',
            'चौंसट्ठी',
            'पैंसट्ठी',
            'छयसट्ठी',
            'सतसट्ठी',
            'अठसट्ठी',
            'उनन्सत्तरी',
            'सत्तरी',
            'एकहत्तर',
            'बहत्तर',
            'त्रिहत्तर',
            'चौहत्तर',
            'पचहत्तर',
            'छयहत्तर',
            'सतहत्तर',
            'अठहत्तर',
            'उनासी',
            'असी',
            'एकासी',
            'बयासी',
            'त्रियासी',
            'चौरासी',
            'पचासी',
            'छयासी',
            'सतासी',
            'अठासी',
            'उनान्नब्बे',
            'नब्बे',
            'एकान्नब्बे',
            'बयानब्बे',
            'त्रियान्नब्बे',
            'चौरान्नब्बे',
            'पन्चानब्बे',
            'छयान्नब्बे',
            'सन्तान्नब्बे',
            'अन्ठान्नब्बे',
            'उनान्सय'
        ];
        follower = new Array('', 'सय', 'हजार', 'लाख', 'करोड', 'अर्ब', 'खर्ब');
        while (i < numberLen) {
            divider = i == 2 ? 10 : 100;
            number = Math.floor(numberTemp % divider);
            numberTemp = Math.floor(numberTemp / divider);
            i += divider == 10 ? 1 : 2;
            console.log(number);
            if (number) {
                counter = str.length;
                str.push(
                    nepaliwords[number - 1] + ' ' + follower[counter] + ' '
                );
            } else {
                str.push('');
            }
        }
        str = str.reverse();
        result = str.join('');
        points = point ? ' ' + nepaliwords[point - 1] + ' पैसा' : '';
        return result + 'रुपैँया' + points + ' मात्र';
        // return (result + " " + points+" मात्र।");
    }
}
