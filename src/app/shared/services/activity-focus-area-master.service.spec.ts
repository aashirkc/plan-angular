/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ActivityFocusAreaMasterService } from './activity-focus-area-master.service';

describe('Service: ActivityFocusAreaMaster', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityFocusAreaMasterService]
    });
  });

  it('should ...', inject([ActivityFocusAreaMasterService], (service: ActivityFocusAreaMasterService) => {
    expect(service).toBeTruthy();
  }));
});
