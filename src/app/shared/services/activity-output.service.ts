import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest,HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ActivityOutputService {

  apiUrl: string;
  req:any;
  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl;
  }

  index(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/ActivityDetails', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  index1(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/ProgressDetails', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  indexAd(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/AdministrativeActivityDetails', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  indexTech(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/TechnicalActivityDetails', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  indexList(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/ActivityDetails/0/0', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  store(post) {
    return this.http.post(this.apiUrl + 'Activity/ActivityDetails', post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  destroy(id) {
    return this.http.delete(this.apiUrl + 'Activity/ActivityDetails/' + id).map(
      (response: Response) => response,
      (error) => error)
  }

  show(searchString,planYear) {

    let Params = new HttpParams();
    for (let key in planYear) {
      if (planYear.hasOwnProperty(key)) {
        Params = Params.append(key, planYear[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/ActivityDetails/' + searchString,{params:Params}).map(
      (response: Response) => response,
      (error) => error
    );
  }
  indexUnit(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Progress/ProgressPhysicalAsset/Unit', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  showActivity(searchString) {
    return this.http.get(this.apiUrl + 'Activity/ActivityDetails/' + searchString).map(
      (response: Response) => response,
      (error) => error
    );
  }
  showActivityPhysicalProgress(searchString) {
    return this.http.get(this.apiUrl + 'Physical/PhysicalData?activity=' + searchString).map(
      (response: Response) => response,
      (error) => error
    );
  }

  update(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Activity/ActivityDetails/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }
  updateActivity(id, post): Observable<any[]> {
    return this.http.patch(this.apiUrl + 'Activity/ActivityDetails/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  showChild(id) {
    return this.http.get(this.apiUrl + 'Activity/ActivityDetails/' + id).map(
      (response: Response) => response,
      (error) => error
    );
  }
  showItem(id) {
    return this.http.get(this.apiUrl + 'Activity/ActivityDetails/' + id).map(
      (response: Response) => response,
      (error) => error
    );
  }

  addTechnicalApproval(data) {
    return this.http.post(this.apiUrl + 'TechnicalApproval/TechnicalApproval', data).map(
      (response: Response) => response,
      (error) => error
    );
  }

  editTechnicalApproval(data) {
    return this.http.post(this.apiUrl + 'TechnicalApproval/TechnicalApproval', data).map(
      (response: Response) => response,
      (error) => error
    );
  }

  approveTechnical(data, id, reject = '') {
    if(reject){
        return this.http.put(this.apiUrl + 'TechnicalApproval/TechnicalApproval/' + id + '?userId=' + data + '&reject=' + reject, data).map(
            (response: Response) => response,
            (error) => error
        );
    }
    return this.http.put(this.apiUrl + 'Approve/TechnicalApproval/' + id + '?userId=' + data + '&reject=' + reject, data).map(
      (response: Response) => response,
      (error) => error
    );
  }

  approveAdministrative(data, id, reject = '') {
    if(reject){
        return this.http.put(this.apiUrl + 'Approval/AdministrativeApproval/' + id + '?userId=' + data + '&reject=' + reject, data).map(
            (response: Response) => response,
            (error) => error
          );
    }
    return this.http.put(this.apiUrl + 'Approve/AdministrativeApproval/' + id + '?userId=' + data + '&reject=' + reject, data).map(
      (response: Response) => response,
      (error) => error
    );
  }

  getTechnicalApproveById(id, post?): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'TechnicalApproval/TechnicalApproval/' + id, { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  deletImage(post?) {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.delete(this.apiUrl + 'TechnicalApproval/TechnicalApproval', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  getServiceDetail(serivceType, post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'ServiceDetails/' + serivceType, { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  storeExecutionStatus(post) {
    return this.http.post(this.apiUrl + 'Progress/ExecutionStatus', post)
      .map(
        (response) => response,
        (error) => error
      );
  }
  getExecutionStatus(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Progress/ExecutionStatus', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  getServiceType(): Observable<any[]> {
    return this.http.get(this.apiUrl + 'Setup/ServicesTypeMaster')
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  getOrganizedBy(): Observable<any[]> {
    return this.http.get(this.apiUrl + 'Setup/OrganizedByMaster')
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  getTrainingCategory(): Observable<any[]> {
    return this.http.get(this.apiUrl + 'Setup/TrainingCategoryMaster')
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  getAssetsCategory(): Observable<any[]> {
    return this.http.get(this.apiUrl + 'Setup/AssetsDetailsCategory/0')
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  getAssetsSubCategory(): Observable<any[]> {
    return this.http.get(this.apiUrl + 'Setup/AssetsDetailsSubCategory')
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  getAssetsType(): Observable<any[]> {
    return this.http.get(this.apiUrl + 'Setup/AssetsDetailsType')
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  approveBy(approveBy, id): Observable<any[]> {
    return this.http.post(this.apiUrl + 'Activity/ActivityDetails/' + id, approveBy)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  addWorkServiceType(data) {
    return this.http.post(this.apiUrl + 'Approval/TechnicalApproveWorkType', data).map(
      (response: Response) => response,
      (error) => error
    );
  }
  updateWorkServiceType(data, id) {
    return this.http.put(this.apiUrl + 'Approval/TechnicalApproveWorkType/' + id, data).map(
      (response: Response) => response,
      (error) => error
    );
  }
  loadTechnicalApprovalWork(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Approval/TechnicalApproveWorkType', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  destroyTechnicalApprovalWork(id) {
    return this.http.delete(this.apiUrl + 'Approval/TechnicalApproveWorkType/' + id).map(
      (response: Response) => response,
      (error) => error)
  }
  getInvestigationStage() {
    return this.http.get(this.apiUrl + 'Setup/BhoutikPragatiAnugamanCharanName').map(
      (response: Response) => response,
      (error) => error
    );
  }
  getActivity() {
    return this.http.get(this.apiUrl + 'Activity/PublicExaminationForm/Activities').map(
      (response: Response) => response,
      (error) => error
    );
  }
  saveActivity(post){
    return this.http.post(this.apiUrl + 'Activity/PublicExaminationForm',post).map(
      (response: Response) => response,
      (error) => error
    );
  }
  getPublicExaminationList(id){
    return this.http.get(this.apiUrl + 'Activity/PublicExaminationForm/'+id).map(
      (response: Response) => response,
      (error) => error
    );
  }

  uploadDocs(post){
    this.req = new HttpRequest('POST', this.apiUrl + 'Activity/PublicExaminationForm/Upload', post, {
      reportProgress: true
    });
  // this.http.request(this.req).subscribe(event => {
  //     // Via this API, you get access to the raw event stream.
  //     // Look for upload progress events.
  //     if (event.type === HttpEventType.UploadProgress) {
  //       // This is an upload progress event. Compute and show the % done:
  //       const percentDone = Math.round(100 * event.loaded / event.total);
  //       console.log(`File is ${percentDone}% uploaded.`);
  //     } else if (event instanceof HttpResponse) {
  //       console.log('File is completely uploaded!');
  //     }
  //   });
    return this.req;
    // return this.http.post(this.apiUrl + 'Activity/PublicExaminationForm/Upload',post).map(
    //   (response: Response) => response,
    //   (error) => error
    // );
  }
  getDocsList(id,post){
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/PublicExaminationForm/'+id,{ headers: myHeaders, params: Params }).map(
      (response: Response) => response,
      (error) => error
    );
  }
  deleteDocs(id,location){
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();

        Params = Params.append('id',id);
        Params=Params.append('location',location)

    return this.http.delete(this.apiUrl + 'Activity/PublicExaminationForm',{ headers: myHeaders, params: Params }).map(
      (response: Response) => response,
      (error) => error
    );
  }
  saveAllReport(post){
    return this.http.post(this.apiUrl + 'htmlToPdfConveter',post).map(
      (response: Response) => response,
      (error) => error
    );
  }
  getAllFiles(id){
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();

        Params = Params.append('activityId',id);

    return this.http.get(this.apiUrl + 'Activity/Report/AllFiles',{ headers: myHeaders, params: Params }).map(
      (response: Response) => response,
      (error) => error
    );
  }

  removeAsset(id: number, post:any){
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
        Params = Params.append('assetType',post.assetType);
        Params = Params.append('assetCategory',post.assetCategory);
        Params = Params.append('assetSubCatagory',post.assetSubCatagory);
    return this.http.delete(this.apiUrl + 'ServiceDetails/WorkOutputAsset/'+id,{ headers: myHeaders, params: Params }).map(
      (response: Response) => response,
      (error) => error
    );
  }

  updateActivityOutput(id: number, outputType: string, value: any){
    const urlMapping = {
      WOA: `ServiceDetails/WorkOutputAsset/${id}`,
      SD: `ServiceDetails/ServiceDetails/${id}`,
      CB: `ServiceDetails/CapacityBuilding/${id}`
    }
    const url = this.apiUrl + urlMapping[outputType];
    return this.http.patch(url, value).map(
      (response: Response) => response,
      (error) => error);
  }
}
