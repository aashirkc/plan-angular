import { Injectable, Inject } from '@angular/core';
declare var require: any;

@Injectable()
export class PrintingService {
  NotoFontBold: any;
  NotoFontRegular: any;
  distType: string;
  constructor(@Inject('DIST_TYPE') distType: string) {
    this.distType = distType;
    this.NotoFontBold = require('../../../assets/font/NotoSerifDevanagari-Bold.ttf');
    this.NotoFontRegular = require('../../../assets/font/NotoSerifDevanagari-Regular.ttf');
  }

  printContents(
    content: string,
    title: string = 'टिप्पणी आदेश',
    landscape: boolean = false,
    styles: string = ''
  ) {
    const popupWin = window.open(
      '',
      '_blank',
      'top=0,left=0,height=100%,width=auto'
    );
    popupWin.document.open();
    popupWin.document.write(`
  <html>
    <head>
      <title>${title}</title>
      <link
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous">
      <style type="text/css" media="print">
          @page{
            size: ${landscape ? 'landscape' : 'portriat'};
          }
      </style>
      <style>
      * {
        font-family: NotoSerif;
      }
      
      .fixedWidth {
        display: inline-block;
        width: 150px;
      }
      
      .showPrint {
        display: inline-block!important;
        font-size: 20px !important;
      }
      
      @font-face {
        font-family: preeti;
        src: url(../assets/css/Preeti.TTF);
        font-weight: 400;
        font-style: normal;
      }

      @font-face {
        font-family: 'NotoSerif';
        src: url(${this.NotoFontRegular});
      }
      @font-face {
        font-family: 'NotoSerif';
        src: url(${this.NotoFontBold});
        font-weight: bold;
      }
      
      .nepaliText {
        font-family: nepaliNormal;
        font-size: 18px !important;
      }
      
      .preeti {
        font-family: preeti;
        font-size: 18px;
      }
      
      p {
        font-size: 23px !important;
      }
      
      .txt {
        font-size: 23px !important;
        line-height: 1.5;
      }
      
      .univ-title {
        padding-top: 20px;
      }
      
      .exam-triplicate {
        background: #fff;
        margin-bottom: 20px;
      }
      
      .table {
        height: auto!important;
        position: relative;
        width: 100%;
        max-width: 100%;
        margin-top: 20px;
        margin-bottom: 1rem;
        font-size: smaller;
        border-collapse: collapse;
        background-color: transparent;
      }
      
      .col-md-4 {
        display: inline-block;
        width: 33%;
      }
      
      .univ-title p {
        margin: 0;
      }
      
      .table-bordered th,
      .table-bordered td {
        border: 1px solid #eceeef;
        padding: 3px;
      }
      
      .logo-top {
        padding-top: 25px;
      }
      
      .printStyle {
        padding: 0!important;
      }
      
      #showDiv,
      #showPrint,
      #divShow,
      #showPrint1,
      #showPrint2,
      .print_show,
      #show,
      .print_show,
      .show_div,
      #showPrint {
        display: inline-block!important;
      }
      
      #hideDiv,
      #hideContent,
      #hidePrint,
      .hidePrint,
      #divHide,
      #printHide1,
      #printHide2,
      .form-style-4,
      .print_hide,
      #hide,
      .form-style-4,
      .print_hide,
      #hidePrint,
      .form_change,
      .hide_div,
      #hideDiv {
        display: none!important;
      }
      
      #space{
        display:none;
      }

      .table-bordered {
        border: 1px solid #eceeef;
      }
      
      @font-face {
        font-family: nepaliNormal;
        src: url(assets/css/Kantipur.TTF);
        font-weight: 400;
        font-style: normal;
      }
      
      .table th,
      .table td {
        vertical-align: top;
        border-top: 1px solid #eceeef;
        text-align: left;
        padding: .55rem;
      }
      
      input,
      .displayHiddenPrint,
      .displayHiddenPrint,
      input,
      .hideElement,
      .last-td {
        display: none;
      }

      .bordered-table tr td{
        border: 1px solid black;
        font-size: 16px;
      }
    .bordered-table{
      width: 100%;
      border-collapse: collapse;
    }
    .bordered-table td{
      padding: 10px;
    }
      
      .p, //........Customized style.......
      .p {
        margin-bottom: 5px;
      }
      
      @media print {
        .bar {
          background-color: #000!important;
          -webkit-print-color-adjust: exact;
        }
        .bar-text {
          color: #FFF;
          -webkit-print-color-adjust: exact;
        }
      }


      @media print {
        #printContent{
          max-height: calc(100% - 50px);
        }
        footer.custom_footer {
            position: fixed;
            bottom: 0;
            width: 100%;
            background: #fff;
        }
        .page-footer-space{
          height: ${this.distType ? '0px' : '100px'}
        }
      }

      @media print {
        table{
          width: 100%;
        }
        thead {display: table-header-group;}
        tfoot {display: table-footer-group;}
     }


      </style>
    </head>
<body onload="window.print();window.close()">
<style>
${styles}
</style>
<table>

    <thead>
      <tr>
        <td>
          <div class="page-header-space"></div>
        </td>
      </tr>
    </thead>

    <tbody>
      <tr>
        <td>
          ${content}
        </td>
      </tr>
    </tbody>

    <tfoot>
      <tr>
        <td>
          <!--place holder for the fixed-position footer-->
          <div class="page-footer-space"></div>
        </td>
      </tr>
    </tfoot>

  </table>
</body>
  </html>`);
    popupWin.document.close();
  }
}
