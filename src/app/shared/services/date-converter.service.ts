import { Injectable } from '@angular/core';

@Injectable()
export class DateConverterService {

  constructor() { }

  /**
   * Get Today BS Date
   */
  public getToday(){
    let currentDate = new Date();
    let today = currentDate.getFullYear()+'-'+(currentDate.getMonth()+1)+'-'+currentDate.getDate();
    return this.ad2Bs(today)
  }

  /**
   * ad_date = '1990-02-20' if 20-02-1990 then pass Format Below
   * format 'd-m-y' = d:Day , m:Month, y:Year
   * @param ad_date 
   * @param format 
   */
  public ad2Bs(ad_date:string, format:string = null) {
    let convertedDate: any;
    let arrayDate = this._array_date(ad_date);
    if (format && format == 'd-m-y') {
      convertedDate = this.eng_to_nep(arrayDate[2], arrayDate[1], arrayDate[0]);
      convertedDate['fulldate'] = this._get_full_date(convertedDate, format);
    } else {
      convertedDate = this.eng_to_nep(arrayDate[0], arrayDate[1], arrayDate[2]);
      convertedDate['fulldate'] = this._get_full_date(convertedDate);
    }
    return convertedDate;
  }

  /**
   * bs_date = '2075-11-08' if 08-11-2075 then pass Format Below
   * format 'd-m-y' = d:Day , m:Month, y:Year
   * @param bs_date 
   * @param format 
   */
  public bs2Ad(bs_date:string, format:string = null) {
    let convertedDate: any = [];
    let arrayDate = this._array_date(bs_date);
    if (format && format == 'd-m-y') {
      console.log(arrayDate);
      convertedDate = this.nep_to_eng(arrayDate[2], arrayDate[1], arrayDate[0]);
      convertedDate['fulldate'] = this._get_full_date(convertedDate, format);
    } else {
      convertedDate = this.nep_to_eng(arrayDate[0], arrayDate[1], arrayDate[2]);
      convertedDate['fulldate'] = this._get_full_date(convertedDate);
    }
    return convertedDate;
  }

  private _get_full_date(dateObj, format = null){
    let fullDate:any;
    if(format && format == 'd-m-y'){
      fullDate = dateObj['day'] + '-' + dateObj['month'] + '-'+ dateObj['year'];
    }else{
      fullDate = dateObj['year'] + '-' + dateObj['month'] + '-'+ dateObj['day'];
    }
    return fullDate;
  }

  private _array_date(date) {
    let dateArray = date.split('-');
    for (let i = 0; i < dateArray.length; i++) {
      dateArray[i] = Number(dateArray[i]);
      if (dateArray[i].length == 2 && dateArray[i].length < 10) {
        dateArray[i] = this._get_day(dateArray[i]);
      }
    }
    return dateArray;
  }

  private _bs = new Array(
    new Array(2000, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31),
    new Array(2001, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2002, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30),
    new Array(2003, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2004, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31),
    new Array(2005, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2006, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30),
    new Array(2007, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2008, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31),
    new Array(2009, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2010, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30),
    new Array(2011, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2012, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30),
    new Array(2013, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2014, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30),
    new Array(2015, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2016, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30),
    new Array(2017, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2018, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30),
    new Array(2019, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31),
    new Array(2020, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2021, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2022, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30),
    new Array(2023, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31),
    new Array(2024, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2025, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2026, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2027, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31),
    new Array(2028, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2029, 31, 31, 32, 31, 32, 30, 30, 29, 30, 29, 30, 30),
    new Array(2030, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2031, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31),
    new Array(2032, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2033, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30),
    new Array(2034, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2035, 30, 32, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31),
    new Array(2036, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2037, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30),
    new Array(2038, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2039, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30),
    new Array(2040, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2041, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30),
    new Array(2042, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2043, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30),
    new Array(2044, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2045, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30),
    new Array(2046, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2047, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2048, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2049, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30),
    new Array(2050, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31),
    new Array(2051, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2052, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2053, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30),
    new Array(2054, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31),
    new Array(2055, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2056, 31, 31, 32, 31, 32, 30, 30, 29, 30, 29, 30, 30),
    new Array(2057, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2058, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31),
    new Array(2059, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2060, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30),
    new Array(2061, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2062, 30, 32, 31, 32, 31, 31, 29, 30, 29, 30, 29, 31),
    new Array(2063, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2064, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30),
    new Array(2065, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2066, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31),
    new Array(2067, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2068, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30),
    new Array(2069, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2070, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30),
    new Array(2071, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2072, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30),
    new Array(2073, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31),
    new Array(2074, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2075, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2076, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30),
    new Array(2077, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31),
    new Array(2078, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2079, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30),
    new Array(2080, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30),
    new Array(2081, 31, 31, 32, 32, 31, 30, 30, 30, 29, 30, 30, 30),
    new Array(2082, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30),
    new Array(2083, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30),
    new Array(2084, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30),
    new Array(2085, 31, 32, 31, 32, 30, 31, 30, 30, 29, 30, 30, 30),
    new Array(2086, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30),
    new Array(2087, 31, 31, 32, 31, 31, 31, 30, 30, 29, 30, 30, 30),
    new Array(2088, 30, 31, 32, 32, 30, 31, 30, 30, 29, 30, 30, 30),
    new Array(2089, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30),
    new Array(2090, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30)
  );

  // year, month, date, day, nmonth, num_day new Array('', '', '', '', '', '');
  private _nep_date: any = [];
  // year, month, date, day, emonth, num_day new Array('', '', '', '', '', '');
  private _eng_date: any = [];

  public debug_info = "";


  private _get_day_of_week($day) {
    switch ($day) {
      case 1:
        $day = "Sunday";
        break;
      case 2:
        $day = "Monday";
        break;
      case 3:
        $day = "Tuesday";
        break;
      case 4:
        $day = "Wednesday";
        break;
      case 5:
        $day = "Thursday";
        break;
      case 6:
        $day = "Friday";
        break;
      case 7:
        $day = "Saturday";
        break;
    }
    return $day;
  };

  /**
   * Return english month name
   *
   * @param int $m
   * @return string
   */
  private _get_english_month($m) {
    let $eMonth: any = false;
    switch ($m) {
      case 1:
        $eMonth = "January";
        break;
      case 2:
        $eMonth = "February";
        break;
      case 3:
        $eMonth = "March";
        break;
      case 4:
        $eMonth = "April";
        break;
      case 5:
        $eMonth = "May";
        break;
      case 6:
        $eMonth = "June";
        break;
      case 7:
        $eMonth = "July";
        break;
      case 8:
        $eMonth = "August";
        break;
      case 9:
        $eMonth = "September";
        break;
      case 10:
        $eMonth = "October";
        break;
      case 11:
        $eMonth = "November";
        break;
      case 12:
        $eMonth = "December";
    }
    return $eMonth;
  }


  /**
   * Return nepali month name
   *
   * @param int $m
   * @return string
   */
  private _get_nepali_month($m) {
    let $n_month: any = false;
    switch ($m) {
      case 1:
        $n_month = "Baishak";
        break;
      case 2:
        $n_month = "Jestha";
        break;
      case 3:
        $n_month = "Ashad";
        break;
      case 4:
        $n_month = "Shrawn";
        break;
      case 5:
        $n_month = "Bhadra";
        break;
      case 6:
        $n_month = "Ashwin";
        break;
      case 7:
        $n_month = "kartik";
        break;
      case 8:
        $n_month = "Mangshir";
        break;
      case 9:
        $n_month = "Poush";
        break;
      case 10:
        $n_month = "Magh";
        break;
      case 11:
        $n_month = "Falgun";
        break;
      case 12:
        $n_month = "Chaitra";
        break;
    }
    return $n_month;
  }

  /**
 * Check if date range is in english
 *
 * @param int $yy
 * @param int $mm
 * @param int $dd
 * @return bool
 */
  private _is_in_range_eng($yy, $mm, $dd) {
    if ($yy < 1944 || $yy > 2033) {
      return 'Supported only between 1944-2022';
    }
    if ($mm < 1 || $mm > 12) {
      return 'Error! month value can be between 1-12 only';
    }
    if ($dd < 1 || $dd > 31) {
      return 'Error! day value can be between 1-31 only';
    }
    return true;
  }



  /**
   * Check if date is with in nepali data range
   *
   * @param int $yy
   * @param int $mm
   * @param int $dd
   * @return bool
   */
  private _is_in_range_nep($yy, $mm, $dd) {
    if ($yy < 2000 || $yy > 2089) {
      return 'Supported only between 2000-2089';
    }
    if ($mm < 1 || $mm > 12) {
      return 'Error! month value can be between 1-12 only';
    }
    if ($dd < 1 || $dd > 32) {
      return 'Error! day value can be between 1-31 only';
    }
    return true;
  }

  /**
* Calculates wheather english year is leap year or not
*
* @param int $year
* @return bool
*/
  public is_leap_year($year) {
    let $a: any = $year;
    if ($a % 100 == 0) {
      if ($a % 400 == 0) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      if ($a % 4 == 0) {
        return true;
      }
      else {
        return false;
      }
    }
  }


  /**
   * currently can only calculate the date between AD 1944-2033...
   *
   * @param int $yy
   * @param int $mm
   * @param int $dd
   * @return array
   */
  public eng_to_nep($yy, $mm, $dd) {
    // Check for date range
    let $chk = this._is_in_range_eng($yy, $mm, $dd);
    if ($chk !== true) {
      // die($chk);
      console.log('DIE')
    }
    else {
      // Month data.
      let $month = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

      // Month for leap year
      let $lmonth = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
      let $def_eyy = 1944;	// initial english date.
      let $def_nyy = 2000;
      let $def_nmm = 9;
      let $def_ndd = 17 - 1;	// inital nepali date.
      let $total_eDays = 0;
      let $total_nDays = 0;
      let $a = 0;
      let $day = 7 - 1;
      let $m = 0;
      let $y = 0;
      let $i = 0;
      let $j = 0;
      let $numDay = 0;
      // Count total no. of days in-terms year
      for (let $i = 0; $i < ($yy - $def_eyy); $i++) //total days for month calculation...(english)
      {
        if (this.is_leap_year($def_eyy + $i) === true) {
          for (let $j = 0; $j < 12; $j++) {
            $total_eDays += $lmonth[$j];
          }
        }
        else {
          for (let $j = 0; $j < 12; $j++) {
            $total_eDays += $month[$j];
          }
        }
      }
      // Count total no. of days in-terms of month
      for (let $i = 0; $i < ($mm - 1); $i++) {
        if (this.is_leap_year($yy) === true) {
          $total_eDays += $lmonth[$i];
        }
        else {
          $total_eDays += $month[$i];
        }
      }

      // Count total no. of days in-terms of date
      $total_eDays += $dd;
      $i = 0;
      $j = $def_nmm;
      $total_nDays = $def_ndd;
      $m = $def_nmm;
      $y = $def_nyy;
      // Count nepali date from array
      while ($total_eDays != 0) {
        $a = this._bs[$i][$j];
        $total_nDays++;		//count the days
        $day++;				//count the days interms of 7 days
        if ($total_nDays > $a) {
          $m++;
          $total_nDays = 1;
          $j++;
        }

        if ($day > 7) {
          $day = 1;
        }

        if ($m > 12) {
          $y++;
          $m = 1;
        }

        if ($j > 12) {
          $j = 1;
          $i++;
        }

        $total_eDays--;
      }
      $numDay = $day;
      this._nep_date['year'] = $y;
      this._nep_date['month'] = $m && $m < 10 && '0'+$m || $m;
      this._nep_date['day'] = $total_nDays && $total_nDays < 10 && '0'+$total_nDays || $total_nDays;
      this._nep_date['weekday'] = this._get_day_of_week($day);
      this._nep_date['nmonth'] = this._get_nepali_month($m);
      this._nep_date['num_day'] = $numDay;
      return this._nep_date;
    }
  }


  /**
   * Currently can only calculate the date between BS 2000-2089
   *
   * @param int $yy
   * @param int $mm
   * @param int $dd
   * @return array
   */
  public nep_to_eng($yy, $mm, $dd) {
    let $def_eyy = 1943;
    let $def_emm = 4;
    let $def_edd = 14 - 1;	// initial english date.
    let $def_nyy = 2000;
    let $def_nmm = 1;
    let $def_ndd = 1;		// iniital equivalent nepali date.
    let $total_eDays = 0;
    let $total_nDays = 0;
    let $a = 0;
    let $day = 4 - 1;
    let $m = 0;
    let $y = 0;
    let $i = 0;
    let $k = 0;
    let $numDay = 0;
    let $month = new Array(
      0,
      31,
      28,
      31,
      30,
      31,
      30,
      31,
      31,
      30,
      31,
      30,
      31
    );
    let $lmonth = new Array(
      0,
      31,
      29,
      31,
      30,
      31,
      30,
      31,
      31,
      30,
      31,
      30,
      31
    );
    // Check for date range
    let $chk = this._is_in_range_nep($yy, $mm, $dd);
    if ($chk !== true) {
      // die($chk);
      console.log('die')
    }
    else {
      // Count total days in-terms of year
      for (let $i = 0; $i < ($yy - $def_nyy); $i++) {
        for (let $j = 1; $j <= 12; $j++) {
          $total_nDays += this._bs[$k][$j];
        }
        $k++;
      }
      // Count total days in-terms of month
      for (let $j = 1; $j < $mm; $j++) {
        $total_nDays += this._bs[$k][$j];
      }
      // Count total days in-terms of dat
      $total_nDays += $dd;
      // Calculation of equivalent english date...
      $total_eDays = $def_edd;
      $m = $def_emm;
      $y = $def_eyy;
      while ($total_nDays != 0) {
        if (this.is_leap_year($y)) {
          $a = $lmonth[$m];
        }
        else {
          $a = $month[$m];
        }
        $total_eDays++;
        $day++;
        if ($total_eDays > $a) {
          $m++;
          $total_eDays = 1;
          if ($m > 12) {
            $y++;
            $m = 1;
          }
        }
        if ($day > 7) {
          $day = 1;
        }
        $total_nDays--;
      }

      $numDay = $day;
      this._eng_date['year'] = $y;
      this._eng_date['month'] = $m && $m < 10 && '0'+$m || $m;
      this._eng_date['day'] = $total_eDays && $total_eDays < 10 && '0'+$total_eDays || $total_eDays;
      this._eng_date['weekday'] = this._get_day_of_week($day);
      this._eng_date['nmonth'] = this._get_english_month($m);
      this._eng_date['num_day'] = $numDay;
      return this._eng_date;
    }
  };

  private _get_day($d) {
    let $day: any;
    switch ($d) {
      case '01':
        $day = 1;
        break;
      case '02':
        $day = 2;
        break;
      case '03':
        $day = 3;
        break;
      case '04':
        $day = 4;
        break;
      case '05':
        $day = 5;
        break;
      case '06':
        $day = 6;
        break;
      case '07':
        $day = 7;
        break;
      case '08':
        $day = 8;
        break;
      case '09':
        $day = 9;
        break;
    }
    return $day;
  }

}
