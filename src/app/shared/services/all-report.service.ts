import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ResponseContentType } from '@angular/http/src/enums';

@Injectable()
export class AllReportService {
  apiUrl: string;
  apiUrl2: string;

  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl;

  }

  getActivityDetailLedger(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/Ledger', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  
  getActivityDetails(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/ActivityLedger', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  getKaryabidhi(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'report/karyabidhi', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getAnusuchi17(): Observable<any[]> {
    return this.http.get(this.apiUrl + 'report/anusuchi17')
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  // getAnusuchi1(): Observable<any[]> {
  //   return this.http.get(this.apiUrl + 'report/anusuchi1')
  //     .map(
  //       (response) => <any[]>response,
  //       (error) => error
  //     )
  // }

  getAnusuchi1(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'report/anusuchi1', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  getAllAnusuchi1(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Report/Anusuchi', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  getAnusuchi172(): Observable<any[]> {
    return this.http.get(this.apiUrl + 'report/anusuchi172')
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getPlanningProgressReport(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Report/WardLevelYojanaProgressReport', { headers: myHeaders, params: Params })
      .map(
        (response) => {
            return <any[]>response
        },
        (error) => error
      )
  }

  getActivityReport(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/Report', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getConsumerCommitteeReport(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/Report/ConsumerCommitte', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getKaryaDeshReport(id, post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Report/Mandate/' + id, { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getKaryaDeshReport1(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Report/Mandate', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  putKaryaDeshReport(id,post){
    return this.http.put(this.apiUrl + 'Report/Mandate/'+id, post)
    .map(
      (response) => response,
      (error) => error
    );
  }
  postAnusuchi1(id,post){
    return this.http.put(this.apiUrl + 'Report/Anusuchi1/'+id, post)
    .map(
      (response) => response,
      (error) => error
    );
  }

  postAnusuchi2(id,post){
    return this.http.patch(this.apiUrl + 'Report/Anusuchi2/'+id, post)
    .map(
      (response) => response,
      (error) => error
    );
  }


  patchKaryaDeshReport(id,post){
    return this.http.patch(this.apiUrl + 'Report/Mandate/'+id, post)
    .map(
      (response) => response,
      (error) => error
    );
  }

   patchSharta(id,post){
    return this.http.patch(this.apiUrl + 'Report/Samjhauta/'+id, post)
    .map(
      (response) => response,
      (error) => error
    );
  }

  getOrg(): Observable<any[]> {
    return this.http.get(this.apiUrl + 'setup/OrganizationsMaster')
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  getPhysicalDetails(id, post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'List/ProgressService/' + id, { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getAdministrative(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Approval/AdministrativeApproval', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  getanusuchi17Report(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Approval/AdministrativeApproval', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  getanusuchi172Report(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Approval/AdministrativeApproval', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getanusuchi6Report(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'report/anusuchi6', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getanusuchi6ReportData(id){
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');


    return this.http.get(this.apiUrl + 'Approval/Anusuchi6/'+id, )
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  sendanusuchi6Report(post){
    return this.http.post(this.apiUrl + 'Approval/Anusuchi6/'+post['activity'], {data:post})
    .map(
      (response) => response,
      (error) => error
    );
  }


  getanusuchi3Report(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'report/anusuchi3', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getanusuchi3ReportData(id){
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');


    return this.http.get(this.apiUrl + 'Approval/Anusuchi3/'+id, )
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  sendanusuchi3Report(post){
    return this.http.post(this.apiUrl + 'Approval/Anusuchi3/'+post['activity'], {data:post})
    .map(
      (response) => response,
      (error) => error
    );
  }

  getanusuchi4Report(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'report/anusuchi4', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getanusuchi4ReportData(id){
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');


    return this.http.get(this.apiUrl + 'Approval/Anusuchi4/'+id, )
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  sendanusuchi4Report(post){
    return this.http.post(this.apiUrl + 'Approval/Anusuchi4/', post)
    .map(
      (response) => response,
      (error) => error
    );
  }



  getanusuchi5Report(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'report/anusuchi5', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  sendanusuchi5Report(post){
    return this.http.post(this.apiUrl + 'Approval/Anusuchi5/', post)
    .map(
      (response) => response,
      (error) => error
    );
  }

  storeAnugaman(id,post) {
    return this.http.put(this.apiUrl + 'Progress/AnugamanPertibadan/'+ id, post)
    .map(
        (response) => response,
        (error) => error
    );

}
getAnugaman(id,post): Observable<any[]> {
  let myHeaders = new HttpHeaders();
  myHeaders.append('Content-Type', 'application/json');
  let Params = new HttpParams();
  for (let key in post) {
    if (post.hasOwnProperty(key)) {
      Params = Params.append(key, post[key]);
    }
  }
  return this.http.get(this.apiUrl + 'Progress/AnugamanPertibadan/'+id, { headers: myHeaders, params: Params })
    .map(
      (response) => <any[]>response,
      (error) => error
    )
}
}
