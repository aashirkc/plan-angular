/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AgencyTypeService } from './agency-type.service';

describe('Service: AgencyType', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AgencyTypeService]
    });
  });

  it('should ...', inject([AgencyTypeService], (service: AgencyTypeService) => {
    expect(service).toBeTruthy();
  }));
});
