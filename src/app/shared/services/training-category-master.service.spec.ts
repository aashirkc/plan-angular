/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TrainingCategoryMasterService } from './training-category-master.service';

describe('Service: TrainingCategoryMaster', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrainingCategoryMasterService]
    });
  });

  it('should ...', inject([TrainingCategoryMasterService], (service: TrainingCategoryMasterService) => {
    expect(service).toBeTruthy();
  }));
});
