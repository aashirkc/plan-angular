/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WorkOutputAssetService } from './work-output-asset.service';

describe('Service: WorkOutputAsset', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkOutputAssetService]
    });
  });

  it('should ...', inject([WorkOutputAssetService], (service: WorkOutputAssetService) => {
    expect(service).toBeTruthy();
  }));
});
