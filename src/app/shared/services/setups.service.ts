import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class SetupsService {

  apiUrl: string;
  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl;
  }

  // Service Type Master
  serviceTypeIndex(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Setup/ServicesTypeMaster', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  serviceTypeStore(post) {
    return this.http.post(this.apiUrl + 'Setup/ServicesTypeMaster', post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  serviceTypeDestroy(id) {
    return this.http.delete(this.apiUrl + 'Setup/ServicesTypeMaster' + id).map(
      (response: Response) => response,
      (error) => error)
  }

  serviceTypeShow(searchString) {
    return this.http.get(this.apiUrl + 'Setup/ServicesTypeMaster' + searchString).map(
      (response: Response) => response,
      (error) => error
    );
  }

  serviceTypeUpdate(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Setup/ServicesTypeMaster' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  // Organized BY Master
  organizedByIndex(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Setup/OrganizedByMaster', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  organizedByStore(post) {
    return this.http.post(this.apiUrl + 'Setup/OrganizedByMaster', post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  organizedByDestroy(id) {
    return this.http.delete(this.apiUrl + 'Setup/OrganizedByMaster' + id).map(
      (response: Response) => response,
      (error) => error)
  }

  organizedByShow(searchString) {
    return this.http.get(this.apiUrl + 'Setup/OrganizedByMaster' + searchString).map(
      (response: Response) => response,
      (error) => error
    );
  }

  organizedByUpdate(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Setup/OrganizedByMaster' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  // Training Category Master
  trainingCategoryIndex(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Setup/TrainingCategoryMaster', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  trainingCategoryStore(post) {
    return this.http.post(this.apiUrl + 'Setup/TrainingCategoryMaster', post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  trainingCategoryDestroy(id) {
    return this.http.delete(this.apiUrl + 'Setup/TrainingCategoryMaster' + id).map(
      (response: Response) => response,
      (error) => error)
  }

  trainingCategoryShow(searchString) {
    return this.http.get(this.apiUrl + 'Setup/TrainingCategoryMaster' + searchString).map(
      (response: Response) => response,
      (error) => error
    );
  }

  trainingCategoryUpdate(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Setup/TrainingCategoryMaster' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  // Assets Category Master
  AssetsCategoryIndex(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Assets/AssetsDetailsCategory', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  AssetsCategoryStore(post) {
    return this.http.post(this.apiUrl + 'Assets/AssetsDetailsCategory', post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  AssetsCategoryDestroy(id) {
    return this.http.delete(this.apiUrl + 'Assets/AssetsDetailsCategory' + id).map(
      (response: Response) => response,
      (error) => error)
  }

  AssetsCategoryShow(searchString) {
    return this.http.get(this.apiUrl + 'Assets/AssetsDetailsCategory' + searchString).map(
      (response: Response) => response,
      (error) => error
    );
  }

  AssetsCategoryUpdate(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Assets/AssetsDetailsCategory' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  // Assets Sub Category Master
  AssetsSubCategoryIndex(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Assets/AssetsDetailsSubCategory', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  AssetsSubCategoryStore(post) {
    return this.http.post(this.apiUrl + 'Assets/AssetsDetailsSubCategory', post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  AssetsSubCategoryDestroy(id) {
    return this.http.delete(this.apiUrl + 'Assets/AssetsDetailsSubCategory' + id).map(
      (response: Response) => response,
      (error) => error)
  }

  AssetsSubCategoryShow(searchString) {
    return this.http.get(this.apiUrl + 'Assets/AssetsDetailsSubCategory' + searchString).map(
      (response: Response) => response,
      (error) => error
    );
  }

  AssetsSubCategoryUpdate(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Assets/AssetsDetailsSubCategory' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  // Assets Sub Category Master
  AssetsTypeIndex(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Assets/AssetsDetailsType', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  AssetsTypeStore(post) {
    return this.http.post(this.apiUrl + 'Assets/AssetsDetailsType', post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  AssetsTypeDestroy(id) {
    return this.http.delete(this.apiUrl + 'Assets/AssetsDetailsType' + id).map(
      (response: Response) => response,
      (error) => error)
  }

  AssetsTypeShow(searchString) {
    return this.http.get(this.apiUrl + 'Assets/AssetsDetailsType' + searchString).map(
      (response: Response) => response,
      (error) => error
    );
  }

  AssetsTypeUpdate(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Assets/AssetsDetailsType' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }


  consumerCommitteeDetailsGet(wardId = null) {
    const params = wardId ? `?wardId=${wardId}`: ``
    return this.http.get(this.apiUrl + 'setup/ConsumerCommiteeDetails' + params)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }
  consumerCommitteeDetailsPost(body) {
    return this.http.post(this.apiUrl + 'setup/ConsumerCommiteeDetails', body)
      .map(
        (response) => response,
        (error) => error
      );
  }
  consumerCommitteeDetailsUpdate() {
  }
  consumerCommitteeDetailsDelete(ids) {
  }
  consumerCommitteeMemberGet() {
    return this.http.get(this.apiUrl + 'setup/ConsumerCommiteeMember')
      .map(
        (response) => response,
        (error) => error
      );
  }
  consumerCommitteeMemberPost(id, body) {
    return this.http.post(this.apiUrl + 'setup/ConsumerCommiteeMember/' + id, body)
      .map(
        (response) => response,
        (error) => error
      );
  }
  consumerCommitteeMemberUpdate() {
  }
  consumerCommitteeMemberDelete(ids) {
  }
  monitoringCommiteeGet() {
    return this.http.get(this.apiUrl + 'setup/AnugamanCommitee')
      .map(
        (response) => response,
        (error) => error
      );
  }
  monitoringCommiteePost(id, body) {
    return this.http.post(this.apiUrl + 'setup/AnugamanCommitee/' + id, body)
      .map(
        (response) => response,
        (error) => error
      );
  }
  monitoringCommiteeUpdate() {
  }
  monitoringCommiteeDelete(ids) {
  }



}
