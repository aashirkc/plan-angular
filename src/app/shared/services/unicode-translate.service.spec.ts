import { TestBed, inject } from '@angular/core/testing';

import { UnicodeTranslateService } from './unicode-translate.service';

describe('UnicodeTranslateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UnicodeTranslateService]
    });
  });

  it('should be created', inject([UnicodeTranslateService], (service: UnicodeTranslateService) => {
    expect(service).toBeTruthy();
  }));
});
