import { TestBed, inject } from '@angular/core/testing';

import { AnugamanSamitiMasterServiceService } from './anugaman-samiti-master-service.service';

describe('AnugamanSamitiMasterServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnugamanSamitiMasterServiceService]
    });
  });

  it('should be created', inject([AnugamanSamitiMasterServiceService], (service: AnugamanSamitiMasterServiceService) => {
    expect(service).toBeTruthy();
  }));
});
