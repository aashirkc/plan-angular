/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { EstimateAbstractOfCostService } from './estimate-abstract-of-cost.service';

describe('Service: EstimateAbstractOfCost', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EstimateAbstractOfCostService]
    });
  });

  it('should ...', inject([EstimateAbstractOfCostService], (service: EstimateAbstractOfCostService) => {
    expect(service).toBeTruthy();
  }));
});
