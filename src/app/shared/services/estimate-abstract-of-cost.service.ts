import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class EstimateAbstractOfCostService {
  apiUrl: string;
  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl;
  }
  index(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Approval/EstimateAbstractOfCost', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  indexDetails(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Approval/EstimateAbstractOfCostDetails', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  indexActivity(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  store(post) {
    return this.http.post(this.apiUrl + 'Approval/EstimateAbstractOfCost', post)
      .map(
        (response) => response,
        (error) => error
      );

  }
  storeDetails(post) {
    return this.http.post(this.apiUrl + 'Approval/EstimateAbstractOfCostDetails', post)
      .map(
        (response) => response,
        (error) => error
      );

  }
  destroy(id) {
    return this.http.delete(this.apiUrl + 'Approval/EstimateAbstractOfCost/' + id).map(
      (response: Response) => response,
      (error) => error)
  }
  destroyDetails(id) {
    return this.http.delete(this.apiUrl + 'Approval/EstimateAbstractOfCostDetails/' + id).map(
      (response: Response) => response,
      (error) => error)
  }
  update(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Approval/EstimateAbstractOfCost/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }
  updateDetails(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Approval/EstimateAbstractOfCostDetails/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  indexOrganizationMaster(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'setup/OrganizationsMaster', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  getTableData(post): Observable<any[]> {
    return this.http.get(this.apiUrl + 'Approval/EstimateAbstractOfCostDetails/' + post['activity'] + '?planYear=' + post['planYear'] || '')
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  getTableSecondData(post): Observable<any[]> {
    return this.http.get(this.apiUrl + 'Progress/MesurementBookAndBillofqty/' + post['activity'] + '?planYear=' + post['planYear'] || '')
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  getTableThirdData(post): Observable<any[]> {
    return this.http.get(this.apiUrl + 'Approval/EstimateAbstractOfCost/' + post['activity'] + '?planYear=' + post['planYear'] || '')
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }


}
