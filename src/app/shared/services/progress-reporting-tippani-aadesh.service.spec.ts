/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProgressReportingTippaniAadeshService } from './progress-reporting-tippani-aadesh.service';

describe('Service: ProgressReportingTippaniAadesh', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProgressReportingTippaniAadeshService]
    });
  });

  it('should ...', inject([ProgressReportingTippaniAadeshService], (service: ProgressReportingTippaniAadeshService) => {
    expect(service).toBeTruthy();
  }));
});
