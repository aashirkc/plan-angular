import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class KataSanchalanService {
    apiUrl: string;
    constructor(private http: HttpClient, @Inject('API_URL') apiUrl: string) {
        this.apiUrl = apiUrl;
    }
    /**************************ACTIVITY GET********************** */
    indexActivity = (post): Observable<any[]> => {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'Bank/KhataSanchalan', {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    };

    indexActivity1 = (post): Observable<any[]> => {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'Bank/CommiteeProgram', {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    };
    /**************************FINANCIAL OPERATION GET********************** */
    indexKhata = (post): Observable<any[]> => {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'bankDocument/KhataSanchalan', {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    };
    storeFinancial(post) {
        return this.http
            .post(this.apiUrl + 'bankDocument/KhataSanchalan', post)
            .map(response => response, error => error);
    }

    /****************************************************************************** */
    /**************************  Beign of Services by Sagun  ********************** */
    /****************************************************************************** */

    indexKhataBanda(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'bankDocument/KhataBanda', {
                headers: myHeaders,
                params: Params
            })
            .map(
                response => {
                    return <any[]>response;
                },
                error => error
            );
    }
    storeKhataBanda(post) {
        return this.http
            .post(this.apiUrl + 'bankDocument/KhataBanda', post)
            .map(response => response, error => error);
    }

    indexFarfaarak(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'bankDocument/Pharphaarak', {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }

    storeFarfaarak(post) {
        return this.http
            .post(this.apiUrl + 'bankDocument/Pharphaarak', post)
            .map(response => response, error => error);
    }
    /**************************ACCOUNT EDIT GET********************** */
    indexAdhyaawadhik(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'bankDocument/KhataAdhyaawadhik', {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }
    storeAdhyaawadhik(post) {
        return this.http
            .post(this.apiUrl + 'bankDocument/KhataAdhyaawadhik', post)
            .map(response => response, error => error);
    }
    indexNameChange(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + 'bankDocument/NameChange', {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }
    storeNameChange(post) {
        return this.http
            .post(this.apiUrl + 'bankDocument/NameChange', post)
            .map(response => response, error => error);
    }
}
