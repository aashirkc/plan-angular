import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ToggleSidebarService {
  private messageSource = new BehaviorSubject('off');
  currentMessage = this.messageSource.asObservable();
  constructor(
    @Inject(DOCUMENT) private document: any
  ) { }


  changeMessage(message: string) {
    this.messageSource.next(message)
    localStorage.setItem('wpms_sidebar', message);
  }
}