/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AssetsDetailTypeService } from './assets-detail-type.service';

describe('Service: AssetsDetailType', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssetsDetailTypeService]
    });
  });

  it('should ...', inject([AssetsDetailTypeService], (service: AssetsDetailTypeService) => {
    expect(service).toBeTruthy();
  }));
});
