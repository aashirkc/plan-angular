import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class AdministrativeApprovalService {

  apiUrl: string;
  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl;
  }
  index(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Approval/AdministrativeApproval', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  store(post) {
    console.log(post);
    return this.http.post(this.apiUrl + 'Approval/AdministrativeApproval', post)
      .map(
        (response) => response,
        (error) => error
      );

  }

  show(id) {
    return this.http.get(this.apiUrl + 'Approval/AdministrativeApproval/' + id)
      .map(
        (response) => response,
        (error) => error
      );

  }
  destroy(id) {
    return this.http.delete(this.apiUrl + 'Approval/AdministrativeApproval/' + id).map(
      (response: Response) => response,
      (error) => error)
  }
  update(id, post): Observable<any[]> {
    // console.log("id: "+id);
    // console.log("post: "+JSON.stringify(post));
    return this.http.put(this.apiUrl + 'Approval/AdministrativeApproval/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  getTechnicalDetail(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'TechnicalApproval/TechnicalApproval', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getTechnicalCostDetail(post): Observable<any[]> {
    return this.http.get(this.apiUrl + 'TechnicalApproval/TechnicalApproval/Administrative/' + post)
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  delete(id): Observable<any[]> {
    return this.http.delete(this.apiUrl + 'Approval/AdministrativeApproval?imageName=' + id)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  delete1(id): Observable<any[]> {
    return this.http.delete(this.apiUrl + 'Approval/WorkServiceTypeContractDocument/' + id)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  deleteContract(id): Observable<any[]> {
    return this.http.delete(this.apiUrl + 'Approval/AdministrativeApproval/Contract/' + id)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  getApprovalByid(id): Observable<any[]> {
    return this.http.get(this.apiUrl + 'Approval/AdministrativeApproval?activity=' + id)
      .map(
        (response) => {
            return <any[]>response
        },
        (error) => error
      );
  }
  getTechCost(id): Observable<any[]> {
    return this.http.get(this.apiUrl + 'TechnicalApproval/TechnicalApproval/' + id)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  saveReport = (id :string, data:any): Observable<any[]> => {
    return this.http.patch(this.apiUrl + `Report/Samjhauta/${id}`, data).map(
        (response) => <any[]>response,
        (error) => error
    )
  }

}
