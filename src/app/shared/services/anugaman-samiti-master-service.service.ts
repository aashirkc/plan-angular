import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AnugamanSamitiMasterServiceService {
    apiUrl: string;
    constructor(private http: HttpClient, @Inject('API_URL') apiUrl: string) {
        this.apiUrl = apiUrl;
    }

    save(post) {
        return this.http
            .post(this.apiUrl + 'Setup/AnugamanSamiti', post)
            .map(response => response, error => error);
    }

    index() {
        const myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        return this.http
            .get(this.apiUrl + 'Setup/AnugamanSamiti', {
                headers: myHeaders
            })
            .map(response => <any[]>response, error => error);
    }
}
