import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class DivisionServiceService {

    apiUrl: string;
    constructor(
      private http: HttpClient,
      @Inject('API_URL') apiUrl: string
    ) {
      this.apiUrl = apiUrl;
    }

    index(post): Observable<any[]> {
      let myHeaders = new HttpHeaders();
      myHeaders.append('Content-Type', 'application/json');
      let Params = new HttpParams();
      for (let key in post) {
        if (post.hasOwnProperty(key)) {
          Params = Params.append(key, post[key]);
        }
      }
      return this.http.get(this.apiUrl + 'Setup/Division', { headers: myHeaders, params: Params })
        .map(
          (response) => <any[]>response,
          (error) => error
        )
    }

    store(post) {
      return this.http.post(this.apiUrl + 'Setup/Division', post)
        .map(
          (response) => response,
          (error) => error
        );
    }

    destroy(id) {
      return this.http.delete(this.apiUrl + 'Setup/Division/' + id).map(
        (response: Response) => response,
        (error) => error)
    }

    show(searchString) {
      return this.http.get(this.apiUrl + 'Setup/Division/' + searchString).map(
        (response: Response) => response,
        (error) => error
      );
    }

    update(id, post): Observable<any[]> {
      return this.http.put(this.apiUrl + 'Setup/Division/' + id, post)
        .map(
          (response) => <any[]>response,
          (error) => error
        );
    }

    showChild(id) {
      return this.http.get(this.apiUrl + 'Setup/Division/' + id).map(
        (response: Response) => response,
        (error) => error
      );
    }
    showItem(id) {
      return this.http.get(this.apiUrl + 'Setup/Division/' + id).map(
        (response: Response) => response,
        (error) => error
      );
    }

    // getParent() {
    //   return this.http.get(this.apiUrl + 'Setup/Division/parent').map(
    //     (response: Response) => response,
    //     (error) => error
    //   );
    // }

}
