import { TestBed, inject } from '@angular/core/testing';

import { AssetsDetailsCategoryService } from './assets-details-category.service';

describe('AssetsDetailsCategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssetsDetailsCategoryService]
    });
  });

  it('should be created', inject([AssetsDetailsCategoryService], (service: AssetsDetailsCategoryService) => {
    expect(service).toBeTruthy();
  }));
});
