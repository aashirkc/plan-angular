import { TestBed, inject } from '@angular/core/testing';

import { AdministrativeApprovalService } from './administrative-approval.service';

describe('AdministrativeApprovalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdministrativeApprovalService]
    });
  });

  it('should be created', inject([AdministrativeApprovalService], (service: AdministrativeApprovalService) => {
    expect(service).toBeTruthy();
  }));
});
