import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProgramsService {

  private apiUrl = "";

  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl;
  }

  saveProgram(post) {
    return this.http.post(this.apiUrl + 'Activity/YearlyPrograms', post)
      .map(
        (response) => response,
        (error) => error
      );
  }
  getYearlyPrograms(post) {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Activity/YearlyPrograms', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getPrograms(post) {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'yearlyPrograms/ForActivity', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getBudget(post): Observable<any[]> {
    return this.http.get(this.apiUrl + 'Activity/YearlyPrograms/' + post)
      .map(
        (response) => <any[]>response,
        (error) => error
      )

  }
  show(searchString) {
    return this.http.get(this.apiUrl + 'Activity/YearlyPrograms/' + searchString).map(
      (response: Response) => response,
      (error) => error
    );
  }
  getPlanYear() {
    return this.http.get(this.apiUrl + 'Setup/PlanYear')
      .map(
        (response) => response,
        (error) => error
      );
  }

  deleteProgram(id) {
    return this.http.delete(this.apiUrl + 'Activity/YearlyPrograms/' + id)
      .map(
        (response) => response,
        (error) => error
      );
  }

  updateProgram(value, id) {
    return this.http.put(this.apiUrl + 'Activity/YearlyPrograms/' + id, value)
      .map(
        (response) => response,
        (error) => error
      );
  }

  getByPlanYearWorkAreaAndSubWorkArea(data) {
    return this.http.get(this.apiUrl + '', data)
      .map(
        (response) => response,
        (error) => error
      );
  }

}
