/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ActivityOutputService } from './activity-output.service';

describe('Service: ActivityOutput', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityOutputService]
    });
  });

  it('should ...', inject([ActivityOutputService], (service: ActivityOutputService) => {
    expect(service).toBeTruthy();
  }));
});
