import { Injectable } from '@angular/core';
import { NgxPermissionsService } from 'ngx-permissions';
import { Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class CurrentUserService {
  public currentUserPermissions: any = [];
  constructor(
    private permissionsService: NgxPermissionsService,
    private router: Router,
  ) {

  }

  getTokenData() {
    let data = {};
    let tokenData = localStorage.getItem('pcToken');
    let userData = JSON.parse(localStorage.getItem('pcUser'));
    data = {
      token: tokenData,
      user: userData
    }
    return data;
  }

  getRole() {
    let data = {};
    let userData = JSON.parse(localStorage.getItem('pcUser'));
    if (userData && userData['roles'] && userData['roles'][0]) {
      data = userData['roles'][0];
    }
    return data;
  }

  /**
   * 
   */
  currentPermissions: any = [];

  setCurrentPermissions() {
    this.currentPermissions = [];
    this.permissionsService.permissions$.subscribe((permissions) => {
      // this.currentPermissions = permissions;
      for (let obj in permissions) {
        this.currentPermissions.push(obj);
      }
      console.log(this.currentPermissions);
    });
  }

  getPermissions() {
    // this.setCurrentPermissions();
    this.currentPermissions = JSON.parse(localStorage.getItem('teaERPPerms'));
    return this.currentPermissions;
  }


  hasPermission(name: string) {
    // this.setCurrentPermissions();
    this.currentPermissions = JSON.parse(localStorage.getItem('teaERPPerms'));

    if (!this.currentPermissions || this.currentPermissions.length < 1) {
      this.removeData();
      this.router.navigate(['/login'], { queryParams: { type: 'error', msg: 'Please, login to continue' } });
      return false;
    }

    if (this.currentPermissions.indexOf(name) > -1) {
      return true;
    }
    return false;
  }

  removeData() {
    localStorage.removeItem('pcToken');
    localStorage.removeItem('pcUser');
  }

}
