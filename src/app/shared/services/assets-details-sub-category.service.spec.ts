import { TestBed, inject } from '@angular/core/testing';

import { AssetsDetailsSubCategoryService } from './assets-details-sub-category.service';

describe('AssetsDetailsSubCategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssetsDetailsSubCategoryService]
    });
  });

  it('should be created', inject([AssetsDetailsSubCategoryService], (service: AssetsDetailsSubCategoryService) => {
    expect(service).toBeTruthy();
  }));
});
