import { Injectable } from '@angular/core';
import { Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpHeaders, HttpXsrfTokenExtractor, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { JwtHelperService } from '@auth0/angular-jwt';
// import { AppDataService } from '../../shared';

@Injectable()
export class TfmHttpInterceptorService implements HttpInterceptor{

  jwtHelper: JwtHelperService = new JwtHelperService();

  constructor(
    private router: Router,
    private tokenExtractor: HttpXsrfTokenExtractor,
    // private dataService: AppDataService
    // private currentUserService: CurrentUserService
  ) { }
  
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      let currentUser =localStorage.getItem('pcToken');
      const token: string = currentUser || null;
      if(token && this.jwtHelper.isTokenExpired(token)){
          localStorage.removeItem('pcToken');
          this.router.navigate(['/login'],{ queryParams: { type:'error', msg: 'Session Expired !! Please login to continue' } });
          // return false;
      }
      if (token) {
        
          req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token)});
          // req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + ' eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJNQU5PSiIsImlhdCI6MTUzNDY2NjcwMCwic3ViIjoiTUFOT0oiLCJpc3MiOiJNQU5PSiIsImV4cCI6MTU1OTI0MDEwMH0.rDIzR8a9_aEs430COk_ftEHQE-b7hBzaByb69JkDbWo')});
         
      }
      
      return next.handle(req);
  }

}
