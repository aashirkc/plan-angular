/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ActivityMasterService } from './activity-master.service';

describe('Service: ActivityMaster', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityMasterService]
    });
  });

  it('should ...', inject([ActivityMasterService], (service: ActivityMasterService) => {
    expect(service).toBeTruthy();
  }));
});
