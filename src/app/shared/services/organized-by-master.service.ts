import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class OrganizedByMasterService {

  apiUrl: string;
    constructor(
        private http: HttpClient,
        @Inject('API_URL') apiUrl: string
    ) {
        this.apiUrl = apiUrl;
    }
    index(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Setup/OrganizedByMaster', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )

    }

    indexOrgMaster(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'setup/OrganizationsMaster', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )

    }

    store(post) {
        return this.http.post(this.apiUrl + 'Setup/OrganizedByMaster', post)
            .map(
                (response) => response,
                (error) => error
            );

    }

    orgStore(post) {
        return this.http.post(this.apiUrl + 'setup/OrganizationsMaster', post)
            .map(
                (response) => response,
                (error) => error
            );

    }

    destroy(id) {
        return this.http.delete(this.apiUrl + 'Setup/OrganizedByMaster/' + id).map(
            (response: Response) => response,
            (error) => error)
    }
    update(id, post): Observable<any[]> {
        return this.http.put(this.apiUrl + 'Setup/OrganizedByMaster/' + id, post)
            .map(
                (response) => <any[]>response,
                (error) => error
            );
    }

    orgUpdate(id, post): Observable<any[]> {
        return this.http.patch(this.apiUrl + 'setup/OrganizationsMaster/' + id, post)
            .map(
                (response) => <any[]>response,
                (error) => error
            );
    }

}
