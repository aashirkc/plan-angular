/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { InclusionGroupService } from './inclusion-group.service';

describe('Service: InclusionGroup', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InclusionGroupService]
    });
  });

  it('should ...', inject([InclusionGroupService], (service: InclusionGroupService) => {
    expect(service).toBeTruthy();
  }));
});
