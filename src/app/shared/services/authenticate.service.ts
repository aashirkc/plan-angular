import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticateService {

  public token: string;
  apiUrl: string;
  LoginUrl: string;

  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string,
    @Inject('LoginUrl') LoginUrl: string

  ) {
    this.apiUrl = apiUrl;
    this.LoginUrl = LoginUrl;
    //set token if saved in localstorage
    this.token = localStorage.getItem('examRegistraionToken') || null;
  }

  getToken(): string {
    let currentToken = localStorage.getItem('examRegistraionToken') || null;
    return currentToken;
  }

  getUser(): any {
    let currentUser = JSON.parse(localStorage.getItem('examRegistraionUser'));
    return currentUser;
  }

  login(post): Observable<any[]> {
    return this.http.post(this.LoginUrl + 'Login', post )
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  logout(): Observable<any[]> {
    this.token = null;
    return this.http.post(this.apiUrl + 'Logout', {})
      .map((response) => {
        console.info(response);
        return <any[]>response;
      },
        (error: Error) => error);
  }

  loginTokenUpdate(id): Observable<any[]> {
    return this.http.patch(this.apiUrl + 'Setup/OrganizationLogin/'+id, {})
      .map((response) => {
        let token = response && response['token'];
        return <any[]>response;
      },
        (error: Error) => error);
  }

  getDetail(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'authenticate', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }


}

