/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CapacityBuildingService } from './capacity-building.service';

describe('Service: CapacityBuilding', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CapacityBuildingService]
    });
  });

  it('should ...', inject([CapacityBuildingService], (service: CapacityBuildingService) => {
    expect(service).toBeTruthy();
  }));
});
