/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { BudgetTransferService } from './budget-transfer.service';

describe('Service: BudgetTransfer', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BudgetTransferService]
    });
  });

  it('should ...', inject([BudgetTransferService], (service: BudgetTransferService) => {
    expect(service).toBeTruthy();
  }));
});
