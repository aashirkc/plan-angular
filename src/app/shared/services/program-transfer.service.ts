import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class ProgramTransferService {

    apiUrl: string;
    constructor(
        private http: HttpClient,
        @Inject('API_URL') apiUrl: string
    ) {
        this.apiUrl = apiUrl;
    }
    index(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Activity/ProgramTransfer', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )

    }
    indexProgram(post): Observable<any[]> {
        return this.http.get(this.apiUrl + 'Activity/ProgramTransfer/'+post)
            .map(
                (response) => <any[]>response,
                (error) => error
            )

    }
    // indexAddition(post): Observable<any[]> {
    //     let myHeaders = new HttpHeaders();
    //     myHeaders.append('Content-Type', 'application/json');
    //     let Params = new HttpParams();
    //     for (let key in post) {
    //         if (post.hasOwnProperty(key)) {
    //             Params = Params.append(key, post[key]);
    //         }
    //     }
    //     return this.http.get(this.apiUrl + 'Activity/ProgramTransfer/sajhedari', { headers: myHeaders, params: Params })
    //         .map(
    //             (response) => <any[]>response,
    //             (error) => error
    //         )

    // }
    store(post) {
        return this.http.post(this.apiUrl + 'Activity/ProgramTransfer', post)
            .map(
                (response) => response,
                (error) => error
            );

    }
    destroy(id) {
        return this.http.delete(this.apiUrl + 'Activity/ProgramTransfer/' + id).map(
            (response: Response) => response,
            (error) => error)
    }
    update(id, post): Observable<any[]> {
        return this.http.put(this.apiUrl + 'Activity/ProgramTransfer/' + id, post)
            .map(
                (response) => <any[]>response,
                (error) => error
            );
    }
    getProgram(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Activity/YearlyPrograms', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )

    }

}
