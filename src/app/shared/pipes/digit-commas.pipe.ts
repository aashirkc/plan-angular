import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'digitCommas'
})
export class DigitCommasPipe implements PipeTransform {
    mapping = {
        '०': '0',
        '१': '1',
        '२': '2',
        '३': '3',
        '४': '4',
        '५': '5',
        '६': '6',
        '७': '7',
        '८': '8',
        '९': '9'
    };

    convertToEnglish = (value: string): string => {
        if (value) {
            let retData = '';
            for (let i = 0; i < value.toString().length; i++) {
                if (value[i] in this.mapping) {
                    retData += this.mapping[value[i]];
                } else {
                    retData += value.toString()[i];
                }
            }
            return retData;
        }
        return null;
    };

    transform(value: string, args?: any): string {
        value = this.convertToEnglish(value);
        const numb = Number(value);
        if (numb) {
            let num = numb.toString();
            let sign = num.substr(0, 1);
            if (sign === '-') {
                num = num.substr(1, num.length - 1);
            } else {
                sign = '';
            }
            const decimal = num.split('.');
            const result = [];
            let data = '';
            value = decimal[0];
            for (let i = 0; i < value.length; i++) {
                if (i === 3) {
                    result.push(data);
                    data = '';
                }
                if (i > 3 && (i - 1) % 2 === 0) {
                    result.push(data);
                    data = '';
                }
                data += value[value.length - 1 - i].toString();
            }
            result.push(data);
            let res = result
                .join(',')
                .split('')
                .reverse()
                .join('');
            if (decimal[1]) {
                res += '.' + decimal[1];
            }
            return sign + res;
        } else {
            return '0';
        }
    }
}