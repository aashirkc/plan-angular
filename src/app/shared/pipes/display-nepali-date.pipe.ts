import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'displayNepaliDate'
})
export class DisplayNepaliDatePipe implements PipeTransform {
  transform(value: any): any {

    let devanagariDigits = {
      '0': '०',
      '1': '१',
      '2': '२',
      '3': '३',
      '4': '४',
      '5': '५',
      '6': '६',
      '7': '७',
      '8': '८',
      '9': '९'
    };

    if (value) {
      /* Converting English number to Devangari (js) */
      let value_np = value.toString().replace(/[0123456789]/g, function (s) {
        return devanagariDigits[s];
      });
      return value_np;
    }

    /* Converting Devanagari number to English (js) */
    // let value_en = value.replace(/[०१२३४५६७८९]/g, function (s) {
    //   return this.englishDigits[s];
    // });

    return '';
  }
}
