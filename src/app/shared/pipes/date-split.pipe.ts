import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateSplit'
})
export class DateSplitPipe implements PipeTransform {

  transform(dateString: any, format: string): any {
    let dateArray = dateString.split('-');
    if(format == 'y'){
      return dateArray[0];
    }
    if(format == 'm'){
      return dateArray[1];
    }
    if(format == 'd'){
      return dateArray[2];
    }
    return dateString;
  }

}
