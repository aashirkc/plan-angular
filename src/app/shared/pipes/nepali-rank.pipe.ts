import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nepaliRank'
})
export class NepaliRankPipe implements PipeTransform {

  transform(value: any): any {
    if (value == 1) {
      return 'प्रथम';
    } else if (value == 2) {
      return 'दृतिय';
    } else if (value == 3) {
      return 'तृतीय';
    } else if (value == 4) {
      return 'चौथो';
    } else if (value == 5) {
      return 'पाँचौ';
    } else if (value == 6) {
      return 'छैठौं';
    } else if (value == 7) {
      return 'सातौ';
    } else if (value == 8) {
      return 'आठौ';
    } else if (value == 9) {
      return 'नोवौ';
    } else if (value == 10) {
      return 'दशौ';
    } else {
      return value;
    }

  }

}
