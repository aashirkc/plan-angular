import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NpdigitTextPipe } from './npdigit-text.pipe';
import { DigitCommasPipe } from './digit-commas.pipe';
// import { DateSplitPipe } from './date-split.pipe';
// import { NepaliRankPipe } from './nepali-rank.pipe';

@NgModule({
    imports: [CommonModule],
    declarations: [
        //NepaliRankPipe,
        //    NpdigitTextPipe
        // DigitCommasPipe
    ],
    exports: []
})
export class SharedPipesModule {}
