import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenerateAllReportComponent } from './generate-all-report.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { GenerateAllReportRoutingModule } from './generate-all-report-routing.module';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GenerateAllReportRoutingModule
  ],
  declarations: [GenerateAllReportComponent]
})
export class GenerateAllReportModule { }
