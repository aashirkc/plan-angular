import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivityOutputService,AdministrativeApprovalService,ProgressReportingTippaniAadeshService,ToggleSidebarService,ActivityDetailService, AllReportService, UnicodeTranslateService,DateConverterService, KataSanchalanService,ProgressReportingService,} from '../../shared';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';


@Component({
  selector: 'app-generate-all-report',
  templateUrl: './generate-all-report.component.html',
  styleUrls: ['./generate-all-report.component.scss']
})
export class GenerateAllReportComponent implements OnInit {
  activityId;
  dataLists: any = [];
  planAggrement: any;
  consumerCommiteName: string;
  consumerCommiteAddres: string;
  totalAmount: number = 0;
  heading: Array<any> = [];
  monitoringCommittees: Array<any> = [];
  adyakshya :any;
  adyakshyaAddress :any;
  kosadakshya :any;
  kosadakshyaAddress :any;
  sachib :any;
  sachibAddress :any;
  activityDatas:any={};
  date:any;
  karyadeshData:any={};
  committeeName:any;
  committeePost:any;
  currentDate:any;
  currentDate1:any;
  tippaniadeshData:any={};
  bankOpenData:any={};
  bankCloseData:any={};
  bankEditData:any={};
  bankNameChangeData:any={};
  anusuchi3Data:any={};
  anusuchi3Data1:any={};
  anusuchi4Data:any={};
  anusuchi4Data1:any={};
  anusuchi5Data:any={};
  anusuchi5Data1:any={};
  anusuchi6Data:any={};
  anusuchi6Data1:any={};
  anugaman:boolean=false;
  capacityBuildingData:any;
  anugamanSadasya:any=[];
  lastStage:boolean=false;
  anugamanArray:any=[];
  // anugamanData:any={};
  approvalData:any={};
  anugamanItem:any;
  financialProgressData:any;
  tippaniArray:any=[];
  insStage:any;
  progressDataArray: any=[];
  totalAmount1:any;
  consumerCommitteOrContractCompany:any;
  resultDatas:any=[];
  startDate:any='';
  endDate:any='';


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private report: AllReportService,
    private aas: AdministrativeApprovalService,
    private ads: ActivityDetailService,    
    private fb: FormBuilder,    
    private unicode: UnicodeTranslateService,
    private dcs: DateConverterService,
    private ks: KataSanchalanService,
    private ts:ToggleSidebarService,
    private progressReport: ProgressReportingService,
    private prtas: ProgressReportingTippaniAadeshService,
    private aos: ActivityOutputService,
    
  ) { 
    // this.ts.currentMessage.subscribe((data) => {
    //   if(data=="off")
    //   {
    //     this.router.navigate(['/activity/activity-output'])
    //   }
    // });
  }

  ngOnInit() {
      
  }

  ngAfterViewInit() {
    this.date = this.dcs.getToday()['fulldate'];
    this.report.getOrg().subscribe((result) => {
      if (result.length == 1 && result[0].error) {
        this.heading = [];
      } else {
        this.heading = result[0];
        console.info(this.heading);

      }

    }, (error) => {
      console.info(error);
    });
    this.unicode.initUnicode();
    this.activityDatas=JSON.parse(localStorage.getItem('ActivityDatas'));
    this.activityId = this.activityDatas['id']
    console.log(this.activityId)
    this.aas.getApprovalByid(this.activityId).subscribe((result: any) => {
      this.dataLists = result;
      this.monitoringCommittees = result && result['monitoringCommittee'] || [];
      let officeCost = this.dataLists && this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromLocalLevel || 0;
      let ccCost = this.dataLists && this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromConsumerCommitte || 0;
      let ingoCost = this.dataLists && this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromIngo || 0;
      let ngoCost = this.dataLists && this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromNgo || 0;
      let stateCost = this.dataLists && this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromState || 0;
      let unionCost = this.dataLists && this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromUnion || 0;
      let others = this.dataLists && this.dataLists['RemainingPlan'] && this.dataLists['RemainingPlan'][0] &&  this.dataLists['RemainingPlan'][0].grantedFromOthers || 0;
      
      this.totalAmount = Number(officeCost) + Number(ccCost) + Number(ingoCost) + Number(ngoCost) + Number(stateCost) + Number(unionCost) + Number(others);

      this.consumerCommiteName = result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['consumerCommitteName'];
      this.consumerCommiteAddres = result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['consumerCommitteAddress'];
    
      for(let i=0;i< result['CommitteDetails'].length;i++){
        if( result['CommitteDetails'][i]['memberPost'] == 'अध्यक्ष'){
          this.adyakshya = result['CommitteDetails'][i]['memberName'];
          this.adyakshyaAddress = result['CommitteDetails'][i]['committeAddress'];
        } 
        else if(result['CommitteDetails'][i]['memberPost'] == 'कोषाध्यक्ष'){
          this.kosadakshya = result['CommitteDetails'][i]['memberName'];
          this.kosadakshyaAddress = result['CommitteDetails'][i]['committeAddress'];
        } 
        else if(result['CommitteDetails'][i]['memberPost'] == 'सचिव'){
          this.sachib = result['CommitteDetails'][i]['memberName'];
          this.sachibAddress = result['CommitteDetails'][i]['committeAddress'];
        }
      }
      this.getKaryadesh();

    });
  }
  getKaryadesh(){
    this.report.getKaryaDeshReport1({'id':this.activityId, }).subscribe((res) => {
      if(res['error']){
        this.getKaryadesh1();
      }
      if(res['karyaAdesh']==null){
        this.getKaryadesh1();
      }else{
        console.log(res);
        let a=JSON.parse(res['karyaAdesh']);
        console.log(a);
        this.karyadeshData=a['karyadesh']
        this.currentDate=this.karyadeshData['currentDate'];
        this.committeeName=this.karyadeshData['committeeName'];
        this.committeePost=this.karyadeshData['committeePost'];
        this.getTippaniadesh()
      }
      }, (error) => {
        console.info(error);
      });
  }
  getKaryadesh1(){

    this.report.getKaryaDeshReport(this.activityId, {}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
       
        //window.alert('error'+res[0].error);
        this.karyadeshData = {};
      } else {
        this.karyadeshData = res[0] || null;
        this.committeeName=this.karyadeshData['committee'][0]['name'] ;
        this.committeePost=this.karyadeshData['committee'][0]['post'] ;
        this.currentDate=this.date;
        this.getTippaniadesh()

      }
console.log(this.karyadeshData)
    }, (error) => {
      console.info(error);
    });
  }
  getTippaniadesh(){
    this.report.getKaryaDeshReport1({'id':this.activityId, }).subscribe((res) => {
      if(res['error']){
        this.getTippaniadesh1();
      }
      if(res['tipaniAdesh']==null){
        this.getTippaniadesh1();
      }else{
        console.log(res);
        let a=JSON.parse(res['tipaniAdesh']);
        console.log(a);
        let sourceData=a['tippani']
        this.tippaniadeshData=sourceData;
        this.currentDate1=this.tippaniadeshData['currentDate']
        this.bankOpen()
      }
      }, (error) => {
        console.info(error);
      });
  }
  getTippaniadesh1(){
    this.report.getKaryaDeshReport(this.activityId, {}).subscribe((res) => {
      if (res.length == 1 && res[0].error || res['error']) {
  
        //window.alert('error'+res[0].error);
        this.karyadeshData = {};
      } else {
        this.tippaniadeshData = res[0] || null;
        this.currentDate1=this.date;
        this.bankOpen()
      }

    }, (error) => {
      console.info(error);
   
    });
  }
  bankOpen(){
    let post={};
          post['planYear']=this.activityDatas['planYear'];
          post['activity']=this.activityDatas['id'];
    this.ks.indexKhata(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
      
        //window.alert(res[0].error);
      
        this.bankOpenData = {};
      } else {
        this.bankOpenData= res[0];
        this.bankClose();

      }
      
    }, (error) => {
      console.info(error);
      
    });
  }
  bankClose(){
    let post={};
          post['planYear']=this.activityDatas['planYear'];
          post['activity']=this.activityDatas['id'];
    this.ks.indexKhataBanda(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        
       //window.alert(res[0].error);
       
        this.bankCloseData = {};
      } else {
        this.bankCloseData = res[0];
             this.bankEdit(); 
      }
     
    }, (error) => {
      console.info(error);
     
    });
    this.ks.indexKhata(post).subscribe((res) => {
      this.bankCloseData['address']=res[0].address;
      });
  }
  bankEdit(){
    let post={};
    post['planYear']=this.activityDatas['planYear'];
    post['activity']=this.activityDatas['id'];
    this.ks.indexAdhyaawadhik(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
      //window.alert(document.getElementById('error'));
        
       
      this.bankEditData={}
      } else {
        this.bankEditData= res[0];
        this.bankNameChange();
       
      }
      
    }, (error) => {
      console.info(error);
     
    });
  }
 bankNameChange(){
  let post={};
  post['planYear']=this.activityDatas['planYear'];
  post['activity']=this.activityDatas['id'];
  this.ks.indexNameChange(post).subscribe((res) => {
    if (res.length == 1 && res[0].error) {
      
      //window.alert(res[0].error);
      this.bankNameChangeData={};
    } else {
      this.bankNameChangeData = res[0];
     this.getAnusuchi3();
    }
  
  }, (error) => {
    console.info(error);
 
  });
 }
getAnusuchi3(){
  let post={};
  post['planYear']=this.activityDatas['planYear'];
  post['activity']=this.activityDatas['id'];
  this.report.getanusuchi3Report(post).subscribe((res) => {
    if (res.length == 1 && res[0].error) {
  
   //window.alert(res[0].error);
   
      this.anusuchi3Data = [];
    } else {
      this.anusuchi3Data = res;
     
      this.report.getanusuchi3ReportData(this.activityDatas['id']).subscribe((res) => {
       
        if(res['error']){
         //window.alert(res['error']);
         this.getAnusuchi4();
        }
        else{
        this.anusuchi3Data1=res;
        this.getAnusuchi4();
        }
      });
    }
    
  }, (error) => {
    console.info(error);
   
  });
}

getAnusuchi4(){
  let post={};
  post['planYear']=this.activityDatas['planYear'];
  post['activity']=this.activityDatas['id'];
  this.report.getanusuchi4Report(post).subscribe((res) => {
    if (res.length == 1 && res[0].error) {
  
      //window.alert(res[0].error);
      
      this.anusuchi4Data = [];
    } else {
      this.anusuchi4Data = res;
    
      this.report.getanusuchi4ReportData(this.activityDatas['id']).subscribe((res) => {
        console.log(res);
        if(res['error']){
          //window.alert(res['error']);
         this.getAnusuchi5()
        }
        else{
          this.anusuchi4Data1=res;
        this.getAnusuchi5()
        }
      });
    }
   
  }, (error) => {
    console.info(error);

  });
}

getAnusuchi5(){
  let post={};
  post['planYear']=this.activityDatas['planYear'];
  post['activity']=this.activityDatas['id'];
  this.report.getanusuchi5Report(post).subscribe((res) => {
    if (res.length == 1 && res[0].error) {
    //window.alert(res[0].error);
    
      this.anusuchi5Data = [];
    } else {
      this.anusuchi5Data = res;
      
      this.aas.getApprovalByid(this.activityDatas['id']).subscribe((result: any) => {
        this.approvalData=result;
        this.anusuchi5Data1 = result['PlanAgreement'][0];
        this.getAnusuchi6();
      //  this.foForm.controls['janasahavagita'].patchValue(this.anusuchi5Data1['PlanAgreement'][0].grantedFromConsumerCommitte)
      });
      
    }
    
  }, (error) => {
    console.info(error);
    
  });
}

getAnusuchi6(){
  let post={};
  post['planYear']=this.activityDatas['planYear'];
  post['activity']=this.activityDatas['id'];
  this.report.getanusuchi6Report(post).subscribe((res) => {
    if (res.length == 1 && res[0].error) {
 
    //window.alert(res[0].error);
   
      this.anusuchi6Data = [];
    } else {
      this.anusuchi6Data = res;

      this.report.getanusuchi6ReportData(this.activityDatas['id']).subscribe((res) => {
        console.log(res);
        if(res['error']){
         //window.alert(res['error']);
         this.getAnugaman()
        }
        else{
        this.anusuchi6Data1=res;
        this.getAnugaman()
          
        }
      });
    }
    
  }, (error) => {
    console.info(error);
   
  });
}
getAnugaman(){
  if (this.activityDatas['outputType'] == 'CB') {
    let dt = {};
    dt['activity'] = this.activityDatas['id'];
    this.progressReport.getPhysicalProgress(dt).subscribe((res) => {
      this.capacityBuildingData = res;
    for(let i=0;i<this.capacityBuildingData.length;i++){
      this.getAnugamanPratibedan(this.capacityBuildingData[i]['id'],this.capacityBuildingData[i]['investigationStage'].name)
    }
    this.anugaman=true;
    }, (error) => {
      console.info(error);
 
    });
  }
  if (this.activityDatas['outputType'] == 'SD') {
    let dt = {};
    dt['activity'] = this.activityDatas['id'];
    this.progressReport.getServiceProgress(dt).subscribe((res) => {
      this.capacityBuildingData = res;
      for(let i=0;i<this.capacityBuildingData.length;i++){
        this.getAnugamanPratibedan(this.capacityBuildingData[i]['id'],this.capacityBuildingData[i]['investigationStage'].name)
      }
      this.anugaman=true;
    }, (error) => {
      console.info(error);
     
    });
  }
  if (this.activityDatas['outputType'] == 'WOA') {
    let dt = {};
    dt['activity'] = this.activityDatas['id'];
    this.progressReport.getPhysicalAssetProgress(dt).subscribe((res) => {
      this.capacityBuildingData = res;
      for(let i=0;i<this.capacityBuildingData.length;i++){
        this.getAnugamanPratibedan(this.capacityBuildingData[i]['id'],this.capacityBuildingData[i]['investigationStage'].name)
      }
      this.getPragatiTippani();
      this.anugaman=true;
    }, (error) => {
      console.info(error);
   
    });
  }

}
getAnugamanPratibedan(id,stage){
  if (stage == 'अन्तिम चरण') {
    this.lastStage = true;
    // this.formAccount.get('category').patchValue(this.categoryAdapter[0].name);
  }
  let post = { type: this.activityDatas['outputType'] }
  this.report.getAnugaman(id, {}).subscribe(
    result => {
      if (result['message']) {
     
        //window.alert(result['message']);
     
     
      }
      if (result && result['dataValue']) {
        let resData=JSON.parse(result['dataValue']);
        this.anugamanItem=resData;
      // this.anugaman=true;
      // this.anugamanData;
       
        for (let i = 0; i < resData['sadasyaDetails'].length; i++) {       
          this.anugamanSadasya.push(resData['sadasyaDetails'][i] && resData['sadasyaDetails'][i].sadasya);
               }

        if (resData && resData['kharidPrakriya']) {
          this.getkharid("y")
          this.anugamanArray.push(this.anugamanItem);
          // this.anugaman=true;
   
        }
        else {
          this.getkharid("n");
          this.anugamanArray.push(this.anugamanItem);
        
         
        }
        console.log(this.anugamanArray);
      }
      //  else {
      //   this.report.getPhysicalDetails(id, post).subscribe((res) => {
      //     this.DetailsData = res['data'] || '';
      //     this.TableData = res['OtherData'] || '';
      //     this.formAccount.get('activityName').patchValue(this.TableData['activityName']);
      //     this.formAccount.controls['anugamanDate'].patchValue(this.DetailsData['enterDate']);
      //     this.formAccount.get('activityAddress').patchValue(this.TableData['activityAddress']);
      //     this.formAccount.get('kharidPrakriya').patchValue(this.TableData['kharidPrakriya']);
      //     this.formAccount.get('fromNepalSarkar').patchValue(this.TableData['fromNepalSarkar']);
      //     this.formAccount.get('fromState').patchValue(this.TableData['fromState']);
      //     this.formAccount.get('fromLocalLevel').patchValue(this.TableData['fromLocalLevel']);
      //     this.formAccount.get('fromWada').patchValue(this.TableData['fromWada']);
      //     this.formAccount.get('fromConsumerCommitte').patchValue(this.TableData['fromConsumerCommitte']);
      //     this.formAccount.get('fromOthers').patchValue(this.TableData['fromOthers']);
      //     this.formAccount.get('totalGranted').patchValue(this.TableData['totalGranted']);
      //     this.formAccount.get('patraSankya').patchValue(this.TableData['patraSankya']);
      //     this.formAccount.get('chalaniNo').patchValue(this.TableData['chalaniNo']);
      //     this.formAccount.get('monitoringDetails').patchValue(this.DetailsData['monitoringDetails']);
      //     if (res['OtherData'] && res['OtherData']['kharidPrakriya']) {
      //       this.getkharid("y")
      //     } else {
      //       this.getkharid("n");
      //     }
      //     if (res.length <= 0) {
      //       let messageDiv: any = document.getElementById('noRecord');
      //       messageDiv.innerText = 'कुनै विवरण भेटिएन';
      //     }

      //   }, (error) => {
      //     console.info(error);

      //   });

      // }
    
      if (result['error']) {
      
     //window.alert(result['error']['message']);
        
      }
    },
    error => {
      
      console.info(error);
    }
  );
}
getkharid(check: string) {

    // this.anugamanItem['estimatedWorkCompletionDate']=this.approvalData['data'].estimatedWorkCompletionDate;
    // this.anugamanItem['contractDate']=this.approvalData['data'].agreementDate;
    // this.anugamanItem['completionDate']=this.approvalData['ActivityDetails'].actualCompletedDate;

    if (check === 'n') {
      if (this.approvalData['CommitteDetails'].length > 0) {
        setTimeout(() => {this.anugamanItem['kharidPrakriya']="उपभोक्ता समिति"; }, 100)

      } else {
        this.anugamanItem['kharidPrakriya']='ठेक्का समिति';
      }
    }
 
}

getPragatiTippani(){
  let post = { activity: this.activityId };
  this.progressReport.getFinancialProgress(post).subscribe(
    result => {
      this.financialProgressData = result['Record'];
      console.log(this.financialProgressData.length);
      for(let i=0;i<this.financialProgressData.length;i++){
        // console.log('a')
        this.getTippani(this.financialProgressData[i]['data'].id,result['charan'][i].name)
      }
      this.getCompletion();
    },
    error => {
      console.log(error);
    
    }
  )
}
getTippani(id,stage){
  this.insStage=stage;
  let dataRecord = {
    financialId: id
  }
  this.prtas.get(this.activityId, dataRecord).subscribe((res) => {
   

    if (res.length <= 0) {
      //window.alert('कुनै विवरण भेटिएन');
    }else{
      this.totalAmount1 = Number(res[0]['issueAmount']) + Number(res[0]['previousAmt']);
      this.progressDataArray.push(res[0]);
      console.log(this.progressDataArray)
    }

  }, (error) => {
    console.info(error);

  });
  this.prtas.getTippani(id, {}).subscribe((res) => {
    this.tippaniArray.push(res)
    console.log(this.tippaniArray); 

  }, (error) => {
    console.info(error);

  });
// console.log(this.tippaniArray);
}

getCompletion(){
  this.progressReport.workCompletionCertificate(this.activityId,{type:this.activityDatas['outputType']}).subscribe(
    result => {
      this.startDate=result[0]['startDate'];
      this.endDate=result[0]['endDate'];
      // this.foForm.get('workStartdate').patchValue(result[0]['startDate']);
      // this.foForm.get('workCompletionDateTime').patchValue(result[0]['endDate']);
      this.resultDatas = result && result[1] 
      if (result[0]['consumerCommitteName'] == "") {
        this.consumerCommitteOrContractCompany = result[0]['contractorName'];
      } else {
        this.consumerCommitteOrContractCompany = result[0]['consumerCommitteName'];
      }
      // if (result[''])
    }, error => {
      console.log(error);
    }
  )
}

save(){
  let printContents = document.getElementById('print-section').innerHTML;
  let printPage= `<html>
  <head>
    <style>
    #showDiv{
      display: inline-block !important;
    }
    #hideDiv{
      display: none !important;
    }
      @font-face {
        font-family: 'nepaliNormal';
        src: url('assets/css/Kantipur.TTF');
        font-weight: normal;
        font-style: normal;
      }
      .nepaliText{
          font-family:'nepaliNormal';
          font-size:18px!important;
      }
      @font-face {
        font-family: 'preeti';
        src: url('../assets/css/Preeti.TTF');
        font-weight: normal;
        font-style: normal;
    }
    
    .preeti{
        font-family:'preeti';
        font-size:18px;
    }
      #space{
        display:none;
      }
    //........Customized style.......
    </style>
  </head>
<body>${printContents}</body>
</html>`
let post={
  'id':this.activityId,
  'html':printPage
}
this.aos.saveAllReport(post).subscribe((res)=>{
  console.log(res)
})
}
  print() {

    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    // document.getElementById('print-section').innerHTML.className = 'nepaliText';
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <style>
          #showDiv{
            display: inline-block !important;
          }
          #hideDiv{
            display: none !important;
          }
            @font-face {
              font-family: 'nepaliNormal';
              src: url('assets/css/Kantipur.TTF');
              font-weight: normal;
              font-style: normal;
            }
            .nepaliText{
                font-family:'nepaliNormal';
                font-size:18px!important;
            }
            @font-face {
              font-family: 'preeti';
              src: url('../assets/css/Preeti.TTF');
              font-weight: normal;
              font-style: normal;
          }
          
          .preeti{
              font-family:'preeti';
              font-size:18px;
          }
            #space{
              display:none;
            }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  close() {
    // this.router.navigate(['/approval/administrative-approval']);
  }
}
