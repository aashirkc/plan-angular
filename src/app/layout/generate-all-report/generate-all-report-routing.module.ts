import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GenerateAllReportComponent } from './generate-all-report.component';

const routes: Routes = [
    { path: '', component: GenerateAllReportComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenerateAllReportRoutingModule { }
