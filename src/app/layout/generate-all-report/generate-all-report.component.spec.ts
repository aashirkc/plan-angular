import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateAllReportComponent } from './generate-all-report.component';

describe('GenerateAllReportComponent', () => {
  let component: GenerateAllReportComponent;
  let fixture: ComponentFixture<GenerateAllReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateAllReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateAllReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
