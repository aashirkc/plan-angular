import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { Observable } from 'rxjs';
import { NgxPermissionsService } from 'ngx-permissions';
import { DateConverterService, ToggleSidebarService, } from '../shared';


@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class LayoutComponent implements OnInit {

    @ViewChild('navBar') navBar;
    @ViewChild('mainContainer') el: ElementRef;
    @ViewChild('innerContainer') innerContainer:ElementRef;

    public online: boolean = true;
    check:boolean=false;

    private roleInfo: any;
    events = [];
    constructor(
        public router: Router,
        private rd: Renderer2,
        private permissionsService: NgxPermissionsService,
        private converter: DateConverterService,
        private ts:ToggleSidebarService,
    ) {
        router.events.subscribe( (event: Event) => {

            if (event instanceof NavigationStart) {
                // Show loading indicator
            }

            if (event instanceof NavigationEnd) {
                // Hide loading indicator
                this.innerContainer.nativeElement.scrollTop = 0;
            }

            if (event instanceof NavigationError) {
                // Hide loading indicator
                // Present error to user
                console.log(event.error);
            }
        });
    }

    ngOnInit() {
        if (this.router.url === '/') {
            this.router.navigate(['/dashboard']);
        }
        this.ts.currentMessage.subscribe((data) => {
            if(data=="on")
            this.check=true;
            if(data=="off")
            this.check=false;
            console.log(this.check);
          });

    }

    ngAfterViewInit() {

    }

    getRoleDetail(id: number) {

    }
    closeBar(){
        this.ts.changeMessage("off");
        window.localStorage.removeItem('ActivityDatas')
    }
    getBack(){
        // this.closeBar();
        this.router.navigate(['/activity/activity-output']);
    }

    online$ = Observable.fromEvent(window, 'online').subscribe(
        x => {
            this.online = true;
        }
    );

    offline$ = Observable.fromEvent(window, 'offline').subscribe(
        x => {
            this.online = false;
        }
    );

    retry() {
        if (this.online) {

        }

    }

    CanDeactivate() {
        alert('Navigating Away');
        return window.confirm('Discard changes?');
    }

}
