import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NameChangeComponent } from './name-change.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { NameChangeRoutingModule } from './name-change-routing.module';
import { AnusuchiSearchBarModule } from 'app/layout/report/anusuchi-search-bar/anusuchi-search-bar.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AnusuchiSearchBarModule,
    NameChangeRoutingModule
  ],
  declarations: [NameChangeComponent],
  schemas: [ NO_ERRORS_SCHEMA ]

})
export class NameChangeModule { }
