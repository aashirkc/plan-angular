import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NameChangeComponent } from './name-change.component';

const routes: Routes = [
  {
    path: '',
    component: NameChangeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NameChangeRoutingModule { }