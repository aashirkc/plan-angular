import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountEditComponent } from './account-edit.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AccountEditRoutingModule } from './account-edit-routing.module';
import { AnusuchiSearchBarModule } from 'app/layout/report/anusuchi-search-bar/anusuchi-search-bar.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AnusuchiSearchBarModule,
    AccountEditRoutingModule
  ],
  declarations: [AccountEditComponent],
  schemas: [ NO_ERRORS_SCHEMA ]

})
export class AccountEditModule { }
