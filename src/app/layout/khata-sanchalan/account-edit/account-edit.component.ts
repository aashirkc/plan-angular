import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
// import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
// import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { PlanningYearService, DateConverterService,ProgressReportingTippaniAadeshService,UnicodeTranslateService, KataSanchalanService,ToggleSidebarService, AgencyTypeService, PrintingService } from '../../../shared';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

@Component({
  selector: 'app-account-edit',
  templateUrl: './account-edit.component.html',
  styleUrls: ['./account-edit.component.scss']
})
export class AccountEditComponent implements OnInit {

  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  aboutAmountForm: FormGroup;
  transData: any;
  yearAdapter: Array<any> = [];
  activityAdapter: Array<any> = [];
  userData: any = {};
  SearchDatas: Array<any> = [];
  generalInfoDatas: Array<any> = [];
  foForm: FormGroup;
  date: any
  organizationMasterData: any;
  activityDatas:any;
  check:boolean=true;
  childType: any = [];
  agencyName: any;
  agencyType: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  // @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;
  @ViewChild('act') act: jqxComboBoxComponent;

  constructor(
    private fb: FormBuilder,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    @Inject('API_URL') API_URL: string,
    private cdr: ChangeDetectorRef,
    private pys: PlanningYearService,
    private ks: KataSanchalanService,
    private translate: TranslateService,
    private dcs: DateConverterService,
    private prtas: ProgressReportingTippaniAadeshService,
    private unicode: UnicodeTranslateService,
    private ts:ToggleSidebarService,
    private ats: AgencyTypeService,
    private printingService: PrintingService
  ) {
    this.createForm();
    // this.getTranslation();
  }

  // --- search bar emitter functions
  search = ({value, id}) => this.SearchData(value, id)

  changeLoadingStatus($event){
    !$event && this.jqxLoader.close();
    $event && this.jqxLoader.open();
  }

  showError($event){
    let messageDiv: any = document.getElementById('error');
    messageDiv.innerText = $event;
    this.errNotification.open();
  }
  // --- end

  ngOnInit() {
    this.ats.show(0).subscribe(
      result => {
        if (result['length'] > 0) {

          this.agencyType = result;
        }
      },
      error => {
        console.log(error);
      }
    );

     this.ts.currentMessage.subscribe((data) => {
      if(data=="on")
      this.check=false;
      if(data=="off")
      this.check=true;
      console.log(this.check);
    });
    this.prtas.getOrganizationName().subscribe((res) => {
      this.organizationMasterData = res;
    }, (error) => {
      console.info(error);

    });
    this.activityDatas=JSON.parse(localStorage.getItem('ActivityDatas'));
    this.pys.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.yearAdapter = [];
      } else {
        this.yearAdapter = res;
        if(this.activityDatas){
          this.aboutAmountForm.get('planYear').patchValue(this.activityDatas['planYear']);
          this.ChangePlanYear1(this.activityDatas['planYear']);
         }else{
        this.aboutAmountForm.get('planYear').patchValue(res[0]['yearCode']);
        this.ChangePlanYear(res[0]['yearCode']);
         }
        // this.aboutAmountForm.get('planYear').patchValue(res[0]['yearCode']);
        // this.ChangePlanYear(res[0]['yearCode']);
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }
  ChangePlanYear1(id){
    console.log(id);
    this.ks.indexActivity({planYear:id}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
        console.log(this.activityDatas['id']);
        setTimeout(() => {
          if(this.check){
            this.act.selectItem(this.activityDatas['id']);
            }
          this.SearchData(this.aboutAmountForm.value,this.activityDatas['id']);
        }, 100);

      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }
  ChangePlanYear(id){
    this.ks.indexActivity({planYear:id}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;

      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }
  createForm() {
    this.aboutAmountForm = this.fb.group({
      'planYear': ['', Validators.required],
      'child': [''],
      'parent': [''],
    });
    this.foForm = this.fb.group({
      'id': [''],
      'activity': [''],
      'patraSankhya': ['', Validators.required],
      'chalaniNo': [''],
      'date': [''],
      'address': [''],
      'bankAddress': [''],
      'officeAddress': [''],
      'accountNo': [''],
      'commettieeName': [''],
      'municipalityName': [''],
      'sachiv': [''],
      'adhyaxya': [''],
      'kosaadhyaxya': [''],
      'adhikrit': [''],
      'wardNo': [''],
      'adhikritPost': [''],
      'bankName': [''],
      'yojana': [''],
      'pradesh': [''],
      'lekhaSakha': [''],
      'prabhidikSakha': [''],
      'prabhidikSakha1': [''],
      // 'sanchalanBankName':['',Validators.required],
    });
  }

  SearchData(post,val) {
    // post['activity'] = this.act.val();
    this.foForm.get('adhikrit').patchValue(this.organizationMasterData['nameOfHead']);
    this.foForm.controls['adhikritPost'].patchValue(this.organizationMasterData['post']);
    post['activity'] = val;
    this.jqxLoader.open();
    if (val) {
      this.ks.indexAdhyaawadhik(post).subscribe((res) => {
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.SearchDatas = [];
        } else {
          this.SearchDatas = res;
          console.info(res[0]);
          this.foForm.controls['activity'].patchValue(val);
          this.foForm.controls['date'].patchValue(res[0].date || ' ');
          this.foForm.controls['patraSankhya'].patchValue(res[0].patraSankhya || ' ');
          this.foForm.controls['commettieeName'].patchValue(res[0].commettieeName || ' ');
          this.foForm.controls['yojana'].patchValue(res[0].commettieeName || ' ');
          this.foForm.controls['municipalityName'].patchValue(res[0].municipalityName || ' ');
          this.foForm.controls['adhyaxya'].patchValue(res[0].adhyaxya || ' ');
          this.foForm.controls['sachiv'].patchValue(res[0].sachiv || ' ');
          this.foForm.controls['wardNo'].patchValue(res[0].wardNo || ' ');
          this.foForm.controls['kosaadhyaxya'].patchValue(res[0].kosaadhyaxya || ' ');
          this.foForm.controls['address'].patchValue(res[0].address || ' ');
          this.foForm.controls['bankAddress'].patchValue(res[0].bankAddress || ' ');
          this.foForm.controls['officeAddress'].patchValue(
            `${this.organizationMasterData['placeName']}, ${this.organizationMasterData['district']}`);
          this.foForm.controls['accountNo'].patchValue(res[0].accountNo || ' ');
          this.foForm.controls['id'].patchValue(res[0].id || ' ');
          this.foForm.controls['chalaniNo'].patchValue(res[0].chalaniNo || ' ');
          this.foForm.controls['pradesh'].patchValue(res[0].pradesh || ' ');
          this.foForm.controls['bankName'].patchValue(res[0].sanchalanBankName || res[0].bankName || ' ');
          this.foForm.controls['prabhidikSakha'].patchValue('भौतिक पूर्वाधार विकास शाखा');
          this.foForm.controls['prabhidikSakha1'].patchValue('योजना शाखा');
          this.foForm.controls['lekhaSakha'].patchValue('आर्थिक प्रशासन शाखा');

        }
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
    } else {
      this.jqxLoader.close();
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'कृपया याेजना छान्नुहाेस्';
      this.errNotification.open();
    }
  }

  getActivity(data){
    this.ks.indexActivity(data).subscribe((res) => {
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.activityAdapter = [];
        } else {
          this.activityAdapter = res;
          setTimeout(() => {
            if (this.check) {
              this.act.selectItem(this.activityDatas['id']);
            }

            this.SearchData(this.aboutAmountForm.value, this.activityDatas['id']);
          }, 100);
        }
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
  }

  // ngAfterViewInit() {
  //   this.unicode.initUnicode();
  // }

  printData(post): void {
    const printContents = document.getElementById('printContent').innerHTML;
    this.printingService.printContents(printContents, 'खाता अध्यावधिक गरिदिने सम्बन्धमा', false, `
    @page{
      /* this affects the margin in the printer settings */
      margin: 24.4mm 24.4mm 24.4mm 37.1mm;
    }
    @media print{
      p{
        font-size: 23px !important;
      }
    }`)
    this.ks.storeAdhyaawadhik(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.aboutAmountForm.reset();
          this.SearchDatas = [];
        }
        this.jqxLoader.close();
        this.aboutAmountForm.get('planYear').patchValue(this.yearAdapter[0]['yearCode']);
        this.ChangePlanYear(this.yearAdapter[0]['yearCode']);
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }


  implementingAgencyChange(selectedEvent) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.agencyName = displayText;
      console.log(selectedEvent)
      let agencyCode = selectedEvent.target.value;
      this.getAgencyType(agencyCode);
      this.getActivity(this.aboutAmountForm.value)
    } else {
      this.childType = '';
    }
  }


  getAgencyType(agencyCode = 0, index = 0, type = null) {
    // remove previously added form control if the parent select is changed.
    this.ats.show(agencyCode).subscribe(
      result => {
        if (result['length'] > 0) {
          this.childType = result;
        } else {
          this.childType = '';
        }
      },
      error => {
        console.log(error);
      }
    );
  }

}
