import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KhataSanchalanComponent } from './khata-sanchalan.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { KhataSanchalanRoutingModule } from './khata-sanchalan-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    KhataSanchalanRoutingModule
  ],
  declarations: [KhataSanchalanComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class KhataSanchalanModule { }
