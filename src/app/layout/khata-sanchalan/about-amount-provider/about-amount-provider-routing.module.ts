import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutAmountProviderComponent } from './about-amount-provider.component';

const routes: Routes = [
  {
    path: '',
    component: AboutAmountProviderComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutAmountProviderRoutingModule { }