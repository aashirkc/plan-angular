import {
    Component,
    ViewChild,
    ViewContainerRef,
    Inject,
    ChangeDetectorRef,
    OnInit
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import {
    PlanningYearService,
    DateConverterService,
    UnicodeTranslateService,
    KataSanchalanService,
    PrintingService
} from '../../../shared';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

@Component({
    selector: 'app-about-amount-provider',
    templateUrl: './about-amount-provider.component.html',
    styleUrls: ['./about-amount-provider.component.scss']
})
export class AboutAmountProviderComponent implements OnInit {
    source: any;
    dataAdapter: any;
    columns: any[];
    editrow: number = -1;
    columngroups: any[];
    aboutAmountForm: FormGroup;
    transData: any;
    yearAdapter: Array<any> = [];
    activityAdapter: Array<any> = [];
    userData: any = {};
    SearchDatas: Array<any> = [];
    generalInfoDatas: Array<any> = [];
    foForm: FormGroup;
    date: any;

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
    // @ViewChild('myGrid') myGrid: jqxGridComponent;
    @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;
    @ViewChild('act') act: jqxComboBoxComponent;

    constructor(
        private fb: FormBuilder,
        @Inject('API_URL_DOC') API_URL_DOC: string,
        @Inject('API_URL') API_URL: string,
        private cdr: ChangeDetectorRef,
        private pys: PlanningYearService,
        private ks: KataSanchalanService,
        private translate: TranslateService,
        private dcs: DateConverterService,
        private unicode: UnicodeTranslateService,
        private printingService: PrintingService
    ) {
        this.createForm();
        // this.getTranslation();
    }

    ngOnInit() {
        this.pys.index({}).subscribe(
            res => {
                if (res.length == 1 && res[0].error) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = res[0].error;
                    this.errNotification.open();
                    this.yearAdapter = [];
                } else {
                    this.yearAdapter = res;
                }
                this.jqxLoader.close();
            },
            error => {
                console.info(error);
                this.jqxLoader.close();
            }
        );
    }
    ChangePlanYear(id) {
        this.ks.indexActivity({ planYear: id }).subscribe(
            res => {
                if (res.length == 1 && res[0].error) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = res[0].error;
                    this.errNotification.open();
                    this.activityAdapter = [];
                } else {
                    this.activityAdapter = res;
                    this.aboutAmountForm
                        .get('planYear')
                        .patchValue(res[0]['yearCode']);
                    this.ChangePlanYear(res[0]['yearCode']);
                }
                this.jqxLoader.close();
            },
            error => {
                console.info(error);
                this.jqxLoader.close();
            }
        );
    }

    createForm() {
        this.aboutAmountForm = this.fb.group({
            planYear: ['', Validators.required]
        });
        this.foForm = this.fb.group({
            patraSankhya: ['', Validators.required],
            chalaniNo: ['', Validators.required],
            date: ['', Validators.required],
            address: ['', Validators.required],
            municipalityName: ['', Validators.required],
            commettieeName: ['', Validators.required],
            sachiv: ['', Validators.required],
            adhyaxya: ['', Validators.required],
            kosaadhyaxya: ['', Validators.required]
        });
    }

    SearchData(post) {
        post['activity'] = this.act.val();
        this.jqxLoader.open();
        if (this.act.val()) {
            this.ks.indexKhata(post).subscribe(
                res => {
                    if (res.length == 1 && res[0].error) {
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = res[0].error;
                        this.errNotification.open();
                        this.SearchDatas = [];
                    } else {
                        this.SearchDatas = res;
                        this.foForm.controls['date'].patchValue(res[0].date);
                        this.foForm.controls['patraSankhya'].patchValue(
                            res[0].planYearName
                        );
                        this.foForm.controls['municipalityName'].patchValue(
                            res[0].nagarPalika
                        );
                        this.foForm.controls['commettieeName'].patchValue(
                            res[0].activityName
                        );
                        this.foForm.controls['adhyaxya'].patchValue(
                            res[0].adhaxya
                        );
                        this.foForm.controls['sachiv'].patchValue(
                            res[0].sachiv
                        );
                        this.foForm.controls['kosaadhyaxya'].patchValue(
                            res[0].koshadaxya
                        );
                        this.foForm.controls['address'].patchValue(
                            res[0].address
                        );
                    }
                    this.jqxLoader.close();
                },
                error => {
                    console.info(error);
                    this.jqxLoader.close();
                }
            );
        }
    }

    // ngAfterViewInit() {
    //     this.unicode.initUnicode();
    // }

    printData(post): void {
        const printContents = document.getElementById('printContent').innerHTML;
        this.printingService.printContents(
            printContents,
            'खाता खोलि आर्थिक कारोबार संचालन गरिदिने सम्वन्धमा'
        );
    }
}
