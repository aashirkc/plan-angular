import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutAmountProviderComponent } from './about-amount-provider.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AboutAmountProviderRoutingModule } from './about-amount-provider-routing.module';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AboutAmountProviderRoutingModule
  ],
  declarations: [AboutAmountProviderComponent],
  schemas:[NO_ERRORS_SCHEMA]
})
export class AboutAmountProviderModule { }
