import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KhataSanchalanComponent } from './khata-sanchalan.component';

const routes: Routes = [
    {
        path: '',
        component: KhataSanchalanComponent,
        children: [
            { path: '', redirectTo: 'about-financial-operation', pathMatch: 'full' },
            { path: 'about-financial-operation', loadChildren: './about-financial-operation/about-financial-operation.module#AboutFinancialOperationModule' },
            { path: 'account-edit', loadChildren: './account-edit/account-edit.module#AccountEditModule' },       
            { path: 'name-change', loadChildren: './name-change/name-change.module#NameChangeModule' },       
            { path: 'farfaarak', loadChildren: './farfaarak/farfaarak.module#FarfaarakModule' },       
            { path: 'about-account-close', loadChildren: './about-account-close/about-account-close.module#AboutAccountCloseModule' },       
            { path: 'about-amount-provider', loadChildren: './about-amount-provider/about-amount-provider.module#AboutAmountProviderModule' },       
      
        ]
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class KhataSanchalanRoutingModule { }
