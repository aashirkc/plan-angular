import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutFinancialOperationComponent } from './about-financial-operation.component';

const routes: Routes = [
  {
    path: '',
    component: AboutFinancialOperationComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutFinancialOperationRoutingModule { }
