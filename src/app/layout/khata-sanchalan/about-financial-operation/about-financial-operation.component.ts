import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
// import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
// import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { PlanningYearService, ProgressReportingTippaniAadeshService, UnicodeTranslateService, KataSanchalanService, DateConverterService, NepaliInputComponent, ToggleSidebarService, AgencyTypeService, PrintingService } from '../../../shared';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

@Component({
  selector: 'app-about-financial-operation',
  templateUrl: './about-financial-operation.component.html',
  styleUrls: ['./about-financial-operation.component.scss']
})
export class AboutFinancialOperationComponent implements OnInit {

  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  heading: Array<any> = [];
  result: any;
  columngroups: any[];
  aboutFinancialOperationForm: FormGroup;
  foForm: FormGroup;
  transData: any;
  yearAdapter: Array<any> = [];
  activityAdapter: Array<any> = [];
  userData: any = {};
  SearchDatas: Array<any> = [];
  date: any
  fullDate: any;
  organizationMasterData: any;
  activityDatas: any;
  check: boolean = true;
  childType: any = [];
  agencyName: any;
  agencyType: any;
  lang: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;
  @ViewChild('act') act: jqxComboBoxComponent;
  isRul: boolean;
  distType: any;

  constructor(
    private fb: FormBuilder,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    @Inject('API_URL') API_URL: string,
    private cdr: ChangeDetectorRef,
    private pys: PlanningYearService,
    private ks: KataSanchalanService,
    private translate: TranslateService,
    private dcs: DateConverterService,
    private unicode: UnicodeTranslateService,
    private prtas: ProgressReportingTippaniAadeshService,
    private ts: ToggleSidebarService,
    private ats: AgencyTypeService,
    private printingService: PrintingService,
    @Inject('DIST_TYPE') distType

  ) {
    this.distType = distType;
    this.createForm();
    // this.getTranslation();
  }
  OfficeAddress: string = '';
  // lekhaSakhaAddress:string = '';
  // prabhidikSakhaAddress:string = '';
  // prabhidikSakha1Address:string = '';
  // ccAddress:string = '';


  // --- search bar emitter functions
  search = ({ value, id }) => this.SearchData(value, id)

  changeLoadingStatus($event) {
    !$event && this.jqxLoader.close();
    $event && this.jqxLoader.open();
  }

  showError($event) {
    let messageDiv: any = document.getElementById('error');
    messageDiv.innerText = $event;
    this.errNotification.open();
  }
  // --- end

  // when sidebar is opened
  searchWithActivityDatas = () => {
    try {
      const activityDatas = JSON.parse(localStorage.getItem("ActivityDatas"));
      if (activityDatas) {
        const requestParams = {
          planYear: activityDatas.planYear,
          activity: activityDatas.id,
          activityType: 3
        }
        this.prtas.getOrganizationName().subscribe((res) => {
          this.organizationMasterData = res;
          const data = { value: requestParams, id: activityDatas.id }
          console.log(data);
          this.search(data);
        });
      }
    } catch (e) {
      // debugger;
      console.warn(e);
    }
  }
  // --- end

  ngOnInit() {

    this.isRul = false;
    const orgMaster = JSON.parse(localStorage.getItem('org'));
    if (orgMaster && orgMaster[0]) {
      const orgType = orgMaster[0]['orgType'];
      this.isRul = orgType === 'RUL';
    }

    this.lang = this.translate.currentLang.substr(0, 2);

    this.ats.show(0).subscribe(
      result => {
        if (result['length'] > 0) {

          this.agencyType = result;
        }
      },
      error => {
        console.log(error);
      }
    );

    this.ts.currentMessage.subscribe((data) => {
      if (data == "on")
        this.check = false;
      this.searchWithActivityDatas();
      if (data == "off")
        this.check = true;
      console.log(this.check);
    });
    this.prtas.getOrganizationName().subscribe((res) => {
      this.organizationMasterData = res;
      this.OfficeAddress = `${res && res['placeName']}, ${res && res['district']}`;
    }, (error) => {
      console.info(error);

    });

  }

  //   static values for consumer committee
  ChangePlanYear1(data) {
    data['activityType'] = 3;
    this.ks.indexActivity1({ data }).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
        console.log(this.activityDatas);
        console.log(this.activityDatas['id']);
        setTimeout(() => {
          if (this.check) {
            this.act.selectItem(this.activityDatas['id']);
          }

          this.SearchData(this.aboutFinancialOperationForm.value, this.activityDatas['id']);
        }, 100);

      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }

  ChangePlanYear(id) {
    let searchdata = {}
    searchdata = this.aboutFinancialOperationForm.value;
    this.getActivity(searchdata)
  }

  getActivity(data) {

    // always search for upabhokta comittee
    // static here change if to make dynamic activity type
    data['activityType'] = 3;
    this.ks.indexActivity(data).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
        setTimeout(() => {
          if (this.check) {
            this.activityDatas && this.act.selectItem(this.activityDatas['id']);
          }

          this.activityDatas && this.SearchData(this.aboutFinancialOperationForm.value, this.activityDatas['id']);
        }, 100);
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  createForm() {
    this.aboutFinancialOperationForm = this.fb.group({
      'planYear': ['', Validators.required],
      'child': [''],
      'parent': [''],
    });
    this.foForm = this.fb.group({
      'id': [''],
      'activity': [''],
      'patraSankhya': ['', Validators.required],
      'chalaniNo': [''],
      'orgChalani': [''],
      'date': ['', Validators.required],
      'address': [''],
      'bankAddress': [''],
      'wardNo': [''],
      'focusCode': [''],
      'bankName': [''],
      'adhikrit': [''],
      'adhikritPost': [''],
      'yojana': [''],
      'municipalityName': ['', Validators.required],
      'commettieeName': [''],
      'sachiv': ['', Validators.required],
      'adhyaxya': ['', Validators.required],
      'kosaadhyaxya': ['', Validators.required],
      'pradesh': ['', Validators.required],
      'consumerCommettieeName': ['', Validators.required],
      'consumerCommettieeAddress': ['', Validators.required],
      'lekhaSakha': [''],
      'prabhidikSakha': [''],
      'prabhidikSakha1': [''],
      'ccNameAddress': [''],
      'ccAddress': [''],
      'engineer1': [''],
      'engineer2': [''],
      'engineer3': [''],
      'engineer4': [''],
    });
  }


  SearchData(post, val) {
    // post['activity'] = this.act.val();
    post['activity'] = val;
    post['activityType'] = 3;
    this.foForm.get('adhikrit').patchValue(this.organizationMasterData['nameOfHead']);
    this.foForm.controls['adhikritPost'].patchValue(this.organizationMasterData['post']);
    this.jqxLoader.open();
    // if (this.act.val()) {

    if (val) {
      this.ks.indexKhata(post).subscribe((res) => {
        if (res && res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.SearchDatas = [];
        } else {
          this.SearchDatas = res;
          this.foForm.controls['date'].patchValue(res[0].date || ' ');
          this.foForm.controls['yojana'].patchValue(res[0].commettieeName || ' ');
          this.foForm.controls['patraSankhya'].patchValue(res[0].patraSankhya || ' ');
          //   this.foForm.controls['municipalityName'].patchValue('अग्नीसाइर कृष्णासवरण गाउँपालिका' || ' ');
          this.foForm.controls['commettieeName'].patchValue(res[0].commettieeName || ' ');
          this.foForm.controls['adhyaxya'].patchValue(res[0].adhyaxya || ' ');
          this.foForm.controls['sachiv'].patchValue(res[0].sachiv || ' ');
          this.foForm.controls['kosaadhyaxya'].patchValue(res[0].kosaadhyaxya || ' ');
          this.foForm.controls['address'].patchValue(`${this.OfficeAddress}` || ' ');
          this.foForm.controls['wardNo'].patchValue(res[0].wardNo || 'नगर स्तरीय');
          this.foForm.controls['focusCode'].patchValue('पुर्वाधार विकास');
          this.foForm.controls['consumerCommettieeAddress'].patchValue(res[0].consumerCommettieeAddress || ' ');
          this.foForm.controls['consumerCommettieeName'].patchValue(res[0].consumerCommettieeName || ' ');
          this.foForm.controls['bankName'].patchValue(res[0].bankName || ' ');
          this.foForm.controls['bankAddress'].patchValue(res[0].bankAddress ||res[0].address || ' ');
          this.foForm.controls['id'].patchValue(res[0].id || ' ');
          this.foForm.controls['chalaniNo'].patchValue(res[0].chalaniNo || ' ');
          this.foForm.controls['orgChalani'].patchValue(res[0].orgChalani || ' ');
          this.foForm.controls['pradesh'].patchValue(res[0].pradesh || ' ');
          this.foForm.controls['ccNameAddress'].patchValue(`${res[0].consumerCommettieeName}` || ' ');
          this.foForm.controls['prabhidikSakha'].patchValue('भौतिक पूर्वाधार विकास शाखा');
          this.foForm.controls['prabhidikSakha1'].patchValue('योजना शाखा');
          this.foForm.controls['lekhaSakha'].patchValue('आर्थिक प्रशासन शाखा');
          // this.foForm.controls['prabhidikSakha'].patchValue(`${this.prabhidikSakhaAddress}`);
          // this.foForm.controls['prabhidikSakha1'].patchValue(`${this.prabhidikSakha1Address}`);
          // this.foForm.controls['lekhaSakha'].patchValue(`${this.lekhaSakhaAddress}`);

        }
        this.foForm.controls['activity'].patchValue(val)
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
    } else {
      this.jqxLoader.close();
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'कृपया याेजना छान्नुहाेस्';
      this.errNotification.open();
    }
  }

  // ngAfterViewInit() {
  //   this.unicode.initUnicode();
  // }

  printData(post): void {
    const printContents = document.getElementById('printContent').innerHTML;
    this.printingService.printContents(printContents, 'खाता खोलि आर्थिक कारोबार संचालन गरिदिने सम्वन्धमा', false, `
    @page{
      /* this affects the margin in the printer settings */
      margin: 24.4mm 24.4mm 24.4mm 37.1mm;
    }
    @media print{
      p{
        font-size: 23px !important;
      }
    }
    `);
    this.ks.storeFinancial(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.aboutFinancialOperationForm.reset();
          // this.SearchDatas = [];
        }
        this.jqxLoader.close();
        // if(this.yearAdapter[0]){
        //   this.aboutFinancialOperationForm.get('planYear').patchValue(this.yearAdapter[0]['yearCode']);
        //   this.ChangePlanYear(this.yearAdapter[0]['yearCode']);
        // }
        // if (result['error']) {
        //   let messageDiv: any = document.getElementById('error');
        //   messageDiv.innerText = result['error']['message'];
        //   this.errNotification.open();
        // }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }


  agencyNameChange($event) {
    debugger;
  }

  printButtonDisabled: boolean = true;
  implementingAgencyChange(selectedEvent) {

    // debugger;
    // // to disable yojana select field
    // const disabledPlans = selectedEvent.target.value == '';
    // disabledPlans && this.aboutFinancialOperationForm.get('child').disable();
    // !disabledPlans && this.aboutFinancialOperationForm.get('child').enable();


    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.agencyName = displayText;
      console.log(selectedEvent)
      let agencyCode = selectedEvent.target.value;
      this.getAgencyType(agencyCode);
      this.getActivity(this.aboutFinancialOperationForm.value)
    } else {
      this.childType = '';
    }
  }


  getAgencyType(agencyCode = 0, index = 0, type = null) {
    // remove previously added form control if the parent select is changed.
    this.ats.show(agencyCode).subscribe(
      result => {
        if (result['length'] > 0) {
          this.childType = result;
        } else {
          this.childType = '';
        }
      },
      error => {
        console.log(error);
      }
    );
  }

}
