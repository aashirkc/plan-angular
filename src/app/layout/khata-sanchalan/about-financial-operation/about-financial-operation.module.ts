import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutFinancialOperationComponent } from './about-financial-operation.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AboutFinancialOperationRoutingModule } from './about-financial-operation-routing.module';
import { AnusuchiSearchBarModule } from 'app/layout/report/anusuchi-search-bar/anusuchi-search-bar.module';
import { FormHelperModule } from 'app/shared/form-helper/form-helper.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormHelperModule,
    AnusuchiSearchBarModule,
    AboutFinancialOperationRoutingModule,
  ],
  declarations: [AboutFinancialOperationComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AboutFinancialOperationModule { }
