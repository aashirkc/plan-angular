import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FarfaarakComponent } from './farfaarak.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { FarfaarakRoutingModule } from './farfaarak-routing.module';
import { AnusuchiSearchBarModule } from 'app/layout/report/anusuchi-search-bar/anusuchi-search-bar.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AnusuchiSearchBarModule,
    FarfaarakRoutingModule
  ],
  declarations: [FarfaarakComponent],
  schemas:[NO_ERRORS_SCHEMA]
})
export class FarfaarakModule { }
