import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,FormArray, Validators } from '@angular/forms';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { TranslateService } from '@ngx-translate/core';
import { PlanningYearService,ToggleSidebarService,ProgressReportingTippaniAadeshService, AgencyTypeService, UnicodeTranslateService, KataSanchalanService, DateConverterService, PrintingService, NepaliNumberToWordService } from '../../../shared';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

@Component({
  selector: 'app-farfaarak',
  templateUrl: './farfaarak.component.html',
  styleUrls: ['./farfaarak.component.scss']
})
export class FarfaarakComponent implements OnInit {

  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  farfaarakForm: FormGroup;
  foForm: FormGroup;
  transData: any;
  yearAdapter: Array<any> = [];
  activityAdapter: Array<any> = [];
  userData: any = {};
  SearchDatas: Array<any> = [];
  date: any;
  organizationMasterData: any;
  activityDatas:any;
  check:boolean=true;
  agencyName: any;
  agencyType: any;
  lang: any;
  childType: any= [];


  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  // @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;
  @ViewChild('act') act: jqxComboBoxComponent;
  distType: any;

  constructor(
    private fb: FormBuilder,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    @Inject('API_URL') API_URL: string,
    @Inject('DIST_TYPE') distType,
    private cdr: ChangeDetectorRef,
    private pys: PlanningYearService,
    private ks: KataSanchalanService,
    private translate: TranslateService,
    private ntw: NepaliNumberToWordService,
    private dcs: DateConverterService,
    private prtas: ProgressReportingTippaniAadeshService,
    private unicode: UnicodeTranslateService,
    private ts:ToggleSidebarService,
    private ats: AgencyTypeService,
    private printingService: PrintingService

  ) {
    this.distType = distType;
    this.createForm();
    // this.getTranslation();
  }

  // --- search bar emitter functions
  search = ({value, id}) => this.SearchData(value, id)

  changeLoadingStatus($event){
    !$event && this.jqxLoader.close();
    $event && this.jqxLoader.open();
  }

  showError($event){
    let messageDiv: any = document.getElementById('error');
    messageDiv.innerText = $event;
    this.errNotification.open();
  }
  // --- end

  ngOnInit() {
    // this.foForm.get('adhikrit').patchValue('उमेश कुमार यादव');
    // this.foForm.controls['adhikritPost'].patchValue('प्रमुख प्रशासकीय अधिकृत');
    this.lang = this.translate.currentLang.substr(0,2);
    this.ats.show(0).subscribe(
        result => {
          if (result['length'] > 0) {

            this.agencyType = result;
          }
        },
        error => {
          console.log(error);
        }
      );
    this.ts.currentMessage.subscribe((data) => {
      if(data=="on")
      this.check=false;
      if(data=="off")
      this.check=true;
      console.log(this.check);
    });
    this.prtas.getOrganizationName().subscribe((res) => {
      this.organizationMasterData = res;
    }, (error) => {
      console.info(error);

    });
    this.activityDatas=JSON.parse(localStorage.getItem('ActivityDatas'));
    this.pys.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.yearAdapter = [];
      } else {
        this.yearAdapter = res;
        if(this.activityDatas){
          this.farfaarakForm.get('planYear').patchValue(this.activityDatas['planYear']);
          this.ChangePlanYear1(this.activityDatas['planYear']);
         }else{
        this.farfaarakForm.get('planYear').patchValue(res[0]['yearCode']);
        this.ChangePlanYear(res[0]['yearCode']);
         }
        // this.farfaarakForm.get('planYear').patchValue(res[0]['yearCode']);
        // this.ChangePlanYear(res[0]['yearCode']);
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  // getTranslation() {
  //   this.translate.get(['SN', 'regNo', 'universityRoll', 'ADD', 'EDIT', 'NAME', 'nepali', 'english', 'mobile', 'VIEW', 'DETAIL', "ACTIONS", "UPDATE", "DELETESELECTED", "RELOAD"]).subscribe((translation: [string]) => {
  //     this.transData = translation;
  //     this.loadGrid();
  //   });
  // }
  ChangePlanYear1(id){
    console.log(id);
    this.ks.indexActivity({planYear:id}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
        console.log("Line 135"+this.activityDatas['id']);
        setTimeout(() => {
          if(this.check){
            this.act.selectItem(this.activityDatas['id']);
            }
          this.SearchData(this.farfaarakForm.value,this.activityDatas['id']);
        }, 100);

      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }


  ChangePlanYear(id){
    this.ks.indexActivity({planYear:id}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }

  getActivity(data){
    this.ks.indexActivity(data).subscribe((res) => {
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.activityAdapter = [];
        } else {
          this.activityAdapter = res;
          console.log("Line 135"+this.activityDatas);
          setTimeout(() => {
            if (this.check) {
              this.act.selectItem(this.activityDatas['id']);
            }

            this.SearchData(this.farfaarakForm.value, this.activityDatas['id']);
          }, 100);
        }
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
  }

  createForm() {
    this.farfaarakForm = this.fb.group({
      'planYear': ['', Validators.required],
      'child': [''],
      'parent': [''],
    });
    this.foForm = this.fb.group({
      'id': [null, Validators.required],
      'activity': [''],
      'planYear': ['', Validators.required],
      'patraSankhya': ['', Validators.required],
      'chalaniNo': [''],
      'percent': [''],
      'farFarakAmt': [''],
      'farFarakAmtWord': [0],
      'date': ['', Validators.required],
      'municipalityName': ['', Validators.required],
      'commettieeName': ['', Validators.required],
      'wardNo': ['', Validators.required],
      'address': ['', Validators.required],
      'meetingDate': ['', Validators.required],
      'activityDate': ['', Validators.required],
      'activityName': [''],
      'adhyaxya': ['', Validators.required],
      'totalAmount': [null, Validators.required],
      'fromNepalSarkar': [null, Validators.required],
      'fromConsumerCommittee': [null, Validators.required],
      'fromWada': [null, Validators.required],
      'fromLocalLevel': [null, Validators.required],
      'fromState': [null, Validators.required],
      'fromUnion': [null, Validators.required],
      'fromOther': [null, Validators.required],
      'adhikrit': [''],
      'adhikritPost': [''],
      'yojana': [''],
      'pradesh': [''],
      'bodarthaDetails': this.fb.array([
        this.initbodarthaDetails(),
      ]),
    });

  }
  initbodarthaDetails() {
    return this.fb.group({
      bodartha: [''],
    });
  }
  digitWord(value, ctrlName: string): void {
    this.foForm.get(ctrlName).patchValue(this.ntw.getNepaliWord(value));
  }
  addItem() {
    const control1 = <FormArray>this.foForm.controls['bodarthaDetails'];
    control1.push(this.initbodarthaDetails());
    // add unicode support for dynamically added input
    setTimeout(() => {
      this.unicode.initUnicode();
    }, 200);
  }
  removeItem(i: number) {
    const control1 = <FormArray>this.foForm.controls['bodarthaDetails'];
    control1.removeAt(i);
    //this.itemAdapter.splice(i, 1);
  }
  SearchData(post,val) {
    this.foForm.get('adhikrit').patchValue(this.organizationMasterData['nameOfHead']);
    this.foForm.controls['adhikritPost'].patchValue(this.organizationMasterData['post']);
    post['activity'] = val
    this.jqxLoader.open();
    if (val) {
      this.ks.indexFarfaarak(post).subscribe((res) => {
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.SearchDatas = [];
        } else {
          this.SearchDatas = res;

          this.date = res[0].date;

          console.log(res[0]);

          // if(res[0] && res[0].id){
          //   this.foForm.get('id').patchValue(res[0].id);
          //   this.date = res[0].date;
          // }else{
          //   let cdate = this.dcs.ad2Bs(res[0].currentDate);
          //   this.date = cdate['fulldate'];
          // }
          //
          //
          this.foForm.get('activity').patchValue(val);
          this.foForm.get('date').patchValue(this.date || ' ');
          this.foForm.get('planYear').patchValue(res[0].planYear || ' ');
          this.foForm.get('percent').patchValue(res[0].planYear || '10');
          this.foForm.get('patraSankhya').patchValue(res[0].patraSankhya || ' ');
          this.foForm.get('chalaniNo').patchValue(res[0].chalaniNo || null || ' ');
          this.foForm.get('wardNo').patchValue(res[0].wardNo || 'नगर स्तरीय');
          this.foForm.get('municipalityName').patchValue(res[0].municipalityName || null || ' ');
          this.foForm.get('address').patchValue(res[0].address || ' ');
          this.foForm.get('adhyaxya').patchValue(res[0].adhyaxya || ' ');
          this.foForm.get('commettieeName').patchValue(res[0].commettieeName || ' ');
          this.foForm.get('totalAmount').patchValue(res[0].totalAmount || ' ');
          this.foForm.get('meetingDate').patchValue(res[0].meetingDate || ' ');
          this.foForm.get('activityName').patchValue(res[0].activityName || ' ');
          this.foForm.get('activityDate').patchValue(res[0].activityDate || ' ');
          this.foForm.get('fromNepalSarkar').patchValue(res[0].fromNepalSarkar || null || ' ');
          this.foForm.get('fromConsumerCommittee').patchValue(res[0].fromConsumerCommittee || ' ');
          this.foForm.get('fromWada').patchValue(res[0].fromWada || null || ' ');
          this.foForm.get('fromLocalLevel').patchValue(res[0].fromLocalLevel || null || ' ');
          this.foForm.get('fromState').patchValue(res[0].fromState || null || ' ');
          this.foForm.get('fromUnion').patchValue(res[0].fromUnion || null || ' ');
          this.foForm.get('fromOther').patchValue(res[0].fromOther || null || ' ');

          // console.log(res[0].fromConsumerCommittee+"check")
          // this.foForm.get('sachiv').patchValue(res[0].sachiv);
          // this.foForm.get('kosaadhyaxya').patchValue(res[0].kosaadhyaxya);
          // this.foForm.get('commettieeName').patchValue(res[0].commettieeName);
          //
          //
        }
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
    } else {
      this.jqxLoader.close();
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'कृपया याेजना छान्नुहाेस्';
      this.errNotification.open();
    }
  }

  // ngAfterViewInit() {
  //   this.unicode.initUnicode();
  // }

  printData(post): void {
    this.ks.storeFarfaarak(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          // this.aboutFinancialOperationForm.reset();
          this.SearchDatas = [];
        }
        this.jqxLoader.close();
        this.farfaarakForm.get('planYear').patchValue(this.yearAdapter[0]['yearCode']);
        this.ChangePlanYear(this.yearAdapter[0]['yearCode']);
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );

    const printContents = document.getElementById('printContent').innerHTML;
    this.printingService.printContents(printContents, 'फरफारक दिईएको सम्वन्धमा', false, `
    @page{
      /* this affects the margin in the printer settings */
      margin: 24.4mm 24.4mm 24.4mm 37.1mm;
    }
    @media print{
      p{
        font-size: 23px !important;
      }
    }`);
  }

  implementingAgencyChange(selectedEvent) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.agencyName = displayText;
      console.log(selectedEvent)
      let agencyCode = selectedEvent.target.value;
      this.getAgencyType(agencyCode);
      this.getActivity(this.farfaarakForm.value)
    } else {
      this.childType = '';
    }
  }


  getAgencyType(agencyCode = 0, index = 0, type = null) {
    // remove previously added form control if the parent select is changed.
    this.ats.show(agencyCode).subscribe(
      result => {
        if (result['length'] > 0) {
          this.childType = result;
        } else {
          this.childType = '';
        }
      },
      error => {
        console.log(error);
      }
    );
  }

}


