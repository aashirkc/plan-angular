import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FarfaarakComponent } from './farfaarak.component';

const routes: Routes = [
  {
    path: '',
    component: FarfaarakComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FarfaarakRoutingModule { }