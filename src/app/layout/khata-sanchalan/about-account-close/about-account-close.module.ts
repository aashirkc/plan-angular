import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutAccountCloseComponent } from './about-account-close.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AboutAccountCloseRoutingModule } from './about-account-close-routing.module';
import { AnusuchiSearchBarModule } from 'app/layout/report/anusuchi-search-bar/anusuchi-search-bar.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AnusuchiSearchBarModule,
    AboutAccountCloseRoutingModule
  ],
  declarations: [AboutAccountCloseComponent],
  schemas:[NO_ERRORS_SCHEMA]
})
export class AboutAccountCloseModule { }
