import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutAccountCloseComponent } from './about-account-close.component';

const routes: Routes = [
  {
    path: '',
    component: AboutAccountCloseComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutAccountCloseRoutingModule { }