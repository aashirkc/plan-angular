import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { TranslateService } from '@ngx-translate/core';
import { PlanningYearService,ProgressReportingTippaniAadeshService, UnicodeTranslateService, KataSanchalanService, DateConverterService,ToggleSidebarService,AgencyTypeService, PrintingService} from '../../../shared';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

@Component({
  selector: 'app-about-account-close',
  templateUrl: './about-account-close.component.html',
  styleUrls: ['./about-account-close.component.scss']
})
export class AboutAccountCloseComponent implements OnInit {

  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  aboutAccountCloseForm: FormGroup;
  foForm: FormGroup;
  transData: any;
  yearAdapter: Array<any> = [];
  activityAdapter: Array<any> = [];
  userData: any = {};
  SearchDatas: Array<any> = [];
  date: any;
  organizationMasterData: any;
  activityDatas:any;
  check:boolean=true;
  childType: any = [];
  agencyName: any;
  agencyType: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  // @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;
  @ViewChild('act') act: jqxComboBoxComponent;

  constructor(
    private fb: FormBuilder,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    @Inject('API_URL') API_URL: string,
    private cdr: ChangeDetectorRef,
    private pys: PlanningYearService,
    private ks: KataSanchalanService,
    private translate: TranslateService,
    private dcs: DateConverterService,
    private prtas: ProgressReportingTippaniAadeshService,
    private unicode: UnicodeTranslateService,
    private ts:ToggleSidebarService,
    private printingService: PrintingService,
    private ats: AgencyTypeService,

  ) {
    this.createForm();
    // this.getTranslation();
  }


  // --- search bar emitter functions
  search = ({value, id}) => this.SearchData(value, id)

  changeLoadingStatus($event){
    !$event && this.jqxLoader.close();
    $event && this.jqxLoader.open();
  }

  showError($event){
    let messageDiv: any = document.getElementById('error');
    messageDiv.innerText = $event;
    this.errNotification.open();
  }
  // --- end

  ngOnInit() {

    this.ats.show(0).subscribe(
      result => {
        if (result['length'] > 0) {

          this.agencyType = result;
        }
      },
      error => {
        console.log(error);
      }
    );


    this.ts.currentMessage.subscribe((data) => {
      if(data=="on")
      this.check=false;
      if(data=="off")
      this.check=true;
      console.log(this.check);
    });
    this.prtas.getOrganizationName().subscribe((res) => {
      this.organizationMasterData = res;
    }, (error) => {
      console.info(error);

    });
    this.activityDatas=JSON.parse(localStorage.getItem('ActivityDatas'));
    this.pys.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.yearAdapter = [];
      } else {
        this.yearAdapter = res;
        if(this.activityDatas){
          this.aboutAccountCloseForm.get('planYear').patchValue(this.activityDatas['planYear']);
          this.ChangePlanYear1(this.activityDatas['planYear']);
         }else{
        this.aboutAccountCloseForm.get('planYear').patchValue(res[0]['yearCode']);
        this.ChangePlanYear(res[0]['yearCode']);
         }
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
  ChangePlanYear1(id){
    console.log(id);
    this.ks.indexActivity({planYear:id, activityType: 3}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
        console.log(this.activityDatas['id']);
        setTimeout(() => {
          if(this.check){
            this.act.selectItem(this.activityDatas['id']);
            }

          this.SearchData(this.aboutAccountCloseForm.value,this.activityDatas['id']);
        }, 100);

      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }
  ChangePlanYear(id){

    this.ks.indexActivity({planYear:id, activityType: 3}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }
  // getTranslation() {
  //   this.translate.get(['SN', 'regNo', 'universityRoll', 'ADD', 'EDIT', 'NAME', 'nepali', 'english', 'mobile', 'VIEW', 'DETAIL', "ACTIONS", "UPDATE", "DELETESELECTED", "RELOAD"]).subscribe((translation: [string]) => {
  //     this.transData = translation;
  //     this.loadGrid();
  //   });
  // }


  createForm() {
    this.aboutAccountCloseForm = this.fb.group({
      'planYear': ['', Validators.required],
      'child': [''],
      'parent': [''],
    });
    this.foForm = this.fb.group({
      'id': [null, Validators.required],
      'planYear': ['', Validators.required],
      'patraSankhya': ['', Validators.required],
      'chalaniNo': [''],
      'date': ['', Validators.required],
      'wadaNo': ['', Validators.required],
      'accountNo': ['', Validators.required],
      'activityAddress': ['', Validators.required],
      'bankAddress': [''],
      'commettieeName': ['', Validators.required],
      'municipalityName': ['', Validators.required],
      'address': ['', Validators.required],
      'bankName': ['', Validators.required],
      'pradesh': ['', Validators.required],
      'adhikrit': [''],
      'adhikritPost':[''],
      'nirnayaDate': [''],
      'sifarisDate': [''],
      'nibedanDate': [''],
      'engineer1':[''],
      'engineer2':[''],
      'engineer3':[''],
      'lekhaSakha': [''],
      'prabhidikSakha': [''],
      'prabhidikSakha1': [''],
      'activity': ['']
      // 'sanchalanBankName':['',Validators.required],
    });
  }

  SearchData(post,val) {
    post['activity'] = val;
    post['activityType'] = 3;
    this.foForm.get('adhikrit').patchValue(this.organizationMasterData['nameOfHead']);
    this.foForm.controls['adhikritPost'].patchValue(this.organizationMasterData['post']);
    this.jqxLoader.open();
    if (val) {
      this.ks.indexKhataBanda(post).subscribe((res) => {
        if (res && res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.SearchDatas = [];
        } else if(res) {
          this.SearchDatas = res || [];
          // console.log(res[0])
          this.foForm.patchValue({
            id: val,
            activity: val,
            date: res[0].date,
            planYear: res[0].planYear,
            patraSankhya: res[0].planYearName || res[0].patraSankhya,
            'chalaniNo': res[0].chalaniNo || ' ',
            'wadaNo': res[0].wadaNo || ' ',
            'accountNo': res[0].accountNo || ' ',
            'commettieeName': res[0].commettieeName || ' ',
            'municipalityName':  res[0].municipalityName || ' ',
            // 'date': res[0].date,
            // 'planYear': res[0].planYear,
            // 'patraSankhya': res[0].planYearName || res[0].patraSankhya,
            'activityAddress': res[0].activityAddress || ' ',
            'bankAddress': res[0].bankAddress || ' ',
            // 'lekhaSakha': res[0].lekhaSakha || ' ',
            // 'prabhidikSakha': res[0].prabhidikSakha || ' ',
            'nirnayaDate': res[0].nirnayaDate || ' ',
            'sifarisDate': res[0].sifarisDate || ' ',
            'nibedanDate': res[0].nibedanDate || ' ',
            'pradesh': res[0].pradesh  || ' ',
            'bankName': res[0].sanchalanBankName || res[0].bankName || ' ',
            'engineer3': '३. ई. श्री',
            'prabhidikSakha': 'भौतिक पूर्वाधार विकास शाखा',
            'prabhidikSakha1': 'योजना शाखा',
            'lekhaSakha': 'आर्थिक प्रशासन शाखा',
          })
          // this.foForm.get('address').patchValue(res[0].address || ' ');
          // this.foForm.get('adhikrit').patchValue(res[0].adhikrit);
          this.foForm.controls['engineer1'].patchValue('१. श्री लेखा शाखा कमला न.पा कार्यालय:– जानकारीको  लागी ।');
          if(res[0]['wadaNo']){
            this.foForm.controls['engineer2'].patchValue(`२. श्री वडा कार्यालय ${res[0]['wadaNo']}– जानकारीका लागी ।`);
          }
          // this.foForm.get('sanchalanBankName').patchValue(res[0].sanchalanBankName);
        }
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
      this.ks.indexKhata(post).subscribe((res) => {
        this.foForm.get('address').patchValue(res[0].address);
        });
    }
    else {
      this.jqxLoader.close();
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'कृपया याेजना छान्नुहाेस्';
      this.errNotification.open();
    }
  }

  // ngAfterViewInit() {
  //   this.unicode.initUnicode();
  // }

  printData(post): void {
    // if(this.check)
    // post['activity'] = this.act.val();
    // else
    // post['activity'] =this.activityDatas['id'];

    const printContents = document.getElementById('printContent').innerHTML;
    this.printingService.printContents(printContents, 'वाँकी रकम निकाशा गरि खाता बन्द गरिदिने सम्वन्धमा', false, `
    @page{
      /* this affects the margin in the printer settings */
      margin: 24.4mm 24.4mm 24.4mm 37.1mm;
    }
    @media print{
      p{
        font-size: 23px !important;
      }
    }
    `);
    this.ks.storeKhataBanda(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          // this.aboutFinancialOperationForm.reset();
          // this.SearchDatas = [];
        }
        this.jqxLoader.close();
        this.aboutAccountCloseForm.get('planYear').patchValue(this.yearAdapter[0]['yearCode']);
        this.ChangePlanYear(this.yearAdapter[0]['yearCode']);
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );

  }

  getActivity(data){
    this.ks.indexActivity(data).subscribe((res) => {
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.activityAdapter = [];
        } else {
          this.activityAdapter = res;
          setTimeout(() => {
            if (this.check) {
              this.act.selectItem(this.activityDatas['id']);
            }

            this.SearchData(this.aboutAccountCloseForm.value, this.activityDatas['id']);
          }, 100);
        }
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
  }

  implementingAgencyChange(selectedEvent) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.agencyName = displayText;
      console.log(selectedEvent)
      let agencyCode = selectedEvent.target.value;
      this.getAgencyType(agencyCode);
      this.getActivity(this.aboutAccountCloseForm.value)
    } else {
      this.childType = '';
    }
  }


  getAgencyType(agencyCode = 0, index = 0, type = null) {
    // remove previously added form control if the parent select is changed.
    this.ats.show(agencyCode).subscribe(
      result => {
        if (result['length'] > 0) {
          this.childType = result;
        } else {
          this.childType = '';
        }
      },
      error => {
        console.log(error);
      }
    );
  }

}
