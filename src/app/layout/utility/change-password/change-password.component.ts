import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { UserService, CurrentUserService, AuthenticateService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

  cpForm: FormGroup;

  constructor(
    public router: Router,
    private fb: FormBuilder,
    private us: UserService,
    private cus: CurrentUserService,
    private auth: AuthenticateService
  ) {
    this.createForm();
  }

  /**
* Create the form group 
* with given form control name 
*/
  createForm() {
    this.cpForm = this.fb.group({
      'oldPass': [null, Validators.required],
      'newPass': [null, [
        Validators.required,
        Validators.minLength(4),
      ]
      ],
      'newPassConfirm': [null, [
        Validators.required,
        Validators.minLength(4)
      ]
      ]
    },
      {
        validator: this.Match('newPass', 'newPassConfirm')
      });
  }

  /**
   * Custom Validator
   * @param firstControlName 
   * @param secondControlName 
   */
  Match(firstControlName, secondControlName) {
    return (AC: AbstractControl) => {
      let firstControlValue = AC.get(firstControlName).value; // to get value in input tag
      let secondControlValue = AC.get(secondControlName).value; // to get value in input tag
      if (firstControlValue != secondControlValue) {
        AC.get(secondControlName).setErrors({ MatchFields: true });
      } else {
        return null
      }
    };
  }

  ngOnInit() {
    console.info(this.cus.getTokenData());
  }


  save(formData) {
    if (formData) {
      this.jqxLoader.open();
      let currentUser = this.cus.getTokenData();
      let id = currentUser['user']['userId'];
      formData['id'] = id;
      this.us.changePassword(formData).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            this.auth.logout();
            this.router.navigate(['/login']);
          }
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
          this.jqxLoader.close();
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Enter All Fields';
      this.errNotification.open();
    }
  }

}
