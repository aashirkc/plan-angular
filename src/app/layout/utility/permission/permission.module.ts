import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { PermissionComponent } from './permission.component';
import { PermissionRoutingModule } from './permission-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PermissionRoutingModule
  ],
  declarations: [PermissionComponent]
})
export class PermissionModule { }
