import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { PermissionService, Permission, UserService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('userCombo') userCombo: jqxComboBoxComponent;

  selectedUser: any;
  permissionForm: FormGroup;
  EditData: Permission;
  update: boolean = false;
  userAdapter: any = [];
  facultyAdapter: Array<any> = [];
  source: any;
  dataAdapter: any;
  columns: any[];

  editrow: number = -1;

  branchAccessAdapter: any = [
    { name: 'All', accessBranch: 'All' },
    { name: 'Home', accessBranch: 'Home' }
  ]

  statusAdapter: any = [
    { status: 'Y', value: 'Y' },
    { status: 'N', value: 'N' }
  ]


  constructor(
    private ps: PermissionService,
    private us: UserService,
    private fb: FormBuilder,
    private translate: TranslateService
  ) {
    this.getTranslation();
    this.createForm();
  }

  ngOnInit() {
    this.loadGrid();
    // this.adService.index({}).subscribe((res) => {
    //   this.facultyAdapter = res[1];
    // }, (error) => {
    //   console.info(error);
    // });
  }

  ngAfterViewInit() {
    this.loadUsers();
  }

  transData: any;
  getTranslation() {
    this.translate.get(['SN', 'USER', 'PERMISSION', 'PERMISSIONSTATUS', 'ACCESSBRANCH', "ACTION", "UPDATE"
      ,"USER_CODE", "FACULTY", "EDIT"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  createForm() {
    this.permissionForm = this.fb.group({
      'faculty': ['', Validators.required],
      'userCode': ['', Validators.required],
      'id': ['']
    });
  }

  loadPermission() {
    let post = {};
    post = this.permissionForm.value;
    if (post['userCode']) {
      this.jqxLoader.open();
      this.ps.index(post).subscribe(
        response => {
          console.info(response);
          if (response[0] && response[0].error) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = response[0].error;
            this.errNotification.open();
            this.source.localdata = [];
            this.myGrid.updatebounddata();
            this.jqxLoader.close();
          } else {
            this.source.localdata = response;
            this.myGrid.updatebounddata();
            this.jqxLoader.close();
          }
        },
        error => {
          console.log(error);
          this.jqxLoader.close();
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Choose All the field!';
      this.errNotification.open();
    }
  }

  loadUsers() {
    this.jqxLoader.open();
    this.us.index({}).subscribe(
      response => {

        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.userAdapter = [];
        } else {
          this.userAdapter = response;
        }
        this.jqxLoader.close();
      },
      error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'faculty', type: 'string' },
          { name: 'facultyName', type: 'string' },
          { name: 'id', type: 'string' },
          { name: 'userCode', type: 'string' },
        ],
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['USER_CODE'], datafield: 'userCode', columntype: 'textbox', editable: false, filtercondition: 'containsignorecase' },
      { text: this.transData['FACULTY'], datafield: 'faculty', displayfield: 'facultyName', columntype: 'textbox', editable: false, filtercondition: 'containsignorecase' },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          this.EditData = new Permission(dataRecord);
          this.permissionForm.setValue(this.EditData);
          this.update = true;
        }
      },
      {
        text: this.transData['ACTION'], datafield: 'Delete', sortable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return 'Delete';
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          this.ps.destroy(dataRecord['id']).subscribe(
            result => {
              this.jqxLoader.close();
              if (result['message']) {
                this.update = false;
                this.permissionForm.controls['faculty'].setValue('');
                let messageDiv: any = document.getElementById('message');
                messageDiv.innerText = result['message'];
                this.msgNotification.open();
                this.loadPermission();
              }
              if (result['error']) {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
              }
            },
            error => {
              this.jqxLoader.close();
              console.info(error);
            }
          );
        }
      }
    ];

  }
  save(post) {
    if (post) {
      this.jqxLoader.open();
      this.ps.store(post).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
           this.loadPermission();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all required Field.';
      this.errNotification.open();
    }
  }

  updateBtn(post) {
    this.jqxLoader.open();
    this.ps.update(post['id'], post).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.update = false;
          this.permissionForm.controls['faculty'].setValue('');
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.loadPermission();
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateBtn() {
    this.update = false;
    this.permissionForm.controls['code'].setValue('');
    this.loadPermission();
  }
}
