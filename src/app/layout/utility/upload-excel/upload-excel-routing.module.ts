import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadExcelComponent } from './upload-excel.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: UploadExcelComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UploadExcelRoutingModule { }
