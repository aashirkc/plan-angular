import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { UploadExcelComponent } from './upload-excel.component';
import { UploadExcelRoutingModule } from './upload-excel-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UploadExcelRoutingModule
  ],
  declarations: [UploadExcelComponent]
})
export class UploadExcelModule { }