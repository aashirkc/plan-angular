import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UserService, UnicodeTranslateService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

import { TranslateService } from '@ngx-translate/core';
function passwordConfirming(c: AbstractControl): any {
  if (!c.parent || !c) return;
  const pwd = c.parent.get('loginPass');
  const cpwd = c.parent.get('confirm_password')

  if (!pwd || !cpwd) return;
  if (pwd.value !== cpwd.value) {
    return { invalid: true };

  }
}

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
  @ViewChild('siteCombo') siteCombo: jqxComboBoxComponent;

  uForm: FormGroup;
  get cpwd() {
    return this.uForm.get('confirm_password');
  }

  // For Grid
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  deleteRowIndexes: Array<any> = [];
  update: boolean = false;

  userTypes: Array<any> = [];
  organizationAdapter: Array<any> = [];
  agencyTypeAdapter: Array<any> = [];
  originalAgencyTypeData: Array<any> = [];
  submitted: boolean = false;

  constructor(
    private fb: FormBuilder,
    private us: UserService,
    private unicode: UnicodeTranslateService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  transData: any;
  getTranslation() {
    this.translate.get(['SN', 'USER', 'CODE', 'FULLNAME', 'CONTACTNO', "ADDRESS", "TYPE", "EMAILADDRESS", "BRANCH", "SITE", "ACTION", "UPDATE", "RELOAD", "DELETE",
      "RELOAD", "USERNAME", "FULL_NAME", "MOBILE_NO", "EMAIL", "ADDRESS", "ORG_NAME", "USER_TYPE", "ACTION", "EDIT", "ADMIN", "GENERAL", "PRIVATE", "GOVERNMENT",
      "ACT_MANAGER", "TECHNICAL_MANAGER", "ADMINISTRATIVE_MANAGER", "WARD"]).subscribe((translation: [string]) => {
        this.transData = translation;
      });
  }

  ngOnInit() {
    this.loadGrid();
    this.userTypes = [
      { type: 'Admin', name: this.transData['ADMIN'] },
      { type: 'Activity Manager', name: this.transData['ACT_MANAGER'] },
      { type: 'Technical Manager', name: this.transData['TECHNICAL_MANAGER'] },
      { type: "Administrative Manager", name: this.transData['ADMINISTRATIVE_MANAGER'] },
      { type: "Ward", name: this.transData['WARD'] }
    ];
    this.organizationAdapter = [
      { type: 2, name: this.transData['GOVERNMENT'] }
    ];
  }

  createForm() {
    this.uForm = this.fb.group({
      'loginId': ['', Validators.required],
      'fullName': ['', Validators.required],
      'email': ['', Validators.required],
      'loginPass': ['', Validators.required],
      'confirm_password': ['', [Validators.required, passwordConfirming]],
      'address': ['', Validators.required],
      'mobile': ['', Validators.required],
      'orgId': [''],
      'id': [''],
      'canPrintSamjhautaBeforeApproval': false,
      'canViewAllData': false,
      'userType': ['', Validators.required]
    });
  }

  get userType() { return this.uForm.get('userType'); }
  get orgId() { return this.uForm.get('orgId'); }

  ngAfterViewInit() {
    this.unicode.initUnicode();
    this.loadGridData();
    this.loadAgencyData();
  }

  userTypeChange(value) {
    this.agencyTypeAdapter = [];
    if (value == "Ward") {
      this.orgId.reset();
      this.orgId.setValue("");
      this.agencyTypeAdapter = this.originalAgencyTypeData.filter(x => x.mgrCode == 1);

    } else {
      this.orgId.reset();
      this.orgId.setValue("");
      this.agencyTypeAdapter = [
        {
          code: "0",
          id: 0,
          mgrCode: "0",
          name: "Ward Level",
          nameNepali: "अन्य"
        }
      ];
    }
  }

  loadGridData() {
    this.myGrid.clearselection();
    this.deleteRowIndexes = [];
    this.jqxLoader.open();
    this.us.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.jqxLoader.close();
      this.myGrid.updatebounddata();
    }, (error) => {
      this.jqxLoader.close();
    });
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'address', type: 'string' },
          { name: 'email', type: 'string' },
          { name: 'fullName', type: 'string' },
          { name: 'id', type: 'string' },
          { name: 'loginId', type: 'string' },
          { name: 'loginPass', type: 'string' },
          { name: 'mobile', type: 'string' },
          { name: 'orgId', type: 'string' },
          { name: 'userType', type: 'string' },
          { name: 'canPrintSamjhautaBeforeApproval', type: 'string' },
          { name: 'canViewAllData', type: 'string' },
        ],
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['USERNAME'], datafield: 'loginId', width: 100, columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: this.transData['FULL_NAME'], datafield: 'fullName', width: 200, editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['MOBILE_NO'], datafield: 'mobile', editable: false, width: 100, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['EMAIL'], datafield: 'email', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['ADDRESS'], datafield: 'address', editable: false, width: 150, columntype: 'textbox', filtercondition: 'starts_with' },
      // { text: this.transData['ORG_NAME'], datafield: 'orgId', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['USER_TYPE'], datafield: 'userType', editable: false, width: 100, columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {
          this.uForm.get('confirm_password').setValidators(null);
          this.uForm.get('loginPass').setValidators(null);

          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          let dt = {};
          dt['id'] = dataRecord['id'];
          dt['loginId'] = dataRecord['loginId'];
          dt['fullName'] = dataRecord['fullName'];
          dt['mobile'] = dataRecord['mobile'];
          dt['email'] = dataRecord['email'];
          dt['address'] = dataRecord['address'];
          dt['orgId'] = dataRecord['orgId'];
          dt['userType'] = dataRecord['userType'];
          dt['canPrintSamjhautaBeforeApproval'] = dataRecord['canPrintSamjhautaBeforeApproval'][0]==='t';
          dt['canViewAllData'] = dataRecord['canViewAllData'][0]==='t';
          dt['loginPass'] = '';
          dt['confirm_password'] = '';
          this.uForm.setValue(dt);
          this.update = true;
        }
      }
    ];
  }
  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 80, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.myGrid.getselectedrowindexes();
      let ids = [];
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['id']);
      }
      if (ids.length > 0) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.us.destroy('(' + ids + ')').subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.loadGridData();

            }
            if (result['error']) {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please Select Some Item To Delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadGridData();
    });

  }; //render toolbar ends

  loadAgencyData() {
    let post = {};
    this.us.getAgencyType(post).subscribe(
      result => {
        this.jqxLoader.close();
        this.originalAgencyTypeData = result;
        this.jqxLoader.close();
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

  save(post) {
    this.submitted = true;
    if (this.uForm.invalid) {
      return;
    }
    if (post) {
      this.jqxLoader.open();
      this.us.store(post).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            this.submitted = false;
            this.loadGridData();
            this.uForm.controls['confirm_password'].setValue('');
            this.uForm.controls['loginPass'].setValue('');
            this.uForm.reset();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all required Field.';
      this.errNotification.open();
    }
  }
  updateBtn(post) {
    this.jqxLoader.open();
    this.us.update(post['id'], post).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.update = false;
          this.uForm.reset();
          this.uForm.controls['confirm_password'].setValue('');
          this.uForm.controls['loginPass'].setValue('');
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.loadGridData();
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateBtn() {
    this.uForm.get('confirm_password').setValidators([Validators.required, passwordConfirming]);
    this.uForm.get('loginPass').setValidators([Validators.required, passwordConfirming]);
    this.update = false;
    this.uForm.reset();
    this.uForm.controls['confirm_password'].setValue('');
    this.uForm.controls['loginPass'].setValue('');
    this.loadGridData();
  }

}
