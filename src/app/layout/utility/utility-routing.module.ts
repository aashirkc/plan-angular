import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {  UtilityComponent } from './utility.component';

const routes: Routes = [
    {
        path: '',
        component:  UtilityComponent,
        children: [
            { path:'', redirectTo: 'user', pathMatch:'full'},
            { path:'user', loadChildren:'./user/user.module#UserModule'},
            { path:'permission', loadChildren:'./permission/permission.module#PermissionModule'},
            { path:'change-password', loadChildren:'./change-password/change-password.module#ChangePasswordModule'},
            { path:'upload-excel', loadChildren:'./upload-excel/upload-excel.module#UploadExcelModule'},
        ]
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UtilityRoutingModule { }
