import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServiceTypeMasterComponent } from './service-type-master.component';

const routes: Routes = [
  {
    path: '',
    component: ServiceTypeMasterComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceTypeMasterRoutingModule { }