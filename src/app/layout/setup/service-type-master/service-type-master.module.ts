import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ServiceTypeMasterComponent } from './service-type-master.component';
import { ServiceTypeMasterRoutingModule } from './service-type-master-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ServiceTypeMasterRoutingModule
  ],
  declarations: [ServiceTypeMasterComponent]
})
export class ServiceTypeMasterModule { }
