import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AgencyTypeComponent } from './agency-type.component';
import { AgencyTypeRoutingModule } from './agency-type-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AgencyTypeRoutingModule
  ],
  declarations: [
    AgencyTypeComponent,
  ]
})
export class AgencyTypeModule { }
