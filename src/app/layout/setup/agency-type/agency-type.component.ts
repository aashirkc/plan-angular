import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AgencyTypeService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxTreeGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxtreegrid';
import { jqxCheckBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcheckbox';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-agency-type',
  templateUrl: './agency-type.component.html',
  styleUrls: ['./agency-type.component.scss']
})
export class AgencyTypeComponent implements OnInit {

  @ViewChild('groupTreeGrid') groupTreeGrid: jqxTreeGridComponent;
  @ViewChild('activityAreaFocusGroupGrid') activityAreaFocusGroupGrid: jqxGridComponent;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jtransactCheck') transactCheck: jqxCheckBoxComponent;

  /**
   * Global Variable decleration
   */
  ActivityFocusAreaMasterForm: FormGroup;
  treeSource: any;
  treeDataAdapter: any;
  treeColumns: any = [];
  gridSource: any;
  editrow: number = -1;
  update: boolean = false;
  gridDataAdapter: any;
  gridColumns: any = [];
  trasValue: any;
  selectedTreeGroupId: string;

  deleteRowIndexes: Array<any> = [];

  isChecked: boolean = false;
  selectedItem: string = '';

  constructor(
    private fb: FormBuilder,
    private aops: AgencyTypeService,
    private translate: TranslateService,
  ) {
    this.createForm();
    this.getTranslation();
  }

  transData: any;
  getTranslation() {
    this.translate.get(['SN', 'ITEM', 'DELETE', 'NAME', 'EDIT', 'ACTION', 'SAVE', 'CODE', 'GROUPNAME', 'AGENCY_TYPE_MASTER_NAMENEPALI', 'AGENCY_TYPE_MASTER_NAMEENGLISH', "TRANSACTION", "DELETESELECTED",]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  /**
   * Create the form group
   * with given form control name
   */
  createForm() {
    this.ActivityFocusAreaMasterForm = this.fb.group({
      'id': [''],
      'code': [''],
      'name': ['', Validators.required],
      'nameNepali': ['', Validators.required],
      'mgrCode': [''],
    });
  }

  ngOnInit() {
    localStorage.removeItem('selectedChard');
    this.treeSource =
      {
        dataType: "json",
        dataFields: [
          { name: 'id', type: 'number' },
          { name: 'code', type: 'number' },
          { name: 'name', type: 'number' },
          { name: 'nameNepali', type: 'string' },
          { name: 'mgrCode', type: 'string' },
        ],
        hierarchy:
        {
          keyDataField: { name: 'code' },
          parentDataField: { name: 'mgrCode' }
        },
        id: 'code',
        localdata: []
      };

    this.treeDataAdapter = new jqx.dataAdapter(this.treeSource);

    this.treeColumns =
      [
        { text: this.transData['CODE'], dataField: 'code', width: '130', filterable: false },
        { text: this.transData['AGENCY_TYPE_MASTER_NAMEENGLISH'], dataField: 'name' },
        { text: this.transData['AGENCY_TYPE_MASTER_NAMENEPALI'], dataField: 'nameNepali' },

      ];

    this.gridSource =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'number' },
          { name: 'code', type: 'number' },
          { name: 'name', type: 'number' },
          { name: 'nameNepali', type: 'string' },
          { name: 'mgrCode', type: 'string' },
        ],
        id: 'code',
        pagesize: 20,
        localdata: [],
      };

    this.gridDataAdapter = new jqx.dataAdapter(this.gridSource);

    this.gridColumns =
      [
        {
          text: this.transData['SN'], sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: 50,
          cellsrenderer: function (row, column, value) {
            return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: this.transData['AGENCY_TYPE_MASTER_NAMEENGLISH'], displayfield: 'name', datafield: 'code', width: 200 },
        { text: this.transData['AGENCY_TYPE_MASTER_NAMENEPALI'], displayfield: 'nameNepali', datafield: 'nameNepali', width: 200 },
        // { text: 'Activity Name(Nepali)', displayfield: 'focusName', datafield: 'focusCode', width: 250 },
        { text: this.transData['CODE'], displayfield: 'mgrCode', datafield: 'mgrCodeName' },
        {
          text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 80, columntype: 'button',
          cellsrenderer: (): string => {
            return this.transData['EDIT'];
          },
          buttonclick: (row: number): void => {
            this.editrow = row;
            let dataRecord = this.activityAreaFocusGroupGrid.getrowdata(this.editrow);
            let dt = {};
            this.ActivityFocusAreaMasterForm.reset();
            dt['id'] = dataRecord['id'];
            dt['code'] = dataRecord['code'];
            dt['name'] = dataRecord['name'];
            dt['nameNepali'] = dataRecord['nameNepali'];
            dt['mgrCode'] = dataRecord['mgrCode'];
            this.ActivityFocusAreaMasterForm.setValue(dt);
            this.update = true;
          }
        }
      ];
  }


  ngAfterViewInit() {
    this.loadTreeData();
  }

  ngOnDestroy() {
    localStorage.removeItem('selectedChard');
  }

  Change($e) {
    this.isChecked = $e.args.checked;
    if (this.isChecked) {
      this.ActivityFocusAreaMasterForm.get('code').enable();
      // this.ActivityFocusAreaMasterForm.get('assetCode').setValidators(Validators.required);
    } else {
      this.ActivityFocusAreaMasterForm.get('code').disable();
      // this.ActivityFocusAreaMasterForm.get('assetCode').re
    }
  }

  /**
   * Row selected event in Tree Grid
   * @param $event
   */
  treeRowSelect($event) {
    // console.log($event);
    this.deleteRowIndexes = [];
    this.selectedTreeGroupId = $event.args['key'];
    this.treeRowSelectedData(this.selectedTreeGroupId);
    localStorage.setItem('selectedChard', JSON.stringify($event.args.row))
    this.selectedItem = $event && $event.args && $event.args.row && $event.args.row['nameNepali'];
  }

  /**
   * Get child Data of selected row in Tree Grid
   * and display the childern in Grid
   * @param groupId
   */
  treeRowSelectedData(groupId) {
    this.activityAreaFocusGroupGrid.clearselection();
    if (groupId) {
      this.aops.showChild(groupId).subscribe(
        response => {
          let TreeChild = response;
          if (response['length'] == 1 && response[0].error) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = response[0].error;
            this.errNotification.open();
            this.gridSource.localdata = [];
          } else if (TreeChild['length'] < 1) {
            this.gridSource.localdata = [];
          } else {
            this.gridSource.localdata = response;
          }
          this.activityAreaFocusGroupGrid.updatebounddata();

        },
        error => {
          console.log(error);
        }
      )
    }
  }


  /**
   * Grid row checked event
   * @param event
   */
  rowChange(event: any) {
    this.deleteRowIndexes.push(event.args.rowindex);
  }

  /**
   * Grid row unchecked event
   * @param event
   */
  rowUnChange(event: any) {
    let index = this.deleteRowIndexes.indexOf(event.args.rowindex);
    if (index > -1) {
      this.deleteRowIndexes.splice(index, 1);
    }

  }

  /**
   * Child Grid Toolbar
   */
  gridRenderToolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer3 = document.createElement('div');

    buttonContainer3.id = 'buttonContainer3';

    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.deleteRowIndexes;
      let ids = [];
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.activityAreaFocusGroupGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['id']);
      }
      //Load Grid After Item Have been deleted.
      this.treeRowSelectedData(this.selectedTreeGroupId);

      if (ids.length > 0) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.aops.destroy(ids).subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.activityAreaFocusGroupGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
            }
            if (result['error']) {
              this.activityAreaFocusGroupGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }

            // Reload Tree grid after item deletion
            this.loadTreeData();

            //Reload grid after item deletion
            this.treeRowSelectedData(this.selectedTreeGroupId);

          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
        this.activityAreaFocusGroupGrid.updatebounddata();
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

  }; //render toolbar ends


  /**
   * Load Tree Grid Data
   */
  loadTreeData() {
    this.aops.index({}).subscribe(
      res => {
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.loadTreeSource([]);
        } else {
          this.loadTreeSource(res);

        }
      },
      error => {
        console.log(error);
      }
    )
  }

  /**
   *  Load Tree Grid Source with Data
   * @param data
   */
  loadTreeSource(data) {
    this.treeSource.localdata = data;
    this.groupTreeGrid.updateBoundData();
    let datat = localStorage.getItem('selectedChard');
    this.groupTreeGrid.expandAll();
    if (datat) {
      let selectedData = JSON.parse(datat);
      this.groupTreeGrid.selectRow(selectedData['code']);
    }

  }

  clearSelection() {
    this.groupTreeGrid.clearSelection();
    localStorage.removeItem('selectedChard');
    this.gridSource.localdata = [];
    this.activityAreaFocusGroupGrid.updatebounddata();
    this.selectedItem = '';
  }

  RemoveOnClick(): void {
    let selectedItem = this.groupTreeGrid.getSelection();
    if (selectedItem.length > 0) {
      let ids = [];
      ids.push(selectedItem[0].id);
      if (confirm("Are you sure? You Want to delete")) {
        this.jqxLoader.open();
        this.aops.destroy(ids).subscribe(result => {
          this.jqxLoader.close();
          if (result['message']) {
            this.activityAreaFocusGroupGrid.clearselection();
            this.deleteRowIndexes = [];
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          if (result['error']) {
            this.activityAreaFocusGroupGrid.clearselection();
            this.deleteRowIndexes = [];
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }

          // Reload Tree grid after item deletion
          this.loadTreeData();

          //Reload grid after item deletion
          this.treeRowSelectedData(this.selectedTreeGroupId);

        }, (error) => {
          this.jqxLoader.close();
          console.info(error);
        });
      }
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = "Please Select One Item";
      this.errNotification.open();
    }
  };

  /**
   * Function triggered when save button is clicked
   * @param formData
   */
  save(formData) {
    let data = localStorage.getItem('selectedChard');
    let selectedData = JSON.parse(data);
    formData['mgrCode'] = selectedData && selectedData['code'] || 0;
    this.jqxLoader.open();
    this.aops.store(formData).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.loadTreeData();
          this.ActivityFocusAreaMasterForm.reset();
          let groupId = selectedData && selectedData['focusCode'];
          this.treeRowSelectedData(groupId);
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
    // } else {
    //   let messageDiv: any = document.getElementById('error');
    //   messageDiv.innerText = 'Please Select Group Name';
    //   this.errNotification.open();
    // }
  }
  updateBtn(post) {
    let data = localStorage.getItem('selectedChard');
    let selectedData = JSON.parse(data);
    this.jqxLoader.open();
    this.aops.update(post['id'], post).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.update = false;
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.loadTreeData();
          this.ActivityFocusAreaMasterForm.reset();
          let groupId = selectedData['code'];
          this.treeRowSelectedData(groupId);
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateBtn() {
    this.update = false;
    this.ActivityFocusAreaMasterForm.reset();
  }

}
