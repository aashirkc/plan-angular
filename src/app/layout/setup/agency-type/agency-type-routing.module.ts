import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgencyTypeComponent } from './agency-type.component';

const routes: Routes = [
  {
    path: '',
    component: AgencyTypeComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgencyTypeRoutingModule { }
