import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramsRoutingModule } from './programs-routing.module';
import { ProgramsComponent } from './programs.component';
import { SharedModule } from '../../../shared/modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ProgramsRoutingModule
  ],
  declarations: [ProgramsComponent]
})
export class ProgramsModule { }
