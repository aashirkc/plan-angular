import { Component, ViewChild, OnInit, Inject, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivityMasterService, ActivityDetailService, AgencyTypeService, ActivityFocusAreaMasterService, NepaliInputComponent, NepaliTextareaComponent, UnicodeTranslateService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { ProgramsService } from '../../../shared/services/programs.service';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';

@Component({
  selector: 'app-programs',
  templateUrl: './programs.component.html',
  styleUrls: ['./programs.component.scss']
})
export class ProgramsComponent implements OnInit, AfterViewInit {

  programForm: FormGroup;
  activityFocusAreaAdapter: any = [];
  subActivityFocusAreaAdapter: any = [];
  planYearAdaptor: any = [];

  source: any;
  dataAdapter: any;
  columns: any[] = [];
  columngroups: any[];
  deleteRowIndexes: Array<any> = [];
  subCatData: any;
  showButton: boolean = false;
  planYearsSelected: boolean = false;
  workAreaSelected: boolean = false;
  subWorkAreaSelected: boolean = false;
  municipaWadaAdapter: any;
  wadaAdapter: any;
  status: boolean = false;
  agencyName: any;
  agencyType: any;
  lang: any;
  childType: any = [];

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('focusCombo') focusCombo: jqxComboBoxComponent;
  @ViewChild('subFocusCombo') subFocusCombo: jqxComboBoxComponent;
  @ViewChild('planYear') planYear: jqxComboBoxComponent;
  @ViewChild('ref') ref: NepaliInputComponent;
  @ViewChild('refT') refT: NepaliTextareaComponent;
  @ViewChild('myWindow') myWindow: jqxWindowComponent;

  private apiUrl: string = "";

  constructor(
    private fb: FormBuilder,
    private ps: ProgramsService,
    private afams: ActivityFocusAreaMasterService,
    private ags: AgencyTypeService,
    private ads: ActivityDetailService,
    private unicode: UnicodeTranslateService,
    @Inject('API_URL') apiUrl: string,
  ) {
    this.apiUrl = apiUrl;
  }

  ngOnInit() {
    this.loadGrid();
    this.createForm();
    console.log(this.programForm);

    this.ags.show(0).subscribe(
      result => {
        if (result['length'] > 0) {

          this.agencyType = result;
        }
      },
      error => {
        console.log(error);
      }
    );

    this.afams.show(0).subscribe(
      result => {
        this.activityFocusAreaAdapter = result;
      },
      error => {
        console.log(error);
      }
    );

    this.ps.getPlanYear().subscribe(
      result => {
        this.planYearAdaptor = result;
        // this.planYear.selectItem(result[0]['yearCode']);
        setTimeout(() => {
          this.planYear.selectIndex(0);
          // this.getPrograms();
        }, 100)
      }, error => {
        console.log(error);
      }
    )
    this.ags.show(0).subscribe((res) => {
      this.municipaWadaAdapter = res;
    }, (error) => {

    });


  }

  getPrograms() {
    let post = {};
    post['planYear'] = '';
    post['focusAreaLevel1'] = '';
    post['focusAreaLevel2'] = '';

    return this.ps.getYearlyPrograms(post).subscribe(
      result => {
        this.source.localdata = result;
        this.myGrid.updatebounddata();
        this.jqxLoader.close();
        setTimeout(() => {
          this.toggleNepalify();
        }, 2000);
      }, error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  ngAfterViewInit() {
    this.jqxLoader.open();
    setTimeout(() => {
      this.toggleNepalify();
    }, 2000);
  }

  private toggleNepalify = () => {
    const nepali = [false, false, true, false, true, true, true];
    const textArea = document.querySelectorAll('.jqx-widget-header input.jqx-input');
    for (let i = 0; i < textArea.length; i++) {
      if (nepali[i]) {
        const area = textArea[i];
        area.classList.add('nepalify');
      }
    }
    this.unicode.initUnicode(['nepalify']);
  }

  createForm() {
    this.programForm = this.fb.group({
      'id': [null],
      'budgetTitleNo': [null, Validators.required],
      'name': [null, Validators.required],
      'amount': [null, Validators.required],
      'focusAreaLevel1': [null, Validators.required],
      'focusAreaLevel2': [null, Validators.required],
      'remarks': [null],
      'planYear': [null, Validators.required],
      'implementingAgencyType1': [''],
      'implementingAgencyType2': ['']
    })
  }

  save(value) {
    value['fundProvider'] = "";
    this.subCatData = '';
    this.jqxLoader.open();
    this.ps.saveProgram(value).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.programForm.reset();
          //   this.unSelectCombo();
          this.focusCombo.clearSelection();
          this.subFocusCombo.clearSelection();
          this.ref.clearInput();
          this.refT.clearInput();
          this.getPrograms();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      }, error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  Change($event) {
    let focusCode = this.focusCombo.val();
    this.programForm.get('focusAreaLevel1').patchValue(focusCode);
    this.afams.show(focusCode).subscribe(
      result => {
        this.subActivityFocusAreaAdapter = result;
        if (this.showButton == true) {
          setTimeout(() => { this.subFocusCombo.selectItem(this.subCatData) }, 100);
        }
      }, error => {
        console.log(error);
      }
    );
    this.workAreaSelected = true;
    this.getData();
  }


  Change1($event) {
    let focusCode = this.subFocusCombo.val();
    this.programForm.get('focusAreaLevel2').patchValue(focusCode);
    this.subWorkAreaSelected = true;
    this.getData();
  }
  Change2($event) {
    this.getData();
  }
  planYearSelected($event) {
    this.programForm.get('planYear').patchValue(this.planYear.val());
    this.planYearsSelected = true;
    this.getData();
  }

  getData() {
    if (this.planYearSelected && !this.showButton) {
      this.jqxLoader.open();
      let post = {};
      post['planYear'] = this.planYear.val();
      this.focusCombo.val() ? post['focusAreaLevel1'] = this.focusCombo.val() : post['focusAreaLevel1'] = '';
      this.subFocusCombo.val() ? post['focusAreaLevel2'] = this.subFocusCombo.val() : post['focusAreaLevel2'] = '';
      post['budgetTitleNo'] = this.programForm.get('budgetTitleNo').value || '';
      this.ps.getYearlyPrograms(post).subscribe(
        result => {
          this.source.localdata = result;
          this.myGrid.updatebounddata();
          this.jqxLoader.close();
          this.toggleNepalify();
        }, error => {
          this.jqxLoader.close();
          console.log(error);
        }
      )
    }
  }
  loadWadaData(data) {
    this.jqxLoader.open();
    this.ags.show(data).subscribe((res) => {
      this.wadaAdapter = res;
      this.jqxLoader.close();
    }, (error) => {

    });
  }
  implementingAgencyChange(selectedEvent) {
    this.jqxLoader.open();
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      if (selectedEvent.target.value == 1) {
        this.ags.show(selectedEvent.target.value).subscribe((res) => {
          this.wadaAdapter = res;
          this.jqxLoader.close();

          let post = {};
          post['planYear'] = this.planYear.val();
          post['focusAreaLevel1'] = this.focusCombo.val();
          post['focusAreaLevel2'] = this.subFocusCombo.val();
          post['implementingAgencyType1'] = selectedEvent.target.value;
          post['implementingAgencyType2'] = '';

          this.ps.getYearlyPrograms(post).subscribe(
            result => {
              this.source.localdata = result;
              this.myGrid.updatebounddata();
              this.jqxLoader.close();
            }, error => {
              this.jqxLoader.close();
              console.log(error);
            });
        }, (error) => {

        });
      } else {
        this.wadaAdapter = [];
        let post = {};
        post['planYear'] = this.planYear.val();
        post['focusAreaLevel1'] = this.focusCombo.val();
        post['focusAreaLevel2'] = this.subFocusCombo.val();
        post['implementingAgencyType1'] = selectedEvent.target.value;
        post['implementingAgencyType2'] = '';

        this.ps.getYearlyPrograms(post).subscribe(
          result => {
            this.source.localdata = result;
            this.myGrid.updatebounddata();
            this.jqxLoader.close();
          }, error => {
            this.jqxLoader.close();
            console.log(error);
          });
      }
    }
  }

  implementingAgencyChangeWada(selectedEvent) {
    this.jqxLoader.open();
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let post = {};
      post['planYear'] = this.planYear.val();
      post['focusAreaLevel1'] = this.focusCombo.val();
      post['focusAreaLevel2'] = this.subFocusCombo.val();
      post['implementingAgencyType1'] = this.programForm.controls['implementingAgencyType1'].value;
      post['implementingAgencyType2'] = selectedEvent.target.value;
      this.ps.getYearlyPrograms(post).subscribe(
        result => {
          this.source.localdata = result;
          this.myGrid.updatebounddata();
          this.jqxLoader.close();
        }, error => {
          this.jqxLoader.close();
          console.log(error);
        });
    }
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'ongoingActivityCount', type: 'number' },
          { name: 'budgetTitleNo', type: 'string' },
          { name: 'name', type: 'string' },
          { name: 'amount', type: 'number' },
          { name: 'focusAreaLevel1', type: 'string' },
          { name: 'focusAreaLevel2', type: 'string' },
          { name: 'focusAreaLevel1Name', type: 'string' },
          { name: 'focusAreaLevel2Name', type: 'string' },
          { name: 'planYear', type: 'string' },
          { name: 'planYearName', type: 'string' },
          { name: 'remarks', type: 'string' },
          { name: 'implementingAgencyType1', type: 'string' },
          { name: 'implementingAgencyType1Name', type: 'string' },
          { name: 'implementingAgencyType2', type: 'string' },
        ],
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      { text: 'status', datafield: 'ongoingActivityCount', width: 150, columntype: 'textbox', editable: false, hidden: true },

      {
        text: 'क्र स', sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        },
      },
      { text: 'बजेट शीर्षक न', filterable: true, datafield: 'budgetTitleNo', width: 150, columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: 'नाम', filterable: true, datafield: 'name', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: 'बजेट रकम',
        filterable: true,
        datafield: 'amount',
        editable: false,
        width: 150,
        columntype: 'textbox',
        aggregates: [{
          '<b>Total</b>': (aggregatedValue: number, currentValue: number, column: any, record: any): number => {
            let total = currentValue;
            return aggregatedValue + total;
          }
        }]
      },

      // { text: 'योजना वर्ष', filterable: false, datafield: 'planYear', displayField:'planYearName',editable: false, width: 100, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: 'कार्यक्षेत्र', filterable: true, datafield: 'focusAreaLevel1', displayField: 'focusAreaLevel1Name', editable: false, columntype: 'textbox', width: 150, filtercondition: 'starts_with' },
      { text: 'उप - कार्यक्षेत्र', filterable: true, datafield: 'focusAreaLevel2', displayField: 'focusAreaLevel2Name', editable: false, columntype: 'textbox', width: 150, filtercondition: 'starts_with' },
      { text: 'स्तरीय', filterable: true, datafield: 'implementingAgencyType1Name', editable: false, columntype: 'textbox', width: 100, filtercondition: 'starts_with' },
      {
        text: 'कैफियत', columntype: 'button', width: 90,
        filterable: false,
        cellsrenderer: (): string => {
          return "हेर्नुहाेस्";
        },
        buttonclick: (row: number): void => {
          let dataRecord = this.myGrid.getrowdata(row);
          let dt = {};
          let messageDiv = document.getElementById('showRemarks');
          messageDiv.innerText = dataRecord['remarks'];
          this.myWindow.draggable(true);
          this.myWindow.title('कैफियत विस्तारमा');
          this.myWindow.open();
        }
      },
      {
        text: 'सच्याउनु होस्', columntype: 'button', width: 90,
        filterable: false,
        cellsrenderer: (): string => {
          return "सच्याउनु होस्";
        },
        buttonclick: (row: number): void => {
          this.subFocusCombo.clearSelection();
          this.subCatData = '';
          let dataRecord = this.myGrid.getrowdata(row);
          let dt = {};
          console.log(dataRecord);
          if (dataRecord['ongoingActivityCount'] !== 0) {
            this.status = true;
          } else {
            this.status = false;
          }
          if (dataRecord['implementingAgencyType1'] == '1') {
            this.getAgencyType(dataRecord['implementingAgencyType1']);
            console.log('hello')
          }
          this.programForm.get('id').patchValue(dataRecord['id']);
          this.programForm.get('budgetTitleNo').patchValue(dataRecord['budgetTitleNo']);
          this.programForm.get('name').patchValue(dataRecord['name']);
          this.programForm.get('amount').patchValue(dataRecord['amount']);
          this.programForm.get('focusAreaLevel1').patchValue(dataRecord['focusAreaLevel1']);
          this.programForm.get('focusAreaLevel2').patchValue(dataRecord['focusAreaLevel2']);
          this.programForm.get('planYear').patchValue(dataRecord['planYear']);
          this.programForm.get('remarks').patchValue(dataRecord['remarks']);
          this.programForm.get('implementingAgencyType1').patchValue(dataRecord['implementingAgencyType1']);
          this.programForm.get('implementingAgencyType2').patchValue(dataRecord['implementingAgencyType2']);
          this.focusCombo.selectItem(dataRecord['focusAreaLevel1']);
          this.subCatData = dataRecord['focusAreaLevel2'];
          this.planYear.selectItem(dataRecord['planYear']);

          this.showButton = true;
        }
      },

    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

  }

  rowChange(event: any) {
    this.deleteRowIndexes.push(event.args.rowindex);
  }
  rowUnChange(event: any) {
    let index = this.deleteRowIndexes.indexOf(event.args.rowindex);
    if (index > -1) {
      this.deleteRowIndexes.splice(index, 1);
    }
  }

  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: 'मेटाउनु होस', theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + 'पुन: लोड गर्नुहोस्', theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', (row: number) => {
      let id = this.deleteRowIndexes;
      let ids = [];
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['id']);
      }

      if (ids.length > 0) {
        if (confirm("के तपाईँ निश्चित हुनुहुन्छ ?")) {
          this.jqxLoader.open();
          this.ps.deleteProgram(ids).subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.myGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.getPrograms();
            }
            if (result['error']) {
              this.myGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              this.myGrid.updatebounddata();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.getPrograms();
    });

  };

  update(value) {
    this.subCatData = '';
    value['fundProvider'] = "";
    this.jqxLoader.open();
    this.ps.updateProgram(value, value['id']).subscribe(
      result => {
        this.showButton = false;
        this.jqxLoader.close();
        let messageDiv = document.getElementById('message');
        messageDiv.innerText = 'अद्यावधिक सफल';
        this.msgNotification.open();
        this.programForm.reset();
        this.ref.clearInput();
        this.refT.clearInput();
        this.unSelectCombo();
        this.subFocusCombo.clearSelection();
        this.focusCombo.clearSelection();
        this.getPrograms();
        this.status = false;
      }, error => {
        console.log(error);
        this.jqxLoader.close();
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'केहि गलत भयो';
        this.errNotification.open();
      }
    )
  }

  cancel() {
    this.subCatData = '';
    this.programForm.reset();
    this.unSelectCombo();
    this.showButton = false;
    this.ref.clearInput();
    this.refT.clearInput();
  }

  private unSelectCombo() {
    this.focusCombo.unselectIndex(0);
    this.planYear.unselectIndex(0);
    this.subFocusCombo.unselectIndex(0);
  }

  nameChane($event) {
    //     console.log($event);
    //    console.log(this.programForm.get('name').value);
    this.programForm.get('name').patchValue($event.target.value)
  }
  remarkChane($event) {
    console.log($event);
    this.programForm.get('remarks').patchValue($event.target.value)
  }

  Changes(event: any): void {
    console.log("HEllo world");
  }
  implementingAgencyChange1(selectedEvent) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.agencyName = displayText;
      let agencyCode = selectedEvent.target.value;
      this.getAgencyType(agencyCode);
    } else {
      this.childType = '';
    }
  }


  getAgencyType(agencyCode = 0, index = 0, type = null) {
    // remove previously added form control if the parent select is changed.
    this.ags.show(agencyCode).subscribe(
      result => {
        if (result['length'] > 0) {
          this.childType = result;
        } else {
          this.childType = '';
        }
      },
      error => {
        console.log(error);
      }
    );
  }

}
