import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KarkattiComponent } from './karkatti.component';

describe('KarkattiComponent', () => {
  let component: KarkattiComponent;
  let fixture: ComponentFixture<KarkattiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KarkattiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KarkattiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
