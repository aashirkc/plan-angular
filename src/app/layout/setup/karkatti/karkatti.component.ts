import { Component, OnInit } from '@angular/core';
import { ActivityMasterService } from 'app/shared';

@Component({
  selector: 'app-karkatti',
  templateUrl: './karkatti.component.html',
  styleUrls: ['./karkatti.component.scss']
})
export class KarkattiComponent implements OnInit {

  constructor(
      private kar:ActivityMasterService,
      ) { }

  ngOnInit() {
      this.kar.karkatii({}).subscribe(res=>{
        console.log(res)
      })
  }

}
