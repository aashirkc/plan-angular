import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { InclusionGroupComponent } from './inclusion-group.component';
import { InclusionGroupRoutingModule } from './inclusion-group-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    InclusionGroupRoutingModule
  ],
  declarations: [
    InclusionGroupComponent,
  ]
})
export class InclusionGroupModule { }
