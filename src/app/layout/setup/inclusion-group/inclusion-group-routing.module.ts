import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InclusionGroupComponent } from './inclusion-group.component';

const routes: Routes = [
  {
    path: '',
    component: InclusionGroupComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InclusionGroupRoutingModule { }
