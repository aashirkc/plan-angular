import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AssetsDetailTypeComponent } from './assets-detail-type.component';
import { AssetsDetailTypeRoutingModule } from './assets-detail-type-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AssetsDetailTypeRoutingModule
  ],
  declarations: [AssetsDetailTypeComponent]
})
export class AssetsDetailTypeModule { }
