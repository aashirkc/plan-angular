import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssetsDetailTypeComponent } from './assets-detail-type.component';

const routes: Routes = [
  {
    path: '',
    component: AssetsDetailTypeComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetsDetailTypeRoutingModule { }