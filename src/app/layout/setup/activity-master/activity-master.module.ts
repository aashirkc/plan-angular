import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ActivityMasterComponent } from './activity-master.component';
import { ActivityMasterRoutingModule } from './activity-master-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ActivityMasterRoutingModule
  ],
  declarations: [
    ActivityMasterComponent,
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class ActivityMasterModule { }
