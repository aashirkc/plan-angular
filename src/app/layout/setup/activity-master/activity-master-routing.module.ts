import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityMasterComponent } from './activity-master.component';

const routes: Routes = [
  {
    path: '',
    component: ActivityMasterComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityMasterRoutingModule { }
