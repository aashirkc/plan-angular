import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ActivityFocusAreaMasterComponent } from './activity-focus-area-master.component';
import { ActivityFocusAreaMasterRoutingModule } from './activity-focus-area-master-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ActivityFocusAreaMasterRoutingModule
  ],
  declarations: [
    ActivityFocusAreaMasterComponent,
  ]
})
export class ActivityFocusAreaMasterModule { }
