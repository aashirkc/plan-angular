import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityFocusAreaMasterComponent } from './activity-focus-area-master.component';

const routes: Routes = [
  {
    path: '',
    component: ActivityFocusAreaMasterComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityFocusAreaMasterRoutingModule { }
