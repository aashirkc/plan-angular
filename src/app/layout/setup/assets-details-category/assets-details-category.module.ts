import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssetsDetailsCategoryRoutingModule } from './assets-details-category-routing.module';
import { AssetsDetailsCategoryComponent } from './assets-details-category.component';
import { SharedModule } from '../../../shared/modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AssetsDetailsCategoryRoutingModule
  ],
  declarations: [AssetsDetailsCategoryComponent]
})
export class AssetsDetailsCategoryModule { }
