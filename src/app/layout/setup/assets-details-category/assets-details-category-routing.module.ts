import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssetsDetailsCategoryComponent } from './assets-details-category.component';

const routes: Routes = [
  {
    path: '',
    component: AssetsDetailsCategoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetsDetailsCategoryRoutingModule { }
