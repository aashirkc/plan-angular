import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnugamanSamitiComponent } from './anugaman-samiti.component';

describe('AnugamanSamitiComponent', () => {
  let component: AnugamanSamitiComponent;
  let fixture: ComponentFixture<AnugamanSamitiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnugamanSamitiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnugamanSamitiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
