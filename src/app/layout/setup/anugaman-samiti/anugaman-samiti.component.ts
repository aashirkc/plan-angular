import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AnugamanSamitiMasterServiceService } from 'app/shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
    selector: 'app-anugaman-samiti',
    templateUrl: './anugaman-samiti.component.html',
    styleUrls: ['./anugaman-samiti.component.scss']
})
export class AnugamanSamitiComponent implements OnInit, AfterViewInit {
    formGroup: FormGroup;
    data: any;

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    constructor(
        private _formBuilder: FormBuilder,
        private _anugamanSamitiService: AnugamanSamitiMasterServiceService
    ) {
        this.formGroup = this._formBuilder.group({
            samiti: this._formBuilder.array([])
        });
    }

    ngOnInit = () => { };

    ngAfterViewInit(): void {
        this.rehydrateValues();
    }

    initSamitiControl = () => {
        return this._formBuilder.group({
            name: [''],
            designation: [''],
            related: [''],
            committeeDesignation: ['']
        });
    };

    addControl = () => {
        (this.formGroup.get('samiti') as FormArray).push(
            this.initSamitiControl()
        );
    };

    removeControl = (index: number) => {
        this.formGroup.get('samiti')['controls'].splice(index, 1);
        this.formGroup.updateValueAndValidity();
        if ((this.formGroup.get('samiti') as FormArray).length === 0) {
            this.addControl();
        }
    };

    save = () => {
        this.jqxLoader.open();
        const samiti = this.formGroup
            .get('samiti')
        ['controls'].map(data => data.value);
        this._anugamanSamitiService.save(samiti).subscribe(
            response => {
                if (response && response['message']) {
                    this.showMessage(response['message']);
                } else if (
                    response &&
                    response['error'] &&
                    response['error']['message']
                ) {
                    this.showError(response['error']['message']);
                }
                this.rehydrateValues();
                this.jqxLoader.close();
            },
            error => {
                this.jqxLoader.close();
            }
        );
    };

    rehydrateValues = () => {
        const samitiArray = this.formGroup.get('samiti') as FormArray;
        this.formGroup.get('samiti')['controls'] = [];
        this.jqxLoader.open();
        this._anugamanSamitiService.index().subscribe(
            response => {
                this.jqxLoader.close();
                if (response.length === 0) {
                    this.addControl();
                    return;
                }
                if (response['error'] && response['error']['message']) {
                    this.showError(response['error']['message']);
                    return;
                }
                for (const data of response) {
                    const formGroup: FormGroup = this.initSamitiControl();
                    formGroup.patchValue(data);
                    samitiArray.push(formGroup);
                }
            },
            error => {
                this.addControl();
                this.jqxLoader.close();
            }
        );
    };

    showMessage = (message: string) => {
        const messageDiv: any = document.getElementById('message');
        messageDiv.innerText = message;
        this.msgNotification.open();
    };

    showError = (message: string) => {
        const messageDiv: any = document.getElementById('error');
        messageDiv.innerText = message;
        this.errNotification.open();
    };
}
