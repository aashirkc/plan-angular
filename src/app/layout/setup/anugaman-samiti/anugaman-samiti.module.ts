import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnugamanSamitiRoutingModule } from './anugaman-samiti-routing.module';
import { AnugamanSamitiComponent } from './anugaman-samiti.component';
import { SharedModule } from 'app/shared/modules/shared.module';

@NgModule({
    imports: [CommonModule, SharedModule, AnugamanSamitiRoutingModule],
    declarations: [AnugamanSamitiComponent]
})
export class AnugamanSamitiModule {}
