import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnugamanSamitiComponent } from './anugaman-samiti.component';

const routes: Routes = [
    {
        path: '',
        component: AnugamanSamitiComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AnugamanSamitiRoutingModule {}
