import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DivisionRoutingModule } from './division-routing.module';
import { DivisionComponent } from './division.component';
import { SharedModule } from 'app/shared/modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DivisionRoutingModule
  ],
  declarations: [DivisionComponent]
})
export class DivisionModule { }
