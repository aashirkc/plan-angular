import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetupComponent } from './setup.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { SetupRoutingModule } from './setup-routing.module';
import { KarkattiComponent } from './karkatti/karkatti.component';



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SetupRoutingModule
  ],
  declarations: [SetupComponent, KarkattiComponent, ],
  schemas:[NO_ERRORS_SCHEMA]
})
export class SetupModule { }
