import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { PlantUnitComponent } from './plant-unit.component';
import { PlantUnitRoutingModule } from './plant-unit-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PlantUnitRoutingModule
  ],
  declarations: [
    PlantUnitComponent,
  ]
})
export class PlantUnitModule { }
