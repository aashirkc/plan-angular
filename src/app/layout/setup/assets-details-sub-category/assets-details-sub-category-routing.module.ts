import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssetsDetailsSubCategoryComponent } from './assets-details-sub-category.component';

const routes: Routes = [
  {
    path: '',
    component: AssetsDetailsSubCategoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetsDetailsSubCategoryRoutingModule { }
