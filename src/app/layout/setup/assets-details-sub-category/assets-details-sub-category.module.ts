import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssetsDetailsSubCategoryRoutingModule } from './assets-details-sub-category-routing.module';
import { AssetsDetailsSubCategoryComponent } from './assets-details-sub-category.component';
import { SharedModule } from '../../../shared/modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AssetsDetailsSubCategoryRoutingModule
  ],
  declarations: [AssetsDetailsSubCategoryComponent]
})
export class AssetsDetailsSubCategoryModule { }
