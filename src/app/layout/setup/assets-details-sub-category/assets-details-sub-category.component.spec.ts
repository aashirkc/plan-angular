import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetsDetailsSubCategoryComponent } from './assets-details-sub-category.component';

describe('AssetsDetailsSubCategoryComponent', () => {
  let component: AssetsDetailsSubCategoryComponent;
  let fixture: ComponentFixture<AssetsDetailsSubCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetsDetailsSubCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetsDetailsSubCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
