import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { PlanningYearComponent } from './planning-year.component';
import { PlanningYearRoutingModule } from './planning-year-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PlanningYearRoutingModule
  ],
  declarations: [
    PlanningYearComponent,
  ]
})
export class PlanningYearModule { }
