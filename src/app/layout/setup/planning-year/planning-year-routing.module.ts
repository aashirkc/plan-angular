import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanningYearComponent } from './planning-year.component';

const routes: Routes = [
  {
    path: '',
    component: PlanningYearComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanningYearRoutingModule { }
