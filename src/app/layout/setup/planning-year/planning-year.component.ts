import { Component, ViewChild, NO_ERRORS_SCHEMA, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { PlanningYearService, CustomValidators, DateConverterService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { test } from 'assets/javascript/test';

@Component({
  selector: 'app-planning-year',
  templateUrl: './planning-year.component.html',
  styleUrls: ['./planning-year.component.scss']
})
export class PlanningYearComponent implements OnInit {

  planningYearForm: FormGroup;
  // EditData: CoursesMaster;
  update: boolean = false;
  source: any;
  comboBoxSource: any[] = [];
  dataAdapter: any;
  columns: any[] = [];
  editrow: number = -1;
  columngroups: any[];
  deleteRowIndexes: Array<any> = [];
  rules: any = [];
  transData: any;
  selectedIndex: number;
  fullDate: string;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('comboBox') comboBox: jqxComboBoxComponent;

  constructor(
    private fb: FormBuilder,
    private pys: PlanningYearService,
    private dcs: DateConverterService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
    test()
    // test().InitializeUnicodeNepali();
  }

  ngOnInit() {
    this.comboBoxSource = [
      { name: this.transData['YESE'], status: "Y" },
      { name: this.transData['NOE'], status: "N" }
    ];
    let currentYear = new Date().getFullYear();
    let month = new Date().getMonth()+ 1;
    let day = new Date().getDate();
    let engDate = currentYear + '-' + month + '-' + day;
    let date = this.dcs.ad2Bs(engDate);
    this.fullDate = date['fulldate'];
    this.planningYearForm.controls['startDate'].patchValue(this.fullDate);
    this.planningYearForm.controls['startDate'].updateValueAndValidity();
    this.planningYearForm.controls['endDate'].patchValue(this.fullDate);
    this.planningYearForm.controls['endDate'].updateValueAndValidity();
  }

  getTranslation() {
    this.translate.get(['SN', "RELOAD", 'STATUS', "PLANNING_YEAR", "YEAR_CODE", "EDIT", "ACTION", "YEAR_NAME", "START_DATE", "END_DATE", "BS", "AD", "DELETE_ROW", "YES", "NO", "YESE", "NOE"]).subscribe((translation: [string]) => {
      this.transData = translation;
      this.loadGrid();
    });
  }

  loadGridData() {
    this.jqxLoader.open();
    this.pys.index({}).subscribe((res) => {
      if (res && res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  createForm() {
    this.planningYearForm = this.fb.group({
      'id': ['',],
      'yearCode': ['', Validators.required],
      'yearName': ['', Validators.required],
      'endDate': ['',Validators.compose([Validators.required, CustomValidators.checkDate])],
      'startDate': ['', Validators.compose([Validators.required, CustomValidators.checkDate])],
      'status': ['', Validators.required]
    });
  }
  ngAfterViewInit() {
    this.loadGridData();

  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'yearCode', type: 'string' },
          { name: 'yearName', type: 'string' },
          { name: 'endDate', type: 'string' },
          { name: 'startDate', type: 'string' },
          { name: 'status', type: 'string' },
        ],
        id: 'id',
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['YEAR_CODE'], datafield: 'yearCode', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: this.transData['YEAR_NAME'], datafield: 'yearName', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['START_DATE'], datafield: 'startDate', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['END_DATE'], datafield: 'endDate', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['STATUS'], datafield: 'status', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 80, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          console.info(dataRecord);
          let dt = {};
          dt['id'] = dataRecord['id'];
          dt['yearCode'] = dataRecord['yearCode'];
          dt['yearName'] = dataRecord['yearName'];
          dt['endDate'] = dataRecord['endDate'];
          dt['startDate'] = dataRecord['startDate'];
          dt['status'] = dataRecord['status'];
          this.planningYearForm.setValue(dt);
          this.update = true;
          this.selectedIndex = dataRecord['status'] == 'Y' ? 0 : 1;
        }
      }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

  }
  rowChange(event: any) {
    this.deleteRowIndexes.push(event.args.rowindex);
  }
  rowUnChange(event: any) {
    let index = this.deleteRowIndexes.indexOf(event.args.rowindex);
    if (index > -1) {
      this.deleteRowIndexes.splice(index, 1);
    }

  }
  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE_ROW'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.deleteRowIndexes;
      let ids = [];
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['id']);
      }

      if (ids.length > 0) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.pys.destroy(ids).subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.myGrid.clearselection();
              this.deleteRowIndexes = [];
              const messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.loadGridData();
            }
            if (result['error']) {
              this.myGrid.clearselection();
              this.deleteRowIndexes = [];
              const messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              this.loadGridData();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.log(error);
          });
        }
      } else {
        const messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadGridData();
    });

  }; //render toolbar ends

  saveBtn(post) {
    // post['status'] = post['status'] == "No" ? 'N' : 'Y';
    this.jqxLoader.open();
    this.pys.store(post).subscribe(
      result => {
        if (result['message']) {
          const messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.planningYearForm.reset();
          this.loadGridData();
        }
        this.jqxLoader.close();
        if (result['error']) {
          const messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.log(error);
      }
    );
  }

  updateBtn(post) {
    // post['status'] = post['status'] == "No" ? 'N' : 'Y';
    this.jqxLoader.open();
    this.pys.update(post['id'], post).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.update = false;
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.planningYearForm.reset();
          this.loadGridData();
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateBtn() {
    this.comboBox.val('');
    this.update = false;
    this.planningYearForm.reset();
    this.loadGridData();
  }

  getName($event) {
    console.log($event.target.value);
  }
}
