import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { TrainingCategoryMasterRoutingModule } from './training-category-master-routing.module';
import { TrainingCategoryMasterComponent } from './training-category-master.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TrainingCategoryMasterRoutingModule
  ],
  declarations: [TrainingCategoryMasterComponent]
})
export class TrainingCategoryMasterModule { }
