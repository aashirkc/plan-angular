import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrainingCategoryMasterComponent } from './training-category-master.component';

const routes: Routes = [
  {
    path: '',
    component: TrainingCategoryMasterComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingCategoryMasterRoutingModule { }