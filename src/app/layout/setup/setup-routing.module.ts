import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SetupComponent } from './setup.component';
import { KarkattiComponent } from './karkatti/karkatti.component';

const routes: Routes = [
    { path: 'karkatti', component: KarkattiComponent },
    {
        path: '',
        component: SetupComponent,
        children: [
            { path: '', redirectTo: 'planning-year', pathMatch: 'full' },
            {
                path: 'anugaman-samiti',
                loadChildren:
                    './anugaman-samiti/anugaman-samiti.module#AnugamanSamitiModule'
            },
            {
                path: 'planning-year',
                loadChildren:
                    './planning-year/planning-year.module#PlanningYearModule'
            },
            {
                path: 'activity-type-master',
                loadChildren:
                    './activity-master/activity-master.module#ActivityMasterModule'
            },
            {
                path: 'agency-type',
                loadChildren:
                    './agency-type/agency-type.module#AgencyTypeModule'
            },
            {
                path: 'inclusion-group',
                loadChildren:
                    './inclusion-group/inclusion-group.module#InclusionGroupModule'
            },
            {
                path: 'plan-unit',
                loadChildren: './plant-unit/plant-unit.module#PlantUnitModule'
            },
            {
                path: 'activity-focus-area-master',
                loadChildren:
                    './activity-focus-area-master/activity-focus-area-master.module#ActivityFocusAreaMasterModule'
            },
            {
                path: 'service-type-master',
                loadChildren:
                    './service-type-master/service-type-master.module#ServiceTypeMasterModule'
            },
            {
                path: 'assets-detail-type',
                loadChildren:
                    './assets-detail-type/assets-detail-type.module#AssetsDetailTypeModule'
            },
            {
                path: 'training-category-master',
                loadChildren:
                    './training-category-master/training-category-master.module#TrainingCategoryMasterModule'
            },
            {
                path: 'assets-details-sub-category',
                loadChildren:
                    './assets-details-sub-category/assets-details-sub-category.module#AssetsDetailsSubCategoryModule'
            },
            {
                path: 'assets-details-category',
                loadChildren:
                    './assets-details-category/assets-details-category.module#AssetsDetailsCategoryModule'
            },
            {
                path: 'organized-by-master',
                loadChildren:
                    './organized-by-master/organized-by-master.module#OrganizedByMasterModule'
            },
            {
                path: 'programs',
                loadChildren: './programs/programs.module#ProgramsModule'
            },
            {
                path: 'organization-master',
                loadChildren:
                    './organization-master/organization-master.module#OrganizationMasterModule'
            },
            {
                path: 'division',
                loadChildren: './division/division.module#DivisionModule'
            },
            {
                path: 'consumer-committee',
                loadChildren:
                    './consumer-committee/consumer-committee.module#ConsumerCommitteeModule'
            }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SetupRoutingModule {}
