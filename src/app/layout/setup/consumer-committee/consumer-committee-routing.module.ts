import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsumerCommitteeComponent } from './consumer-committee.component';

const routes: Routes = [
  { path: '', component: ConsumerCommitteeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsumerCommitteeRoutingModule { }
