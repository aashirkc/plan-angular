import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsumerCommitteeRoutingModule } from './consumer-committee-routing.module';
import { ConsumerCommitteeComponent } from './consumer-committee.component';
import { SharedModule } from 'app/shared/modules/shared.module';
import { NepaliInputComponent, NepaliTextareaComponent } from 'app/shared';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ConsumerCommitteeRoutingModule
  ],
  declarations: [ConsumerCommitteeComponent]
})
export class ConsumerCommitteeModule { }
