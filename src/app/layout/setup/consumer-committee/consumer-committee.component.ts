import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SetupsService, UnicodeTranslateService, NepaliNumberToWordService, DateConverterService } from '../../../shared';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

@Component({
  selector: 'app-consumer-committee',
  templateUrl: './consumer-committee.component.html',
  styleUrls: ['./consumer-committee.component.scss']
})
export class ConsumerCommitteeComponent implements OnInit {

    @ViewChild('planYear') planYear: jqxComboBoxComponent;

  foForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[] = [];
  dataId: any = '';
  checkMUn: boolean = false;
  b:any;
  deleteRowIndexes: Array<any> = [];
  monitoringAdapter: Array<any> = [
    {
      name: 'Yes',
      id: 'हो'
    },
    {
      name: 'No',
      id: 'होइन'
    }
  ];
  district: any = [
    { name: 'भोजपुर' },
    { name: 'धनकुटा' },
    { name: 'इलाम' },
    { name: 'झापा' },
    { name: 'खोटाँग' },
    { name: 'मोरंग' },
    { name: 'ओखलढुंगा' },
    { name: 'पांचथर' },
    { name: 'संखुवासभा' },
    { name: 'सोलुखुम्बू' },
    { name: 'सुनसरी' },
    { name: 'ताप्लेजुंग' },
    { name: 'तेह्रथुम' },
    { name: 'उदयपुर' },
    { name: 'सप्तरी' },
    { name: 'सिराहा' },
    { name: 'धनुषा' },
    { name: 'महोत्तरी' },
    { name: 'सर्लाही' },
    { name: 'बारा' },
    { name: 'पर्सा' },
    { name: 'रौतहट' },
    { name: 'सिन्धुली' },
    { name: 'रामेछाप' },
    { name: 'दोलखा' },
    { name: 'भक्तपुर' },
    { name: 'धादिङ' },
    { name: 'काठमाडौँ' },
    { name: 'ललितपुर' },
    { name: 'नुवाकोट' },
    { name: 'सिन्धुपाल्चोक' },
    { name: 'रसुवा' },
    { name: 'काभ्रेपलान्चोक' },
    { name: 'चितवन' },
    { name: 'मकवानपुर' },
    { name: 'गोरखा' },
    { name: 'कास्की' },
    { name: 'लमजुङ' },
    { name: 'स्याङग्जा' },
    { name: 'तनहुँ' },
    { name: 'मनाङ' },
    { name: 'नवलपुर' },
    { name: 'बागलुङ' },
    { name: 'म्याग्दी' },
    { name: 'पर्वत' },
    { name: 'मुस्ताङ' },
    { name: 'कपिलवस्तु' },
    { name: 'परासी' },
    { name: 'रुपन्देही' },
    { name: 'अर्घाखाँची' },
    { name: 'गुल्मी' },
    { name: 'पाल्पा' },
    { name: 'दाङ' },
    { name: 'प्युठान' },
    { name: 'रोल्पा' },
    { name: 'पूर्वी रूकुम' },
    { name: 'बाँके' },
    { name: 'बर्दिया' },
    { name: 'पश्चिमी रूकुम' },
    { name: 'सल्यान' },
    { name: 'डोल्पा' },
    { name: 'हुम्ला' },
    { name: 'जुम्ला' },
    { name: 'कालिकोट' },
    { name: 'मुगु' },
    { name: 'सुर्खेत' },
    { name: 'दैलेख' },
    { name: 'जाजरकोट' },
    { name: 'कैलाली' },
    { name: 'अछाम' },
    { name: 'डोटी' },
    { name: 'बझाङ' },
    { name: 'बाजुरा' },
    { name: 'कंचनपुर' },
    { name: 'डडेलधुरा' },
    { name: 'बैतडी' },
    { name: 'दार्चुला' },


  ];
  showMonitoringTable: boolean = false;
  workServiceTypeArrayData: any;
  monitoringArrayData: any;
  ccData: any = [];
  mcData: any = [];
  editId: any;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('myWindow') myWindow: jqxWindowComponent;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private unicode: UnicodeTranslateService,
    private ntw: NepaliNumberToWordService,
    private dcs: DateConverterService,
    private setupService: SetupsService
  ) {
    this.createForm();
  }
  createForm() {
    this.foForm = this.fb.group({
      address: [''],
      bankAccountNumber: [''],
      bankName: [''],
      createdDate: [''],
      karyalaya: [''],
      nagrpalika: [''],
      name: [''],
      nepalSarkar: [''],
      pradeshSarkar: [''],
      profitPopulation: [''],
      totalPopulation: [''],
      user: [''],
      ward: [''],
      bankAddress: [''],
      consumerCommitee: [''],
      workServiceTypeArray: this.fb.array([
        this.workServiceTypeForm()
      ]),
      monitoring: [''],
      monitoringCommitteeArray: this.fb.array([
        this.workMonitoringForm()
      ]),
    })
  }


  workServiceTypeForm() {
    return this.fb.group({
      committeAddress: [''],
      committeId: ['',],
      administrativeId: ['',],
      memberName: [''],
      memberPost: [{ value: '', disabled: true },],
      citizenshipNumber: [''],
      scanCopy: [''],
      issueDistrict: [''],
      issueDate: [''],
      phoneNumber: ['']
    });
  }
  private workMonitoringForm() {
    return this.fb.group({
      monitoringCommitteeAddress: [''],
      monitoringCommitteeName: [''],
      monitoringCommitteePost: [{ value: '', disabled: true },],
      monitoringCommitteeCitizenshipNo: [''],
      monitoringCommitteeMobile: ['']
    });
  }
  addMoreWorkServiceType() {
    this.workServiceTypeArrayData = this.foForm.controls["workServiceTypeArray"];
    this.workServiceTypeArrayData.push(this.workServiceTypeForm());
  }

  addMonitoringCommittee() {
    this.monitoringArrayData = this.foForm.controls[
      "monitoringCommitteeArray"
    ];

    this.monitoringArrayData.push(
      this.workMonitoringForm()
    );
  }
  ngOnInit() {

    
    // this.foForm.get('workServiceTypeArray')['controls'][].get('issueDistrict').patchValue(this.b[0]['district']);
    // console.info();
    this.loadGrid();
    this.checkWorkService();
    // this.jqxLoader.open();
    this.getDetails();
  }

  setDefaultDistrictSelected(){
    let a = localStorage.getItem('org')
    this.b = JSON.parse(a)
    // default
    const curr_district = this.b[0]['district'];
    
    let dist = [];
    for(let dis of this.district){
      (dis.name !== curr_district) && dist.push(dis); 
    }
    this.district = dist;

    this.district.push({
      name: curr_district,
    });

    const workServiceControls = this.foForm.get('workServiceTypeArray')['controls']
    for(let wsctl of workServiceControls){
      const issueDistrict = wsctl.controls['issueDistrict'];
      issueDistrict.setValue(curr_district);
    }

  }

  getDetails() {
    this.setupService.consumerCommitteeDetailsGet().subscribe(response => {
      this.source.localdata = response;
      this.myGrid.updatebounddata();
    });
  }

  addDetails() {

    this.foForm.reset();
    this.setDefaultDistrictSelected();
    this.foForm.patchValue({'monitoring': ''});
    this.monitoringChange('');
    // for(let i = 0; i<11; i++){
    //     this.foForm.get('workServiceTypeArray')['controls'][i].get('issueDistrict').patchValue(this.b[0]['district']);
    //     console.log(this.b[0]['district'])
    // }
    // this.foForm.get('workServiceTypeArray')['controls'][0].get('issueDistrict').patchValue(this.b[0]['district']);
    this.myWindow.draggable(true);
    this.myWindow.title('उपभोक्ता समिति विवरण');
    this.myWindow.open();
  }



  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'address', type: 'string' },
          { name: 'bankAccountNumber', type: 'string' },
          { name: 'bankName', type: 'string' },
          { name: 'consumerCommitee', type: 'string' },
          { name: 'createdDate', type: 'string' },
          { name: 'karyalaya', type: 'string' },
          { name: 'commiteeMember' },
          { name:'issueDistrict'},
          { name: 'anugamanCommitee', },
          { name: 'nagrpalika', type: 'string' },
          { name: 'nepalSarkar', type: 'string' },
          { name: 'pradeshSarkar', type: 'string' },
          { name: 'profitPopulation', type: 'string' },
          { name: 'totalPopulation', type: 'string' },
          { name: 'user', type: 'string' },
          { name: 'ward', type: 'string' },
          { name: 'name', type: 'string' }
        ],
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [

      {
        text: 'क्र स', sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        },
      },
      { text: 'नाम', datafield: 'name', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: 'ठेगाना', datafield: 'address', editable: false, width: 150, columntype: 'textbox',

      },
      { text: ' गठन मिति', datafield: 'createdDate', displayField: 'createdDate', editable: false, width: 100, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: 'उपस्थित संख्या', datafield: 'totalPopulation', displayField: 'totalPopulation', editable: false, columntype: 'textbox', width: 150, filtercondition: 'starts_with' },
      { text: 'लाभान्वितकाे संख्या', datafield: 'profitPopulation', displayField: 'profitPopulation', editable: false, columntype: 'textbox', width: 150, filtercondition: 'starts_with' },
      {
        text: 'सच्याउनु होस्', columntype: 'button', width: 90,
        cellsrenderer: (): string => {
          return "सच्याउनु होस्";
        },
        buttonclick: (row: number): void => {

          let dataRecord = this.myGrid.getrowdata(row);
          this.editId = dataRecord && dataRecord.id;
          let dt = {
            address: dataRecord && dataRecord['address'] || '',
            bankName: dataRecord && dataRecord['bankName'] || '',
            bankAccountNumber: dataRecord && dataRecord['bankAccountNumber'] || '',
            createdDate: dataRecord && dataRecord['createdDate'] || '',
            karyalaya: dataRecord && dataRecord['karyalaya'] || '',
            nagrpalika: dataRecord && dataRecord['nagrpalika'] || '',
            name: dataRecord && dataRecord['name'] || '',
            nepalSarkar: dataRecord && dataRecord['nepalSarkar'] || '',
            pradeshSarkar: dataRecord && dataRecord['pradeshSarkar'] || '',
            profitPopulation: dataRecord && dataRecord['profitPopulation'] || '',
            totalPopulation: dataRecord && dataRecord['totalPopulation'] || '',
            user: dataRecord && dataRecord['user'] || '',
            ward: dataRecord && dataRecord['ward'] || '',
            // issueDistrict: dataRecord && dataRecord['issueDistrict'] || '',
            bankAddress: dataRecord && dataRecord['bankAddress'] || '',
            consumerCommitee: dataRecord && dataRecord['consumerCommitee'] || '',
            monitoring: dataRecord && dataRecord['monitoring'] || '',
            monitoringCommitteeArray: [],
            workServiceTypeArray: [],

          };
          this.addDetails();
          this.foForm.patchValue(dt);

          let arrayData1 = dataRecord && dataRecord['commiteeMember'] || [];
          let arrayData2 = dataRecord && dataRecord['anugamanCommitee'] || [];
          if (arrayData2.length > 0) {
            this.showMonitoringTable = true;
            this.foForm.controls['monitoring'].setValue('Yes');
          } else {
            this.showMonitoringTable = false;
          }
          this.foForm.setControl('monitoringCommitteeArray', this.fb.array([]));
          this.foForm.setControl('workServiceTypeArray', this.fb.array([]));

          for (let i = 0; i < arrayData1.length; i++) {
            this.addMoreWorkServiceType();
            this.setEditData(i, arrayData1[i]);
          }

          for (let i = 0; i < arrayData2.length; i++) {
            this.addMonitoringCommittee();
            this.setEditAnugamanData(i, arrayData2[i]);
          }
        }
      },

    ];


  }

  setEditAnugamanData(index, data) {
    this.foForm.get('monitoringCommitteeArray')['controls'][index].get('monitoringCommitteeAddress').patchValue(data['monitoringCommitteeAddress']);
    this.foForm.get('monitoringCommitteeArray')['controls'][index].get('monitoringCommitteeCitizenshipNo').patchValue(data['monitoringCommitteeCitizenshipNo']);
    this.foForm.get('monitoringCommitteeArray')['controls'][index].get('monitoringCommitteePost').patchValue(data['monitoringCommitteePost'] || '');
    this.foForm.get('monitoringCommitteeArray')['controls'][index].get('monitoringCommitteeMobile').patchValue(data['monitoringCommitteeMobile']);
    this.foForm.get('monitoringCommitteeArray')['controls'][index].get('monitoringCommitteeName').patchValue(data['monitoringCommitteeName']);

  }

  setEditData(index, data) {
    let MemberPost = [
      { name: 'अध्यक्ष' },
      { name: 'उपाध्यक्ष' },
      { name: 'कोषाध्यक्ष' },
      { name: 'सचिव' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
    ];
    this.foForm.get('workServiceTypeArray')['controls'][index].get('committeAddress').patchValue(data['committeAddress']);
    this.foForm.get('workServiceTypeArray')['controls'][index].get('committeId').patchValue(data['committeId']);
    this.foForm.get('workServiceTypeArray')['controls'][index].get('administrativeId').patchValue(data['administrativeId']);
    this.foForm.get('workServiceTypeArray')['controls'][index].get('memberName').patchValue(data['memberName']);
    this.foForm.get('workServiceTypeArray')['controls'][index].get('memberPost').patchValue(MemberPost[index].name || '');
    this.foForm.get('workServiceTypeArray')['controls'][index].get('citizenshipNumber').patchValue(data['citizenshipNumber']);
    this.foForm.get('workServiceTypeArray')['controls'][index].get('scanCopy').patchValue(data['scanCopy']);
    this.foForm.get('workServiceTypeArray')['controls'][index].get('issueDistrict').patchValue(data['issueDistrict']);

    this.foForm.get('workServiceTypeArray')['controls'][index].get('issueDate').patchValue(data['issueDate']);
    this.foForm.get('workServiceTypeArray')['controls'][index].get('phoneNumber').patchValue(data['phoneNumber']);
  }

  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);


    let addGridButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: '<i class="fa fa-plus fa-f pa-2"></i> ' + 'समिति विवरण थप्नुहोस्', theme: 'energyblue' });
    // let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: 'मेटाउनु होस', theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw pa-2"></i> ' + 'पुन: लोड गर्नुहोस्', theme: 'energyblue' });

    addGridButton.addEventHandler('click', () => {
      this.addDetails();
    });
    // deleteRowButton.addEventHandler('click', (row: number) => {
    //   let id = this.deleteRowIndexes;
    //   let ids = [];
    //   for (let i = 0; i < id.length; i++) {
    //     let dataRecord = this.myGrid.getrowdata(Number(id[i]));
    //     ids.push(dataRecord['id']);
    //   }

    //   if (ids.length > 0) {
    //     if (confirm("के तपाईँ निश्चित हुनुहुन्छ ?")) {
    //       this.jqxLoader.open();
    //       this.setupService.consumerCommitteeDetailsDelete(ids).subscribe(result => {
    //         this.jqxLoader.close();
    //         if (result['message']) {
    //           this.myGrid.clearselection();
    //           this.deleteRowIndexes = [];
    //           let messageDiv: any = document.getElementById('message');
    //           messageDiv.innerText = result['message'];
    //           this.msgNotification.open();
    //           this.getDetails();
    //         }
    //         if (result['error']) {
    //           this.myGrid.clearselection();
    //           this.deleteRowIndexes = [];
    //           let messageDiv: any = document.getElementById('error');
    //           messageDiv.innerText = result['error']['message'];
    //           this.errNotification.open();
    //           this.myGrid.updatebounddata();
    //         }
    //       }, (error) => {
    //         this.jqxLoader.close();
    //         console.info(error);
    //       });
    //     }
    //   } else {
    //     let messageDiv = document.getElementById('error');
    //     messageDiv.innerText = 'Please select some item to delete';
    //     this.errNotification.open();
    //   }
    // })

    reloadGridButton.addEventHandler('click', () => {
      this.getDetails();
    });

  };
  checkWorkService() {
    let MemberPost = [
      { name: 'अध्यक्ष' },
      { name: 'उपाध्यक्ष' },
      { name: 'कोषाध्यक्ष' },
      { name: 'सचिव' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
    ];

    for (let i = 0; i < 11; i++) {
      if (i !== 0) {
        this.addMoreWorkServiceType();
      }
      this.foForm.get('workServiceTypeArray')['controls'][i].get('memberPost').patchValue(MemberPost[i].name);

      if (i < 6) {
        if (i === 1) {
          this.foForm.get('workServiceTypeArray')['controls'][i].get("committeAddress");
          this.foForm.get('workServiceTypeArray')['controls'][i].get("memberName");
          this.foForm.get('workServiceTypeArray')['controls'][i].get("memberPost");
          this.foForm.get('workServiceTypeArray')['controls'][i].get("citizenshipNumber");
          this.foForm.get('workServiceTypeArray')['controls'][i].get("issueDistrict");
          this.foForm.get('workServiceTypeArray')['controls'][i].get("issueDate");
          this.foForm.get('workServiceTypeArray')['controls'][i].get("phoneNumber");
        } else {
          this.foForm.get('workServiceTypeArray')['controls'][i].get("memberName").setValidators(Validators.required);
          this.foForm.get('workServiceTypeArray')['controls'][i].get("memberPost").setValidators(Validators.required);
          if (i === 4 || i === 5) {
            this.foForm.get('workServiceTypeArray')['controls'][i].get("committeAddress");
            this.foForm.get('workServiceTypeArray')['controls'][i].get("issueDistrict");
            this.foForm.get('workServiceTypeArray')['controls'][i].get("issueDate");
            this.foForm.get('workServiceTypeArray')['controls'][i].get("phoneNumber");
          } else {
            this.foForm.get('workServiceTypeArray')['controls'][i].get("committeAddress").setValidators(Validators.required);
          }
        }
      }
    }
  }
  committeeChange(value) {
    let data = this.foForm.get('workServiceTypeArray')['controls'][value].get('citizenshipNumber').value;
    if (data) {
      for (let i = 0; i <= value - 1; i++) {
        let pp = this.foForm.get('workServiceTypeArray')['controls'][i].get('citizenshipNumber').value;
        if (data == pp) {
          this.foForm.get('workServiceTypeArray')['controls'][value].get('citizenshipNumber').patchValue('');
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = 'कृपया ना.प्र.नं सबैकाे बेग्लाबेग्लै लेख्नुहाेला';
          this.errNotification.open();
          break;
        }
      }

    }
  }
  monitoringChange(value) {
    this.foForm.setControl('monitoringCommitteeArray', this.fb.array([]));
    if (value == 'Yes') {
      this.showMonitoringTable = true;
      let MemberPost = [
        { name: 'संयोजक' },
        { name: 'सदस्य' },
        { name: 'सदस्य' },
        { name: 'सदस्य' },
        { name: 'सदस्य' },
      ];
      for (let i = 0; i < 5; i++) {
        this.addMonitoringCommittee();
        this.foForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteePost').patchValue(MemberPost[i].name);

        if (i < 3) {
          this.foForm.get('monitoringCommitteeArray')['controls'][i].get("monitoringCommitteeName").setValidators(Validators.required);
        }

      }
    } else {
      this.showMonitoringTable = false;
    }
  }
  mCommitteeChange(value) {
    let data = this.foForm.get('monitoringCommitteeArray')['controls'][value].get('monitoringCommitteeCitizenshipNo').value;
    if (data) {
      console.log(data);
      for (let i = 0; i <= value - 1; i++) {
        let pp = this.foForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteeCitizenshipNo').value;
        if (data == pp) {
          this.foForm.get('monitoringCommitteeArray')['controls'][value].get('monitoringCommitteeCitizenshipNo').patchValue('');
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = 'कृपया ना.प्र.नं सबैकाे बेग्लाबेग्लै लेख्नुहाेला';
          this.errNotification.open();
          break;
        }
      }

    }
  }
  save() {
    const userData = JSON.parse(localStorage.getItem('pcUser'))

    let a = this.foForm.getRawValue();
    let ccDetails = {
      address: a['address'],
      bankAccountNumber: a['bankAccountNumber'],
      bankName: a['bankName'],
      createdDate: a['createdDate'],
      karyalaya: a['karyalaya'],
      nagrpalika: a['nagrpalika'],
      name: a['name'],
      nepalSarkar: a['nepalSarkar'],
      pradeshSarkar: a['pradeshSarkar'],
      profitPopulation: a['profitPopulation'],
      totalPopulation: a['totalPopulation'],
      user: a['user'],
      ward: a['ward'],
      bankAddress: a['bankAddress'],
      consumerCommitee: a['consumerCommitee'],
      id: this.editId,
      wardCode: userData && userData['orgId'],
    };
    let ccmDetails = a['workServiceTypeArray']
    let mcmDetails = a['monitoringCommitteeArray'];
    this.setupService.consumerCommitteeDetailsPost(ccDetails).subscribe((response) => {
      let setupId = response && response['data'];
      if (response['message'] === 'Success') {
        this.setupService.consumerCommitteeMemberPost(setupId, ccmDetails).subscribe((response1) => {
          if (response1['message'] === 'Success') {
            this.setupService.monitoringCommiteePost(setupId, mcmDetails).subscribe((response2) => {
              if (response2['message'] === 'Success') {
                let messageDiv: any = document.getElementById('message');
                messageDiv.innerText = 'Success';
                this.msgNotification.open();
                this.getDetails();
                this.myWindow.close();
                this.createForm();
                this.checkWorkService();
              } else {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = 'error';
                this.errNotification.open();
              }
            }, error => {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = error;
              this.errNotification.open();
            });
          } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'error';
            this.errNotification.open();
          }
        }, error => {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = error;
          this.errNotification.open();
        });

      }
      else {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'error';
        this.errNotification.open();
      }
    }, error => {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = error;
      this.errNotification.open();
    })
  }
}
