import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizedByMasterComponent } from './organized-by-master.component';

describe('OrganizedByMasterComponent', () => {
  let component: OrganizedByMasterComponent;
  let fixture: ComponentFixture<OrganizedByMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizedByMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizedByMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
