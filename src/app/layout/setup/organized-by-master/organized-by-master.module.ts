import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganizedByMasterRoutingModule } from './organized-by-master-routing.module';
import { OrganizedByMasterComponent } from './organized-by-master.component';
import { SharedModule } from '../../../shared/modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    OrganizedByMasterRoutingModule
  ],
  declarations: [OrganizedByMasterComponent]
})
export class OrganizedByMasterModule { }
