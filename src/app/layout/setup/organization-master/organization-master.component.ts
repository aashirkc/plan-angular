import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DateConverterService, ActivityOutputService, ActivityFocusAreaMasterService, PlanningYearService, InclusionGroupService, CurrentUserService, OrganizedByMasterService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';

@Component({
  selector: 'app-organization-master',
  templateUrl: './organization-master.component.html',
  styleUrls: ['./organization-master.component.scss']
})
export class OrganizationMasterComponent implements OnInit {

  activityForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  transData: any;
  planYearAdapter: Array<any> = [];
  activityAdapter: Array<any> = [];
  activityFocusAreaAdapter: any = [];
  activityCategoryAdapter: any = [];
  userData: any = {};
  SearchDatas: Array<any> = [];
  API_URL_DOC: any;
  API_URL: any;
  lang: any;
  id: any ;


  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('myViewWindow') myViewWindow: jqxWindowComponent;
  @ViewChild('myEditWindow') myEditWindow: jqxWindowComponent;
  @ViewChild('myReportWindow') myReportWindow: jqxWindowComponent;
  @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;

  constructor(
    private fb: FormBuilder,
    private cus: CurrentUserService,
    private dfs: DateConverterService,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    @Inject('API_URL') API_URL: string,
    private cdr: ChangeDetectorRef,
    private pys: PlanningYearService,
    private igs: InclusionGroupService,
    private afams: ActivityFocusAreaMasterService,
    private activityOutputService: ActivityOutputService,
    private translate: TranslateService,
    private router: Router,
    private org: OrganizedByMasterService

  ) {
    this.createForm();
    this.getTranslation();
    this.userData = this.cus.getTokenData();
    this.API_URL_DOC = API_URL_DOC;
    this.API_URL = API_URL;
  }



  ngOnInit() {
    this.lang = this.translate.currentLang.substr(0,2);

    this.org.indexOrgMaster({}).subscribe(res=>{
        this.planYearAdapter=res;
        localStorage.setItem('org',  JSON.stringify(res))

        console.log(this.planYearAdapter)

        this.postData(this.planYearAdapter);
    })
    // this.pys.index({}).subscribe((res) => {
    //   this.planYearAdapter = res;
    //   if (this.planYearAdapter.length > 0) {
    //     let planCode = this.planYearAdapter[0].yearCode || '';
    //     this.activityForm.get('planYear').patchValue(planCode);
    //     this.SearchData(this.activityForm.value)
    //   }

    // }, (error) => {
    //   console.info(error);
    // });
    // this.igs.index({}).subscribe((res) => {
    //   this.activityAdapter = res;
    // }, (error) => {
    //   console.info(error);
    // });
    // this.afams.show(0).subscribe((res) => {
    //   this.activityFocusAreaAdapter = res;
    // }, (error) => {
    //   console.info(error);
    // });
  }

postData(x){

    this.id = x[0]['id']
    this.activityForm.get('orgType').patchValue(this.planYearAdapter[0]['orgType']);
    this.activityForm.get('orgName').patchValue(this.planYearAdapter[0]['orgName']);
    this.activityForm.get('officeName').patchValue(this.planYearAdapter[0]['officeName']);
    this.activityForm.get('province').patchValue(this.planYearAdapter[0]['province']);
    this.activityForm.get('placeName').patchValue(this.planYearAdapter[0]['placeName']);
    this.activityForm.get('nameOfHead').patchValue(this.planYearAdapter[0]['nameOfHead']);
    this.activityForm.get('post').patchValue(this.planYearAdapter[0]['post']);
    this.activityForm.get('officeTelNo').patchValue(this.planYearAdapter[0]['officeTelNo']);
    this.activityForm.get('headTelNo').patchValue(this.planYearAdapter[0]['headTelNo']);
     this.activityForm.get('district').patchValue(this.planYearAdapter[0]['district']);

}



  getTranslation() {
    this.translate.get(['SN', 'regNo', 'DETAIL_VIEW', 'universityRoll', 'ADD', 'EDIT', 'NAME', 'nepali', 'english', 'mobile', 'VIEW', 'DETAIL', "EDIT", "ACTION", "UPDATE", "DELETESELECTED", "RELOAD",
      "ACTIVITY_NAME", "ACTIVITY_TYPE", "ACTIVITY_FOCUS_AREA", "ACTIVITY_FOR"]).subscribe((translation: [string]) => {
        this.transData = translation;

      });
  }

//   loadGridData() {
//     let post = {};
//     post = this.activityForm.value;
//     if (post['status']) {

//     }
//   }

  createForm() {
    this.activityForm = this.fb.group({
      'orgName': [''],
      'orgType': [''],
      'officeName':[''],
      'placeName': [''],
      'district': [''],
      'province': [''],
      'nameOfHead': [''],
      'post': [''],
      'headTelNo': [''],
      'officeTelNo': [''],
    });
  }

  ngAfterViewInit() {
//     this.loadGridData();
//     let formData = JSON.parse(localStorage.getItem('ManageActivity'));
//     if (formData) {
//       if (formData['activityFocusArea']) {
//         this.LoadActivityCategory(formData);
//       } else {
//         this.activityForm.setValue(formData);
//         this.SearchData(formData);
//         setTimeout(() => {
//           localStorage.removeItem('ManageActivity');
//         }, 100);
//       }
//     }
//     this.cdr.detectChanges();
//   }

//   LoadActivityCategory(post?: any) {
//     if (post) {
//       let data = post;
//       if (data['activityFocusArea']) {
//         this.jqxLoader.open();
//         this.afams.show(data['activityFocusArea']).subscribe((res) => {
//           this.activityCategoryAdapter = res;
//           // Setting Data from local Storage
//           let formData = JSON.parse(localStorage.getItem('ManageActivity'));
//           this.activityForm.setValue(formData);
//           this.SearchData(formData);
//           setTimeout(() => {
//             localStorage.removeItem('ManageActivity');
//           }, 100);
//           this.jqxLoader.close();
//         }, (error) => {
//           console.info(error);
//           this.jqxLoader.close();
//         });
//       }
//     } else {
//       let data = this.activityForm.value;
//       if (data['activityFocusArea']) {
//         this.jqxLoader.open();
//         this.afams.show(data['activityFocusArea']).subscribe((res) => {
//           this.activityCategoryAdapter = res;
//           this.jqxLoader.close();
//         }, (error) => {
//           console.info(error);
//           this.jqxLoader.close();
//         });
//       }
//     }
//   }

//   loadGrid() {
//     this.source =
//       {
//         datatype: 'json',
//         datafields: [
//           { name: 'id', type: 'string' },
//           { name: 'planYear', type: 'string' },
//           { name: 'activityForName', type: 'string' },
//           { name: 'activityFor', type: 'string' },
//           { name: 'focusAreaLevel1', type: 'string' },
//           { name: 'focusAreaLevel1Name', type: 'string' },
//           { name: 'activityName', type: 'string' },
//           { name: 'activityCategory', type: 'string' },
//           { name: 'activityTypeName', type: 'string' },
//           { name: 'a_ApprovedBy', type: 'string' },
//           { name: 't_ApprovedBy', type: 'string' },

//         ],
//         id: 'id',
//         localdata: [],
//         pagesize: 100
//       }

//     this.dataAdapter = new jqx.dataAdapter(this.source);
//     this.columns = [
//       {
//         text: this.transData['SN'], sortable: false, filterable: false, editable: false,
//         groupable: false, draggable: false, resizable: false,
//         datafield: 'id', columntype: 'number', width: 50,
//         cellsrenderer: function (row, column, value) {
//           return "<div style='margin:4px;'>" + (value + 1) + "</div>";
//         }
//       },
//       { text: this.transData['ACTIVITY_NAME'],  datafield: 'activityName', columntype: 'textbox', editable: false, },
//       { text: this.transData['ACTIVITY_TYPE'], datafield: 'activityTypeName', columntype: 'textbox',width:150, editable: false },
//       { text: this.transData['ACTIVITY_FOCUS_AREA'],  datafield: 'focusAreaLevel1', displayfield: "focusAreaLevel1Name",width:150, columntype: 'textbox', editable: false, },
//       { text: this.transData['ACTIVITY_FOR'], datafield: 'activityFor', displayfield: "activityForName", columntype: 'textbox', width:150 , editable: false },
//       {
//         text: this.transData['VIEW'], datafield: 'View', sortable: false, filterable: false, width: 100, columntype: 'button', columngroup: 'Actions',
//         cellsrenderer: (): string => {
//           return this.transData['DETAIL_VIEW'];
//         },
//         buttonclick: (row: number): void => {
//           localStorage.setItem('ManageActivity', JSON.stringify(this.activityForm.value));
//           this.editrow = row;
//           let dataRecord = this.myGrid.getrowdata(this.editrow);
//           if (dataRecord['id']) {
//             this.router.navigate(['/activity/view-activity', { id: dataRecord['id'] }]);
//           }
//         }
//       },
//       {
//         text: this.transData['EDIT'], datafield: 'Edit', sortable: false, filterable: false, width: 100, columntype: 'button', columngroup: 'Actions',
//         cellsrenderer: (): string => {
//           return this.transData['EDIT'];
//         },
//         buttonclick: (row: number): void => {
//           localStorage.setItem('ManageActivity', JSON.stringify(this.activityForm.value));
//           this.editrow = row;
//           let dataRecord = this.myGrid.getrowdata(this.editrow);
//           if(dataRecord['t_ApprovedBy']<=0  || dataRecord['t_ApprovedBy']== null){
//           if (dataRecord['id']) {
//             this.jqxLoader.open();
//             console.log(dataRecord["id"]);
//             this.router.navigate(['/activity/activity-details/edit/', dataRecord['id']]);
//           }
//         } else {
//           let messageDiv: any = document.getElementById('error');
//           messageDiv.innerText = 'स्वीकृत भइसकेकाे छ !!';
//           this.errNotification.open();
//         }
//         }
//       },
//     ];
//   }

//   columngroups: any[] =
//     [
//       { text: 'कार्य', align: 'center', name: 'Actions' }
//     ];


//     planYearSelected($event){
//         let post = {}
//         post = this.activityForm.value;
//         this.SearchData(post)
//     }




}


saveData(post){

   if(this.planYearAdapter.length == 0){
       this.org.orgStore(post).subscribe(res=>{
           console.log(res);

           if (res['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = res['message'];
            this.msgNotification.open();
           } if (res['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = res['error']['message'];
            this.errNotification.open();
          }
       });
   }else{
       this.org.orgUpdate(this.id, post).subscribe(res=>{

           console.log("sucess on editing")

           if (res['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = res['message'];
            this.msgNotification.open();
           }
           if (res['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = res['error']['message'];
            this.errNotification.open();
          }
       },error=>{
        if (error) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = error['error']['message'];
            this.errNotification.open();
       }
   });
}


//   SearchData(post) {
//     this.jqxLoader.open();
//     this.activityOutputService.index(post).subscribe((res) => {
//       if (res.length == 1 && res[0].error) {
//         let messageDiv: any = document.getElementById('error');
//         messageDiv.innerText = res[0].error;
//         this.errNotification.open();
//         this.source.localdata = [];
//         this.SearchDatas = [];
//       } else {
//         this.source.localdata = res;
//         this.SearchDatas = res;
//       }
//       this.myGrid.updatebounddata();
//       this.jqxLoader.close();
//     }, (error) => {
//       console.info(error);
//       this.jqxLoader.close();
//     });
//   }
}
}
