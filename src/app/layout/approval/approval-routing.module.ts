import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApprovalComponent } from './approval.component';

const routes: Routes = [
    {
        path: '',
        component: ApprovalComponent,
        children: [
            { path: '', redirectTo: 'administrative-approval', pathMatch: 'full' },
            { path: 'administrative-approval', loadChildren: './administrative-approval/administrative-approval.module#AdministrativeApprovalModule' },
            { path: 'administrative-approval/:id', loadChildren: './administrative-approval-entry/administrative-approval-entry.module#AdministrativeApprovalEntryModule' },

            { path: 'administrative-approval/edit/:id', loadChildren: './administrative-approval-edit/administrative-approval-edit.module#AdministrativeApprovalEditModule' },
            { path: 'administrtive-approval-aggrement/:id', loadChildren: './administrtive-approval-aggrement/administrtive-approval-aggrement.module#AdministrtiveApprovalAggrementModule' },
            { path: 'administrative-approval/view/:id', loadChildren: './administrative-approval-view/administrative-approval-view.module#AdministrativeApprovalViewModule' },

            { path: 'technical-approval', loadChildren: './technical-approval/technical-approval.module#TechnicalApprovalModule' },
            { path: 'administrative-approval-karyadesh/:id', loadChildren: './administrative-approval-karyadesh/administrative-approval-karyadesh.module#AdministrativeApprovalKaryadeshModule' },
            { path: 'tipani-adesh/:id', loadChildren: './tipani-adesh/tipani-adesh.module#TipaniAdeshModule' },
            { path: 'estimate-abstract-of-cost', loadChildren: './estimate-abstract-of-cost/estimate-abstract-of-cost.module#EstimateAbstractOfCostModule' },

        ]
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ApprovalRoutingModule { }
