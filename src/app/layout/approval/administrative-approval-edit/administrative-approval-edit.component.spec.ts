import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrativeApprovalEditComponent } from './administrative-approval-edit.component';

describe('AdministrativeApprovalEditComponent', () => {
  let component: AdministrativeApprovalEditComponent;
  let fixture: ComponentFixture<AdministrativeApprovalEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrativeApprovalEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrativeApprovalEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
