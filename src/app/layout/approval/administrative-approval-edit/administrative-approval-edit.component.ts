import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { AdministrativeApprovalService, ActivityDetailService, AdministrativeApproval, CustomValidators, EstimateDateConverterService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-administrative-approval-edit',
  templateUrl: './administrative-approval-edit.component.html',
  styleUrls: ['./administrative-approval-edit.component.scss']
})
export class AdministrativeApprovalEditComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  contractStatus: boolean = false;
  approvalEntryForm: FormGroup;
  activityDetailId: string;
  activityDetailData: any;
  workServiceTypeArrayData: any;
  monitoringArrayData: any;
  showTable: boolean = false;
  contractTable: boolean = false;
  showAmanantaTable: boolean = false;
  dataWorkTable: boolean = false;
  contractSelectedFiles: any = [];
  showArray: boolean = false;
  contractDocData: any;
  technicalCost: string;
  administrativeDetail: any;
  selectedFiles: any = [];
  imagesLists: any = [];
  docUrl: any;
  committeeEdidData: any = [];
  grantedFromOthers: any = [];
  checkOtherGranted: boolean = false;
  showMonitoringTable: boolean = false;
  contractEdidData: any;
  userData: any = {};
  workServiceTypeAdapter: any = [
    {
      name: 'Consumer Committe',
      id: 'उपभोक्ता समिति'
    },
    {
      name: 'Contract',
      id: 'ठेक्का मार्फत'
    },
    {
      name: 'Amanathan Marfat',
      id: 'अमानत मार्फत'
    }, { name: 'Sahakari', id: 'श्रम सहकारी / संस्था मार्फत' }
  ];
  monitoringAdapter: Array<any> = [
    {
      name: 'Yes',
      id: 'हो'
    },
    {
      name: 'No',
      id: 'होइन'
    }
  ];

  showButton: boolean = true;
  totalDurationDays: any;
  totalDurationMonth: any;
  isRul: boolean;
  monitoringCommitteConfig: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private ads: ActivityDetailService,
    private aas: AdministrativeApprovalService,
    private eds: EstimateDateConverterService,
    @Inject('API_URL_DOC') docUrl: string,
    private location: Location
  ) {
    this.createForm();
    this.docUrl = docUrl;
  }

  ngOnInit() {
    let userDetalils = JSON.parse(localStorage.getItem('pcUser'));
    this.userData = userDetalils['userType'];
    this.isRul = false;
    const orgMaster = JSON.parse(localStorage.getItem('org'));
    if (orgMaster && orgMaster[0]) {
      const orgType = orgMaster[0]['orgType'];
      this.isRul = orgType === 'RUL';
    }

    // console.log(this.approvalEntryForm);
  }

  ngAfterViewInit() {
    this.activityDetailId = this.route.snapshot.paramMap.get('id');
    this.getActivityDetail(this.activityDetailId);
    let post = { activity: this.activityDetailId };

    this.aas.getTechnicalDetail(post).subscribe(
      result => {
        this.technicalCost = result && result[0] && result[0].techCost || null;
      },
      error => {
        console.log(error);
      }
    );

    this.getAdministrativeDetail();

  }

  addButton() {
    this.addMoreWorkServiceType(false);
  }

  getAdministrativeDetail() {
    this.approvalEntryForm.setControl('workServiceTypeArray', this.fb.array([]));
    this.approvalEntryForm.setControl('monitoringCommitteeArray', this.fb.array([]));
    this.aas.index({ activity: this.activityDetailId, form: '' }).subscribe(
      result => {
        let data = result['PlanAgreement'][0];
        this.approvalEntryForm.controls['workOrderDate'].patchValue(result['data'] && result['data']['workOrderDate']);
        this.approvalEntryForm.controls['issuingAuthority'].patchValue(result['data'] && result['data']['officeName']);
        this.approvalEntryForm.controls['workServiceType'].patchValue(result['data'] && result['data']['workServiceType']);
        setTimeout(() => {
          this.approvalEntryForm.controls['agreementDate'].patchValue(result['data'] && result['data']['agreementDate']);
          this.approvalEntryForm.controls['estimatedWorkCompletionDate'].patchValue(result['data'] && result['data']['estimatedWorkCompletionDate']);
        }, 100);
        let newData = result && result['data'] || {};
        newData['befinitedOrganizations'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].befinitedOrganizations || '';
        newData['benifitedHouses'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].benifitedHouses || '';
        newData['benifitedPopulation'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].benifitedPopulation || '';
        newData['femalePopulation'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0].femalePopulation || '';
        newData['malePopulation'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0].malePopulation || '';
        newData['dalitPopulation'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0].dalitPopulation || '';
        newData['janajatiPopulation'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0].janajatiPopulation || '';
        newData['othersPopulation'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0].othersPopulation || '';
        newData['consumerCommitteAddress'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].consumerCommitteAddress || '';
        newData['consumerCommitteName'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].consumerCommitteName || '';
        newData['grantedFromConsumerCommitte'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].grantedFromConsumerCommitte || '';
        newData['grantedFromIngo'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].grantedFromIngo || '';
        newData['grantedFromLocalLevel'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].grantedFromLocalLevel || '';
        newData['grantedFromNgo'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].grantedFromNgo || '';
        newData['grantedFromOthers'] = result && result['PlanAgreement'] && result['PlanAgreement'].grantedFromOthers || '';
        newData['grantedFromState'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].grantedFromState || '';
        //newData['grantedFromUnion'] = result && result['PlanAgreement'] && result['PlanAgreement'].grantedFromUnion || '';
        newData['othersBenifited'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].othersBenifited || '';
        newData['formationDate'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].formationDate || '';
        newData['totalBenefited'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].totalBenefited || '';

        newData['totalParticipant'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].totalParticipant || '';
        newData['bankName'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].bankName || '';
        newData['bankAddress'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].bankAddress || ''; newData['acNo'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].acNo || '';
        this.imagesLists = result && result['image'] || [];
        this.administrativeDetail = newData;
        this.committeeEdidData = result['CommitteDetails'];
        this.grantedFromOthers = result['otherGranted'];
        if (this.grantedFromOthers.length > 0) {
          this.showArray = true;
          setTimeout(() => {
            this.checkOtherGranted = true;
          }, 100);
        }
        // console.log(newData['workServiceType'])
        this.contractEdidData = result['contractDetails'];
        if (newData['workServiceType'] === 'Consumer Committe' || newData['workServiceType'] === 'Sahakari') {
          this.showTable = true;
          this.dataWorkTable = true;
          this.contractTable = false;
          this.showMonitoringTable = true;
          for (let i = 0; i < this.committeeEdidData.length; i++) {
            this.addMoreWorkServiceType();
            this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].patchValue(this.committeeEdidData[i]);
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('memberName').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].memberName || '');
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('id').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].id || '');
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('memberPost').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].memberPost || '');
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('committeAddress').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].committeAddress || '');
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('citizenshipNumber').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].citizenshipNumber || '');
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('issueDistrict').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].issueDistrict || '');
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('issueDate').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].issueDate || '');
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('phoneNumber').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].phoneNumber || '');

          }
          this.approvalEntryForm.get('workServiceTypeArray').updateValueAndValidity();
          this.approvalEntryForm.setControl('othersDonators', this.fb.array([]));
          for (let i = 0; i < this.grantedFromOthers.length; i++) {
            this.addItem();
            this.approvalEntryForm.get('othersDonators')['controls'][i].get('name').patchValue(this.grantedFromOthers[i] && this.grantedFromOthers[i].grantedName);
            this.approvalEntryForm.get('othersDonators')['controls'][i].get('id').patchValue(this.grantedFromOthers[i] && this.grantedFromOthers[i].id);
            this.approvalEntryForm.get('othersDonators')['controls'][i].get('value').patchValue(this.grantedFromOthers[i] && this.grantedFromOthers[i].value);
          }
          if (result['monitoringCommittee'].length > 0) {
            setTimeout(() => {
              this.approvalEntryForm.controls['monitoring'].setValue('Yes');
            }, 100);
            for (let i = 0; i < result['monitoringCommittee'].length; i++) {
              this.addMonitoringCommittee();
              this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteeAddress').patchValue(result['monitoringCommittee'][i] && result['monitoringCommittee'][i].address);
              this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteeName').patchValue(result['monitoringCommittee'][i] && result['monitoringCommittee'][i].name);
              this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteePost').patchValue(result['monitoringCommittee'][i] && result['monitoringCommittee'][i].post);
              this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteeCitizenshipNo').patchValue(result['monitoringCommittee'][i] && result['monitoringCommittee'][i].citizenshipNo);
              // this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteeFatherName').patchValue(result['monitoringCommittee'][i] && result['monitoringCommittee'][i].fatherName);
              this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteeMobile').patchValue(result['monitoringCommittee'][i] && result['monitoringCommittee'][i].mobile);
            }
          } else {
            setTimeout(() => {
              this.approvalEntryForm.controls['monitoring'].setValue('No');
            }, 100);
            this.showMonitoringTable = false;
          }
        }

        if (newData['workServiceType'] === 'Contract') {
          this.showTable = false;
          this.contractTable = true;
          this.contractDocData = result['contractDetailsDoc'] || [];
          this.approvalEntryForm.setControl('contractData', this.fb.array([]));
          // debugger;
          for (let i = 0; i < this.contractEdidData.length; i++) {
            this.addItem1(i);
            this.approvalEntryForm.get('contractData')['controls'][i].get('cid').patchValue(this.contractEdidData[i] && this.contractEdidData[i].id);
            this.approvalEntryForm.get('contractData')['controls'][i].get('docId').patchValue(this.contractDocData[i] && this.contractDocData[i].id);
            this.approvalEntryForm.get('contractData')['controls'][i].get('attatchment').patchValue(this.contractDocData[i] && this.contractDocData[i].attatchment);
            this.approvalEntryForm.get('contractData')['controls'][i].get('attachmentData').patchValue(this.contractDocData[i] && this.contractDocData[i].attatchment);
            this.approvalEntryForm.get('contractData')['controls'][i].get('name').patchValue(this.contractEdidData[i] && this.contractEdidData[i].name);
            this.approvalEntryForm.get('contractData')['controls'][i].get('address').patchValue(this.contractEdidData[i] && this.contractEdidData[i].address);
            this.approvalEntryForm.get('contractData')['controls'][i].get('contactPerson').patchValue(this.contractEdidData[i] && this.contractEdidData[i].contactPerson);
            this.approvalEntryForm.get('contractData')['controls'][i].get('contactNumber').patchValue(this.contractEdidData[i] && this.contractEdidData[i].contactNumber);
            this.approvalEntryForm.get('contractData')['controls'][i].get('contractAmount').patchValue(this.contractEdidData[i] && this.contractEdidData[i].contractAmount);
            this.approvalEntryForm.get('contractData')['controls'][i].get('contractDate').patchValue(this.contractEdidData[i] && this.contractEdidData[i].contractDate);
            this.approvalEntryForm.get('contractData')['controls'][i].get('selected').patchValue(this.contractEdidData[i] && this.contractEdidData[i].selected);
          }
          // console.log(this.contractEdidData);
          // newData['name'] = this.contractEdidData && this.contractEdidData['name'];
          // newData['address'] = this.contractEdidData && this.contractEdidData['address'];
          // newData['contactPerson'] = this.contractEdidData && this.contractEdidData['contactPerson'];
          // newData['contactNumber'] = this.contractEdidData && this.contractEdidData['contactNumber'];
          // newData['contractDate'] = this.contractEdidData && this.contractEdidData['contractDate'];
          // newData['contractAmount'] = this.contractEdidData && this.contractEdidData['contractAmount'];
          newData['contractBankAcNo'] = this.contractEdidData && this.contractEdidData['bankAcNo'];
          newData['contractBankName'] = this.contractEdidData && this.contractEdidData['bankName'];
          newData['bankName'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].bankName || '';
          newData['bankAddress'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].bankAddress || '';
          newData['contractBankAcHolder'] = this.contractEdidData && this.contractEdidData['bankAcHolder'];
        }
        if (newData['workServiceType'] === 'Amanathan Marfat') {
          this.showTable = false;
          this.contractTable = false;
          this.showAmanantaTable = true;
        }
        this.administrativeDetail = newData;
        let formData = new AdministrativeApproval(this.administrativeDetail);
        this.jqxLoader.open();
        setTimeout(() => {
          this.jqxLoader.close();
          this.approvalEntryForm.patchValue(formData);
        }, 300);
        this.jqxLoader.open();
        setTimeout(() => {
          this.jqxLoader.close();
          this.approvalEntryForm.get('wadaRepresentative').patchValue(result['data'] && result['data']['wadaRepresentative']);
          this.approvalEntryForm.get('officeName').patchValue(result['data'] && result['data']['officeName']);
          this.approvalEntryForm.get('orderName').patchValue(result['data'] && result['data']['orderName']);
          this.approvalEntryForm.get('acNo').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['acNo']);
          this.approvalEntryForm.get('bankName').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['bankName']); //= result['PlanAgreement'][0]['bankName']
          this.approvalEntryForm.get('bankAddress').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['bankAddress']);
          this.approvalEntryForm.get('befinitedOrganizations').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['befinitedOrganizations']);
          this.approvalEntryForm.get('benifitedHouses').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['benifitedHouses']);
          this.approvalEntryForm.get('benifitedPopulation').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['benifitedPopulation']);
          this.approvalEntryForm.get('consumerCommitteAddress').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['consumerCommitteAddress']);
          this.approvalEntryForm.get('consumerCommitteName').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['consumerCommitteName']);
          this.approvalEntryForm.get('formationDate').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['formationDate']);
          this.approvalEntryForm.get('totalParticipant').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['totalParticipant']);
          this.approvalEntryForm.get('totalBenefited').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['totalBenefited']);

          this.approvalEntryForm.get('grantedFromConsumerCommitte').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['grantedFromConsumerCommitte']);
          this.approvalEntryForm.get('grantedFromIngo').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['grantedFromIngo']);
          this.approvalEntryForm.get('grantedFromLocalLevel').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['grantedFromLocalLevel']);
          this.approvalEntryForm.get('grantedFromNgo').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['grantedFromNgo']);
          this.approvalEntryForm.get('grantedFromOthers').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['grantedFromOthers']);
          this.approvalEntryForm.get('grantedFromState').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['grantedFromState']);
          //this.approvalEntryForm.get('grantedFromUnion').patchValue(result['PlanAgreement'][0]['grantedFromUnion']);
          this.approvalEntryForm.get('othersBenifited').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['othersBenifited']);
        }, 300);
      },
      error => {
        console.log(error);
      }
    )
  }

  selectChange(a: any) {
    let len = this.approvalEntryForm.get('contractData')['controls'].length;
    for (let i = 0; i < len; i++) {
      if (i != a) {
        this.approvalEntryForm.get('contractData')['controls'][i].get("selected").setValue(0)
      }
    }
  }

  addNewContract() {
    let length = this.approvalEntryForm.get('contractData')['controls'].length;
    const control1 = <FormArray>this.approvalEntryForm.controls['contractData'];
    control1.push(this.initContractForm());
    this.approvalEntryForm.get('contractData')['controls'][length].get("name").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("address").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("contactPerson").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("contactNumber").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("contractDate").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("contractAmount").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("attatchment").setValidators(Validators.required);
  }

  addItem1(length) {
    const control1 = <FormArray>this.approvalEntryForm.controls['contractData'];
    control1.push(this.initContractForm());
    this.approvalEntryForm.get('contractData')['controls'][length].get("name").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("address").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("contactPerson").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("contactNumber").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("contractDate").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("contractAmount").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("attatchment").setValidators(Validators.required);
  }

  committeeAddressChange($event) {
    for (let wsControls of this.approvalEntryForm.get('workServiceTypeArray')['controls']) {
      wsControls.patchValue({
        'committeAddress': $event.target.value
      });
    }
    for (let wsControls of this.approvalEntryForm.get('monitoringCommitteeArray')['controls']) {
      wsControls.patchValue({
        'monitoringCommitteeAddress': $event.target.value
      });
    }
  }

  /**
 * Create the form group
 * with given form control name
 */
  createForm() {
    this.approvalEntryForm = this.fb.group({
      // 'prasasanikSakha': ['', Validators.required],
      'workOrderDate': [null],
      'issuingAuthority': [null, Validators.required],
      'workServiceType': ['', Validators.required],
      'wadaRepresentative': [''],
      // 'name': [''],
      // 'address': [''],
      // 'contactPerson': [''],
      // 'contactNumber': [''],
      // 'contractDate': [''],
      // 'contractAmount': [0],
      // 'attatchment': [''],
      'consumerCommitteName': [''],
      'consumerCommitteAddress': [''],
      'formationDate': [''],
      'totalParticipant': [0],
      'totalBenefited': [0],
      'bankName': [''],
      'bankAddress': [''],
      'acNo': [''],
      'monitoring': [''],
      'contractBankAcNo': [''],
      'contractBankName': [''],
      'contractBankAcHolder': [''],
      //'grantedFromUnion': [''],
      'grantedFromState': [0],
      'grantedFromLocalLevel': [0],
      'grantedFromNgo': [0],
      'grantedFromIngo': [0],
      'grantedFromConsumerCommitte': [0],
      'grantedFromOthers': [false],
      'benifitedHouses': [0],
      'benifitedPopulation': [0],
      'befinitedOrganizations': [0],
      'othersBenifited': [0],
      'estimatedWorkCompletionDate': [null, Validators.compose([Validators.required, CustomValidators.checkDate])],
      'agreementDate': [null, Validators.compose([Validators.required, CustomValidators.checkDate])],
      'malePopulation': [''],
      'femalePopulation': [''],
      'dalitPopulation': [''],
      'janajatiPopulation': [''],
      'othersPopulation': [''],
      'officeName': [''],
      'orderName': [''],
      workServiceTypeArray: this.fb.array([
        this.workServiceTypeForm()
      ]),
      monitoringCommitteeArray: this.fb.array([
        this.workMonitoringForm()
      ]),
      'othersDonators': this.fb.array([
        this.initOtherItems(),
      ]),
      'contractData': this.fb.array([
        this.initContractForm(),
      ]),
    });
    console.log(this.approvalEntryForm);
  }

  initOtherItems() {
    return this.fb.group({
      name: ['',],
      value: ['',],
      id: ['']
    });
  }

  initContractForm() {
    return this.fb.group({
      cid: null,
      docId: null,
      name: [''],
      address: [''],
      contactPerson: [''],
      contactNumber: [''],
      contractDate: [''],
      contractAmount: [''],
      attatchment: [''],
      selected: ['0'],
      attachmentData: [null]
    });
  }

  addDaysToDate(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }
  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  changeAgreementDate(date) {
    // let dateData = date;
    // let tFMdays = Number(this.totalDurationMonth) * 30;
    // let tFddays = Number(this.totalDurationDays);
    // let totalDays = tFMdays + tFddays;
    // let patt = new RegExp(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/);
    // if (patt.test(date)) {
    //   this.approvalEntryForm.controls['estimatedWorkCompletionDate'].setValue(this.formatDate(this.addDaysToDate(dateData, totalDays)));
    // }
    const _formData1 = {
      'activity': this.activityDetailId,
      'contractDate': date
    }
    // console.log(_formData1);

    // this.eds.getDate(_formData1).subscribe((result) => {
    //   console.log(result);
    //   this.approvalEntryForm.controls['estimatedWorkCompletionDate'].setValue(result['completionDate']);
    //   this.contractStatus = false;
    // },
    //   (error) => {
    //     this.contractStatus = false;
    //     console.log(error);
    //   });
    this.contractStatus = false;
  }

  addItem() {
    const control1 = <FormArray>this.approvalEntryForm.controls['othersDonators'];
    control1.push(this.initOtherItems());
  }
  removeItem(i: number) {
    const control1 = <FormArray>this.approvalEntryForm.controls['othersDonators'];
    control1.removeAt(i);
  }
  check() {
    this.contractStatus = true;
  }
  sum1() {

    if (this.approvalEntryForm.get('benifitedPopulation').value != 0) {
      let totalPoultaion = this.approvalEntryForm.get('benifitedPopulation').value
      // console.log(totalPoultaion)

      let femalePopulation = this.approvalEntryForm.get('femalePopulation').value

      // console.log(femalePopulation)
      let malePopulation = totalPoultaion - femalePopulation
      // console.log(malePopulation)
      this.approvalEntryForm.get('malePopulation').patchValue(malePopulation)
    } else {
      console.log('no total popultaion')
    }

  }
  sum2() {
    if (this.approvalEntryForm.get('benifitedPopulation').value != 0) {
      let totalPoultaion = this.approvalEntryForm.get('benifitedPopulation').value
      // console.log(totalPoultaion)
      let malePopulation = this.approvalEntryForm.get('malePopulation').value
      // console.log(malePopulation)
      let femalePopulation = totalPoultaion - malePopulation
      // console.log(femalePopulation)
      this.approvalEntryForm.get('femalePopulation').patchValue(femalePopulation)
    } else {
      console.log('no total pouplation')
    }

  }
  //   sum2(){
  //       console.log('hey2')
  //   }


  private workServiceTypeForm(required: boolean = true) {
    if (window['wpms_config']['monitoringCommitteeDescription']) {
      this.monitoringCommitteConfig = window['wpms_config']['monitoringCommitteeDescription'];
      const config = {};
      for (const data of window['wpms_config']['monitoringCommitteeDescription']) {
        config[data.name] = ['']
      }
      const group = this.fb.group(config);
      return group;
    }
    return this.fb.group({
      committeAddress: [''],
      committeId: ['',],
      administrativeId: ['',],
      memberName: [''],
      memberPost: [''],
      citizenshipNumber: [''],
      scanCopy: [''],
      issueDistrict: [null],
      issueDate: [''],
      phoneNumber: ['']
    });
  }

  private workMonitoringForm() {
    return this.fb.group({
      monitoringCommitteeAddress: [''],
      monitoringCommitteeName: [''],
      monitoringCommitteePost: [{ value: '', disabled: false },],
      monitoringCommitteeCitizenshipNo: [''],
      // monitoringCommitteeFatherName: [''],
      monitoringCommitteeMobile: ['']
    });
  }


  addMoreWorkServiceType(required = true) {
    this.workServiceTypeArrayData = this.approvalEntryForm.controls[
      "workServiceTypeArray"
    ];

    this.workServiceTypeArrayData.push(
      this.workServiceTypeForm(required)
    );
  }

  addMonitoringCommittee() {
    this.monitoringArrayData = this.approvalEntryForm.controls[
      "monitoringCommitteeArray"
    ];

    this.monitoringArrayData.push(
      this.workMonitoringForm()
    );
  }

  removeServiceType(i) {
    this.workServiceTypeArrayData.removeAt(i);
  }

  removeForm(i) {
    this.workServiceTypeArrayData.removeAt(i);
  }

  checkWorkService(event) {
    if (event.target.value === 'Consumer Committe' || event.target.value === 'Sahakari') {
      this.showTable = true;
      this.contractTable = false;
      this.showAmanantaTable = false;
      this.showButton = true;
    } else if (event.target.value == 'Contract') {
      this.showTable = false;
      this.contractTable = true;
      this.showAmanantaTable = false;
      this.showButton = false;

    } else if (event.target.value == 'Amanathan Marfat') {
      this.showTable = false;
      this.contractTable = false;
      this.showAmanantaTable = true;
      this.showButton = false;

    }
  }
  citizenshipAttachmentFlies: any[] = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}];

  onFileChange(event, i) {
    if (event.target.files && event.target.files[0]) {
      var formData: FormData = new FormData();
      let file = event.target.files[0];
      let obj = {
        file: file,
        index: 'Y'
      };
      this.citizenshipAttachmentFlies[i] = obj;
    } else {
      this.citizenshipAttachmentFlies[i] = {};
    }
  }

  getActivityDetail(id) {
    this.jqxLoader.open();
    this.ads.show(id).subscribe(
      result => {
        this.activityDetailData = result;
        this.totalDurationDays = result && result['totalDurationDays'];
        this.totalDurationMonth = result && result['totalDurationMonth'];

        this.jqxLoader.close();
      },
      error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  handleFileInput(event) {
    this.selectedFiles = event.target.files;
  }

  handleFileInputContract(i, event) {

    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.approvalEntryForm.get('contractData')['controls'][i].patchValue({
        attachmentData: file
      });
    }
  }

  monitoringChange(value) {
    if (value == 'Yes') {
      this.showMonitoringTable = true;
      // let MemberPost = [
      //   { name: 'संयोजक' },
      //   { name: 'सदस्य' },
      //   { name: 'सदस्य' },
      //   { name: 'सदस्य' },
      //   { name: 'सदस्य' },
      // ];
      // for (let i = 0; i < 5; i++) {
      //   this.addMonitoringCommittee();
      //   this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteePost').patchValue(MemberPost[i].name);

      //   if (i < 3) {
      //     this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get("monitoringCommitteeName").setValidators(Validators.required);
      //     // this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get("monitoringCommitteeFatherName");
      //     //   this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get("monitoringCommitteeAddress").setValidators(Validators.required);
      //     //   this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get("monitoringCommitteeCitizenshipNo").setValidators(Validators.required);
      //     //   this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get("monitoringCommitteeMobile").setValidators(Validators.required);
      //   }

      // }
    } else {
      this.showMonitoringTable = false;
    }
  }
  /**
 * Function triggered when save button is clicked
 * @param formData
 */
  save(formData) {
    //   if (this.approvalEntryForm.invalid) {
    //     for (const name in this.approvalEntryForm.controls) {
    //       if (this.approvalEntryForm.controls[name].invalid) {
    //         const id = name
    //         console.log(id);

    //       }
    //   }
    // }
    formData['grantedFromUnion'] = '0';
    const _formData = new FormData();
    _formData.append('activity', this.activityDetailId);
    _formData.append('id', this.administrativeDetail['id']);
    for (const data in formData) {
      if (data === 'workServiceTypeArray') {
        for (let j = 0; j < formData[data].length; j++) {
          for (const config of this.monitoringCommitteConfig) {
            _formData.append(config.name, formData[data][j][config.name] || '');
          }
          // _formData.append('committeAddress', formData[data][j].committeAddress || '');
          // _formData.append('committeId', formData[data][j].id || '');
          // _formData.append('memberName', formData[data][j].memberName || '');
          // _formData.append('memberPost', formData[data][j].memberPost || '');
          // _formData.append('citizenshipNumber', formData[data][j].citizenshipNumber || '');
          // _formData.append('issueDistrict', formData[data][j].issueDistrict || '');
          // _formData.append('phoneNumber', formData[data][j].phoneNumber || '');
          // _formData.append('issueDate', formData[data][j].issueDate || '');
          // if (this.citizenshipAttachmentFlies[j] && this.citizenshipAttachmentFlies[j].file) {
          //   _formData.append('scanCopy', this.citizenshipAttachmentFlies[j].file, this.citizenshipAttachmentFlies[j].file.fileName);
          //   _formData.append('attchStatus', this.citizenshipAttachmentFlies[j].index);
          // } else {
          //   _formData.append('scanCopy', null);
          //   _formData.append('attchStatus', 'N');
          // }
        }
      } else if (data == 'monitoringCommitteeArray') {
        for (let j = 0; j < formData[data].length; j++) {
          _formData.append('monitoringCommitteePost', formData[data][j].monitoringCommitteePost);
          _formData.append('monitoringCommitteeName', formData[data][j].monitoringCommitteeName);
          // _formData.append('monitoringCommitteeFatherName', formData[data][j].monitoringCommitteeFatherName);
          _formData.append('monitoringCommitteeAddress', formData[data][j].monitoringCommitteeAddress);
          _formData.append('monitoringCommitteeCitizenshipNo', formData[data][j].monitoringCommitteeCitizenshipNo);
          _formData.append('monitoringCommitteeMobile', formData[data][j].monitoringCommitteeMobile);
        }
      } else {
        _formData.append(data, formData[data]);
      }

    }
    for (let i = 0; i < this.selectedFiles.length; i++) {
      let fileIndex = i + 1;
      _formData.append('imageName', this.selectedFiles[i], this.selectedFiles[i].name)
    }
    if (this.contractSelectedFiles.length > 0) {
      for (let i = 0; i < this.contractSelectedFiles.length; i++) {
        let fileIndex = i + 1;
        _formData.append('attatchment', this.contractSelectedFiles[i], this.contractSelectedFiles[i].name)
      }
    }
    for (let i = 0; i < formData['othersDonators'].length; i++) {
      _formData.append('grantedFromOthersName', formData['othersDonators'][i].name);
      _formData.append('grantedFromOthersNameValue', formData['othersDonators'][i].value);
      _formData.append('grantedFromOthersId', formData['othersDonators'][i].id || 0);
    }
    if (this.userData.toLowerCase() == 'super admin' || this.userData.toLowerCase() == 'admin') {
      _formData.append("approvedBy", this.administrativeDetail['approvedBy']);
      //   _formData.append("approvedBy", JSON.parse(localStorage.getItem('pcUser'))['userId']);
    }
    for (let data of formData.contractData) {
      _formData.append('cid', data.cid);
      _formData.append('docId', data.docId);
      _formData.append('name', data.name);
      _formData.append('address', data.address);
      _formData.append('contactPerson', data.contactPerson);
      _formData.append('contactNumber', data.contactNumber);
      _formData.append('contractDate', data.contractDate);
      _formData.append('contractAmount', data.contractAmount);
      _formData.append('selected', data.selected);
      _formData.append('attachment', data.attachmentData);
      // _formData.append('')
    }
    this.jqxLoader.open();
    _formData.append('userId', JSON.parse(localStorage.getItem('pcUser'))['userId'])
    if (this.administrativeDetail.id) {
      this.aas.store(_formData).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();

            setTimeout(() => {
              this.location.back();
            }, 2000);

          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Administrative Approval Id Missing, Please Reload Page';
      this.errNotification.open();
    }

  }

  removeItem1(i: number) {
    let id = this.approvalEntryForm.get('contractData')['controls'][i].get('cid').value;
    if (id != null) {
      if (this.contractDocData[i]) {
        this.DeleteDoc(this.contractDocData[i]['id'])
      }
      this.aas.deleteContract(id).subscribe(
        result => {
          if (result['message']) {
            this.jqxLoader.close();
            this.getAdministrativeDetail();
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            // window.location.reload();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    }
    else {
      const control2 = <FormArray>this.approvalEntryForm.controls['contractData'];
      control2.removeAt(i);
    }
  }

  DeletePhoto(imgName) {
    this.jqxLoader.open();
    this.aas.delete(imgName).subscribe(
      result => {
        if (result['message']) {
          this.jqxLoader.close();
          this.getAdministrativeDetail();
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          // window.location.reload();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );

  }

  DeleteDoc(id) {
    this.jqxLoader.open();
    this.aas.delete1(id).subscribe(
      result => {
        if (result['message']) {
          this.jqxLoader.close();
          this.getAdministrativeDetail();
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          // window.location.reload();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );

  }

  resetForm() {
    this.approvalEntryForm.reset();
    // this.myComboBox.clearSelection();
  }
  close() {
    // this.router.navigate(['/approval/administrative-approval']);
    this.location.back();
  }

  printPreview() {
    this.router.navigate(['/approval/administrtive-approval-aggrement', this.activityDetailId]);
  }
  checkValue(event: any) {
    if (event == 'A') {
      this.approvalEntryForm.setControl('othersDonators', this.fb.array([]));
      this.showArray = true;
    }
    if (event == 'B') {
      this.approvalEntryForm.setControl('othersDonators', this.fb.array([]));
      this.showArray = false;
    }
  }
}
