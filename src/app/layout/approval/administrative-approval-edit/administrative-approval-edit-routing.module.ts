import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministrativeApprovalEditComponent } from './administrative-approval-edit.component';

const routes: Routes = [
  {
    path: '',
    component: AdministrativeApprovalEditComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrativeApprovalEditRoutingModule { }
