import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrativeApprovalEditComponent } from './administrative-approval-edit.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AdministrativeApprovalEditRoutingModule } from './administrative-approval-edit-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdministrativeApprovalEditRoutingModule
  ],
  declarations: [AdministrativeApprovalEditComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AdministrativeApprovalEditModule { }
