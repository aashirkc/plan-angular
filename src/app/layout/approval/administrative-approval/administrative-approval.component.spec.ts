import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrativeApprovalComponent } from './administrative-approval.component';

describe('AdministrativeApprovalComponent', () => {
  let component: AdministrativeApprovalComponent;
  let fixture: ComponentFixture<AdministrativeApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrativeApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrativeApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
