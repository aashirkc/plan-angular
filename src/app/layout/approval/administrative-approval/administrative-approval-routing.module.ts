import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministrativeApprovalComponent } from './administrative-approval.component';

const routes: Routes = [
  {
    path: '',
    component: AdministrativeApprovalComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrativeApprovalRoutingModule { }
