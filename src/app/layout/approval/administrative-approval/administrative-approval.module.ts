import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrativeApprovalComponent } from './administrative-approval.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AdministrativeApprovalRoutingModule } from './administrative-approval-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdministrativeApprovalRoutingModule,
  ],
  declarations: [AdministrativeApprovalComponent],
  // schemas: [ NO_ERRORS_SCHEMA ]
})
export class AdministrativeApprovalModule { }
