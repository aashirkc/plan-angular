import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DateConverterService, ActivityOutputService, ActivityFocusAreaMasterService, PlanningYearService, InclusionGroupService, CurrentUserService, AgencyTypeService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';

@Component({
  selector: 'app-administrative-approval',
  templateUrl: './administrative-approval.component.html',
  styleUrls: ['./administrative-approval.component.scss']
})
export class AdministrativeApprovalComponent implements OnInit {

  approvalSearchForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  transData: any;
  userType: any;
  planYearAdapter: Array<any> = [];
  activityAdapter: Array<any> = [];
  activityFocusAreaAdapter: any = [];
  activityCategoryAdapter: any = [];
  approveAdapter: any = [
    { id: '1', name: "स्वीकृत भएको" },
    { id: '0', name: "स्वीकृत नभएको" },
    ];
  userData: any = {};
  SearchDatas: Array<any> = [];
  API_URL_DOC: any;
  API_URL: any;
  columngroups: any[] = [];
  lang: any;
  agencyType: any = [];
  childType: any = [];
  agencyName: any;
  workServiceTypeAdapter: any = [
    {
      name: 'Consumer Committe',
      id: 'उपभोक्ता समिति'
    },
    {
      name: 'Contract',
      id: 'ठेक्का मार्फत'
    },
    {
      name: 'Amanathan Marfat',
      id: 'अमानत मार्फत'
    },{ name: 'Sahakari', id: 'श्रम सहकारी / संस्था मार्फत' }
  ];
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('myViewWindow') myViewWindow: jqxWindowComponent;
  @ViewChild('myEditWindow') myEditWindow: jqxWindowComponent;
  @ViewChild('myReportWindow') myReportWindow: jqxWindowComponent;
  @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;

  constructor(
    private fb: FormBuilder,
    private cus: CurrentUserService,
    private ats: AgencyTypeService,
    private dfs: DateConverterService,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    @Inject('API_URL') API_URL: string,
    private cdr: ChangeDetectorRef,
    private pys: PlanningYearService,
    private igs: InclusionGroupService,
    private afams: ActivityFocusAreaMasterService,
    private activityOutputService: ActivityOutputService,
    private translate: TranslateService,
    private router: Router
  ) {
    this.createForm();
    this.getTranslation();
    this.userData = this.cus.getTokenData();
    this.API_URL_DOC = API_URL_DOC;
    this.API_URL = API_URL;
  }

  ngOnInit() {

    this.ats.show(0).subscribe(
      result => {
        if (result['length'] > 0) {

          this.agencyType = result;
        }
      },
      error => {
        console.log(error);
      }
    );

    this.lang = this.translate.currentLang.substr(0,2);
    let userDetalils = JSON.parse(localStorage.getItem('pcUser'));
    this.userData = userDetalils['userType'];
    this.pys.index({}).subscribe((res) => {
      this.planYearAdapter = res;
      if (this.planYearAdapter.length > 0) {
        let planCode = this.planYearAdapter[0].yearCode || '';
        this.approvalSearchForm.get('planYear').patchValue(planCode);
        this.SearchData(this.approvalSearchForm.value)
      }
    }, (error) => {
      console.info(error);
    });
    this.igs.index({}).subscribe((res) => {
      this.activityAdapter = res;
    }, (error) => {
      console.info(error);
    });
    this.afams.show(0).subscribe((res) => {
      this.activityFocusAreaAdapter = res;
    }, (error) => {
      console.info(error);
    });

    this.columngroups =
      [
        { text: this.transData['ACTION'], align: 'center', name: 'Actions' }
      ];
  }
  getTranslation() {
    this.translate.get(['SN', 'ADD', 'APPROVE', 'Approved', 'Approve', 'EDIT', 'NAME', 'nepali', 'english', 'mobile', 'VIEW', 'DETAIL', "ACTIONS", "UPDATE", "DELETESELECTED", "RELOAD",
      "CHOOSE", "CHOOSE_ACTIVITY_FOCUS", "CHOOSE_CATEGORY", "ACTIVITY_NAME", "ACTIVITY_TYPE", "ACTIVITY_FOCUS_AREA",
      "ACTIVITY_FOR", "STATUS", "ACTION"]).subscribe((translation: [string]) => {
        this.transData = translation;
        this.loadGrid();
      });
  }

  loadGridData() {
    let post = {};
    post = this.approvalSearchForm.value;
    if (post['status']) {
      this.SearchData(post);
    }
  }

  createForm() {
    this.approvalSearchForm = this.fb.group({
      'planYear': ['', Validators.required],
      'activityFor': [''],
      'activityFocusArea': ['', Validators.required],
      'activityCategory': ['', Validators.required],
      'activityName': [''],
      'approveState': [''],
      'parent':[''],
      'child':[''],
      'workServiceType':['']
    });
  }

  implementingAgencyChange(selectedEvent) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.agencyName = displayText;
      let agencyCode = selectedEvent.target.value;
      this.getAgencyType(agencyCode);
    } else {
      this.approvalSearchForm.get('child').patchValue('');
      this.childType = '';
    }
  }
  getAgencyType(agencyCode = 0, index = 0, type = null) {
    // remove previously added form control if the parent select is changed.
    this.ats.show(agencyCode).subscribe(
      result => {
        if (result['length'] > 0) {
          this.childType = result;
        } else {
          this.approvalSearchForm.get('child').patchValue('');
          this.childType = '';
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  ngAfterViewInit() {
    this.loadGridData();
    let formData = JSON.parse(localStorage.getItem('AdministrativeApproval'));
    if (formData) {
      if (formData['activityFocusArea']) {
        this.LoadActivityCategory(formData);
      } else {
        this.approvalSearchForm.setValue(formData);
        this.SearchData(formData);
        setTimeout(() => {
          localStorage.removeItem('AdministrativeApproval');
        }, 100);
      }
    }
    this.cdr.detectChanges();
  }

  LoadActivityCategory(post?: any) {
    if (post) {
      let data = post;
      if (data['activityFocusArea']) {
        this.jqxLoader.open();
        this.afams.show(data['activityFocusArea']).subscribe((res) => {
          this.activityCategoryAdapter = res;
          // Setting Data from local Storage
          let formData = JSON.parse(localStorage.getItem('AdministrativeApproval'));
          this.approvalSearchForm.setValue(formData);
          this.SearchData(formData);
          setTimeout(() => {
            localStorage.removeItem('AdministrativeApproval');
          }, 100);
          this.jqxLoader.close();
        }, (error) => {
          console.info(error);
          this.jqxLoader.close();
        });
      }
    } else {
      let data = this.approvalSearchForm.value;
      if (data['activityFocusArea']) {
        this.jqxLoader.open();
        this.afams.show(data['activityFocusArea']).subscribe((res) => {
          this.activityCategoryAdapter = res;
          this.jqxLoader.close();
        }, (error) => {
          console.info(error);
          this.jqxLoader.close();
        });
      }
    }
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'planYear', type: 'string' },
          { name: 'activityForName', type: 'string' },
          { name: 'activityFor', type: 'string' },
          { name: 'focusAreaLevel1', type: 'string' },
          { name: 'focusAreaLevel1Name', type: 'string' },
          { name: 'activityName', type: 'string' },
          { name: 'activityCategory', type: 'string' },
          { name: 'activityTypeName', type: 'string' },
          { name: 'outputType', type: 'string' },
          { name: 'administrativeApproval', type: 'string' },
          { name: "executionReportStatus", type: "string" },
          { name: "administrativeMessage", type: "string" },
          { name: "taStatus", type: "string" },
          { name: "a_ApprovedBy", type: "string" },
          { name: "t_ApprovedBy", type: "string" }
        ],
        id: 'id',
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['ACTIVITY_NAME'], datafield: 'activityName', columntype: 'textbox', editable: false },
      { text: this.transData['ACTIVITY_TYPE'], datafield: 'activityTypeName', columntype: 'extbox', editable: false, width: 120 },
      { text: this.transData['ACTIVITY_FOCUS_AREA'], datafield: 'focusAreaLevel1', displayfield: "focusAreaLevel1Name", columntype: 'textbox', editable: false, width: 120 },
      { text: 'लक्षित <br>समावेशी समूह', datafield: 'activityFor', displayfield: "activityForName", columntype: 'textbox', editable: false, width: 100, },
      {
        text: "स्वीकृत स्थिति", datafield: "approved_by", width: 90, columntype: "textbox", editable: false,
        cellsrenderer: (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
          let cellValue = "";
          if (rowdata.a_ApprovedBy > 0) {
            cellValue = '<span style="font-weight: bold;color: #158232;"> स्वीकृत भएको </span>'
          } else if (rowdata.a_ApprovedBy == null) {
            cellValue = '<span style="font-weight: bold;color:red;"> स्वीकृत नभएको </span>';
          } else if (rowdata.a_ApprovedBy == 0){
            cellValue = '<span style="font-weight: bold;color:red;"> स्वीकृत नभएको </span>';
          } else if (rowdata.a_ApprovedBy == -1) {
            cellValue = '<span style="font-weight: bold;color:red;"> अस्वीकृत भएको </span>';;
          } else {
            cellValue = '<span style="font-weight: bold;color: #158232;"> स्वीकृत भएको </span>'
          }
          return '<div style="padding:4px 3px;">' + cellValue + '</div>';
        }
      },
      {
        text: this.transData['APPROVE'], datafield: 'outputType', sortable: false, filterable: false, width: 100, columntype: 'button', columngroup: 'Actions',
        cellsrenderer: (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          if (dataRecord['administrativeApproval'] != 'N') {
            return 'अनुमोदित';
        } else if (dataRecord['administrativeApproval'] == 'N') {
            return "अनुमोदन गर्नुहोस";
        }else{
          return "अनुमोदन गर्नुहोस";
        }
        },
        buttonclick: (row: number): void => {
          let data = this.myGrid.getrowdata(row);
          if (data.t_ApprovedBy > 0) {
            if (data.taStatus == "N") {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = 'प्राविधिक अनुमोदन गरिएको छैन';
              this.errNotification.open();
            } else {
              localStorage.setItem('AdministrativeApproval', JSON.stringify(this.approvalSearchForm.value));
              this.editrow = row;
              let dataRecord = this.myGrid.getrowdata(this.editrow);
                if (dataRecord['outputType'] == null) {
                  let messageDiv: any = document.getElementById('error');
                  messageDiv.innerText = 'कृपया पहिले गतिविधि परिभाषित गर्नुहोस्!!';
                  this.errNotification.open();
                } else {
                  if (this.userData.toLowerCase() != 'technical manager') {
                    if (dataRecord['id']) {
                      if (dataRecord['administrativeApproval'] == 'N') {
                        this.jqxLoader.open();
                        this.router.navigate(['/approval/administrative-approval/', dataRecord['id']]);
                      } else {
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = 'तपाईले पहिलेनै अनुमोदन गर्नुभएको छ !!';
                        this.errNotification.open();
                      }
                    }

                  } else {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = 'तपाईलार्इ याे कार्य गर्न स्वीकृती दिएकाे छैन !!';
                    this.errNotification.open();
                  }
                }
            }

          } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'प्राविधिक स्वीकृत भएको छैन';
            this.errNotification.open();
          }
        }
      },
      {
        text: this.transData['EDIT'], datafield: 'administrativeApproval', sortable: false, filterable: false, width: 100, columntype: 'button', columngroup: 'Actions',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {

          let data = this.myGrid.getrowdata(row);
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          if (dataRecord['administrativeApproval'] != 'N') {
            if(dataRecord['a_ApprovedBy'] > 0 ){
            if (this.userData.toLowerCase() == 'admin'||this.userData.toLowerCase() == 'super admin') {
                this.router.navigate(['/approval/administrative-approval/edit/', dataRecord['id']]);
              } else {
                  let messageDiv: any = document.getElementById('error');
                  messageDiv.innerText = 'तपाईलार्इ याे कार्य गर्न स्वीकृती दिएकाे छैन !!';
                  this.errNotification.open();
                }
              }else{
                this.router.navigate(['/approval/administrative-approval/edit/', dataRecord['id']]);

              }
        }
        else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'अनुमोदन भएको छैन !!';
            this.errNotification.open();
          }
          // if (this.userData.toLowerCase() == 'super user') {
          //   if (dataRecord['id']) {
          //     this.jqxLoader.open();
          //     this.router.navigate(['/approval/administrative-approval/edit/', dataRecord['id']]);
          //   }
          // } else if (data.a_ApprovedBy > 0) {
          //   let messageDiv: any = document.getElementById('error');
          //   messageDiv.innerText = 'स्विकृत भएको';
          //   this.errNotification.open();
          // } else {
          //   localStorage.setItem('AdministrativeApproval', JSON.stringify(this.approvalSearchForm.value));

          //   if (dataRecord['administrativeApproval'] == 'N') {
          //     let messageDiv: any = document.getElementById('error');
          //     messageDiv.innerText = 'कृपया पहिले गतिविधि अनुमोदन गर्नुहोस्!!';
          //     this.errNotification.open();
          //   } else {
          //     if (this.userData.toLowerCase() == 'program approver' || this.userData.toLowerCase() == 'program preparer' || this.userData.toLowerCase() == 'admin') {
          //       if (dataRecord['id']) {
          //         this.jqxLoader.open();
          //         this.router.navigate(['/approval/administrative-approval/edit/', dataRecord['id']]);
          //       }
          //     }
          //     else {
          //       let messageDiv: any = document.getElementById('error');
          //       messageDiv.innerText = 'तपाईलार्इ याे कार्य गर्न स्वीकृती दिएकाे छैन !!';
          //       this.errNotification.open();

          //     }
          //   }
          // }
        }
      },
      {
        text: this.transData['VIEW'], datafield: 'view', sortable: false, filterable: false, width: 60, columntype: 'button', columngroup: 'Actions',
        cellsrenderer: (): string => {
          return this.transData['VIEW'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          if (dataRecord['administrativeApproval'] == 'N') {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'कृपया पहिले गतिविधि अनुमोदन गर्नुहोस्!!';
            this.errNotification.open();
          } else {
            localStorage.setItem('AdministrativeApproval', JSON.stringify(this.approvalSearchForm.value));
            this.editrow = row;
            let dataRecord = this.myGrid.getrowdata(this.editrow);
            this.router.navigate(['/approval/administrative-approval/view/', dataRecord['id']]);
          }
        }
      },
      {
        text: 'सम्झौता फाराम', datafield: 'viewCommittee', sortable: false, filterable: false, width: 90, columntype: 'button', columngroup: 'Actions',
        cellsrenderer: (): string => {
          return 'प्रिन्ट';
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          if (dataRecord['id']) {
            this.router.navigate(['/approval/administrtive-approval-aggrement', dataRecord['id']]);
          }
        }
      },
      {
        text: "कार्यादेश", sortable: false, filterable: false, width: 58, columntype: 'button', columngroup: 'Actions',
        cellsrenderer: (): string => {
          return 'प्रिन्ट';
        },
        buttonclick: (row: number): void => {
          localStorage.setItem('AdministrativeApproval', JSON.stringify(this.approvalSearchForm.value));
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          if (dataRecord['a_ApprovedBy'] <= 0) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'स्विकृत नभएको';
            this.errNotification.open();
          }else {
          this.router.navigate(['/approval/administrative-approval-karyadesh/', dataRecord['id']]);
          }
        }
      },
      {
        text: "टिप्पणी आदेश", sortable: false, filterable: false, width: 90, columntype: 'button', columngroup: 'Actions',
        cellsrenderer: (): string => {
          return 'प्रिन्ट';
        },
        buttonclick: (row: number): void => {
          localStorage.setItem('AdministrativeApproval', JSON.stringify(this.approvalSearchForm.value));
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          if (Number(dataRecord['a_ApprovedBy']) <= 0) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'स्विकृत नभएको';
            this.errNotification.open();
          }else {
          this.router.navigate(['/approval/tipani-adesh/', dataRecord['id']]);
          }
        }
      }
    ];
  }

  // columngroups: any[] =
  //   [
  //     { text: this.transData['ACTION'], align: 'center', name: 'Actions' }
  //   ];
  planYearSelected($event){
      let post = {}
      post = this.approvalSearchForm.value
      this.SearchData(post)
  }

  SearchData(post) {
    this.jqxLoader.open();
    this.activityOutputService.indexAd(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
        this.SearchDatas = [];
      } else {
        this.source.localdata = res;
        this.SearchDatas = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

}
