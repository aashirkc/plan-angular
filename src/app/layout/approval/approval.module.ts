import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApprovalComponent } from './approval.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { ApprovalRoutingModule } from './approval-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ApprovalRoutingModule
  ],
  declarations: [ApprovalComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class ApprovalModule { }
