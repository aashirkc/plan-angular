import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TechnicalApprovalComponent } from './technical-approval.component';
import { TechnicalApprovalEditComponent } from './technical-approval-edit/technical-approval-edit.component';
import { TechnicalApprovalViewComponent } from './technical-approval-view/technical-approval-view.component';
import { TechnicalApprovalDataViewComponent } from './technical-approval-data-view/technical-approval-data-view.component';
const routes: Routes = [
  {
    path: '',
    component: TechnicalApprovalComponent
  },
  {
    path: 'edit',
    component: TechnicalApprovalEditComponent
  },
  {
    path: 'view',
    component: TechnicalApprovalViewComponent
  },
  {
    path: 'data-view',
    component: TechnicalApprovalDataViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TechnicalApprovalRoutingModule { }
