import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicalApprovalEditComponent } from './technical-approval-edit.component';

describe('TechnicalApprovalEditComponent', () => {
  let component: TechnicalApprovalEditComponent;
  let fixture: ComponentFixture<TechnicalApprovalEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicalApprovalEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicalApprovalEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
