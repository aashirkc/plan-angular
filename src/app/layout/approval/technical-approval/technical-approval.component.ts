import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, RouterState, RouterStateSnapshot, ActivatedRouteSnapshot } from "@angular/router";
import { DateConverterService, ActivityOutputService, ActivityFocusAreaMasterService, PlanningYearService, InclusionGroupService, CurrentUserService, UnicodeTranslateService, AgencyTypeService } from "../../../shared";
import { jqxNotificationComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxnotification";
import { jqxLoaderComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxloader";
import { jqxGridComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxgrid";
import { TranslateService } from "@ngx-translate/core";
import { jqxWindowComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxwindow";
import { DataService } from "../../../shared/data-service";
import { Location } from '@angular/common';

@Component({
    selector: "app-technical-approval",
    templateUrl: "./technical-approval.component.html",
    styleUrls: ["./technical-approval.component.scss"]
})
export class TechnicalApprovalComponent implements OnInit {
    courseRegistrationForm: FormGroup;
    source: any;
    activityDatas: any;
    dataAdapter: any;
    columns: any[];
    editrow: number = -1;
    transData: any;
    planYearAdapter: Array<any> = [];
    activityAdapter: Array<any> = [];
    activityFocusAreaAdapter: any = [];
    activityCategoryAdapter: any = [];
    userData: any = {};
    SearchDatas: Array<any> = [];
    API_URL_DOC: any;
    API_URL: any;
    columngroups: any[];
    userType: any;
    message: string;
    lang: any;
    agencyName: any;
    agencyType: any = [];
    childType: any = [];
    approveAdapter: any = [
        { id: '1', name: "स्वीकृत भएको" },
        { id: '0', name: "स्वीकृत नभएको" },
    ];

    @ViewChild("msgNotification")
    msgNotification: jqxNotificationComponent;
    @ViewChild("errNotification")
    errNotification: jqxNotificationComponent;
    @ViewChild("jqxLoader")
    jqxLoader: jqxLoaderComponent;
    @ViewChild("jqxEditLoader")
    jqxEditLoader: jqxLoaderComponent;
    @ViewChild("myGrid")
    myGrid: jqxGridComponent;
    @ViewChild("myViewWindow")
    myViewWindow: jqxWindowComponent;
    @ViewChild("myEditWindow")
    myEditWindow: jqxWindowComponent;
    @ViewChild("myReportWindow")
    myReportWindow: jqxWindowComponent;
    @ViewChild("Insert", { read: ViewContainerRef })
    Insert: ViewContainerRef;

    constructor(
        private fb: FormBuilder,
        private cus: CurrentUserService,
        private dfs: DateConverterService,
        @Inject("API_URL_DOC") API_URL_DOC: string,
        @Inject("API_URL") API_URL: string,
        private cdr: ChangeDetectorRef,
        private pys: PlanningYearService,
        private igs: InclusionGroupService,
        private router: Router,
        private afams: ActivityFocusAreaMasterService,
        private activityOutputService: ActivityOutputService,
        private translate: TranslateService,
        private data: DataService,
        private unicode: UnicodeTranslateService,
        private _location: Location,
        private ats: AgencyTypeService,
    ) {
        this.createForm();
        this.getTranslation();
        this.userData = this.cus.getTokenData();
        this.API_URL_DOC = API_URL_DOC;
        this.API_URL = API_URL;
    }

    ngOnInit() {


        this.ats.show(0).subscribe(
            result => {
                if (result['length'] > 0) {

                    this.agencyType = result;
                }
            },
            error => {
                console.log(error);
            }
        );


        this.lang = this.translate.currentLang.substr(0, 2);
        this.getTranslation();
        let userDetalils = JSON.parse(localStorage.getItem('pcUser'));
        this.userData = userDetalils['userType'];


        this.pys.index({}).subscribe(
            res => {
                this.planYearAdapter = res;
                if (this.planYearAdapter.length > 0) {
                    let planCode = this.planYearAdapter[0].yearCode || '';
                    this.courseRegistrationForm.get('planYear').patchValue(planCode);
                    this.SearchData(this.courseRegistrationForm.value);
                }
            },
            error => {
                console.info(error);
            }
        );
        this.igs.index({}).subscribe(
            res => {
                this.activityAdapter = res;
            },
            error => {
                console.info(error);
            }
        );
        this.afams.show(0).subscribe(
            res => {
                this.activityFocusAreaAdapter = res;
            },
            error => {
                console.info(error);
            }
        );
    }
    getTranslation() {
        this.translate
            .get(["SN", "regNo", "STATUS", "universityRoll", 'Approved', 'Approve', 'APPROVE', "ADD", "EDIT", "NAME", "nepali", "english", "mobile", "VIEW", "DETAIL", "ACTION", "UPDATE", "DELETESELECTED", "RELOAD", "ACTIVITY_NAME", "ACTIVITY_FOCUS_AREA", "ACTIVITY_CATEGORY", "ACTIVITY_FOR", "TECHNICAL_APPROVAL", "viewEdit"]).subscribe((translation: [string]) => {
                this.transData = translation;
                this.loadGrid();
            });
    }

    loadGridData() {
        let post = {};
        post = this.courseRegistrationForm.value;
        // this.SearchData(post);
    }

    createForm() {
        this.courseRegistrationForm = this.fb.group({
            planYear: ["", Validators.required],
            activityFor: [""],
            activityFocusArea: ["", Validators.required],
            activityCategory: ["", Validators.required],
            activityName: [""],
            parent: [""],
            child: [""],
            approveState: [""],

        });
    }


    implementingAgencyChange(selectedEvent) {
        if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
            let displayText = selectedEvent.target.selectedOptions[0].text;
            this.agencyName = displayText;
            let agencyCode = selectedEvent.target.value;
            this.getAgencyType(agencyCode);
        } else {
            this.courseRegistrationForm.get('child').patchValue('');
            this.childType = '';
        }
    }
    getAgencyType(agencyCode = 0, index = 0, type = null) {
        // remove previously added form control if the parent select is changed.
        this.ats.show(agencyCode).subscribe(
            result => {
                if (result['length'] > 0) {
                    this.childType = result;
                } else {
                    this.courseRegistrationForm.get('child').patchValue('');
                    this.childType = '';
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    ngAfterViewInit() {
        this.loadGridData();
        let formData = JSON.parse(localStorage.getItem("ActivityOutput"));
        if (formData) {
            if (formData["activityFocusArea"]) {
                this.LoadActivityCategory(formData);
            } else {
                this.courseRegistrationForm.setValue(formData);
                this.SearchData(formData);
                setTimeout(() => {
                    localStorage.removeItem("ActivityOutput");
                }, 100);
            }
        }
        this.unicode.initUnicode();
        this.cdr.detectChanges();
    }

    LoadActivityCategory(post?: any) {
        if (post) {
            let data = post;
            if (data["activityFocusArea"]) {
                this.jqxLoader.open();
                this.afams.show(data["activityFocusArea"]).subscribe(
                    res => {
                        this.activityCategoryAdapter = res;
                        // Setting Data from local Storage
                        let formData = JSON.parse(
                            localStorage.getItem("ActivityOutput")
                        );
                        this.courseRegistrationForm.setValue(formData);
                        this.SearchData(formData);
                        setTimeout(() => {
                            localStorage.removeItem("ActivityOutput");
                        }, 100);
                        this.jqxLoader.close();
                    },
                    error => {
                        console.info(error);
                        this.jqxLoader.close();
                    }
                );
            }
        } else {
            let data = this.courseRegistrationForm.value;
            if (data["activityFocusArea"]) {
                this.jqxLoader.open();
                this.afams.show(data["activityFocusArea"]).subscribe(
                    res => {
                        this.activityCategoryAdapter = res;
                        this.jqxLoader.close();
                    },
                    error => {
                        console.info(error);
                        this.jqxLoader.close();
                    }
                );
            }
        }
    }

    loadGrid() {
        this.source = {
            datatype: "json",
            datafields: [
                { name: "id", type: "string" },
                { name: "planYear", type: "string" },
                { name: "activityFor", type: "string" },
                { name: "activityFocusArea", type: "string" },
                { name: "activityName", type: "string" },
                { name: "activityCategory", type: "string" },
                { name: "focusAreaLevel1", type: "string" },
                { name: "activityForName", type: "string" },
                { name: "focusAreaLevel1Name", type: "string" },
                { name: "focusAreaLevel2", type: "string" },
                { name: "focusAreaLevel2Name", type: "string" },
                { name: "approveStatus", type: "string" },
                { name: "outputType", type: "string" },
                { name: "administrativeApproval", type: "string" },
                { name: "taStatus", type: "string" },
                { name: "executionReportStatus", type: "string" },
                { name: "taMessage", type: "string" },
                { name: "totalEstimatedCost", type: "string" },
                { name: "t_ApprovedBy" }
            ],
            id: "id",
            localdata: [],
            pagesize: 20
        };

        this.dataAdapter = new jqx.dataAdapter(this.source);
        this.columns = [
            {
                text: this.transData["SN"], sortable: false, filterable: false, editable: false, groupable: false, draggable: false, resizable: false, datafield: "id", columntype: "number", width: 50,
                cellsrenderer: function (row, column, value) {
                    return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                }
            },
            { text: this.transData['ACTIVITY_NAME'], datafield: "activityName", columntype: "textbox", editable: false },
            { text: this.transData['ACTIVITY_FOCUS_AREA'], datafield: "focusAreaLevel1", displayfield: "focusAreaLevel1Name", width: 100, columntype: "textbox", editable: false },
            { text: this.transData['ACTIVITY_CATEGORY'], datafield: "focusAreaLevel2", displayfield: "focusAreaLevel2Name", width: 100, columntype: "textbox", editable: false },
            { text: this.transData['ACTIVITY_FOR'], datafield: "activityForName", columntype: "textbox", width: 100, editable: false },
            {
                text: "स्वीकृत स्थिति", datafield: "approved_by", width: 120, columntype: "textbox", editable: false,
                cellsrenderer: (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
                    let cellValue = "";


                    if (rowdata.t_ApprovedBy > 0) {
                        cellValue = '<span style="font-weight: bold;color: #158232;"> स्वीकृत भएको </span>'
                    } else if (rowdata.t_ApprovedBy == null) {
                        cellValue = '<span style="font-weight: bold;color:red;"> स्वीकृत नभएको </span>';
                    } else if (rowdata.t_ApprovedBy == 0) {
                        cellValue = '<span style="font-weight: bold;color:red;"> स्वीकृत नभएको </span>';
                    } else if (rowdata.t_ApprovedBy == -1) {
                        cellValue = '<span style="font-weight: bold;color:red;"> अस्वीकृत भएको </span>';;
                    } else {
                        cellValue = '<span style="font-weight: bold;color: #158232;"> स्वीकृत भएको </span>'
                    }
                    return '<div style="padding:4px 3px;">' + cellValue + '</div>';
                }
            },
            {
                text: this.transData['TECHNICAL_APPROVAL'], datafield: "taStatus", columngroup: "action", sortable: false, filterable: false, width: 110, columntype: "button",
                cellsrenderer: (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
                    // this.editrow = row;
                    // let dataRecord = this.myGrid.getrowdata(this.editrow);
                    // return this.transData[dataRecord['taMessage']];

                    if (rowdata.taStatus == "Y") {
                        // return '<input type="button" disabled="true" value="अनुमोदित"/>';
                        return 'अनुमोदित';
                    } else if (rowdata.taStatus == "N") {
                        // return '<input type="button" value="अनुमोदन गर्नुहोस"/>';
                        return "अनुमोदन गर्नुहोस";
                    }
                },
                buttonclick: (row: number): void => {
                    localStorage.setItem("ActivityOutput", JSON.stringify(this.courseRegistrationForm.value));
                    this.editrow = row;
                    let dataRecord = this.myGrid.getrowdata(this.editrow);
                    if (dataRecord['outputType'] == null || dataRecord['outputType'] == 'N') {
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = 'कृपया पहिले गतिविधि परिभाषित गर्नुहोस्!!';
                        this.errNotification.open();
                    } else {
                        if (dataRecord['id']) {
                            if (dataRecord['administrativeApproval'] == 'N') {
                                if (dataRecord['taStatus'] == 'N') {
                                    this.jqxLoader.open();
                                    this.router.navigate(["/approval/technical-approval/view", { id: dataRecord["id"], name: dataRecord["activityName"], cost: dataRecord["totalEstimatedCost"] }]);
                                } else {
                                    let messageDiv: any = document.getElementById('error');
                                    messageDiv.innerText = 'तपाईले पहिलेनै अनुमोदन गर्नु भएको छ !!';
                                    this.errNotification.open();
                                }
                            } else {
                                let messageDiv: any = document.getElementById('error');
                                messageDiv.innerText = 'यो तपाईले पहिलेनै स्वीकृति गर्नु भएको छ वा प्रशासनिक अनुमोदन गरेको छ?!!';
                                this.errNotification.open();
                            }
                        } else {
                            let messageDiv: any = document.getElementById('error');
                            messageDiv.innerText = 'Technical Aprrove Error Occured';
                            this.errNotification.open();
                        }
                    }
                }

            },
            {
                text: this.transData['EDIT'], datafield: "View", columngroup: "action", sortable: false, filterable: false, width: 90, columntype: "button",
                cellsrenderer: (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
                    return this.transData['EDIT'];
                },
                buttonclick: (row: number): void => {
                    localStorage.setItem("ActivityOutput", JSON.stringify(this.courseRegistrationForm.value));
                    this.editrow = row;
                    let data = this.myGrid.getrowdata(row);
                    let dataRecord = this.myGrid.getrowdata(this.editrow);
                    if (dataRecord.taStatus == "Y") {
                        if (dataRecord.t_ApprovedBy > 0) {
                            //  console.log(this.userData.toLowerCase());
                            if (this.userData.toLowerCase() == 'super admin' || this.userData.toLowerCase() == 'admin') {
                                // this.router.navigate(["/approval/technical-approval/edit", dataRecord['id']]);
                                this.router.navigate(["/approval/technical-approval/edit", { id: dataRecord["id"], name: dataRecord["activityName"], cost: dataRecord["totalEstimatedCost"] }]);
                            }
                            else {
                                let messageDiv: any = document.getElementById('error');
                                messageDiv.innerText = 'तपाईलाई याे कार्य गर्न स्वीकृती दिएकाे छैन !!';
                                this.errNotification.open();
                            }
                        } else {
                            this.router.navigate(["/approval/technical-approval/edit", { id: dataRecord["id"], name: dataRecord["activityName"], cost: dataRecord["totalEstimatedCost"] }]);
                        }
                    } else {
                        ` let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = 'अनुमोदन भएको छैन !!';
                        this.errNotification.open();`
                    }

                    // if (this.userData.toLowerCase() == 'super admin') {
                    //     if (dataRecord['id']) {
                    //         this.jqxLoader.open();
                    //         this.router.navigate(["/approval/technical-approval/edit", dataRecord['id']]);
                    //     }
                    // }
                    // } else if (data.t_ApprovedBy > 0) {
                    //     let message = document.getElementById('error');
                    //     message.innerText = "स्विकृत भएको";
                    //     this.errNotification.open();
                    // } else if (data.t_ApprovedBy == null) {
                    //     alert(data.t_ApprovedBy);
                    // } else if (data.t_ApprovedBy == 0) {
                    //     if (dataRecord['executionReportStatus'] == 'Not Started') {
                    //         if (dataRecord['taStatus'] == 'N') {
                    //             let messageDiv: any = document.getElementById('error');
                    //             messageDiv.innerText = 'कृपया पहिले गतिविधि अनुमोदन गर्नुहोस्!!';
                    //             this.errNotification.open();
                    //         } else {
                    //             if (this.userData.toLowerCase() == 'technical approver' || this.userData.toLowerCase() == 'activity manager' || this.userData.toLowerCase() == 'admin') {
                    //                 if (dataRecord['id']) {
                    //                     this.jqxLoader.open();
                    //                     this.router.navigate(["/approval/technical-approval/edit", { id: dataRecord["id"], name: dataRecord["activityName"], cost: dataRecord["totalEstimatedCost"] }]);
                    //                 }
                    //             } else {
                    //                 let messageDiv: any = document.getElementById('error');
                    //                 messageDiv.innerText = 'तपाईलार्इ याे कार्य गर्न स्वीकृती दिएकाे छैन !!';
                    //                 this.errNotification.open();
                    //             }
                    //         }
                    //     } else {
                    //         let messageDiv: any = document.getElementById('error');
                    //         messageDiv.innerText = 'कार्य पहिले नै सुरु भएको छ !!';
                    //         this.errNotification.open();
                    //     }
                    // }
                }
            },
            {
                text: this.transData['VIEW'], datafield: "ViewData", columngroup: "action", sortable: false, filterable: false, width: 60, columntype: "button",
                cellsrenderer: (): string => {
                    return this.transData['VIEW'];
                },
                buttonclick: (row: number): void => {
                    localStorage.setItem("ActivityOutput", JSON.stringify(this.courseRegistrationForm.value));
                    this.editrow = row;
                    let dataRecord = this.myGrid.getrowdata(this.editrow);
                    if (dataRecord['taStatus'] == 'N') {
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = 'कृपया पहिले अनुमोदन गर्नुहाेस्';
                        this.errNotification.open();
                    }
                    else {
                        this.router.navigate(["/approval/technical-approval/data-view", { id: dataRecord["id"], name: dataRecord["activityName"] }]);
                    }
                    // if (dataRecord['executionReportStatus'] == 'Not Started') {
                    //     if (dataRecord['taStatus'] == 'N') {
                    //         let messageDiv: any = document.getElementById('error');
                    //         messageDiv.innerText = 'Please Aprove Activity First!!';
                    //         this.errNotification.open();
                    //     } else {
                    //         if (dataRecord['id']) {
                    //             this.jqxLoader.open();
                    //             this.router.navigate(["/approval/technical-approval/data-view", { id: dataRecord["id"], name: dataRecord["activityName"] }]);
                    //         }
                    //     }
                    // } else {
                    //     let messageDiv: any = document.getElementById('error');
                    //     messageDiv.innerText = 'Work Already Started';
                    //     this.errNotification.open();
                    // }
                }
            }
        ];
        this.columngroups = [
            { text: this.transData['ACTION'], align: "center", name: "action" }
        ];
    }

    planYearSelected($event) {
        let post = {}
        post = this.courseRegistrationForm.value;
        this.SearchData(post)
    }

    SearchData(post) {
        this.jqxLoader.open();
        this.activityOutputService.indexTech(post).subscribe(
            res => {
                if (res.length == 1 && res[0].error) {
                    let messageDiv: any = document.getElementById("error");
                    messageDiv.innerText = res[0].error;
                    this.errNotification.open();
                    this.source.localdata = [];
                    this.SearchDatas = [];
                } else {
                    this.source.localdata = res;
                    this.SearchDatas = res;
                }
                this.myGrid.updatebounddata();
                this.jqxLoader.close();
            },
            error => {
                console.info(error);
                this.jqxLoader.close();
            }
        );
    }
}
