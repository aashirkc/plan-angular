import {
    Component,
    OnInit,
    ChangeDetectorRef,
    Input,
    Inject,
    ViewChild
} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ActivityOutputService } from '../../../../shared';
import { DataService } from '../../../../shared/data-service';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { ApprovalData } from '../../../../shared/model/approval-data.model';
import { TranslateService } from '@ngx-translate/core';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

@Component({
    selector: 'app-technical-approval-data-view',
    templateUrl: './technical-approval-data-view.component.html',
    styleUrls: ['./technical-approval-data-view.component.scss']
})
export class TechnicalApprovalDataViewComponent implements OnInit {
    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('myGrid') myGrid: jqxGridComponent;

    technicalApprovalForm: FormGroup;
    technicalApprovalLetter: any;
    technicalSpecificationDocument: any;
    name: string = '';
    approvalId: number = 0;
    docUrl: string;
    tecLetter: any[] = [];
    tecDocument: any[] = [];
    id: number = 0;
    activityData: any;
    formattedData: any;
    showApproveButton: boolean = false;

    technicalApprovalData: any[] = [];
    hideButton: boolean = false;
    showApprovalButton: boolean = false;

    source: any;
    dataAdapter: any;
    columns: any[] = [];
    editrow: number = -1;
    columngroups: any[];
    transData: any;
    enable_disableApproveFeature: boolean;

    constructor(
        private fb: FormBuilder,
        private activeRoute: ActivatedRoute,
        private aos: ActivityOutputService,
        private location: Location,
        private translate: TranslateService,
        private data: DataService,
        private router: Router,
        @Inject('API_URL_DOC') docUrl: string,
        @Inject('ENABLE_ADMINISTRATIVE_APPROVAL_DISABLE_FEATURE')
        enable_disableApproveFeature: boolean
    ) {
        this.enable_disableApproveFeature =
            enable_disableApproveFeature || false;
        this.docUrl = docUrl;
    }

    ngOnInit() {
        this.getTranslation();
        let userDetalils = JSON.parse(localStorage.getItem('pcUser'));

        if (userDetalils['userType'].toLowerCase() == 'admin') {
            this.showApproveButton = true;
        } else if (
            userDetalils['userType'].toLowerCase() == 'activity manager'
        ) {
            this.showApproveButton = true;
        } else if (
            userDetalils['userType'].toLowerCase() == 'technical manager'
        ) {
            this.showApproveButton = true;
        } else if (
            userDetalils['userType'].toLowerCase() == 'administrative manager'
        ) {
            this.showApproveButton = true;
        } else if (userDetalils['userType'].toLowerCase() == 'super admin') {
            this.showApproveButton = true;
        } else {
            this.showApproveButton = false;
        }

        let data = this.activeRoute.snapshot.params;
        this.name = data['name'];
        this.approvalId = data['id'];

        this.getApprovalDetails();

        this.technicalApprovalForm = this.fb.group({
            id: [null, Validators.required],
            activity: [data['id'], Validators.required],
            techCost: [null, Validators.required],
            proposedCost: [null, Validators.required],
            orderIssuing: [null, Validators.required],
            techOrderNo: [null, Validators.required],
            techDate: [null, Validators.required],
            technicalApprovalLetter: this.fb.array([
                this.createTechnicalApprovalLetterForm()
            ]),
            technicalSpecificationDocument: this.fb.array([
                this.createTechnicalSpecificationDocumentForm()
            ])
        });
        this.aos.showActivity(data['id']).subscribe(
            res => {
                this.activityData = res;
                this.showApprovalButton =
                    this.activityData['t_ApprovedBy'] > 0 ? true : false;
            },
            error => {
                console.info(error);
            }
        );
    }

    getTranslation() {
        this.translate
            .get([
                'SN',
                'RELOAD',
                'STATUS',
                'DESCRIPTION',
                'UNIT',
                'QUANTITY',
                'PLANNING_YEAR',
                'YEAR_CODE',
                'EDIT',
                'ACTION',
                'YEAR_NAME',
                'START_DATE',
                'END_DATE',
                'BS',
                'AD',
                'DELETE_ROW',
                'YES',
                'NO',
                'YESE',
                'NOE'
            ])
            .subscribe((translation: [string]) => {
                this.transData = translation;
                this.loadGrid();
            });
    }
    private getApprovalDetails() {
        this.aos.getTechnicalApproveById(this.approvalId).subscribe(
            (result: any) => {
                console.log(result);
                if (result['data']) {
                    this.technicalApprovalData = result['data'];
                    this.id = result['data'].id;
                    let formData = new ApprovalData(result['data']);
                    // console.log(result['data']);

                    // this.showApprovalButton = result['data']['t_ApprovedBy'] > 0 ? true : false;
                    let data = this.activeRoute.snapshot.params;
                    let dt = {};
                    dt['activity'] = data['id'];
                    let formatData = result['data']['techOrderNo'];
                    console.log(formatData);
                    this.formattedData = formatData.substr(0, 2);
                    this.formattedData =
                        this.formattedData +
                        '/' +
                        formatData.substr(2, 2) +
                        '/' +
                        formatData.substr(4, formatData.length);
                    console.log(this.formattedData);
                    this.tecLetter = [];
                    this.tecDocument = [];
                    result['image'].forEach(result => {
                        if (result[2].substr(0, 20) == 'TechnicalAproval/TAL') {
                            let data = {
                                id: result[0],
                                title: result[1],
                                name: result[2],
                                url: this.docUrl + result[2]
                            };
                            this.tecLetter.push(data);
                        } else if (
                            result[2].substr(0, 20) == 'TechnicalAproval/TSD'
                        ) {
                            let data = {
                                id: result[0],
                                title: result[1],
                                name: result[2],
                                url: this.docUrl + result[2]
                            };
                            this.tecDocument.push(data);
                        }
                    });
                    this.technicalApprovalForm.patchValue(formData);
                    this.aos.loadTechnicalApprovalWork(dt).subscribe(
                        res => {
                            console.log(res[0]);
                            if (res.length == 1 && res[0].error) {
                                let messageDiv: any = document.getElementById(
                                    'error'
                                );
                                messageDiv.innerText = res[0].error;
                                this.errNotification.open();
                                this.source.localdata = [];
                            } else {
                                this.source.localdata = res;
                            }
                            this.myGrid.updatebounddata();
                            this.jqxLoader.close();
                        },
                        error => {
                            console.info(error);
                            this.jqxLoader.close();
                        }
                    );
                } else {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = 'कुनै विवरण भेटिएन';
                    this.errNotification.open();
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    private createTechnicalApprovalLetterForm() {
        return this.fb.group({
            id: [null],
            fileTitle: [null],
            file: [null]
        });
    }

    private createTechnicalSpecificationDocumentForm() {
        return this.fb.group({
            id: [null],
            fileTitle: [null],
            file: [null]
        });
    }

    addMoreApprovalLetter() {
        this.technicalApprovalLetter = this.technicalApprovalForm.controls[
            'technicalApprovalLetter'
        ];
        this.technicalApprovalLetter.push(
            this.createTechnicalApprovalLetterForm()
        );
    }

    addMoreSpecificationDocument() {
        this.technicalSpecificationDocument = this.technicalApprovalForm.controls[
            'technicalSpecificationDocument'
        ];
        this.technicalSpecificationDocument.push(
            this.createTechnicalSpecificationDocumentForm()
        );
    }

    removeForm(type, i) {
        if (type == '0') {
            this.technicalApprovalLetter.removeAt(i);
        } else if (type == '1') {
            this.technicalSpecificationDocument.removeAt(i);
        }
    }
    ngAfterViewInit() {
        let data = this.activeRoute.snapshot.params;
        let dt = {};
        dt['activity'] = data['id'];
        // this.loadGridData(dt);
    }

    save(value) {
        if (this.activityData['administrativeApproval'] != 'Y') {
            this.jqxLoader.open();
            delete value['technicalApprovalLetter'];
            delete value['technicalSpecificationDocument'];
            value.id = this.id;
            const _formData = new FormData();
            for (var i = 0; i < this.techicalFiles.length; i++) {
                _formData.append('talTitle', this.techicalFiles[i].fileName);
                _formData.append(
                    'talFile',
                    this.techicalFiles[i].file,
                    this.techicalFiles[i].file.name
                );
            }

            for (var i = 0; i < this.specificationFlies.length; i++) {
                _formData.append(
                    'tsdTitle',
                    this.specificationFlies[i].fileName
                );
                _formData.append(
                    'tsdFile',
                    this.specificationFlies[i].file,
                    this.specificationFlies[i].file.name
                );
            }

            for (let data in value) {
                _formData.append(data, value[data]);
            }

            this.aos.editTechnicalApproval(_formData).subscribe(
                result => {
                    console.log(result);
                    if (result['message']) {
                        this.jqxLoader.close();
                        let messageDiv: any = document.getElementById(
                            'message'
                        );
                        messageDiv.innerText = result['message'];
                        this.msgNotification.open();
                        this.getApprovalDetails();
                    }
                    if (result['error']) {
                        this.jqxLoader.close();
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = result['error']['message'];
                        this.errNotification.open();
                    }
                },
                error => {
                    this.jqxLoader.close();
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = error['message'];
                    this.errNotification.open();
                }
            );
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText =
                'After administrative approval you can not edit this..';
            this.errNotification.open();
        }
    }

    clearForm() {
        this.technicalApprovalForm.reset();
    }

    techicalFiles: any[] = [];
    specificationFlies: any[] = [];

    onFileChange(event, type, i) {
        if (event.target.files && event.target.files[0]) {
            var formData: FormData = new FormData();
            if (type == '0') {
                let fileName = this.technicalApprovalForm
                    .get('technicalApprovalLetter')
                    ['controls'][i].get('fileTitle').value;

                let file = event.target.files[0];
                let obj = {
                    fileName: fileName,
                    file: file
                };
                this.techicalFiles.push(obj);
            } else if (type == '1') {
                let fileName = this.technicalApprovalForm
                    .get('technicalSpecificationDocument')
                    ['controls'][i].get('fileTitle').value;
                let file = event.target.files[0];
                let obj = {
                    fileName: fileName,
                    file: file
                };
                this.specificationFlies.push(obj);
            }
        }
    }

    deleteImage(name) {
        this.jqxLoader.open();
        this.aos.deletImage({ imageName: name }).subscribe(
            result => {
                this.clearForm();
                if (result['message']) {
                    this.jqxLoader.close();
                    let messageDiv: any = document.getElementById('message');
                    messageDiv.innerText = result['message'];
                    this.msgNotification.open();
                    this.getApprovalDetails();
                }
                if (result['error']) {
                    this.jqxLoader.close();
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = result['error']['message'];
                    this.errNotification.open();
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    close() {
        // this.router.navigate(["/approval/technical-approval"]);
        this.location.back();
    }

    reject() {
        debugger;
        this.jqxLoader.open();
        const userId = JSON.parse(localStorage.getItem('pcUser'))['userId'];
        this.aos.approveTechnical(userId, this.approvalId, '-1').subscribe(
            result => {
                if (result['message']) {
                    this.showMessage(result['message']);
                    this.location.back();
                }
                if (result['error']) {
                    this.showError(result['error']['message']);
                }
                this.jqxLoader.close();
            },
            error => {
                this.jqxLoader.close();
                this.showError(error['message']);
            }
        );
    }

    showMessage(message: string) {
        let messageDiv: any = document.getElementById('message');
        messageDiv.innerText = message;
        this.msgNotification.open();
    }

    showError(message: string) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = message;
        this.errNotification.open();
    }

    approve(value) {
        this.aos
            .approveTechnical(
                JSON.parse(localStorage.getItem('pcUser'))['userId'],
                this.approvalId
            )
            .subscribe(
                result => {
                    if (result['message']) {
                        this.jqxLoader.close();
                        let messageDiv: any = document.getElementById(
                            'message'
                        );
                        messageDiv.innerText = result['message'];
                        this.msgNotification.open();
                        this.showApprovalButton = true;
                        this.location.back();
                    }
                    if (result['error']) {
                        this.jqxLoader.close();
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = result['error']['message'];
                        this.errNotification.open();
                    }
                },
                error => {
                    this.jqxLoader.close();
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = error['message'];
                    this.errNotification.open();
                }
            );
    }
    loadGridData(post) {
        // this.jqxLoader.open();
        // this.aos.loadTechnicalApprovalWork(post).subscribe((res) => {
        //     console.log(res[0]);
        //     if (res.length == 1 && res[0].error) {
        //         let messageDiv: any = document.getElementById('error');
        //         messageDiv.innerText = res[0].error;
        //         this.errNotification.open();
        //         this.source.localdata = [];
        //     } else {
        //         this.source.localdata = res;
        //     }
        //     this.myGrid.updatebounddata();
        //     this.jqxLoader.close();
        // }, (error) => {
        //     console.info(error);
        //     this.jqxLoader.close();
        // });
    }
    loadGrid() {
        this.source = {
            datatype: 'json',
            datafields: [
                { name: 'id', type: 'string' },
                { name: 'description', type: 'string' },
                { name: 'quantity', type: 'string' },
                { name: 'unit', type: 'string' }
            ],
            id: 'id',
            localdata: [],
            pagesize: 20
        };

        this.dataAdapter = new jqx.dataAdapter(this.source);
        this.columns = [
            {
                text: this.transData['SN'],
                sortable: false,
                filterable: false,
                editable: false,
                groupable: false,
                draggable: false,
                resizable: false,
                datafield: 'id',
                columntype: 'number',
                width: 50,
                cellsrenderer: function(row, column, value) {
                    return "<div style='margin:4px;'>" + (value + 1) + '</div>';
                }
            },
            {
                text: this.transData['DESCRIPTION'],
                datafield: 'description',
                columntype: 'textbox',
                editable: false,
                filtercondition: 'starts_with'
            },
            {
                text: this.transData['QUANTITY'],
                datafield: 'quantity',
                editable: false,
                columntype: 'textbox',
                filtercondition: 'starts_with'
            },
            {
                text: this.transData['UNIT'],
                datafield: 'unit',
                editable: false,
                columntype: 'textbox',
                filtercondition: 'starts_with'
            }
        ];
        this.columngroups = [
            { text: 'Actions', align: 'center', name: 'action' }
        ];
    }
    rendertoolbar = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';

        let buttonContainer2 = document.createElement('div');
        let buttonContainer3 = document.createElement('div');

        buttonContainer2.id = 'buttonContainer2';
        buttonContainer3.id = 'buttonContainer3';

        buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
        buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

        container.appendChild(buttonContainer3);
        // container.appendChild(buttonContainer2);
        toolbar[0].appendChild(container);

        let deleteRowButton = jqwidgets.createInstance(
            '#buttonContainer3',
            'jqxButton',
            {
                width: 150,
                value: this.transData['DELETE_ROW'],
                theme: 'energyblue'
            }
        );
        // let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

        deleteRowButton.addEventHandler('click', () => {
            let id = this.myGrid.getselectedrowindexes();
            let ids = [];
            for (let i = 0; i < id.length; i++) {
                let dataRecord = this.myGrid.getrowdata(Number(id[i]));
                ids.push(dataRecord['id']);
            }
            if (ids.length > 0) {
                if (confirm('Are you sure? You Want to delete')) {
                    this.jqxLoader.open();
                    this.aos.destroyTechnicalApprovalWork(ids).subscribe(
                        result => {
                            this.jqxLoader.close();
                            if (result['message']) {
                                this.myGrid.clearselection();
                                let messageDiv: any = document.getElementById(
                                    'message'
                                );
                                messageDiv.innerText = result['message'];
                                this.msgNotification.open();
                                let data = this.activeRoute.snapshot.params;
                                let dt = {};
                                dt['activity'] = data['id'];
                                this.loadGridData(dt);
                            }
                            if (result['error']) {
                                this.myGrid.clearselection();
                                let messageDiv: any = document.getElementById(
                                    'error'
                                );
                                messageDiv.innerText =
                                    result['error']['message'];
                                this.errNotification.open();
                                let data = this.activeRoute.snapshot.params;
                                let dt = {};
                                dt['activity'] = data['id'];
                                this.loadGridData(dt);
                            }
                        },
                        error => {
                            this.jqxLoader.close();
                            console.info(error);
                        }
                    );
                }
            } else {
                let messageDiv = document.getElementById('error');
                messageDiv.innerText = 'Please select some item to delete';
                this.errNotification.open();
            }
        });
    }; //render toolbar ends
}
