import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TechnicalApprovalRoutingModule } from './technical-approval-routing.module';
import { TechnicalApprovalComponent } from './technical-approval.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { TechnicalApprovalViewComponent } from './technical-approval-view/technical-approval-view.component';
import { TechnicalApprovalEditComponent } from './technical-approval-edit/technical-approval-edit.component';
import { TechnicalApprovalDataViewComponent } from './technical-approval-data-view/technical-approval-data-view.component';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TechnicalApprovalRoutingModule
  ],
  declarations: [TechnicalApprovalComponent, TechnicalApprovalDataViewComponent, TechnicalApprovalViewComponent, TechnicalApprovalEditComponent]
})
export class TechnicalApprovalModule { }
