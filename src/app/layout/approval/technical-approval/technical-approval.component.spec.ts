import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicalApprovalComponent } from './technical-approval.component';

describe('TechnicalApprovalComponent', () => {
  let component: TechnicalApprovalComponent;
  let fixture: ComponentFixture<TechnicalApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicalApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicalApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
