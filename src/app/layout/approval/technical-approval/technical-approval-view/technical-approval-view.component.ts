import { Component, OnInit, ChangeDetectorRef, ViewChild, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from "@angular/common";
import { ActivityOutputService,ToggleSidebarService, AllReportService, DateConverterService, CustomValidators } from "../../../../shared";
import { jqxNotificationComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxnotification";
import { jqxLoaderComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxloader";
import { TranslateService } from "@ngx-translate/core";
import { jqxGridComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxgrid";

@Component({
    selector: "app-technical-approval-view",
    templateUrl: "./technical-approval-view.component.html",
    styleUrls: ["./technical-approval-view.component.scss"]
})
export class TechnicalApprovalViewComponent implements OnInit {

    @ViewChild("msgNotification") msgNotification: jqxNotificationComponent;
    @ViewChild("errNotification") errNotification: jqxNotificationComponent;
    @ViewChild("jqxLoader") jqxLoader: jqxLoaderComponent;
    @ViewChild('myGrid') myGrid: jqxGridComponent;

    technicalApprovalForm: FormGroup;
    workTypeForm: FormGroup;
    technicalApprovalLetter: any;
    technicalSpecificationDocument: any;
    update: boolean = false;
    name: string = "";
    cost: any;
    userId: any;
    userData: any = {};
    notAuthorized : boolean = false;
    source: any;
    dataAdapter: any;
    columns: any[] = [];
    editrow: number = -1;
    columngroups: any[];
    heading: any[];
    transData: any;
    fullDate: string;
    activityDatas={};
    check:boolean=true;
    actId:any;
    constructor(
        private ts:ToggleSidebarService,
        private fb: FormBuilder,
        private activeRoute: ActivatedRoute,
        private aos: ActivityOutputService,
        private location: Location,
        private report: AllReportService,
        private translate: TranslateService,
        private dcs: DateConverterService,
        private router: Router,
    ) {
        this.report.getOrg().subscribe((result) => {
            if (result.length == 1 && result[0].error) {
                this.heading = [];
            } else {
                this.heading = result[0];
                this.technicalApprovalForm.controls['orderIssuing'].patchValue(this.heading['officeName'] || '');

            }

        }, (error) => {
            console.info(error);
        });
    }

    ngOnInit() {

        let org = localStorage.getItem('org')
        console.log(org)

        let org1 = JSON.parse(org)

        let org2 = org1['officeName']
        let userDetalils = JSON.parse(localStorage.getItem('pcUser'));
        this.userData = userDetalils['userType'];
        if (!(this.userData.toLowerCase() == 'super admin' || this.userData.toLowerCase() == 'admin')) {
            this.notAuthorized=true;
        }

        // this.technicalApprovalForm.get('orderIssuing').patchValue(org2)

         this.ts.currentMessage.subscribe((data) => {
      if(data=="on"){

      this.check=false;
      }
      if(data=="off")
      {
      this.check=true;
      }

    });
        let data = this.activeRoute.snapshot.params;
        this.name = data["name"];
        this.cost = data["cost"];
        this.actId=data['id'];
        this.userId = JSON.parse(localStorage.getItem('pcUser')).userId;

        this.technicalApprovalForm = this.fb.group({
            activity: [data["id"], Validators.required],
            techCost: [null, Validators.required],
            proposedCost: [data["cost"], Validators.required],
            orderIssuing: [null, Validators.required],
            // yojanaSakha: ['', Validators.required],
            techDate: [null,Validators.compose([Validators.required, CustomValidators.checkDate])],
            technicalApprovalLetter: this.fb.array([
                this.createTechnicalApprovalLetterForm()
            ]),
            technicalSpecificationDocument: this.fb.array([
                this.createTechnicalSpecificationDocumentForm()
            ]),
        });
        this.workTypeForm = this.fb.group({
            description: ['', Validators.required],
            quantity: ['', Validators.required],
            unit: ['', Validators.required],
            id: [''],
            techItems: this.fb.array([
                this.initTechItems(),
            ]),
        });
        this.getTranslation();
        let currentYear = new Date().getFullYear();
        let month = new Date().getMonth() + 1;
        let day = new Date().getDate();
        let engDate = currentYear + '-' + month + '-' + day;
        let date = this.dcs.ad2Bs(engDate);
        this.fullDate = date['fulldate'];
        this.technicalApprovalForm.controls['techDate'].patchValue(this.fullDate);

        this.technicalApprovalForm.controls['techDate'].updateValueAndValidity();
        // this.technicalApprovalForm.controls['yojanaSakha'].patchValue('योजना शाखा');

    }
    initTechItems() {
        return this.fb.group({
            id: [''],
            description: ['', Validators.required],
            quantity: ['', Validators.required],
            unit: ['', Validators.required],
        });
    }
    addItem() {
        const control1 = <FormArray>this.workTypeForm.controls['techItems'];
        control1.push(this.initTechItems());
        // add unicode support for dynamically added input

    }
    removeItem(i: number) {
        const control1 = <FormArray>this.workTypeForm.controls['techItems'];
        control1.removeAt(i);
        //this.itemAdapter.splice(i, 1);
    }
    getTranslation() {
        this.translate.get(['SN', "RELOAD", 'STATUS', 'DESCRIPTION', 'UNIT', 'QUANTITY', "PLANNING_YEAR", "YEAR_CODE", "EDIT", "ACTION", "YEAR_NAME", "START_DATE", "END_DATE", "BS", "AD", "DELETE_ROW", "YES", "NO", "YESE", "NOE"]).subscribe((translation: [string]) => {
            this.transData = translation;
            this.loadGrid();
        });
    }

    ngAfterViewInit() {
        let data = this.activeRoute.snapshot.params;
        let dt = {};
        dt['activity'] = data['id'];
        this.loadGridData(dt);
    }

    private createTechnicalApprovalLetterForm() {
        return this.fb.group({
            fileTitle: [null],
            file: [null]
        });
    }

    private createTechnicalSpecificationDocumentForm() {
        return this.fb.group({
            fileTitle: [null],
            file: [null, Validators.required]
        });
    }

    addMoreApprovalLetter() {
        this.technicalApprovalLetter = this.technicalApprovalForm.controls[
            "technicalApprovalLetter"
        ];
        this.technicalApprovalLetter.push(
            this.createTechnicalApprovalLetterForm()
        );
    }

    addMoreSpecificationDocument() {
        this.technicalSpecificationDocument = this.technicalApprovalForm.controls[
            "technicalSpecificationDocument"
        ];
        this.technicalSpecificationDocument.push(
            this.createTechnicalSpecificationDocumentForm()
        );
    }

    removeForm(type, i) {
        if (type == "0") {
            this.technicalApprovalLetter.removeAt(i);
        } else if (type == "1") {
            this.technicalSpecificationDocument.removeAt(i);
        }
    }
    updateWorkType(value) {
        console.log(value.id);
        let post = this.workTypeForm.value;
        let data = this.activeRoute.snapshot.params;
        post['activity'] = data['id'];
        this.aos.updateWorkServiceType(value, value.id).subscribe(
            result => {
                if (result["message"]) {
                    this.jqxLoader.close();
                    let messageDiv: any = document.getElementById("message");
                    messageDiv.innerText = result["message"];
                    this.msgNotification.open();
                    let data = this.activeRoute.snapshot.params;
                    let dt = {};
                    dt['activity'] = data['id'];
                    this.loadGridData(dt);
                }
                if (result["error"]) {
                    this.jqxLoader.close();
                    let messageDiv: any = document.getElementById("error");
                    messageDiv.innerText = result["error"]["message"];
                    this.errNotification.open();
                }
            },
            error => {
                this.jqxLoader.close();
                let messageDiv: any = document.getElementById("error");
                messageDiv.innerText = error["message"];
                this.errNotification.open();
            }
        );
    }
    save(value) {
        this.jqxLoader.open();
        delete value["technicalApprovalLetter"];
        delete value["technicalSpecificationDocument"];

        const _formData = new FormData();



        for (var i = 0; i < this.technicalApprovalForm.get("technicalApprovalLetter")["controls"].length; i++) {
            _formData.append("talTitle", this.technicalApprovalForm.get("technicalApprovalLetter")["controls"][i].get("fileTitle").value);
        }

        for (var i = 0; i < this.techicalFiles.length; i++) {
            _formData.append(
                "talFile",
                this.techicalFiles[i].file,
                this.techicalFiles[i].file.name
            );
        }

        for (var i = 0; i < this.technicalApprovalForm.get("technicalSpecificationDocument")["controls"].length; i++) {
            _formData.append("tsdTitle", this.technicalApprovalForm.get("technicalSpecificationDocument")["controls"][i].get("fileTitle").value);
        }

        for (var i = 0; i < this.specificationFlies.length; i++) {
            _formData.append(
                "tsdFile",
                this.specificationFlies[i].file,
                this.specificationFlies[i].file.name
            );
        }

        for (let data in value) {
            _formData.append(data, value[data]);
        }

        _formData.append("userId", this.userId);
        this.aos.addTechnicalApproval(_formData).subscribe(
            result => {
                if (result["message"]) {
                    this.jqxLoader.close();
                    let messageDiv: any = document.getElementById("message");
                    messageDiv.innerText = result["message"];
                    this.msgNotification.open();
                    if (this.userData.toLowerCase() === 'super admin' || this.userData.toLowerCase() === 'admin') {
                        this.approve();
                    } else {
                        this.router.navigate(['approval/technical-approval']);
                    }
                }
                if (result["error"]) {
                    this.jqxLoader.close();
                    let messageDiv: any = document.getElementById("error");
                    messageDiv.innerText = result["error"]["message"];
                    this.errNotification.open();
                }
            },
            error => {
                this.jqxLoader.close();
                let messageDiv: any = document.getElementById("error");
                messageDiv.innerText = error["message"];
                this.errNotification.open();
            }
        );
    }
    approve() {
        this.aos.approveTechnical(JSON.parse(localStorage.getItem('pcUser'))['userId'], this.actId).subscribe(
            result => {
                if (result["message"]) {
                    this.jqxLoader.close();
                    let messageDiv: any = document.getElementById("message");
                    messageDiv.innerText = result["message"];
                    this.msgNotification.open();
                    this.aos.showActivity(this.actId).subscribe((res) => {

                        localStorage.setItem('ActivityDatas',JSON.stringify(res));
                        if(this.check){
                            this.router.navigate(['approval/technical-approval']);
                        }else{
                            this.activityDatas=JSON.parse(localStorage.getItem('ActivityDatas'));
                            this.router.navigate(['/approval/administrative-approval/', this.activityDatas['id']]);
                        }


                      }, (error) => {
                        console.info(error);
                      });
                }
                if (result["error"]) {
                    this.jqxLoader.close();
                    let messageDiv: any = document.getElementById("error");
                    messageDiv.innerText = result["error"]["message"];
                    this.errNotification.open();
                }
            },
            error => {
                this.jqxLoader.close();
                let messageDiv: any = document.getElementById("error");
                messageDiv.innerText = error["message"];
                this.errNotification.open();
            }
        );

    }

    clearForm() {
        this.technicalApprovalForm.reset();
    }

    techicalFiles: any[] = [];
    specificationFlies: any[] = [];

    onFileChange(event, type, i) {
        console.log('ppp')
        this.jqxLoader.open();
        if (event.target.files && event.target.files[0]) {
            var formData: FormData = new FormData();

            if (type == "0") {
                let file = event.target.files[0];
                let obj = {
                    file: file
                };
                if (this.techicalFiles[i]) {
                    this.techicalFiles[i] = obj;
                } else {
                    this.techicalFiles.push(obj);
                }

            } else if (type == "1") {
                let file = event.target.files[0];
                let obj = {
                    file: file
                };
                if (this.specificationFlies[i]) {
                    this.specificationFlies[i] = obj;
                } else {
                    this.specificationFlies.push(obj);
                }
            }
        }
        this.jqxLoader.close();
    }

    close() {
        this.router.navigate(["/approval/technical-approval"]);
    }

    saveWorkType() {
        let post = this.workTypeForm.value;
        let data = this.activeRoute.snapshot.params;
        post['activity'] = data['id'];
        this.aos.addWorkServiceType(post).subscribe(
            result => {
                if (result["message"]) {
                    this.jqxLoader.close();
                    let messageDiv: any = document.getElementById("message");
                    messageDiv.innerText = result["message"];
                    this.msgNotification.open();
                    let data = this.activeRoute.snapshot.params;
                    let dt = {};
                    dt['activity'] = data['id'];
                    this.loadGridData(dt);
                }
                if (result["error"]) {
                    this.jqxLoader.close();
                    let messageDiv: any = document.getElementById("error");
                    messageDiv.innerText = result["error"]["message"];
                    this.errNotification.open();
                }
            },
            error => {
                this.jqxLoader.close();
                let messageDiv: any = document.getElementById("error");
                messageDiv.innerText = error["message"];
                this.errNotification.open();
            }
        );

    }
    cancelUpdateBtn() {
        this.update = false;
        this.workTypeForm.reset();
        let data = this.activeRoute.snapshot.params;
        let dt = {};
        dt['activity'] = data['id'];
        this.loadGridData(dt);
    }
    loadGridData(post) {
        // this.jqxLoader.open();
        // this.aos.loadTechnicalApprovalWork(post).subscribe((res) => {
        //     console.log(res[0]);
        //     if (res.length == 1 && res[0].error) {
        //         let messageDiv: any = document.getElementById('error');
        //         messageDiv.innerText = res[0].error;
        //         this.errNotification.open();
        //         this.source.localdata = [];
        //     } else {
        //         this.source.localdata = res;
        //     }
        //     this.myGrid.updatebounddata();
        //     this.jqxLoader.close();
        // }, (error) => {
        //     console.info(error);
        //     this.jqxLoader.close();
        // });
    }
    loadGrid() {
        this.source =
            {
                datatype: 'json',
                datafields: [
                    { name: 'id', type: 'string' },
                    { name: 'description', type: 'string' },
                    { name: 'quantity', type: 'string' },
                    { name: 'unit', type: 'string' },
                ],
                id: 'id',
                localdata: [],
                pagesize: 20
            }

        this.dataAdapter = new jqx.dataAdapter(this.source);
        this.columns = [
            {
                text: this.transData['SN'], sortable: false, filterable: false, editable: false,
                groupable: false, draggable: false, resizable: false,
                datafield: 'id', columntype: 'number', width: 50,
                cellsrenderer: function (row, column, value) {
                    return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                }
            },
            { text: this.transData['DESCRIPTION'], datafield: 'description', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
            { text: this.transData['QUANTITY'], datafield: 'quantity', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
            { text: this.transData['UNIT'], datafield: 'unit', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
            {
                text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 80, columntype: 'button',
                cellsrenderer: (): string => {
                    return this.transData['EDIT'];
                },
                buttonclick: (row: number): void => {
                    this.editrow = row;
                    let dataRecord = this.myGrid.getrowdata(this.editrow);
                    let dt = {};
                    dt['id'] = dataRecord['id'];
                    dt['description'] = dataRecord['description'];
                    dt['quantity'] = dataRecord['quantity'];
                    dt['unit'] = dataRecord['unit'];
                    this.workTypeForm.setValue(dt);
                    this.update = true;
                }
            }
        ];
        this.columngroups =
            [
                { text: 'Actions', align: 'center', name: 'action' },
            ];

    }
    rendertoolbar = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';

        let buttonContainer2 = document.createElement('div');
        let buttonContainer3 = document.createElement('div');

        buttonContainer2.id = 'buttonContainer2';
        buttonContainer3.id = 'buttonContainer3';

        buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
        buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

        container.appendChild(buttonContainer3);
        // container.appendChild(buttonContainer2);
        toolbar[0].appendChild(container);

        let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE_ROW'], theme: 'energyblue' });
        // let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

        deleteRowButton.addEventHandler('click', () => {
            let id = this.myGrid.getselectedrowindexes();
            let ids = [];
            for (let i = 0; i < id.length; i++) {
                let dataRecord = this.myGrid.getrowdata(Number(id[i]));
                ids.push(dataRecord['id']);
            }
            if (ids.length > 0) {
                if (confirm("Are you sure? You Want to delete")) {
                    this.jqxLoader.open();
                    this.aos.destroyTechnicalApprovalWork(ids).subscribe(result => {
                        this.jqxLoader.close();
                        if (result['message']) {
                            this.myGrid.clearselection();
                            let messageDiv: any = document.getElementById('message');
                            messageDiv.innerText = result['message'];
                            this.msgNotification.open();
                            let data = this.activeRoute.snapshot.params;
                            let dt = {};
                            dt['activity'] = data['id'];
                            this.loadGridData(dt);
                        }
                        if (result['error']) {
                            this.myGrid.clearselection();
                            let messageDiv: any = document.getElementById('error');
                            messageDiv.innerText = result['error']['message'];
                            this.errNotification.open();
                            let data = this.activeRoute.snapshot.params;
                            let dt = {};
                            dt['activity'] = data['id'];
                            this.loadGridData(dt);
                        }
                    }, (error) => {
                        this.jqxLoader.close();
                        console.info(error);
                    });
                }
            } else {
                let messageDiv = document.getElementById('error');
                messageDiv.innerText = 'Please select some item to delete';
                this.errNotification.open();
            }
        })


    }; //render toolbar ends
}
