import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicalApprovalViewComponent } from './technical-approval-view.component';

describe('TechnicalApprovalViewComponent', () => {
  let component: TechnicalApprovalViewComponent;
  let fixture: ComponentFixture<TechnicalApprovalViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicalApprovalViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicalApprovalViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
