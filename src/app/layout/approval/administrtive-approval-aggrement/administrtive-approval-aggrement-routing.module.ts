import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministrtiveApprovalAggrementComponent } from './administrtive-approval-aggrement.component';

const routes: Routes = [
  {
    path: '',
    component: AdministrtiveApprovalAggrementComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrtiveApprovalAggrementRoutingModule { }
