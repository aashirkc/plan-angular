import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrtiveApprovalAggrementComponent } from './administrtive-approval-aggrement.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AdministrtiveApprovalAggrementRoutingModule } from './administrtive-approval-aggrement-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdministrtiveApprovalAggrementRoutingModule
  ],
  declarations: [AdministrtiveApprovalAggrementComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AdministrtiveApprovalAggrementModule { }
