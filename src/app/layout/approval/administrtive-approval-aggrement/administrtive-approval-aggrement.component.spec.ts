import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrtiveApprovalAggrementComponent } from './administrtive-approval-aggrement.component';

describe('AdministrtiveApprovalAggrementComponent', () => {
  let component: AdministrtiveApprovalAggrementComponent;
  let fixture: ComponentFixture<AdministrtiveApprovalAggrementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrtiveApprovalAggrementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrtiveApprovalAggrementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
