import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdministrativeApprovalService, ActivityDetailService, AllReportService, UnicodeTranslateService, NepaliNumberToWordService, DateConverterService, PrintingService } from '../../../shared';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { ChangePasswordComponent } from 'app/layout/utility/change-password/change-password.component';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-administrtive-approval-aggrement',
  templateUrl: './administrtive-approval-aggrement.component.html',
  styleUrls: ['./administrtive-approval-aggrement.component.scss']
})
export class AdministrtiveApprovalAggrementComponent implements OnInit {
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;

  activityId;
  date: any = '';
  dataLists: any = [];
  planAggrement: any;
  consumerCommiteName: string;
  consumerCommiteAddres: string;
  totalAmount: number = 0;
  foForm: FormGroup;
  heading: Array<any> = [];
  monitoringCommittees: Array<any> = [];
  committeeDetails: Array<any> = [];
  adyakshya: any = '';
  adyakshyaAddress: any;
  kosadakshya: any = '';
  kosadakshyaAddress: any;
  sachib: any = '';
  sachibAddress: any;
  type: boolean;
  wada: any = '';
  nnum: any;
  nword: any;
  isRul: boolean;
  distType: any;
  typeText: any;
  bidAmount: any;
  activityData: Object;
  // samjhautaAdapter: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private report: AllReportService,
    private aas: AdministrativeApprovalService,
    private ads: ActivityDetailService,
    private fb: FormBuilder,
    private unicode: UnicodeTranslateService,
    private ntw: NepaliNumberToWordService,
    private dcs: DateConverterService,
    private printingService: PrintingService,
    @Inject('DIST_TYPE') distType
  ) {
    this.distType = distType;
    this.foForm = this.fb.group({
      'ccname': [''],
      'cctitle': [''],
      'ccaddress': [''],
      'cccontact': [''],
      'ccdate': [''],
      'vname': [''],
      'vtitle': [''],
      'vaddress': [''],
      'vcontact': [''],
      'vdate': [''],
      'sname': [''],
      'stitle': [''],
      'saddress': [''],
      'scontact': [''],
      'sdate': [''],
      'oname': [''],
      'otitle': [''],
      'oaddress': [''],
      'ocontact': [''],
      'odate': [''],
      'aggrementDate': [''],
      'rose': [''],
      'firstparty': [''],
      'secondparty': [''],
      'estamount': [''],
      'estamountword': [''],
      'completiondate': [''],
      'activityName': [''],
      'wadaNo': [''],
      'aadhaar': [''],
      'fname': [''],
      'prop': [''],
      'toname': [''],
      'ttitle': [''],
      'tdate': [''],
      'myadDate': [''],
      'date': [''],
      'pCost': [''],
      'conName': [''],
      'conAddress': [''],
      'ndate': [''],
      'prasasan': [''],
      'padh': [''],
      'samjhautaShirta': [''],
      'rakam1': [''],
      'rakam2': [''],
      'rakam3': [''],
      'rakam4': [''],
      'gcc': [''],
      'bankAccount': [''],
      'newspaper': [''],
      'newspaperDate': [''],
      'contractNumber': [''],
      'bidAmount': [''],
      'contractDay': ['सात'],
      'awadhi': ['१ बर्ष'],
      'contractComments': ''
    })
  }

  ngOnInit() {
    this.activityId = this.route.snapshot.paramMap.get('id');

    this.isRul = false;
    const orgMaster = JSON.parse(localStorage.getItem('org'));
    if (orgMaster && orgMaster[0]) {
      const orgType = orgMaster[0]['orgType'];
      this.isRul = orgType === 'RUL';
    }

    this.date = this.dcs.getToday()['fulldate'];
    this.foForm.get('date').patchValue(this.date)

    let p = { "id": this.activityId }
    this.report.getKaryaDeshReport1(p).subscribe(res => {
      // console.log(res['samjhautaShirta'])
      // this.samjhautaAdapter = res['samjhautaShirta']
      try {
        const samjhautaShirta = JSON.parse(res['samjhautaShirta']);
        this.foForm.get('vname').patchValue(samjhautaShirta['vname'])
        this.foForm.get('vaddress').patchValue(samjhautaShirta['vaddress'])
        this.foForm.get('vcontact').patchValue(samjhautaShirta['vcontact'])
        this.foForm.get('vtitle').patchValue(samjhautaShirta['vtitle'])
        if (!this.foForm.get('vdate').value) { this.foForm.get('vdate').patchValue(this.date) }
        this.foForm.get('sname').patchValue(samjhautaShirta['sname'])
        this.foForm.get('saddress').patchValue(samjhautaShirta['saddress'])
        this.foForm.get('scontact').patchValue(samjhautaShirta['scontact'])
        this.foForm.get('stitle').patchValue(samjhautaShirta['stitle'])
        if (!this.foForm.get('sdate').value) { this.foForm.get('sdate').patchValue(this.date) }
        this.foForm.get('samjhautaShirta').patchValue(samjhautaShirta['samjhautaShirta']);
        this.bidAmount = samjhautaShirta['bidAmount'];
      } catch (e) {
        console.log(e);
      }
      // console.log(this.samjhautaAdapter)
    })

    this.unicode.initUnicode();
    this.getActivityDetail(this.activityId);
    this.aas.index({ activity: this.activityId, form: 'samjauta' }).subscribe((result: any) => {
      if (result['data']['workServiceType'] === 'Contract' || result['data']['workServiceType'] === 'Amanathan Marfat') {
        this.type = false;
      }
      else {
        this.type = true;
      }

      this.dataLists = result;
      if (this.dataLists['data']['workServiceType'] === 'Consumer Committe') {
        this.typeText = "उपभोक्ता समिति"
      }
      else if (this.dataLists['data']['workServiceType'] === 'Sahakari') {
        this.typeText = "श्रम सहकारी"
      }
      if (this.bidAmount && this.bidAmount != '') {
        this.foForm.get('bidAmount').patchValue(this.bidAmount)
      }
      else {
        this.foForm.get('bidAmount').patchValue(this.calculateBidAmount(
          this.dataLists && this.dataLists['techCost'],
          this.dataLists && this.dataLists['contractDetails']
          && this.dataLists['contractDetails']['contractAmount']));
      }
      //   console.log(this.dataLists['data'].agreementDate);
      //   debugger;
      // console.log(this.foForm);
      this.foForm.get('ndate').patchValue(this.dataLists['data'] && this.dataLists['data'].agreementDate);
      this.foForm.get('aggrementDate').patchValue(this.dataLists['data'] && this.dataLists['data'].agreementDate);

      if (!this.foForm.get('tdate').value) {
        this.foForm.get('tdate').patchValue(this.date);
      }
      this.committeeDetails = this.dataLists['CommitteDetails']
      this.committeeDetails = this.committeeDetails.filter(x => {
        return x.memberName !== ''
      })
      this.foForm.get('rose').patchValue('रोज ५')
      let secondParty = `श्री ${this.dataLists['contractDetails'] && this.dataLists['contractDetails']['name']}  ${this.dataLists['contractDetails'] && this.dataLists['contractDetails'].address} प्रो. ${this.dataLists['contractDetails'] && this.dataLists['contractDetails'].contactPerson}`
      let prop = `प्रो. श्री ${this.dataLists['contractDetails'] && this.dataLists['contractDetails'].contactPerson}`;
      if (this.dataLists && this.dataLists['data']['workServiceType'] === 'Amanathan Marfat') {
        secondParty = `श्री ${this.dataLists['data']['officeName']}, प्रो. ${this.dataLists['data']['orderName']} `
        prop = `प्रो. श्री ${this.dataLists['data']['orderName']}`;
      }
      this.foForm.get('prop').patchValue(prop);
      this.foForm.get('secondparty').patchValue(secondParty);
      this.foForm.get('myadDate').patchValue(this.date);
      this.foForm.get('estamount').patchValue(this.dataLists['contractDetails'] && this.dataLists['contractDetails']['contractAmount'])
      this.nword = this.ntw.getNepaliWord(Number(this.foForm.get('estamount').value));
      this.foForm.get('estamountword').patchValue(this.nword)
      this.foForm.get('completiondate').patchValue(this.dataLists['data'].estimatedWorkCompletionDate)
      this.foForm.get('activityName').patchValue(this.dataLists['ActivityDetails'].ActivityName)
      this.foForm.get('fname').patchValue(this.dataLists['contractDetails'] && this.dataLists['contractDetails'].name)
      this.foForm.get('ccname').patchValue(this.dataLists['CommitteDetails'] && this.dataLists['CommitteDetails'][0] && this.dataLists['CommitteDetails'][0].memberName)
      this.foForm.get('ccaddress').patchValue(this.dataLists['CommitteDetails'] && this.dataLists['CommitteDetails'][0] && this.dataLists['CommitteDetails'][0].committeAddress)
      this.foForm.get('cccontact').patchValue(this.dataLists['CommitteDetails'] && this.dataLists['CommitteDetails'][0] && this.dataLists['CommitteDetails'][0].phoneNumber)
      // this.foForm.get('ccdate').patchValue(this.dataLists['CommitteDetails'] && this.dataLists['CommitteDetails'][0] && this.dataLists['CommitteDetails'][0].issueDate)
      if (!this.foForm.get('ccdate').value) {
        this.foForm.get('ccdate').patchValue(this.date)
      }
      this.foForm.get('cctitle').patchValue(this.dataLists['CommitteDetails'] && this.dataLists['CommitteDetails'][0] && this.dataLists['CommitteDetails'][0].memberPost)
      this.foForm.controls['conName'].patchValue(this.dataLists['contractDetails'] && this.dataLists['contractDetails'].name);
      this.foForm.controls['conName'].patchValue(this.dataLists['contractDetails'] && this.dataLists['contractDetails'].name);
      this.foForm.controls['conAddress'].patchValue(this.dataLists['contractDetails'] && this.dataLists['contractDetails'].address);
      // this.foForm.controls[''].patchValue(this.dataLists['contractDetails'] && this.dataLists['contractDetails'].address);


      this.monitoringCommittees = result && result['monitoringCommittee'] || [];
      let officeCost = this.dataLists && this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromLocalLevel || 0;
      let ccCost = this.dataLists && this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromConsumerCommitte || 0;
      let ingoCost = this.dataLists && this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromIngo || 0;
      let ngoCost = this.dataLists && this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromNgo || 0;
      let stateCost = this.dataLists && this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromState || 0;
      let unionCost = this.dataLists && this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromUnion || 0;
      let others = this.dataLists && this.dataLists['RemainingPlan'] && this.dataLists['RemainingPlan'][0] && this.dataLists['RemainingPlan'][0].grantedFromOthers || 0;

      this.totalAmount = Number(officeCost) + Number(ccCost) + Number(ingoCost) + Number(ngoCost) + Number(stateCost) + Number(unionCost) + Number(others);

      try {
        console.log(this.totalAmount);
        this.totalAmount = Number(this.totalAmount.toFixed(2));
      } catch (e) { }

      this.consumerCommiteName = result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['consumerCommitteName'];
      this.consumerCommiteAddres = result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['consumerCommitteAddress'];

      this.report.getOrg().subscribe((result) => {
        if (result.length == 1 && result[0].error) {
          this.heading = [];
        } else {
          this.heading = result[0];
          // console.log(this.dataLists);
          // console.log(this.heading);
          const firstParty = `श्री ${this.heading['officeName']} ${this.heading['placeName']}, ${this.heading['district']}`;

          // console.info(this.heading);
          this.foForm.get('firstparty').patchValue(firstParty);
          this.foForm.get('wadaNo').patchValue(`${this.heading['officeName']} ${this.wada} `)
          this.foForm.get('oname').patchValue(this.heading['nameOfHead'])
          this.foForm.get('otitle').patchValue(this.heading['post'])
          this.foForm.get('oaddress').patchValue(this.heading['officeName'])
          // this.foForm.get('samjhautaShirta').patchValue(this.heading['samjhautaShirta'])

          // this.foForm.get('ccaddress').patchValue(this.heading['orgName'])
          this.foForm.get('ocontact').patchValue(this.heading['officeTelNo'])
          if (!this.foForm.get('odate').value) {
            this.foForm.get('odate').patchValue(this.date)
          }
          if (!this.foForm.get('newspaperDate').value) {
            this.foForm.get('newspaperDate').patchValue(this.date);
          }

          this.foForm.get('toname').patchValue(this.heading['nameOfHead'])
          this.foForm.get('ttitle').patchValue(this.heading['post'])

          this.foForm.get('prasasan').patchValue(this.heading['nameOfHead'])
          this.foForm.get('padh').patchValue(this.heading['post'])

        }

      }, (error) => {
        console.info(error);
      });
      for (let i = 0; i < result['CommitteDetails'].length; i++) {
        if (result['CommitteDetails'][i]['memberPost'] == 'अध्यक्ष') {
          this.adyakshya = result['CommitteDetails'][i]['memberName'];
          this.adyakshyaAddress = result['CommitteDetails'][i]['committeAddress'];
        }
        else if (result['CommitteDetails'][i]['memberPost'] == 'कोषाध्यक्ष') {
          this.kosadakshya = result['CommitteDetails'][i]['memberName'];
          this.kosadakshyaAddress = result['CommitteDetails'][i]['committeAddress'];
        }
        else if (result['CommitteDetails'][i]['memberPost'] == 'सचिव') {
          this.sachib = result['CommitteDetails'][i]['memberName'];
          this.sachibAddress = result['CommitteDetails'][i]['committeAddress'];
        }
      }
    });

  }
  getActivityDetail(id) {
    this.ads.show(id).subscribe(
      result => {
        this.wada = result['implementingAgencyTypeLevel2Name'] || ''
        this.activityData = result;
        this.foForm.get('pCost').patchValue(result['pCost'])
      },
      error => {
        console.log(error);

      }
    )
  }
  ngAfterViewInit() {

  }

  // changes(){
  //   let shirtaData = {
  //     "samjhautaShirta" : this.foForm.value.samjhautaShirta
  //   }
  //   // console.log(shirtaData);
  //   this.report.putKaryaDeshReport(this.activityId, shirtaData).subscribe(res=>{
  //     if (res['message']) {
  //       let messageDiv: any = document.getElementById('message');
  //       messageDiv.innerText = res['message'];
  //       this.msgNotification.open();
  //     }
  //   })
  // }

  toNumber = (str: string): string => {
    if (str) {
      const data = Number.parseFloat(str);
      if (data !== NaN) {
        return data.toString();
      }
    }
    return '0';
  }

  calculateBidAmount(techCost, contractAmount) {
    const cost_estimate = techCost;
    const bid_price = contractAmount;
    let retData;
    if (bid_price >= 0.85 * cost_estimate) {
      retData = 0.05 * bid_price;
    } else {
      retData = (((0.85 * cost_estimate) - bid_price) * 0.5) + 0.05 * bid_price
    }
    return Number(retData).toFixed(2).toString();
  }

  amountChange() {
    this.nword = this.ntw.getNepaliWord(Number(this.foForm.get('estamount').value));
    this.foForm.get('estamountword').patchValue(this.nword)
  }

  // add(){
  //   console.log('hdsbkjl')
  //   this.totalAmount =  this.totalAmount + Number(this.foForm.get('gcc').value)
  // }
  print() {
    this.sendData();
    const printContents = document.getElementById('print-section').innerHTML;
    // document.getElementById('print-section').innerHTML.className = 'nepaliText';
    this.printingService.printContents(printContents, 'Administrative Approval Agreement', false, `
      @page{
        /* this affects the margin in the printer settings */
        margin: 24.4mm 24.4mm 24.4mm 37.1mm;
      }
      @media print{
        p, div, td, th {
          font-size: 18px !important;
        }
        table{
          margin: 3px !important;
        }
        p{
          margin-bottom: 1px !important;
        }
        td, th{
          padding: 3px !important;
        }
      }
    `);
    // this.changes();
  }

  sendData() {
    const data = this.foForm.value;
    this.aas.saveReport(this.activityId, data).subscribe((data) => {
      // console.log(data)
    }, error => console.log(error));
    // console.log(this.foForm.value);
  }

  close() {
    this.router.navigate(['/approval/administrative-approval']);
  }

}
