import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EstimateAbstractOfCostComponent } from './estimate-abstract-of-cost.component';

const routes: Routes = [
  {
    path: '',
    component: EstimateAbstractOfCostComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstimateAbstractOfCostRoutingModule { }
