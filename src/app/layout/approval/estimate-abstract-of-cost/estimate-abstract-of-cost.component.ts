import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EstimateAbstractOfCostService, PlanningYearService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

@Component({
  selector: 'app-estimate-abstract-of-cost',
  templateUrl: './estimate-abstract-of-cost.component.html',
  styleUrls: ['./estimate-abstract-of-cost.component.scss']
})
export class EstimateAbstractOfCostComponent implements OnInit {
  EstimateAbstractOfCostForm: FormGroup;
  EstimateAbstractOfCostDetailsForm: FormGroup;
  update: boolean = false;
  updateDetails: boolean = false;
  source: any;
  sourceDetails: any;
  activityAdapter: Array<any> = [];
  dataAdapter: any;
  dataDetailsAdapter: any;
  columns: any[] = [];
  columnsDetails: any[] = [];
  editrow: number = -1;
  columngroups: any[];
  transData: any;
  fullDate: string;
  unitAdapter: Array<any> = [
    {
      name: 'm2'
    },
    {
      name: 'm3'
    },
    {
      name: 'ft2'
    },
    {
      name: 'ft3'
    }
  ]
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('myDetailsGrid') myDetailsGrid: jqxGridComponent;
  @ViewChild('myComboBox') myComboBox: jqxComboBoxComponent;
  planYearAdapter: Array<any> = [];
  constructor(
    private fb: FormBuilder,
    private estimateCostService: EstimateAbstractOfCostService,
    private pys: PlanningYearService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  ngOnInit() {
    this.pys.index({}).subscribe((res) => {
      this.planYearAdapter = res;
      if (this.planYearAdapter.length > 0) {
        let planCode = this.planYearAdapter[0].yearCode || '';
        this.EstimateAbstractOfCostForm.get('planYear').patchValue(planCode);
      }
    }, (error) => {
      console.info(error);
    });
    this.estimateCostService.indexActivity({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  getTranslation() {
    this.translate.get(['SN', "RELOAD", 'UNIT', "ACTIVITY_NAME", "DESCRIPTION", "NOM", "LENGTH", "HEIGHT", "REMARKS", "BREADTH", "QUANTITY", "RATE", "AMOUNT", "UNIT", "DESCRIPTION_OF_WORK", "EDIT", "ACTION", "ADD_DETAILS", "START_DATE", "END_DATE", "BS", "AD", "DELETE_ROW", "YES", "NO", "YESE", "NOE"]).subscribe((translation: [string]) => {
      this.transData = translation;
      this.loadGrid();
    });
  }

  loadGridData(post) {
    this.jqxLoader.open();
    this.estimateCostService.index(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
  loadGridDetailsData(post) {
    this.jqxLoader.open();
    this.estimateCostService.indexDetails(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.sourceDetails.localdata = [];
      } else {
        this.sourceDetails.localdata = res;
      }
      this.myDetailsGrid.updatebounddata();

      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  createForm() {
    this.EstimateAbstractOfCostForm = this.fb.group({
      'id': ['',],
      'activity': ['', Validators.required],
      'planYear': [''],
      'unit': ['', Validators.required],
      'descriptionOfWork': ['', Validators.required],
      'descriptionOfWorkSn': ['', Validators.required],
    });
    this.EstimateAbstractOfCostDetailsForm = this.fb.group({
      'eacId': ['',],
      'sn': ['',],
      'no': [''],
      'length': [''],
      'breadth': [''],
      'heigth': [''],
      'qty': ['', Validators.required],
      'unit': ['', Validators.required],
      'rate': ['', Validators.required],
      'amount': ['', Validators.required],
      'remarks': ['', Validators.required],
      'description': ['']
    });
  }
  comboBoxOnChange() {
    let dt = {};
    dt['activity'] = this.myComboBox.val();
    this.loadGridData(dt);

    this.sourceDetails.localdata = [];
    this.myDetailsGrid.updatebounddata();
    this.EstimateAbstractOfCostDetailsForm.reset();
  }
  ngAfterViewInit() {

  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'activity', type: 'string' },
          { name: 'activityName', type: 'string' },
          { name: 'descriptionOfWork', type: 'string' },
          { name: 'descriptionOfWorkSn', type: 'string' },
          { name: 'unit', type: 'string' },
        ],
        id: 'id',
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      { text: this.transData['SN'], datafield: 'descriptionOfWorkSn', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 80, },
      { text: this.transData['DESCRIPTION_OF_WORK'], datafield: 'descriptionOfWork', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 823, },
      { text: this.transData['UNIT'], datafield: 'unit', columntype: 'textbox', editable: false, width: 80, filtercondition: 'starts_with' },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 100, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          let dt = {};
          dt['id'] = dataRecord['id'];
          dt['activity'] = dataRecord['activity'];
          dt['unit'] = dataRecord['unit'];
          dt['descriptionOfWork'] = dataRecord['descriptionOfWork'];
          dt['descriptionOfWorkSn'] = dataRecord['descriptionOfWorkSn'];
          this.EstimateAbstractOfCostForm.setValue(dt);
          this.update = true;
        }
      },
      {
        text: this.transData['ACTION'], datafield: 'Add', sortable: false, filterable: false, width: 100, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['ADD_DETAILS'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          this.EstimateAbstractOfCostDetailsForm.controls['eacId'].setValue(dataRecord['id']);
          this.EstimateAbstractOfCostDetailsForm.controls['unit'].setValue(dataRecord['unit']);
          let dt = {};
          dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
          this.loadGridDetailsData(dt);
        }
      }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

    this.sourceDetails =
      {
        datatype: 'json',
        datafields: [
          { name: 'eacId', type: 'string' },
          { name: 'no', type: 'string' },
          { name: 'sn', type: 'string' },
          { name: 'length', type: 'string' },
          { name: 'breadth', type: 'string' },
          { name: 'heigth', type: 'string' },
          { name: 'qty', type: 'string' },
          { name: 'unit', type: 'string' },
          { name: 'rate', type: 'string' },
          { name: 'description', type: 'string' },
          { name: 'amount', type: 'string' },
          { name: 'remarks', type: 'string' },
        ],
        id: 'id',
        localdata: [],
        pagesize: 20
      }

    this.dataDetailsAdapter = new jqx.dataAdapter(this.sourceDetails);
    this.columnsDetails = [

      { text: this.transData['NOM'], datafield: 'no', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 40, },
      { text: this.transData['DESCRIPTION'], datafield: 'description', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 200, },
      { text: this.transData['LENGTH'], datafield: 'length', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 80, },
      { text: this.transData['BREADTH'], datafield: 'breadth', columntype: 'textbox', editable: false, width: 80, filtercondition: 'starts_with' },
      { text: this.transData['HEIGHT'], datafield: 'heigth', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 80, },
      { text: this.transData['QUANTITY'], datafield: 'qty', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 80, },
      { text: this.transData['UNIT'], datafield: 'unit', columntype: 'textbox', editable: false, width: 80, filtercondition: 'starts_with' },
      { text: this.transData['RATE'], datafield: 'rate', columntype: 'textbox', editable: false, width: 80, filtercondition: 'starts_with' },
      { text: this.transData['AMOUNT'], datafield: 'amount', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 80, },
      { text: this.transData['REMARKS'], datafield: 'remarks', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 205, },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 137, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myDetailsGrid.getrowdata(this.editrow);
          let dt = {};
          dt['eacId'] = dataRecord['eacId'];
          dt['no'] = dataRecord['no'];
          dt['length'] = dataRecord['length'];
          dt['breadth'] = dataRecord['breadth'];
          dt['heigth'] = dataRecord['heigth'];
          dt['qty'] = dataRecord['qty'];
          dt['unit'] = dataRecord['unit'];
          dt['sn'] = dataRecord['sn'];
          dt['rate'] = dataRecord['rate'];
          dt['amount'] = dataRecord['amount'];
          dt['remarks'] = dataRecord['remarks'];
          dt['description'] = dataRecord['description'];
          this.EstimateAbstractOfCostDetailsForm.setValue(dt);
          this.updateDetails = true;
        }
      }
    ];

  }

  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE_ROW'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.myGrid.getselectedrowindexes();
      let ids = [];
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['id']);
      }

      if (ids.length > 0) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.estimateCostService.destroy(ids).subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              let dt = {};
              dt['activity'] = this.myComboBox.val();

              this.loadGridData(dt);
            }
            if (result['error']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              let dt = {};
              dt['activity'] = this.myComboBox.val();

              this.loadGridData(dt);
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      let dt = {};
      dt['activity'] = this.myComboBox.val();

      this.loadGridData(dt);
    });

  }; //render toolbar ends

  rendertoolbarDetails = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer4 = document.createElement('div');
    let buttonContainer5 = document.createElement('div');

    buttonContainer4.id = 'buttonContainer4';
    buttonContainer5.id = 'buttonContainer5';

    buttonContainer4.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer5.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer4);
    container.appendChild(buttonContainer5);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer4', 'jqxButton', { width: 150, value: this.transData['DELETE_ROW'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer5', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.myDetailsGrid.getselectedrowindexes();
      let ids = [];
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myDetailsGrid.getrowdata(Number(id[i]));
        let dta = dataRecord['eacId'] + dataRecord['sn'];
        ids.push(dataRecord['eacId'] + '' + dataRecord['sn']);
      }

      if (ids.length > 0) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.estimateCostService.destroyDetails(ids).subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.myDetailsGrid.clearselection();
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              let dt = {};
              dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
              this.loadGridDetailsData(dt);
            }
            if (result['error']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              let dt = {};
              dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
              this.loadGridDetailsData(dt);
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      let dt = {};
      dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
      this.loadGridDetailsData(dt);
    });

  }; //render toolbar ends

  saveBtn(post) {
    this.jqxLoader.open();
    this.estimateCostService.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.EstimateAbstractOfCostForm.reset();
          let dt = {};
          dt['activity'] = this.myComboBox.val();

          this.loadGridData(dt);
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

  saveDetailsBtn(post) {
    this.jqxLoader.open();
    this.estimateCostService.storeDetails(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();

          let dt = {};
          dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
          this.loadGridDetailsData(dt);
          this.EstimateAbstractOfCostDetailsForm.controls['no'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['lengthDemo'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['breadth'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['heigth'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['qty'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['rate'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['amount'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['description'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['remarks'].setValue('');
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

  updateBtn(post) {
    // post['status'] = post['status'] == "No" ? 'N' : 'Y';
    this.jqxLoader.open();
    this.estimateCostService.update(post['id'], post).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.update = false;
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.EstimateAbstractOfCostForm.reset();
          let dt = {};
          dt['activity'] = this.myComboBox.val();
          this.loadGridData(dt);
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateBtn() {
    this.update = false;
    this.EstimateAbstractOfCostForm.reset();
    let dt = {};
    dt['activity'] = this.myComboBox.val();
    this.loadGridData(dt);
  }

  updateDetailsBtn(post) {
    this.jqxLoader.open();
    this.estimateCostService.updateDetails(post['sn'], post).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.updateDetails = false;
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          let dt = {};
          dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
          this.loadGridDetailsData(dt);
          this.EstimateAbstractOfCostDetailsForm.controls['no'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['lengthDemo'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['breadth'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['heigth'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['qty'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['rate'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['amount'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['description'].setValue('');
          this.EstimateAbstractOfCostDetailsForm.controls['remarks'].setValue('');
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateDetailsBtn() {
    this.updateDetails = false;
    this.EstimateAbstractOfCostDetailsForm.reset();
    let dt = {};
    dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
    this.loadGridDetailsData(dt);
  }

  calculate() {
    let no = this.EstimateAbstractOfCostDetailsForm.value['no'] || 1;
    let lengthDemo = this.EstimateAbstractOfCostDetailsForm.value['lengthDemo'] || 1;
    let heigth = this.EstimateAbstractOfCostDetailsForm.value['heigth'] || 1;
    let breadth = this.EstimateAbstractOfCostDetailsForm.value['breadth'] || 1;
    let qty = no * lengthDemo * heigth * breadth;
    this.EstimateAbstractOfCostDetailsForm.controls['qty'].patchValue(qty);
    this.dataChange();
  }
  dataChange() {
    let rate = this.EstimateAbstractOfCostDetailsForm.value['rate'] || 0;
    let qty = this.EstimateAbstractOfCostDetailsForm.value['qty'] || 0;
    let amount = rate * qty;
    this.EstimateAbstractOfCostDetailsForm.controls['amount'].patchValue(amount);
  }
}
