import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EstimateAbstractOfCostComponent } from './estimate-abstract-of-cost.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { EstimateAbstractOfCostRoutingModule } from './estimate-abstract-of-cost-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    EstimateAbstractOfCostRoutingModule,
  ],
  declarations: [EstimateAbstractOfCostComponent],
  // schemas: [ NO_ERRORS_SCHEMA ]
})
export class EstimateAbstractOfCostModule { }
