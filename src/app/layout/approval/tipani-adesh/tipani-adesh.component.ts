import { Component, OnInit, ViewChild, Inject, } from '@angular/core';
import { AdministrativeApprovalService, DateConverterService, AllReportService, UnicodeTranslateService, NepaliNumberToWordService, PrintingService } from '../../../shared';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { ProgramsService } from 'app/shared/services/programs.service';

@Component({
  selector: 'app-tipani-adesh',
  templateUrl: './tipani-adesh.component.html',
  styleUrls: ['./tipani-adesh.component.scss']
})
export class TipaniAdeshComponent implements OnInit {
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  date: any;
  imgSrc: any = 'assets/images/logo.jpg';
  SearchDatas: Array<any> = [];
  committee: Array<any> = [];
  heading: Array<any> = [];
  adyakshya: any;
  adyakshyaAddress: any;
  kosadakshya: any;
  kosadakshyaAddress: any;
  totalAmount: number = 0;
  sachib: any;
  sachibAddress: any;
  foForm: FormGroup;
  planYear: any;
  programName: any;
  activityName: any;
  loadedData: boolean = false;
  contractName: any;
  consumerCommitteeName: any;
  type: boolean;
  activityDetailData: any;
  orgMaster: any[];
  distType: any;
  stharAdapter: any = [{ value: "n", name: 'नगर स्तरिय' }, { value: "w", name: 'वडा स्तरिय' }, { value: "p", name: 'प्रदेश स्तरिय' }, { value: "s", name: 'संसदिय स्तरिय' }]
  isRul: boolean;
  typeText: string;
  workServiceType: any;



  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private reprot: AllReportService,
    private dcs: DateConverterService,
    private unicode: UnicodeTranslateService,
    private fb: FormBuilder,
    private aas: AdministrativeApprovalService,
    private ntw: NepaliNumberToWordService,
    private programService: ProgramsService,
    private printingService: PrintingService,
    @Inject('DIST_TYPE') distType
  ) {
    try {
      this.orgMaster = JSON.parse(localStorage.getItem('org'));
    } catch (e) { }
    this.distType = distType;
    this.foForm = this.fb.group({
      'currentDate': [''],
      'contractDate': [''],
      'adhikrit': [''],
      'planYear': [''],
      'tippaniNo': [''],
      'programName': [''],
      'activityName': [''],
      'contractName': [''],
      'consumerCommitteeName': [''],
      'paarit': [''],
      'wardNo': [''],
      'fromNagarpalika': [''],
      'fromNagarpalikaWord': [''],
      'fromPopPercent': [''],
      'fromPop': [''],
      'fromPopWord': [''],
      'totalAmount': [''],
      'totalAmountWord': [''],
      'wardNo1': [''],
      'activityName1': [''],
      'fromPopPercent1': [''],
      'fromPop1': [''],
      'fromPopWord1': [''],
      'bplace1': [''],
      'bname1': [''],
      'bplace2': [''],
      'bname2': [''],
      'type': [true],
      'conName': [''],
      'conAddress': [''],
      'contactPerson': [''],
      'completionDate': [''],
      'activityAddress': [''],
      'techCost': [''],
      'pCost': [''],
      'fromConsumerCommittee': [''],
      'business1': [''],
      'business2': [''],
      'business3': [''],
      'pesh': [''],
      'raya1': [''],
      'raya2': [''],
      'raya3': [''],
      'pramukh': [''],
      'officeName': [''],
      'orderName': [''],
      'agreementDate': [''],
      'totalBudget': 0,
      'proposedCost': 0,
      'workServiceType': ''
    })
  }

  ngOnInit() {
    this.foForm.get('adhikrit').patchValue('उमेश कुमार यादव');
    this.date = this.dcs.getToday()['fulldate'];
    this.isRul = false;
    const orgMaster = JSON.parse(localStorage.getItem('org'));
    if (orgMaster && orgMaster[0]) {
      const orgType = orgMaster[0]['orgType'];
      this.isRul = orgType === 'RUL';
    }
    this.getData();

  }

  private getKaryadeshReport() {
    this.reprot.getKaryaDeshReport1({ 'id': this.activeRoute.snapshot.params['id'], }).subscribe((res) => {
      if (res['tipaniAdesh']) {
        this.loadedData = true;
        let a = JSON.parse(res['tipaniAdesh']);
        let sourceData = a['tippani'];
        this.type = sourceData['type'];
        this.foForm.patchValue(sourceData);
        this.planYear = sourceData['planYear'];
        this.programName = sourceData['programName'];
        this.activityName = sourceData['activityName'];
        this.contractName = sourceData['contractName'];
        this.consumerCommitteeName = sourceData['consumerCommitteeName'];
        this.getAas();
      }
    }, (error) => {
      console.info(error);
    });
  }

  getAas() {
    this.aas.index({ activity: this.activeRoute.snapshot.params['id'] }).subscribe(
      result => {
        this.workServiceType = result['data']['workServiceType'];
        if (result['data']['workServiceType'] === 'Consumer Committe') {
          this.typeText = "उपभोक्ता समिति"
        }
        else if (result['data']['workServiceType'] === 'Sahakari') {
          this.typeText = "श्रम सहकारी"
        }
      });
  }

  getData() {
    this.aas.index({ activity: this.activeRoute.snapshot.params['id'], form: 'tippani' }).subscribe(
      result => {
        this.loadedData = true;
        this.activityDetailData = result;
        this.workServiceType = result['data']['workServiceType'];
        if (this.activityDetailData['data']['workServiceType'] === 'Consumer Committe') {
          this.typeText = "उपभोक्ता समिति"
        } else if (this.activityDetailData['data']['workServiceType'] === 'Sahakari') {
          this.typeText = "श्रम सहकारी"
        }
        this.foForm.controls['workServiceType'].patchValue(this.activityDetailData['data']['workServiceType'])
        if (this.activityDetailData['CommitteDetails'].length > 0) {
          this.type = true;
          this.foForm.controls['type'].setValue(true);
        }

        if (this.workServiceType === 'Contract') {
          this.type = false;
          this.foForm.controls['type'].setValue(false);
          try {
            const contractData = this.activityDetailData['contractDetails'].filter(d => d.selected);
            this.foForm.controls['contactPerson'].patchValue(contractData[0].contactPerson);
            this.foForm.controls['conName'].patchValue(contractData[0].address);
            this.foForm.controls['conAddress'].patchValue(contractData[0].name);
          } catch (e) { }
        } else if (this.workServiceType === 'Consumer Committe') {
          this.foForm.controls['conAddress'].patchValue(this.activityDetailData['PlanAgreement'][0]['consumerCommitteName']);
          this.foForm.controls['contactPerson'].patchValue(this.activityDetailData['CommitteDetails'][0]['memberName']);
        }

        if (this.activityDetailData && this.activityDetailData['data']) {
          const orderName = this.activityDetailData['data']['orderName']
          this.foForm.controls['orderName'].patchValue(`श्री ${orderName}`);
          try {
            const organization = JSON.parse(localStorage.getItem("org"))[0];
            this.foForm.controls['officeName'].patchValue(organization['officeName']);
          } catch (e) { }
          this.foForm.controls['agreementDate'].patchValue(this.activityDetailData['data']['agreementDate']);
        }
        this.foForm.controls['techCost'].patchValue(this.activityDetailData['techCost']);
        this.foForm.controls['pCost'].patchValue(this.activityDetailData['proposedCost']);

        this.foForm.controls['totalBudget'].patchValue(this.activityDetailData['ActivityDetails']['totalBudget'] || 0);
        this.foForm.controls['proposedCost'].patchValue(this.activityDetailData['proposedCost'] || 0);

        this.reprot.getKaryaDeshReport(this.activeRoute.snapshot.params['id'], {}).subscribe((res) => {
          this.getKaryadeshReport();
          if (res && res.length == 1 && res[0].error || res && res['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = res[0].error;
            this.SearchDatas = null;
          } else if (res) {

            this.SearchDatas = res[0] || null;
            this.committee = res[0]['committee'];
            let officeCost;
            let ccCost;
            let ingoCost;
            let ngoCost;
            let stateCost;
            let others;
            if (!this.foForm.controls['currentDate'].value) {
              this.foForm.controls['currentDate'].patchValue(this.date);
            }
            this.planYear = this.SearchDatas['planYear']
            this.programName = this.SearchDatas['programName'];
            this.activityName = this.SearchDatas['activityName'];
            this.contractName = this.SearchDatas['contractName'];
            this.consumerCommitteeName = this.SearchDatas['consumerCommitteeName'];
            this.foForm.controls['planYear'].patchValue(this.SearchDatas['planYear']);
            this.foForm.controls['fromPopPercent'].patchValue('१५%');
            this.foForm.controls['programName'].patchValue(this.SearchDatas['programName']);
            this.foForm.controls['tippaniNo'].patchValue(this.SearchDatas['tippaniNo']);
            this.foForm.controls['activityName'].patchValue(this.SearchDatas['activityName']);
            this.foForm.controls['contractName'].patchValue(this.SearchDatas['contractName']);
            if (this.orgMaster && this.orgMaster[0] && this.orgMaster[0]['orgType']) {
              this.foForm.controls['paarit'].patchValue(this.orgMaster[0]['orgType'] === 'RUL' ? 'गाउँसभा' : 'नगरसभा');
            }
            // console.log(this.SearchDatas)
            if (this.SearchDatas['wardNumber'])
              this.foForm.controls['wardNo'].patchValue(this.SearchDatas['wardNumber'] || '');
            else
              this.foForm.controls['wardNo'].patchValue('नगर स्तरीय');

            this.foForm.controls['wardNo1'].patchValue(this.SearchDatas['wardNumber'] || '');

            this.foForm.controls['bplace1'].patchValue('');
            this.foForm.controls['bname1'].patchValue('');

            this.foForm.controls['bplace2'].patchValue('सघारा');

            this.foForm.controls['consumerCommitteeName'].patchValue(this.SearchDatas['consumerCommitteeName']);
            this.foForm.controls['fromNagarpalika'].patchValue(this.SearchDatas['fromNagarpalika']);
            this.foForm.controls['fromNagarpalikaWord'].patchValue(this.ntw.getNepaliWord(this.SearchDatas['fromNagarpalika']));

            this.foForm.controls['contractDate'].patchValue(this.SearchDatas['contractDate'] || '');
            this.foForm.controls['completionDate'].patchValue(this.SearchDatas['completionDate']);
            this.foForm.controls['activityAddress'].patchValue(this.SearchDatas['activityAddress']);
            this.foForm.controls['pCost'].patchValue(this.SearchDatas['pCost']);
            this.foForm.controls['fromConsumerCommittee'].patchValue(this.SearchDatas['fromConsumerCommittee']);
            this.foForm.controls['business1'].patchValue(this.SearchDatas['contractName']);

            this.foForm.controls['fromPopWord'].patchValue(this.ntw.getNepaliWord(this.SearchDatas['fromConsumerCommittee']))

            res[0].fromNepalSarkar != 'null' && res[0].fromNepalSarkar != '' ? officeCost = res[0].fromNepalSarkar : officeCost = 0;
            res[0].fromPradeshSarkar != 'null' && res[0].fromPradeshSarkar != '' ? ccCost = res[0].fromPradeshSarkar : ccCost = 0;
            res[0].fromNagarpalika != 'null' && res[0].fromNagarpalika != '' ? ingoCost = res[0].fromNagarpalika : ingoCost = 0;
            res[0].fromWada != 'null' && res[0].fromWada != '' ? ngoCost = res[0].fromWada : ngoCost = 0;
            res[0].fromConsumerCommittee != 'null' && res[0].fromConsumerCommittee != '' ? stateCost = res[0].fromConsumerCommittee : stateCost = 0;
            res[0].fromOthers != 'null' && res[0].fromOthers != '' ? others = res[0].fromOthers : others = 0;
            this.totalAmount = Number(officeCost) + Number(ccCost) + Number(ingoCost) + Number(ngoCost) + Number(stateCost) + Number(others);

            this.foForm.controls['totalAmount'].patchValue(this.totalAmount);
            this.foForm.controls['totalAmountWord'].patchValue(this.ntw.getNepaliWord(this.totalAmount))

            for (let i = 0; i < this.committee.length; i++) {
              if (this.committee[i]['post'] == 'अध्यक्ष') {
                this.adyakshya = this.committee[i]['name'];
                this.adyakshyaAddress = this.committee[i]['address'];
              }
              else if (this.committee[i]['post'] == 'कोषाध्यक्ष') {
                this.kosadakshya = this.committee[i]['name'];
                this.kosadakshyaAddress = this.committee[i]['address'];
              }
              else if (this.committee[i]['post'] == 'सचिव') {
                this.sachib = this.committee[i]['name'];
                this.sachibAddress = this.committee[i]['address'];
              }
            }
            this.reprot.getOrg().subscribe((result) => {
              if (result.length == 1 && result[0].error) {
                this.heading = [];
              } else {
                this.heading = result[0];


              }

            }, (error) => {
              console.info(error);
            });
          }

        }, (error) => {
          console.info(error);
          this.SearchDatas = null;
        });
      },
      error => {
        console.log(error);

      }
    )
  }

  close() {
    this.router.navigate(['/approval/administrative-approval']);
  }

  ngAfterViewInit() {
    this.unicode.initUnicode();
  }

  digitWord(value, ctrlName: string): void {
    this.foForm.get(ctrlName).patchValue(this.ntw.getNepaliWord(value));
  }

  saveData(post) {
    let postData = {
      'tippani': post,
    };
    console.log(postData);
    this.reprot.patchKaryaDeshReport(this.activeRoute.snapshot.params['id'], postData).subscribe(
      result => {
        console.log(result);
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {

      }
    );
  }

  print(post) {
    const printContents = document.getElementById('printSection').innerHTML;
    // popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    this.printingService.printContents(printContents, 'कार्य आदेश', false, `
      @page{
        /* this affects the margin in the printer settings */
        margin: 24.4mm 24.4mm 24.4mm 37.1mm;
      }
      @media print{
        p, td {
          font-size: 25px !important;
        }
      }
    `)
    // popupWin.document.open();
    // popupWin.document.write(`
    //   <html>
    //     <head>
    //       <title>कार्य आदेश</title>
    //       <style>
    //       @font-face {
    //         font-family: 'preeti';
    //         src: url('../assets/css/Preeti.TTF');
    //         font-weight: normal;
    //         font-style: normal;
    //     }
    //     .show_div{
    //       display:block !important;
    //     }
    //     .hide_div{
    //       display:none !important;
    //     }
    //     .printStyle{
    //       padding:0 !important;
    //     }
    //     @page
    //     {
    //         size: auto;   /* auto is the initial value */

    //         /* this affects the margin in the printer settings */
    //         /* margin: 24.4mm 24.4mm 24.4mm 37.1mm; */
    //     }
    //     .preeti{
    //         font-family:'preeti';
    //         font-size:18px;
    //     }
    //       //........Customized style.......
    //       </style>
    //     </head>
    // <body onload="window.print();window.close()">${printContents}</body>
    //   </html>`
    // );
    // popupWin.document.close();
    this.saveData(post);
  }

}
