import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TipaniAdeshComponent } from './tipani-adesh.component';
import { TipaniAdeshRoutingModule } from './tipani-adesh-routing.module';
import { SharedModule } from '../../../shared/modules/shared.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TipaniAdeshRoutingModule
  ],
  declarations: [TipaniAdeshComponent]
})
export class TipaniAdeshModule { }
