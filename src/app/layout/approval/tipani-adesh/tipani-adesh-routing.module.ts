import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TipaniAdeshComponent } from './tipani-adesh.component';

const routes: Routes = [
  {
    path: '',
    component: TipaniAdeshComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TipaniAdeshRoutingModule { }
