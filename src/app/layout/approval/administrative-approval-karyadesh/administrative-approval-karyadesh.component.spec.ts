import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrativeApprovalKaryadeshComponent } from './administrative-approval-karyadesh.component';

describe('AdministrativeApprovalKaryadeshComponent', () => {
  let component: AdministrativeApprovalKaryadeshComponent;
  let fixture: ComponentFixture<AdministrativeApprovalKaryadeshComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrativeApprovalKaryadeshComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrativeApprovalKaryadeshComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
