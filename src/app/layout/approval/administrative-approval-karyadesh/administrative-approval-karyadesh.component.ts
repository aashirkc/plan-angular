import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, Compiler, Injector, NgModuleRef, AfterViewInit, NgModule, Inject } from '@angular/core';
import { AdministrativeApprovalService, ActivityDetailService, DateConverterService, LetterheadComponent, AllReportService, UnicodeTranslateService, ProgressReportingTippaniAadeshService, PrintingService } from '../../../shared';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-administrative-approval-karyadesh',
  templateUrl: './administrative-approval-karyadesh.component.html',
  styleUrls: ['./administrative-approval-karyadesh.component.scss']
})
export class AdministrativeApprovalKaryadeshComponent implements OnInit, AfterViewInit {

  date: any;
  imgSrc: any = 'assets/images/logo.jpg';
  SearchDatas: Array<any> = [];
  heading: Array<any> = [];
  foForm: FormGroup;
  type: boolean;
  ifWard: boolean;
  activityDetailData: any;
  organizationMasterData: any;

  @ViewChild('letterHead') letterHead: LetterheadComponent;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  orgMaster: any;
  distType: any;
  constructor(
    private router: Router,
    private ads: ActivityDetailService,
    private activeRoute: ActivatedRoute,
    private reprot: AllReportService,
    private dcs: DateConverterService,
    private fb: FormBuilder,
    private unicode: UnicodeTranslateService,
    private aas: AdministrativeApprovalService,
    private prtas: ProgressReportingTippaniAadeshService,
    private printingService: PrintingService,
    @Inject('DIST_TYPE') distType
  ) {
    this.distType = distType;
    this.orgMaster = JSON.parse(localStorage.getItem('org'))
    this.foForm = this.fb.group({
      'prasasan': [''],
      'engineer1': [''],
      'engineer2': [''],
      'engineer3': [''],
      'engineer4': [''],
      'engineer5': [''],
      'engineer6': [''],
      'engineer7': [''],
      'currentDate': [''],
      'plannedStartDate': [''],
      'contractDate': [''],
      'completionDate': [''],
      'planY': [''],
      'wadaNo': [''],
      'activityName': [''],
      'focusAreaLevel1NameNepali': [''],
      'activityAddress': [''],
      'ccName': [''],
      'ccAddress': [''],
      'conName': [''],
      'conAddress': [''],
      'prabhidikSakha1': [''],
      'type': [true],
      'paarit': [''],
      'sthar': [''],
      'padh': [''],
      'contactPerson': [''],
      'contractNumber': [''],
      'contractDays': ['२८'],
      'municipalityName': [''],
      'adhikrit': [''],
      'adhikritPost': [''],
      'lekhaSakha': [''],
      'ccNameAddress': [''],
      'prabhidikSakha': [''],
      'phone': [''],
      'municipality': [''],
      'estimatedCost': [''],
      'techCost': [''],
      'pCost': [''],
    })
  }

  ngOnInit() {

    this.prtas.getOrganizationName().subscribe((res) => {
      this.organizationMasterData = res;

    }, (error) => {
      console.info(error);

    });

    this.date = this.dcs.getToday()['fulldate'];

    this.reprot.getKaryaDeshReport1({ 'id': this.activeRoute.snapshot.params['id'], }).subscribe((res) => {
      if (res['error']) {
        this.callData();
      }
      if (res['karyaAdesh'] == null) {
        this.callData();
      } else {
        // console.log(res);
        this.callData();
        let a = JSON.parse(res['karyaAdesh']);
        // console.log(a);
        let sourceData = a['karyadesh']
        // console.log(sourceData);
        if (sourceData['conName'] === '') {
          this.type = true
        } else {
          this.type = false
        }
        this.foForm.controls['municipalityName'].patchValue(sourceData['municipalityName']);
        this.foForm.controls['activityName'].patchValue(sourceData['activityName']);
        this.foForm.controls['activityAddress'].patchValue(sourceData['activityAddress']);
        this.foForm.controls['currentDate'].patchValue(sourceData['currentDate']);
        this.foForm.controls['contractDate'].patchValue(sourceData['contractDate']);
        this.foForm.controls['contractNumber'].patchValue(sourceData['contractNumber']);
        sourceData['contractDay'] && this.foForm.controls['contractDays'].patchValue(sourceData['contractDay']);
        this.foForm.controls['plannedStartDate'].patchValue(sourceData['plannedStartDate']);
        this.foForm.controls['completionDate'].patchValue(sourceData['completionDate']);
        this.foForm.controls['prasasan'].patchValue(sourceData['prasasan']);
        this.foForm.controls['engineer1'].patchValue(sourceData['engineer1']);
        this.foForm.controls['engineer2'].patchValue(sourceData['engineer2']);
        this.foForm.controls['engineer3'].patchValue(sourceData['engineer3']);
        this.foForm.controls['engineer4'].patchValue(sourceData['engineer4']);
        this.foForm.controls['engineer5'].patchValue(sourceData['engineer5']);
        this.foForm.controls['engineer6'].patchValue(sourceData['engineer6']);
        this.foForm.controls['planY'].patchValue(sourceData['planY']);
        this.foForm.controls['wadaNo'].patchValue(sourceData['wadaNo']);
        this.foForm.controls['estimatedCost'].patchValue(sourceData['estimatedCost']);
        this.foForm.controls['techCost'].patchValue(sourceData['techCost']);
        this.foForm.controls['pCost'].patchValue(sourceData['pCost']);
        this.foForm.controls['ccName'].patchValue(sourceData['ccName']);
        this.foForm.controls['ccAddress'].patchValue(sourceData['ccAddress']);
        this.foForm.controls['conName'].patchValue(sourceData['conName']);
        this.foForm.controls['contactPerson'].patchValue(sourceData['contactPerson']);
        this.foForm.controls['conAddress'].patchValue(sourceData['conAddress']);
        this.foForm.controls['sthar'].patchValue(sourceData['sthar']);
        this.foForm.controls['focusAreaLevel1NameNepali'].patchValue(sourceData['focusAreaLevel1NameNepali']);
        // if(res['implementingAgencyType1']=== '1'){
        //   this.ifWard = true;
        // }
        this.foForm.controls['paarit'].patchValue(sourceData['paarit']);
        this.foForm.controls['padh'].patchValue(sourceData['padh']);
        // this.foForm.controls['address'].patchValue(sourceData['address']);
        // this.foForm.controls['date'].patchValue(sourceData['date']);
        this.foForm.controls['phone'].patchValue(sourceData['phone']);

      }
    }, (error) => {
      console.info(error);
    });


    // console.log(this.date);
    // this.foForm.controls['engineer1'].patchValue('रामाकान्त चाैधरी');
    // this.foForm.controls['prasasan'].patchValue('धिरेन्द्र कुमार यादव');
    // this.foForm.controls['currentDate'].patchValue(this.date);
  }

  callData() {
    // this.ads.show(this.activeRoute.snapshot.params['id']).subscribe(
    this.aas.index({ activity: this.activeRoute.snapshot.params['id'], form: 'karyadesh' }).subscribe(
      result => {
        this.activityDetailData = result;
        if (!this.foForm.controls['currentDate'].value) {
          this.foForm.controls['currentDate'].patchValue(this.activityDetailData['data']['workOrderDate']);
        }
        if (this.activityDetailData['ActivityDetails']['implementingAgencyLevel1'] == "1") {
          this.ifWard = true;
        }
        if (this.activityDetailData['CommitteDetails'].length > 0) {
          this.type = true;
          this.foForm.controls['type'].patchValue(true);
          this.foForm.controls['ccName'].patchValue(this.activityDetailData['PlanAgreement'][0].consumerCommitteName);
          this.foForm.controls['ccAddress'].patchValue(this.activityDetailData['PlanAgreement'][0].consumerCommitteAddress);

        }
        if (this.activityDetailData['contractDetails']) {
          this.type = false;
          this.foForm.controls['type'].patchValue(false);
          this.foForm.controls['contactPerson'].patchValue(this.activityDetailData['contractDetails'].contactPerson);
          this.foForm.controls['conName'].patchValue(this.activityDetailData['contractDetails'].name);
          this.foForm.controls['conAddress'].patchValue(this.activityDetailData['contractDetails'].address);
          this.foForm.controls['pCost'].patchValue(this.activityDetailData['contractDetails']['contractAmount']);
        }
      },
      error => {
        console.log(error);

      }
    )
    this.reprot.getKaryaDeshReport(this.activeRoute.snapshot.params['id'], {}).subscribe(
      (res) => {
        // console.log(res);
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.SearchDatas = [];
        } else {
          this.SearchDatas = res[0] || null;
          if (this.SearchDatas['wardNumber']) {
            this.foForm.controls['wadaNo'].patchValue(this.SearchDatas['wardNumber']);
            this.foForm.controls['sthar'].patchValue('वडा स्तरीय');
          } else {
            this.foForm.controls['wadaNo'].patchValue('नगर स्तरीय');
            this.foForm.controls['sthar'].patchValue('नगर स्तरीय');
          }
          if (this.orgMaster && this.orgMaster[0] && this.orgMaster[0]['orgType']) {
            this.foForm.controls['paarit'].patchValue(this.orgMaster[0]['orgType'] === 'RUL' ? 'गाउँसभा' : 'नगरसभा');
          }
          // this.foForm.controls['currentDate'].patchValue(this.date);

          this.foForm.controls['focusAreaLevel1NameNepali'].patchValue(this.SearchDatas['focusAreaLevel1NameNepali']);
          this.foForm.controls['activityName'].patchValue(this.SearchDatas['activityName']);
          this.foForm.controls['activityAddress'].patchValue(this.SearchDatas['activityAddress']);
          this.foForm.controls['planY'].patchValue(this.SearchDatas['planYear']);
          this.foForm.controls['estimatedCost'].patchValue(this.SearchDatas['estimatedCost']);
          this.foForm.controls['techCost'].patchValue(this.SearchDatas['techCost']);
          if (this.SearchDatas['total'] !== 0) {
            this.foForm.controls['pCost'].patchValue(this.SearchDatas['total']);
          }

          this.foForm.controls['contractDate'].patchValue(this.SearchDatas['contractDate'] || '');
          this.foForm.controls['plannedStartDate'].patchValue(this.SearchDatas['plannedStartDate'] || '');
          this.foForm.controls['completionDate'].patchValue(this.SearchDatas['completionDate'] || '');

          // this.foForm.controls['engineer1'].patchValue('१. श्री लेखा शाखा सिदिङ्वा गा.पा. कार्यालय:– जानकारीको  लागी ।');
          //   this.foForm.controls['date'].patchValue(this.date);

          // this.foForm.controls['engineer3'].patchValue('३. योजना शाखा सिदिङ्वा गा.पा.'+ this.heading['officeName']);
          // this.foForm.controls['engineer4'].patchValue('४. पुर्वाधार शाखा सिदिङ्वा गा.पा.');
          this.reprot.getOrg().subscribe((result) => {
            if (result && result.length == 1 && result[0].error) {
              this.heading = [];
            } else {
              this.heading = result[0];
              // console.info(this.heading);
              //   this.foForm.controls['address'].patchValue(this.heading['officeName']);
              this.foForm.controls['engineer1'].patchValue(`१. श्री आर्थिक प्रशासन शाखा ${this.heading && this.heading['officeName']} कार्यालय:– जानकारीको  लागी ।`);
              this.foForm.controls['engineer3'].patchValue(`२. योजना शाखा ${this.heading && this.heading['officeName']} ।`);
              this.foForm.controls['engineer4'].patchValue(`३. पुर्वाधार शाखा ${this.heading && this.heading['officeName']} ।`);
              if(this.distType === 'aathbiskot'){
                this.foForm.controls['engineer4'].patchValue(`३. पुर्वाधार विकास शाखा ${this.heading && this.heading['officeName']} ।`);
              }
              this.foForm.controls['engineer2'].patchValue(`४. श्री वडा कार्यालय ${this.SearchDatas['wardNumber']} ${this.heading && this.heading['officeName']} ।`);
              if (this.distType == 'bideha') {
                this.foForm.controls['engineer1'].patchValue(`१. श्री आर्थिक प्रशासन शाखा, ${this.heading && this.heading['officeName']}, ${this.heading && this.heading['district']} ।`);
                this.foForm.controls['engineer7'].patchValue(`२. श्री प्राविधिक शाखा, ${this.heading && this.heading['officeName']}, ${this.heading && this.heading['district']} ।`);
              }
              if (this.ifWard) {
                this.foForm.controls['engineer5'].patchValue(`५. इन्जिनियर श्री `);
                this.foForm.controls['engineer6'].patchValue(`६. साइट इन्चार्ज श्री `);
              }
              if (!this.ifWard) {
                this.foForm.controls['engineer5'].patchValue(`४. इन्जिनियर श्री `);
                this.foForm.controls['engineer6'].patchValue(`५. साइट इन्चार्ज श्री `);
              }
              this.foForm.controls['prasasan'].patchValue(this.heading['nameOfHead']);
              this.foForm.controls['padh'].patchValue(this.heading['post']);
              this.foForm.controls['phone'].patchValue(this.heading['headTelNo']);
            }

          }, (error) => {
            console.info(error);
          });
        }

      }, (error) => {
        console.info(error);
      });
  }

  close() {
    this.router.navigate(['/approval/administrative-approval']);
  }

  ngAfterViewInit() {
    this.unicode.initUnicode();
  }

  saveData(post) {
    let postData = {
      'karyadesh': post,
    };
    // console.log(postData);
    this.reprot.putKaryaDeshReport(this.activeRoute.snapshot.params['id'], postData).subscribe(
      result => {
        // console.log(result+"check")
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {

      }
    );
  }

  print(post) {
    const printContents = document.getElementById('printSection').innerHTML;
    this.printingService.printContents(printContents, 'कार्याधेश', false, `
    @page{
      /* this affects the margin in the printer settings */
      margin: 24.4mm 24.4mm 24.4mm 37.1mm;
    }
    @media print{
      p, div, th, td{
        font-size: 25px !important;
      }
    }    `);
    this.saveData(post);
  }

}
