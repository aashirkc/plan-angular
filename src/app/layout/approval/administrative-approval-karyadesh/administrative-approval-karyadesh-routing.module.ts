import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministrativeApprovalKaryadeshComponent } from './administrative-approval-karyadesh.component';

const routes: Routes = [
  {
    path: '',
    component: AdministrativeApprovalKaryadeshComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrativeApprovalKaryadeshRoutingModule { }
