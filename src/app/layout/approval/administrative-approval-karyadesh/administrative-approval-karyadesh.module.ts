import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministrativeApprovalKaryadeshRoutingModule } from './administrative-approval-karyadesh-routing.module';
import { AdministrativeApprovalKaryadeshComponent } from './administrative-approval-karyadesh.component';
import { SharedModule } from '../../../shared/modules/shared.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        AdministrativeApprovalKaryadeshRoutingModule
    ],
    declarations: [AdministrativeApprovalKaryadeshComponent]
})
export class AdministrativeApprovalKaryadeshModule {}
