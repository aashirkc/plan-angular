import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrativeApprovalViewComponent } from './administrative-approval-view.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AdministrativeApprovalViewRoutingModule } from './administrative-approval-view-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdministrativeApprovalViewRoutingModule
  ],
  declarations: [AdministrativeApprovalViewComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AdministrativeApprovalViewModule { }
