import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministrativeApprovalViewComponent } from './administrative-approval-view.component';

const routes: Routes = [
  {
    path: '',
    component: AdministrativeApprovalViewComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrativeApprovalViewRoutingModule { }
