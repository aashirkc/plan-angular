import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { AdministrativeApprovalService, DateConverterService, ActivityDetailService, AdministrativeApproval, ActivityOutputService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-administrative-approval-view',
  templateUrl: './administrative-approval-view.component.html',
  styleUrls: ['./administrative-approval-view.component.scss']
})
export class AdministrativeApprovalViewComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

  monitoringArrayData: any;
  showArray: boolean = false;
  approvalEntryForm: FormGroup;
  activityDetailId: string;
  activityDetailData: any;
  workServiceTypeArrayData: any;
  currentDate: any;
  showTable: boolean = false;
  contractTable: boolean = false;
  contractSelectedFiles: any = [];
  checkOtherGranted: boolean = false;
  showAmanantaTable: boolean = false;
  showMonitoringTable: boolean = false;
  technicalCost: string;
  administrativeDetail: any;
  selectedFiles: any = [];
  imagesLists: any = [];
  docUrl: any;
  committeeEdidData: any = [];
  grantedFromOthers: any = [];
  contractEdidData: any;
  contractDocData: any;
  workServiceTypeAdapter: any = [
    {
      name: 'Consumer Committe',
      id: 'उपभोक्ता समिति'
    },
    {
      name: 'Contract',
      id: 'ठेक्का मार्फत'
    },
    {
      name: 'Amanathan Marfat',
      id: 'अमानत मार्फत'
    }, { name: 'Sahakari', id: 'श्रम सहकारी / संस्था मार्फत' }
  ];
  monitoringAdapter: Array<any> = [
    {
      name: 'Yes',
      id: 'हो'
    },
    {
      name: 'No',
      id: 'होइन'
    }
  ];
  showApproveButton: boolean = false;
  showApprovalButton: boolean = false;
  isRul: boolean;
  enable_disableApproveFeature: boolean;
  monitoringCommitteConfig: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private ads: ActivityDetailService,
    private aas: AdministrativeApprovalService,
    private aos: ActivityOutputService,
    private ds: DateConverterService,
    @Inject('API_URL_DOC') docUrl: string,
    private location: Location,
    @Inject('ENABLE_ADMINISTRATIVE_APPROVAL_DISABLE_FEATURE')
    enable_disableApproveFeature: boolean
  ) {
    this.enable_disableApproveFeature =
      enable_disableApproveFeature || false;
    this.createForm();
    this.docUrl = docUrl;
  }

  ngOnInit() {
    let userDetalils = JSON.parse(localStorage.getItem('pcUser'));
    this.currentDate = this.ds.getToday().fulldate;
    this.isRul = false;
    const orgMaster = JSON.parse(localStorage.getItem('org'));
    if (orgMaster && orgMaster[0]) {
      const orgType = orgMaster[0]['orgType'];
      this.isRul = orgType === 'RUL';
    }
    // console.log(this.currentDate);
    if (userDetalils['userType'].toLowerCase() == 'admin') {
      this.showApproveButton = true;
    } else if (userDetalils['userType'].toLowerCase() == 'activity manager') {
      this.showApproveButton = true;
    } else if (userDetalils['userType'].toLowerCase() == 'technical manager') {
      this.showApproveButton = false;
    } else if (userDetalils['userType'].toLowerCase() == 'administrative manager') {
      this.showApproveButton = true;
    } else if (userDetalils['userType'].toLowerCase() == 'super admin') {
      this.showApproveButton = true;
    } else {
      this.showApproveButton = false;
    }
  }


  ngAfterViewInit() {
    this.activityDetailId = this.route.snapshot.paramMap.get('id');
    this.getActivityDetail(this.activityDetailId);
    let post = { activity: this.activityDetailId };

    this.aas.getTechnicalDetail(post).subscribe(
      result => {
        this.technicalCost = result && result[0] && result[0].techCost || null;
      },
      error => {
        console.log(error);
      }
    );
    this.getAdministrativeDetail();

  }

  getAdministrativeDetail() {
    this.approvalEntryForm.setControl('workServiceTypeArray', this.fb.array([]));
    this.approvalEntryForm.setControl('monitoringCommitteeArray', this.fb.array([]));
    this.aas.index({ activity: this.activityDetailId, form: '' }).subscribe(
      result => {

        let newData = result && result['data'] || {};

        setTimeout(() => {
          this.approvalEntryForm.controls['workServiceType'].patchValue(result['data']['workServiceType']);
        }, 100);
        newData['workServiceType'] = result && result['data'] && result['data'].workServiceType || '';
        newData['femalePopulation'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0].femalePopulation || '';
        newData['malePopulation'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0].malePopulation || '';
        newData['dalitPopulation'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0].dalitPopulation || '';
        newData['janajatiPopulation'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0].janajatiPopulation || '';
        newData['othersPopulation'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0].othersPopulation || '';
        newData['befinitedOrganizations'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].befinitedOrganizations || '';
        newData['benifitedHouses'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].benifitedHouses || '';
        newData['benifitedPopulation'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].benifitedPopulation || '';
        newData['consumerCommitteAddress'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].consumerCommitteAddress || '';
        newData['consumerCommitteName'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].consumerCommitteName || '';
        newData['grantedFromConsumerCommitte'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].grantedFromConsumerCommitte || '';
        newData['grantedFromIngo'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].grantedFromIngo || '';
        newData['grantedFromLocalLevel'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].grantedFromLocalLevel || '';
        newData['grantedFromNgo'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].grantedFromNgo || '';
        newData['grantedFromOthers'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].grantedFromOthers || '';
        newData['grantedFromState'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].grantedFromState || '';
        // newData['grantedFromUnion'] = result && result['PlanAgreement'] && result['PlanAgreement'].grantedFromUnion || '';
        newData['othersBenifited'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].othersBenifited || '';
        newData['formationDate'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].formationDate || '';
        newData['totalBenefited'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].totalBenefited || '';

        newData['totalParticipant'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].totalParticipant || '';
        newData['bankName'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].bankName || '';
        newData['bankAddress'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].bankAddress || '';
        newData['acNo'] = result && result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'].acNo || '';
        this.imagesLists = result && result['image'] || [];
        this.administrativeDetail = newData;
        this.committeeEdidData = result['CommitteDetails'];
        this.contractEdidData = result['contractDetails'] || '';
        this.grantedFromOthers = result['otherGranted'];
        if (this.grantedFromOthers.length > 0) {
          this.showArray = true;
          this.checkOtherGranted = true;
        }
        if (newData['workServiceType'] === 'Consumer Committe' || newData['workServiceType'] === 'Sahakari') {
          this.showTable = true;
          this.contractTable = false;
          this.showAmanantaTable = false;
          this.showMonitoringTable = true;
          for (let i = 0; i < this.committeeEdidData.length; i++) {
            this.addMoreWorkServiceType();
            // for (const config of this.monitoringCommitteConfig) {
            this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].patchValue(this.committeeEdidData[i]);
            // }
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('memberName').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].memberName);
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('id').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].id);
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('memberPost').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].memberPost);
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('committeAddress').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].committeAddress);
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('citizenshipNumber').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].citizenshipNumber);
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('issueDistrict').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].issueDistrict);
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('issueDate').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].issueDate);
            // this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].get('phoneNumber').patchValue(this.committeeEdidData[i] && this.committeeEdidData[i].phoneNumber);

          }
          this.approvalEntryForm.setControl('othersDonators', this.fb.array([]));
          for (let i = 0; i < this.grantedFromOthers.length; i++) {
            this.addItem();
            this.approvalEntryForm.get('othersDonators')['controls'][i].get('name').patchValue(this.grantedFromOthers[i] && this.grantedFromOthers[i].grantedName);
            this.approvalEntryForm.get('othersDonators')['controls'][i].get('id').patchValue(this.grantedFromOthers[i] && this.grantedFromOthers[i].id);
            this.approvalEntryForm.get('othersDonators')['controls'][i].get('value').patchValue(this.grantedFromOthers[i] && this.grantedFromOthers[i].value);
          }
        }
        if (newData['workServiceType'] === 'Contract') {
          this.showTable = false;
          this.contractTable = true;
          this.showAmanantaTable = false;
          this.contractDocData = result['contractDetailsDoc'];
          this.approvalEntryForm.setControl('contractData', this.fb.array([]));
          for (let i = 0; i < this.contractEdidData.length; i++) {
            this.addItem1();
            this.approvalEntryForm.get('contractData')['controls'][i].get('name').patchValue(this.contractEdidData[i] && this.contractEdidData[i].name);
            this.approvalEntryForm.get('contractData')['controls'][i].get('address').patchValue(this.contractEdidData[i] && this.contractEdidData[i].address);
            this.approvalEntryForm.get('contractData')['controls'][i].get('contactPerson').patchValue(this.contractEdidData[i] && this.contractEdidData[i].contactPerson);
            this.approvalEntryForm.get('contractData')['controls'][i].get('contactNumber').patchValue(this.contractEdidData[i] && this.contractEdidData[i].contactNumber);
            this.approvalEntryForm.get('contractData')['controls'][i].get('contractAmount').patchValue(this.contractEdidData[i] && this.contractEdidData[i].contractAmount);
            this.approvalEntryForm.get('contractData')['controls'][i].get('contractDate').patchValue(this.contractEdidData[i] && this.contractEdidData[i].contractDate);
            this.approvalEntryForm.get('contractData')['controls'][i].get('selected').patchValue(this.contractEdidData[i] && this.contractEdidData[i].selected);
          }
          // newData['name'] = this.contractEdidData && this.contractEdidData['name'];
          // newData['address'] = this.contractEdidData && this.contractEdidData['address'];
          // newData['contactPerson'] = this.contractEdidData && this.contractEdidData['contactPerson'];
          // newData['contactNumber'] = this.contractEdidData && this.contractEdidData['contactNumber'];
          // newData['contractDate'] = this.contractEdidData && this.contractEdidData['contractDate'];
          // newData['contractAmount'] = this.contractEdidData && this.contractEdidData['contractAmount'];
          newData['contractBankAcNo'] = this.contractEdidData && this.contractEdidData['bankAcNo'];
          newData['contractBankName'] = this.contractEdidData && this.contractEdidData['bankName'];
          newData['contractBankAcHolder'] = this.contractEdidData && this.contractEdidData['bankAcHolder'];
        }
        if (newData['workServiceType'] === 'Amanathan Marfat') {
          this.showTable = false;
          this.contractTable = false;
          this.showAmanantaTable = true;
        }
        this.administrativeDetail = newData;
        let formData = new AdministrativeApproval(this.administrativeDetail);
        this.jqxLoader.open();
        setTimeout(() => {
          this.approvalEntryForm.patchValue(formData);
          // console.log(this.approvalEntryForm);
          this.jqxLoader.close();
        }, 300);


        setTimeout(() => {
          this.approvalEntryForm.get('orderName').patchValue(result['data'] && result['data']['orderName'] || '');
          this.approvalEntryForm.get('officeName').patchValue(result['data'] && result['data']['officeName'] || '');
          this.approvalEntryForm.get('wadaRepresentative').patchValue(result['data'] && result['data']['wadaRepresentative'] || '');
          this.approvalEntryForm.get('acNo').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['acNo'] || '');
          this.approvalEntryForm.get('bankName').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['bankName'] || ''); //= result['PlanAgreement'][0]['bankName']
          this.approvalEntryForm.get('bankAddress').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['bankAddress'] || '');
          this.approvalEntryForm.get('befinitedOrganizations').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['befinitedOrganizations'] || '');
          this.approvalEntryForm.get('benifitedHouses').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['benifitedHouses'] || '');
          this.approvalEntryForm.get('benifitedPopulation').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['benifitedPopulation'] || '');
          this.approvalEntryForm.get('consumerCommitteAddress').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['consumerCommitteAddress'] || '');
          this.approvalEntryForm.get('consumerCommitteName').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['consumerCommitteName'] || '');
          this.approvalEntryForm.get('formationDate').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['formationDate'] || '');
          this.approvalEntryForm.get('totalBenefited').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['totalBenefited'] || '');

          this.approvalEntryForm.get('totalParticipant').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['totalParticipant'] || '');
          this.approvalEntryForm.get('grantedFromConsumerCommitte').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['grantedFromConsumerCommitte'] || '');
          this.approvalEntryForm.get('grantedFromIngo').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['grantedFromIngo'] || '');
          this.approvalEntryForm.get('grantedFromLocalLevel').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['grantedFromLocalLevel'] || '');
          this.approvalEntryForm.get('grantedFromNgo').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['grantedFromNgo'] || '');
          this.approvalEntryForm.get('grantedFromOthers').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['grantedFromOthers'] || '');
          this.approvalEntryForm.get('grantedFromState').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['grantedFromState'] || '');
          // this.approvalEntryForm.get('grantedFromUnion').patchValue(result['PlanAgreement'][0]['grantedFromUnion']);
          this.approvalEntryForm.get('othersBenifited').patchValue(result['PlanAgreement'] && result['PlanAgreement'][0] && result['PlanAgreement'][0]['othersBenifited'] || '');
        }, 300);
        if (result['monitoringCommittee'].length > 0) {
          setTimeout(() => {
            this.approvalEntryForm.controls['monitoring'].setValue('Yes');
          }, 100);
          for (let i = 0; i < result['monitoringCommittee'].length; i++) {
            this.addMonitoringCommittee();
            this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteeAddress').patchValue(result['monitoringCommittee'][i] && result['monitoringCommittee'][i].address);
            this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteeName').patchValue(result['monitoringCommittee'][i] && result['monitoringCommittee'][i].name);
            this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteePost').patchValue(result['monitoringCommittee'][i] && result['monitoringCommittee'][i].post);
            this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteeCitizenshipNo').patchValue(result['monitoringCommittee'][i] && result['monitoringCommittee'][i].citizenshipNo);
            // this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteeFatherName').patchValue(result['monitoringCommittee'][i] && result['monitoringCommittee'][i].fatherName);
            this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteeMobile').patchValue(result['monitoringCommittee'][i] && result['monitoringCommittee'][i].mobile);
          }
        } else {
          setTimeout(() => {
            this.approvalEntryForm.controls['monitoring'].setValue('No');
          }, 100);
          this.showMonitoringTable = false;
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  /**
 * Create the form group
 * with given form control name
 */

  createForm() {
    this.approvalEntryForm = this.fb.group({
      // 'prasasanikSakha': [{ value: '', disabled: true }, Validators.required],
      'workOrderDate': [{ value: '', disabled: true }],
      'issuingAuthority': [{ value: '', disabled: true }, Validators.required],
      'workServiceType': [{ value: '', disabled: true }, Validators.required],
      'wadaRepresentative': [{ value: '', disabled: true }],
      'name': [{ value: '', disabled: true }],
      'address': [{ value: '', disabled: true }],
      'contactPerson': [{ value: '', disabled: true }],
      'contactNumber': [{ value: '', disabled: true }],
      'contractDate': [{ value: '', disabled: true }],
      'attatchment': [''],
      'consumerCommitteName': [{ value: '', disabled: true }],
      'consumerCommitteAddress': [{ value: '', disabled: true }],
      'formationDate': [{ value: '', disabled: true }],
      'totalBenefited': [{ value: '', disabled: true }],
      'totalParticipant': [{ value: '', disabled: true }],
      'bankName': [{ value: '', disabled: true }],
      'bankAddress': [{ value: '', disabled: true }],
      'acNo': [{ value: '', disabled: true }],
      'contractBankAcNo': [{ value: '', disabled: true }],
      'contractBankName': [{ value: '', disabled: true }],
      'contractBankAcHolder': [{ value: '', disabled: true }],
      'contractAmount': [{ value: '', disabled: true }],
      //'grantedFromUnion': [''],
      'grantedFromState': [{ value: '', disabled: true }],
      'grantedFromLocalLevel': [{ value: '', disabled: true }],
      'grantedFromNgo': [{ value: '', disabled: true }],
      'grantedFromIngo': [{ value: '', disabled: true }],
      'grantedFromConsumerCommitte': [{ value: '', disabled: true }],
      'grantedFromOthers': [{ value: '', disabled: true }],
      'benifitedHouses': [{ value: '', disabled: true }],
      'benifitedPopulation': [{ value: '', disabled: true }],
      'befinitedOrganizations': [{ value: '', disabled: true }],
      'othersBenifited': [{ value: '', disabled: true }],
      'estimatedWorkCompletionDate': [{ value: '', disabled: true }, Validators.required],
      'agreementDate': [{ value: '', disabled: true }, Validators.required],
      'malePopulation': [{ value: '', disabled: true },],
      'femalePopulation': [{ value: '', disabled: true },],
      'dalitPopulation': [{ value: '', disabled: true },],
      'janajatiPopulation': [{ value: '', disabled: true },],
      'othersPopulation': [{ value: '', disabled: true },],
      'officeName': [{ value: '', disabled: true }],
      'orderName': [{ value: '', disabled: true }],
      'monitoring': [{ value: '', disabled: true }],

      workServiceTypeArray: this.fb.array([
        this.workServiceTypeForm()
      ]),
      'othersDonators': this.fb.array([
        this.initOtherItems(),
      ]),
      'contractData': this.fb.array([
        this.initContractForm(),
      ]),
      monitoringCommitteeArray: this.fb.array([
        this.workMonitoringForm()
      ]),
    });
  }
  private workMonitoringForm() {
    return this.fb.group({
      monitoringCommitteeAddress: [{ value: '', disabled: false },],
      monitoringCommitteeName: [{ value: '', disabled: false },],
      monitoringCommitteePost: [{ value: '', disabled: false },],
      monitoringCommitteeCitizenshipNo: [{ value: '', disabled: false },],
      monitoringCommitteeFatherName: [{ value: '', disabled: false },],
      monitoringCommitteeMobile: [{ value: '', disabled: false },]
    });
  }
  initOtherItems() {
    return this.fb.group({
      name: [{ value: '', disabled: true }, Validators.required],
      value: [{ value: '', disabled: true }, Validators.required],
      id: [{ value: '', disabled: true }]
    });
  }
  initContractForm() {
    return this.fb.group({
      name: [{ value: '', disabled: true }],
      address: [{ value: '', disabled: true }],
      contactPerson: [{ value: '', disabled: true }],
      contactNumber: [{ value: '', disabled: true }],
      contractDate: [{ value: '', disabled: true }],
      contractAmount: [{ value: '', disabled: true }],
      attatchment: [{ value: '', disabled: true }],
      selected: [{ value: '', disabled: true }],
    });
  }
  addMonitoringCommittee() {
    this.monitoringArrayData = this.approvalEntryForm.controls[
      "monitoringCommitteeArray"
    ];

    this.monitoringArrayData.push(
      this.workMonitoringForm()
    );
  }
  addItem() {
    const control1 = <FormArray>this.approvalEntryForm.controls['othersDonators'];
    control1.push(this.initOtherItems());
    // add unicode support for dynamically added input
  }

  addItem1() {
    const control1 = <FormArray>this.approvalEntryForm.controls['contractData'];
    control1.push(this.initContractForm());
    // add unicode support for dynamically added input
  }
  removeItem(i: number) {
    const control1 = <FormArray>this.approvalEntryForm.controls['othersDonators'];
    control1.removeAt(i);
  }
  private workServiceTypeForm() {
    if (window['wpms_config']['monitoringCommitteeDescription']) {
      this.monitoringCommitteConfig = window['wpms_config']['monitoringCommitteeDescription'];
      const config = {};
      for (const data of window['wpms_config']['monitoringCommitteeDescription']) {
        config[data.name] = null
      }
      const group = this.fb.group(config);
      return group;
    }
    return this.fb.group({
      committeAddress: [''],
      committeId: ['',],
      administrativeId: ['',],
      memberName: [''],
      memberPost: [{ value: '', disabled: true },],
      citizenshipNumber: [''],
      scanCopy: [''],
      issueDistrict: [null],
      issueDate: [''],
      phoneNumber: ['']
    });
  }

  addMoreWorkServiceType() {
    this.workServiceTypeArrayData = this.approvalEntryForm.controls[
      "workServiceTypeArray"
    ];
    this.workServiceTypeArrayData.push(
      this.workServiceTypeForm()
    );
  }

  removeForm(i) {
    this.workServiceTypeArrayData.removeAt(i);
  }

  checkWorkService(event) {
    if (event.target.value == 'Consumer Committe' || event.target.value == 'Sahakari') {
      this.showTable = true;
      this.contractTable = false;
      this.showMonitoringTable = true;
    } else {
      this.showTable = false;
      this.contractTable = true;
      this.showMonitoringTable = false;
    }
  }
  getActivityDetail(id) {
    this.jqxLoader.open();
    this.ads.show(id).subscribe(
      result => {
        // debugger;
        this.activityDetailData = result;
        this.jqxLoader.close();
        if (result['a_ApprovedBy'] > 0) { // approved
          this.showApprovalButton = true; // nadekhauni
        } else if (result['a_ApprovedBy'] == -1) { // rejected
          this.showApprovalButton = true; // nadekhauni
        } else { // edited
          this.showApprovalButton = false; // dekhauni
        }
      },
      error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }
  close() {
    // this.router.navigate(['/approval/administrative-approval']);
    this.location.back();
  }

  reject() {
    const userId = JSON.parse(localStorage.getItem('pcUser'))['userId'];
    this.aos.approveAdministrative(userId, this.activityDetailId, "-1").subscribe(
      result => {
        if (result["message"]) {
          this.jqxLoader.close();
          this.showMessage(result['message']);
          this.showApprovalButton = true;
        }
        if (result["error"]) {
          this.jqxLoader.close();
          this.showError(result['error']['message']);
        }
      },
      error => {
        this.jqxLoader.close();
        let messageDiv: any = document.getElementById("error");
        messageDiv.innerText = error["message"];
        this.errNotification.open();
      }
    );
  }

  showMessage(message: string) {
    let messageDiv: any = document.getElementById("message");
    messageDiv.innerText = message;
    this.msgNotification.open();
  }

  showError(message: string) {
    let messageDiv: any = document.getElementById("error");
    messageDiv.innerText = message;
    this.errNotification.open();
  }

  approve() {
    const userId = JSON.parse(localStorage.getItem('pcUser'))['userId'];
    this.aos.approveAdministrative(userId, this.activityDetailId).subscribe(
      result => {
        if (result["message"]) {
          this.jqxLoader.close();
          let messageDiv: any = document.getElementById("message");
          messageDiv.innerText = result["message"];
          this.msgNotification.open();
          this.showApprovalButton = true;
        }
        if (result["error"]) {
          this.jqxLoader.close();
          let messageDiv: any = document.getElementById("error");
          messageDiv.innerText = result["error"]["message"];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        let messageDiv: any = document.getElementById("error");
        messageDiv.innerText = error["message"];
        this.errNotification.open();
      }
    );
    let post = {
      activity: this.activityDetailId,
      amount: "",
      entryDate: this.currentDate,
      id: "",
      karmagat: "No",
      nextPlanYear: "",
      reason: "",
      status: "Ongoing",
    }
    this.aos.storeExecutionStatus(post).subscribe((result) => {
      if (result['message']) {
        let messageDiv: any = document.getElementById('sus');
        messageDiv.innerText = result['message'];
      }
    },
      error => {
        this.jqxLoader.close();
        console.info(error);
      });
  }

}
