import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministrativeApprovalEntryComponent } from './administrative-approval-entry.component';

const routes: Routes = [
  {
    path: '',
    component: AdministrativeApprovalEntryComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrativeApprovalEntryRoutingModule { }
