import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { ToggleSidebarService, AdministrativeApprovalService, AllReportService, ActivityOutputService, ActivityDetailService, DateConverterService, CustomValidators, EstimateDateConverterService, SetupsService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-administrative-approval-entry',
  templateUrl: './administrative-approval-entry.component.html',
  styleUrls: ['./administrative-approval-entry.component.scss']
})
export class AdministrativeApprovalEntryComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  contractStatus: boolean = false;
  approvalEntryForm: FormGroup;
  activityDetailId: string;
  activityDetailData: any;
  workServiceTypeArrayData: any;
  monitoringArrayData: any;
  showTable: boolean = false;
  showTableContract: boolean = false;
  showArray: boolean = false;
  showAmanantaTable: boolean = false;
  showMonitoringTable: boolean = false;
  fullDate: string;
  OrgType: any;
  checkMUn: boolean;
  userData: any = {};
  workServiceTypeAdapter: any = [
    {
      name: 'Consumer Committe',
      id: 'उपभोक्ता समिति'
    },
    {
      name: 'Contract',
      id: 'ठेक्का मार्फत'
    },
    {
      name: 'Amanathan Marfat',
      id: 'अमानत मार्फत'
    }, { name: 'Sahakari', id: 'श्रम सहकारी / संस्था मार्फत' }
  ];
  monitoringAdapter: Array<any> = [
    {
      name: 'Yes',
      id: 'हो'
    },
    {
      name: 'No',
      id: 'होइन'
    }
  ];
  district: any = [
    { name: 'भोजपुर' },
    { name: 'धनकुटा' },
    { name: 'इलाम' },
    { name: 'झापा' },
    { name: 'खोटाँग' },
    { name: 'मोरंग' },
    { name: 'ओखलढुंगा' },
    { name: 'पांचथर' },
    { name: 'संखुवासभा' },
    { name: 'सोलुखुम्बू' },
    { name: 'सुनसरी' },
    { name: 'ताप्लेजुंग' },
    { name: 'तेह्रथुम' },
    { name: 'उदयपुर' },
    { name: 'सप्तरी' },
    { name: 'सिराहा' },
    { name: 'धनुषा' },
    { name: 'महोत्तरी' },
    { name: 'सर्लाही' },
    { name: 'बारा' },
    { name: 'पर्सा' },
    { name: 'रौतहट' },
    { name: 'सिन्धुली' },
    { name: 'रामेछाप' },
    { name: 'दोलखा' },
    { name: 'भक्तपुर' },
    { name: 'धादिङ' },
    { name: 'काठमाडौँ' },
    { name: 'ललितपुर' },
    { name: 'नुवाकोट' },
    { name: 'सिन्धुपाल्चोक' },
    { name: 'रसुवा' },
    { name: 'काभ्रेपलान्चोक' },
    { name: 'चितवन' },
    { name: 'मकवानपुर' },
    { name: 'गोरखा' },
    { name: 'कास्की' },
    { name: 'लमजुङ' },
    { name: 'स्याङग्जा' },
    { name: 'तनहुँ' },
    { name: 'मनाङ' },
    { name: 'नवलपुर' },
    { name: 'बागलुङ' },
    { name: 'म्याग्दी' },
    { name: 'पर्वत' },
    { name: 'मुस्ताङ' },
    { name: 'कपिलवस्तु' },
    { name: 'परासी' },
    { name: 'रुपन्देही' },
    { name: 'अर्घाखाँची' },
    { name: 'गुल्मी' },
    { name: 'पाल्पा' },
    { name: 'दाङ' },
    { name: 'प्युठान' },
    { name: 'रोल्पा' },
    { name: 'पूर्वी रूकुम' },
    { name: 'बाँके' },
    { name: 'बर्दिया' },
    { name: 'पश्चिमी रूकुम' },
    { name: 'सल्यान' },
    { name: 'डोल्पा' },
    { name: 'हुम्ला' },
    { name: 'जुम्ला' },
    { name: 'कालिकोट' },
    { name: 'मुगु' },
    { name: 'सुर्खेत' },
    { name: 'दैलेख' },
    { name: 'जाजरकोट' },
    { name: 'कैलाली' },
    { name: 'अछाम' },
    { name: 'डोटी' },
    { name: 'बझाङ' },
    { name: 'बाजुरा' },
    { name: 'कंचनपुर' },
    { name: 'डडेलधुरा' },
    { name: 'बैतडी' },
    { name: 'दार्चुला' },


  ];
  technicalCost: any;

  selectedFiles: any = [];
  contractSelectedFiles: any = [];
  showButton: boolean = false;
  heading: any[];
  totalDurationDays: any;
  totalDurationMonth: any;
  techCost: any;
  activityDatas: any = {};
  checking: boolean = true;
  consumerCommittees: Array<any> = [];
  committeDataDetails: any;
  monitoringCommitteConfig: any;

  constructor(
    private ts: ToggleSidebarService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private ads: ActivityDetailService,
    private aas: AdministrativeApprovalService,
    private setupService: SetupsService,
    private dcs: DateConverterService,
    private report: AllReportService,
    private location: Location,
    private aos: ActivityOutputService,
    private cdr: ChangeDetectorRef,
    private eds: EstimateDateConverterService,
    private cd: ChangeDetectorRef
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.ts.currentMessage.subscribe((data) => {
      if (data == "on") {

        this.checking = false;
      }
      if (data == "off") {
        this.checking = true;
      }

    });
    let currentYear = new Date().getFullYear();
    let month = new Date().getMonth() + 1;
    let day = new Date().getDate();
    let engDate = currentYear + '-' + month + '-' + day;
    let date = this.dcs.ad2Bs(engDate);
    this.fullDate = date['fulldate'];
    this.approvalEntryForm.controls['workOrderDate'].patchValue(this.fullDate);
    this.approvalEntryForm.controls['agreementDate'].patchValue(this.fullDate);
    this.approvalEntryForm.controls['workOrderDate'].updateValueAndValidity();
    this.approvalEntryForm.controls['agreementDate'].updateValueAndValidity();
    let userDetalils = JSON.parse(localStorage.getItem('pcUser'));
    this.userData = userDetalils['userType'];

    let orgMaster = JSON.parse(localStorage.getItem('org'));
    if (orgMaster && orgMaster[0]) {
      const orgType = orgMaster[0]['orgType'];
      this.checkMUn = orgType === 'RUL';
    }

    this.report.getOrg().subscribe((result) => {
      if (result.length == 1 && result[0].error) {
        this.heading = [];
      } else {
        this.heading = result[0];
        // this.OrgType = this.heading['orgType'];
        // if (this.OrgType == "गाउँ कार्यपालिकाको कार्यालय") {
        //   this.checkMUn = true;
        // } else {
        //   this.checkMUn = false;
        // }
        this.approvalEntryForm.controls['issuingAuthority'].patchValue(this.heading['officeName'] || '');
      }

    }, (error) => {
      console.info(error);
    });

    this.setupService.consumerCommitteeDetailsGet().subscribe(response => {
      this.consumerCommittees = response;
    });

  }

  ngAfterViewInit() {

    this.activityDetailId = this.route.snapshot.paramMap.get('id');
    this.getActivityDetail(this.activityDetailId);
    let post = { activity: this.activityDetailId };
    this.aas.getTechCost(this.activityDetailId).subscribe(
      result => {
        if (result['data'] && result['data']['techCost']) {
          this.techCost = result['data'] && result['data']['techCost'];
        }
      },
      error => {
        // console.log(error);
      });
    this.aas.getTechnicalCostDetail(this.activityDetailId).subscribe(
      result => {
        this.technicalCost = result || null;
      },
      error => {
        console.log(error);
      });
    this.approvalEntryForm.setControl('workServiceTypeArray', this.fb.array([]));
    this.approvalEntryForm.setControl('monitoringCommitteeArray', this.fb.array([]));
    this.cdr.detectChanges();
  }

  /**
 * Create the form group
 * with given form control name
 */
  createForm() {
    this.approvalEntryForm = this.fb.group({

      'workOrderDate': ['',],
      'wadaRepresentative': ['',],
      // 'name': [''],
      // 'address': [''],
      // 'contactPerson': [''],
      // 'contactNumber': [''],
      // 'contractDate': [''],
      // 'contractAmount': [0],
      // 'attatchment': [''],
      'consumerCommitteName': [''],
      'consumerCommitteAddress': [''],
      'consumerCommitteAddressId': [''],
      'formationDate': [''],
      'totalBenefited': [0],
      'totalParticipant': [0],
      'bankName': [''],
      'bankAddress': [''],
      'acNo': [''],
      'monitoring': [''],
      'contractBankAcNo': [''],
      'contractBankName': [''],
      'contractBankAcHolder': [''],
      'grantedFromState': [0],
      'grantedFromLocalLevel': [0],
      'grantedFromNgo': [0],
      'grantedFromIngo': [0],
      'grantedFromConsumerCommitte': [0],
      'grantedFromOthers': [0],
      'benifitedHouses': [0],
      'benifitedPopulation': [0],
      'befinitedOrganizations': [0],
      'othersBenifited': [0],
      'agreementDate': [''],
      'malePopulation': [0],
      'femalePopulation': [0],
      'dalitPopulation': [0],
      'janajatiPopulation': [0],
      'othersPopulation': [0],
      'officeName': [''],
      'orderName': [''],
      'issuingAuthority': ['', Validators.required],
      'workServiceType': ['', Validators.required],
      'estimatedWorkCompletionDate': [null, Validators.compose([Validators.required, CustomValidators.checkDate])],
      workServiceTypeArray: this.fb.array([
        this.workServiceTypeForm()
      ]),
      monitoringCommitteeArray: this.fb.array([
        this.workMonitoringForm()
      ]),
      'othersDonators': this.fb.array([
        this.initOtherItems(),
      ]),
      'contractData': this.fb.array([
        // this.initContractForm(),
      ]),
    });
  }

  get consumerCommitteAddressId() { return this.approvalEntryForm.get('consumerCommitteAddressId'); }

  committeeChangeName(id) {
    let tabledata = this.consumerCommittees.filter(x => x.id == id);
    this.committeDataDetails = tabledata && tabledata[0];
    let dt = {
      consumerCommitteAddress: this.committeDataDetails && this.committeDataDetails['address'] || '',
      bankName: this.committeDataDetails && this.committeDataDetails['bankName'] || '',
      acNo: this.committeDataDetails && this.committeDataDetails['bankAccountNumber'] || '',
      formationDate: this.committeDataDetails && this.committeDataDetails['createdDate'] || '',
      karyalaya: this.committeDataDetails && this.committeDataDetails['karyalaya'] || '',
      grantedFromLocalLevel: this.committeDataDetails && this.committeDataDetails['nagrpalika'] || '',
      consumerCommitteName: this.committeDataDetails && this.committeDataDetails['name'] || '',
      grantedFromState: this.committeDataDetails && this.committeDataDetails['nepalSarkar'] || '',
      grantedFromNgo: this.committeDataDetails && this.committeDataDetails['pradeshSarkar'] || '',
      totalBenefited: this.committeDataDetails && this.committeDataDetails['profitPopulation'] || '',
      totalParticipant: this.committeDataDetails && this.committeDataDetails['totalPopulation'] || '',
      user: this.committeDataDetails && this.committeDataDetails['user'] || '',
      grantedFromIngo: this.committeDataDetails && this.committeDataDetails['ward'] || '',
      bankAddress: this.committeDataDetails && this.committeDataDetails['bankAddress'] || '',
      grantedFromConsumerCommitte: this.committeDataDetails && this.committeDataDetails['consumerCommitee'] || '',
      monitoring: this.committeDataDetails && this.committeDataDetails['monitoring'] || '',
      monitoringCommitteeArray: [],
      workServiceTypeArray: [],
    };
    // this.addDetails();
    this.approvalEntryForm.patchValue(dt);

    let arrayData1 = this.committeDataDetails && this.committeDataDetails['commiteeMember'] || [];
    let arrayData2 = this.committeDataDetails && this.committeDataDetails['anugamanCommitee'] || [];
    if (arrayData2.length > 0) {
      this.showMonitoringTable = true;
    } else {
      this.showMonitoringTable = false;
    }
    this.approvalEntryForm.setControl('monitoringCommitteeArray', this.fb.array([]));
    this.approvalEntryForm.setControl('workServiceTypeArray', this.fb.array([]));

    for (let i = 0; i < arrayData1.length; i++) {
      this.addMoreWorkServiceType();
      this.setEditData(i, arrayData1[i]);
    }

    if (arrayData2.length > 0) {
      this.approvalEntryForm.controls['monitoring'].setValue('Yes');
    }

    for (let i = 0; i < arrayData2.length; i++) {
      this.addMonitoringCommittee();
      this.setEditAnugamanData(i, arrayData2[i]);
    }


  }

  setEditAnugamanData(index, data) {
    this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][index].get('monitoringCommitteeAddress').patchValue(data['monitoringCommitteeAddress']);
    this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][index].get('monitoringCommitteeCitizenshipNo').patchValue(data['monitoringCommitteeCitizenshipNo']);
    this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][index].get('monitoringCommitteePost').patchValue(data['monitoringCommitteePost'] || '');
    this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][index].get('monitoringCommitteeMobile').patchValue(data['monitoringCommitteeMobile']);
    this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][index].get('monitoringCommitteeName').patchValue(data['monitoringCommitteeName']);

  }

  setEditData(index, data) {
    let MemberPost = [
      { name: 'अध्यक्ष' },
      { name: 'उपाध्यक्ष' },
      { name: 'कोषाध्यक्ष' },
      { name: 'सचिव' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
    ];

    this.approvalEntryForm.get('workServiceTypeArray')['controls'][index].get('committeAddress').patchValue(data['committeAddress']);
    this.approvalEntryForm.get('workServiceTypeArray')['controls'][index].get('committeId').patchValue(data['committeId']);
    this.approvalEntryForm.get('workServiceTypeArray')['controls'][index].get('administrativeId').patchValue(data['administrativeId']);
    this.approvalEntryForm.get('workServiceTypeArray')['controls'][index].get('memberName').patchValue(data['memberName']);
    this.approvalEntryForm.get('workServiceTypeArray')['controls'][index].get('memberPost').patchValue(MemberPost[index].name);
    this.approvalEntryForm.get('workServiceTypeArray')['controls'][index].get('citizenshipNumber').patchValue(data['citizenshipNumber']);
    this.approvalEntryForm.get('workServiceTypeArray')['controls'][index].get('scanCopy').patchValue(data['scanCopy']);
    this.approvalEntryForm.get('workServiceTypeArray')['controls'][index].get('issueDistrict').patchValue(data['issueDistrict']);
    this.approvalEntryForm.get('workServiceTypeArray')['controls'][index].get('issueDate').patchValue(data['issueDate']);
    this.approvalEntryForm.get('workServiceTypeArray')['controls'][index].get('phoneNumber').patchValue(data['phoneNumber']);
    this.approvalEntryForm.updateValueAndValidity();
  }

  setdata1() {

    // set default consumer comitte name and location
    this.approvalEntryForm.patchValue({
      'consumerCommitteName': this.activityDetailData['activityName'] || '',
      'consumerCommitteAddress': this.activityDetailData['address'] || ''
    });

    let MemberPost = [
      { name: 'अध्यक्ष', required: true },
      { name: 'उपाध्यक्ष' },
      { name: 'कोषाध्यक्ष', required: true },
      { name: 'सचिव', required: true },
      { name: 'सदस्य', required: true },
      { name: 'सदस्य', required: true },
      { name: 'सदस्य', required: true },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
      { name: 'सदस्य' },
    ];
    this.approvalEntryForm.get('workServiceTypeArray').reset();
    let add = true;
    if (this.approvalEntryForm.get('workServiceTypeArray')['controls'].length > 0) {
      add = false;
    }
    for (let i = 0; i < MemberPost.length; i++) {
      if (add) {
        this.addMoreWorkServiceType();
      }
      this.approvalEntryForm.get('workServiceTypeArray')['controls'][i].patchValue({
        'memberPost': MemberPost[i].name,
        'committeAddress': this.activityDetailData['address']
      });
      if (MemberPost[i].required) {
        this.approvalEntryForm.get('workServiceTypeArray')['controls'][i]['controls']['memberName'].setValidators([
          Validators.required
        ]);
      }
    }
  }

  populationChange($event, type) {
    const totalPopulation = this.approvalEntryForm.get('benifitedPopulation').value;
    const targetPopulation = this.approvalEntryForm.get(`${type}Population`);
    const targetValue = totalPopulation - $event.target.value;
    targetPopulation.patchValue(targetValue);
  }

  consumerCommitteeAddressChange($event) {
    console.log($event);
    for (let wsControls of this.approvalEntryForm.get('workServiceTypeArray')['controls']) {
      wsControls.patchValue({
        'committeAddress': $event.target.value
      });
    }
    for (let wsControls of this.approvalEntryForm.get('monitoringCommitteeArray')['controls']) {
      wsControls.patchValue({
        'monitoringCommitteeAddress': $event.target.value
      });
    }
  }

  initOtherItems() {
    return this.fb.group({
      name: [''],
      value: ['',]
    });
  }

  initContractForm() {
    let selected = true;
    try {
      selected = this.approvalEntryForm.get('contractData')['controls'].length === 0;
    } catch (e) { }
    return this.fb.group({
      docId: [''],
      name: [''],
      address: [''],
      contactPerson: [''],
      contactNumber: [''],
      contractDate: [''],
      contractAmount: [''],
      attatchment: [''],
      selected: [selected ? '1' : '0'],
      attachmentData: null
    });
  }

  addDaysToDate(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;

  }
  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  check() {
    this.contractStatus = true;
  }
  changeAgreementDate(date) {
    // let dateData = date;
    // let tFMdays = Number(this.totalDurationMonth) * 30;
    // let tFddays = Number(this.totalDurationDays);
    // let totalDays = tFMdays + tFddays;
    // console.log(totalDays)
    // let patt = new RegExp(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/);
    // if (patt.test(date)) {
    //   this.approvalEntryForm.controls['estimatedWorkCompletionDate'].setValue(this.formatDate(this.addDaysToDate(dateData, totalDays)));
    // }
    const _formData1 = {
      'activity': this.activityDetailId,
      'contractDate': date
    }

    this.eds.getDate(_formData1).subscribe((result) => {
      this.approvalEntryForm.controls['estimatedWorkCompletionDate'].setValue(result['completionDate']);
      this.contractStatus = false;
    },
      (error) => {
        this.contractStatus = false;
      });
    this.contractStatus = false;
  }

  addItem() {
    let length = this.approvalEntryForm.get('othersDonators')['controls'].length;
    const control1 = <FormArray>this.approvalEntryForm.controls['othersDonators'];
    control1.push(this.initOtherItems());
    this.approvalEntryForm.get('othersDonators')['controls'][length].get("name").setValidators(Validators.required);
    this.approvalEntryForm.get('othersDonators')['controls'][length].get("value").setValidators(Validators.required);
    // add unicode support for dynamically added input
  }

  removeItem(i: number) {
    const control1 = <FormArray>this.approvalEntryForm.controls['othersDonators'];
    control1.removeAt(i);
  }

  removeItem1(i: number) {
    const control2 = <FormArray>this.approvalEntryForm.controls['contractData'];
    control2.removeAt(i);
  }

  addItem1() {
    let length = this.approvalEntryForm.get('contractData')['controls'].length;
    const control2 = <FormArray>this.approvalEntryForm.controls['contractData'];
    const contractForm = this.initContractForm();
    contractForm.patchValue({
      contractDate: this.approvalEntryForm.get('workOrderDate').value
    });
    control2.push(contractForm);
    this.approvalEntryForm.get('contractData')['controls'][length].get("name").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("address").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("contactPerson").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("contactNumber").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("contractDate").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("contractAmount").setValidators(Validators.required);
    this.approvalEntryForm.get('contractData')['controls'][length].get("attatchment").setValidators(Validators.required);
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.approvalEntryForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
  }

  private workServiceTypeForm() {
    if (window['wpms_config']['monitoringCommitteeDescription']) {
      this.monitoringCommitteConfig = window['wpms_config']['monitoringCommitteeDescription'];
      const config = {};
      for (const data of window['wpms_config']['monitoringCommitteeDescription']) {
        config[data.name] = ['']
      }
      const group = this.fb.group(config);
      return group;
    }
    return this.fb.group({
      committeAddress: [''],
      committeId: ['',],
      administrativeId: ['',],
      memberName: [''],
      memberPost: [{ value: '', disabled: true },],
      citizenshipNumber: [''],
      scanCopy: [''],
      issueDistrict: [null],
      issueDate: [''],
      phoneNumber: ['']
    });
  }

  private workMonitoringForm() {

    return this.fb.group({
      monitoringCommitteeAddress: [''],
      monitoringCommitteeName: [''],
      monitoringCommitteePost: [{ value: '', disabled: true },],
      monitoringCommitteeCitizenshipNo: [''],
      // monitoringCommitteeFatherName: [''],
      monitoringCommitteeMobile: ['']
    });
  }

  addMoreWorkServiceType() {
    this.workServiceTypeArrayData = this.approvalEntryForm.controls[
      "workServiceTypeArray"
    ];

    this.workServiceTypeArrayData.push(
      this.workServiceTypeForm()
    );
  }

  addMonitoringCommittee() {
    this.monitoringArrayData = this.approvalEntryForm.controls[
      "monitoringCommitteeArray"
    ];

    this.monitoringArrayData.push(
      this.workMonitoringForm()
    );
  }

  removeForm(i) {
    this.workServiceTypeArrayData.removeAt(i);
  }
  addbutton() {
    this.addMoreWorkServiceType();
  }
  checkWorkService(event) {
    if (event.target.value === 'Consumer Committe' || event.target.value === 'Sahakari') {

      this.setdata1();

      this.showTable = true;
      this.showTableContract = false;
      this.showAmanantaTable = false;
      this.showButton = true;
      this.approvalEntryForm.controls['formationDate'].patchValue(this.approvalEntryForm.value['agreementDate']);

    } else if (event.target.value == 'Contract') {
      this.showTable = false;
      this.showTableContract = true;
      this.showAmanantaTable = false;
      this.showButton = false;
      // this.approvalEntryForm.controls['contractDate'].patchValue(this.technicalCost['agreementDate']);
      this.approvalEntryForm.setControl('workServiceTypeArray', this.fb.array([]));
      this.approvalEntryForm.setControl('contractData', this.fb.array([]));
      this.addItem1();

    } else if (event.target.value == 'Amanathan Marfat') {
      this.showTable = false;
      this.showTableContract = false;
      this.showAmanantaTable = true;
      this.showButton = false;

    }
    this.cdr.detectChanges();
  }
  // citizenshipAttachmentFlies: any[] = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}];

  // onFileChange(event, i) {
  //   if (event.target.files && event.target.files[0]) {
  //     var formData: FormData = new FormData();
  //     let file = event.target.files[0];
  //     let obj = {
  //       file: file,
  //       index: 'Y'
  //     };
  //     this.citizenshipAttachmentFlies[i] = obj;
  //   } else {
  //     this.citizenshipAttachmentFlies[i] = {};
  //   }
  // }

  monitoringChange(value) {
    this.approvalEntryForm.setControl('monitoringCommitteeArray', this.fb.array([]));
    if (value == 'Yes') {
      this.showMonitoringTable = true;
      let MemberPost = [
        { name: 'संयोजक' },
        { name: 'सदस्य' },
        { name: 'सदस्य' },
        { name: 'सदस्य' },
        { name: 'सदस्य' },
      ];
      for (let i = 0; i < 5; i++) {
        this.addMonitoringCommittee();
        this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get('monitoringCommitteePost').patchValue(MemberPost[i].name);

        if (i < 3) {
          this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get("monitoringCommitteeName").setValidators(Validators.required);
          // this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get("monitoringCommitteeFatherName");
          // this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get("monitoringCommitteeAddress").setValidators(Validators.required);
          // this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get("monitoringCommitteeCitizenshipNo").setValidators(Validators.required);
          // this.approvalEntryForm.get('monitoringCommitteeArray')['controls'][i].get("monitoringCommitteeMobile").setValidators(Validators.required);
        }

      }
    } else {
      this.showMonitoringTable = false;
    }
  }

  getActivityDetail(id) {
    this.jqxLoader.open();
    this.ads.show(id).subscribe(
      result => {
        this.techCost = result['pCost'];
        this.activityDetailData = result;
        this.totalDurationDays = result && result['totalDurationDays'];
        this.totalDurationMonth = result && result['totalDurationMonth'];
        // setTimeout(() => {

        //   this.approvalEntryForm.controls['benifitedHouses'].patchValue(this.technicalCost['benifitedHouses']);
        //   this.approvalEntryForm.controls['benifitedPopulation'].patchValue(this.technicalCost['benifitedPopulation']);
        //   this.approvalEntryForm.controls['befinitedOrganizations'].patchValue(this.technicalCost['befinitedOrganizations']);
        //   this.approvalEntryForm.controls['othersBenifited'].patchValue(this.technicalCost['othersBenifited']);
        //   this.approvalEntryForm.controls['malePopulation'].patchValue(this.technicalCost['malePopulation']);
        //   this.approvalEntryForm.controls['femalePopulation'].patchValue(this.technicalCost['femalePopulation']);
        //   this.approvalEntryForm.controls['dalitPopulation'].patchValue(this.technicalCost['dalitPopulation']);
        //   this.approvalEntryForm.controls['janajatiPopulation'].patchValue(this.technicalCost['janajatiPopulation']);
        //   this.approvalEntryForm.controls['othersPopulation'].patchValue(this.technicalCost['othersPopulation']);
        //   this.approvalEntryForm.controls['agreementDate'].patchValue(this.technicalCost['agreementDate']);
        //   this.approvalEntryForm.controls['estimatedWorkCompletionDate'].patchValue(this.technicalCost['completionDate']);
        // }, 200);
        this.jqxLoader.close();
      },
      error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  selectChange(a: any) {
    let len = this.approvalEntryForm.get('contractData')['controls'].length;
    for (let i = 0; i < len; i++) {
      if (i != a) {
        this.approvalEntryForm.get('contractData')['controls'][i].get("selected").setValue(0)
      }
    }
  }

  // public uploadFileCreate(fileStu: File, fileFath: File, fileMath: File, id) {
  //   //note: no HttpHeaders passed as 3d param to POST!
  //   //So no Content-Type constructed manually.
  //   //Angular 4.3/4.x does it automatically.
  //   const _formData = new FormData();
  //   _formData.append('studentPhoto', fileStu, fileStu.name);
  //   _formData.append('fatherPhoto', fileFath, fileFath.name);
  //   _formData.append('motherPhoto', fileMath, fileMath.name);
  //   return this.http.post(this.apiUrl + 'students/studentphotodetailsSave?studentId=' + id, _formData);
  // }

  handleFileInput(event) {
    this.selectedFiles = event.target.files;
  }
  handleFileInputContract(i, event) {

    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.approvalEntryForm.get('contractData')['controls'][i].patchValue({
        attachmentData: file
      });
    }
  }

  /**
 * Function triggered when save button is clicked
 * @param formData
 */
  save(formValue) {
    let formData = this.approvalEntryForm.getRawValue();
    formData['grantedFromUnion'] = "0";
    const _formData = new FormData();
    _formData.append('activity', this.activityDetailId);

    // _formData.delete('benifitedPopulation');
    // _formData.append('benifitedPopulation', this.approvalEntryForm.get('benifitedPopulation').value)

    for (const data in formData) {
      if (data == 'workServiceTypeArray') {
        for (let j = 0; j < formData[data].length; j++) {
          for (const config of this.monitoringCommitteConfig) {
            _formData.append(config.name, formData[data][j][config.name] || '');
          }
        }
      } else if (data == 'monitoringCommitteeArray') {
        for (let j = 0; j < formData[data].length; j++) {
          if (formData[data][j].monitoringCommitteeName !== "") {
            _formData.append('monitoringCommitteePost', formData[data][j].monitoringCommitteePost);
            _formData.append('monitoringCommitteeName', formData[data][j].monitoringCommitteeName);
            // _formData.append('monitoringCommitteeFatherName', formData[data][j].monitoringCommitteeFatherName);
            _formData.append('monitoringCommitteeAddress', formData[data][j].monitoringCommitteeAddress);
            _formData.append('monitoringCommitteeCitizenshipNo', formData[data][j].monitoringCommitteeCitizenshipNo);
            _formData.append('monitoringCommitteeMobile', formData[data][j].monitoringCommitteeMobile);
          }
        }
      } else {
        _formData.append(data, formData[data]);
      }

    }
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let fileIndex = i + 1;
        _formData.append('imageName', this.selectedFiles[i], this.selectedFiles[i].name)
      }
    }

    // 
    for (let data of formValue.contractData) {
      _formData.append('docId', data.docId);
      _formData.append('name', data.name);
      _formData.append('address', data.address);
      _formData.append('contactPerson', data.contactPerson);
      _formData.append('contactNumber', data.contactNumber);
      _formData.append('contractDate', data.contractDate);
      _formData.append('contractAmount', data.contractAmount);
      _formData.append('selected', data.selected);
      _formData.append('attachment', data.attachmentData);
      // _formData.append('')
    }

    // if (this.contractSelectedFiles.length > 0) {
    //   for (let i = 0; i < this.contractSelectedFiles.length; i++) {
    //     let fileIndex = i + 1;
    //     _formData.append('attatchment', this.contractSelectedFiles[i], this.contractSelectedFiles[i].name)
    //   }
    // }
    for (let i = 0; i < formData['othersDonators'].length; i++) {
      let fileIndex = i + 1;
      _formData.append('grantedFromOthersName', formData['othersDonators'][i].name);
      _formData.append('grantedFromOthersNameValue', formData['othersDonators'][i].value);
    }
    _formData.append("userId", JSON.parse(localStorage.getItem('pcUser')).userId);

    this.jqxLoader.open();
    console.log(_formData)
    this.aas.store(_formData).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          const userType = JSON.parse(localStorage.getItem("pcUser")).userType;
          if (userType.toLowerCase() == 'super admin' || userType.toLowerCase() == 'admin') {
            this.approve();
          } else {
            this.router.navigate(['approval/administrative-approval']);
          }
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

  approve() {
    const userId = JSON.parse(localStorage.getItem('pcUser'))['userId'];
    this.aos.approveAdministrative(userId, this.activityDetailId).subscribe(
      result => {
        if (result["message"]) {
          this.jqxLoader.close();
          let messageDiv: any = document.getElementById("message");
          messageDiv.innerText = result["message"];
          this.msgNotification.open();
          this.aos.showActivity(this.activityDetailId).subscribe((res) => {

            localStorage.setItem('ActivityDatas', JSON.stringify(res));
            if (this.checking) {
              this.location.back();
            } else {
              this.activityDatas = JSON.parse(localStorage.getItem('ActivityDatas'));
              this.router.navigate(['/approval/administrtive-approval-aggrement', this.activityDatas['id']]);
            }


          }, (error) => {
            console.info(error);
          });
        }
        if (result["error"]) {
          this.jqxLoader.close();
          let messageDiv: any = document.getElementById("error");
          messageDiv.innerText = result["error"]["message"];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        let messageDiv: any = document.getElementById("error");
        messageDiv.innerText = error["message"];
        this.errNotification.open();
      }
    );
    let post = {
      activity: this.activityDetailId,
      amount: "",
      entryDate: this.fullDate,
      id: "",
      karmagat: "No",
      nextPlanYear: "",
      reason: "",
      status: "Ongoing",
    }
    this.aos.storeExecutionStatus(post).subscribe((result) => {
      if (result['message']) {
        // let messageDiv: any = document.getElementById('sus');
        // messageDiv.innerText = result['message'];
        this.aos.showActivity(this.activityDetailId).subscribe((res) => {

          localStorage.setItem('ActivityDatas', JSON.stringify(res));
        }, (error) => {
          console.info(error);
        });
      }
    },
      error => {
        // this.jqxLoader.close();
        console.info(error);
      });
  }

  resetForm() {
    this.approvalEntryForm.reset();
    // this.myComboBox.clearSelection();
  }

  Close() {
    this.location.back();
  }
  checkValue(event: any) {
    if (event == 'A') {
      this.approvalEntryForm.setControl('othersDonators', this.fb.array([]));
      this.showArray = true;
    }
    if (event == 'B') {
      this.approvalEntryForm.setControl('othersDonators', this.fb.array([]));
      this.showArray = false;
    }
  }
  // CheckDate(date){
  //   console.log(date);
  // }
  // findInvalidControls() {
  //   const invalid = [];
  //   const controls = this.AddCustomerForm.controls;
  //   for (const name in controls) {
  //     if (controls[name].invalid) {
  //       invalid.push(name);
  //     }
  //   }
  //   return invalid;
  // }
  committeeChange(value) {
    const wscontrols = this.approvalEntryForm.get('workServiceTypeArray')['controls'];
    let data = wscontrols[value].get('citizenshipNumber').value;
    const error = 'कृपया ना.प्र.नं सबैकाे बेग्लाबेग्लै लेख्नुहाेला'
    if (data) {
      data = data.trim();
      for (let i = 0; i < wscontrols.length; i++) {
        if (i === value) { continue; }
        const pp = wscontrols[i].get('citizenshipNumber').value;
        if (data === (pp && pp.trim())) {
          wscontrols[value].get('citizenshipNumber').patchValue('');
          this.showError(error);
          break;
        }
      }

      const mc_controls = this.approvalEntryForm.get('monitoringCommitteeArray')['controls'];
      for (let i = 0; i < mc_controls.length; i++) {
        const mc_value = mc_controls[i].get('monitoringCommitteeCitizenshipNo').value;
        if ((mc_value && mc_value.trim()) === data) {
          wscontrols[value].get('citizenshipNumber').patchValue('');
          this.showError(error);
          break;
        }
      }

    }
  }
  private showError(error: string) {
    const messageDiv: any = document.getElementById('error');
    messageDiv.innerText = error;
    this.errNotification.open();
  }

  mCommitteeChange(value) {
    const error = 'कृपया ना.प्र.नं सबैकाे बेग्लाबेग्लै लेख्नुहाेला';
    const mc_controls = this.approvalEntryForm.get('monitoringCommitteeArray')['controls'];
    let data = mc_controls[value].get('monitoringCommitteeCitizenshipNo').value;
    if (data) {
      data = data.trim();
      for (let i = 0; i < mc_controls.length; i++) {
        if (i === value) { continue; }
        const pp = mc_controls[i].get('monitoringCommitteeCitizenshipNo').value;
        if (data === (pp && pp.trim())) {
          mc_controls[value].get('monitoringCommitteeCitizenshipNo').patchValue('');
          this.showError(error);
          break;
        }
      }

      const ws_controls = this.approvalEntryForm.get('workServiceTypeArray')['controls'];
      for (let i = 0; i < ws_controls.length; i++) {
        const pp = ws_controls[i].get('citizenshipNumber').value;
        if (data === (pp && pp.trim())) {
          mc_controls[value].get('monitoringCommitteeCitizenshipNo').patchValue('');
          this.showError(error);
          break;
        }
      }
    }
  }
}
