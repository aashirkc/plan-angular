import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrativeApprovalEntryComponent } from './administrative-approval-entry.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AdministrativeApprovalEntryRoutingModule } from './administrative-approval-entry-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdministrativeApprovalEntryRoutingModule
  ],
  declarations: [AdministrativeApprovalEntryComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AdministrativeApprovalEntryModule { }
