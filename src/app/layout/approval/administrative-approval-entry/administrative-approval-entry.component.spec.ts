import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrativeApprovalEntryComponent } from './administrative-approval-entry.component';

describe('AdministrativeApprovalEntryComponent', () => {
  let component: AdministrativeApprovalEntryComponent;
  let fixture: ComponentFixture<AdministrativeApprovalEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrativeApprovalEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrativeApprovalEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
