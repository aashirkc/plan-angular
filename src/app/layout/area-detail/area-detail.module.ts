import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreaDetailComponent } from './area-detail.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { AreaDetailRoutingModule } from './area-detail-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AreaDetailRoutingModule
  ],
  declarations: [AreaDetailComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AreaDetailModule { }
