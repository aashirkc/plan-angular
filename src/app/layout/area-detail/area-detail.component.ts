import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Location } from '@angular/common';
import { DashboardServiceService } from '../../shared';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-area-detail',
  templateUrl: './area-detail.component.html',
  styleUrls: ['./area-detail.component.scss']
})
export class AreaDetailComponent implements OnInit {

  @ViewChild('myGrid') myGrid: jqxGridComponent;

  ward: any;
  code: any;
  planYear: any;
  wardData: any;

  source:any = {};
  dataAdapter:any;
  columns:any = [];
  lang:string;
  transData:any;


  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private dss: DashboardServiceService,
    private translate: TranslateService,
  ) {
    this.getTranslation();
  }

  ngOnInit() {
    this.lang = this.translate.currentLang.substr(0,2);
    this.ward = this.route.snapshot.paramMap.get('ward');
    this.code = this.route.snapshot.paramMap.get('code');
    this.planYear = this.route.snapshot.paramMap.get('planYear');
    console.log(this.planYear)
    console.log(this.code);

    let post = {'planYear':this.planYear}




      this.dss.getWardByWardCode(this.code,post).subscribe(
        result => {
          this.wardData = result;
          result['data'].forEach(element => {
            switch(element.executionReportStatus) {
              case "Not Started": {
                element.executionReportStatus = "सुरू भएको छैन";
                break;
              }
              case "Ongoing": {
                element.executionReportStatus = "चलिरहेको";
                break;
              }
              case "Completed": {
                element.executionReportStatus = "पूर्ण भयो";
                break;
              }
              case "Suspended": {
                element.executionReportStatus = "निलम्बित";
                break;
              }
              case "Spill Over": {
                element.executionReportStatus = "स्पिल ओभर";
                break;
              }
              case "Abandoned": {
                element.executionReportStatus = "त्याग्यो";
                break;
              }
            }
          });
          this.source.localdata = result['data'];
          console.log(result['data']);
          this.myGrid.updatebounddata();
        },
        error => {
          console.log(error);
        }
      );

  }

  getTranslation() {
    this.translate.get(['SN', 'VIEW', 'DETAIL', "EDIT", "ACTION", "UPDATE", "DELETESELECTED", "RELOAD",
      "ACTIVITY_NAME", "ACTIVITY_TYPE","TECH_PROPOSED_COST", "ACTIVITY_FOCUS_AREA", "ACTIVITY_FOR", "STATUS", "PROPOSED_COST", "TOT_EST_COST","FOCUS_AREA_LEVEL"]).subscribe((translation: [string]) => {
        this.transData = translation;
        this.loadGrid();
      });
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'planYear', type: 'string' },
          { name: 'activityForName', type: 'string' },
          { name: 'activityFor', type: 'string' },
          { name: 'focusAreaLevel1', type: 'string' },
          { name: 'focusAreaLevel1Name', type: 'string' },
          { name: 'activityName', type: 'string' },
          { name: 'activityCategory', type: 'string' },
          { name: 'activityTypeName', type: 'string' },
          { name: 'executionReportStatus', type:'string'},
          { name: 'totalEstimatedCost', type:'string'},
          { name: 'pCost', type:'string'},
          { name: 'techCost', type:'string'},
        ],
        id: 'id',
        localdata: [],
        pagesize: 100
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      { text: this.transData['ACTIVITY_NAME'],  datafield: 'activityName', columntype: 'textbox', editable: false, },
      { text: this.transData['ACTIVITY_FOR'],  datafield: 'activityForName', columntype: 'textbox', editable: false },
   
      { text: this.transData['FOCUS_AREA_LEVEL'],  datafield: 'focusAreaLevel1Name', columntype: 'textbox', editable: false, },
      { text: this.transData['TOT_EST_COST'], datafield: 'totalEstimatedCost', displayfield: "totalEstimatedCost", columntype: 'textbox', editable: false,
        // cellsrenderer:(row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
        //   let cellValue = 0;
        //   if(rowdata.pCost > 0){
        //     cellValue = rowdata.pCost;
        //   }else{
        //     cellValue = rowdata.totalEstimatedCost;
        //   }
        //   return '<div style="padding:4px 3px;">'+cellValue+'</div>';
        // }
      },
      { text: this.transData['TECH_PROPOSED_COST'], datafield: 'pCost', displayfield: "pCost", columntype: 'textbox', editable: false, },
      { text: this.transData['STATUS'], datafield: 'executionReportStatus', displayfield: "executionReportStatus", columntype: 'textbox', editable: false, width:'200' },
    ];
  }

  goBack() {
    this.location.back();
  }

}
