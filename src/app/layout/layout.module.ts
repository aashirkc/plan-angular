import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { HttpClientModule, HttpClientXsrfModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HeaderComponent, TfmHttpInterceptorService, SidebarComponent, QuickaccessComponent, LetterheadComponent } from '../shared';
import { SharedModule } from '../shared/modules/shared.module';

import { jqxMenuComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxmenu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { SidebarMenuItemComponent } from 'app/shared/components/sidebar/sidebar-menu-item/sidebar-menu-item.component';
// import { GetAllFilesComponent } from './get-all-files/get-all-files.component';
// import { GenerateAllReportComponent } from './generate-all-report/generate-all-report.component';


@NgModule({
    imports: [
        CommonModule,
        NgbDropdownModule.forRoot(),
        LayoutRoutingModule,
        TranslateModule,
        SharedModule,
        MatSidenavModule,
        MatListModule
    ],
    declarations: [
        LayoutComponent,
        HeaderComponent,
        SidebarComponent,
        QuickaccessComponent,
        jqxMenuComponent,
        SidebarMenuItemComponent
    ],
    providers: [

    ],
    entryComponents: [
        QuickaccessComponent,
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class LayoutModule { }


