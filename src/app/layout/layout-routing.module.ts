import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', 
        component: LayoutComponent,
        children: [
            { path:'', redirectTo: 'dashboard', pathMatch:'full'},
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'area', loadChildren: './area-detail/area-detail.module#AreaDetailModule' },
            { path: 'setup', loadChildren:'./setup/setup.module#SetupModule'},
            { path: 'utility', loadChildren:'./utility/utility.module#UtilityModule'},
            { path: 'activity', loadChildren:'./activity/activity.module#ActivityModule'},
            { path: 'approval', loadChildren:'./approval/approval.module#ApprovalModule'},
            { path: 'progress-reporting', loadChildren:'./progress-reporting/progress-reporting.module#ProgressReportingModule'},
            { path: 'report', loadChildren:'./report/report.module#ReportModule'},
            { path: 'khata-sanchalan', loadChildren:'./khata-sanchalan/khata-sanchalan.module#KhataSanchalanModule'},
            { path: 'remaining-activity', loadChildren: './remaining-activity/remaining-activity.module#RemainingActivityModule' },
            { path:'generate-all-report',loadChildren:'./generate-all-report/generate-all-report.module#GenerateAllReportModule'},
            { path:'get-all-files',loadChildren:'./get-all-files/get-all-files.module#GetAllFilesModule'}
          
        
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
