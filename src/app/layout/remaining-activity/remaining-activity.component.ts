import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivityMasterService } from '../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { TranslateService } from '@ngx-translate/core';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';

@Component({
  selector: 'app-remaining-activity',
  templateUrl: './remaining-activity.component.html',
  styleUrls: ['./remaining-activity.component.scss']
})
export class RemainingActivityComponent implements OnInit {

  
  transData: any;
  lang: any;
  SearchData:any=[];
  
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

  constructor(
    private translate: TranslateService,
    private as: ActivityMasterService,    
    private router: Router, 
  ) {
  }

  ngOnInit() {
    this.lang = this.translate.currentLang.substr(0,2);
    this.as.remIndex({}).subscribe((res) => {
      if(res && res.length>0){
      this.SearchData = res;
     
      }
    }, (error) => {
      console.info(error);
    });
  }
  
  ngAfterViewInit() {
   
  }


  goto(id){
    console.log(id)
     this.router.navigate(['/approval/administrtive-approval-aggrement',id]);
  }



  printReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            position:relative;
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }

          @font-face {
            font-family: 'nepaliNormal';
            src: url('assets/css/Kantipur.TTF');
            font-weight: normal;
            font-style: normal;
        }
        .nepaliText{
          font-family:'nepaliNormal';
          font-size:18px!important;
      }

          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .last-td{
            display:none;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
