import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RemainingActivityComponent } from './remaining-activity.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { RemainingActivityRoutingModule } from './remaining-activity-routing.module';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RemainingActivityRoutingModule
  ],
  declarations: [RemainingActivityComponent]
})
export class RemainingActivityModule { }
