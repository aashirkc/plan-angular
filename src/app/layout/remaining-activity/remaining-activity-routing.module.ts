import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RemainingActivityComponent } from './remaining-activity.component';

const routes: Routes = [
    { path: '', component: RemainingActivityComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RemainingActivityRoutingModule { }
