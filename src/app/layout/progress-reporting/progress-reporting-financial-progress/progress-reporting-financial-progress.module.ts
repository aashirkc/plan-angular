import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ProgressReportingFinancialProgressComponent } from './progress-reporting-financial-progress.component';
import { ProgressReportingFinancialProgressRoutingModule } from './progress-reporting-financial-progress-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ProgressReportingFinancialProgressRoutingModule
  ],
  declarations: [
    ProgressReportingFinancialProgressComponent,
  ]
})
export class ProgressReportingFinancialProgressModule { }
