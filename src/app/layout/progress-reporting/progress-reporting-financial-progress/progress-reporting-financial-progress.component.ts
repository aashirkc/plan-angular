import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import { switchMap } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { ProgressReportingService, ActivityOutputService, DateConverterService, FinancialProgress, CustomValidators } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';


@Component({
  selector: 'app-progress-reporting-financial-progress',
  templateUrl: './progress-reporting-financial-progress.component.html',
  styleUrls: ['./progress-reporting-financial-progress.component.scss']
})
export class ProgressReportingFinancialProgressComponent implements OnInit {

  @ViewChild('viewWindow') viewWindow: jqxWindowComponent;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;

  // myExcel:boolean=false;
  columns: any[] = [];
  datafields: any[] = [];
  documnet: any;
  percentLeft: any;
  userData: any = {};
  source: any =
    {
      localdata: [],
      unboundmode: true,
      totalrecords: 50,
      datafields: this.datafields
    };
  workCompletionExcelSelected: File;
  dataAdapter: any = new jqx.dataAdapter(this.source);
  numberrenderer = (row: number, column: any, value: any): string => {
    return '<div style="text-align: center; margin-top: 5px;">' + (1 + value) + '</div>';
  }
  outputTypeAdapter: any = [
    {
      name: 'Service Details',
      value: 'SD'
    },
    {
      name: 'Capacity Buildings',
      value: 'CB'
    },
    {
      name: 'Work Output Assets',
      value: 'WOA'
    }
  ];
  oType: any;
  activeRoute: any;
  generateData(): void {
    for (let i = 0; i < 26; i++) {
      let text = String.fromCharCode(65 + i);
      if (i == 0) {
        let cssclass = 'jqx-widget-header';
        this.columns[this.columns.length] = { pinned: true, exportable: false, text: '', columntype: 'number', cellclassname: cssclass, cellsrenderer: this.numberrenderer };
      }
      this.datafields[this.datafields.length] = { name: text };
      this.columns[this.columns.length] = { text: text, datafield: text, width: 60, align: 'center' };
    }
  }
  investigationStageAdapter: Array<any> = [];
  financialProgressForm: FormGroup;
  activityDetailId: string;
  technicalCost: string;
  financialId: any;
  selectedFiles: any;
  billDoucment: any;
  issueDocument: any = [];
  attachment: any = [];
  fullDate: string;
  showExel: boolean = false;
  financialProgressData: any = [];
  financialProgressTable: any = [];
  docUrl: string;
  windowData: any;
  limitAmount: any;
  charan: any;
  charanName: any;
  checkAmount: boolean = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private prs: ProgressReportingService,
    private dcs: DateConverterService,
    private avs: ActivityOutputService,
    private location: Location,
    @Inject('API_URL_DOC') docUrl: string,
  ) {
    this.docUrl = docUrl;
    this.createForm();
  }

  ngOnInit() {
    // this.oType = this.activeRoute.snapshot.params['outputType'];
    let userDetalils = JSON.parse(localStorage.getItem('pcUser'));
    this.userData = userDetalils['userType'];
    this.generateData();
    let currentYear = new Date().getFullYear();
    let month = new Date().getMonth() + 1;
    let day = new Date().getDate();
    let engDate = currentYear + '-' + month + '-' + day;
    let date = this.dcs.ad2Bs(engDate);
    this.fullDate = date['fulldate'];
    this.financialProgressForm.controls['enterDate'].patchValue(this.fullDate);
    this.financialProgressForm.controls['enterDate'].updateValueAndValidity();
    this.financialProgressForm.controls['issueDate'].patchValue(this.fullDate);
    this.financialProgressForm.controls['issueDate'].updateValueAndValidity();
  }

  ngAfterViewInit() {
    this.activityDetailId = this.route.snapshot.paramMap.get('id');
    let post = { activity: this.activityDetailId };
    this.getFinancialProgressDetail(post);
    this.financialProgressForm.controls['billedAmount'].setValue(this.financialProgressTable && this.financialProgressTable.restIssueAmount);
  }

  addItem() {
    this.showExel = true;

  }
  /**
 * Create the form group
 * with given form control name
 */
  createForm() {
    this.financialProgressForm = this.fb.group({
      'id': [''],
      'enterDate': ['', Validators.compose([Validators.required, CustomValidators.checkDate])],
      'issueAmount': ['', Validators.required],
      'billedAmount': ['', Validators.required],

      'issueDate': ['', Validators.compose([Validators.required, CustomValidators.checkDate])],
      // 'voucherNo': ['', Validators.required],
      'investigationStage': ['']
    });
  }

  chnge(value) {
    if (value < 0) {
      this.financialProgressForm.controls['billedAmount'].patchValue(0);
    }

    if (Number(value) > Number(this.limitAmount)) {
      this.showError('बिल रकम प्राबिधिक रकम भन्दा धेरै भयो ।');
      this.financialProgressForm.controls['issueAmount'].patchValue(0);
    }
    const bill_amount = value;
    const issue_amount = this.financialProgressForm.controls['issueAmount'].value;
    if (bill_amount < issue_amount) {
      this.showError('पेश गरिएको रकम भन्दा प्राबिधिक मुल्यांकन रकम धेरै भयो');
      this.financialProgressForm.controls['issueAmount'].patchValue(0);
    }
  }

  issueChange(value) {
    if (value < 0) {
      this.financialProgressForm.controls['issueAmount'].patchValue(0);
    }

    if (Number(value) > Number(this.limitAmount)) {
      this.showError('जारी गरिएकाे रकम बाँकि रकम भन्दा धेरै भयाे ।')
      this.financialProgressForm.controls['issueAmount'].patchValue(0);
    }
    const issue_amount = value;
    const bill_amount = this.financialProgressForm.controls['billedAmount'].value; //mageko paisa
    if (bill_amount < issue_amount) {
      this.showError('पेश गरिएको रकम भन्दा प्राबिधिक मुल्यांकन रकम धेरै भयो');
      this.financialProgressForm.controls['issueAmount'].patchValue(0);
    }
  }

  workCompletionPercentage(value) {
    if (Number(value) > 100) {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'प्रतिशत १०० भन्दा धेरै हुदैन';
      this.errNotification.open();
      this.financialProgressForm.controls['workCompletionPercentage'].patchValue(100);
    }
  }
  getFinancialProgressDetail(post) {
    this.jqxLoader.open();
    this.prs.getFinancialProgress(post).subscribe(
      result => {
        console.info(result);
        this.financialProgressData = result['Record'];
        let totalPhysicalPhase = result['charan'].length;
        if (totalPhysicalPhase < 1) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = 'पहिले भौतिक प्रगति प्रविष्ट गर्नुहोस् ।';
          this.errNotification.open();
        }
        this.charan = result['charan'][totalPhysicalPhase - 1]['id']
        this.charanName = result['charan'][totalPhysicalPhase - 1]['name']
        // for (let i = 0; i < result['record'].length; i++){
        //     if((result['record']['totalremainingAmt']) == 0){
        //       this.checkAmount = true;
        //       console.log('sucess')
        //     }
        // }
        this.financialProgressTable = result;
        this.financialProgressForm.controls['billedAmount'].setValue(result && result['restIssueAmount']);
        this.getLimitAmount(result);
        this.jqxLoader.close();
      },
      error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
    this.avs.getInvestigationStage().subscribe((res) => {
      this.investigationStageAdapter = res && res['Record'];
    }, (error) => {
      console.info(error);
    });
  }

  selectedEditData: any;
  editViewData: any;

  private getLimitAmount(result: any[]) {
    let lenth = result && result['Record'] && result['Record'].length || 0;
    if (result && result['Record'] && result['Record'].length > 0) {

      this.limitAmount = result['Record'][lenth - 1]['totalremainingAmt'] || '';
      if (this.limitAmount == 0) {
        this.checkAmount = true;
        console.log(this.limitAmount);
      }
      this.percentLeft = 100 - Number(result['Record'][lenth - 1]['data']['workCompletionPercentage']);
    } else {
      this.avs.getTechnicalApproveById(this.activityDetailId).subscribe((result: any) => {
        this.percentLeft = 100;
        if (result['data']) {
          this.limitAmount = result['data']['techCost'] || '';
          if (this.limitAmount == 0) {
            this.checkAmount = true;
          }
        }
      });
    }
  }

  setTipadiAdeshData(data) {
    this.router.navigate(['/progress-reporting/progress-reporting-tippani-aadesh/' + this.activityDetailId + '/' + data['data']['id'], { stage: data['data']['investigationStage']['name'] }]);
  }
  setEditData(data) {
    console.log(data);
    this.selectedEditData = data;
    let formData = new FinancialProgress(data['data']);
    setTimeout(() => {
      this.financialProgressForm.get('investigationStage').patchValue(data['data']['investigationStage'].id || null);
    }, 100)
    this.financialProgressForm.patchValue(formData);

    this.selectedEditData['attType'] = [];
    for (let i = 0; i < this.selectedEditData['attDocument'].length; i++) {
      if (this.selectedEditData['attDocument'][i]) {
        let ext = this.selectedEditData['attDocument'][i].split('.').pop();
        this.selectedEditData['attType'].push(ext);
      }
    }

    // For Setting Excelsheet
    if (JSON.parse(data['data'].billDocument).length > 0) {
      this.showExel = true;
      let array = JSON.parse(data['data'].billDocument);
      setTimeout(() => {
        this.source.localdata = array;
        this.myGrid.updatebounddata();
      }, 100);
    }
    this.selectedEditData['issueType'] = [];
    for (let i = 0; i < this.selectedEditData['issueDocument'].length; i++) {
      if (this.selectedEditData['issueDocument'][i]) {
        let ext = this.selectedEditData['issueDocument'][i].split('.').pop();
        this.selectedEditData['issueType'].push(ext);
      }
    }

    // remaining data
    console.log(this.selectedEditData);

    this.limitAmount = Number(this.limitAmount) + Number(this.selectedEditData['data']['issueAmount']);

    this.editViewData = this.selectedEditData;

  }

  viewData(data) {
    this.windowData = {};
    let formatData = data;

    this.windowData['attType'] = [];

    formatData['attType'] = [];
    for (let i = 0; i < formatData['attDocument'].length; i++) {
      if (formatData['attDocument'][i]) {
        let ext = formatData['attDocument'][i].split('.').pop();
        formatData['attType'].push(ext);
      }
    }

    formatData['billType'] = [];
    for (let i = 0; i < formatData['billDocument'].length; i++) {
      if (formatData['billDocument'][i]) {
        let ext = formatData['billDocument'][i].split('.').pop();
        formatData['billType'].push(ext);
      }
    }

    formatData['issueType'] = [];
    for (let i = 0; i < formatData['issueDocument'].length; i++) {
      if (formatData['issueDocument'][i]) {
        let ext = formatData['issueDocument'][i].split('.').pop();
        formatData['issueType'].push(ext);
      }
    }
    this.windowData = formatData;

    this.viewWindow.open();

    //filename.split('.').pop();
  }

  handleFileInput(event, type) {
    if (type == 'bill') {
      const fileSelected: File = event.target.files[0];
      this.billDoucment = fileSelected
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
      }
    } else if (type == 'issue') {
      for (let file of event.target.files) {
        this.issueDocument.push(file);
      }
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
      }
    } else if (type == 'attachment') {
      for (let file of event.target.files) {
        this.attachment.push(file);
      }
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
      }
    }
    // this.selectedFiles = event.target.files;
  }

  /**
 * Function triggered when save button is clicked
 * @param formData
 */
  public fileEventM(event: any) {
    const fileSelected: File = event.target.files[0];
    this.workCompletionExcelSelected = fileSelected;
  }

  showError = (message: string) => {
    let messageDiv: any = document.getElementById('error');
    messageDiv.innerText = message;
    this.errNotification.open();
  }

  checkTotalAmount = (): boolean => {
    const formValue = this.financialProgressForm.value;
    if (this.financialProgressData && this.financialProgressData.length > 0) {
      const remainingBudget = this.financialProgressData[this.financialProgressData.length - 1]['totalremainingAmt'];
      return (remainingBudget - formValue.issueAmount) < 0
    }
    return false;
  }

  save(formData) {

    if (this.checkTotalAmount()) {
      this.showError('बजेट भन्दा पेश गरिएको रकम बढी भयो');
      return;
    }

    if (this.userData.toLowerCase() == 'admin' || this.userData.toLowerCase() == 'super admin' || this.userData.toLowerCase() == 'activity manager') {
      const _formData = new FormData();
      let arrayCollections = [];
      if (this.showExel) {
        let data = this.myGrid.getrows();
        for (let i = 0; i < data.length; i++) {
          if (data[i]['A'] == "" && data[i]['B'] == "" && data[i]['C'] == "" && data[i]['D'] == "" && data[i]['E'] == "" && data[i]['F'] == "" && data[i]['G'] == "" && data[i]['H'] == "" && data[i]['I'] == "" && data[i]['J'] == "" && data[i]['K'] == "" && data[i]['L'] == "" && data[i]['M'] == "" && data[i]['N'] == "" && data[i]['O'] == "" && data[i]['P'] == "" && data[i]['Q'] == "" && data[i]['R'] == "" && data[i]['S'] == "" && data[i]['T'] == "" && data[i]['U'] == "" && data[i]['V'] == "" && data[i]['W'] == "" && data[i]['X'] == "" && data[i]['Y'] == "" && data[i]['Z'] == "") {

          } else {
            arrayCollections.push(data[i]);
          }
        }
      }
      if (this.workCompletionExcelSelected && this.workCompletionExcelSelected.name) {
        _formData.append('document', this.workCompletionExcelSelected, this.workCompletionExcelSelected && this.workCompletionExcelSelected.name);
      }
      _formData.append('billDocument', JSON.stringify(arrayCollections));
      _formData.append('activity', this.activityDetailId);
      _formData.append('voucherNo', '');
      _formData.append('investigationStage', this.charan);
      if (this.selectedEditData) {
        _formData.append('id', this.selectedEditData.id);
      }
      for (let data in formData) {
        _formData.append(data, formData[data]);
      }
      if (this.issueDocument) {
        for (let docs of this.issueDocument) {
          _formData.append('issueDocument', docs, docs.name);
        }
      }

      if (this.attachment) {
        for (let docs of this.attachment) {
          _formData.append('attachment', docs, docs.name);
        }
      }

      // let checkFileValidation: boolean = false;
      // if (this.selectedEditData) {
      //   checkFileValidation = true;
      // } else {
      //   if (this.billDoucment && this.issueDocument) {
      //     checkFileValidation = true
      //   } else {
      //     checkFileValidation = false;
      //   }
      // }

      // if (checkFileValidation) {
      this.jqxLoader.open();
      this.prs.financialProgressStore(_formData).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            let post = { activity: this.activityDetailId };
            this.showExel = false;
            this.getFinancialProgressDetail(post);

            // this.financialProgressForm.reset();
            if (this.selectedEditData) {
              this.selectedEditData['issueType'] = [];
              this.selectedEditData['attType'] = [];
              this.selectedEditData['billType'] = [];
              this.editViewData = this.selectedEditData;
            }

            this.resetForm();
            this.financialProgressForm.markAsDirty();

          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
      // } else {
      //   let messageDiv: any = document.getElementById('error');
      //   messageDiv.innerText = 'कृपया सम्बन्धित फाेटाे/फाइल समाबेश गर्नुहाेला';
      //   this.errNotification.open();
      // }

    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'तपाईलाई यो कार्य गर्न स्वीकृत दिएको छैन !!!!';
      this.errNotification.open();
    }
  }
  edit(formData) {
    if (this.userData.toLowerCase() == 'admin' || this.userData.toLowerCase() == 'super admin' || this.userData.toLowerCase() == 'activity manager') {
      const _formData = new FormData();
      let arrayCollections = [];
      if (this.showExel) {
        let data = this.myGrid.getrows();
        for (let i = 0; i < data.length; i++) {
          if (data[i]['A'] == "" && data[i]['B'] == "" && data[i]['C'] == "" && data[i]['D'] == "" && data[i]['E'] == "" && data[i]['F'] == "" && data[i]['G'] == "" && data[i]['H'] == "" && data[i]['I'] == "" && data[i]['J'] == "" && data[i]['K'] == "" && data[i]['L'] == "" && data[i]['M'] == "" && data[i]['N'] == "" && data[i]['O'] == "" && data[i]['P'] == "" && data[i]['Q'] == "" && data[i]['R'] == "" && data[i]['S'] == "" && data[i]['T'] == "" && data[i]['U'] == "" && data[i]['V'] == "" && data[i]['W'] == "" && data[i]['X'] == "" && data[i]['Y'] == "" && data[i]['Z'] == "") {

          } else {
            arrayCollections.push(data[i]);
          }
        }
      }
      if (this.workCompletionExcelSelected) {
        _formData.append('document', this.workCompletionExcelSelected,
          this.workCompletionExcelSelected.name);
      }
      _formData.append('billDocument', JSON.stringify(arrayCollections));
      _formData.append('voucherNo', '');

      _formData.append('activity', this.activityDetailId);
      for (let data in formData) {
        _formData.append(data, formData[data]);
      }
      if (this.issueDocument) {
        for (let data of this.issueDocument) {
          _formData.append('issueDocument[]', data, data.name);
        }
      }
      if (this.attachment) {
        for (let data of this.attachment) {
          _formData.append('attachment', data, data.name);
        }
      }

      let checkFileValidation: boolean = false;
      if (this.selectedEditData) {
        checkFileValidation = true;
      } else {
        if (this.billDoucment && this.issueDocument) {
          checkFileValidation = true
        } else {
          checkFileValidation = false;
        }
      }

      if (checkFileValidation) {
        this.jqxLoader.open();
        this.prs.financialProgressUpdate(_formData, formData.id).subscribe(
          result => {
            if (result['message']) {
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              // setTimeout(() => {
              //   window.location.reload();
              // }, 100);
              let post = { activity: this.activityDetailId };
              this.getFinancialProgressDetail(post);
              if (this.selectedEditData) {
                this.selectedEditData['issueType'] = [];
                this.selectedEditData['attType'] = [];
                this.selectedEditData['billType'] = [];
                this.editViewData = this.selectedEditData;
                this.showExel = false;
              }
              this.resetForm();
              this.source.localdata = [];
              if (this.myGrid) {
                this.myGrid.updatebounddata();
              }
            }
            this.jqxLoader.close();
            if (result['error']) {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }
          },
          error => {
            this.jqxLoader.close();
            console.info(error);
          }
        );
      } else {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'कृपया सम्बन्धित फाेटाे/फाइल समाबेश गर्नुहाेला';
        this.errNotification.open();
      }

    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'तपाईलाई यो कार्य गर्न स्वीकृत दिएको छैन !!!!';
      this.errNotification.open();
    }
  }
  resetForm() {
    const formDOM = document.getElementById('financialProgressForm') as HTMLFormElement
    if (formDOM) {
      formDOM.reset()
    }
    this.financialProgressForm.reset();
    this.getLimitAmount(this.financialProgressTable);
    this.editViewData = null;
    this.financialProgressForm.controls['enterDate'].patchValue(this.fullDate);
    this.financialProgressForm.controls['enterDate'].updateValueAndValidity();
    this.financialProgressForm.controls['issueDate'].patchValue(this.fullDate);
    this.financialProgressForm.controls['issueDate'].updateValueAndValidity();
    const attachmentControls = document.getElementsByClassName("attachment");
    this.selectedEditData = {};
  }

  close() {
    // this.router.navigate(['/progress-reporting/progress-reporting-listing']);
    this.location.back();
  }
  forward() {
    if (localStorage.getItem('outputType')) {
      this.router.navigate(['/progress-reporting/progress-reporting-physical-progress/', this.activityDetailId, { outputType: localStorage.getItem('outputType') }]);
    } else {
      this.router.navigate(['/progress-reporting/progress-reporting-listing']);
    }

    // localStorage.removeItem('outputType');
  }
  viewExcelData(dataComming) {
    this.router.navigate(['/progress-reporting/financial-bill-document-print/', dataComming['id']]);
  }

  deleteImage(imageName) {
    this.prs.deleteFinancialProgress(imageName).subscribe(response => {
      if (response && response['error']) {
        this.showError(response['error']['message']);
      } else {
        // find all the document array and 
        // then remove from the array if the name matches
        // this.editViewData['issueDocument']
        // selectedEditData['data']['excelDocument'] single element
        // editViewData['attDocument'] multiple document
        debugger;
        if (this.editViewData && this.editViewData['issueDocument'].constructor === Array) {
          const index = (this.editViewData['issueDocument'] as Array<any>).indexOf(d => d === imageName);
          if (index >= 0) {
            this.editViewData.splice(index, 1);
          }
        }

        if (
          this.selectedEditData &&
          this.selectedEditData['data'] &&
          this.selectedEditData['data']['excelDocument'] === imageName
        ) {
          this.selectedEditData['data']['excelDocument'] = '';
        }

        if (this.editViewData && this.editViewData['attDocument'].constructor === Array) {
          const index = (this.editViewData['attDocument'] as Array<any>).indexOf(d => d === imageName);
          if (index >= 0) {
            this.editViewData.splice(index, 1);
          }
        }
      }
    }, error => {
      this.showError("Error occured!");
    })
  }


}
