import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgressReportingFinancialProgressComponent } from './progress-reporting-financial-progress.component';

const routes: Routes = [
  {
    path: '',
    component: ProgressReportingFinancialProgressComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgressReportingFinancialProgressRoutingModule { }
