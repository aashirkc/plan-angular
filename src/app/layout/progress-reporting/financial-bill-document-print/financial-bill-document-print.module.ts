import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { FinancialBillDocumentPrintComponent } from './financial-bill-document-print.component';
import { FinancialBillDocumentPrintRoutingModule } from './financial-bill-document-print-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FinancialBillDocumentPrintRoutingModule
  ],
  declarations: [
    FinancialBillDocumentPrintComponent,
  ]
})
export class FinancialBillDocumentPrintModule { }
