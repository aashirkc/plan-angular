import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinancialBillDocumentPrintComponent } from './financial-bill-document-print.component';

const routes: Routes = [
  {
    path: '',
    component: FinancialBillDocumentPrintComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinancialBillDocumentPrintRoutingModule { }
