import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProgressReportingService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';

@Component({
  selector: 'app-financial-bill-document-print',
  templateUrl: './financial-bill-document-print.component.html',
  styleUrls: ['./financial-bill-document-print.component.scss']
})
export class FinancialBillDocumentPrintComponent implements OnInit {

  tableDatas: Array<any> = [];
  Heads: Array<any> = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  resultData: any;
  programDatas: any;
  organizationData: any;
  remainingAmount: number;
  constructor(
    private route: ActivatedRoute,
    private prs: ProgressReportingService,
    private router: Router,
  ) { }


  ngOnInit() {
    this.prs.getOrganization({}).subscribe(
      result => {
        this.jqxLoader.close();
        this.organizationData = result && result[0];
      },
      error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  ngAfterViewInit() {
    let financialId = this.route.snapshot.paramMap.get('id');
    this.getFinancialProgressDetail(financialId);
    this.getProgramDetails(financialId);
  }

  getFinancialProgressDetail(id) {
    this.jqxLoader.open();
    this.prs.getFinancialProgressSingle(id).subscribe(
      result => {
        this.jqxLoader.close();
        this.resultData = result;
        let data = JSON.parse(result['billDocument']);
        let arrayCollections = [];
        for (let i = 0; i < data.length; i++) {
          if (data[i]['A'] == "" && data[i]['B'] == "" && data[i]['C'] == "" && data[i]['D'] == "" && data[i]['E'] == "" && data[i]['F'] == "" && data[i]['G'] == "" && data[i]['H'] == "" && data[i]['I'] == "" && data[i]['J'] == "" && data[i]['K'] == "" && data[i]['L'] == "" && data[i]['M'] == "" && data[i]['N'] == "" && data[i]['O'] == "" && data[i]['P'] == "" && data[i]['Q'] == "" && data[i]['R'] == "" && data[i]['S'] == "" && data[i]['T'] == "" && data[i]['U'] == "" && data[i]['V'] == "" && data[i]['W'] == "" && data[i]['X'] == "" && data[i]['Y'] == "" && data[i]['Z'] == "") {

          } else {
            arrayCollections.push(data[i]);
          }
        }
        this.tableDatas = arrayCollections;
      },
      error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  getProgramDetails(id) {
    this.jqxLoader.open();
    this.prs.getFinancialProgressProgramDetails(id).subscribe(
      result => {
        this.jqxLoader.close();
        this.programDatas = result;
        this.remainingAmount = result['billedAmount'] - result['issueAmount'];
        console.info(result);
      },
      error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  printExcel() {
    let htmltable = document.getElementById('printAttachmentPrint');
    let html = htmltable.outerHTML;
    let fhtml = `
    <html>
      <head>
        <style>
         table td{
          border: 1px solid #757879;
         }
        </style>
      </head>
  <body onload="window.print();window.close()">${html}</body>
    </html>`
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(fhtml));
  }

  close() {
    this.router.navigate(['/progress-reporting/progress-reporting-financial-progress/', this.resultData['activity']]);
  }
}
