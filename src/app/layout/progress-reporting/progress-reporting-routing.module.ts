import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgressReportingComponent } from './progress-reporting.component';

const routes: Routes = [
    {
        path: '',
        component: ProgressReportingComponent,
        children: [
            { path: '', redirectTo: 'progress-reporting-listing', pathMatch: 'full' },
            { path: 'progress-reporting-listing', loadChildren: './progress-reporting-listing/progress-reporting-listing.module#ProgressReportingListingModule' },
            { path: 'progress-reporting-physical-progress/:id', loadChildren: './progress-reporting-physical-progress/progress-reporting-physical-progress.module#ProgressReportingPhysicalProgressModule' },
            { path: 'progress-reporting-financial-progress/:id', loadChildren: './progress-reporting-financial-progress/progress-reporting-financial-progress.module#ProgressReportingFinancialProgressModule' },
            { path: 'progress-reporting-completion-certificate/:id', loadChildren: './progress-reporting-completion-certificate/progress-reporting-completion-certificate.module#ProgressReportingCompletionCertificateModule' },
            { path: 'progress-reporting-tippani-aadesh/:id/:financialId', loadChildren: './progress-reporting-tippani-aadesh/progress-reporting-tippani-aadesh.module#ProgressReportingTippaniAadeshModule' },
            { path: 'anugaman-pratibedan', loadChildren: './anugaman-pratibedan/anugaman-pratibedan.module#AnugamanPratibedanModule' },
            { path: 'financial-bill-document-print/:id', loadChildren: './financial-bill-document-print/financial-bill-document-print.module#FinancialBillDocumentPrintModule' },
            { path: 'physical-bill-document-print/:id', loadChildren: './physical-bill-document-print/physical-bill-document-print.module#PhysicalBillDocumentPrintModule' },
            { path: 'anugaman-pratibedan', loadChildren: './anugaman-pratibedan/anugaman-pratibedan.module#AnugamanPratibedanModule' },           
            { path: 'mulyankan-form', loadChildren: './mulyankan-form/mulyankan-form.module#MulyankanFormModule' },
        ]
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProgressReportingRoutingModule { }
