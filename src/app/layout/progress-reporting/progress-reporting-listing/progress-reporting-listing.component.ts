import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DateConverterService, ProgressReportingService, ActivityOutputService, ActivityFocusAreaMasterService, PlanningYearService, InclusionGroupService, CurrentUserService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';

@Component({
  selector: 'app-progress-reporting-listing',
  templateUrl: './progress-reporting-listing.component.html',
  styleUrls: ['./progress-reporting-listing.component.scss']
})
export class ProgressReportingListingComponent implements OnInit {

  approvalSearchForm: FormGroup;
  viewStatusForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  sourceStatus: any;
  dataStatusAdapter: any;
  columnsStatus: any[];
  editrow: number = -1;
  fullDate: string;
  transData: any;
  planYearAdapter: Array<any> = [];
  activityAdapter: Array<any> = [];
  activityFocusAreaAdapter: any = [];
  activityCategoryAdapter: any = [];
  userData: any = {};
  SearchDatas: Array<any> = [];
  API_URL_DOC: any;
  API_URL: any;
  showSpillRow: boolean = false;
  workCompleted: boolean = false;
  columngroups: any[];
  activityId: any;
  activityName: any;
  name: 'outputType';
  type: 'string';
  workServiceTypeAdapter: any = [
    {
      name: 'Consumer Committe',
      id: 'उपभोक्ता समिति'
    },
    {
      name: 'Contract',
      id: 'ठेक्का मार्फत'
    },
    {
      name: 'Amanathan Marfat',
      id: 'अमानत मार्फत'
    },{ name: 'Sahakari', id: 'श्रम सहकारी / संस्था मार्फत' }
  ];
  workStageAdapter: any = [
    {
      id: "0",
      name: 'Not Started',
      nameNepali: 'सुरु भएको छैन'
    },
    {
      id: "1",
      name: 'Ongoing',
      nameNepali: 'चलिरहेको'
    },
    {
      id: "2",
      name: 'Completed',
      nameNepali: 'पूर्ण भएको'
    },
    {
      id: "3",
      name: 'Suspended',
      nameNepali: 'निलम्बित'
    },
    {
      id: "4",
      name: 'Spill Over',
      nameNepali: 'समयावधि थपिएको'
    },
    {
      id: "5",
      name: 'Abandoned',
      nameNepali: 'त्यागिएको'
    }
  ];
  workSAdapter: any = [
    {
      id: "0",
      name: 'Not Started',
      nameNepali: 'सुरु नभएकाे'
    },
    {
      id: "1",
      name: 'Ongoing',
      nameNepali: 'चलिरहेको'
    },
    {
      id: "2",
      name: 'Completed',
      nameNepali: 'पूर्ण भएको'
    },
    {
      id: "3",
      name: 'Suspended',
      nameNepali: 'निलम्बित'
    },
    {
      id: "4",
      name: 'Spill Over',
      nameNepali: 'समयावधि थपिएको'
    },
    {
      id: "5",
      name: 'Abandoned',
      nameNepali: 'त्यागिएको'
    }
  ];
  lang: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('errWin') errWin: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('statusGridList') statusGridList: jqxGridComponent;
  @ViewChild('myViewWindow') myViewWindow: jqxWindowComponent;
  @ViewChild('myEditWindow') myEditWindow: jqxWindowComponent;
  @ViewChild('myReportWindow') myReportWindow: jqxWindowComponent;
  @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;
  @ViewChild('susWin') susWin: jqxNotificationComponent;

  isCompleted: boolean = false;

  constructor(
    private fb: FormBuilder,
    private cus: CurrentUserService,
    private dfs: DateConverterService,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    @Inject('API_URL') API_URL: string,
    private cdr: ChangeDetectorRef,
    private pys: PlanningYearService,
    private igs: InclusionGroupService,
    private afams: ActivityFocusAreaMasterService,
    private activityOutputService: ActivityOutputService,
    private progressReportingService: ProgressReportingService,
    private planYear: PlanningYearService,
    private translate: TranslateService,
    private router: Router
  ) {
    this.createForm();
    this.getTranslation();
    this.userData = this.cus.getTokenData();
    this.API_URL_DOC = API_URL_DOC;
    this.API_URL = API_URL;
  }

  ngOnInit() {
    this.lang = this.translate.currentLang.substr(0,2);
    this.pys.index({}).subscribe((res) => {
      this.planYearAdapter = res;
      if (this.planYearAdapter.length > 0) {
        let planCode = this.planYearAdapter[0].yearCode || '';
        this.approvalSearchForm.get('planYear').patchValue(planCode);
        this.SearchData(this.approvalSearchForm.value)
      }
    }, (error) => {
      console.info(error);
    });
    this.igs.index({}).subscribe((res) => {
      this.activityAdapter = res;
    }, (error) => {
      console.info(error);
    });
    this.afams.show(0).subscribe((res) => {
      this.activityFocusAreaAdapter = res;
    }, (error) => {
      console.info(error);
    });
    this.planYear.index({}).subscribe((res) => {
      this.planYearAdapter = res;
    }, (error) => {
      console.info(error);
    });
  }
  getTranslation() {
    this.translate.get(['SN', 'ADD', 'NOT_STARTED', 'ONGOINGE', 'AMOUNT_SPILL', 'REASON_SPILL', 'SUSPENDED', 'COMPLETEDE', 'SPILLOVERE', 'ABANDONEDE', 'EDIT', 'ENTRY_DATE', 'REASON', 'STATUS', 'AMOUNT', 'ACTIVITY_FOR', 'VIEW', 'PHY_PROG', 'FINANCIAL', 'COM_CER', 'EXE_STATUS', 'ACTIVITY_FOCUS_AREA', 'ACTIVITY_TYPE', 'ACTION', 'ACTIVITY_NAME', 'NAME', 'nepali', 'english', 'mobile', 'VIEW', 'DETAIL', "UPDATE", "DELETESELECTED", "RELOAD"]).subscribe((translation: [string]) => {
      this.transData = translation;
      this.loadGrid();
    });
    let currentYear = new Date().getFullYear();
    let month = new Date().getMonth() + 1;
    let day = new Date().getDate();
    let engDate = currentYear + '-' + month + '-' + day;
    let date = this.dfs.ad2Bs(engDate);
    this.fullDate = date['fulldate'];
    this.viewStatusForm.controls['entryDate'].patchValue(this.fullDate);
    this.viewStatusForm.controls['entryDate'].updateValueAndValidity();
  }

  loadGridData() {
    let post = {};
    post = this.approvalSearchForm.value;
    // this.SearchData(post);
  }

  createForm() {
    this.approvalSearchForm = this.fb.group({
      'planYear': ['', Validators.required],
      'activityFor': [''],
      'activityFocusArea': ['', Validators.required],
      'activityCategory': ['', Validators.required],
      'activityName': [''],
      'status': [''],
      'workServiceType': [''],
      'fundProvider': ['']
    });
    this.viewStatusForm = this.fb.group({
      'entryDate': ['', Validators.required],
      'activity': [''],
      'status': ['', Validators.required],
      'amount': [''],
      'karmagat': [''],
      'nextPlanYear': [''],
      'reason': [''],
      'id': [''],
      'fundProvider': ['']
    });

  }

  ngAfterViewInit() {
    this.loadGridData();
    let formData = JSON.parse(localStorage.getItem('physicalProgress'));
    if (formData) {
      if (formData['activityFocusArea']) {
        this.LoadActivityCategory(formData);
      } else {
        this.approvalSearchForm.setValue(formData);
        this.SearchData(formData);
        setTimeout(() => {
          localStorage.removeItem('physicalProgress');
        }, 100);
      }
    }
    this.viewStatusForm.controls['karmagat'].setValue('No');
    // this.viewStatusForm.controls['entryDate'].markAsTouched();
    this.cdr.detectChanges();
  }

  LoadActivityCategory(post?: any) {
    if (post) {
      let data = post;
      if (data['activityFocusArea']) {
        this.jqxLoader.open();
        this.afams.show(data['activityFocusArea']).subscribe((res) => {
          this.activityCategoryAdapter = res;
          // Setting Data from local Storage
          let formData = JSON.parse(localStorage.getItem('physicalProgress'));
          this.approvalSearchForm.setValue(formData);
          this.SearchData(formData);
          setTimeout(() => {
            localStorage.removeItem('physicalProgress');
          }, 100);
          this.jqxLoader.close();
        }, (error) => {
          console.info(error);
          this.jqxLoader.close();
        });
      }
    } else {
      let data = this.approvalSearchForm.value;
      if (data['activityFocusArea']) {
        this.jqxLoader.open();
        this.afams.show(data['activityFocusArea']).subscribe((res) => {
          this.activityCategoryAdapter = res;
          this.jqxLoader.close();
        }, (error) => {
          console.info(error);
          this.jqxLoader.close();
        });
      }
    }
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'planYear', type: 'string' },
          { name: 'activityForName', type: 'string' },
          { name: 'activityFor', type: 'string' },
          { name: 'focusAreaLevel1', type: 'string' },
          { name: 'focusAreaLevel1Name', type: 'string' },
          { name: 'activityName', type: 'string' },
          { name: 'activityCategory', type: 'string' },
          { name: 'activityTypeName', type: 'string' },
          { name: 'outputType', type: 'string' },
          { name: 'executionReportStatus', type: 'string' },
          { name: 'a_ApprovedBy', type: 'string' },
          { name: 'kramagat', type: 'string' },

        ],
        id: 'id',
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['ACTIVITY_NAME'], datafield: 'activityName', columntype: 'textbox', editable: false },
      { text: this.transData['ACTIVITY_TYPE'], datafield: 'activityTypeName', columntype: 'extbox', editable: false, width: 120 },
      { text: this.transData['ACTIVITY_FOCUS_AREA'], datafield: 'focusAreaLevel1', displayfield: "focusAreaLevel1Name", columntype: 'textbox', editable: false, width: 200 },
      { text: 'लक्षित समावेशी<br> समूह', datafield: 'activityFor', displayfield: "activityForName", columntype: 'textbox', editable: false, width: 90 },
      {
        text: 'योजना <br>क्रमागत', datafield: 'kramagat', columntype: 'textbox', editable: false, width: 70,
        cellsrenderer: function (row, column, value) {
          if (value == "Yes") {
            return "<div style='margin:4px;color:blue;'>हो </div>";
          } else if (value == "No") {
            return "<div style='margin:4px;'>होइन </div>";
          } else {
            return "<div style='margin:4px;'>&nbsp;-&nbsp;</div>";
          }
        }
      },
      {
        text: this.transData['EXE_STATUS'], datafield: 'executionReportStatus', sortable: false, filterable: false, width: 100, columntype: 'button', columngroup: 'Actions',
        cellsrenderer: (row: number): string => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          if (dataRecord['executionReportStatus'] == 'Not Started') {
            return this.transData['NOT_STARTED'];
          } else if (dataRecord['executionReportStatus'] == 'Ongoing') {
            return this.transData['ONGOINGE'];
          } else if (dataRecord['executionReportStatus'] == 'Completed') {
            return this.transData['COMPLETEDE'];
          } else if (dataRecord['executionReportStatus'] == 'Suspended') {
            return this.transData['SUSPENDED'];
          } else if (dataRecord['executionReportStatus'] == 'Spill Over') {
            return this.transData['SPILLOVERE'];
          } else if (dataRecord['executionReportStatus'] == 'Abandoned') {
            return this.transData['ABANDONEDE'];
          }
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);

          if (dataRecord['a_ApprovedBy'] > 0) {
            this.activityId = dataRecord['id'];
            this.activityName = dataRecord['activityName'];
            this.viewStatusForm.controls['status'].setValue(dataRecord['executionReportStatus']);
            this.jqxLoader.open();
            this.loadStatusGrid()
            if (dataRecord['executionReportStatus'] == 'Spill Over' || dataRecord['executionReportStatus'] == 'Abandoned' || dataRecord['executionReportStatus'] == 'Suspended') {

              this.showSpillRow = true;
            } else {
              this.showSpillRow = false;
            }
            if (dataRecord['executionReportStatus'] == 'Completed') {
              this.workCompleted = true;
            } else {
              this.workCompleted = false;
            }
            this.myReportWindow.draggable(true);
            this.myReportWindow.title(this.transData['EXE_STATUS']);
            this.myReportWindow.open();
          }
          else {
            let message = document.getElementById('error');
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'प्रशासनिक अनुमोदन गरिएको छैन';
            this.errNotification.open();
          }
        }
      },
      {
        text: this.transData['PHY_PROG'], datafield: 'Approve', sortable: false, filterable: false, width: 80, columntype: 'button', columngroup: 'Actions',
        cellsrenderer: (): string => {
          return this.transData['PHY_PROG'];
        },
        buttonclick: (row: number): void => {
          let data = this.myGrid.getrowdata(row);

          if (data['executionReportStatus'] == "Not Started") {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'योजना सुरु भएको छैन';
            this.errNotification.open();
          } else {
            this.editrow = row;
            let dataRecord = this.myGrid.getrowdata(this.editrow);
            this.jqxLoader.close();
            if (dataRecord['outputType'] == null || dataRecord['outputType'] == 'N') {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = 'Activity has not been defined!!';
              this.errNotification.open();
            } else {
              localStorage.setItem('physicalProgress', JSON.stringify(this.approvalSearchForm.value));
              this.jqxLoader.open();
              this.router.navigate(['/progress-reporting/progress-reporting-physical-progress/', dataRecord['id'], { outputType: dataRecord['outputType'] }]);
            }
          }
        }
      },
      {
        text: 'आर्थिक प्रगति', datafield: 'financial', sortable: false, width: 90, filterable: false, columntype: 'button', columngroup: 'Actions',
        cellsrenderer: (): string => {
          return 'आर्थिक प्रगति';
        },
        buttonclick: (row: number): void => {
          let data = this.myGrid.getrowdata(row);
          localStorage.setItem('outputType', data.outputType);

          if (data['executionReportStatus'] == "Not Started") {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'योजना सुरु भएको छैन';
            this.errNotification.open();
          } else {
            this.editrow = row;
            let dataRecord = this.myGrid.getrowdata(this.editrow);
            if (dataRecord['executionReportStatus'] != 'Not Started') {
              localStorage.setItem('physicalProgress', JSON.stringify(this.approvalSearchForm.value));
              if (dataRecord['id']) {
                this.jqxLoader.open();
                this.router.navigate(['/progress-reporting/progress-reporting-financial-progress/', dataRecord['id']]);
              }
            } else {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = 'Work Not Started';
              this.errNotification.open();
            }
          }
        }
      },
      {//'<i class="fa fa-eye"></i>' +
        text: this.transData['COM_CER'], datafield: 'Edit', sortable: false, filterable: false, width: 130, columntype: 'button', columngroup: 'Actions',
        cellsrenderer: (): string => {
          return this.transData['VIEW'];
        },
        buttonclick: (row: number): void => {
          let dataRecord = this.myGrid.getrowdata(row);
          if (dataRecord['executionReportStatus'] == "Completed") {
            this.jqxLoader.open();
            localStorage.setItem('physicalProgress', JSON.stringify(this.approvalSearchForm.value));
            this.editrow = row;
            this.router.navigate(['/progress-reporting/progress-reporting-completion-certificate/', dataRecord['id'], { outputType: dataRecord['outputType'] }]);
          } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = "योजना पुरा भएको छैन";
            this.errNotification.open();
          }
        }
      },
    ];

    this.sourceStatus =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'activity', type: 'string' },
          { name: 'amount', type: 'string' },
          { name: 'entryDate', type: 'string' },
          { name: 'reason', type: 'string' },
          { name: 'status', type: 'string' },
        ],
        id: 'id',
        localdata: [],
        pagesize: 100
      }

    this.dataStatusAdapter = new jqx.dataAdapter(this.sourceStatus);
    this.columnsStatus = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['ENTRY_DATE'], datafield: 'entryDate', columntype: 'textbox', editable: false, width: 200 },
      {
        text: this.transData['STATUS'], datafield: 'status', columntype: 'textbox', editable: false, width: 200,
        cellsrenderer: function (row, column, value) {
          if (value == 'Not Started') {
            return '<div style="margin:5px"> सुरु भएको छैन </div>';
          } else if (value == 'Ongoing') {
            return '<div style="margin:5px">चलिरहेको</div>';
          } else if (value == 'Completed') {
            return '<div style="margin:5px">पूर्ण भएको</div>';
          } else if (value == 'Suspended') {
            return '<div style="margin:5px">निलम्बित</div>';
          } else if (value == 'Spill Over') {
            return '<div style="margin:5px">समयावधि थपिएको</div>';
          } else if (value == 'Abandoned') {
            return '<div style="margin:5px">त्यागिएको</div>';
          }
        }
      },
      { text: this.transData['AMOUNT_SPILL'], datafield: 'amount', columntype: 'textbox', editable: false, width: 100 },
      { text: this.transData['REASON_SPILL'], datafield: 'reason', columntype: 'textbox', editable: false },
    ];
    this.columngroups =
      [
        { text: this.transData['ACTION'], align: 'center', name: 'Actions' }
      ];
  }

  planYearSelected($event) {
    let post = {}
    post = this.approvalSearchForm.value
    this.SearchData(post);
  }

  SearchData(post) {
    this.jqxLoader.open();
    post['fundProvider'] = '';
    this.activityOutputService.index(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
        this.SearchDatas = [];
      } else {
        this.source.localdata = res;
        this.SearchDatas = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  Reset() {
    this.viewStatusForm.reset();
  }

  SaveExecutionStatus(post) {
    post['activity'] = this.activityId;
    let sta = post[status];
    if (this.showSpillRow) {
      if ((post['amount'] || post['amount'] == 0) && post['reason']) {
        this.jqxLoader.open();
        this.activityOutputService.storeExecutionStatus(post).subscribe((result) => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('sus');
            messageDiv.innerText = result['message'];
            this.susWin.open();
            this.loadGridData();
            this.loadStatusGrid();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('err');
            messageDiv.innerText = result['error']['message'];
            this.errWin.open();
          }
        },
          error => {
            this.jqxLoader.close();
            console.info(error);
          });
      } else {
        let messageDiv: any = document.getElementById('err');
        messageDiv.innerText = 'रकम र कारण अावश्यक छ!!';
        this.errWin.open();
      }
    } else {
      console.log("triggered");
      this.jqxLoader.open();
      this.activityOutputService.storeExecutionStatus(post).subscribe((result) => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('sus');
          messageDiv.innerText = result['message'];
          this.susWin.open();

          this.loadGridData();
          this.loadStatusGrid();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('err');
          messageDiv.innerText = result['error']['message'];
          this.errWin.open();
        }
      },
        error => {
          this.jqxLoader.close();
          console.info(error);
        });
    }
  }

  checkDataChange(post) {
    console.info(post);
    if (post == 'Spill Over' || post == 'Suspended') {
      console.log("true");
      this.showSpillRow = true;
    } else {
      console.log("false");
      this.showSpillRow = false;
    }

  }
  loadStatusGrid() {
    let dt = {};
    dt['fundProvider'] = '';
    dt['activity'] = this.activityId;
    this.activityOutputService.getExecutionStatus(dt).subscribe((res) => {
      let r = (res['data'].length) - 1;
      if (res['data'][r]['status'] == 'Completed') {
        this.workCompleted = true;
        this.workStageAdapter = [];
      } else {
        this.workCompleted = false;
      }
      this.sourceStatus.localdata = res['data'];
      this.statusGridList.updatebounddata();
      if (res['data'][r]['status'] != 'Not Started') {
        this.workStageAdapter = [

          {
            id: "1",
            name: 'Ongoing',
            nameNepali: 'चलिरहेको'
          },
          // {
          //   id: "2",
          //   name: 'Completed',
          //   nameNepali: 'पूर्ण भएको'
          // },
          {
            id: "3",
            name: 'Suspended',
            nameNepali: 'निलम्बित'
          },
          {
            id: "4",
            name: 'Spill Over',
            nameNepali: 'समयावधि थपिएको'
          },
          {
            id: "5",
            name: 'Abandoned',
            nameNepali: 'त्यागिएको'
          }
        ];
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
}
