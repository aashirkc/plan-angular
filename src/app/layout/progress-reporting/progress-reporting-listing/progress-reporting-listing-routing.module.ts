import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgressReportingListingComponent } from './progress-reporting-listing.component';

const routes: Routes = [
  {
    path: '',
    component: ProgressReportingListingComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgressReportingListingRoutingModule { }
