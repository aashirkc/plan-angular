import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ProgressReportingListingComponent } from './progress-reporting-listing.component';
import { ProgressReportingListingRoutingModule } from './progress-reporting-listing-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ProgressReportingListingRoutingModule
  ],
  declarations: [
    ProgressReportingListingComponent,
  ]
})
export class ProgressReportingListingModule { }
