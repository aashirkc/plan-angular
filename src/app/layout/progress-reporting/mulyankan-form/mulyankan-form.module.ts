import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MulyankanFormComponent } from './mulyankan-form.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { MulyankanFormRoutingModule } from './mulyankan-form-routing.module';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MulyankanFormRoutingModule,
  ],
  declarations: [MulyankanFormComponent],
  schemas:[NO_ERRORS_SCHEMA]
})
export class MulyankanFormModule { }
