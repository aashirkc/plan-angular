import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MulyankanFormComponent } from './mulyankan-form.component';

const routes: Routes = [
  {
    path: '',
    component:MulyankanFormComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MulyankanFormRoutingModule { }