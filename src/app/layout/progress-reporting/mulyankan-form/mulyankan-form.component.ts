import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentUserService, UnicodeTranslateService, AllReportService, ProgressReportingTippaniAadeshService, } from '../../../shared';
import { DateConverterService } from '../../../shared';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';

@Component({
  selector: 'app-mulyankan-form',
  templateUrl: './mulyankan-form.component.html',
  styleUrls: ['./mulyankan-form.component.scss']
})
export class MulyankanFormComponent implements OnInit {

  date: any;
  progressData: any;
  tippaniData: any;
  heading: Array<any> = [];
  totalAmount
  organizationMasterData: any;
  workOutputAssetForm: FormGroup;
  DetailsData: Array<any> = [];
  TableData: Array<any> = [];

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private dcs: DateConverterService,
    private unicode: UnicodeTranslateService,
    private fb: FormBuilder,
    private ars: AllReportService,

  ) {
    this.workOutputAssetForm = this.fb.group({
      'id': [''],
      'programName': [''],
      'topicName': [''],
      'activityName': [''],
      'place': [''],
      'consumerCommitteeName': [''],
      'adhaxya': [''],
      'contactNumber': [''],
      'agreementDate': [''],
      'startDate': [''],
      'completionDate': [''],
      'reason': [''],
      'totalLaie': [''],
      'nagarpalikaCost': [''],
      'participationCost': [''],
      'otherCost': [''],
      'totalEvaluation': [''],
      'bhuktani': [''],
      
      'programMulyankan': [''],
      'topicMulyankan': [''],
      'activityMulyankan': [''],
      'placeMulyankan': [''],
      'consumerMulyankan': [''],
      'adhaxyaMulyankan': [''],
      'contactMulyankan': [''],
      'startMulyankan': [''],
      'agreementMulyankan': [''],
      'completionMulyankan': [''],
      'reasonMulyankan': [''],
      'totalMulyankan': [''],
      'nagarpalikaMulyankan': [''],
      'participationMulyankan': [''],
      'otherMulyankan': [''],
      'totalEvaluationMulyankan': [''],
      'bhuktaniMulyankan': [''],

    });
    this.date = this.dcs.getToday()['fulldate'];




  }

  ngAfterViewInit() {
    this.unicode.initUnicode();
   
  }

  ngOnInit() {
    this.ars.getOrg().subscribe((res) => {
      this.progressData = res;
      if (this.progressData.length <= 0) {
        let messageDiv: any = document.getElementById('noRecord');
        messageDiv.innerText = 'कुनै विवरण भेटिएन';
      }

    }, (error) => {
      console.info(error);

    });
    this.date = this.dcs.getToday()['fulldate'];
    let data = this.activeRoute.snapshot.params;
    let idData = this.activeRoute.snapshot.params['singleid'];
    let oType = this.activeRoute.snapshot.params['outputType']; 
    let post={type:oType}
    this.ars.getPhysicalDetails(idData,post).subscribe((res) => {
      this.DetailsData = res['data'] || '';
      this.TableData = res['OtherData'] || '';
      this.workOutputAssetForm.controls['activityName'].patchValue(this.TableData['activityName'] || '') ;
      this.workOutputAssetForm.controls['place'].patchValue(this.TableData['activityAddress'] || '') ;
      this.workOutputAssetForm.controls['place'].patchValue(this.TableData['activityAddress'] || '') ;
      this.workOutputAssetForm.controls['programName'].patchValue(this.TableData['programName'] || '') ;   
      this.workOutputAssetForm.controls['consumerCommitteeName'].patchValue(this.TableData['consumerCommitteName'] || '') ;       
      this.workOutputAssetForm.controls['adhaxya'].patchValue(this.TableData['adhaxya'] || '') ;  
      this.workOutputAssetForm.controls['contactNumber'].patchValue(this.TableData['contactNumber'] || '') ;       
      this.workOutputAssetForm.controls['nagarpalikaCost'].patchValue(this.TableData['fromLocalLevel'] || '') ;       
      this.workOutputAssetForm.controls['otherCost'].patchValue(this.TableData['fromOthers'] || '') ;       
      this.workOutputAssetForm.controls['totalEvaluation'].patchValue(this.TableData['totalGranted'] || '') ;       
      this.workOutputAssetForm.controls['totalLaie'].patchValue(this.TableData['techCost'] || '') ;       
      this.workOutputAssetForm.controls['agreementDate'].patchValue(this.TableData['contractDate'] || '') ;       
      this.workOutputAssetForm.controls['startDate'].patchValue(this.TableData['startDate'] || '') ;       
      this.workOutputAssetForm.controls['completionDate'].patchValue(this.TableData['completionDate'] || '') ;       
    
      if (res.length <= 0) {
        let messageDiv: any = document.getElementById('noRecord');
        messageDiv.innerText = 'कुनै विवरण भेटिएन';
      }

    }, (error) => {
      console.info(error);

    });
  }

  close() {
    this.router.navigate(['/progress-reporting/progress-reporting-listing']);
  }

  print() {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
  <html>
    <head>
      <title>टिप्पणी आदेश</title>
      <style>
      #showDiv{
        display: inline-block !important;
      }
      #hideDiv{
        display: none !important;
      }
      .showPrint{
        display: inline-block !important;
      }
      .hidePrint{
        display: none !important;
      }
      @font-face {
        font-family: 'preeti';
        src: url('assets/css/Preeti.TTF');
        font-weight: normal;
        font-style: normal;
    }
       .preeti{
        font-family:'preeti';
        font-size:18px!important;
        font-size: 24px !important;
       }
      </style>
    </head>
  <body onload="window.print();window.close()">${printContents}</body>
  </html>`
    );
    popupWin.document.close();
  
}
}
