import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgressReportingComponent } from './progress-reporting.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { ProgressReportingRoutingModule } from './progress-reporting-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ProgressReportingRoutingModule
  ],
  declarations: [ProgressReportingComponent,],
  schemas:[NO_ERRORS_SCHEMA]
})
export class ProgressReportingModule { }
