import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgressReportingPhysicalProgressComponent } from './progress-reporting-physical-progress.component';

const routes: Routes = [
  {
    path: '',
    component: ProgressReportingPhysicalProgressComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgressReportingPhysicalProgressRoutingModule { }
