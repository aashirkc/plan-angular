import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { CurrentUserService, ProgressReportingService, DateConverterService, ActivityOutputService, ServiceDetailsService, CapacityBuildingService, WorkOutputAssetService, CustomValidators } from '../../../shared';
import { TranslateService } from '@ngx-translate/core';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
// import { l } from '@angular/core/src/render3';
import { Location } from '@angular/common';


@Component({
  selector: 'app-progress-reporting-physical-progress',
  templateUrl: './progress-reporting-physical-progress.component.html',
  styleUrls: ['./progress-reporting-physical-progress.component.scss']
})
export class ProgressReportingPhysicalProgressComponent implements OnInit {

  @ViewChild('viewWindow') viewWindow: jqxWindowComponent;
  @ViewChild('dataView') dataView: jqxWindowComponent;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  // @ViewChild('myExcelGrid') myExcelGrid: jqxGridComponent;
  // @ViewChild('myExcelWoaGrid') myExcelWoaGrid: jqxGridComponent;

  // For Service Details
  columnsExcel: any[] = [];
  datafields: any[] = [];
  woaData: any;
  sdData: any;
  cbData: any
  getUnit: any;
  sourceExcel: any =
    {
      localdata: [],
      unboundmode: true,
      totalrecords: 50,
      datafields: this.datafields
    }
  workCompletionExcelSelected: any = [];
  workCompletionExcelSelectedCb: any = [];
  documnet: any;
  documnetCb: any;
  dataAdapterExcel: any = new jqx.dataAdapter(this.sourceExcel);
  numberrenderer = (row: number, column: any, value: any): string => {
    return '<div style="text-align: center; margin-top: 5px;">' + (1 + value) + '</div>';
  }
  generateData(): void {
    for (let i = 0; i < 26; i++) {
      let text = String.fromCharCode(65 + i);
      if (i == 0) {
        let cssclass = 'jqx-widget-header';
        this.columnsExcel[this.columnsExcel.length] = { pinned: true, exportable: false, text: '', columntype: 'number', cellclassname: cssclass, cellsrenderer: this.numberrenderer };
      }
      this.datafields[this.datafields.length] = { name: text };
      this.columnsExcel[this.columnsExcel.length] = { text: text, datafield: text, width: 60, align: 'center' };
    }
  }
  // For WOA
  columnsExcelWoa: any[] = [];
  datafieldsWoa: any[] = [];
  sourceExcelWoa: any =
    {
      localdata: [],
      unboundmode: true,
      totalrecords: 50,
      datafields: this.datafieldsWoa
    }
  workCompletionExcelSelectedWoa: any = [];
  documnetWoa: any;
  dataAdapterExcelWoa: any = new jqx.dataAdapter(this.sourceExcelWoa);
  numberrendererWoa = (row: number, column: any, value: any): string => {
    return '<div style="text-align: center; margin-top: 5px;">' + (1 + value) + '</div>';
  }
  generateDataWoa(): void {
    for (let i = 0; i < 26; i++) {
      let text = String.fromCharCode(65 + i);
      if (i == 0) {
        let cssclass = 'jqx-widget-header';
        this.columnsExcelWoa[this.columnsExcelWoa.length] = { pinned: true, exportable: false, text: '', columntype: 'number', cellclassname: cssclass, cellsrenderer: this.numberrendererWoa };
      }
      this.datafieldsWoa[this.datafieldsWoa.length] = { name: text };
      this.columnsExcelWoa[this.columnsExcelWoa.length] = { text: text, datafield: text, width: 60, align: 'center' };
    }
  }

  outputTypeForm: FormGroup;
  workOutputAssetForm: FormGroup;
  capacityBuildingForm: FormGroup;
  serviceDetailsForm: FormGroup;

  updated: boolean = false;
  transData: any;
  userData: any = {};
  API_URL_DOC: any;
  API_URL: any;
  ViewData: any;
  activityType: any;
  selectedFiles: any;
  fileSelect: any;
  quantityAdapter: any;
  quantityCheker: any = 100;
  capacityBuildingData: any;
  capacityEditData: any = [];
  fileData: any = [];
  // sdData: any = [];
  // cbData: any = [];
  detailData: any = [];
  isCompleted: boolean = false;
  showExcelSheet: boolean = false;
  showExcelSheetWoa: boolean = false;
  showExcelSheetCb: boolean = false;
  oType: any;
  fullDate: string;
  serviceDataAdapter: any = [
    {
      id: "1",
      name: 'Sample Service'
    }
  ];

  organaizeByAdapter: any = [
    {
      id: "1",
      name: 'Administrator'
    },
    {
      id: "1",
      name: 'Nawin Ghimire'
    }
  ];

  batchesDataAdapter: any = [
    {
      id: "Yes",
      name: 'Yes'
    },
    {
      id: "No",
      name: 'No'
    }
  ];
  locationAdapter: any = [
    {
      id: "1",
      name: 'Chabahil'
    }
  ];
  workStageAdapter: any = [
    {
      id: "0",
      name: 'Not Started',
      dname: 'सुरु भएको छैन'
    },
    {
      id: "1",
      name: 'Ongoing',
      dname: 'चलिरहेको'
    },
    {
      id: "2",
      name: 'Completed',
      dname: 'पूर्ण भएको'
    },
    {
      id: "3",
      name: 'Suspended',
      dname: 'रोक्का गरिएको'
    },
    {
      id: "4",
      name: 'Spill Over',
      dname: 'समयावधि थप्नु पर्ने'
    },
    {
      id: "5",
      name: 'Abandoned',
      dname: 'त्याग गरेको'
    }
  ];
  assetSubCategoryAdapter: any = [
    {
      id: "1",
      name: 'Activity Sub Category'
    }
  ];
  unitParameterAdapter: any = [
    {
      id: "1",
      name: 'Meter / Kilogram'
    }
  ];
  assetUnitTypeAdapter: any = [
    {
      id: "1",
      name: 'General Type'
    }
  ];

  outputTypeAdapter: any = [
    {
      name: 'Service Details',
      value: 'SD'
    },
    {
      name: 'Capacity Buildings',
      value: 'CB'
    },
    {
      name: 'Work Output Assets',
      value: 'WOA'
    }
  ];
  source: any;
  dataAdapter: any;
  columns: any[] = [];
  editrow: number = -1;
  columngroups: any[];
  acData: any;
  investigationStageAdapter: Array<any> = [];
  tempInvestigationStage: Array<any> = [];

  constructor(
    private cus: CurrentUserService,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    @Inject('API_URL') API_URL: string,
    private cdr: ChangeDetectorRef,
    private activeRoute: ActivatedRoute,
    private avs: ActivityOutputService,
    private fb: FormBuilder,
    private router: Router,
    private cbs: CapacityBuildingService,
    private dcs: DateConverterService,
    private wos: WorkOutputAssetService,
    private serviceDetails: ServiceDetailsService,
    private progressReport: ProgressReportingService,
    private translate: TranslateService,
    private location: Location,
  ) {
    this.getTranslation();
    this.createForm();
    this.userData = this.cus.getTokenData();
    this.API_URL_DOC = API_URL_DOC;
    this.API_URL = API_URL;
  }


  ngOnInit() {
    let userDetalils = JSON.parse(localStorage.getItem('pcUser'));
    this.userData = userDetalils['userType'];
    this.generateData();
    this.generateDataWoa();
    let data = this.activeRoute.snapshot.params;
    this.acData = this.activeRoute.snapshot.params['id'];
    this.oType = this.activeRoute.snapshot.params['outputType'];
    this.avs.showActivity(data['id']).subscribe((res) => {
      this.ViewData = res;
      this.isCompleted = res['executionReportStatus'] == "Completed" ? true : false;
    }, (error) => {
      console.info(error);
    });
    // this.avs.indexUnit({ activity: data['id'] }).subscribe((res) => {
    //   this.getUnit = res['unit'] || '';
    //   // this.isCompleted = res['executionReportStatus'] == "Completed" ? true : false;
    // }, (error) => {
    //   console.info(error);
    // });
    let currentYear = new Date().getFullYear();
    let month = new Date().getMonth() + 1;
    let day = new Date().getDate();
    let engDate = currentYear + '-' + month + '-' + day;
    let date = this.dcs.ad2Bs(engDate);
    this.fullDate = date['fulldate'];
    this.workOutputAssetForm.controls['enterDate'].patchValue(this.fullDate);
    this.workOutputAssetForm.controls['enterDate'].updateValueAndValidity();
    this.capacityBuildingForm.controls['enterDate'].patchValue(this.fullDate);
    this.capacityBuildingForm.controls['enterDate'].updateValueAndValidity();
    this.serviceDetailsForm.controls['enterDate'].patchValue(this.fullDate);
    this.serviceDetailsForm.controls['enterDate'].updateValueAndValidity();
  }

  patchDate(){
    let currentYear = new Date().getFullYear();
    let month = new Date().getMonth() + 1;
    let day = new Date().getDate();
    let engDate = currentYear + '-' + month + '-' + day;
    let date = this.dcs.ad2Bs(engDate);
    this.fullDate = date['fulldate'];
    this.workOutputAssetForm.controls['enterDate'].patchValue(this.fullDate);
    this.workOutputAssetForm.controls['enterDate'].updateValueAndValidity();
    this.capacityBuildingForm.controls['enterDate'].patchValue(this.fullDate);
    this.capacityBuildingForm.controls['enterDate'].updateValueAndValidity();
    this.serviceDetailsForm.controls['enterDate'].patchValue(this.fullDate);
    this.serviceDetailsForm.controls['enterDate'].updateValueAndValidity();
  }

  getTranslation() {
    this.translate.get(['SN', 'DETAIL_VIEW', 'DESCRIPTION', 'CURRENT_QUANTITY', 'UNIT', "ACTION", "EDIT", "DELETE_ROW", 'QUANTITY']).subscribe((translation: [string]) => {
      this.transData = translation;
      this.loadGrid();
    });
  }

  createForm() {
    this.outputTypeForm = this.fb.group({
      'outputType': ['', Validators.required],
    });

    this.workOutputAssetForm = this.fb.group({
      'enterDate': ['', Validators.compose([Validators.required, CustomValidators.checkDate])],
      'quantity': ['', Validators.compose([Validators.max(100), Validators.required])],
      'monitoringDetails': [''],
      'status': [''],
      'investigationStage': ['', Validators.required],
      'workCompletedStatement': [''],
      'flage': ['No', Validators.required],
      'id': [''],
      documentArray: this.fb.array([
        this.documentArray()
      ]),
    });

    this.capacityBuildingForm = this.fb.group({
      'bcdNow': ['', Validators.required],
      'enterDate': ['', Validators.compose([Validators.required, CustomValidators.checkDate])],
      'flag': ['',],
      'investigationStage': ['No'],
      'workCompletedStatement': [''],
      'monitoringDetails': [''],
      'id': [''],
    });

    this.serviceDetailsForm = this.fb.group({
      'enterDate': ['', Validators.compose([Validators.required, CustomValidators.checkDate])],
      'flage': ['No', Validators.required],
      'monitoringDetails': [''],
      'personBeniefiled': ['', Validators.required],
      'id': [''],
      'investigationStage': ['No'],
      'workCompletedStatement': ['']
    });

    // console.log(this.capacityBuildingForm);
    // console.log(this.serviceDetailsForm);
    // console.log(this.workOutputAssetForm);
  }

  documentArray() {
    return this.fb.group({
      'imageTitle': [' '],
    });
  }

  addDocumentArray() {
    const formControl = <FormArray>this.workOutputAssetForm.controls["documentArray"];
    formControl.push(this.documentArray());
  }

  removeDocumentArray(i) {
    const formControl = <FormArray>this.workOutputAssetForm.controls["documentArray"];
    formControl.removeAt(i);

    // this.selectedWOAFiles[i] = event.target.files[0];
    if (this.selectedFiles[i]) {
      this.selectedFiles.splice(i, 1);
    }

    // console.log(this.selectedFiles);
  }


  ngAfterViewInit() {
    let routeParams = this.activeRoute.snapshot.params;
    this.activityType = routeParams['outputType'] || '';
    this.loadTableData();
    this.serviceDetailsForm.controls['flage'].setValue('No');
    let data = this.activeRoute.snapshot.params;
    let dt = {};
    dt['activity'] = data['id'];
    this.loadGridData(dt);
    this.serviceDetailsForm.valueChanges.subscribe(val => {
      if (val['flage'] == 'Yes') {
        this.showExcelSheet = true;
      } else if (val['flage'] == 'No') {
        this.showExcelSheet = false;
      }
    });
    this.workOutputAssetForm.valueChanges.subscribe(val => {
      if (val['flage'] == 'Yes') {
        this.showExcelSheetWoa = true;
      } else if (val['flage'] == 'No') {
        this.showExcelSheetWoa = false;
      }
    });
    this.capacityBuildingForm.valueChanges.subscribe(val => {
      if (val['flag'] == 'Yes') {
        this.showExcelSheetCb = true;
      } else if (val['flagg'] == 'No') {
        this.showExcelSheetCb = false;
      }
    });
    this.cdr.detectChanges();
  }

  forward() {
    let data = this.activeRoute.snapshot.params;
    localStorage.setItem('outputType', data['outputType']);
    this.router.navigate(['/progress-reporting/progress-reporting-financial-progress/', data['id']]);
  }

  Close() {
    this.router.navigate(['/progress-reporting/progress-reporting-listing']);
    // this.location.back();
  }

  handleFileInput(event) {
    this.selectedFiles = event.target.files;
  }

  // selectedWOAFiles: any = [];
  // handleWOAFileInput(event, i) {
  //   console.log(event);
  //   if (event.target.files && event.target.files[0]) {
  //     this.selectedWOAFiles[i] = event.target.files[0];
  //   } else {
  //     this.selectedWOAFiles.splice(i, 0);
  //   }

  // }

  unitShow: any;

  loadTableData() {
    this.jqxLoader.open();
    let data = this.activeRoute.snapshot.params;
    if (this.activityType == 'CB') {
      let dt = {};
      dt['activity'] = data['id'];
      this.progressReport.getPhysicalProgress(dt).subscribe((res) => {
        this.capacityBuildingData = res;
        this.investigationStage();
        this.quantityAdapter = this.capacityBuildingData["quantity"]
        // console.log(this.quantityAdapter)
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
    }
    if (this.activityType == 'SD') {
      let dt = {};
      dt['activity'] = data['id'];
      this.progressReport.getServiceProgress(dt).subscribe((res) => {
        this.capacityBuildingData = res;
        this.investigationStage();
        this.quantityAdapter = this.capacityBuildingData["quantity"]
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
    }
    if (this.activityType == 'WOA') {
      let dt = {};
      dt['activity'] = data['id'];
      this.progressReport.getPhysicalAssetProgress(dt).subscribe((res) => {
        this.capacityBuildingData = res;
        this.investigationStage();
        this.quantityCheker = this.checkValidator();
        this.workOutputAssetForm.controls['quantity'].setValidators(
          Validators.compose([Validators.required, Validators.max(this.quantityCheker)])
        );
        this.workOutputAssetForm.controls['quantity'].updateValueAndValidity();
        // this.quantityAdapter=this.capacityBuildingData["quantity"]
        // console.log(this.quantityAdapter)
        this.unitShow = res && res[0] && res[0].unit;
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
    }
  }

  investigationStage(){
    this.avs.getInvestigationStage().subscribe((res) => {
      const record = res['Record'];
      const adapter = record.sort((pres, next) => (pres.priority > next.priority ? 1: -1));
      this.investigationStageAdapter = adapter;
      let temp = this.capacityBuildingData[this.capacityBuildingData.length - 1].investigationStage['priority']
      this.tempInvestigationStage = this.investigationStageAdapter.filter(x => {
        return (x.priority >= Number(temp))
      })
      this.investigationStageAdapter = this.investigationStageAdapter.filter(x => {
        return (x.priority > Number(temp))
      })
      // console.log(this.investigationStageAdapter)
    }, (error) => {
      console.info(error);
    });
  }

  private checkValidator() {
    // console.log(this.capacityBuildingData);
    let sum = 0;
    for (let i = 0; i < this.capacityBuildingData.length; i++) {
      sum = sum + (this.capacityBuildingData[i]['quantity'] || 0);
      this.quantityAdapter = sum;
    }
    // change this to validate the maximum value available
    this.quantityCheker = 100 - (this.quantityAdapter || 0);
    return this.quantityCheker;
  }

  public fileEventM(event: any) {
    const fileSelected: File = event.target.files;
    this.workCompletionExcelSelected = fileSelected;
  }
  public fileEventMWoa(event: any) {
    this.workCompletionExcelSelectedWoa = event.target.files;
    // this.workCompletionExcelSelectedWoa = fileSelected;
  }
  public fileEventMCb(event: any) {
    const fileSelected: File = event.target.files;
    this.workCompletionExcelSelectedCb = fileSelected;
  }

  Save(post) {
    console.log(post)
    if (this.userData.toLowerCase() == 'admin' || this.userData.toLowerCase() == 'super admin' || this.userData.toLowerCase() == 'activity manager') {
      let routeParams = this.activeRoute.snapshot.params;
      const _formData = new FormData();
      _formData.append('activity', routeParams['id']);
      for (let data in post) {
        _formData.append(data, post[data]);
      }

      // Save service Details
      if (this.activityType == 'SD') {
        if (this.selectedFiles) {
          for (let i = 0; i < this.selectedFiles.length; i++) {
            let fileIndex = i + 1;
            _formData.append('images', this.selectedFiles[i], this.selectedFiles[i].name)
          }
        }
        if (routeParams['id']) {
          if (post['flage'] == 'Yes') {
            // let dataExcel = this.myExcelGrid.getrows();
            let arrayCollections = [];
            // for (let i = 0; i < dataExcel.length; i++) {
            //   if (dataExcel[i]['A'] == "" && dataExcel[i]['B'] == "" && dataExcel[i]['C'] == "" && dataExcel[i]['D'] == "" && dataExcel[i]['E'] == "" && dataExcel[i]['F'] == "" && dataExcel[i]['G'] == "" && dataExcel[i]['H'] == "" && dataExcel[i]['I'] == "" && dataExcel[i]['J'] == "" && dataExcel[i]['K'] == "" && dataExcel[i]['L'] == "" && dataExcel[i]['M'] == "" && dataExcel[i]['N'] == "" && dataExcel[i]['O'] == "" && dataExcel[i]['P'] == "" && dataExcel[i]['Q'] == "" && dataExcel[i]['R'] == "" && dataExcel[i]['S'] == "" && dataExcel[i]['T'] == "" && dataExcel[i]['U'] == "" && dataExcel[i]['V'] == "" && dataExcel[i]['W'] == "" && dataExcel[i]['X'] == "" && dataExcel[i]['Y'] == "" && dataExcel[i]['Z'] == "") {

            //   } else {
            //     arrayCollections.push(dataExcel[i]);
            //   }
            // }
            // _formData.append('document', this.workCompletionExcelSelected, this.workCompletionExcelSelected && this.workCompletionExcelSelected.name);
            if (this.workCompletionExcelSelected) {
              for (let i = 0; i < this.workCompletionExcelSelected.length; i++) {
                let fileIndex = i + 1;
                _formData.append('document', this.workCompletionExcelSelected[i], this.workCompletionExcelSelected[i].name)
              }
            }
            _formData.append('excelData', JSON.stringify(arrayCollections));
          }
          this.jqxLoader.open();
          this.progressReport.storeServiceProgress(_formData).subscribe(
            result => {
              if (result['message']) {
                this.loadTableData();
                let messageDiv: any = document.getElementById('message');
                messageDiv.innerText = result['message'];
                this.msgNotification.open();
                this.Reset();
              }
              this.jqxLoader.close();
              if (result['error']) {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
              }
            },
            error => {
              this.jqxLoader.close();
              console.info(error);
            }
          );
        } else {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = 'Select Data..!!';
          this.errNotification.open();
        }
      }

      // Save Capacity Building
      if (this.activityType == 'CB') {
        if (this.selectedFiles) {
          for (let i = 0; i < this.selectedFiles.length; i++) {
            let fileIndex = i + 1;
            _formData.append('images', this.selectedFiles[i], this.selectedFiles[i].name)
          }
        }
        if (routeParams['id']) {
          if (post['flag'] == 'Yes') {
            // let dataExcel = this.myExcelGrid.getrows();
            let arrayCollections = [];
            // for (let i = 0; i < dataExcel.length; i++) {
            //   if (dataExcel[i]['A'] == "" && dataExcel[i]['B'] == "" && dataExcel[i]['C'] == "" && dataExcel[i]['D'] == "" && dataExcel[i]['E'] == "" && dataExcel[i]['F'] == "" && dataExcel[i]['G'] == "" && dataExcel[i]['H'] == "" && dataExcel[i]['I'] == "" && dataExcel[i]['J'] == "" && dataExcel[i]['K'] == "" && dataExcel[i]['L'] == "" && dataExcel[i]['M'] == "" && dataExcel[i]['N'] == "" && dataExcel[i]['O'] == "" && dataExcel[i]['P'] == "" && dataExcel[i]['Q'] == "" && dataExcel[i]['R'] == "" && dataExcel[i]['S'] == "" && dataExcel[i]['T'] == "" && dataExcel[i]['U'] == "" && dataExcel[i]['V'] == "" && dataExcel[i]['W'] == "" && dataExcel[i]['X'] == "" && dataExcel[i]['Y'] == "" && dataExcel[i]['Z'] == "") {

            //   } else {
            //     arrayCollections.push(dataExcel[i]);
            //   }
            // }
            // _formData.append('document', this.workCompletionExcelSelectedCb, this.workCompletionExcelSelectedCb && this.workCompletionExcelSelectedCb.name);
            if (this.workCompletionExcelSelectedCb) {
              for (let i = 0; i < this.workCompletionExcelSelectedCb.length; i++) {
                let fileIndex = i + 1;
                _formData.append('document', this.workCompletionExcelSelectedCb[i], this.workCompletionExcelSelectedCb[i].name)
              }
            }
            _formData.append('excelData', JSON.stringify(arrayCollections));
          }
          this.jqxLoader.open();
          this.progressReport.storePhysicalProgress(_formData).subscribe(
            result => {
              if (result['message']) {
                this.loadTableData();
                let messageDiv: any = document.getElementById('message');
                messageDiv.innerText = result['message'];
                this.msgNotification.open();
                this.Reset();
              }
              this.jqxLoader.close();
              if (result['error']) {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
              }
            },
            error => {
              this.jqxLoader.close();
              console.info(error);
            }
          );
        } else {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = 'Select Data..!!';
          this.errNotification.open();
        }
      }

      // For workoutput asset
      if (this.activityType == 'WOA') {
        if (this.selectedFiles) {
          for (let i = 0; i < this.selectedFiles.length; i++) {
            let fileIndex = i + 1;
            _formData.append('images', this.selectedFiles[i], this.selectedFiles[i].name);
          }
        }

        if(post['documentArray']){
          for (let i = 0; i < post['documentArray'].length; i++) {
            _formData.append('imageTitle', post['documentArray'][i].imageTitle);
          }
        }

        delete post['documentArray'];
        if (routeParams['id']) {
          if (post['flage'] == 'Yes') {
            // let dataExcel = this.myExcelWoaGrid.getrows();
            let arrayCollections = [];
            // for (let i = 0; i < dataExcel.length; i++) {
            //   if (dataExcel[i]['A'] == "" && dataExcel[i]['B'] == "" && dataExcel[i]['C'] == "" && dataExcel[i]['D'] == "" && dataExcel[i]['E'] == "" && dataExcel[i]['F'] == "" && dataExcel[i]['G'] == "" && dataExcel[i]['H'] == "" && dataExcel[i]['I'] == "" && dataExcel[i]['J'] == "" && dataExcel[i]['K'] == "" && dataExcel[i]['L'] == "" && dataExcel[i]['M'] == "" && dataExcel[i]['N'] == "" && dataExcel[i]['O'] == "" && dataExcel[i]['P'] == "" && dataExcel[i]['Q'] == "" && dataExcel[i]['R'] == "" && dataExcel[i]['S'] == "" && dataExcel[i]['T'] == "" && dataExcel[i]['U'] == "" && dataExcel[i]['V'] == "" && dataExcel[i]['W'] == "" && dataExcel[i]['X'] == "" && dataExcel[i]['Y'] == "" && dataExcel[i]['Z'] == "") {

            //   } else {
            //     arrayCollections.push(dataExcel[i]);
            //   }
            // }
            if (this.workCompletionExcelSelectedWoa) {
              for (let i = 0; i < this.workCompletionExcelSelectedWoa.length; i++) {
                let fileIndex = i + 1;
                _formData.append('document', this.workCompletionExcelSelectedWoa[i], this.workCompletionExcelSelectedWoa[i].name)
              }
            }
            // _formData.append('document', this.workCompletionExcelSelectedWoa, this.workCompletionExcelSelectedWoa && this.workCompletionExcelSelectedWoa.name);
            _formData.append('excelData', JSON.stringify(arrayCollections));
          }
          this.jqxLoader.open();
          this.progressReport.storePhysicalAssetProgress(_formData).subscribe(
            result => {
              if (result['message']) {
                this.loadTableData();
                let messageDiv: any = document.getElementById('message');
                messageDiv.innerText = result['message'];
                this.msgNotification.open();
                this.Reset();
              }
              this.jqxLoader.close();
              if (result['error']) {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
              }
            },
            error => {
              this.jqxLoader.close();
              console.info(error);
            }
          );
        } else {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = 'Select Data..!!';
          this.errNotification.open();
        }
      }
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'तपाईलाई यो कार्य गर्न स्वीकृत दिएको छैन !!!!';
      this.errNotification.open();
    }
  }

  Reset() {
    const forms = document.getElementsByClassName("physical_progress_form");
    for(let i = 0; i < forms.length; i++){
      const form = forms[i] as HTMLFormElement;
      form.reset();
    }
    if (this.activityType == 'SD') {
      this.serviceDetailsForm.reset();
      this.capacityEditData = [];
      this.selectedFiles = [];
      this.fileData = [];
      this.documnet = '';
      this.sourceExcel.localdata = [];
      this.serviceDetailsForm.controls['workCompletedStatement'].reset('');
      this.serviceDetailsForm.controls['monitoringDetails'].reset('');
      this.serviceDetailsForm.controls['flage'].patchValue('No');
      this.workOutputAssetForm.controls['id'].patchValue('');
      this.serviceDetailsForm.markAsDirty();
      console.log(this.serviceDetailsForm)
      // this.myExcelGrid.updatebounddata();
    }
    if (this.activityType == 'CB') {
      this.capacityBuildingForm.reset();
      this.capacityEditData = [];
      this.selectedFiles = [];
      this.fileData = [];

      this.documnetCb = '';
      this.sourceExcel.localdata = [];
      this.capacityBuildingForm.controls['workCompletedStatement'].reset('');
      this.capacityBuildingForm.controls['monitoringDetails'].reset('');
      this.capacityBuildingForm.controls['flag'].patchValue('No');
      this.workOutputAssetForm.controls['id'].patchValue('');
      this.capacityBuildingForm.markAsDirty();
      console.log(this.capacityBuildingForm)
      // this.myExcelGrid.updatebounddata();
    }
    if (this.activityType == 'WOA') {
      this.workOutputAssetForm.setControl('documentArray', this.fb.array([]));
      this.addDocumentArray();
      this.workOutputAssetForm.reset();
      this.capacityEditData = [];
      this.selectedFiles = [];
      this.fileData = [];

      this.documnetWoa = '';
      this.sourceExcelWoa.localdata = [];
      this.workOutputAssetForm.controls['workCompletedStatement'].reset('');
      this.workOutputAssetForm.controls['monitoringDetails'].reset('');
      this.checkValidator();
      this.workOutputAssetForm.controls['quantity'].setValidators(
        Validators.compose([Validators.required, Validators.max(this.quantityCheker)])
      );
      this.workOutputAssetForm.controls['flage'].patchValue('No');
      this.workOutputAssetForm.controls['id'].patchValue('');
      this.workOutputAssetForm.markAsDirty();
      console.log(this.workOutputAssetForm);
      // this.myExcelWoaGrid.updatebounddata();
    }

    this.patchDate();
    const input = document.getElementsByClassName('wcstatement');
    if(input){
      for(let i = 0; i < input.length; i++){
        input[i]['value'] = '';
      }
    }
  }

  setData(data) {
    this.router.navigate(['/progress-reporting/anugaman-pratibedan/', { singleid: data['id'], id: this.acData, outputType: this.oType, stage: data['investigationStage'].name, }]);
  }
  // setMulyankan(data){
  //   console.log("entered");
  //   console.log(data);
  //   this.router.navigate(['/progress-reporting/mulyankan-form/', { singleid: data['id'], id: this.acData, outputType: this.oType }]);
  // }

  setCapacityEditData(data) {
    this.investigationStageAdapter = this.tempInvestigationStage;
    if (this.activityType == 'CB') {
      this.jqxLoader.open();
      let editData = data;
      if (data['flag'] == 'Yes') {
        this.documnetCb = data['document'];
        this.showExcelSheetCb = true;

        // setTimeout(() => {
        // let array = JSON.parse(data['excelData']);
        // this.sourceExcel.localdata = array;
        // this.myExcelGrid.updatebounddata();
        // }, 300);
      }

      this.capacityEditData = data['images'];
      this.fileData = data['document'];
      let formData = {};
      formData['bcdNow'] = data['bcdNow'];
      formData['monitoringDetails'] = data['monitoringDetails'];
      formData['workCompletedStatement'] = data['workCompletedStatement'];
      formData['investigationStage'] = data['investigationStage'] && data['investigationStage'].id;
      formData['enterDate'] = data['enterDate'];
      formData['flag'] = data['flag'] || 'No';
      formData['id'] = data['id'];
      this.capacityBuildingForm.patchValue(formData);
      this.jqxLoader.close();
    }
    if (this.activityType == 'SD') {
      this.jqxLoader.open();
      let editData = data;
      if (data['flag'] == 'Yes') {
        this.documnet = data['document'];
        this.showExcelSheet = true;

        // setTimeout(() => {
        //   let array = JSON.parse(data['excelData']);
        //   this.sourceExcel.localdata = array;
        //   this.myExcelGrid.updatebounddata();
        // }, 300);
      }


      this.capacityEditData = data['progressServiceImage'];
      this.fileData = data['document'];
      console.log(data);
      let formData = {};
      formData['personBeniefiled'] = data['personBenifited'];
      formData['enterDate'] = data['enterDate'];
      formData['flage'] = data['flag'] || 'No';
      formData['monitoringDetails'] = data['monitoringDetails'];
      formData['id'] = data['id'];
      formData['investigationStage'] = data['investigationStage'] && data['investigationStage'].id;
      formData['workCompletedStatement'] = data['workCompletedStatement'];
      this.serviceDetailsForm.patchValue(formData);
      console.log(this.serviceDetailsForm);
      this.serviceDetailsForm.markAsDirty();
      this.jqxLoader.close();
    }
    if (this.activityType == 'WOA') {
      this.jqxLoader.open();
      let editData = data;
      if (data['flage'] == 'Yes') {
        this.documnetWoa = data['document'];
        this.showExcelSheetWoa = true;
        // setTimeout(() => {
        //   let array = JSON.parse(data['excelData']);
        //   this.sourceExcelWoa.localdata = array;
        //   this.myExcelWoaGrid.updatebounddata();
        // }, 300);
      }

      this.fileData = data['workCompletionDocument'];
      this.capacityEditData = data['images'];
      // console.log(this.capacityEditData);
      let formData = {};
      formData['quantity'] = data['quantity'];
      formData['investigationStage'] = data['investigationStage'] && data['investigationStage'].id;
      formData['status'] = data['status'];
      formData['monitoringDetails'] = data['monitoringDetails'];
      formData['workCompletedStatement'] = data['workCompletedStatement'];
      formData['enterDate'] = data['enterDate'];
      formData['flage'] = data['flage'] || 'No';
      formData['id'] = data['id'];
      
      // get data from table and add the current percentage to remaining percentage
      const remainingPercentage = this.checkValidator();
      this.quantityCheker = remainingPercentage + data['quantity'];
      this.workOutputAssetForm.controls['quantity'].setValidators(
        Validators.compose([Validators.required, Validators.max(100)])
      );
      
      this.workOutputAssetForm.patchValue(formData);
      this.jqxLoader.close();
    }
    const input = document.getElementsByClassName('wcstatement');
    if(input){
      for(let i = 0; i < input.length; i++){
        input[i]['value'] = data['monitoringDetails'];
      }
    }
  }

  deleteImage(image) {
    if (this.activityType == 'CB') {
      this.jqxLoader.open();
      this.progressReport.deletePhysicalProgress(image).subscribe(
        result => {
          if (result['message']) {
            let index = this.capacityEditData.indexOf(image);
            if (index > -1) {
              this.capacityEditData.splice(index, 1);
            }
            this.jqxLoader.close();
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    }
    if (this.activityType == 'SD') {
      this.jqxLoader.open();
      this.progressReport.deleteServiceProgress(image).subscribe(
        result => {
          if (result['message']) {
            let index = this.capacityEditData.indexOf(image);
            if (index > -1) {
              this.capacityEditData.splice(index, 1);
            }
            this.jqxLoader.close();
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    }
    if (this.activityType == 'WOA') {
      this.jqxLoader.open();
      this.progressReport.deletePhysicalAssetProgress(image).subscribe(
        result => {
          if (result['message']) {
            const index = this.capacityEditData.findIndex((data) => data.name === image);
            if (index > -1) {
              this.capacityEditData.splice(index, 1);
            }
            this.jqxLoader.close();
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    }
  }


  detailView() {
    this.jqxLoader.open();
    let data = this.activeRoute.snapshot.params;
    if (data['outputType'] == 'SD') {
      this.progressReport.loadSd(data['id']).subscribe((res) => {
        console.log(res[0]);
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.detailData = [];
        } else {
          console.log(res);
          this.detailData = res;
          this.dataView.open();
          this.dataView.title('विस्तारमा हेर्नुहाेस्');
        }
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
    } else if (data['outputType'] == 'CB') {
      this.progressReport.loadCb(data['id']).subscribe((res) => {
        console.log(res[0]);
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.detailData = [];
        } else {
          console.log(res);
          this.detailData = res;
          this.dataView.open();
          this.dataView.title('विस्तारमा हेर्नुहाेस्');
        }
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
    } else if (data['outputType'] == 'WOA') {
      this.progressReport.loadWoa(data['id']).subscribe((res) => {
        console.log(res[0]);
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.detailData = [];
        } else {
          console.log(res);
          this.detailData = res;
          this.dataView.open();
          this.dataView.title('विस्तारमा हेर्नुहाेस्');
        }
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
    }

  }
  loadGridData(post) {
    // this.jqxLoader.open();
    // this.avs.loadTechnicalApprovalWork(post).subscribe((res) => {
    //   console.log(res[0]);
    //   if (res.length == 1 && res[0].error) {
    //     let messageDiv: any = document.getElementById('error');
    //     messageDiv.innerText = res[0].error;
    //     this.errNotification.open();
    //     this.source.localdata = [];
    //   } else {
    //     this.source.localdata = res;
    //   }
    //   this.myGrid.updatebounddata();
    //   this.jqxLoader.close();
    // }, (error) => {
    //   console.info(error);
    //   this.jqxLoader.close();
    // });
  }
  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'workTypeId', type: 'string', map: 'id' },
          { name: 'description', type: 'string' },
          { name: 'quantity', type: 'string' },
          { name: 'unit', type: 'string' },
          { name: 'currentQuantity', type: 'string' },
        ],
        id: 'workTypeId',
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'workTypeId', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['DESCRIPTION'], datafield: 'description', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: this.transData['QUANTITY'], datafield: 'quantity', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['UNIT'], datafield: 'unit', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['CURRENT_QUANTITY'], datafield: 'currentQuantity', editable: true, width: 100, columntype: 'textbox', filtercondition: 'starts_with' },
      // {
      //   text: this.transData['ACTION'], datafield: 'action', sortable: false, filterable: false, width: 160, columntype: 'button',
      //   cellsrenderer: (row): string => {
      //     this.editrow = row;
      //    return this.transData['DETAIL_VIEW'];
      //   },
      //   buttonclick: (row: number): void => {
      //     this.editrow = row;
      //     let dataRecord = this.myGrid.getrowdata(this.editrow);
      //     this.viewWindow.open();
      //   }
      // },
    ];


  }
  viewExcelData(dataComming) {
    this.router.navigate(['/progress-reporting/physical-bill-document-print/', dataComming['id'], { outputType: this.oType }]);
  }

  // GoTothisLink() {
  //   let data = this.activeRoute.snapshot.params['id'];
  //   let dataType = this.activeRoute.snapshot.params['outputType'];
  //   this.router.navigate(['/progress-reporting/measurement-book-and-bill-of-quantity', { outputType: dataType, id: data }]);
  // }
}
