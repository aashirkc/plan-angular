import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ProgressReportingPhysicalProgressComponent } from './progress-reporting-physical-progress.component';
import { ProgressReportingPhysicalProgressRoutingModule } from './progress-reporting-physical-progress-routing.module';
import { CustomMaxDirective } from 'app/shared/validators/custom-max-validator.directive';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ProgressReportingPhysicalProgressRoutingModule
  ],
  declarations: [
      CustomMaxDirective,
    ProgressReportingPhysicalProgressComponent,
  ]
})
export class ProgressReportingPhysicalProgressModule { }
