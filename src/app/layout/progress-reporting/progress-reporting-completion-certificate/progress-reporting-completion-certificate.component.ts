import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
    ProgressReportingService,
    ProgressReportingTippaniAadeshService,
    UnicodeTranslateService,
    AllReportService,
    DateConverterService,
    PrintingService
} from '../../../shared';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
    selector: 'app-progress-reporting-completion-certificate',
    templateUrl: './progress-reporting-completion-certificate.component.html',
    styleUrls: ['./progress-reporting-completion-certificate.component.scss']
})
export class ProgressReportingCompletionCertificateComponent implements OnInit {
    date: any;
    // workOutputType: string = "नाफामुलक कार्यहरु";
    activityName: string = '';
    consumerCommitteOrContractCompany: string = '';
    workStartdate: string = '';
    workCompletionDateTime: string = '';
    heading: any;
    resultDatas: any = [];
    foForm: FormGroup;
    planYear: any;
    constructor(
        private fb: FormBuilder,
        private router: Router,
        private activeRoute: ActivatedRoute,
        private prs: ProgressReportingService,
        private unicode: UnicodeTranslateService,
        private prtas: ProgressReportingTippaniAadeshService,
        private report: AllReportService,
        private dcs: DateConverterService,
        private location: Location,
        private printingService: PrintingService
    ) {
        this.foForm = this.fb.group({
            date: [''],
            workStartdate: [''],
            workCompletionDateTime: [''],
            prasasan: [''],
            prasasanPost: [''],
            chalaniNo: ['']
        });
    }

    get prasasan() {
        return this.foForm.get('prasasan');
    }
    get prasasanPost() {
        return this.foForm.get('prasasanPost');
    }

    ngOnInit() {
        this.date = this.dcs.getToday()['fulldate'];
        this.foForm.get('date').patchValue(this.date);
        this.report.getOrg().subscribe(
            result => {
                if (result.length == 1 && result[0].error) {
                    this.heading = [];
                } else {
                    this.heading = result[0];
                    this.foForm
                        .get('prasasan')
                        .patchValue(this.heading && this.heading.nameOfHead);
                    this.foForm
                        .get('prasasanPost')
                        .patchValue(this.heading && this.heading.post);
                    this.foForm
                        .get('chalaniNo')
                        .patchValue(this.heading && this.heading.chalaniNo);
                }
            },
            error => {
                console.info(error);
            }
        );
        this.prs
            .workCompletionCertificate(this.activeRoute.snapshot.params['id'], {
                type: this.activeRoute.snapshot.params['outputType']
            })
            .subscribe(
                result => {
                    this.activityName = result[0]['activityName'];
                    // this.workStartdate = result[0]['startDate'];
                    this.planYear = result[0]['planYear'];
                    this.foForm
                        .get('workStartdate')
                        .patchValue(result[0]['startDate']);
                    this.foForm
                        .get('workCompletionDateTime')
                        .patchValue(result[0]['endDate']);
                    this.foForm
                        .get('chalaniNo')
                        .patchValue(this.heading && this.heading.chalaniNo);
                    this.resultDatas = result && result[1];
                    if (result[0]['consumerCommitteName'] == '') {
                        this.consumerCommitteOrContractCompany =
                            result[0]['contractorName'];
                    } else {
                        this.consumerCommitteOrContractCompany =
                            result[0]['consumerCommitteName'];
                    }
                    // if (result[''])
                },
                error => {
                    console.log(error);
                }
            );
    }
    ngAfterViewInit() {
        this.unicode.initUnicode();
    }
    close() {
        // this.router.navigate(['/progress-reporting/progress-reporting-listing']);
        this.location.back();
    }

    print() {
        const printContents = document.getElementById('print-section')
            .innerHTML;
        this.printingService.printContents(
            printContents,
            'कार्य सम्पन्न प्रमाण पत्र',
            false,
            `
            @page{
              /* this affects the margin in the printer settings */
              margin: 24.4mm 24.4mm 24.4mm 37.1mm;
            }
            @media print{
              p, td {
                font-size: 25px !important;
              }
            }
            #showDiv{
              display: block !important;
            }
            #hideContent{
              display: none !important;
            }
            .fixedWidth{
              display: inline-block;
          width: 150px;
          }
            .show_div{
              display: inline-block !important;
            }
            .hide_div{
              display: none !important;
            }
            #divShow{
              display: inline-block !important;
            }
            #divHide{
              display: none !important;
            }
            `
        );
    }
}
