import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ProgressReportingCompletionCertificateComponent } from './progress-reporting-completion-certificate.component';
import { ProgressReportingCompletionCertificateRoutingModule } from './progress-reporting-completion-certificate-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ProgressReportingCompletionCertificateRoutingModule
  ],
  declarations: [
    ProgressReportingCompletionCertificateComponent,
  ]
})
export class ProgressReportingCompletionCertificateModule { }
