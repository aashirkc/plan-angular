import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgressReportingCompletionCertificateComponent } from './progress-reporting-completion-certificate.component';

const routes: Routes = [
  {
    path: '',
    component: ProgressReportingCompletionCertificateComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgressReportingCompletionCertificateRoutingModule { }
