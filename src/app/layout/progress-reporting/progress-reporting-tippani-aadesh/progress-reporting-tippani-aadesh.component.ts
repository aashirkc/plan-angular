import { Component, OnInit, ViewChild, AfterViewInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UnicodeTranslateService, AllReportService, ProgressReportingTippaniAadeshService, PrintingService, } from '../../../shared';
import { DateConverterService } from '../../../shared';
import { FormGroup, FormBuilder } from '@angular/forms';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';

@Component({
  selector: 'app-progress-reporting-tippani-aadesh',
  templateUrl: './progress-reporting-tippani-aadesh.component.html',
  styleUrls: ['./progress-reporting-tippani-aadesh.component.scss']
})
export class ProgressReportingTippaniAadeshComponent implements OnInit, AfterViewInit {
  date: any;
  progressData: any;
  formAccount: FormGroup;
  heading: Array<any> = [];
  totalAmount
  organizationMasterData: any;
  insStage: any;
  tippaniData: any;
  tippaniData1: any;
  finId: any;
  distType: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  isRul: boolean;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private dcs: DateConverterService,
    private unicode: UnicodeTranslateService,
    private report: AllReportService,
    private fb: FormBuilder,
    private printingService: PrintingService,
    private prtas: ProgressReportingTippaniAadeshService,
    @Inject('DIST_TYPE') distType
  ) {
    this.distType = distType;
    this.date = this.dcs.getToday()['fulldate'];
    this.formAccount = this.fb.group({
      'id': [''],
      'financial': [''],
      'issueAmount': [''],
      'chalaniNo': [''],
      'tippaniNo': [''],
      'peskiKatti': [0],
      'contengencyInput': [0],
      'contengencyKatti': [0],
      'janasahavagitaKatti': [''],
      'janasahavagitaInput': [0],
      'laborDonation': [0],
      'agrimKatti': [0],
      'agrimInput': [''],
      'duwaniInput': [0],
      'duwaniKatti': [0],
      'anyaKatti': [''],
      'anyaInput': [0],
      'vuktanRakam': [''],
      'vuktan': [''],
      'total': [''],
      'grandTotal': [''],
      'peski': [''],
      'totalEstimatedCost': [''],
      'peskiVuktan': [''],
      'enterDate': [''],
      'previousAmount': [''],
      'totalBhuktani': [''],
      'dateNow': ['']

    });
    let data = this.activeRoute.snapshot.params;
    let charan = this.activeRoute.snapshot.params['stage'];
    this.insStage = charan.split(" ");
    if (this.insStage[0] === 'अन्तिम') {
      this.insStage[0] = 'अन्तिम'
    } else {
      this.insStage[0] = this.insStage[0] + ' रनिङ्ग'
    }
    this.finId = data['financialId'];
    let dataRecord = {
      financialId: data['financialId']
    }
    this.prtas.get(data['id'], dataRecord).subscribe((res) => {
      this.progressData = res;
      console.info(res);

      if (this.progressData.length <= 0) {
        let messageDiv: any = document.getElementById('noRecord');
        messageDiv.innerText = 'कुनै विवरण भेटिएन';
      } else {
        this.totalAmount = Number(this.progressData[0]['issueAmount']) + Number(this.progressData[0]['previousAmt']);
      }

    }, (error) => {
      console.info(error);

    });
    this.prtas.getOrganizationName().subscribe((res) => {
      this.organizationMasterData = res;
    }, (error) => {
      console.info(error);

    });
  }

  ngAfterViewInit() {
    this.unicode.initUnicode();
    this.date = this.dcs.getToday()['fulldate'];
    let data = this.activeRoute.snapshot.params;
    this.formAccount.controls['financial'].patchValue(this.finId);

    this.finId = data['financialId'];
    let dataRecord = {
      financialId: data['financialId']
    }
    this.prtas.get(data['id'], dataRecord).subscribe((res) => {
      this.progressData = res;
      console.info(res);

      if (this.progressData.length <= 0) {
        let messageDiv: any = document.getElementById('noRecord');
        messageDiv.innerText = 'कुनै विवरण भेटिएन';
      } else {
        this.totalAmount = Number(this.progressData[0]['issueAmount']) + Number(this.progressData[0]['previousAmt']);
      }

      this.prtas.getTippani(data['financialId'], {}).subscribe((res) => {
        console.info(res);

        if (res['error']) {
          setTimeout(() => {
            this.formAccount.controls['issueAmount'].patchValue(this.progressData[0]['issueAmount']);
            this.formAccount.controls['financial'].patchValue(data['financialId']);
            this.totalAmount = Number(this.progressData[0]['issueAmount']) + Number(this.progressData[0]['previousAmt']);
            this.formAccount.controls['grandTotal'].patchValue(0);
            this.formAccount.controls['total'].patchValue(this.progressData[0]['budget']);
            this.formAccount.controls['totalEstimatedCost'].patchValue(this.progressData && this.progressData[0] && this.progressData[0].totalEstimatedCost);
            this.formAccount.controls['contengencyInput'].patchValue(3);
            this.formAccount.controls['janasahavagitaInput'].patchValue(10);

            this.formAccount.controls['agrimInput'].patchValue(1);
            this.formAccount.controls['duwaniInput'].patchValue(0);
            this.formAccount.controls['anyaInput'].patchValue(0);
            this.formAccount.controls['enterDate'].patchValue(this.date);
            this.formAccount.controls['dateNow'].patchValue(this.date);
            this.formAccount.controls['previousAmount'].patchValue(this.progressData[0].previousAmt);
            this.formAccount.controls['totalBhuktani'].patchValue(this.totalAmount);
            this.changeJanata();
            this.changeContengency();
            this.changeAgrim();
            this.changeDuwani();
            this.changeAnya();
          }, 200)
        } else {
          setTimeout(() => {
            this.tippaniData1 = res['data'];
            let a = JSON.parse(this.tippaniData1);
            this.tippaniData = a['data'];

            // this should be refactored to single line
            // this.formAccount.patchValue(this.tippaniData);
            // check this later

            this.formAccount.controls['issueAmount'].patchValue(this.tippaniData.issueAmount || 0);
            this.formAccount.controls['financial'].patchValue(this.tippaniData.financial || '');
            this.formAccount.controls['total'].patchValue(this.tippaniData['total']);
            this.formAccount.controls['peskiKatti'].patchValue(this.tippaniData['peskiKatti']);
            this.formAccount.controls['contengencyInput'].patchValue(this.tippaniData['contengencyInput']);
            this.formAccount.controls['contengencyKatti'].patchValue(this.tippaniData['contengencyKatti']);
            this.formAccount.controls['chalaniNo'].patchValue(this.tippaniData['chalaniNo']);
            this.formAccount.controls['tippaniNo'].patchValue(this.tippaniData['tippaniNo']);
            this.formAccount.controls['laborDonation'].patchValue(this.tippaniData['laborDonation']);
            this.formAccount.controls['janasahavagitaKatti'].patchValue(this.tippaniData['janasahavagitaKatti']);
            this.formAccount.controls['janasahavagitaInput'].patchValue(this.tippaniData['janasahavagitaInput']);
            this.formAccount.controls['agrimKatti'].patchValue(this.tippaniData['agrimKatti']);
            this.formAccount.controls['agrimInput'].patchValue(this.tippaniData['agrimInput']);
            this.formAccount.controls['duwaniInput'].patchValue(this.tippaniData['duwaniInput']);
            this.formAccount.controls['duwaniKatti'].patchValue(this.tippaniData['duwaniKatti']);
            this.formAccount.controls['anyaKatti'].patchValue(this.tippaniData['anyaKatti']);
            this.formAccount.controls['anyaInput'].patchValue(this.tippaniData['anyaInput']);
            this.formAccount.controls['vuktanRakam'].patchValue(this.tippaniData['vuktanRakam']);
            this.formAccount.controls['vuktan'].patchValue(this.tippaniData['vuktan'] || this.tippaniData['vuktanRakam']);
            this.formAccount.controls['grandTotal'].patchValue(Number(this.tippaniData['grandTotal']).toFixed(2));
            this.formAccount.controls['peski'].patchValue(this.tippaniData['peski']);
            this.formAccount.controls['totalEstimatedCost'].patchValue(this.tippaniData['totalEstimatedCost']);
            this.formAccount.controls['peskiVuktan'].patchValue(this.tippaniData['peskiVuktan']);
            this.formAccount.controls['enterDate'].patchValue(this.tippaniData['enterDate']);
            this.formAccount.controls['previousAmount'].patchValue(this.tippaniData['previousAmount']);
            this.formAccount.controls['totalBhuktani'].patchValue(this.tippaniData['totalBhuktani']);
          }, 100)
        }

      }, (error) => {
        console.info(error);

      });

    }, (error) => {
      console.info(error);

    });

  }

  ngOnInit() {
    this.date = this.dcs.getToday()['fulldate'];
    this.isRul = false;
    const orgMaster = JSON.parse(localStorage.getItem('org'));
    if (orgMaster && orgMaster[0]) {
      const orgType = orgMaster[0]['orgType'];
      this.isRul = orgType === 'RUL';
    }
  }

  close() {
    this.router.navigate(['/progress-reporting/progress-reporting-listing']);
  }

  changeJanata() {
    let a = this.progressData && this.progressData[0] && this.progressData[0].totalEstimatedCost || 0;
    let b = Number(this.formAccount.controls['janasahavagitaInput'].value)
    let p = (b / 100 * a).toFixed(2);
    this.formAccount.controls['janasahavagitaKatti'].patchValue(p);
    this.programCalculate()
  }

  changeContengency() {
    let a = Number(this.formAccount.controls['issueAmount'].value) || 0;
    let b = Number(this.formAccount.controls['contengencyInput'].value)
    let p = (b / 100 * a).toFixed(2);
    this.formAccount.controls['contengencyKatti'].patchValue(p);
    this.Calculate()
  }
  changeAgrim() {
    let a = Number(this.formAccount.controls['issueAmount'].value) || 0;
    let b = Number(this.formAccount.controls['agrimInput'].value)
    let p = (b / 100 * a).toFixed(2);
    this.formAccount.controls['agrimKatti'].patchValue(p);
    this.Calculate()
  }
  changeDuwani() {
    let a = Number(this.formAccount.controls['issueAmount'].value) || 0;
    let b = Number(this.formAccount.controls['duwaniInput'].value)
    let p = (b / 100 * a).toFixed(2);
    this.formAccount.controls['duwaniKatti'].patchValue(p);
    this.Calculate()
  }
  changeAnya() {
    let a = Number(this.formAccount.controls['issueAmount'].value) || 0;
    let b = Number(this.formAccount.controls['anyaInput'].value)
    let p = (b / 100 * a).toFixed(2);
    this.formAccount.controls['anyaKatti'].patchValue(p);
    this.Calculate()
  }

  changePercentage() {
    const total = Number(this.formAccount.controls['totalEstimatedCost'].value) || 0;
    const current = Number(this.formAccount.controls['janasahavagitaKatti'].value) || 0;
    const p = ((current / total) * 100).toFixed(2);
    this.formAccount.controls['janasahavagitaInput'].patchValue(p);
  }

  print(post) {
    const printContents = document.getElementById('print-section').innerHTML;
    this.printingService.printContents(printContents, 'Tipani Adesh', false, `
      @page {
        /* this affects the margin in the printer settings */
        margin: 24.4mm 24.4mm 24.4mm 37.1mm;
      }
      p, td{
        font-size: 20px !important;
      }
    `);

    // adding labor donation [for backend]

    // laborDonation
    let p = { data: post }

    this.prtas.storeTippani(post['financial'], p).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          // this.aboutFinancialOperationForm.reset();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  keyUp(value) {
    this.formAccount.controls['vuktan'].patchValue(value);

  }
  Calculate() {
    let issueAmount = Number(this.formAccount.get('issueAmount').value) || 0;
    let contengency = Number(this.formAccount.get('contengencyKatti').value) || 0;
    let agrim = Number(this.formAccount.get('agrimKatti').value) || 0;
    let duwani = Number(this.formAccount.get('duwaniKatti').value) || 0;
    let anya = Number(this.formAccount.get('anyaKatti').value) || 0;
    let peski = Number(this.formAccount.get('peskiKatti').value) || 0;

    let result = Number(issueAmount) - Number(agrim) - Number(duwani) - Number(peski) - Number(anya) - Number(contengency);
    this.formAccount.get('vuktanRakam').patchValue(result.toFixed(2));
    this.changeJanata();

  }
  programCalculate() {
    let janasahavagita = Number(this.formAccount.get('janasahavagitaKatti').value) || 0;
    let totalEstimatedCost = Number(this.formAccount.get('totalEstimatedCost').value) || 0;
    let laborDonation = Number(this.formAccount.get('laborDonation').value) || 0;
    let result = totalEstimatedCost + janasahavagita + laborDonation;
    this.formAccount.get('total').patchValue(result);
  }

}
