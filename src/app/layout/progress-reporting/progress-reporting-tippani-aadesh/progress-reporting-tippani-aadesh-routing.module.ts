import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgressReportingTippaniAadeshComponent } from './progress-reporting-tippani-aadesh.component';

const routes: Routes = [
  {
    path: '',
    component: ProgressReportingTippaniAadeshComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgressReportingTippaniAadeshRoutingModule { }
