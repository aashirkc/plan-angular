import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgressReportingTippaniAadeshRoutingModule } from './progress-reporting-tippani-aadesh-routing.module';
import { ProgressReportingTippaniAadeshComponent } from './progress-reporting-tippani-aadesh.component';
import { SharedModule } from '../../../shared/modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ProgressReportingTippaniAadeshRoutingModule
  ],
  declarations: [ProgressReportingTippaniAadeshComponent]
})
export class ProgressReportingTippaniAadeshModule { }
