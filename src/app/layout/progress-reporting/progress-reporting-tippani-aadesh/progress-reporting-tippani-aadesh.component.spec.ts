import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressReportingTippaniAadeshComponent } from './progress-reporting-tippani-aadesh.component';

describe('ProgressReportingTippaniAadeshComponent', () => {
  let component: ProgressReportingTippaniAadeshComponent;
  let fixture: ComponentFixture<ProgressReportingTippaniAadeshComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressReportingTippaniAadeshComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressReportingTippaniAadeshComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
