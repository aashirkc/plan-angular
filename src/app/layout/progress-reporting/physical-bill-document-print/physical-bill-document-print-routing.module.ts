import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PhysicalBillDocumentPrintComponent } from './physical-bill-document-print.component';

const routes: Routes = [
  {
    path: '',
    component: PhysicalBillDocumentPrintComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhysicalBillDocumentPrintRoutingModule { }
