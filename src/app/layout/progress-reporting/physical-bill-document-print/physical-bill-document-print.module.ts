import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { PhysicalBillDocumentPrintComponent } from './physical-bill-document-print.component';
import { PhysicalBillDocumentPrintRoutingModule } from './physical-bill-document-print-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PhysicalBillDocumentPrintRoutingModule
  ],
  declarations: [
    PhysicalBillDocumentPrintComponent,
  ]
})
export class PhysicalBillDocumentPrintModule { }
