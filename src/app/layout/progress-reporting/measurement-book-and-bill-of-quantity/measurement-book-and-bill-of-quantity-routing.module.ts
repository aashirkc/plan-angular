import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MeasurementBookAndBillOfQuantityComponent } from './measurement-book-and-bill-of-quantity.component';

const routes: Routes = [
  {
    path: '',
    component: MeasurementBookAndBillOfQuantityComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MeasurementBookAndBillOfQuantityRoutingModule { }
