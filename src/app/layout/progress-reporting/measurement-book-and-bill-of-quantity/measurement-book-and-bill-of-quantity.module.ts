import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeasurementBookAndBillOfQuantityComponent } from './measurement-book-and-bill-of-quantity.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { MeasurementBookAndBillOfQuantityRoutingModule } from './measurement-book-and-bill-of-quantity-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MeasurementBookAndBillOfQuantityRoutingModule,
  ],
  declarations: [MeasurementBookAndBillOfQuantityComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class MeasurementBookAndBillOfQuantityModule { }
