import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MeasurementBookAndBillOfQuantityService, EstimateAbstractOfCostService, PlanningYearService, ActivityOutputService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-measurement-book-and-bill-of-quantity',
  templateUrl: './measurement-book-and-bill-of-quantity.component.html',
  styleUrls: ['./measurement-book-and-bill-of-quantity.component.scss']
})
export class MeasurementBookAndBillOfQuantityComponent implements OnInit {

  EstimateAbstractOfCostDetailsForm: FormGroup;
  searchForm: FormGroup;
  updateDetails: boolean = false;
  source: any;
  sourceDetails: any;
  activityAdapter: Array<any> = [];
  dataAdapter: any;
  dataDetailsAdapter: any;
  columns: any[] = [];
  columnsDetails: any[] = [];
  editrow: number = -1;
  columngroups: any[];
  transData: any;
  fullDate: string;
  unitAdapter: Array<any> = [
    {
      name: 'm2'
    },
    {
      name: 'm3'
    },
    {
      name: 'ft2'
    },
    {
      name: 'ft3'
    }
  ];
  planYearAdapter: Array<any> = [];
  investigationStageAdapter: Array<any> = [];
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('myDetailsGrid') myDetailsGrid: jqxGridComponent;
  @ViewChild('myComboBox') myComboBox: jqxComboBoxComponent;

  PhysicalProgressDetails: any;

  constructor(
    private fb: FormBuilder,
    private estimateCostService: EstimateAbstractOfCostService,
    private estimateCostBillsService: MeasurementBookAndBillOfQuantityService,
    private activeRoute: ActivatedRoute,
    private pys: PlanningYearService,
    private avs: ActivityOutputService,
    private router: Router,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  ngOnInit() {

    this.pys.index({}).subscribe((res) => {
      this.planYearAdapter = res;
      if (this.planYearAdapter.length > 0) {
        let planCode = this.planYearAdapter[0].yearCode || '';
        this.searchForm.get('planYear').patchValue(planCode);
      }
    }, (error) => {
      console.info(error);
    });
    this.estimateCostService.indexActivity({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

    let data = this.activeRoute.snapshot.params['id'];
    this.avs.showActivityPhysicalProgress(data).subscribe((res) => {
      this.PhysicalProgressDetails = res;
    }, (error) => {
      console.info(error);
    });
  }

  getTranslation() {
    this.translate.get(['SN', "RELOAD", 'UNIT', "ACTIVITY_NAME", "NOM", "LENGTH", "HEIGHT", "REMARKS", "BREADTH", "QUANTITY", "RATE", "AMOUNT", "UNIT", "DESCRIPTION_OF_WORK", "EDIT", "ACTION", "ADD_DETAILS", "START_DATE", "END_DATE", "BS", "AD", "DELETE_ROW", "YES", "NO", "YESE", "NOE"]).subscribe((translation: [string]) => {
      this.transData = translation;
      this.loadGrid();
    });
  }

  loadGridData(post) {
    this.jqxLoader.open();
    this.estimateCostService.index(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
  loadGridDetailsData(post) {
    this.jqxLoader.open();
    this.estimateCostBillsService.index(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.sourceDetails.localdata = [];
      } else {
        this.sourceDetails.localdata = res;
      }
      this.myDetailsGrid.updatebounddata();

      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  createForm() {
    this.EstimateAbstractOfCostDetailsForm = this.fb.group({
      'eacId': ['',],
      'sn': ['',],
      'physicalProgressId': [''],
      'no': ['', Validators.required],
      'length': ['', Validators.required],
      'breadth': ['', Validators.required],
      'heigth': ['', Validators.required],
      'qty': ['', Validators.required],
      'description': ['', Validators.required],
      'unit': ['', Validators.required],
      'rate': ['', Validators.required],
      'amount': ['', Validators.required],
      'remarks': ['', Validators.required],
    });

    this.searchForm = this.fb.group({
      'planYear': ['',],
    });
  }
  comboBoxOnChange() {
    let dt = {};
    dt['activity'] = this.myComboBox.val();
    this.loadGridData(dt);

    this.sourceDetails.localdata = [];
    this.myDetailsGrid.updatebounddata();
    this.EstimateAbstractOfCostDetailsForm.reset();
  }
  ngAfterViewInit() {

  }

  SearchData(post) {
    post['activity'] = this.myComboBox.val() || '';
    this.loadGridData(post);
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'activity', type: 'string' },
          { name: 'activityName', type: 'string' },
          { name: 'descriptionOfWork', type: 'string' },
          { name: 'descriptionOfWorkSn', type: 'string' },
          { name: 'unit', type: 'string' },
        ],
        id: 'id',
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      { text: this.transData['SN'], datafield: 'descriptionOfWorkSn', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 105, },
      { text: this.transData['DESCRIPTION_OF_WORK'], datafield: 'descriptionOfWork', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 823, },
      { text: this.transData['UNIT'], datafield: 'unit', columntype: 'textbox', editable: false, width: 80, filtercondition: 'starts_with' },
      {
        text: this.transData['ACTION'], datafield: 'Add', sortable: false, filterable: false, width: 200, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['ADD_DETAILS'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          this.EstimateAbstractOfCostDetailsForm.controls['eacId'].setValue(dataRecord['id']);
          this.EstimateAbstractOfCostDetailsForm.controls['unit'].setValue(dataRecord['unit']);
          let dt = {};
          dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
          this.loadGridDetailsData(dt);
        }
      }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

    this.sourceDetails =
      {
        datatype: 'json',
        datafields: [
          { name: 'eacId', type: 'string' },
          { name: 'no', type: 'string' },
          { name: 'sn', type: 'string' },
          { name: 'length', type: 'string' },
          { name: 'breadth', type: 'string' },
          { name: 'heigth', type: 'string' },
          { name: 'qty', type: 'string' },
          { name: 'unit', type: 'string' },
          { name: 'rate', type: 'string' },
          { name: 'description', type: 'string' },
          { name: 'physicalProgressId', type: 'string' },
          { name: 'amount', type: 'string' },
          { name: 'remarks', type: 'string' },
        ],
        id: 'id',
        localdata: [],
        pagesize: 20
      }

    this.dataDetailsAdapter = new jqx.dataAdapter(this.sourceDetails);
    this.columnsDetails = [
      { text: this.transData['NOM'], datafield: 'no', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 40, },
      { text: this.transData['LENGTH'], datafield: 'length', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 80, },
      { text: this.transData['BREADTH'], datafield: 'breadth', columntype: 'textbox', editable: false, width: 80, filtercondition: 'starts_with' },
      { text: this.transData['HEIGHT'], datafield: 'heigth', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 80, },
      { text: this.transData['QUANTITY'], datafield: 'qty', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 80, },
      { text: this.transData['UNIT'], datafield: 'unit', columntype: 'textbox', editable: false, width: 80, filtercondition: 'starts_with' },
      { text: this.transData['RATE'], datafield: 'rate', columntype: 'textbox', editable: false, width: 80, filtercondition: 'starts_with' },
      { text: this.transData['AMOUNT'], datafield: 'amount', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 80, },
      { text: this.transData['REMARKS'], datafield: 'remarks', columntype: 'textbox', editable: false, filtercondition: 'starts_with', width: 445, },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 137, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myDetailsGrid.getrowdata(this.editrow);
          let dt = {};
          dt['eacId'] = dataRecord['eacId'];
          dt['no'] = dataRecord['no'];
          dt['length'] = dataRecord['length'];
          dt['breadth'] = dataRecord['breadth'];
          dt['heigth'] = dataRecord['heigth'];
          dt['qty'] = dataRecord['qty'];
          dt['description'] = dataRecord['description'];
          dt['unit'] = dataRecord['unit'];
          dt['sn'] = dataRecord['sn'];
          dt['physicalProgressId'] = dataRecord['physicalProgressId'];
          dt['rate'] = dataRecord['rate'];
          dt['amount'] = dataRecord['amount'];
          dt['remarks'] = dataRecord['remarks'];
          this.EstimateAbstractOfCostDetailsForm.setValue(dt);
          this.updateDetails = true;
        }
      }
    ];

  }

  rendertoolbarDetails = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer4 = document.createElement('div');
    let buttonContainer5 = document.createElement('div');

    buttonContainer4.id = 'buttonContainer4';
    buttonContainer5.id = 'buttonContainer5';

    buttonContainer4.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer5.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer4);
    container.appendChild(buttonContainer5);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer4', 'jqxButton', { width: 150, value: this.transData['DELETE_ROW'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer5', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.myDetailsGrid.getselectedrowindexes();
      let ids = [];
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myDetailsGrid.getrowdata(Number(id[i]));
        let dta = dataRecord['eacId'] + dataRecord['sn'];
        ids.push(dataRecord['eacId'] + '' + dataRecord['sn']);
      }

      if (ids.length > 0) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.estimateCostService.destroyDetails(ids).subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.myDetailsGrid.clearselection();
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              let dt = {};
              dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
              this.loadGridDetailsData(dt);
            }
            if (result['error']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              let dt = {};
              dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
              this.loadGridDetailsData(dt);
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      let dt = {};
      dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
      this.loadGridDetailsData(dt);
    });

  }; //render toolbar ends


  saveDetailsBtn(post) {
    this.jqxLoader.open();
    this.estimateCostBillsService.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();

          let dt = {};
          dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
          this.loadGridDetailsData(dt);
          setTimeout(() => {
            this.EstimateAbstractOfCostDetailsForm.reset();
          }, 100);
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

  updateDetailsBtn(post) {
    this.jqxLoader.open();
    this.estimateCostBillsService.update(post['sn'], post).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.updateDetails = false;
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          let dt = {};
          dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
          this.loadGridDetailsData(dt);
          setTimeout(() => {
            this.EstimateAbstractOfCostDetailsForm.reset();
          }, 100);
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateDetailsBtn() {
    this.updateDetails = false;
    this.EstimateAbstractOfCostDetailsForm.reset();
    let dt = {};
    dt['id'] = this.EstimateAbstractOfCostDetailsForm.controls['eacId'].value;
    this.loadGridDetailsData(dt);
  }

  Close() {
    let data = this.activeRoute.snapshot.params['id'];
    let dataType = this.activeRoute.snapshot.params['outputType'];
    this.router.navigate(['/progress-reporting/progress-reporting-physical-progress/', data, { outputType: dataType }]);
  }

  dataChange() {
    let rate = this.EstimateAbstractOfCostDetailsForm.value['rate'] || 0;
    let qty = this.EstimateAbstractOfCostDetailsForm.value['qty'] || 0;
    let amount = rate * qty;
    this.EstimateAbstractOfCostDetailsForm.controls['amount'].patchValue(amount);
  }

}
