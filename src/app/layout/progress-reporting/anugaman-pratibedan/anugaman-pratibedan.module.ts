import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AnugamanPratibedanComponent } from './anugaman-pratibedan.component';
import { AnugamanPratibedanRoutingModule } from './anugaman-pratibedan-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AnugamanPratibedanRoutingModule
  ],
  declarations: [AnugamanPratibedanComponent]
})
export class AnugamanPratibedanModule { }
