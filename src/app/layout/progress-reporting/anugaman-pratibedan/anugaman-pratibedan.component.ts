import { Component, OnInit, ViewChild, AfterViewInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AllReportService, UnicodeTranslateService, AdministrativeApprovalService, AnugamanSamitiMasterServiceService, PrintingService } from '../../../shared';
import { DateConverterService } from '../../../shared';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';

@Component({
  selector: 'app-anugaman-pratibedan',
  templateUrl: './anugaman-pratibedan.component.html',
  styleUrls: ['./anugaman-pratibedan.component.scss']
})
export class AnugamanPratibedanComponent implements OnInit, AfterViewInit {
  date: any;
  progressData: Array<any> = [];
  heading: Array<any> = [];
  category: any;
  DetailsData: Array<any> = [];
  CommitteeDetailsData: Array<any> = [];
  TableData: Array<any> = [];
  formAccount: FormGroup;
  actAddress: any;
  actValue: any;
  padh: any = ['संयोजक', 'सदस्य', 'सदस्य', 'सदस्य', 'सदस्य', 'सदस्य सचिव']
  categoryAdapter: any = [
    { name: "सन्ताेषजनक" },
    { name: "उत्कृष्ठ" }
  ];
  lastStage: boolean = false;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  oType: any;
  distType: any;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private dcs: DateConverterService,
    private ars: AllReportService,
    private aas: AdministrativeApprovalService,
    private fb: FormBuilder,
    private unicode: UnicodeTranslateService,
    private printingService: PrintingService,
    private anugamanMaster: AnugamanSamitiMasterServiceService,
    @Inject('DIST_TYPE') distType
  ) {
    this.distType = distType
    this.date = this.dcs.getToday()['fulldate'];
    this.formAccount = this.fb.group({
      'id': [''],
      'nagarPramukh': [''],
      'financeId': [''],
      'samyojak': [''],
      'chalaniNo': [''],
      'sachiv': [''],
      'activityName': [''],
      'activityAddress': [''],
      'kharidPrakriya': [''],
      'fromNepalSarkar': [''],
      'fromState': [''],
      'fromLocalLevel': [''],
      'fromWada': [''],
      'fromConsumerCommitte': [''],
      'fromOthers': [''],
      'totalGranted': [''],
      'monitoringDetails': [''],
      'contractDate': [''],
      'patraSankya': [''],
      'estimatedWorkCompletionDate': [''],
      'completionDate': [''],
      'category': [''],
      'date': [''],
      'planYear': [''],
      'anugamanDate': [''],
      'activityDate': [''],
      'activityComDate': [''],
      'committeHead': [''],
      'committeeName': [''],
      'VdcBud': [''],
      'shramDan': [''],
      'technicalIncharge': [''],
      'techCost': [''],
      'proposedCost': [''],
      'gudastar': [''],
      'work_quality': [''],
      'work_quality1': [''],
      'work_quality2': [''],
      'work_quality3': [''],
      'work_quality4': [''],
      'work_quality5': [''],
      'work_quality6': [''],
      'sadasyaDetails': this.fb.array([
        this.initbodarthaDetails(),
      ]),
      'tabled': this.fb.array([
        this.initTableDetails(),
      ]),
      'tabledata1': this.fb.array([
        // this.initTableDetails1(),
      ]),
    })
  }
  initTableDetails1() {
    return this.fb.group({
      sn: [''],
      name: [''],
      designation: [''],
      related: ['']
    });
  }

  initTableDetails() {
    return this.fb.group({
      sn: [''],
      activity: [''],
      unit: [''],
      anusar: [''],
      repair: [''],
      incdsc: [''],
      remark: ['']
    });
  }
  initbodarthaDetails() {
    return this.fb.group({
      sadasya: [''],
    });
  }

  addItem1() {
    const control1 = <FormArray>this.formAccount.controls['tabled'];
    control1.push(this.initTableDetails());
  }
  addItem5() {
    const control1 = <FormArray>this.formAccount.controls['tabledata1'];
    control1.push(this.initTableDetails());
  }
  addItem2() {
    const control1 = <FormArray>this.formAccount.controls['tabledata1'];
    control1.push(this.initTableDetails1());
  }

  addItem() {
    const control1 = <FormArray>this.formAccount.controls['sadasyaDetails'];
    control1.push(this.initbodarthaDetails());
    // // add unicode support for dynamically added input
    // setTimeout(() => {
    //   this.unicode.initUnicode();
    // }, 200);
  }
  removeItem(i: number) {
    const control1 = <FormArray>this.formAccount.controls['sadasyaDetails'];
    control1.removeAt(i);
    //this.itemAdapter.splice(i, 1);
  }


  // tableFillingData = [
  //     { sn: "१", name: ' उप सचिव श्री दधिराम अर्याल', designation: 'संयोजक', related: 'नगरपालिका' },
  //     { sn: "२", name: 'वडा अध्यक्ष श्री बसन्तराज दहाल', designation: 'सदस्य', related: 'नगरपालिका' },
  //     { sn: "३", name: ' वडा अध्यक्ष श्री कन्हैया थारु', designation: 'सदस्य', related: 'नगरपालिका' },
  //     { sn: "४", name: 'वडा अध्यक्ष श्री यमलाल पाण्डे', designation: 'सदस्य', related: 'नगरपालिका' },
  //     { sn: "५", name: 'कार्यपालिका सदस्य श्री गौमाया गुरुङ्ग', designation: 'सदस्य', related: 'नगरपालिका' },
  //     { sn: "६", name: 'कार्यपालिका सदस्य श्री तुल्सी ज्ञावली', designation: 'सदस्य', related: 'नगरपालिका' },
  //     { sn: "७", name: 'प्रमुख प्रशासकिय अधिकृत श्री राम लाल श्रेष्ठ', designation: 'सदस्य', related: 'नगरपालिका' },
  //     { sn: "८", name: 'अनुगमान बिषय गत सम्बन्धित बिषयगत शाखा प्रमुख श्री पुर्षोतम ढकाल', designation: 'सदस्य', related: 'नगरपालिका' },

  // ]
  tableFillingData: any[];
  ngAfterViewInit() {
    this.unicode.initUnicode();
    let idData = this.activeRoute.snapshot.params['singleid'];

    // administrative approval activity id
    let id = this.activeRoute.snapshot.params['id'];
    let oType = this.activeRoute.snapshot.params['outputType'];
    let stage = this.activeRoute.snapshot.params['stage'];
    if (stage == 'अन्तिम चरण') {
      this.lastStage = true;
      this.formAccount.get('category').patchValue(this.categoryAdapter[0].name);
    }
    let post = { type: oType }

    // patch tableData
    // console.log(this.formAccount);


    this.aas.getApprovalByid(id).subscribe(
      (result) => {
        // if (result['message']) {
        //   let messageDiv: any = document.getElementById('message');
        //   messageDiv.innerText = result['message'];
        //   this.msgNotification.open();
        // }

        // if (result && result['dataValue']) {
        // let resData = JSON.parse(result['D']);
        // console.log(resData);
        // this.CommitteeDetailsData = result['committeeDetails'];
        // for (let i = 0; i < resData['sadasyaDetails'].length - 1; i++) {
        //   this.addItem();
        // }
        // for (let i = 0; i < resData['sadasyaDetails'].length; i++) {
        // console.log("check"+this.CommitteeDetailsData);

        result['data'] && this.formAccount.get('activityDate').patchValue(result['data'].agreementDate);
        result['ActivityDetails'] && this.formAccount.get('activityComDate').patchValue(result['ActivityDetails'].completionDate);
        if (result['PlanAgreement'] && result['PlanAgreement'][0]['consumerCommitteName'] != "") {
          this.formAccount.get('committeeName').patchValue(result['PlanAgreement'][0]['consumerCommitteName']);
        } else if (result['contractDetails']) {
          try {
            const selectedContract = result['contractDetails'].filter(d => d.selected)
            this.formAccount.get('committeeName').patchValue(selectedContract[0]['name'])
          } catch (e) {
            console.log(e);
          }
        } else {
          this.formAccount.get('committeeName').patchValue(result['officeName']);
        }

        if (result['CommitteDetails'][0]) {
          this.formAccount.get('committeHead').patchValue(result['CommitteDetails'][0]['memberName']);
        } else if (result['contractDetails']) {
          try {
            const selectedContract = result['contractDetails'].filter(d => d.selected)
            this.formAccount.get('committeHead').patchValue(selectedContract[0]['contactPerson'])
          } catch (e) {
            console.log(e);
          }
        } else {
          this.formAccount.get('committeHead').patchValue(result['orderName'])
        }
        result['PlanAgreement'][0] && this.formAccount.get('fromLocalLevel').patchValue(result['PlanAgreement'][0]['grantedFromLocalLevel']);
        result['PlanAgreement'][0] && this.formAccount.get('shramDan').patchValue(result['PlanAgreement'][0]['grantedFromConsumerCommitte']);
        result['RemainingPlan'][0] && this.formAccount.get('planYear').patchValue(result['RemainingPlan'][0]['planYear']);
        this.formAccount.get('techCost').patchValue(result['techCost']);
        this.formAccount.get('proposedCost').patchValue(result['proposedCost']);
        // }
        // }
      }
    );




    this.ars.getAnugaman(idData, {}).subscribe(
      result => {

        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
        }
        if (result && result['dataValue']) {
          let resData = JSON.parse(result['dataValue']);
          // console.log("Res Data"+resData);
          for (let i = 0; i < resData['sadasyaDetails'].length - 1; i++) {
            this.addItem();
          }
          for (let i = 0; i < resData['sadasyaDetails'].length; i++) {
            this.formAccount.get('committeHead').patchValue(resData['committeHead']);
            // this.formAccount.get('activityComDate').patchValue(resData['activityComDate']);
            this.formAccount.get('committeeName').patchValue(resData['committeeName']);
            this.formAccount.get('date').patchValue(resData['date']);
            this.formAccount.get('chalaniNo').patchValue(resData['chalaniNo']);
            this.formAccount.get('activityName').patchValue(resData['activityName']);
            this.formAccount.get('activityDate').patchValue(resData['activityDate']);
            this.formAccount.get('activityAddress').patchValue(resData['activityAddress']);
            this.formAccount.get('technicalIncharge').patchValue(resData['technicalIncharge']);
            this.formAccount.get('fromNepalSarkar').patchValue(resData['fromNepalSarkar']);
            this.formAccount.get('fromState').patchValue(resData['fromState']);
            this.formAccount.get('fromLocalLevel').patchValue(resData['fromLocalLevel']);
            this.formAccount.get('fromWada').patchValue(resData['fromWada']);
            this.formAccount.get('anugamanDate').patchValue(resData['anugamanDate']);
            this.formAccount.get('fromConsumerCommitte').patchValue(resData['fromConsumerCommitte']);
            this.formAccount.get('fromOthers').patchValue(resData['fromOthers']);
            this.formAccount.get('totalGranted').patchValue(resData['totalGranted']);
            this.formAccount.get('contractDate').patchValue(resData['contractDate']);
            this.formAccount.get('estimatedWorkCompletionDate').patchValue(resData['estimatedWorkCompletionDate']);
            // this.formAccount.get('completionDate').patchValue(resData['completionDate']);
            this.formAccount.get('shramDan').patchValue(resData['shramDan']);
            this.formAccount.get('monitoringDetails').patchValue(resData['monitoringDetails']);
            this.formAccount.get('gudastar').patchValue(resData['gudastar']);
            this.formAccount.get('work_quality').patchValue(resData['work_quality']);
            this.formAccount.get('work_quality1').patchValue(resData['work_quality1']);
            this.formAccount.get('work_quality2').patchValue(resData['work_quality2']);
            this.formAccount.get('work_quality3').patchValue(resData['work_quality3']);
            this.formAccount.get('work_quality4').patchValue(resData['work_quality4']);
            this.formAccount.get('work_quality5').patchValue(resData['work_quality5']);
            this.formAccount.get('work_quality6').patchValue(resData['work_quality6']);
            // this.formAccount.get('tabledata1').patchValue(this.TableData['tabledata1']);
          }

          const tableData = resData['tabledata1'];
          const control1 = <FormArray>this.formAccount.controls['tabledata1'];
          for (let i = 0; i < tableData.length; i++) {
            const details = this.initTableDetails1();
            details.patchValue(tableData[i]);
            control1.push(details);
          }
          if (resData && resData['kharidPrakriya']) {
            this.getkharid("y")
          }
          else {
            this.getkharid("n");
          }
        } else {

          this.anugamanMaster.index().subscribe(response => {
            this.tableFillingData = response.map(data => {
              return {
                name: ` ${data.designation} ${data.name}`,
                designation: data.committeeDesignation,
                related: data.related
              }
            })
            const control1 = <FormArray>this.formAccount.controls['tabledata1'];
            for (const data of this.tableFillingData) {
              const details = this.initTableDetails1();
              details.patchValue(data);
              // console.log(details);
              control1.push(details);
            }

          }, error => { })

          this.addItem();
          this.addItem();
          this.addItem();
          this.ars.getPhysicalDetails(idData, post).subscribe((res) => {
            this.DetailsData = res['data'] || '';
            this.TableData = res['OtherData'] || '';

            this.formAccount.get('activityName').patchValue(this.TableData['activityName']);
            this.formAccount.get('date').patchValue(this.date);
            this.formAccount.get('chalaniNo').patchValue(this.TableData['chalaniNo']);
            // this.formAccount.get('activityComDate').patchValue(this.TableData['activityComDate']);
            this.TableData['committeeName'] && this.formAccount.get('committeeName').patchValue(this.TableData['committeeName']);
            this.DetailsData['enterDate'] && this.formAccount.controls['anugamanDate'].patchValue(this.DetailsData['enterDate']);
            this.TableData['activityAddress'] && this.formAccount.get('activityAddress').patchValue(this.TableData['activityAddress']);
            this.TableData['activityDate'] && this.formAccount.get('activityDate').patchValue(this.TableData['activityDate']);
            this.TableData['technicalIncharge'] && this.formAccount.get('technicalIncharge').patchValue(this.TableData['technicalIncharge']);
            this.TableData['fromNepalSarkar'] && this.formAccount.get('fromNepalSarkar').patchValue(this.TableData['fromNepalSarkar']);
            this.TableData['fromState'] && this.formAccount.get('fromState').patchValue(this.TableData['fromState']);
            this.TableData['fromLocalLevel'] && this.formAccount.get('fromLocalLevel').patchValue(this.TableData['fromLocalLevel']);
            this.TableData['fromWada'] && this.formAccount.get('fromWada').patchValue(this.TableData['fromWada']);
            this.TableData['fromConsumerCommitte'] && this.formAccount.get('fromConsumerCommitte').patchValue(this.TableData['fromConsumerCommitte']);
            this.TableData['fromOthers'] && this.formAccount.get('fromOthers').patchValue(this.TableData['fromOthers']);
            this.TableData['totalGranted'] && this.formAccount.get('totalGranted').patchValue(this.TableData['totalGranted']);
            this.TableData['patraSankya'] && this.formAccount.get('patraSankya').patchValue(this.TableData['patraSankya']);
            this.TableData['gudastar'] && this.formAccount.get('gudastar').patchValue(this.TableData['gudastar']);
            this.TableData['monitoringDetails'] && this.formAccount.get('monitoringDetails').patchValue(this.DetailsData['monitoringDetails']);
            this.TableData['work_quality'] && this.formAccount.get('work_quality').patchValue(this.TableData['work_quality']);
            this.TableData['work_quality1'] && this.formAccount.get('work_quality1').patchValue(this.TableData['work_quality1']);
            this.TableData['work_quality2'] && this.formAccount.get('work_quality2').patchValue(this.TableData['work_quality2']);
            this.TableData['work_quality3'] && this.formAccount.get('work_quality3').patchValue(this.TableData['work_quality3']);
            this.TableData['work_quality4'] && this.formAccount.get('work_quality4').patchValue(this.TableData['work_quality4']);
            this.TableData['work_quality5'] && this.formAccount.get('work_quality5').patchValue(this.TableData['work_quality5']);
            this.TableData['work_quality6'] && this.formAccount.get('work_quality6').patchValue(this.TableData['work_quality6']);
            // this.formAccount.get('tabledata1').patchValue(this.TableData['tabledata1']);


            if (res['OtherData'] && res['OtherData']['kharidPrakriya']) {
              this.getkharid("y")
            } else {
              this.getkharid("n");
            }
            if (res.length <= 0) {
              let messageDiv: any = document.getElementById('noRecord');
              messageDiv.innerText = 'कुनै विवरण भेटिएन';
            }

          }, (error) => {
            console.info(error);
          });

        }
        this.jqxLoader.close();
        if (result['error']) {
          // let messageDiv: any = document.getElementById('error');
          // messageDiv.innerText = result['error']['message'];
          // this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );


  }

  ngOnInit() {
    let idData = this.activeRoute.snapshot.params['singleid'];
    this.date = this.dcs.getToday()['fulldate'];
    const oType = this.activeRoute.snapshot.params['outputType'];
    this.oType = oType;
    this.ars.getAdministrative({
      activity: this.activeRoute.snapshot.params['id']
    }).subscribe((res) => {
      if (res['data']['workServiceType'] == 'Consumer Committe') {
        this.category = 'उपभोक्ता समिति';
      }
      else if (res['data']['workServiceType'] == 'Contract') {
        this.category = 'ठेक्का मार्फत';
      }
      else if (res['data']['workServiceType'] == 'Amanathan Marfat') {
        this.category = 'अमानत मार्फत';
      }
      else {
        this.category = '';
      }
    }, (error) => {
      console.info(error);

    });
    let post = { type: oType }
    this.ars.getPhysicalDetails(idData, post).subscribe((res) => {
      this.DetailsData = res['data'] || '';
      if (this.DetailsData) {
        const monitoringDetails = this.DetailsData['monitoringDetails'];
        this.formAccount.get('work_quality6').patchValue(monitoringDetails);
      }
      this.TableData = res['OtherData'] || '';
    }, (error) => {
      console.info(error);

    });
    this.ars.getOrg().subscribe((res) => {
      this.progressData = res;
      if (this.progressData && this.progressData.length <= 0) {
        let messageDiv: any = document.getElementById('noRecord');
        messageDiv.innerText = 'कुनै विवरण भेटिएन';
      } else {
        this.formAccount.controls['nagarPramukh'].patchValue(this.progressData[0]['nameOfHead'])

      }

    }, (error) => {
      console.info(error);

    });
  }

  close() {
    let acId = this.activeRoute.snapshot.params['id'];
    let oType = this.activeRoute.snapshot.params['outputType'];
    this.router.navigate(['/progress-reporting/progress-reporting-physical-progress/' + acId, { outputType: oType }]);
  }

  getkharid(check: string) {
    this.ars.getAdministrative({
      activity: this.activeRoute.snapshot.params['id']
    }).subscribe((res) => {
      this.actAddress = res['RemainingPlan'][0]['address'] || '';
      this.actValue = res;
      this.formAccount.controls['estimatedWorkCompletionDate'].patchValue(res['data'].estimatedWorkCompletionDate);
      this.formAccount.get('contractDate').patchValue(res['data'].agreementDate);
      this.formAccount.get('completionDate').patchValue(res['ActivityDetails'].actualCompletedDate);

      if (check === 'n') {
        if (res['CommitteDetails'].length > 0) {
          setTimeout(() => { this.formAccount.controls['kharidPrakriya'].patchValue("उपभोक्ता समिति"); }, 100)

        } else {
          this.formAccount.controls['kharidPrakriya'].patchValue('ठेक्का समिति');
        }
      }
    }, (error) => {
      console.info(error);
    });
  }
  print(value) {
    // console.log(value)
    let idData = this.activeRoute.snapshot.params['singleid'];

    const content = document.getElementById('print-section').innerHTML;
    this.printingService.printContents(content)

    this.ars.storeAnugaman(idData, value).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          // this.aboutFinancialOperationForm.reset();
          //this.SearchDatas = [];
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  calculate() {
    let ns = Number(this.formAccount.get('fromNepalSarkar').value) || 0;
    let fs = Number(this.formAccount.get('fromState').value) || 0;
    let ll = Number(this.formAccount.get('fromLocalLevel').value) || 0;
    let fw = Number(this.formAccount.get('fromWada').value) || 0;
    let cc = Number(this.formAccount.get('fromConsumerCommitte').value) || 0;
    let fo = Number(this.formAccount.get('fromOthers').value) || 0;
    let tg = ns + fs + ll + fw + cc + fo;
    this.formAccount.get('totalGranted').patchValue(tg);
  }

}
