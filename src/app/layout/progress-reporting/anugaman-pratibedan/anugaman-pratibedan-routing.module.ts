import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AnugamanPratibedanComponent } from './anugaman-pratibedan.component';

const routes: Routes = [
  {
    path: '',
    component:AnugamanPratibedanComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnugamanPratibedanRoutingModule { }