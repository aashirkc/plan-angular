import { NgModule } from "@angular/core";
import { ActivityOutputEditComponent } from "./activity-output-edit.component";
import { CommonModule } from "@angular/common";
import { SharedModule } from "app/shared/modules/shared.module";
import { ActivityOutputEditRoutingModule } from "./activity-output-edit.routing.module";

@NgModule({
    imports: [CommonModule, SharedModule, ActivityOutputEditRoutingModule],
    declarations: [ActivityOutputEditComponent]
})
export class ActivityOutputEditModule {}
