import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
    ActivityOutputService,
    PlantUnitService,
    AssetsDetailsCategoryService
} from 'app/shared';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
    selector: 'app-activity-output-edit',
    templateUrl: './activity-output-edit.component.html',
    styleUrls: ['./activity-output-edit.component.scss']
})
export class ActivityOutputEditComponent implements OnInit, AfterViewInit {
    activityId: number;
    activityData: any;
    serviceDetail: any;

    // adapters
    trainingCategoryAdapter: any[] = [];
    organaizeByAdapter: any[] = [];
    serviceDataAdapter: any[] = [];
    assetTypeAdapter: any[] = [];
    assetCreatedAdapter: any[] = [];
    assetSubCategoryAdapter: any[] = [];
    planAdapter: any[];

    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;

    outputType = {
        WOA: {
            nepali: 'सम्पति',
            routeName: 'WorkOutputAsset',
            formControl: this._fb.group({
                assetLocation: ['', Validators.required],
                id: [''],
                assetDetails: this._fb.array([])
            })
        },
        SD: {
            nepali: 'सेवा विवरण',
            routeName: 'ServiceDetails',
            formControl: this._fb.group({
                service: ['', Validators.required],
                durationOfService: ['', Validators.required],
                totalExpectedBeneficiaries: ['', Validators.required],
                id: ['']
            })
        },
        CB: {
            nepali: 'क्षमता अभिबृदी',
            routeName: 'CapacityBuilding',
            formControl: this._fb.group({
                organizedBy: ['', Validators.required],
                subjectOfTraining: ['', Validators.required],
                totalDuration: ['', Validators.required],
                totalTrainess: ['', Validators.required],
                trainingCatagory: ['', Validators.required],
                id: ['']
            })
        }
    };
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _avs: ActivityOutputService,
        private _fb: FormBuilder,
        private _adcs: AssetsDetailsCategoryService,
        private _pu: PlantUnitService,
        private _router: Router
    ) { }

    ngOnInit() { }

    ngAfterViewInit(): void {
        // get adapter
        this.jqxLoader.open();
        this.getAdapter('getTrainingCategory', 'trainingCategoryAdapter');
        this.getAdapter('getOrganizedBy', 'organaizeByAdapter');
        this.getAdapter('getServiceType', 'serviceDataAdapter');
        this.getAdapter('getAssetsCategory', 'assetCreatedAdapter');
        this.getAdapter('getAssetsType', 'assetTypeAdapter');

        this._pu.index({}).subscribe(res => {
            this.planAdapter = res;
        });

        this._activatedRoute.params.subscribe(({ id }) => {
            this.activityId = id;
            this.getCurrentData();
        });
    }

    getCurrentData = () => {
        // get current data now
        this._avs.showActivity(this.activityId).subscribe(res => {
            this.activityData = res;
            const serviceType = this.getOutputType(
                res['outputType'],
                'routeName'
            );
            this._avs
                .getServiceDetail(serviceType, {
                    activity: this.activityId
                })
                .subscribe(
                    response => {
                        this.jqxLoader.close();
                        this.handleResponse(response);
                    },
                    error => () => {
                        this.jqxLoader.close();
                    }
                );
        });
    };

    assetCategory(event, index) {
        //   console.log('hello')
        //   console.log(event.args.item.originalItem.assetsCode)
        //   let value1 = event.args.item.originalItem.assetsCode
        // console.log(event.target.value);
        this._adcs.showChild(event.target.value).subscribe(
            result => {
                this.assetSubCategoryAdapter[index] = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    getAdapter = (functionName, variable) => {
        let subscription;
        switch (functionName) {
            case 'getTrainingCategory':
                subscription = this._avs.getTrainingCategory();
                break;
            case 'getOrganizedBy':
                subscription = this._avs.getOrganizedBy();
                break;
            case 'getServiceType':
                subscription = this._avs.getServiceType();
                break;
            case 'getAssetsCategory':
                subscription = this._avs.getAssetsCategory();
                break;
            case 'getAssetsType':
                subscription = this._avs.getAssetsType();
        }
        subscription.subscribe(
            result => {
                if (variable in this) {
                    this[variable] = result;
                }
            },
            error => {
                console.log(error);
            }
        );
    };

    handleResponse = response => {
        this.serviceDetail = response;
        const control = this.getOutputType(
            this.activityData.outputType,
            'formControl'
        );
        // build the form and patch the values to send
        switch (this.activityData.outputType) {
            case 'WOA':
                if (control) {
                    let i = 0;
                    for (const data of response[0].arrayData) {
                        const assetDetails = this.initassetDetails();
                        assetDetails.patchValue({
                            assetType: data.assetType.id,
                            assetCategory: data.assetCategory.assetsCode,
                            assetSubCatagory: data.assetSubCatagory.assetsCode,
                            assetName: data.assetName,
                            unitParameter: data.unitParameter,
                            totalNoAsset: data.totalNoAsset
                        });
                        this.assetCategory(
                            {
                                target: { value: data.assetCategory.assetsCode }
                            },
                            i++
                        );
                        control.get('assetDetails').push(assetDetails);
                    }

                    control.patchValue({
                        assetLocation: (response[0].assetLocation),
                        id: response[0].id
                    });
                }
                break;
            case 'SD':
                if (control) {
                    control.patchValue({
                        service: response[0].service.id,
                        durationOfService: response[0].durationOfService,
                        totalExpectedBeneficiaries:
                            response[0].totalExpectedBeneficiaries,
                        id: response[0].id
                    });
                }
                break;
            case 'CB':
                if (control) {
                    control.patchValue({
                        organizedBy: response[0].organizedBy.id,
                        subjectOfTraining: response[0].subjectOfTraining,
                        totalDuration: response[0].totalDuration,
                        totalTrainess: response[0].totalTrainess,
                        trainingCatagory: response[0].trainingCatagory.id,
                        id: response[0].id
                    });
                }
                break;
        }
        if (control) {
            setTimeout(() => {
                // error faleko bhayera yesto garnu pareko
                control.markAsDirty();
                for (const ctrl in control.controls) {
                    if (ctrl in control.controls) {
                        control.controls[ctrl].markAsTouched();
                    }
                }
                control.setValue(control.value);
            }, 100);
        }
    };

    getOutputType = (index: string, sub_index: string = 'nepali'): any => {
        if (index in this.outputType) {
            const data = this.outputType[index];
            if (sub_index in data) {
                return data[sub_index];
            } else {
                throw new Error(`sub_index invalid got ${sub_index}`);
            }
        }
        return '';
    };

    initassetDetails = () => {
        return this._fb.group({
            assetType: [''],
            assetCategory: [''],
            assetSubCatagory: [''],
            assetName: [''],
            unitParameter: [''],
            totalNoAsset: ['']
        });
    };

    addItem() {
        const control1 = <FormArray>(
            this.getOutputType('WOA', 'formControl').controls['assetDetails']
        );
        control1.push(this.initassetDetails());
        this.assetChange();
    }
    removeItem(i: number) {
        const control1 = <FormArray>(
            this.getOutputType('WOA', 'formControl').controls['assetDetails']
        );
        let data = {
                assetType: [''],
                assetCategory: [''],
                assetSubCatagory: ['']
            }
        data.assetType = control1.value[i].assetType
        data.assetCategory = control1.value[i].assetCategory
        data.assetSubCatagory = control1.value[i].assetSubCatagory
        this._avs.removeAsset(this.activityId, data).subscribe(response=>{
            if (response && response['error']) {
                this.showError(response['error']['message']);
            } else if (response && response['message']) {
                this.showSuccess(response['message']);
            }
        },
        error => {
            this.showError('हटाउन सकिएन');
            this.jqxLoader.close();
        });
        control1.removeAt(i);
    }

    assetChange() {
        const control1 = <FormArray>(
            this.getOutputType('WOA', 'formControl').controls['assetDetails']
        );
        console.log(control1)
        const assetDetails = control1['controls'][0] || null;
        let def_value = '';
        if (assetDetails) {
            def_value = assetDetails['controls']['assetType'].value;
        }

        let i = 0;
        for (const contr of control1.controls) {
            contr.patchValue({ assetType: def_value });
            // if (i !== 0) {
                // contr['controls']['assetType'].disable();
            // }
            i++;
        }
        control1.markAsDirty();
    }

    Save() {
        this.jqxLoader.open();
        const value = this.getOutputType(
            this.activityData.outputType,
            'formControl'
        ).value;
        this._avs
            .updateActivityOutput(
                this.activityId,
                this.activityData.outputType,
                value
            )
            .subscribe(
                response => {
                    if (response && response['error']) {
                        this.showError(response['error']['message']);
                    } else if (response && response['message']) {
                        this.showSuccess(response['message']);
                        this._router.navigate(['/activity/activity-output']);
                    }
                    this.jqxLoader.close();
                },
                error => {
                    this.showError('सच्याउन सकिएन');
                    this.jqxLoader.close();
                }
            );
    }

    Close() {
        this._router.navigate(['/activity/activity-output']);
    }

    showError(message: string): void {
        const messageDiv: any = document.getElementById('error');
        messageDiv.innerText = message;
        this.errNotification.open();
    }

    showSuccess(message: string): void {
        const messageDiv: any = document.getElementById('message');
        messageDiv.innerText = message;
        this.msgNotification.open();
    }
}
