import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityOutputEditComponent } from './activity-output-edit.component';

describe('ActivityOutputEditComponent', () => {
  let component: ActivityOutputEditComponent;
  let fixture: ComponentFixture<ActivityOutputEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityOutputEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityOutputEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
