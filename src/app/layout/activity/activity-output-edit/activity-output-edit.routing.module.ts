import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ActivityOutputEditComponent } from "./activity-output-edit.component";

const routes: Routes = [
    {
        path: "",
        component: ActivityOutputEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ActivityOutputEditRoutingModule {}
