import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { BudgetTransferComponent } from './budget-transfer.component';
import { BudgetTransferRoutingModule } from './budget-transfer-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BudgetTransferRoutingModule
  ],
  declarations: [
    BudgetTransferComponent,
  ]
})
export class BudgetTransferModule { }
