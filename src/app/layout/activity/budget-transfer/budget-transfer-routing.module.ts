import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BudgetTransferComponent } from './budget-transfer.component';

const routes: Routes = [
  {
    path: '',
    component: BudgetTransferComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetTransferRoutingModule { }
