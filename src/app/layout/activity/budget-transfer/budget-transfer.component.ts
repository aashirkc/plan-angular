import {
    Component,
    ViewChild,
    ViewContainerRef,
    Inject,
    ChangeDetectorRef,
    OnInit
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { CurrentUserService, BudgetTransferService } from '../../../shared';

@Component({
    selector: 'app-budget-transfer',
    templateUrl: './budget-transfer.component.html',
    styleUrls: ['./budget-transfer.component.scss']
})
export class BudgetTransferComponent implements OnInit {
    budgetTransferForm: FormGroup;
    update: boolean = false;
    source: any;
    dataAdapter: any;
    columns: any[];
    editrow: number = -1;
    transData: any;
    activityAdapter: Array<any> = [];
    userData: any = {};
    columngroups: any[];
    lang: any;

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('myGrid') myGrid: jqxGridComponent;

    constructor(
        private fb: FormBuilder,
        private cus: CurrentUserService,
        private bts: BudgetTransferService,
        private cdr: ChangeDetectorRef,
        private router: Router,
        private translate: TranslateService
    ) {
        this.createForm();
        this.getTranslation();
        this.userData = this.cus.getTokenData();
    }

    ngOnInit() {
        this.bts.getActivityList({}).subscribe(
            res => {
                this.activityAdapter = res;
            },
            error => {
                console.info(error);
            }
        );
    }
    getTranslation() {
        this.translate
            .get([
                'SN',
                'ACTIVITY_NAME',
                'FROM',
                'TO',
                'VIEW',
                'BUDGET_AMOUNT',
                'ACTION',
                'REMARKS',
                'EDIT',
                'DEFINE_ACTIVITY',
                'DEFINED_ACTIVITY',
                'regNo',
                'universityRoll',
                'ADD',
                'EDIT',
                'NAME',
                'nepali',
                'english',
                'mobile',
                'VIEW',
                'DETAIL',
                'ACTIONS',
                'UPDATE',
                'DELETE',
                'RELOAD'
            ])
            .subscribe((translation: [string]) => {
                this.transData = translation;
                this.loadGrid();
            });
    }

    loadGridData() {
        this.jqxLoader.open();
        this.bts.index({}).subscribe(
            res => {
                if (res.length == 1 && res[0].error) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = res[0].error;
                    this.errNotification.open();
                    this.source.localdata = [];
                } else {
                    this.source.localdata = res;
                }
                this.myGrid.updatebounddata();
                this.jqxLoader.close();
            },
            error => {
                console.info(error);
                this.jqxLoader.close();
            }
        );
    }

    createForm() {
        this.budgetTransferForm = this.fb.group({
            budgetFrom: ['', Validators.required],
            budgetTo: [''],
            transNo: [''],
            budgetAmt: ['', Validators.required],
            remarks: ['', Validators.required]
        });
    }

    ngAfterViewInit() {
        this.loadGridData();
        this.cdr.detectChanges();
    }

    loadGrid() {
        this.source = {
            datatype: 'json',
            datafields: [
                { name: 'id', type: 'string' },
                { name: 'transNo', type: 'string' },
                { name: 'budgetFrom', type: 'string', map: 'budgetFrom>id' },
                { name: 'budgetTo', type: 'string', map: 'budgetTo>id' },
                {
                    name: 'budgetFromName',
                    type: 'string',
                    map: 'budgetFrom>activityName'
                },
                {
                    name: 'budgetToName',
                    type: 'string',
                    map: 'budgetTo>activityName'
                },
                { name: 'amount', type: 'string' },
                { name: 'remarks', type: 'string' }
            ],
            id: 'id',
            localdata: [],
            pagesize: 100
        };

        this.dataAdapter = new jqx.dataAdapter(this.source);
        this.columns = [
            {
                text: this.transData['SN'],
                sortable: false,
                filterable: false,
                editable: false,
                groupable: false,
                draggable: false,
                resizable: false,
                datafield: 'id',
                columntype: 'number',
                width: 50,
                cellsrenderer: function(row, column, value) {
                    return "<div style='margin:4px;'>" + (value + 1) + '</div>';
                }
            },
            {
                text:
                    this.transData['ACTIVITY_NAME'] +
                    '(' +
                    this.transData['FROM'] +
                    ')',
                datafield: 'budgetFrom',
                displayfield: 'budgetFromName',
                columntype: 'textbox',
                editable: false
            },
            {
                text:
                    this.transData['ACTIVITY_NAME'] +
                    '(' +
                    this.transData['TO'] +
                    ')',
                datafield: 'budgetTo',
                displayfield: 'budgetToName',
                columntype: 'textbox',
                editable: false
            },
            {
                text: this.transData['BUDGET_AMOUNT'],
                datafield: 'amount',
                columntype: 'textbox',
                editable: false
            },
            {
                text: this.transData['REMARKS'],
                datafield: 'remarks',
                columntype: 'textbox',
                editable: false
            }
            // {
            //   text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 80, columntype: 'button',
            //   cellsrenderer: (): string => {
            //     return this.transData['EDIT'];
            //   },
            //   buttonclick: (row: number): void => {
            //     this.editrow = row;
            //     let dataRecord = this.myGrid.getrowdata(this.editrow);
            //     let dt = {};
            //     dt['transNo'] = dataRecord['transNo'] || '';
            //     dt['budgetFrom'] = dataRecord['budgetFrom'];
            //     dt['budgetTo'] = dataRecord['budgetTo'];
            //     dt['budgetAmt'] = dataRecord['amount'];
            //     dt['remarks'] = dataRecord['remarks'];
            //     this.budgetTransferForm.setValue(dt);
            //     this.update = true;
            //   }
            // }
        ];
        this.columngroups = [
            { text: this.transData['ACTION'], align: 'center', name: 'action' }
        ];
    }
    saveBtn(post) {
        this.jqxLoader.open();
        const _formData = new FormData();
        for (let data in post) {
            _formData.append(data, post[data]);
        }

        this.bts.store(_formData).subscribe(
            result => {
                if (result['message']) {
                    let messageDiv: any = document.getElementById('message');
                    messageDiv.innerText = result['message'];
                    this.msgNotification.open();
                    this.budgetTransferForm.reset();
                    this.loadGridData();
                }
                this.jqxLoader.close();
                if (result['error']) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = result['error']['message'];
                    this.errNotification.open();
                }
            },
            error => {
                this.jqxLoader.close();
                console.info(error);
            }
        );
    }

    updateBtn(post) {
        this.jqxLoader.open();
        const _formData = new FormData();
        for (let data in post) {
            _formData.append(data, post[data]);
        }
        this.bts.update(post['transNo'], _formData).subscribe(
            result => {
                this.jqxLoader.close();
                if (result['message']) {
                    this.update = false;
                    let messageDiv: any = document.getElementById('message');
                    messageDiv.innerText = result['message'];
                    this.msgNotification.open();
                    this.budgetTransferForm.reset();
                    this.loadGridData();
                }
                if (result['error']) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = result['error']['message'];
                    this.errNotification.open();
                }
            },
            error => {
                this.jqxLoader.close();
                console.info(error);
            }
        );
    }
    cancelUpdateBtn() {
        this.update = false;
        this.budgetTransferForm.reset();
        this.loadGridData();
    }
}
