import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ActivityComponent } from "./activity.component";

const routes: Routes = [
    {
        path: "",
        component: ActivityComponent,
        children: [
            { path: "", redirectTo: "activity-details", pathMatch: "full" },
            {
                path: "activity-details",
                loadChildren:
                    "./activity-details/activity-details.module#ActivityDetailsModule"
            },
            {
                path: "manage-activity",
                loadChildren:
                    "./manage-activity/manage-activity.module#ManageActivityModule"
            },
            {
                path: "activity-details/edit/:id",
                loadChildren:
                    "./activity-details-edit/activity-details-edit.module#ActivityDetailsEditModule"
            },
            {
                path: "activity-output",
                loadChildren:
                    "./activity-output/activity-output.module#ActivityOutputModule"
            },
            {
                path: "activity-output-edit",
                loadChildren:
                    "./activity-output-edit/activity-output-edit.module#ActivityOutputEditModule"
            },
            {
                path: "view-activity",
                loadChildren:
                    "./view-activity/view-activity.module#ViewActivityModule"
            },
            {
                path: "work-output-asset",
                loadChildren:
                    "./work-output-asset/work-output-asset.module#WorkOutputAssetModule"
            },
            {
                path: "technical-approve",
                loadChildren:
                    "./technical-approve/technical-approve.module#TechnicalApproveModule"
            },
            {
                path: "budget-transfer",
                loadChildren:
                    "./budget-transfer/budget-transfer.module#BudgetTransferModule"
            },
            {
                path: "program-transfer",
                loadChildren:
                    "./program-transfer/program-transfer.module#ProgramTransferModule"
            },
            {
                path: "budget-addition",
                loadChildren:
                    "./budget-addition/budget-addition.module#BudgetAdditionModule"
            }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ActivityRoutingModule {}
