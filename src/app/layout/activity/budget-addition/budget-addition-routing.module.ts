import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BudgetAdditionComponent } from './budget-addition.component';

const routes: Routes = [
  {
    path: '',
    component: BudgetAdditionComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetAdditionRoutingModule { }
