import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { BudgetAdditionComponent } from './budget-addition.component';
import { BudgetAdditionRoutingModule } from './budget-addition-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BudgetAdditionRoutingModule
  ],
  declarations: [
    BudgetAdditionComponent,
  ]
})
export class BudgetAdditionModule { }
