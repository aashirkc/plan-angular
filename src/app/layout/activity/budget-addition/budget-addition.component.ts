import {
    Component,
    ViewChild,
    ViewContainerRef,
    Inject,
    ChangeDetectorRef,
    OnInit,
    AfterViewInit
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { CurrentUserService, BudgetTransferService } from '../../../shared';

@Component({
    selector: 'app-budget-addition',
    templateUrl: './budget-addition.component.html',
    styleUrls: ['./budget-addition.component.scss']
})
export class BudgetAdditionComponent implements OnInit, AfterViewInit {
    budgetTransferForm: FormGroup;
    update: boolean = false;
    source: any;
    dataAdapter: any;
    columns: any[];
    editrow: number = -1;
    transData: any;
    activityAdapter: Array<any> = [];
    userData: any = {};
    columngroups: any[];
    lang: any;

    // handle updload file input
    selectedFiles: any[] = [];
    API_URL_DOC: string;

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('myGrid') myGrid: jqxGridComponent;

    constructor(
        private fb: FormBuilder,
        private cus: CurrentUserService,
        private bts: BudgetTransferService,
        private cdr: ChangeDetectorRef,
        private router: Router,
        private translate: TranslateService,
        @Inject('API_URL_DOC') API_URL_DOC: string
    ) {
        this.createForm();
        this.getTranslation();
        this.userData = this.cus.getTokenData();
        this.API_URL_DOC = API_URL_DOC;
    }

    ngOnInit() {
        this.bts.getActivityList({}).subscribe(
            res => {
                this.activityAdapter = res;
            },
            error => {
                console.log(error);
            }
        );
    }
    getTranslation() {
        this.translate
            .get([
                'SN',
                'ACTIVITY_NAME',
                'DONOR',
                'FROM',
                'TO',
                'VIEW',
                'ADDITION_AMOUNT',
                'ACTION',
                'REMARKS',
                'EDIT',
                'DEFINE_ACTIVITY',
                'DEFINED_ACTIVITY',
                'regNo',
                'universityRoll',
                'ADD',
                'EDIT',
                'NAME',
                'nepali',
                'english',
                'mobile',
                'VIEW',
                'DETAIL',
                'ACTIONS',
                'UPDATE',
                'DELETE',
                'RELOAD'
            ])
            .subscribe((translation: [string]) => {
                this.transData = translation;
                this.loadGrid();
            });
    }

    loadGridData() {
        this.jqxLoader.open();
        this.bts.indexAddition({}).subscribe(
            res => {
                if (res.length == 1 && res[0].error) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = res[0].error;
                    this.errNotification.open();
                    this.source.localdata = [];
                } else {
                    this.source.localdata = res;
                }
                this.myGrid.updatebounddata();
                this.jqxLoader.close();
            },
            error => {
                console.info(error);
                this.jqxLoader.close();
            }
        );
    }

    createForm() {
        this.budgetTransferForm = this.fb.group({
            donor: ['', Validators.required],
            budgetTo: [''],
            transNo: [''],
            budgetAmt: ['', Validators.required],
            remarks: ['']
        });
    }

    ngAfterViewInit() {
        this.loadGridData();
        this.cdr.detectChanges();
    }

    handleFileInput = event => {
        this.selectedFiles = event.target.files;
        console.log(this.selectedFiles);
    };

    loadGrid() {
        this.source = {
            datatype: 'json',
            datafields: [
                { name: 'id', type: 'string' },
                { name: 'transNo', type: 'string' },
                { name: 'donor', type: 'string' },
                { name: 'budgetTo', type: 'string', map: 'budgetTo>id' },
                {
                    name: 'budgetToName',
                    type: 'string',
                    map: 'budgetTo>activityName'
                },
                { name: 'amount', type: 'string' },
                { name: 'remarks', type: 'string' }
            ],
            id: 'id',
            localdata: [],
            pagesize: 100
        };

        this.dataAdapter = new jqx.dataAdapter(this.source);
        this.columns = [
            {
                text: this.transData['SN'],
                sortable: false,
                filterable: false,
                editable: false,
                groupable: false,
                draggable: false,
                resizable: false,
                datafield: 'id',
                columntype: 'number',
                width: 50,
                cellsrenderer: function(row, column, value) {
                    return "<div style='margin:4px;'>" + (value + 1) + '</div>';
                }
            },
            {
                text: this.transData['DONOR'],
                datafield: 'donor',
                width: 150,
                columntype: 'textbox',
                editable: false
            },
            {
                text:
                    this.transData['ACTIVITY_NAME'] +
                    '(' +
                    this.transData['TO'] +
                    ')',
                datafield: 'budgetTo',
                displayfield: 'budgetToName',
                columntype: 'textbox',
                editable: false
            },
            {
                text: this.transData['ADDITION_AMOUNT'],
                width: 150,
                datafield: 'amount',
                columntype: 'textbox',
                editable: false
            },
            {
                text: this.transData['REMARKS'],
                width: 250,
                datafield: 'remarks',
                columntype: 'textbox',
                editable: false
            },
            {
                text: 'फाइल',
                datafield: 'imageName',
                editable: false,
                cellsrenderer: row => {
                    const image =
                        this.source.localdata &&
                        this.source.localdata[row]['imageName'];
                    return image
                        ? `<a href="${this.API_URL_DOC}/${image}" target="_blank">हेर्नुहोस्</a>`
                        : 'छैन्';
                }
            }
        ];
        this.columngroups = [
            { text: this.transData['ACTION'], align: 'center', name: 'action' }
        ];
    }
    saveBtn(post) {
        this.jqxLoader.open();
        const _formData = new FormData();
        for (const data in post) {
            if (post[data]) {
                _formData.append(data, post[data]);
            }
        }

        for (const file of this.selectedFiles) {
            _formData.append('document', file, file.name);
        }

        this.bts.store(_formData).subscribe(
            result => {
                if (result['message']) {
                    let messageDiv: any = document.getElementById('message');
                    messageDiv.innerText = result['message'];
                    this.msgNotification.open();
                    this.budgetTransferForm.reset();
                    this.loadGridData();
                }
                this.jqxLoader.close();
                if (result['error']) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = result['error']['message'];
                    this.errNotification.open();
                }
            },
            error => {
                this.jqxLoader.close();
                console.info(error);
            }
        );
    }
}
