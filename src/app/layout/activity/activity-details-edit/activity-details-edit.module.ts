import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityDetailsEditComponent } from './activity-details-edit.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ActivityDetailsEditRoutingModule } from './activity-details-edit-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ActivityDetailsEditRoutingModule
  ],
  declarations: [ActivityDetailsEditComponent]
})
export class ActivityDetailsEditModule { }
