import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityDetailsEditComponent } from './activity-details-edit.component';

const routes: Routes = [
  {
    path: '',
    component: ActivityDetailsEditComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityDetailsEditRoutingModule { }
