import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { ActivityDetailService, ActivityMasterService, AgencyTypeService, UnicodeTranslateService, ActivityFocusAreaMasterService, InclusionGroupService, ActivityDetails,CustomValidators } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxRadioButtonComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxradiobutton';
import { jqxCheckBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcheckbox';
import { TranslateService } from '@ngx-translate/core';
import { ProgramsService } from '../../../shared/services/programs.service';
import { DivisionServiceService } from 'app/shared/services/division-service.service';
import { asElementData } from '@angular/core/src/view';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-activity-details-edit',
  templateUrl: './activity-details-edit.component.html',
  styleUrls: ['./activity-details-edit.component.scss']
})
export class ActivityDetailsEditComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('programNameList') programNameList: jqxComboBoxComponent;

  // @ViewChild('activityFocusAreaCombo') activityFocusAreaCombo: jqxComboBoxComponent;
  @ViewChild('typeActivityCombo') typeActivityCombo: jqxComboBoxComponent;
  @ViewChild('activityFor1Combo') activityFor1Combo: jqxComboBoxComponent;
  @ViewChild('activityFor2Combo') activityFor2Combo: jqxComboBoxComponent;
  @ViewChild('activityFor3Combo') activityFor3Combo: jqxComboBoxComponent;
  // @ViewChild('activityCatCombo') activityCatCombo: jqxComboBoxComponent;
  // @ViewChild('implementingCombo') implementingCombo: jqxComboBoxComponent;
  // @ViewChild('executingCombo') executingCombo: jqxComboBoxComponent;
  @ViewChild('startMonthCombo') startMonthCombo: jqxComboBoxComponent;
  @ViewChild('startYearCombo') startYearCombo: jqxComboBoxComponent;

  @ViewChild('womenCheck') womenCheck: jqxCheckBoxComponent;
  @ViewChild('childrenCheck') childrenCheck: jqxCheckBoxComponent;

  @ViewChild('diffAbledRadioY') diffAbledRadioY: jqxRadioButtonComponent;
  @ViewChild('diffAbledRadioN') diffAbledRadioN: jqxRadioButtonComponent;
  @ViewChild('livelihoodRadioY') livelihoodRadioY: jqxRadioButtonComponent;
  @ViewChild('livelihoodRadioN') livelihoodRadioN: jqxRadioButtonComponent;
  @ViewChild('disasterRadioY') disasterRadioY: jqxRadioButtonComponent;
  @ViewChild('disasterRadioN') disasterRadioN: jqxRadioButtonComponent;
  @ViewChild('costlessRadioY') costlessRadioY: jqxRadioButtonComponent;
  @ViewChild('costlessRadioN') costlessRadioN: jqxRadioButtonComponent;
  @ViewChild('activityTypeRadioF') activityTypeRadioF: jqxRadioButtonComponent;
  @ViewChild('activityTypeRadioM') activityTypeRadioM: jqxRadioButtonComponent;
  @ViewChild('activityTypeRadioU') activityTypeRadioU: jqxRadioButtonComponent;


  private dataLoaded: boolean = false;
  observerCount: number = 0;
  dataCount: number = 0;
  totalRs: any;

  activityTypeAdapter: any = [];
  activityFocusAreaAdapter: any = [[], [], []];
  activityFocusArea2Adapter: any = [];
  activityFocusArea3Adapter: any = [];
  activityCategoryAdapter: any = [];
  activityForAdapter: any = [];
  agencyAdapter: any = [];

  implementingAgencyType: any = [[], [], []];
  implementingAgencyTypeName: any = [];
  executingAgencyType: any = [[], [], []];
  executingAgencyTypeName: any = [];

  programId: number = 0;

  /**
   * Global Variable decleration
   */
  adForm: FormGroup;

  source: any = [
    { name: 'test 1', id: '1' },
    { name: 'test 2', id: '2' },
    { name: 'test 3', id: '3' },
    { name: 'test 4', id: '4' },
    { name: 'test 5', id: '5' }
  ];

  monthAdapter: any = [
    { id: "01" },
    { id: "02" },
    { id: "03" },
    { id: "04" },
    { id: "05" },
    { id: "06" },
    { id: "07" },
    { id: "08" },
    { id: "09" },
    { id: "10" },
    { id: "11" },
    { id: "12" }
  ];

  yearAdapter: any = [];

  activityCategoryLevel: any = [];

  activityDetailId: string;
  activityDetailData: any;
  lang: any;
  dropDownSource: any = [];
  divisionAdpater: any = [];
  yearlyProgramId: number = 0;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private ads: ActivityDetailService,
    private ams: ActivityMasterService,
    private ats: AgencyTypeService,
    private afams: ActivityFocusAreaMasterService,
    private igs: InclusionGroupService,
    private translate: TranslateService,
    private unicode: UnicodeTranslateService,
    private ps: ProgramsService,
    private location: Location,
    private aops: DivisionServiceService,
  ) {
    this.createForm();
    this.getTranslation();
    this.getYears(this.translate.currentLang.substr(0,2));
  }

  transData: any;
  getTranslation() {
    this.translate.get(['SN', 'ITEM', 'NAME', 'CODE', 'GROUPNAME', "TRANSACTION", "DELETESELECTED",]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  getYears(lang) {
    let currentYear;
    if (lang == "en") {
      currentYear = new Date().getFullYear();
    } else if (lang == "ne") {
      currentYear = new Date().getFullYear() + 57;
    }

    let years = [];
    let index = 1;
    for (let i = 0; i < 30; i++) {
      years[i] = currentYear - i;
    }
    for (let i = 30; i < 60; i++) {
      let a = currentYear + index;
      index = index + 1;
      years.unshift(a);
    }
    this.yearAdapter = years;
  }
  Change(event: any): void {
    let id = event.args.item.originalItem['id'];
    this.ps.getBudget(id).subscribe(
      result => {
        console.log(result);
        if (result) {
          this.totalRs = Number(result);
        } else {
          this.totalRs = '';
        }
      }, error => {
        this.totalRs = '';
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }
  /**
   * Create the form group
   * with given form control name
   */
  createForm() {
    this.adForm = this.fb.group({
      'activityName': [null, Validators.required],
      'activityDescription': [null],
      'totalEstimatedCost': [null, Validators.required],
      'totalDurationMonth': [''],
      'totalDurationDays': [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(29)])],
      'focusAreaLevel1': ["", Validators.required],
      'focusAreaLevel2': [""],
      'focusAreaLevel3': [""],
      'implementingAgencyTypeLevel1': ["", Validators.required],
      'implementingAgencyTypeLevel2': [''],
      'implementingAgencyTypeLevel3': [''],
      'activityType': [""],
      'activityFor': [""],
      'activityForLevel2': [""],
      'activityForLevel3': [""],
      // 'benifitedHouses': [0],
      // 'benifitedPopulation': [0],
      // 'befinitedOrganizations': [0],
      // 'othersBenifited': [0],
      'estimatedWorkCompletionDate': [''],
      'agreementDate': [''],
      // 'malePopulation': [0],
      // 'femalePopulation': [0],
      // 'dalitPopulation': [0],
      // 'janajatiPopulation': [0],
      // 'othersPopulation': [0],

      'address': ["", Validators.required],
      'division': [''],
      'plannedStartDate':["",  Validators.compose([Validators.required, CustomValidators.checkDate])],
      // 'plannedStartMonth': ["", Validators.required],
      // 'plannedStartYear': ["", Validators.required],
      // 'programId': [null, Validators.required],

    });
  }


  ngOnInit() {

    this.aops.index({}).subscribe(
        res => {
            this.divisionAdpater = res;
        })

    this.lang = this.translate.currentLang.substr(0,2);
    this.activityCategoryLevel[0] = [];
    this.implementingAgencyTypeName[0] = null;
    this.executingAgencyTypeName[0] = null;
    this.getAgencyType(0, 0);
    this.ams.index({}).subscribe(
      result => {
        this.activityTypeAdapter = result;
      },
      error => {
        console.log(error);
      }
    );

    this.afams.show(0).subscribe(
      result => {
        this.activityFocusAreaAdapter[0] = result;
      },
      error => {
        console.log(error);
      }
    );

    this.igs.index({}).subscribe(
      result => {
        this.activityForAdapter = result;
      },
      error => {
        console.log(error);
      }
    );
    this.ps.getPrograms({
      planYear:'',
      focusAreaLevel1:'',
      implementingAgencyType1:''
    }).subscribe(
      res => {
        this.dropDownSource = res;
      }
    )
  }


  ngAfterViewInit() {
    this.activityDetailId = this.route.snapshot.paramMap.get('id');
    this.getActivityDetail(this.activityDetailId);
    this.unicode.initUnicode();

  }

  ngOnDestroy() {

  }
  program: any;
  getProgram() {
    let focusAreaId = this.adForm.get('focusAreaLevel1').value;
    let subFocusAreaId = this.adForm.get('focusAreaLevel2').value;
    let implementingAgencyType1 = this.adForm.get('implementingAgencyTypeLevel1').value;
    let implementingAgencyType2 = this.adForm.get('implementingAgencyTypeLevel2').value;
    let implementingAgencyType3 = this.adForm.get('implementingAgencyTypeLevel3').value;
    let post = [];
    post['planYear'] = '';
    post['focusAreaLevel1'] = focusAreaId;
    post['focusAreaLevel2'] = subFocusAreaId;
    post['implementingAgencyType1'] = implementingAgencyType1;
    post['implementingAgencyType2'] = implementingAgencyType2;
    post['implementingAgencyType3'] = implementingAgencyType3;

    this.ps.getPrograms(post).subscribe(
      result => {
        this.dropDownSource = result;
        setTimeout(() => {
          this.program = this.dropDownSource.filter(d => d.id == this.yearlyProgramId);
          this.programNameList.selectItem(this.yearlyProgramId);
          this.programNameList.disabled(true);
        }, 100);

        this.jqxLoader.close();
      }, error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }
  getActivityDetail(id) {
    this.jqxLoader.open();
    this.ads.show(id).subscribe(
      result => {

        this.activityDetailData = result;
        this.yearlyProgramId = <number>result['programId'];
        if (result['activityForWomen']) this.womenCheck.check();
        if (result['activityForChildren']) this.childrenCheck.check();

        (result['activityForAbled'] == 'Y') ? this.diffAbledRadioY.check() : this.diffAbledRadioN.check();
        (result['activityOpportunities'] == 'Y') ? this.livelihoodRadioY.check() : this.livelihoodRadioN.check();
        (result['costlessActivity'] == 'Y') ? this.costlessRadioY.check() : this.costlessRadioN.check();
        (result['disasterManagementPlan'] == 'Y') ? this.disasterRadioY.check() : this.disasterRadioN.check();
        if (result['activityTypeDetails'] == 'FW') {
          this.activityTypeRadioF.check();
        } else if (result['activityTypeDetails'] == 'MW') {
          this.activityTypeRadioM.check();
        } else {
          this.activityTypeRadioU.check();
        }

        this.activityTypeDetail = result['activityTypeDetails'];

        if (result['focusAreaLevel1']) this.dataCount += 1; this.activityFocusAreaChange(result['focusAreaLevel1'], 0);
        if (result['focusAreaLevel2']) this.dataCount += 1; this.activityFocusAreaChange(result['focusAreaLevel2'], 1);



        if (result['implementingAgencyTypeLevel1']) {
          this.dataCount += 1;
          this.implementingAgencyChange(result['implementingAgencyTypeLevel1'], 0);
        }
        if (result['implementingAgencyTypeLevel2'] != null) {
          this.implementingAgencyTypeName[1] = result['implementingAgencyTypeLevel1Name'];
          this.dataCount += 1;
          this.implementingAgencyChange(result['implementingAgencyTypeLevel2'], 1);
        }

        if (result['implementingAgencyTypeLevel3'] != null) {
          this.implementingAgencyTypeName[2] = result['implementingAgencyTypeLevel2Name'];
        }

        if (result['executingAgencyTypeLevel3'] != null) {
          this.executingAgencyTypeName[2] = result['executingAgencyTypeLevel2Name'];
        }
        let formData = new ActivityDetails(result);
        this.adForm.patchValue(formData);
        // this.adForm.get('totalDurationMonth').patchValue(result['totalDurationMonth'] || null);
        // this.adForm.get('totalDurationDays').patchValue(result['totalDurationDays'] || null);
        this.jqxLoader.close();
      },
      error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  setDataLoaded(count) {
    this.observerCount += 1;
    if (this.dataCount == this.observerCount) {
      this.dataLoaded = true;
      if (this.activityDetailData['implementingAgencyTypeLevel2'] != null) this.adForm.get('implementingAgencyTypeLevel2').patchValue(this.activityDetailData['implementingAgencyTypeLevel2'] || null);
      if (this.activityDetailData['implementingAgencyTypeLevel3'] != null) this.adForm.get('implementingAgencyTypeLevel3').patchValue(this.activityDetailData['implementingAgencyTypeLevel3'] || null);
      if (this.activityDetailData['focusAreaLevel2']) this.adForm.get('focusAreaLevel2').patchValue(this.activityDetailData['focusAreaLevel2'] || null);
      if (this.activityDetailData['focusAreaLevel3']) this.adForm.get('focusAreaLevel3').patchValue(this.activityDetailData['focusAreaLevel3'] || null);
    }
  }

  activityFocusAreaChange(value, index) {
    this.setActivityFocusAreaAdapter(value, index);
    this.getProgram();
  }

  activityCategoryChange(value, i) {
    let nextIndex = i + 1;
    this.setActivityFocusAreaAdapter(value, nextIndex);
  }

  setActivityFocusAreaAdapter(focusCode, index) {
    if (index == 0) this.activityFocusAreaAdapter[1] = []; this.activityFocusAreaAdapter[2] = [];
    if (index == 1) this.activityFocusAreaAdapter[2] = [];

    this.afams.show(focusCode).subscribe(
      result => {
        if (result['length'] > 0) {
          this.activityFocusAreaAdapter[index + 1] = result;
        }
        if (this.dataLoaded == false) {
          this.setDataLoaded(1);
        }

      },
      error => {
        console.log(error);
      }
    )
  }

  implementingAgencyChange(selectedEvent, i) {
    if (selectedEvent != null) {
      let agencyCode = selectedEvent;

      if (selectedEvent && selectedEvent.target) {
        let displayText = selectedEvent.target.selectedOptions[0].text;
        this.implementingAgencyTypeName[i + 1] = displayText;
        agencyCode = selectedEvent.target.value;
      }
      let nextIndex = i + 1;
      this.getAgencyType(agencyCode, nextIndex, 'implementing');
      this.getProgram();
    }
  }

  // executingAgencyChange(selectedEvent, i) {
  //   if (selectedEvent != null) {
  //     let agencyCode = selectedEvent;
  //     if (selectedEvent.target) {
  //       let displayText = selectedEvent.target.selectedOptions[0].text;
  //       this.executingAgencyTypeName[i + 1] = displayText;
  //       agencyCode = selectedEvent.target.value;
  //     }
  //     let nextIndex = i + 1;
  //     this.getAgencyType(agencyCode, nextIndex, 'executing');
  //   }
  // }

  getAgencyType(agencyCode = 0, index = 0, type = null) {


    if (index == 1 && type == 'implementing') {
      this.implementingAgencyType[1] = []; this.implementingAgencyType[2] = [];
    }
    if (index == 2 && type == 'implementing') {
      this.implementingAgencyType[2] = [];
    }

    if (index == 1 && type == 'executing') {
      this.executingAgencyType[1] = []; this.executingAgencyType[2] = [];
    }
    if (index == 2 && type == 'executing') {
      this.executingAgencyType[2] = [];
    }

    this.ats.show(agencyCode).subscribe(
      result => {
        if (result['length'] > 0) {
          // this.implementingAgencyType[0] = [];
          if (type == 'implementing') {
            this.implementingAgencyType[index] = result;

          }
          if (type == 'executing') {
            this.executingAgencyType[index] = result;

          }
          if (index == 0 && !type) {
            this.implementingAgencyType[index] = result;
            this.executingAgencyType[index] = result;
          }

        }
        if (this.dataLoaded == false) {
          this.setDataLoaded(1);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  AbledPerson: string = '';
  AbledPersonChange(event, selected) {
    this.AbledPerson = selected;
  }

  LivelihoodOpp: string = '';
  LivelihoodOppChange(event, selected) {
    this.LivelihoodOpp = selected;
  }

  CostlessActivity: string = '';
  CostlessActivityChange(event, selected) {
    this.CostlessActivity = selected;
  }

  disastarMgmt: string = '';
  disastarMgmtChange(event, selected) {
    this.disastarMgmt = selected;
  }

  activityTypeDetail: string = '';
  ActivityTypeChange(event, selected) {
    this.activityTypeDetail = selected;
  }

  showError = (msg: string) => {
    let messageDiv: any = document.getElementById('error');
    messageDiv.innerText = msg;
    this.errNotification.open();
  }


  /**
   * Function triggered when save button is clicked
   * @param formData
   */
  save(formData) {
    // formData['activityType'] = this.typeActivityCombo.val() || '';
    // formData['activityFor'] = this.activityFor1Combo.val() || null;
    // formData['activityFor1'] = this.activityFor2Combo.val() || null;
    // formData['activityFor2'] = this.activityFor3Combo.val() || null;
    // formData['fundProvider'] = '';



    for (let i = 0; i < 1; i++) {
      let index = i + 1;
      formData['executingAgencyTypeLevel' + index] = "1";
    }

    if(this.program && this.program[0] && (this.program[0]['amount'] < formData['totalEstimatedCost'])){
        this.showError('बजेट रकम अनुमानित भन्दा बढी भयो ');
        return;
    }

    formData['activityForWomen'] = (this.womenCheck.val()) ? 1 : 0 || '';
    formData['activityForChildren'] = (this.childrenCheck.val()) ? 1 : 0 || '';
    formData['activityForAbled'] = this.AbledPerson;
    formData['activityOpportunities'] = this.LivelihoodOpp;
    formData['disasterManagementPlan'] = this.disastarMgmt;
    formData['costlessActivity'] = this.CostlessActivity;
    formData['activityTypeDetails'] = this.activityTypeDetail;
    formData['programId'] = this.programNameList.val();
    formData['outputType'] = this.activityDetailData['outputType']
    if (formData['activityName'] && formData['activityFor']) {
      this.jqxLoader.open();
      this.ads.update(this.activityDetailData.id, formData).subscribe(
        result => {
          console.log(result);
          if (result['message']) {
            this.resetForm();
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();

            setTimeout(() => {
              this.closeForm();
            }, 1500);

          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          if(error.constructor === HttpErrorResponse){
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = error['error']['error'];
            this.errNotification.open();
          }
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Select Group Name';
      this.errNotification.open();
    }
  }

  closeForm() {
    this.location.back();
  }

  resetForm() {
    this.adForm.reset();
    // this.myComboBox.clearSelection();
  }

}
