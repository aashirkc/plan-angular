import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityDetailsEditComponent } from './activity-details-edit.component';

describe('ActivityDetailsEditComponent', () => {
  let component: ActivityDetailsEditComponent;
  let fixture: ComponentFixture<ActivityDetailsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityDetailsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityDetailsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
