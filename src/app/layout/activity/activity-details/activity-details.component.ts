import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl, ValidationErrors } from '@angular/forms';
import { ActivityDetailService, ActivityMasterService, AgencyTypeService, ActivityFocusAreaMasterService, InclusionGroupService, CustomValidators, UnicodeTranslateService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxRadioButtonComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxradiobutton';
import { jqxCheckBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcheckbox';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { jqxDropDownListComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxdropdownlist';
import { ProgramsService } from '../../../shared/services/programs.service';
import { DateConverterService } from '../../../shared';
import { DivisionServiceService } from 'app/shared/services/division-service.service';

@Component({
  selector: 'app-activity-details',
  templateUrl: './activity-details.component.html',
  styleUrls: ['./activity-details.component.scss']
})
export class ActivityDetailsComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('list') list: jqxComboBoxComponent;

  // @ViewChild('activityFocusAreaCombo') activityFocusAreaCombo: jqxComboBoxComponent;
  @ViewChild('typeActivityCombo') typeActivityCombo: jqxComboBoxComponent;
  @ViewChild('activityFor1Combo') activityFor1Combo: jqxComboBoxComponent;
  @ViewChild('activityFor2Combo') activityFor2Combo: jqxComboBoxComponent;
  @ViewChild('activityFor3Combo') activityFor3Combo: jqxComboBoxComponent;
  // @ViewChild('activityCatCombo') activityCatCombo: jqxComboBoxComponent;
  // @ViewChild('implementingCombo') implementingCombo: jqxComboBoxComponent;
  // @ViewChild('executingCombo') executingCombo: jqxComboBoxComponent;
  // @ViewChild('startMonthCombo') startMonthCombo: jqxComboBoxComponent;
  // @ViewChild('startYearCombo') startYearCombo: jqxComboBoxComponent;

  @ViewChild('womenCheck') womenCheck: jqxCheckBoxComponent;
  @ViewChild('childrenCheck') childrenCheck: jqxCheckBoxComponent;
  @ViewChild('diffAbledRadio') diffAbledRadio: jqxRadioButtonComponent;
  @ViewChild('livelihoodRadio') livelihoodRadio: jqxRadioButtonComponent;
  @ViewChild('disasterRadio') disasterRadio: jqxRadioButtonComponent;
  @ViewChild('costlessRadio') costlessRadio: jqxRadioButtonComponent;
  @ViewChild('activityTypeRadio') activityTypeRadio: jqxRadioButtonComponent;


  activityTypeAdapter: any = [];
  activityFocusAreaAdapter: any = [];
  activityCategoryAdapter: any = [];
  activityForAdapter: any = [];
  agencyAdapter: any = [];
  totalRs: any;
  lang: any;
  activitySubCategoryData: any = {};
  dropDownSource: any = [

  ];

  /**
   * Global Variable decleration
   */
  adForm: FormGroup;

  source: any = [
    { name: 'test 1', id: '1' },
    { name: 'test 2', id: '2' },
    { name: 'test 3', id: '3' },
    { name: 'test 4', id: '4' },
    { name: 'test 5', id: '5' }
  ];

  monthAdapter: any = [
    { id: "01" },
    { id: "02" },
    { id: "03" },
    { id: "04" },
    { id: "05" },
    { id: "06" },
    { id: "07" },
    { id: "08" },
    { id: "09" },
    { id: "10" },
    { id: "11" },
    { id: "12" }
    ];

  yearAdapter: any = [];

  activityCategoryLevel: any = [];
  implementingAgencyType: any = [];
  divisionAdpater: any = [];
  implementingAgencyTypeName: any = [];
  // executingAgencyType: any = [];
  // executingAgencyTypeName: any = [];

  constructor(
    private fb: FormBuilder,
    private ads: ActivityDetailService,
    private ams: ActivityMasterService,
    private ats: AgencyTypeService,
    private afams: ActivityFocusAreaMasterService,
    private igs: InclusionGroupService,
    private translate: TranslateService,
    private location: Location,
    private router: Router,
    private unicode: UnicodeTranslateService,
    private ds : DateConverterService,
    private ps: ProgramsService,
    private aops: DivisionServiceService,
  ) {
    this.createForm();
    this.getTranslation();
    this.getYears(this.translate.currentLang.substr(0,2));
  }

  transData: any;
  getTranslation() {
    this.translate.get(['SN', 'ITEM', 'NAME', 'CODE', 'GROUPNAME', "TRANSACTION", "DELETESELECTED",]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }
  ngAfterViewInit() {
    this.unicode.initUnicode();
    let a = this.ds.getToday();

    // this.startYearCombo.selectItem(a['year']);
    // this.startMonthCombo.selectItem(a['month']);
  }
  getYears(lang) {
    let currentYear;
    if (lang == "en") {
      currentYear = new Date().getFullYear();
    } else if (lang == "ne") {
      currentYear = new Date().getFullYear() + 57;
    }

    let years = [];
    let yr = [];
    let index = 1;
    for (let i = 0; i < 30; i++) {
      years[i] = currentYear - i;
    }
    for (let i = 30; i < 60; i++) {
      let a = currentYear + index;
      index = index + 1;
      years.unshift(a);
    }
    this.yearAdapter = years;
  }

  /**
   * Create the form group
   * with given form control name
   */


  createForm() {
    this.adForm = this.fb.group({
      'activityName': ['', Validators.required],
      'address': ['', Validators.required],
      'activityDescription': [''],
      'totalEstimatedCost': ['', Validators.required],
      'plannedStartDate': ['', Validators.compose([ CustomValidators.checkDate])],
      'totalDurationMonth': [0, Validators.required],
      'totalDurationDays': [0, Validators.compose([
          Validators.required,
          Validators.min(0),
          Validators.max(29)]
        )],
      'focusArea': ["", Validators.required],
      'activityType': ['', Validators.required],
      'activityFor': ['', Validators.required],
      'activityForLevel2': [''],
      'activityForLevel3': [''],
      'division':[''],
      // 'benifitedHouses': [0],
      // 'benifitedPopulation': [0],
      // 'befinitedOrganizations': [0],
      // 'othersBenifited': [0],
      'estimatedWorkCompletionDate': [''],
      'agreementDate': [''],
      // 'malePopulation': [0],
      // 'femalePopulation': [0],
      // 'dalitPopulation': [0],
      // 'janajatiPopulation': [0],
      // 'othersPopulation': [0],
      'activityLevels': this.fb.array([
        this.activityFocusAreaArray(),
      ]),
      'implementingAgencyTypes': this.fb.array([
        this.implementingAgencyArray(),
      ]),
    });

  }

  activityFocusAreaArray() {
    return this.fb.group({
      activityLevel: ["", Validators.required]
    })
  }

  implementingAgencyArray() {
    return this.fb.group({
      implementingAgencyType: [null]
    })
  }


  ngOnInit() {


    let b = localStorage.getItem('pcUser')
    let c = JSON.parse(b);
    let d = c['date'];
    console.log(d)
    this.adForm.get('plannedStartDate').patchValue(d);
    // let date1= localStorage.getItem('currentDate');

    // this.adForm.get('plannedStartDate').patchValue(date1);

    this.aops.index({}).subscribe(
        res => {
            this.divisionAdpater = res;
        })

    this.lang = this.translate.currentLang.substr(0,2);
    this.activityCategoryLevel[0] = [];
    this.implementingAgencyTypeName[0] = null;
    this.getAgencyType(0, 0);
    this.ams.index({}).subscribe(
      result => {
        this.activityTypeAdapter = result;
      },
      error => {
        console.log(error);
      }
    );

    this.afams.show(0).subscribe(
      result => {
        this.activityFocusAreaAdapter = result;
      },
      error => {
        console.log(error);
      }
    );

    this.igs.index({}).subscribe(
      result => {
        this.activityForAdapter = result;
      },
      error => {
        console.log(error);
      }
    )

    this.ps.getPrograms({
      planYear:'',
      focusAreaLevel1:'',
      implementingAgencyType1:''
    }).subscribe(
      res => {
        this.dropDownSource = res;
        console.log(this.dropDownSource)
      }
    )
  }

  get1($event){
      console.log($event)
  }

  activityFocusAreaChange(value) {
    this.setActivityFocusAreaAdapter(value, 0);
  }

  activityCategoryChange(value, i) {
  //   let nextIndex = i + 1;
  //   this.setActivityFocusAreaAdapter(value, nextIndex);
  //   this.jqxLoader.open();
    this.getProgram();
  }
  getProgram() {
    let focusAreaId = this.adForm.get('focusArea').value;
    let subFocusAreaId = this.adForm.get('activityLevels')['controls'][0]['value']['activityLevel']
    let implementingAgencyTypes = this.adForm.get('implementingAgencyTypes')['controls'].length;
    console.log(implementingAgencyTypes);
    let post = [];
    post['planYear'] = '';
    post['focusAreaLevel1'] = focusAreaId;
    post['focusAreaLevel2'] = subFocusAreaId;
    this.activitySubCategoryData = post;
    for (let i = 0; i < implementingAgencyTypes; i++) {
      let index = i+1;
      post['implementingAgencyType' + index] = this.adForm.get('implementingAgencyTypes')['controls'][i]['value']['implementingAgencyType'] || '';
      console.log(this.adForm.get('implementingAgencyTypes')['controls'][i]['value']['implementingAgencyType']);
    }
    this.ps.getPrograms(post).subscribe(
      result => {
        this.dropDownSource = this.getPositiveOnly(result);
       
        this.jqxLoader.close();
      }, error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  getPositiveOnly(result: any[]): any {
    return result.filter(data => {
      if('remainingAmount' in data){
        return data['remainingAmount'] > 0
      }
      return true;
    });
  }

  setActivityFocusAreaAdapter(focusCode, index = 0) {
    this.activityCategoryLevel[index] = [];
    // remove previously added form control if the parent select is changed.
    let formArrayLength = this.adForm.get('activityLevels')['controls'].length;
    if (index > 0 && formArrayLength > index) {
      for (let i = index; i <= formArrayLength; i++) {
        console.log(i);
        const control = <FormArray>this.adForm.controls['activityLevels'];
        control.removeAt(i);
      }
    }
    this.afams.show(focusCode).subscribe(
      result => {
        if (result['length'] > 0) {
          this.activityCategoryLevel[index] = result;
          if (index > 0) {
            const control = <FormArray>this.adForm.controls['activityLevels'];
            control.push(this.activityFocusAreaArray());
          }
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  implementingAgencyChange(selectedValue, selectedEvent, i,) {
    console.log(this.implementingAgencyType[1]);
      console.log(selectedValue);
      let selectedData =  this.implementingAgencyType[i].filter((x)=>x.code ==selectedValue);
      console.log(selectedData)
// if((this.adForm.get('implementingAgencyTypes')['controls'][i]['value']['implementingAgencyType'])==1){
      this.adForm.get('address').patchValue(selectedData && selectedData[0].nameNepali);

      this.ats.show(selectedValue).subscribe(res=>{
          console.log(res);
      })
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.implementingAgencyTypeName[i + 1] = displayText;
      let agencyCode = selectedEvent.target.value;
      let nextIndex = i + 1;
      this.getAgencyType(agencyCode, nextIndex, 'implementing');
      this.getProgram();
    }
  }
  Change(selectedEvent) {
    let displayText = selectedEvent.args.item.label;
    console.log(displayText)
    this.adForm.get('activityName').patchValue(displayText)
    let id = this.list.val();
    this.ps.getBudget(id).subscribe(
      result => {
          console.log(result)
        if (result) {
          this.totalRs = Number(result);
          console.log(this.totalRs);
          this.adForm.get('totalEstimatedCost').patchValue(this.totalRs)

        } else {
          this.totalRs = '';
        }
      }, error => {
        this.totalRs = '';
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  getAgencyType(agencyCode = 0, index = 0, type = null) {

    if (type == 'implementing') {
      this.implementingAgencyType[index] = [];
      let formArrayLength = this.adForm.get('implementingAgencyTypes')['controls'].length;
      if (index > 0 && formArrayLength > index) {
        for (let i = index; i <= formArrayLength; i++) {
          const control = <FormArray>this.adForm.controls['implementingAgencyTypes'];
          control.removeAt(i);
        }
      }
    }

    this.ats.show(agencyCode).subscribe(
      result => {
          console.log(result)
        if (result['length'] > 0) {
          if (type == 'implementing') {
            this.implementingAgencyType[index] = result;
            const control = <FormArray>this.adForm.controls['implementingAgencyTypes'];
            control.push(this.implementingAgencyArray());
          }
          if (index == 0) {
            this.implementingAgencyType[index] = result;
          }
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  AbledPerson: string = '';
  AbledPersonChange(event, selected) {
    this.AbledPerson = selected;
  }

  LivelihoodOpp: string = '';
  LivelihoodOppChange(event, selected) {
    this.LivelihoodOpp = selected;
  }

  CostlessActivity: string = '';
  CostlessActivityChange(event, selected) {
    this.CostlessActivity = selected;
  }

  disastarMgmt: string = '';
  disastarMgmtChange(event, selected) {
    this.disastarMgmt = selected;
    console.log(this.disastarMgmt);
  }

  activityTypeDetail: string = '';
  ActivityTypeChange(event, selected) {
    this.activityTypeDetail = selected;
  }


  /**
   * Function triggered when save button is clicked
   * @param formData
   */
  save(formData) {

    let activityFocusLevels = formData['activityLevels'];
    // let executingAgencyTypes = formData['executingAgencyTypes'];
    let implementingAgencyTypes = formData['implementingAgencyTypes'];

    formData['focusAreaLevel1'] = formData['focusArea'];
    for (let i = 0; i < activityFocusLevels.length; i++) {
      let index = i + 2;
      formData['focusAreaLevel' + index] = activityFocusLevels[i].activityLevel;
    }

    for (let i = 0; i < 1; i++) {
      let index = i + 1;
      formData['executingAgencyTypeLevel' + index] = "1";
    }
    for (let i = 0; i < implementingAgencyTypes.length; i++) {
      let index = i + 1;
      formData['implementingAgencyTypeLevel' + index] = implementingAgencyTypes[i].implementingAgencyType;
    }

    // formData['plannedStartMonth'] = this.startMonthCombo.val() || '';
    // formData['plannedStartYear'] = this.startYearCombo.val() || '';

    formData['activityForWomen'] = (this.womenCheck.val()) ? 1 : 0 || '';
    formData['activityForChildren'] = (this.childrenCheck.val()) ? 1 : 0 || '';
    formData['activityForAbled'] = this.AbledPerson;
    formData['activityOpportunities'] = this.LivelihoodOpp;
    formData['disasterManagementPlan'] = this.disastarMgmt;
    formData['costlessActivity'] = this.CostlessActivity;
    formData['activityTypeDetails'] = this.activityTypeDetail;
    formData['programId'] = this.list.val();
    if (formData['activityName'] && formData['activityFor']) {
      this.jqxLoader.open();
      this.ads.store(formData).subscribe(
        result => {
          if (result['message']) {
            this.resetForm();
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            this.router.navigate(['/activity/activity-output']);
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Select Group Name';
      this.errNotification.open();
    }
  }

  closeForm() {
    this.location.back();
  }

  resetForm() {
    this.adForm.reset();
    // this.myComboBox.clearSelection();
  }



}
