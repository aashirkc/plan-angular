import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ActivityDetailsComponent } from './activity-details.component';
import { ActivityDetailsRoutingModule } from './activity-details-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ActivityDetailsRoutingModule
  ],
  declarations: [
    ActivityDetailsComponent
  ],schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ActivityDetailsModule { }
