import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageActivityComponent } from './manage-activity.component';

const routes: Routes = [
  {
    path: '',
    component: ManageActivityComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageActivityRoutingModule { }
