import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DateConverterService, ActivityOutputService, ActivityFocusAreaMasterService, PlanningYearService, InclusionGroupService, CurrentUserService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';


@Component({
  selector: 'app-manage-activity',
  templateUrl: './manage-activity.component.html',
  styleUrls: ['./manage-activity.component.scss']
})
export class ManageActivityComponent implements OnInit {

  activityForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  transData: any;
  itemFocus: any = [];
  itemAdapter: any = [];

  planYearAdapter: Array<any> = [];
  activityAdapter: Array<any> = [];
  activityFocusAreaAdapter: any = [];
  activityCategoryAdapter: any = [];
  userData: any = {};
  SearchDatas: Array<any> = [];
  API_URL_DOC: any;
  API_URL: any;
  lang: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('myViewWindow') myViewWindow: jqxWindowComponent;
  @ViewChild('myEditWindow') myEditWindow: jqxWindowComponent;
  @ViewChild('myReportWindow') myReportWindow: jqxWindowComponent;
  @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;
  @ViewChild("item") itemOption: ElementRef;

  constructor(
    private fb: FormBuilder,
    private cus: CurrentUserService,
    private dfs: DateConverterService,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    @Inject('API_URL') API_URL: string,
    private cdr: ChangeDetectorRef,
    private pys: PlanningYearService,
    private igs: InclusionGroupService,
    private afams: ActivityFocusAreaMasterService,
    private activityOutputService: ActivityOutputService,
    private translate: TranslateService,
    private router: Router
  ) {
    this.createForm();
    this.itemFocus[0] = false;
    this.getTranslation();
    // this.userData = this.cus.getTokenData();
    let userDetalils = JSON.parse(localStorage.getItem('pcUser'));
    this.userData = userDetalils['userType'];
    this.API_URL_DOC = API_URL_DOC;
    this.API_URL = API_URL;
  }

  ngOnInit() {
    this.lang = this.translate.currentLang.substr(0,2);
    this.pys.index({}).subscribe((res) => {
      this.planYearAdapter = res;
      if (this.planYearAdapter.length > 0) {
        let planCode = this.planYearAdapter[0].yearCode || '';
        this.activityForm.get('planYear').patchValue(planCode);
        this.SearchData(this.activityForm.value)
      }

    }, (error) => {
      console.info(error);
    });
    this.igs.index({}).subscribe((res) => {
      this.activityAdapter = res;
    }, (error) => {
      console.info(error);
    });
    this.afams.show(0).subscribe((res) => {
      this.activityFocusAreaAdapter = res;
    }, (error) => {
      console.info(error);
    });
  }
  getTranslation() {
    this.translate.get(['SN', 'regNo', 'DETAIL_VIEW', 'universityRoll', 'ADD', 'EDIT', 'NAME', 'nepali', 'english', 'mobile', 'VIEW', 'DETAIL', "EDIT", "ACTION", "UPDATE", "DELETESELECTED", "RELOAD",
      "ACTIVITY_NAME", "ACTIVITY_TYPE", "ACTIVITY_FOCUS_AREA", "ACTIVITY_FOR"]).subscribe((translation: [string]) => {
        this.transData = translation;
        this.loadGrid();
      });
  }

  loadGridData() {
    let post = {};
    post = this.activityForm.value;
    if (post['status']) {
      this.SearchData(post);
    }
  }

  createForm() {
    this.activityForm = this.fb.group({
      'planYear': ['', Validators.required],
      'activityFor': [''],
      'activityFocusArea': ['', Validators.required],
      'activityCategory': ['', Validators.required],
      'activityName': [''],
      'workServiceType':['']
    });
  }

  ngAfterViewInit() {
    this.loadGridData();
    let formData = JSON.parse(localStorage.getItem('ManageActivity'));
    if (formData) {
      if (formData['activityFocusArea']) {
        this.LoadActivityCategory(formData);
      } else {
        this.activityForm.setValue(formData);
        this.SearchData(formData);
        setTimeout(() => {
          localStorage.removeItem('ManageActivity');
        }, 100);
      }
    }
    this.cdr.detectChanges();
  }

  LoadActivityCategory(post?: any) {
    if (post) {
      let data = post;
      if (data['activityFocusArea']) {
        this.jqxLoader.open();
        this.afams.show(data['activityFocusArea']).subscribe((res) => {
          this.activityCategoryAdapter = res;
          // Setting Data from local Storage
          let formData = JSON.parse(localStorage.getItem('ManageActivity'));
          this.activityForm.setValue(formData);
          this.SearchData(formData);
          setTimeout(() => {
            localStorage.removeItem('ManageActivity');
          }, 100);
          this.jqxLoader.close();
        }, (error) => {
          console.info(error);
          this.jqxLoader.close();
        });
      }
    } else {
      let data = this.activityForm.value;
      if (data['activityFocusArea']) {
        this.jqxLoader.open();
        this.afams.show(data['activityFocusArea']).subscribe((res) => {
          this.activityCategoryAdapter = res;
          this.jqxLoader.close();
        }, (error) => {
          console.info(error);
          this.jqxLoader.close();
        });
      }
    }
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'planYear', type: 'string' },
          { name: 'activityForName', type: 'string' },
          { name: 'activityFor', type: 'string' },
          { name: 'focusAreaLevel1', type: 'string' },
          { name: 'focusAreaLevel1Name', type: 'string' },
          { name: 'activityName', type: 'string' },
          { name: 'activityCategory', type: 'string' },
          { name: 'activityTypeName', type: 'string' },
          { name: 'a_ApprovedBy', type: 'string' },
          { name: 't_ApprovedBy', type: 'string' },

        ],
        id: 'id',
        localdata: [],
        pagesize: 100
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['ACTIVITY_NAME'],  datafield: 'activityName', columntype: 'textbox', editable: false, },
      { text: this.transData['ACTIVITY_TYPE'], datafield: 'activityTypeName', columntype: 'textbox',width:150, editable: false },
      { text: this.transData['ACTIVITY_FOCUS_AREA'],  datafield: 'focusAreaLevel1', displayfield: "focusAreaLevel1Name",width:150, columntype: 'textbox', editable: false, },
      { text: this.transData['ACTIVITY_FOR'], datafield: 'activityFor', displayfield: "activityForName", columntype: 'textbox', width:150 , editable: false },
      {
        text: this.transData['VIEW'], datafield: 'View', sortable: false, filterable: false, width: 100, columntype: 'button', columngroup: 'Actions',
        cellsrenderer: (): string => {
          return this.transData['DETAIL_VIEW'];
        },
        buttonclick: (row: number): void => {
          localStorage.setItem('ManageActivity', JSON.stringify(this.activityForm.value));
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          if (dataRecord['id']) {
            this.router.navigate(['/activity/view-activity', { id: dataRecord['id'] }]);
          }
        }
      },
      {
        text: this.transData['EDIT'], datafield: 'Edit', sortable: false, filterable: false, width: 100, columntype: 'button', columngroup: 'Actions',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {
          localStorage.setItem('ManageActivity', JSON.stringify(this.activityForm.value));
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          if(this.userData.toLowerCase() == 'admin' || dataRecord['t_ApprovedBy']<=0  || dataRecord['t_ApprovedBy']== null){
          if (dataRecord['id']) {
            this.jqxLoader.open();
            console.log(dataRecord["id"]);
            this.router.navigate(['/activity/activity-details/edit/', dataRecord['id']]);
          }
        } else {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = 'स्वीकृत भइसकेकाे छ !!';
          this.errNotification.open();
        }
        }
      },
    ];
  }

  columngroups: any[] =
    [
      { text: 'कार्य', align: 'center', name: 'Actions' }
    ];


    planYearSelected($event){
        let post = {}
        post = this.activityForm.value;
        this.SearchData(post)
    }



    itemFilter(searchPr, index){

        let keycode = searchPr['keyCode'];
        if ((keycode == 40)) {
        //   document.getElementById('itemCode').focus();
          document.getElementById('itemCode').focus();



        }
        let searchString = searchPr['target'].value;
        let len = searchString.length;
        let dataString = searchString.substr(len - 1, len);
        let temp = searchString.replace(/ /g, '');
        if (dataString == ' ' && searchString.length > 2) {
          if(searchString){
            this.itemFocus[index] = true;
            let post={'name':temp};
            let post1={
                'planYear': this.activityForm.get('planYear').value
            }
            this.activityOutputService.show(searchString,post1).subscribe(
              response => {
                this.itemAdapter[index] = [];
                this.itemAdapter[index] = response;
                console.log(response)
              },
              error => {
                console.log(error);
              }
            );
          }else{
            this.itemFocus[index] = false;
          }
        }
      }


      moveFocus(eventRef,i){
        //console.log(eventRef)
        var charCode = (window.event) ? eventRef.keyCode : eventRef.which;
        if ((charCode === 40)) {
        // //console.log(this.itemOption.nativeElement)

            this.itemOption.nativeElement.focus();
            this.itemOption.nativeElement.selectedIndex=0;

            this.activityForm.get('activityName').patchValue(this.itemAdapter[i][0].activityName);




        }
    }


    itemListSelected(selectedEvent, index) {
        //console.log(selectedEvent)
      if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
          //console.log(selectedEvent.target.value)
        let displayText = selectedEvent.target.selectedOptions[0].text;
        console.log('hey')
      //   //console.log(displayText)
        this.activityForm.get('activityName').patchValue(selectedEvent.target.value);
        this.activityForm.get('activityName').patchValue(displayText);
        console.log(selectedEvent.target.value, displayText)
      //   this.itemFocus[index] = false;
        let item;
    // //console.log('he');
    // //console.log(selectedEvent.target.value);
      //   //console.log(this.itemAdapter[index])
        if(this.itemAdapter[index]){
          item =  this.itemAdapter[index].filter(x => {
              // //console.log(x)
            return x.ActivityName == selectedEvent.target.value;
          });
          item = item && item[0];
          // //console.log(item)

        }

      }
    }


    itemFilterShow(selectedEvent, index){
        if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
          this.itemFocus[index] = true;
        }
      }




  SearchData(post) {
    this.jqxLoader.open();
    this.activityOutputService.index(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
        this.SearchDatas = [];
      } else {
        this.source.localdata = res;
        this.SearchDatas = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
}
