import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageActivityComponent } from './manage-activity.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ManageActivityRoutingModule } from './manage-activity-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ManageActivityRoutingModule
  ],
  declarations: [ManageActivityComponent]
})
export class ManageActivityModule { }
