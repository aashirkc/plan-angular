import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityOutputComponent } from './activity-output.component';

const routes: Routes = [
  {
    path: '',
    component: ActivityOutputComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityOutputRoutingModule { }
