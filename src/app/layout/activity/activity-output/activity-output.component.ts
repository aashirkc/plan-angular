import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DateConverterService, ToggleSidebarService, ActivityOutputService, ActivityFocusAreaMasterService, PlanningYearService, InclusionGroupService, CurrentUserService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';

@Component({
  selector: 'app-activity-output',
  templateUrl: './activity-output.component.html',
  styleUrls: ['./activity-output.component.scss']
})
export class ActivityOutputComponent implements OnInit {

  activityOutputForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  transData: any;
  planYearAdapter: Array<any> = [];
  activityAdapter: Array<any> = [];
  activityFocusAreaAdapter: any = [];
  activityCategoryAdapter: any = [];
  userData: any = {};
  SearchDatas: Array<any> = [];
  API_URL_DOC: any;
  API_URL: any;
  columngroups: any[];
  lang: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('myViewWindow') myViewWindow: jqxWindowComponent;
  @ViewChild('myEditWindow') myEditWindow: jqxWindowComponent;
  @ViewChild('myReportWindow') myReportWindow: jqxWindowComponent;
  @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;

  constructor(
    private fb: FormBuilder,
    private cus: CurrentUserService,
    private dfs: DateConverterService,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    @Inject('API_URL') API_URL: string,
    private cdr: ChangeDetectorRef,
    private pys: PlanningYearService,
    private igs: InclusionGroupService,
    private router: Router,
    private afams: ActivityFocusAreaMasterService,
    private activityOutputService: ActivityOutputService,
    private translate: TranslateService,
    private ts: ToggleSidebarService,
  ) {
    this.createForm();
    this.getTranslation();
    this.userData = this.cus.getTokenData();
    this.API_URL_DOC = API_URL_DOC;
    this.API_URL = API_URL;
  }


  ngOnInit() {
    this.lang = this.translate.currentLang.substr(0, 2);
    this.pys.index({}).subscribe((res) => {
      this.planYearAdapter = res;
      if (this.planYearAdapter.length > 0) {
        let planCode = this.planYearAdapter[0].yearCode || '';
        this.activityOutputForm.get('planYear').patchValue(planCode);
        this.SearchData(this.activityOutputForm.value);
      }
    }, (error) => {
      console.info(error);
    });
    this.igs.index({}).subscribe((res) => {
      this.activityAdapter = res;
    }, (error) => {
      console.info(error);
    });
    this.afams.show(0).subscribe((res) => {
      this.activityFocusAreaAdapter = res;
    }, (error) => {
      console.info(error);
    });
  }
  getTranslation() {
    this.translate.get(['SN', 'ACTIVITY_NAME', 'ACTIVITY_FOCUS_AREA', 'TYPE',
      'ACTIVITY_CATEGORY', 'VIEW', 'ACTIVITY_FOR', 'ACTION', 'EDIT', 'DEFINE_ACTIVITY',
      'DEFINED_ACTIVITY', 'regNo', 'universityRoll', 'ADD', 'EDIT', 'NAME', 'nepali',
      'english', 'mobile', 'VIEW', 'DETAIL', 'ACTIONS', 'UPDATE', 'DELETE',
      'RELOAD']).subscribe((translation: [string]) => {
        this.transData = translation;
        this.loadGrid();
      });
  }

  loadGridData() {
    let post = {};
    post = this.activityOutputForm.value;
    if (post['status']) {
      this.SearchData(post);
    }
  }

  createForm() {
    this.activityOutputForm = this.fb.group({
      'planYear': ['', Validators.required],
      'activityFor': [''],
      'activityFocusArea': ['', Validators.required],
      'activityCategory': ['', Validators.required],
      'activityName': [''],
      'workServiceType': ['']
    });
  }

  ngAfterViewInit() {
    this.loadGridData();
    let formData = JSON.parse(localStorage.getItem('ActivityOutput'));
    if (formData) {
      if (formData['activityFocusArea']) {
        this.LoadActivityCategory(formData);
      } else {
        this.activityOutputForm.patchValue(formData);
        this.SearchData(formData);
        setTimeout(() => {
          // localStorage.removeItem('ActivityOutput');
        }, 100);
      }
    }
    this.cdr.detectChanges();
  }

  LoadActivityCategory(post?: any) {
    if (post) {
      let data = post;
      if (data['activityFocusArea']) {
        this.jqxLoader.open();
        this.afams.show(data['activityFocusArea']).subscribe((res) => {
          this.activityCategoryAdapter = res;
          // Setting Data from local Storage
          let formData = JSON.parse(localStorage.getItem('ActivityOutput'));
          this.activityOutputForm.setValue(formData);
          this.SearchData(formData);
          setTimeout(() => {
            localStorage.removeItem('ActivityOutput');
          }, 100);
          this.jqxLoader.close();
        }, (error) => {
          this.jqxLoader.close();
        });
      }
    } else {
      let data = this.activityOutputForm.value;
      if (data['activityFocusArea']) {
        this.jqxLoader.open();
        this.afams.show(data['activityFocusArea']).subscribe((res) => {
          this.activityCategoryAdapter = res;
          this.jqxLoader.close();
        }, (error) => {
          this.jqxLoader.close();
        });
      }
    }

  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'planYear', type: 'string' },
          { name: 'activityFor', type: 'string' },
          { name: 'activityFocusArea', type: 'string' },
          { name: 'activityName', type: 'string' },
          { name: 'activityCategory', type: 'string' },
          { name: 'focusAreaLevel1', type: 'string' },
          { name: 'activityForName', type: 'string' },
          { name: 'focusAreaLevel1Name', type: 'string' },
          { name: 'focusAreaLevel2', type: 'string' },
          { name: 'focusAreaLevel2Name', type: 'string' },
          { name: 'outputType', type: 'string' },
          { name: 'taStatus', type: 'string' }
        ],
        id: 'id',
        localdata: [],
        pagesize: 100
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['ACTIVITY_NAME'], datafield: 'activityName', columntype: 'textbox', editable: false, },
      {
        text: this.transData['ACTIVITY_FOCUS_AREA'], datafield: 'focusAreaLevel1',
        displayfield: "focusAreaLevel1Name", width: 150, columntype: 'textbox', editable: false,
      },
      {
        text: this.transData['ACTIVITY_CATEGORY'], datafield: 'focusAreaLevel2',
        displayfield: "focusAreaLevel2Name", width: 150, columntype: 'textbox', editable: false,
      },
      {
        text: this.transData['ACTIVITY_FOR'], datafield: 'activityForName', width: 150,
        columntype: 'textbox', editable: false,
      },

      {
        text: this.transData['TYPE'], datafield: 'outputType', width: 100, columntype: 'textbox', editable: false,
        cellsrenderer: (row): string => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          if (dataRecord['outputType'] == 'SD') {
            return "<div style='margin:4px;'>सेवा</div>";
          } else if (dataRecord['outputType'] == 'CB') {
            return "<div style='margin:4px;'>क्षमता अभिबृदी</div>";
          } else if (dataRecord['outputType'] == 'WOA') {
            return "<div style='margin:4px;'>सम्पति</div>";
          } else {
            return "<div style='margin:4px;'>&nbsp;</div>";
          }
        }
      },
      {
        text: this.transData['ACTION'], datafield: 'type', columngroup: 'action',
        sortable: false, filterable: false, width: 160, columntype: 'button',
        cellsrenderer: (row): string => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          if (dataRecord['outputType'] == 'N' || dataRecord['outputType'] == null) {
            return this.transData['DEFINE_ACTIVITY'];
          } else {
            return this.transData['DEFINED_ACTIVITY'];
          }
        },
        buttonclick: (row: number): void => {
          localStorage.setItem('ActivityOutput', JSON.stringify(this.activityOutputForm.value));
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          if (dataRecord['outputType'] == 'N' || dataRecord['outputType'] == null) {
            this.router.navigate(['/activity/work-output-asset', { id: dataRecord['id'] }]);
          } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'गतिविधि पहिल्यै परिभाषित गरिएको छ!!';
            this.errNotification.open();
          }
        }
      },
      {
        text: 'सच्याउनुहोस्', dataField: 'Edit', columngroup: 'action',
        sortable: false, filterable: false, width: 100, columntype: 'button',
        cellsrenderer: (): string => 'सच्याउनुहोस्',
        buttonclick: (row: number): void => {
          // navigate to edit page 
          let dataRecord = this.myGrid.getrowdata(row);
          // if admin then only editable
          try {
            const userData = JSON.parse(localStorage.getItem('pcUser'));
            if (dataRecord.outputType === 'N') {
              this.showMessage('पहिला गतिबिधि परिभाषित गर्नुहोस', 'error');
            } else {
              if (userData['userType'].toLowerCase() === 'admin' || dataRecord['taStatus'] !== 'Y') {
                this.router.navigate([`/activity/activity-output-edit`, { id: dataRecord['id'] }])
              } else {
                this.showMessage('तपाइलाइ यो कार्य गर्न अनुमति दिइएको छैन', 'error');
              }
            }
          } catch (e) {
            console.warn(e);
          }
        }
      },
      {
        text: this.transData['VIEW'], dataField: 'EditParinam', columngroup: 'action',
        sortable: false, filterable: false, width: 100, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['VIEW'];
        },
        buttonclick: (row: number): void => {
          localStorage.setItem('ActivityOutput', JSON.stringify(this.activityOutputForm.value));
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);


          this.ts.changeMessage("on");
          this.router.navigate(['/activity/view-activity', { id: dataRecord['id'] }]);

        }
      },
    ];
    this.columngroups =
      [
        { text: this.transData['ACTION'], align: 'center', name: 'action' },
      ];
  }


  showMessage = (message: string, type = 'message'): void => {
    if (type === 'error') {
      const messageDiv: any = document.getElementById('error');
      messageDiv.innerText = message;
      this.errNotification.open();
    } else {
      // let messageDiv: any = document.getElementById('')
      console.log('Message showing not implemened.');
    }
  }

  planYearSelected($event) {
    let post = {}
    post = this.activityOutputForm.value;
    this.SearchData(post)
    console.log("sucess")
  }

  SearchData(post) {
    this.jqxLoader.open();
    this.activityOutputService.index(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
        this.SearchDatas = [];
      } else {
        this.source.localdata = res;
        this.SearchDatas = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
}
