import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ActivityOutputComponent } from './activity-output.component';
import { ActivityOutputRoutingModule } from './activity-output-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ActivityOutputRoutingModule
  ],
  declarations: [
    ActivityOutputComponent,
  ]
})
export class ActivityOutputModule { }
