import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgramTransferComponent } from './program-transfer.component';

const routes: Routes = [
  {
    path: '',
    component: ProgramTransferComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramTransferRoutingModule { }
