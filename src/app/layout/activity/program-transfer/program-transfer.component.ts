import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit, ElementRef } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { CurrentUserService, ProgramTransferService, UnicodeTranslateService, NepaliTextareaComponent, PlanningYearService } from '../../../shared';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

function amountCheck(c: AbstractControl): any {
  console.log(c);
  if (!c.parent || !c || c.value == "") return;
  const amnt = c.parent.get('amountOld')
  const amntNew = c.parent.get('amount')
  if (Number(amntNew.value) <= Number(amnt.value)) return;
  if (Number(amntNew.value) > Number(amnt.value)) {
    console.log("invalid");
    return { invalid: true };

  }
}

@Component({
  selector: 'app-program-transfer',
  templateUrl: './program-transfer.component.html',
  styleUrls: ['./program-transfer.component.scss']
})
export class ProgramTransferComponent implements OnInit {

  programTransferForm: FormGroup;
  update: boolean = false;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  transData: any;
  pAdapter: Array<any> = [];
  userData: any = {};
  columngroups: any[];
  lang: any;
  API_URL_DOC: any;
  planYearAdapter: any;
  PlanYear: any;

  get amntNew() {
    return this.programTransferForm.get('amount');
  }
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('list') list: jqxComboBoxComponent;
  @ViewChild('list2') list2: jqxComboBoxComponent;
  @ViewChild('txt') txt: NepaliTextareaComponent;
  @ViewChild('documentInput') documentInput: ElementRef;

  constructor(
    @Inject('API_URL_DOC') API_URL_DOC: string,
    private fb: FormBuilder,
    private cus: CurrentUserService,
    private pts: ProgramTransferService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private pys: PlanningYearService,
    private unicode: UnicodeTranslateService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
    this.userData = this.cus.getTokenData();
    this.API_URL_DOC = API_URL_DOC;
  }


  ngOnInit() {
    let planCode
    this.pys.index({}).subscribe((res) => {
      this.planYearAdapter = res;
      if (this.planYearAdapter.length > 0) {
        planCode = this.planYearAdapter[0].yearCode || '';
        this.programTransferForm.get('planYear').patchValue(planCode);
        this.getYearly(planCode)


      }
    }, (error) => {
      console.info(error);
    });


    //   console.log(planCode)
    // this.pts.getProgram({}).subscribe((res) => {
    //   this.pAdapter = res;
    // }, (error) => {
    //   console.info(error);
    // });
  }
  getYearly(x) {
    let post = {
      'planYear': x
    }
    this.pts.getProgram(post).subscribe((res) => {
      this.pAdapter = res['data'];
    }, (error) => {
      console.info(error);
    });
  }


  getTranslation() {
    this.translate.get(['SN', 'ACTIVITY_NAME', 'FROM', 'TO', 'VIEW', 'PROGRAM', 'BUDGET_AMOUNT', 'ACTION', 'REMARKS', 'EDIT', 'DEFINE_ACTIVITY', 'DEFINED_ACTIVITY', 'regNo', 'universityRoll', 'ADD', 'EDIT', 'NAME', 'nepali', 'english', 'mobile', 'VIEW', 'DETAIL', "ACTIONS", "UPDATE", "DELETE", "RELOAD"]).subscribe((translation: [string]) => {
      this.transData = translation;
      this.loadGrid();
    });
  }

  loadGridData() {
    this.jqxLoader.open();
    this.pts.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  createForm() {
    this.programTransferForm = this.fb.group({
      'id': [''],
      'amount': ['', [Validators.required, amountCheck]],
      'amountOld': [''],
      'remarks': [''],
      'planYear': ['']
    });
  }

  ngAfterViewInit() {
    this.unicode.initUnicode();
    this.loadGridData();
    this.cdr.detectChanges();
  }
  programChange() {
    let id = this.list.val();
    this.pts.indexProgram(id).subscribe((res) => {
      if (res['error']) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res['error'].message;
        this.errNotification.open();
      } else {
        this.programTransferForm.controls['amountOld'].patchValue(res['totalAmount']);
        this.programTransferForm.controls['amount'].patchValue(res['totalAmount']);
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  // handle updload file input
  selectedFiles: any = [];
  handleFileInput = (event) => {
    this.selectedFiles = event.target.files;
    console.log(this.selectedFiles);
  }

  loadGrid() {
    this.source =
    {
      datatype: 'json',
      datafields: [
        { name: 'id', type: 'string' },
        { name: 'programFrom', type: 'string', map: 'programFrom>id' },
        { name: 'programTo', type: 'string', map: 'programTo>id' },
        { name: 'programFromName', type: 'string', map: 'programFrom>name' },
        { name: 'programToName', type: 'string', map: 'programTo>name' },
        { name: 'amount', type: 'number' },
        { name: 'remarks', type: 'string' },
      ],
      id: 'id',
      localdata: [],
      pagesize: 100
    }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['PROGRAM'] + '(' + this.transData['FROM'] + ')', datafield: 'programFrom', displayfield: "programFromName", columntype: 'textbox', editable: false, sortable: false },
      { text: this.transData['PROGRAM'] + '(' + this.transData['TO'] + ')', datafield: 'programTo', displayfield: "programToName", columntype: 'textbox', editable: false, sortable: false },
      { text: this.transData['BUDGET_AMOUNT'], datafield: 'amount', columntype: 'textbox', editable: false, cellsformat: "D2", align: "right", cellsalign: "right" },
      { text: this.transData['REMARKS'], datafield: 'remarks', columntype: 'textbox', editable: false, sortable: false },
      {
        text: 'फाइल', datafield: 'imageName', editable: false, cellsrenderer: (row, column, value) => {
          const image = this.source.localdata && this.source.localdata[row]['imageName'];
          return (image) ? `<a href="${this.API_URL_DOC}/${image}" target="_blank">हेर्नुहोस्</a>` : 'छैन्';
        }
      }
      // {
      //   text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 80, columntype: 'button',
      //   cellsrenderer: (): string => {
      //     return this.transData['EDIT'];
      //   },
      //   buttonclick: (row: number): void => {
      //     this.editrow = row;
      //     let dataRecord = this.myGrid.getrowdata(this.editrow);
      //     let dt = {};
      //     dt['transNo'] = dataRecord['transNo'] || '';
      //     dt['budgetFrom'] = dataRecord['budgetFrom'];
      //     dt['budgetTo'] = dataRecord['budgetTo'];
      //     dt['budgetAmt'] = dataRecord['amount'];
      //     dt['remarks'] = dataRecord['remarks'];
      //     this.programTransferForm.setValue(dt);
      //     this.update = true;
      //   }
      // }
    ];
    this.columngroups =
      [
        { text: this.transData['ACTION'], align: 'center', name: 'action' },
      ];
  }
  saveBtn(post) {
    if (this.list.val() && this.list2.val()) {
      let fd = new FormData();
      for (let keys in post) {
        fd.append(keys, post[keys]);
      }
      if (this.selectedFiles.length > 0) {
        for (let file of this.selectedFiles) {
          fd.append('document', file, file.name)
        }
      } else {
        this.showError('कृपया फाइल अपलोड गर्नु होला');
        return;
      }
      fd.append('programFrom', this.list.val());
      fd.append('programTo', this.list2.val());
      this.jqxLoader.open();
      // const _formData = new FormData();
      // for (let data in post) {
      //   _formData.append(data, post[data]);
      // }

      this.pts.store(fd).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            this.programTransferForm.reset();
            this.txt.clearInput();
            this.list.clearSelection();
            this.list2.clearSelection();
            this.loadGridData();
          }
          this.jqxLoader.close();
          if (result['error']) {
            this.showError(result['error']['message']);
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'कृपया कार्यक्रम बाट र लाई दुबै छनाेट गर्नुहाेस्';
      this.errNotification.open();
    }
  }

  showError = (msg: string) => {
    const messageDiv: any = document.getElementById('error');
    messageDiv.innerText = msg;
    this.errNotification.open();
  }

  updateBtn(post) {
    this.jqxLoader.open();
    const _formData = new FormData();
    for (let data in post) {
      _formData.append(data, post[data]);
    }
    this.pts.update(post['id'], _formData).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.update = false;
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.programTransferForm.reset();
          this.loadGridData();
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateBtn() {
    this.update = false;
    this.programTransferForm.reset();
    this.txt.clearInput();
    this.loadGridData();
  }

}
