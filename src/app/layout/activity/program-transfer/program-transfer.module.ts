import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgramTransferComponent } from './program-transfer.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ProgramTransferRoutingModule } from './program-transfer-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ProgramTransferRoutingModule
  ],
  declarations: [ProgramTransferComponent],
  schemas:[NO_ERRORS_SCHEMA]
})
export class ProgramTransferModule { }
