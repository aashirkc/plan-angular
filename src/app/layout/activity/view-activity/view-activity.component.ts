import {
    Component,
    ViewChild,
    ViewContainerRef,
    Inject,
    ChangeDetectorRef,
    OnInit
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import {
    CurrentUserService,
    ActivityOutputService,
    AssetsDetailsCategoryService,
    PlantUnitService,
    UnicodeTranslateService,
    WorkOutputAssetService,
    PrintingService
} from '../../../shared';
import { TranslateService } from '@ngx-translate/core';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

@Component({
    selector: 'app-view-activity',
    templateUrl: './view-activity.component.html',
    styleUrls: ['./view-activity.component.scss']
})
export class ViewActivityComponent implements OnInit {
    transData: any;
    userData: any = {};
    API_URL_DOC: any;
    API_URL: any;
    ViewData: any;
    lang: any;

    serviceDetail: any = [];
    addNewAssetsForm: FormGroup;
    assetTypeAdapter: any = [];
    assetCreatedAdapter: any = [];
    assetSubCategoryAdapter: any = [];
    planAdapter: any;
    assetsDetails: any;
    propertyDetails: Array<any> = [];

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('addNewDataWindow') addNewDataWindow: jqxWindowComponent;

    constructor(
        private cus: CurrentUserService,
        @Inject('API_URL_DOC') API_URL_DOC: string,
        @Inject('API_URL') API_URL: string,
        private cdr: ChangeDetectorRef,
        private activeRoute: ActivatedRoute,
        private avs: ActivityOutputService,
        private fb: FormBuilder,
        private pu: PlantUnitService,
        private adcs: AssetsDetailsCategoryService,
        private unicode: UnicodeTranslateService,
        private wos: WorkOutputAssetService,
        private router: Router,
        private printingService: PrintingService,
        private translate: TranslateService,
        private location: Location
    ) {
        this.getTranslation();
        this.userData = this.cus.getTokenData();
        this.API_URL_DOC = API_URL_DOC;
        this.API_URL = API_URL;
    }
    dataType: any;
    ngOnInit() {
        this.createFormData();

        this.avs.getAssetsType().subscribe(
            result => {
                this.assetTypeAdapter = result;
            },
            error => {
                // console.log(error);
            }
        );

        this.avs.getAssetsCategory().subscribe(
            result => {
                this.assetCreatedAdapter = result;
            },
            error => {
                // console.log(error);
            }
        );

        this.pu.index({}).subscribe(res => {
            // console.log(res)
            this.planAdapter = res;
        });

        this.lang = this.translate.currentLang.substr(0,2);
        let data = this.activeRoute.snapshot.params;
        this.avs.showActivity(data['id']).subscribe(
            res => {
                this.ViewData = res;
                this.dataType = this.ViewData.outputType;
                if (this.ViewData && this.ViewData.outputType == 'SD') {
                    this.loadServiceDetails('ServiceDetails', data['id']);
                } else if (this.ViewData && this.ViewData.outputType == 'WOA') {
                    this.loadServiceDetails('WorkOutputAsset', data['id']);
                } else if (this.ViewData && this.ViewData.outputType == 'CB') {
                    this.loadServiceDetails('CapacityBuilding', data['id']);
                }
                localStorage.setItem(
                    'ActivityDatas',
                    JSON.stringify(this.ViewData)
                );
            },
            error => {
                // console.info(error);
            }
        );
    }

    loadServiceDetails(type, id) {
        let post = {
            activity: id
        };
        this.avs.getServiceDetail(type, post).subscribe(
            result => {
                let data = {
                    type: type,
                    detail: result || null
                };
                this.serviceDetail = data;
                let assetData = data && data.detail && data.detail;
                // console.info(assetData);
                this.assetsDetails = assetData && assetData['outputAssets'];
                let assetType =
                    this.assetsDetails &&
                    this.assetsDetails[0] &&
                    this.assetsDetails[0].assetType &&
                    this.assetsDetails[0].assetType.id;
                let assetCategory =
                    this.assetsDetails &&
                    this.assetsDetails[0] &&
                    this.assetsDetails[0].assetCategory &&
                    this.assetsDetails[0].assetCategory.assetsCode;
                this.addNewAssetsForm.controls['assetType'].setValue(assetType);
                this.addNewAssetsForm.controls['assetCategory'].setValue(
                    assetCategory
                );
                let arrayData =
                    (result && result[0] && result[0].arrayData) || [];
                this.propertyDetails = arrayData;
                console.info(this.propertyDetails);
                this.assetCategory(assetCategory);
                // console.info(this.assetsDetails);
            },
            error => {
                console.log(error);
            }
        );
    }

    getTranslation() {
        this.translate.get(['SN']).subscribe((translation: [string]) => {
            this.transData = translation;
        });
    }

    ngAfterViewInit() {
        this.cdr.detectChanges();
        let routeParams = this.activeRoute.snapshot.params;
    }

    Close() {
        this.location.back();
    }

    printData(): void {
        const printContents = document.getElementById('printContent').innerHTML;
        this.printingService.printContents(printContents);
    }

    createFormData() {
        this.addNewAssetsForm = this.fb.group({
            assetType: [{ value: '', disabled: true }, Validators.required],
            assetCategory: [{ value: '', disabled: true }, Validators.required],
            assetName: ['', Validators.required],
            unitParameter: ['', Validators.required],
            totalNoAsset: ['', Validators.required],
            assetLocation: ['', Validators.required],
            id: [''],
            assetDetails: this.fb.array([this.initassetDetails()])
        });
    }

    assetCategory(value) {
        this.adcs.showChild(value).subscribe(
            result => {
                this.assetSubCategoryAdapter = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    initassetDetails() {
        return this.fb.group({
            assetSubCatagory: ['']
        });
    }

    Save(post) {
        let routeParams = this.activeRoute.snapshot.params;
        // WOA
        // For workoutput asset
        if (post['assetLocation'] == 1) {
            post['assetLocation'] = 'Office';
        }
        if (post['assetLocation'] == 2) {
            post['assetLocation'] = 'Area';
        }
        if (routeParams['id']) {
            this.jqxLoader.open();
            post['activity'] = routeParams['id'];
            post['outputType'] = 'WOA';
            this.wos.store(post).subscribe(
                result => {
                    if (result['message']) {
                        this.addNewAssetsForm.reset();
                        this.addNewDataWindow.close();
                        this.loadServiceDetails(
                            'WorkOutputAsset',
                            routeParams['id']
                        );
                        let messageDiv: any = document.getElementById(
                            'message'
                        );
                        messageDiv.innerText = result['message'];
                        this.msgNotification.open();
                    }
                    this.jqxLoader.close();
                    if (result['error']) {
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = result['error']['message'];
                        this.errNotification.open();
                    }
                },
                error => {
                    this.jqxLoader.close();
                }
            );
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'कृपया डाटा छान्नुहाेस्';
            this.errNotification.open();
        }
    }
}
