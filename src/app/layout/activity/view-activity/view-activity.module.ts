import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ViewActivityComponent } from './view-activity.component';
import { ViewActivityRoutingModule } from './view-activity-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ViewActivityRoutingModule
  ],
  declarations: [
    ViewActivityComponent,
  ]
})
export class ViewActivityModule { }
