import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ActivityOutputService } from "../../../shared";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

@Component({
    selector: "app-technical-approve",
    templateUrl: "./technical-approve.component.html",
    styleUrls: ["./technical-approve.component.scss"]
})
export class TechnicalApproveComponent implements OnInit {
    technicalApprovalForm: FormGroup;
    technicalApprovalLetter: any;
    technicalSpecificationDocument: any;
    name: string = "";

    constructor(
        private fb: FormBuilder,
        private activeRoute: ActivatedRoute,
        private aos: ActivityOutputService,
        private location: Location
    ) {}

    ngOnInit() {
        let data = this.activeRoute.snapshot.params;
        this.name = data["name"];

        this.technicalApprovalForm = this.fb.group({
            activity: [data["id"], Validators.required],
            techCost: [null, Validators.required],
            proposedCost: [null, Validators.required],
            orderIssuing: [null, Validators.required],
            techOrderNo: [null, Validators.required],
            techDate: [null, Validators.required],
            technicalApprovalLetter: this.fb.array([
                this.createTechnicalApprovalLetterForm()
            ]),
            technicalSpecificationDocument: this.fb.array([
                this.createTechnicalSpecificationDocumentForm()
            ])
        });
    }

    private createTechnicalApprovalLetterForm() {
        return this.fb.group({
            fileTitle: [null, Validators.required],
            file: [null, Validators.required]
        });
    }

    private createTechnicalSpecificationDocumentForm() {
        return this.fb.group({
            fileTitle: [null, Validators.required],
            file: [null, Validators.required]
        });
    }

    addMoreApprovalLetter() {
        this.technicalApprovalLetter = this.technicalApprovalForm.controls[
            "technicalApprovalLetter"
        ];
        this.technicalApprovalLetter.push(
            this.createTechnicalApprovalLetterForm()
        );
    }

    addMoreSpecificationDocument() {
        this.technicalSpecificationDocument = this.technicalApprovalForm.controls[
            "technicalSpecificationDocument"
        ];
        this.technicalSpecificationDocument.push(
            this.createTechnicalSpecificationDocumentForm()
        );
    }

    removeForm(type, i) {
        if (type == "0") {
            this.technicalApprovalLetter.removeAt(i);
        } else if (type == "1") {
            this.technicalSpecificationDocument.removeAt(i);
        }
    }

    save(value) {
        // delete value["technicalApprovalLetter"];
        // delete value["technicalSpecificationDocument"];

        const _formData = new FormData();
        // this.techicalFiles.forEach()
        for (var i = 0; i < this.techicalFiles.length; i++) {
          console.log(this.techicalFiles[i]);
          console.log(this.techicalFiles[i].fileName);
            _formData.append("talTitle", this.techicalFiles[i].fileName);
            _formData.append(
                "talFile",
                this.techicalFiles[i].file,
                this.techicalFiles[i].file.name
            );
        }

        for (var i = 0; i < this.specificationFlies.length; i++) {
            _formData.append("tsdTitle", this.specificationFlies[i].fileName);
            _formData.append(
                "tsdFile",
                this.specificationFlies[i].file,
                this.specificationFlies[i].file.name
            );
        }

        for (let data in value) {
            _formData.append(data, value[data]);
        }

        console.log(_formData);

        this.aos.addTechnicalApproval(_formData).subscribe(
            result => {
                if (result["message"]) {
                    let messageDiv: any = document.getElementById("message");
                    messageDiv.innerText = result["message"];
                    // this.msgNotification.open();

                    this.location.back();
                }
                // this.jqxLoader.close();
                if (result["error"]) {
                    let messageDiv: any = document.getElementById("error");
                    messageDiv.innerText = result["error"]["message"];
                    // this.errNotification.open();
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    clearForm() {
        this.technicalApprovalForm.reset();
    }

    techicalFiles: any[] = [];
    specificationFlies: any[] = [];

    onFileChange(event, type, i) {
        console.log(i);
        if (event.target.files && event.target.files[0]) {
            var formData: FormData = new FormData();
            // formData.append("file", file, file.name);
            if (type == "0") {
                let fileName = this.technicalApprovalForm
                    .get("technicalApprovalLetter")
                    ["controls"][i].get("fileTitle").value;
                    
                let file = event.target.files[0];
                let obj = {
                    fileName: fileName,
                    file: file
                };

                console.log(obj);
                this.techicalFiles.push(obj);
            } else if (type == "1") {
                let fileName = this.technicalApprovalForm
                    .get("technicalSpecificationDocument")
                    ["controls"][i].get("fileTitle").value;
                let file = event.target.files[0];
                let obj = {
                    fileName: fileName,
                    file: file
                };
                this.specificationFlies.push(obj);
            }
        }
    }
}

// if (event.target.files && event.target.files.length) {
//   const [file] = event.target.files;
//   reader.readAsDataURL(file);

//   reader.onload = () => {
//     if (type == "0") {
//       this.technicalApprovalForm.get('technicalApprovalLetter')['controls'][i].get('file').patchValue(reader.result);
//     } else if (type == "1") {
//       this.technicalApprovalForm.get('technicalSpecificationDocument')['controls'][i].get('file').patchValue(reader.result);
//     }
//   }
//   this.cd.markForCheck();
// }
