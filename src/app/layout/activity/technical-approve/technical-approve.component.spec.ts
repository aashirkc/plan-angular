import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicalApproveComponent } from './technical-approve.component';

describe('TechnicalApproveComponent', () => {
  let component: TechnicalApproveComponent;
  let fixture: ComponentFixture<TechnicalApproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicalApproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicalApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
