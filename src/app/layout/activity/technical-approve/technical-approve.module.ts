import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TechnicalApproveRoutingModule } from './technical-approve-routing.module';
import { TechnicalApproveComponent } from './technical-approve.component';
import { SharedModule } from '../../../shared/modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TechnicalApproveRoutingModule
  ],
  declarations: [TechnicalApproveComponent]
})
export class TechnicalApproveModule { }
