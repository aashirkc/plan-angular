import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ToggleSidebarService, CurrentUserService, ActivityOutputService, AssetsDetailsCategoryService, ServiceDetailsService, CapacityBuildingService, WorkOutputAssetService, UnicodeTranslateService, PlantUnitService } from '../../../shared';
import { TranslateService } from '@ngx-translate/core';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

@Component({
  selector: 'app-work-output-asset',
  templateUrl: './work-output-asset.component.html',
  styleUrls: ['./work-output-asset.component.scss']
})
export class WorkOutputAssetComponent implements OnInit {

  outputTypeForm: FormGroup;
  workOutputAssetForm: FormGroup;
  capacityBuildingForm: FormGroup;
  serviceDetailsForm: FormGroup;
  updated: boolean = false;
  transData: any;
  userData: any = {};
  API_URL_DOC: any;
  API_URL: any;
  ViewData: any;
  activityType: any;
  check: any = true;
  activityDatas: any = {};
  actId: any;
  serviceDataAdapter: any = [];
  organaizeByAdapter: any = [];
  trainingCategoryAdapter: any = [];
  assetTypeAdapter: any = [];
  planAdapter: any;
  assetCreatedAdapter: any = [];
  assetSubCategoryAdapter: any = [];
  lang: any;

  unitParameterAdapter: any = [
    {
      id: "1",
      name: 'Meter / Kilogram',
      nameNepali: 'मिटर र किलोग्राम'
    }
  ];
  assetUnitTypeAdapter: any = [
    {
      id: "1",
      name: 'General Type',
      nameNepali: 'सामान्य प्रकार'
    }
  ];

  outputTypeAdapter: any = [


    {
      nameNepali: 'सम्पति',
      name: 'Work Output Assets',
      value: 'WOA'
    },
    {
      nameNepali: 'सेवा विवरण',
      name: 'Service Details',
      value: 'SD'
    },
    {
      nameNepali: 'क्षमता अभिबृदी',
      name: 'Capacity Buildings',
      value: 'CB'
    },

  ];

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('planYear') planYear: jqxComboBoxComponent;
  @ViewChild('planYear1') planYear1: jqxComboBoxComponent;
  @ViewChild('planYear2') planYear2: jqxComboBoxComponent;
  constructor(
    private cus: CurrentUserService,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    @Inject('API_URL') API_URL: string,
    private cdr: ChangeDetectorRef,
    private activeRoute: ActivatedRoute,
    private avs: ActivityOutputService,
    private fb: FormBuilder,
    private router: Router,
    private cbs: CapacityBuildingService,
    private wos: WorkOutputAssetService,
    private serviceDetails: ServiceDetailsService,
    private adcs: AssetsDetailsCategoryService,
    private translate: TranslateService,
    private unicode: UnicodeTranslateService,
    private ts: ToggleSidebarService,
    private pu: PlantUnitService
  ) {
    this.getTranslation();
    this.createForm();
    this.userData = this.cus.getTokenData();
    this.API_URL_DOC = API_URL_DOC;
    this.API_URL = API_URL;
  }

  assetCategory(event, index) {
    //   console.log('hello')
    //   console.log(event.args.item.originalItem.assetsCode)
    //   let value1 = event.args.item.originalItem.assetsCode
    // console.log(event.target.value);

    this.adcs.showChild(event.target.value).subscribe(
      result => {
        this.assetSubCategoryAdapter[index] = result;

      },
      error => {
        console.log(error);
      }
    );

  }
  // subCat(event,i){
  //   console.log(i);
  //   this.adcs.showChild(event.target.value).subscribe(
  //     result => {
  //       this.assetSubCategoryAdapter[i+1] = result;
  //     },
  //     error => {
  //       console.log(error);
  //     }
  //   );
  // }

  assetChange() {

    const control1 = <FormArray>this.workOutputAssetForm.controls['assetDetails'];
    const assetDetails = control1['controls'][0] || null;
    let def_value = '';
    if (assetDetails) {
      def_value = assetDetails['controls']['assetType'].value;
    }

    let i = 0;
    for (let contr of control1.controls) {
      contr.patchValue({ 'assetType': def_value });
      (i != 0) && contr['controls']['assetType'].disable();
      i++;
    }
    control1.markAsDirty();
  }

  addItem() {
    const control1 = <FormArray>this.workOutputAssetForm.controls['assetDetails'];
    control1.push(this.initassetDetails());
    this.assetChange();
    // add unicode support for dynamically added input
    setTimeout(() => {
      this.unicode.initUnicode();
    }, 200);
  }
  removeItem(i: number) {
    const control1 = <FormArray>this.workOutputAssetForm.controls['assetDetails'];
    control1.removeAt(i);
    //this.itemAdapter.splice(i, 1);
  }
  ngOnInit() {
    // this.ts.currentMessage.subscribe((data) => {
    //   if(data=="on"){
    //     this.activityDatas=JSON.parse(localStorage.getItem('ActivityDatas'));
    //   this.check=false;
    //   }
    //   if(data=="off")
    //   {
    //   this.check=true;
    //   }

    // });
    this.pu.index({}).subscribe(res => {
      // console.log(res)
      this.planAdapter = res
    })
    this.lang = this.translate.currentLang.substr(0,2);
    let data = this.activeRoute.snapshot.params;
    this.actId = data['id'];
    this.avs.showActivity(data['id']).subscribe((res) => {
      this.ViewData = res;

      // total duration calculation
      const month = Number(res['totalDurationMonth']);
      const days = Number(res['totalDurationDays']);
      const duration = Number(30 * month + days);
      duration && this.serviceDetailsForm.get('durationOfService').patchValue(duration);
    }, (error) => {
      console.info(error);
    });

    this.avs.getOrganizedBy().subscribe(
      result => {
        this.organaizeByAdapter = result;
      },
      error => {
        console.log(error);
      }
    );

    this.avs.getServiceType().subscribe(
      result => {
        this.serviceDataAdapter = result;
      },
      error => {
        console.log(error);
      }
    );

    this.avs.getTrainingCategory().subscribe(
      result => {
        this.trainingCategoryAdapter = result;
      },
      error => {
        console.log(error);
      }
    );

    this.avs.getAssetsCategory().subscribe(
      result => {
        this.assetCreatedAdapter = result;
      },
      error => {
        console.log(error);
      }
    );

    // this.avs.getAssetsSubCategory().subscribe(
    //   result => {
    //     this.assetSubCategoryAdapter = result;
    //     console.log(result);
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // );

    this.avs.getAssetsType().subscribe(
      result => {
        this.assetTypeAdapter = result;
      },
      error => {
        console.log(error);
      }
    );
  }

  getTranslation() {
    this.translate.get(['SN']).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }
  value(event, value) {

  }

  initassetDetails() {
    return this.fb.group({
      assetType: ['', Validators.required],
      assetCategory: ['', Validators.required],
      assetSubCatagory: ['', Validators.required],
      assetName: ['', Validators.required],
      unitParameter: ['', Validators.required],
      totalNoAsset: ['', Validators.required]
    });
  }

  createForm() {
    this.outputTypeForm = this.fb.group({
      'outputType': ['', Validators.required],
    });

    this.workOutputAssetForm = this.fb.group({
      'assetLocation': ['', Validators.required],
      'id': [''],
      'assetDetails': this.fb.array([
        this.initassetDetails(),
      ]),
    });

    this.capacityBuildingForm = this.fb.group({
      'organizedBy': ['', Validators.required],
      'subjectOfTraining': ['', Validators.required],
      'totalDuration': ['', Validators.required],
      'totalTrainess': ['', Validators.required],
      'trainingCatagory': ['', Validators.required],
      'id': [''],
    });

    this.serviceDetailsForm = this.fb.group({
      'service': ['', Validators.required],
      'durationOfService': ['', Validators.required],
      'totalExpectedBeneficiaries': ['', Validators.required],
      'id': [''],
    });

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.unicode.initUnicode();
    }, 1000);

    this.cdr.detectChanges();
    // this.router.get('id');
  }
  updateOutputDetails(post) {
    let routeParams = this.activeRoute.snapshot.params;
    if (routeParams['id'] && post['outputType']) {
      this.jqxLoader.open();
      this.avs.updateActivity(routeParams['id'], post).subscribe(
        result => {
          if (result['message']) {
            this.updated = true;
            this.outputTypeForm.get('outputType').disable();
            this.activityType = result['data'];

            setTimeout(() => {
              this.unicode.initUnicode();
            }, 200);

            if (this.activityType == 'SD') {
              let dt = {};
              dt['service'] = result['detail'] && result['detail'].service;
              dt['durationOfService'] = result['detail'] && result['detail'].durationOfService;
              dt['totalExpectedBeneficiaries'] = result['detail'] && result['detail'].totalExpectedBeneficiaries;
              dt['id'] = result['detail'] && result['detail'].id;
              setTimeout(() => {
                if (result['detail']) {
                  this.serviceDetailsForm.setValue(dt);
                }
              }, 200);
              this.unicode.initUnicode();
            }
            if (this.activityType == 'CB') {
              let dt = {};
              dt['organizedBy'] = result['detail'] && result['detail'].organizedBy;
              dt['subjectOfTraining'] = result['detail'] && result['detail'].subjectOfTraining;
              dt['totalDuration'] = result['detail'] && result['detail'].totalDuration;
              dt['totalTrainess'] = result['detail'] && result['detail'].totalTrainess;
              dt['trainingCatagory'] = result['detail'] && result['detail'].trainingCatagory;
              dt['id'] = result['detail'] && result['detail'].id;
              setTimeout(() => {
                if (result['detail']) {
                  this.capacityBuildingForm.setValue(dt);
                }
              }, 200);
              this.unicode.initUnicode();
            }
            if (this.activityType == 'WOA') {
              let dt = {};
              dt['assetType'] = result['detail'] && result['detail'].assetType;
              // dt['assetCreate'] = result['detail'] && result['detail'].assetCreate;
              dt['assetCategory'] = result['detail'] && result['detail'].assetCategory;
              dt['assetSubCatagory'] = result['detail'] && result['detail'].assetSubCatagory;
              dt['assetName'] = result['detail'] && result['detail'].assetName;
              dt['unitParameter'] = result['detail'] && result['detail'].unitParameter;
              //dt['assetUnit'] = result['detail'] && result['detail'].assetUnit;
              dt['totalNoAsset'] = result['detail'] && result['detail'].totalNoAsset;
              dt['assetLocation'] = '';
              if (result['detail']) {
                if (result['detail'].assetLocation == 'Area') {
                  dt['assetLocation'] = 2;
                }
                if (result['detail'].assetLocation == 'Office') {
                  dt['assetLocation'] = 1;
                }
                dt['id'] = result['detail'] && result['detail'].id;
              }

              setTimeout(() => {
                if (result['detail']) {
                  this.workOutputAssetForm.setValue(dt);
                }
              }, 200);
              this.unicode.initUnicode();
            }
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'कृपया डाटा छान्नुहाेस्';
      this.errNotification.open();
    }
  }

  Close() {

    this.router.navigate(['/activity/activity-output']);

  }
  Close1() {
    this.avs.showActivity(this.actId).subscribe((res) => {

      localStorage.setItem('ActivityDatas', JSON.stringify(res));
    //   this.ts.changeMessage("on");
      this.activityDatas = JSON.parse(localStorage.getItem('ActivityDatas'));
    //   this.router.navigate(["/approval/technical-approval/view", { id: this.activityDatas["id"], name: this.activityDatas["activityName"], cost: this.activityDatas["totalEstimatedCost"] }]);
    this.router.navigate(['/activity/activity-output']);
}, (error) => {
      console.info(error);
    });
  }

  clear() {
    this.updated = false;
    this.outputTypeForm.get('outputType').enable();
    this.outputTypeForm.controls['outputType'].setValue('');
    this.activityType = '';
  }

  Save(post) {
    let routeParams = this.activeRoute.snapshot.params;
    // Save service Details
    if (this.activityType == 'SD') {
      if (routeParams['id']) {
        this.jqxLoader.open();
        post['activity'] = routeParams['id'];
        post['outputType'] = this.outputTypeForm.controls['outputType'].value;


        // console.info(post);
        this.serviceDetails.store(post).subscribe(
          result => {
            if (result['message']) {
              this.serviceDetailsForm.reset();
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.Close1();
            }
            this.jqxLoader.close();
            if (result['error']) {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }
          },
          error => {
            this.jqxLoader.close();
            console.info(error);
          }
        );
      } else {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'कृपया डाटा छान्नुहाेस्';
        this.errNotification.open();
      }
    }
    // Save Capacity Building
    if (this.activityType == 'CB') {
      if (routeParams['id']) {
        this.jqxLoader.open();
        post['activity'] = routeParams['id'];
        post['outputType'] = this.outputTypeForm.controls['outputType'].value;
        this.cbs.store(post).subscribe(
          result => {
            if (result['message']) {
              this.capacityBuildingForm.reset();
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.Close1();
            }
            this.jqxLoader.close();
            if (result['error']) {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }
          },
          error => {
            this.jqxLoader.close();
            console.info(error);
          }
        );
      } else {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'कृपया डाटा छान्नुहाेस्';
        this.errNotification.open();
      }
    }
    // For workoutput asset
    if (this.activityType == 'WOA') {
      const assetDetails = [];
      let i = 0, assetType = "";
      for (let assetDetail of post['assetDetails']) {
        if (i == 0) { assetType = assetDetail['assetType'] }
        assetDetail['assetType'] = assetType;
        assetDetails.push(assetDetail);
        i++;
      }
      post['assetDetails'] = assetDetails;
      if (post['assetLocation'] == 1) {
        post['assetLocation'] = 'Office';
      }
      if (post['assetLocation'] == 2) {
        post['assetLocation'] = 'Area';
      }
      if (routeParams['id']) {
        this.jqxLoader.open();
        post['activity'] = routeParams['id'];
        post['outputType'] = this.outputTypeForm.controls['outputType'].value;

        this.wos.store(post).subscribe(
          result => {
            if (result['message']) {
              this.workOutputAssetForm.reset();
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.Close1();
            }
            this.jqxLoader.close();
            if (result['error']) {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }
          },
          error => {
            this.jqxLoader.close();
            console.info(error);
          }
        );
      } else {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'कृपया डाटा छान्नुहाेस्';
        this.errNotification.open();
      }
    }
  }
}
