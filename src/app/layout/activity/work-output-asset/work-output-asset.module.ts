import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { WorkOutputAssetComponent } from './work-output-asset.component';
import { WorkOutputAssetRoutingModule } from './work-output-asset-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    WorkOutputAssetRoutingModule
  ],
  declarations: [
    WorkOutputAssetComponent,
  ]
})
export class WorkOutputAssetModule { }
