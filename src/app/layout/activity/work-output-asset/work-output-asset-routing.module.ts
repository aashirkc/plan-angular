import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {  WorkOutputAssetComponent } from './work-output-asset.component';

const routes: Routes = [
  {
    path: '',
    component:  WorkOutputAssetComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkOutputAssetRoutingModule { }
