import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ActivityComponent } from "./activity.component";
import { SharedModule } from "../../shared/modules/shared.module";
import { ActivityRoutingModule } from "./activity-routing.module";

@NgModule({
    imports: [CommonModule, SharedModule, ActivityRoutingModule],
    declarations: [ActivityComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class ActivityModule {}
