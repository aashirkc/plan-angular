import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavListWidgetComponent } from './nav-list-widget.component';

describe('NavListWidgetComponent', () => {
  let component: NavListWidgetComponent;
  let fixture: ComponentFixture<NavListWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavListWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavListWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
