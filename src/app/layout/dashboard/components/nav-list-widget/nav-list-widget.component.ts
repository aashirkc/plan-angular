import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-nav-list-widget',
  templateUrl: './nav-list-widget.component.html',
  styleUrls: ['./nav-list-widget.component.scss']
})
export class NavListWidgetComponent implements OnInit {

  @Input('ListData') ListData;
  @Input() ListTitle:string;
  @Input('ListIcon') ListIcon;

  widgetItems:any;
  widgetTitle:string;
  widgetIcon:string;

  constructor() { }

  ngOnInit() {
    this.widgetItems = this.ListData;
    this.widgetTitle = this.ListTitle
    this.widgetIcon = this.ListIcon;
  }

}
