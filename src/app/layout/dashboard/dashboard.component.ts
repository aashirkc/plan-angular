import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { TranslateService } from '@ngx-translate/core';
import { jqxChartComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxchart';
// import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { Router } from '@angular/router';
import { DashboardServiceService, PlanningYearService } from '../../shared';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';

// import * as XLSX from 'xlsx';
// import { AOA } from 'file-saver';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    @ViewChild('myChart') myChart: jqxChartComponent;
    @ViewChild('myChart2') myChart2: jqxChartComponent;
    @ViewChild('myChart3') myChart3: jqxChartComponent;
    @ViewChild('myChart4') myChart4: jqxChartComponent;
    // @ViewChild('myChart5') myChart5: jqxChartComponent;
    // @ViewChild('myChart6') myChart6: jqxChartComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    // @ViewChild('excelGrid') excelGrid: jqxGridComponent;

    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    planYearAdapter: Array<any> = [];
    sisUsersDetails: any;

    public Setup: any = {
        title: 'Setup',
        icon: 'fa-cog',
        items: [
            {
                name: 'Academic Year',
                route: '/setup/academic-year'
            }
        ]
    };
    public utility: any = {
        title: 'Utility',
        icon: 'fa-clock-o',
        items: [
            {
                name: 'Manage Users',
                route: '/utility/user'
            }
            // {
            //     name: 'Manage Permissions',
            //     route: '/utility/permission'
            // },
            // {
            //     name: 'Change Password',
            //     route: '/utility/change-password'
            // }
        ]
    };
    approvalSearchForm: FormGroup;
    fiscalYear: string;
    branch: number;
    site: number;
    supplier: number;
    user: number;
    department: number;

    constructor(
        private pys: PlanningYearService,
        private fb: FormBuilder,
        private translate: TranslateService,
        private router: Router,
        private dbs: DashboardServiceService
    ) {
        this.approvalSearchForm = this.fb.group({
            planYear: ['', Validators.required]
        });
        let userData = JSON.parse(localStorage.getItem('sisUser'));
        this.sisUsersDetails = userData;
        this.getTranslation();
    }

    transData: any;

    getTranslation() {
        this.translate
            .get([
                'FINANCIALYEAR',
                'DEPARTMENT',
                'CHARTOFITEMS',
                'DEPRECIATIONRATION',
                'BRANCH',
                'SUPPLIER',
                'SITE',
                'ASSETACCESSORIESMASTER',
                'REQUISITIONSLIP',
                'GOODSISSUE',
                'GOODSISSUERETURN',
                'GOODSRETURN',
                'GOODSDISPOSAL',
                'PURCHASEORDER',
                'GOODSRECEIVINGNOTES',
                'GOODSRECEIVINGNOTESOPENING',
                'ASSETSLEDGER',
                'ASSETSWISEREPORT',
                'REQUISITIONREPORT',
                'PURCHASEORDERREPORT',
                'GOODSRECEIPTNOTEREPORT',
                'GOODSISSUEREPORT',
                'GOODSDISPOSALREPORT',
                'TAXDEPRECIATIONREPORT',
                'APPROVEREQUISITION',
                'APPROVEPO',
                'APPROVEISSUE',
                'YEARLYREPAIREXPENSES',
                'ITEMREPAIR',
                'GRNUPDATE',
                'MANAGEUSER',
                'MANAGEPERMISSION'
            ])
            .subscribe((translation: [string]) => {
                this.transData = translation;
            });
    }

    ngOnInit() {
        this.pys.index({}).subscribe(
            res => {
                this.planYearAdapter = res;
                if (this.planYearAdapter.length > 0) {
                    let planCode = this.planYearAdapter[0].yearCode || '';
                    this.approvalSearchForm
                        .get('planYear')
                        .patchValue(planCode);
                    this.getDashboardData(this.approvalSearchForm.value);
                }
            },
            error => {
                console.info(error);
            }
        );
        // this.getDashboardData();
        // this.generateData();
        // let idsArray = ['input1','input2'];

        // setTimeout(() => {
        //     this.dbs.setTranslation(idsArray);
        // }, 2000);
    }
    planYearChange($event) {
        this.jqxLoader.open();
        let post = {};
        post = this.approvalSearchForm.value;
        this.getDashboardData(post);
        this.jqxLoader.close();
    }

    gettranslation(event) {
        // let input = event.target.value;
        // console.log(input);
        // this.dbs.getTranslation(input);
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

    projectDetail: any;
    projectDetailLoading: string = 'open';
    costDetail: any;
    costDetailLoading: string = 'open';
    inclusionDetail: any;
    inclusionDetailLoading: string = 'open';
    getDashboardData(data) {
        this.dbs.getchartData(data).subscribe(
            result => {
                this.projectDetail = result;
                this.projectDetail['wardData'].forEach(result => {
                    // let temp: string[] = result['wardName'].split(" ");
                    // result['wardName'] = "वडा" + temp[1];
                });
                // For Chart 1
                let commProjects =
                    (this.projectDetail && this.projectDetail.committe) || 0;
                let contProjects =
                    (this.projectDetail && this.projectDetail.contract) || 0;
                let AmtProjects =
                    (this.projectDetail && this.projectDetail.Amanat) || 0;
                let SProjects =
                    (this.projectDetail && this.projectDetail.Sahakari) || 0;
                this.sampleData = [
                    {
                        Name: 'उपभोक्ता समिति',
                        Projects: commProjects
                    },
                    {
                        Name: 'ठेक्का',
                        Projects: contProjects
                    },
                    {
                        Name: 'अमानत',
                        Projects: AmtProjects
                    },
                    {
                        Name: 'सहकारी संस्था',
                        Projects: SProjects
                    },
                ];

                //For Chart 2

                this.wardData =
                    this.projectDetail && this.projectDetail.wardData;

                // For Chart 3
                let appCost =
                    (this.projectDetail && this.projectDetail.proposedCost) ||
                    0;
                let issCost =
                    (this.projectDetail && this.projectDetail.issueAmount) || 0;
                this.dataCosts = [
                    { CostType: 'खर्च रकम ', Amount: issCost },
                    { CostType: 'बजेट रकम', Amount: appCost }
                ];

                this.dataInclusions =
                    (this.projectDetail &&
                        this.projectDetail.inclusion.filter(d => d.noi > 0)) ||
                    [];
                // [
                //             { InclusionType: 'दलित', Total: dalit },
                //             { InclusionType: 'आदिवासि', Total: indigenous },
                //             { InclusionType: 'मदेसी', Total: madhesi },
                //             { InclusionType: 'थारु', Total: tharu },
                //             { InclusionType: 'महिला', Total: woman },
                //         ];

                this.projectDetailLoading = 'close';
            },
            error => {
                this.projectDetailLoading = 'close';
                console.log(error);
            }
        );

        this.dbs.getGraph(data).subscribe(
            result => {
                // console.log(result);
                // this.dataDemo = [result && result[0],result && result[4],result && result[5]];
                // this.dataDemo1 = [result && result[1],result && result[2],result && result[3]];
                // let appCost = this.costDetail && this.costDetail.proposedCost[0][0] || 0;
                // let issCost = this.costDetail && this.costDetail.proposedCost[0][1] || 0;
                // this.dataCosts =  [
                //     { CostType: 'बजेट रकम', Amount: appCost },
                //     { CostType: 'खर्च रकम ', Amount: issCost },
                // ];
                // this.costDetailLoading = 'close';
            },
            error => {
                this.dataDemo = [];
                console.log(error);
            }
        );

        // this.dbs.getInclusionGroup({}).subscribe(
        //     result =>{
        //         console.log(result);
        //         this.inclusionDetail = result;
        //         let dalit = this.inclusionDetail && this.inclusionDetail.sumDalit[0] || 0;
        //         let indigenous = this.inclusionDetail && this.inclusionDetail.sumIndigenous[0] || 0;
        //         let madhesi = this.inclusionDetail && this.inclusionDetail.sumMadhesi[0] || 0;
        //         let tharu = this.inclusionDetail && this.inclusionDetail.sumTharu[0] || 0;
        //         let woman = this.inclusionDetail && this.inclusionDetail.sumWoman[0] || 0;
        //         this.dataInclusions = [
        //             { InclusionType: 'दलित', Total: dalit },
        //             { InclusionType: 'आदिवासि', Total: indigenous },
        //             { InclusionType: 'मदेसी', Total: madhesi },
        //             { InclusionType: 'थारु', Total: tharu },
        //             { InclusionType: 'महिला', Total: woman },
        //         ];

        //         this.inclusionDetailLoading = 'close';
        //     },
        //     error => {
        //         this.inclusionDetailLoading = 'close';
        //         console.log(error);
        //     }
        // );

        // this.dbs.getImplementationArea({}).subscribe(
        //     result => {
        //         console.log(result);
        //     },
        //     error => {
        //         console.log(error);
        //     }
        // )
    }

    //For Chart 1

    description = '';
    title = '';
    sampleData: any = [];
    // sampleData: any = [
    //     { Name: 'Committe Projects', Projects: 2100, Percent: 35 },
    //     { Name: 'Contractor Projects', Projects: 3900, Percent: 65 },
    // ];

    padding: any = { left: 20, top: 5, right: 20, bottom: 5 };

    titlePadding: any = { left: 20, top: 0, right: 0, bottom: 10 };

    xAxis: any = {
        // unitInterval: 1,
        dataField: 'Name',
        displayText: 'नाम',
        gridLines: { visible: true },
        flip: false
    };

    valueAxis: any = {
        // dataField:'Projects',
        // displayText:'परियोजनाहरु',
        minValue: 0,

        flip: true,
        labels: {
            visible: true
            // formatFunction: (value: string) => {
            //     return parseInt(value) / 1000000;
            // }
        }
    };
    seriesGroups: any[] = [
        {
            type: 'column',
            orientation: 'horizontal',
            columnsGapPercent: 70,
            toolTipFormatSettings: { thousandsSeparator: ',' },
            series: [
                {
                    dataField: 'Projects',
                    displayText: 'परियोजनाहरु',
                    showLabels: true
                }
            ]
        }
    ];

    printClick1() {
        let content = this.myChart.host[0].outerHTML;
        this.printChart(content);
    }

    // for chart 2
    wardData: any[] = [];
    // [
    //     { ward: 1, total: 30, completed: 5, working: 25},
    //     { ward: 2, total: 25, completed: 25, working: 5},
    //     { ward: 3, total: 30, completed: 5, working: 25 },
    //     { ward: 6, total: 35, completed: 25, working: 45},
    //     { ward: 7, total: 5, completed: 20, working: 25},
    //     { ward: 9, total: 30, completed: 10, working: 30},
    //     { ward: 10, total: 60, completed: 45, working: 10},
    //     { ward: 11, total: 30, completed: 5, working: 25 },
    //     { ward: 12, total: 35, completed: 25, working: 45},
    //     { ward: 13, total: 5, completed: 20, working: 25},
    //     { ward: 14, total: 30, completed: 10, working: 30},
    //     { ward: 15, total: 60, completed: 45, working: 10}
    // ];

    xAxis2: any = {
        dataField: 'wardName',
        displayText: 'वडाको नाम',
        tickMarks: {
            visible: true,
            interval: 1,
            color: '#BCBCBC'
        },
        gridLines: {
            visible: true,
            interval: 1,
            color: '#BCBCBC'
        },
        flip: false,
        valuesOnTicks: true
    };

    valueAxis2: any = {
        // unitInterval: 5,
        title: { text: '' },
        tickMarks: { color: '#BCBCBC' },
        gridLines: { color: '#BCBCBC' },
        labels: {
            horizontalAlignment: 'left'
        }
    };

    seriesGroups2: any[] = [
        {
            type: 'column',
            columnsGapPercent: 10,
            seriesGapPercent: 5,
            columnsMaxWidth: 10,
            columnsMinWidth: 1,
            toolTipFormatSettings: { thousandsSeparator: ',' },
            series: [
                {
                    dataField: 'ongoing',
                    displayText: 'चलिरहेको परियोजना',
                    formatSettings: { thousandsSeparator: ',' }
                },
                {
                    dataField: 'notStarted',
                    displayText: 'सुरु हुन बाँकी योजना',
                    formatSettings: { thousandsSeparator: ',' }
                },
                {
                    dataField: 'completed',
                    displayText: 'पूर्ण भएका परियोजना',
                    formatSettings: { thousandsSeparator: ',' }
                }
            ]
        }
    ];

    barClicked2(event) {
        if (event.args) {
            let plan = this.approvalSearchForm.get('planYear').value;
            let area = this.wardData[event.args.elementIndex].wardName;
            let wardCode = this.wardData[event.args.elementIndex].wardCode;
            // alert('Ward: ' + (event.args.elementIndex + 1) + ' - ' + event.args.serie.displayText + ' :' + event.args.elementValue);
            this.router.navigate([
                '/area',
                { planYear: plan, ward: area, code: wardCode }
            ]);
        }
    }

    printClick2() {
        let content = this.myChart2.host[0].outerHTML;
        this.printChart(content);
    }

    // for chart 3
    dataCosts: any = [];
    // [
    //     { CostType: 'Approved', Amount: 50000000 },
    //     { CostType: 'Issued', Amount: 20000000 },
    // ];

    seriesGroups3: any[] = [
        {
            type: 'pie',
            showLegend: true,
            enableSeriesToggle: false,
            toolTipFormatSettings: { thousandsSeparator: ',' },
            series: [
                {
                    dataField: 'Amount',
                    displayText: 'CostType',
                    showLabels: true,
                    labelRadius: 160,
                    labelLinesEnabled: true,
                    labelLinesAngles: true,
                    labelsAutoRotate: false,
                    initialAngle: 0,
                    radius: 125,
                    minAngle: 0,
                    maxAngle: 180,
                    centerOffset: 0,
                    offsetY: 170,
                    formatSettings: { thousandsSeparator: ',' },
                    formatFunction: (
                        value: any,
                        itemIdx: any,
                        serieIndex: any,
                        groupIndex: any
                    ) => {
                        if (isNaN(value)) return value;
                        return 'रु. ' + value;
                    }
                }
            ]
        }
    ];

    printClick3() {
        let content = this.myChart3.host[0].outerHTML;
        this.printChart(content);
    }

    // Chart 4

    dataInclusions: any = [];

    seriesGroups4: any[] = [
        {
            type: 'pie',
            showLegend: true,
            showLabels: true,
            enableSeriesToggle: false,
            toolTipFormatSettings: { thousandsSeparator: ',' },
            series: [
                {
                    dataField: 'noi',
                    displayText: 'nameNepali',
                    labelRadius: 120,
                    initialAngle: 15,
                    radius: 170,
                    centerOffset: 0,
                    formatSettings: { thousandsSeparator: ',' }
                    // formatSettings: { sufix: '', decimalPlaces: 1 }
                }
            ]
        }
    ];

    printClick4() {
        let content = this.myChart4.host[0].outerHTML;
        this.printChart(content);
    }

    // Global Chart Function
    printChart(content): void {
        let newWindow = window.open('', '', 'width=800, height=500'),
            document = newWindow.document.open(),
            pageContent =
                '<!DOCTYPE html>' +
                '<html>' +
                '<head>' +
                '<meta charset="utf-8" />' +
                '<title>jQWidgets Chart</title>' +
                '</head>' +
                '<body>' +
                content +
                '</body></html>';
        try {
            document.write(pageContent);
            document.close();
            newWindow.print();
            newWindow.close();
        } catch (error) {}
    }

    /*************************************************CHART 5******************************* */
    dataDemo: any[];
    dataDemo1: any[];
    // [
    //     { ward: 'तालिम', total: 30, completed: 5, ongoing: 5 },
    //     { ward: 'पक्की सडक', total: 30, completed: 5, ongoing: 5 },
    //     { ward: 'ग्राभेल बाटो', total: 30, completed: 5, ongoing: 5 },
    //     { ward: 'खानेपानी', total: 30, completed: 5, ongoing: 5 },
    //     { ward: 'सिंचाई', total: 30, completed: 5, ongoing: 5 },

    // ];
    xAxis5: any = {
        dataField: 'name',
        displayText: 'नाम',
        showGridLines: true
    };
    valueAxis5: any = {
        unitInterval: 1,
        // title: {
        //     visible: true,
        //     text: 'Temperature [C]<br>'
        // },
        labels: {
            formatSettings: {
                decimalPlaces: 1,
                negativeWithBrackets: false
            },
            horizontalAlignment: 'right'
        }
    };
    seriesGroups5: any[] = [
        {
            type: 'column',
            columnsGapPercent: 25,
            seriesGapPercent: 10,
            columnsMaxWidth: 40,
            columnsMinWidth: 1,
            toolTipFormatSettings: { thousandsSeparator: ',' },
            series: [
                {
                    dataField: 'Total',
                    displayText: 'जम्मा',
                    formatSettings: { thousandsSeparator: ',' }
                },
                {
                    dataField: 'Ongoing',
                    displayText: 'चलिरहेको',
                    formatSettings: { thousandsSeparator: ',' }
                },
                {
                    dataField: 'Completed',
                    displayText: 'पूर्णभएको',
                    formatSettings: { thousandsSeparator: ',' }
                }
            ]
        }
    ];
    // printClick5() {
    //     let content = this.myChart5.host[0].outerHTML;
    //     let newWindow = window.open('', '', 'width=800, height=500'),
    //         document = newWindow.document.open(),
    //         pageContent =
    //             '<!DOCTYPE html>' +
    //             '<html>' +
    //             '<head>' +
    //             '<meta charset="utf-8" />' +
    //             '<title>jQWidgets Chart</title>' +
    //             '</head>' +
    //             '<body style="width:100%">' + content + '</body></html>';
    //     try {
    //         document.write(pageContent);
    //         document.close();
    //         newWindow.print();
    //         newWindow.close();
    //     }
    //     catch (error) {
    //     }
    // }
    printWholePage(): void {
        let printContents, popupWin;
        printContents = document.getElementById('reportContainer').innerHTML;
        popupWin = window.open(
            '',
            '_blank',
            'top=0,left=0,height=100%,width=auto'
        );
        popupWin.document.open();
        popupWin.document.write(`
          <html>
            <head>
              <title>Print tab</title>
              <style>

              .hidePrint{
                  display:inline;
              }
              .p{
                margin-bottom: 5px;
              }
              .table-bordered {
                  border: 1px solid #eceeef;
              }
              .chart-container{
                background: #fff;
                background: #0a57b3;
                border: 0px !important;
                margin-bottom: 10px;
            }
              .hideDiv{display:none}
              .col-md-1 {width:8%;  float:left;padding-bottom:15px;}
              .col-md-2 {width:16%; float:left;padding-bottom:15px;}
              .col-md-3 {width:25%; float:left;padding-bottom:15px;}
              .col-md-4 {width:33%; float:left;padding-bottom:15px;}
              .col-md-5 {width:42%; float:left;padding-bottom:15px;}
              .col-md-6 {width:50%; float:left; margin:0;padding:0;}
              .col-md-7 {width:58%; float:left;padding-bottom:15px;}
              .col-md-8 {width:66%; float:left;padding-bottom:15px;}
              .col-md-9 {width:75%; float:left;padding-bottom:15px;}
              .col-md-10{width:83%; float:left;padding-bottom:15px;}
              .col-md-11{width:92%; float:left;padding-bottom:15px;}
              .col-md-12{width:100%; float:left;padding-bottom:15px;}
              .row{width:100%; float:left;}
              #fullPage{width:100% !important; float:left; padding-bottom:15px !important;}
              #fullPage2{width:100% !important; float:left;padding-bottom:15px !important;}
              #paddingDiv{padding-top:20px !important;}
              .table {
                position:relative;
                width: 100%;
                max-width: 100%;
                margin-top: 20px;
                margin-bottom: 1rem;
                font-size: smaller;
              }
              .table {
                border-collapse: collapse;
                background-color: transparent;
              }
              .table-bordered th, .table-bordered td {
                  border: 1px solid #eceeef;
              }
              @font-face {
                font-family: 'nepaliNormal';
                src: url('assets/css/Kantipur.TTF');
                font-weight: normal;
                font-style: normal;
            }
            .nepaliText{
              font-family:'nepaliNormal';
              font-size:18px!important;
          }
              .table th, .table td {
                  padding: 0.55rem;
                  vertical-align: top;
                  border-top: 1px solid #eceeef;
                  text-align:left;
              }
              .last-td{
                display:none;
              }
              //........Customized style.......
              </style>
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
          </html>`);
        popupWin.document.close();
    }
}
