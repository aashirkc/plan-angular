import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Anusuchi4Component } from './anusuchi4.component';

const routes: Routes = [
    {
        path: '',
        component: Anusuchi4Component,
    }
]; 

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class Anusuchi4RoutingModule { }
