import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Anusuchi4Component } from './anusuchi4.component';
import { SharedModule } from 'app/shared/modules/shared.module';
import { Anusuchi4RoutingModule } from './anusuchi4-routing.module';
import { AnusuchiSearchBarModule } from '../anusuchi-search-bar/anusuchi-search-bar.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AnusuchiSearchBarModule,
    Anusuchi4RoutingModule
  ],
  declarations: [Anusuchi4Component],
  schemas: [NO_ERRORS_SCHEMA]
})
export class Anusuchi4Module { }
