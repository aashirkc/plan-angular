import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit, AfterViewInit } from '@angular/core';
import { PlanningYearService, ToggleSidebarService, UnicodeTranslateService, AgencyTypeService, AllReportService, NepaliInputComponent, KataSanchalanService, DateConverterService, ProgressReportingTippaniAadeshService } from '../../../shared';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-anusuchi4',
  templateUrl: './anusuchi4.component.html',
  styleUrls: ['./anusuchi4.component.scss']
})
export class Anusuchi4Component implements OnInit {


  organizationMasterData: any;
  yearAdapter: Array<any> = [];
  anusuchi4: FormGroup;
  SearchDatas: any;
  activityAdapter: Array<any> = [];
  date: any;
  anusuchiReport: FormGroup;
  activityDatas: any;
  check: boolean = true;
  childType: any = [];
  agencyName: any;
  agencyType: any;
  lang: any;


  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  @ViewChild('act') act: jqxComboBoxComponent;

  @ViewChild('nepin1') nepin1: NepaliInputComponent;
  @ViewChild('nepin2') nepin2: NepaliInputComponent;
  @ViewChild('nepin3') nepin3: NepaliInputComponent;
  @ViewChild('nepin4') nepin4: NepaliInputComponent;
  @ViewChild('nepin5') nepin5: NepaliInputComponent;

  constructor(
    private fb: FormBuilder,
    private pys: PlanningYearService,
    private rs: AllReportService,
    private ks: KataSanchalanService,
    private unicode: UnicodeTranslateService,
    private ds: DateConverterService,
    private ts: ToggleSidebarService,
    private ats: AgencyTypeService,
    private prtas: ProgressReportingTippaniAadeshService
  ) {
    this.createForm();

  }

  createForm() {
    this.anusuchi4 = this.fb.group({
      'planYear': ['', Validators.required],
      'child': [''],
      'parent': [''],
    });
    this.anusuchiReport = this.fb.group({
      'date': [this.ds.getToday()['fulldate']],
      'activity': [''],
      'aamdaaniBibaran1': [''],
      'aamdaaniBibaran2': [''],
      'aamdaaniRakam1': [''],
      'aamdaaniRakam2': [''],
      'aamdaaniRakam3': [''],
      'aamdaaniRakam4': [''],
      'aamdaaniRakam5': [''],
      'aamdaaniRakam6': [''],
      'aamdaaniRakam7': [''],
      'aamdaaniRakam8': [''],
      'kharchaBibaran1': [''],
      'kharchaBibaran2': [''],
      'kharchaBibaran3': [''],
      'kharchaRakam1': [''],
      'kharchaRakam2': [''],
      'kharchaRakam3': [''],
      'kharchaRakam4': [''],
      'kharchaRakam5': [''],
      'kharchaRakam6': [''],
      'kharchaRakam7': [''],
      'kharchaRakam8': [''],

    });

    this.anusuchiReport.patchValue({
      date: this.ds.getToday()['fulldate']
    });

    this.anusuchiReport.patchValue({
      date: this.ds.getToday()['fulldate']
    });

    this.anusuchiReport.patchValue({
      date: this.ds.getToday()['fulldate']
    });
    // this.unicode.initUnicode();
  }

  // --- search bar emitter functions
  search = ({ value, id }) => this.SearchData(value, id)

  changeLoadingStatus($event) {
    !$event && this.jqxLoader.close();
    $event && this.jqxLoader.open();
  }

  showError($event) {
    let messageDiv: any = document.getElementById('error');
    messageDiv.innerText = $event;
    this.errNotification.open();
  }

  // when sidebar is opened
  searchWithActivityDatas = () => {
    try {
      const activityDatas = JSON.parse(localStorage.getItem("ActivityDatas"));
      if (activityDatas) {
        const requestParams = {
          planYear: activityDatas.planYear,
          activity: activityDatas.id,
          activityType: 3
        }
        this.prtas.getOrganizationName().subscribe((res) => {
          this.organizationMasterData = res;
          this.search({ value: requestParams, id: activityDatas.id });
        });
      }
    } catch (e) {
      debugger;
      console.warn(e);
    }
  }
  // --- end

  ngOnInit() {
    this.ats.show(0).subscribe(
      result => {
        if (result['length'] > 0) {

          this.agencyType = result;
        }
      },
      error => {
        console.log(error);
      }
    );

    this.date = this.ds.getToday()['fulldate'];
    this.ts.currentMessage.subscribe((data) => {
      if (data == "on") {
        this.check = false;
        // local storage gets set when sidebar is opened taking current activity
        this.searchWithActivityDatas();
      }
      if (data == "off") {
        this.check = true;
      }
      console.log(this.check);
    });
    this.rs.getOrg().subscribe((res) => {
      this.organizationMasterData = res;
    }, (error) => {
      console.info(error);

    });
    this.activityDatas = JSON.parse(localStorage.getItem('ActivityDatas'));


    this.pys.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.yearAdapter = [];
      } else {
        this.yearAdapter = res;
        if (this.activityDatas) {
          this.anusuchi4.get('planYear').patchValue(this.activityDatas['planYear']);
          this.ChangePlanYear1(this.activityDatas['planYear']);
        } else {
          this.anusuchi4.get('planYear').patchValue(res[0]['yearCode']);
          this.ChangePlanYear(res[0]['yearCode']);
        }

      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
  ChangePlanYear1(id) {
    console.log(id);
    this.ks.indexActivity({ planYear: id, activityType: 3 }).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
        console.log(this.activityDatas['id']);
        setTimeout(() => {
          if (this.check) {
            this.act.selectItem(this.activityDatas['id']);
          }
          // this.act.selectItem(this.activityDatas['id']);
          this.SearchData(this.anusuchi4.value, this.activityDatas['id']);
        }, 100);

      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }


  clearNepin() {
    this.nepin1.clearInput();
    this.nepin2.clearInput();
    this.nepin3.clearInput();
    this.nepin4.clearInput();
    this.nepin5.clearInput();
  }

  SearchData(post, val) {
    // post['activity'] = this.act.val();
    post['activity'] = val;

    this.jqxLoader.open();
    if (val) {
      this.rs.getanusuchi4Report(post).subscribe((res) => {
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.SearchDatas = [];
        } else {
          this.SearchDatas = res;
          this.anusuchiReport.reset();

          this.clearNepin();
          let id = val;
          this.rs.getanusuchi4ReportData(id).subscribe((res) => {
            console.log(res);
            if (res['error']) {
              this.anusuchiReport.reset();
              // res['error'] && res['error']['message'] && this.showMessage(res['error']['message']);
              this.clearNepin();
              this.anusuchiReport.controls['date'].patchValue(this.date);
            } else {
              this.anusuchiReport.reset();
              this.clearNepin();
              this.anusuchiReport.controls['aamdaaniBibaran1'].patchValue(res['aamdaaniBibaran1']);
              this.anusuchiReport.controls['aamdaaniBibaran2'].patchValue(res['aamdaaniBibaran2']);
              this.anusuchiReport.controls['aamdaaniRakam1'].patchValue(res['aamdaaniRakam1']);
              this.anusuchiReport.controls['aamdaaniRakam2'].patchValue(res['aamdaaniRakam2']);
              this.anusuchiReport.controls['aamdaaniRakam3'].patchValue(res['aamdaaniRakam3']);
              this.anusuchiReport.controls['aamdaaniRakam4'].patchValue(res['aamdaaniRakam4']);
              this.anusuchiReport.controls['aamdaaniRakam5'].patchValue(res['aamdaaniRakam5']);
              this.anusuchiReport.controls['aamdaaniRakam6'].patchValue(res['aamdaaniRakam6']);
              this.anusuchiReport.controls['aamdaaniRakam7'].patchValue(res['aamdaaniRakam7']);
              this.anusuchiReport.controls['aamdaaniRakam8'].patchValue(res['aamdaaniRakam8']);
              this.anusuchiReport.controls['kharchaBibaran1'].patchValue(res['kharchaBibaran1']);
              this.anusuchiReport.controls['kharchaBibaran2'].patchValue(res['kharchaBibaran2']);
              this.anusuchiReport.controls['kharchaBibaran3'].patchValue(res['kharchaBibaran3']);
              this.anusuchiReport.controls['kharchaRakam1'].patchValue(res['kharchaRakam1']);
              this.anusuchiReport.controls['kharchaRakam2'].patchValue(res['kharchaRakam2']);
              this.anusuchiReport.controls['kharchaRakam3'].patchValue(res['kharchaRakam3']);
              this.anusuchiReport.controls['kharchaRakam4'].patchValue(res['kharchaRakam4']);
              this.anusuchiReport.controls['kharchaRakam5'].patchValue(res['kharchaRakam5']);
              this.anusuchiReport.controls['kharchaRakam6'].patchValue(res['kharchaRakam6']);
              this.anusuchiReport.controls['kharchaRakam7'].patchValue(res['kharchaRakam7']);
              this.anusuchiReport.controls['kharchaRakam8'].patchValue(res['kharchaRakam8']);
              this.anusuchiReport.controls['date'].patchValue(this.date);
            }
            this.anusuchiReport.controls['activity'].patchValue(id);
          });
        }
        this.jqxLoader.close();
      }, (error) => {
        this.jqxLoader.close();
        error['error'] && this.showMessage(error['error']);
      });
    }
    else {
      this.jqxLoader.close();
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'कृपया याेजना छान्नुहाेस्';
      this.errNotification.open();
    }
  }

  showMessage(message) {
    let messageDiv: any = document.getElementById('error');
    messageDiv.innerText = message;
    this.errNotification.open();
  }

  ChangePlanYear(id) {
    this.ks.indexActivity({ planYear: id, activityType: 3 }).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }

  /**
* Export Report Data to Excel
*/
  exportReport(): void {
    let htmltable = document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  /**
   * Open Print Window to print report
   */
  printReport(): void {
    console.log(this.anusuchiReport.value);
    let post = this.anusuchiReport.value;
    // if (this.check)
    //   post['activity'] = this.act.val();
    // else
    //   post['activity'] = this.activityDatas['id'];
    //  console.log(this.act.val());
    this.rs.sendanusuchi4Report(post).subscribe((res) => {
      console.log(res);
      if (res['message'] === 'success') {

      }
    });
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .table-container table th{
              text-align: center;
          }

          .table-container table .tleft{
              text-align: left !important;
          }

          .table-container table .tright{
              text-align: right !important;
          }
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }

          @font-face {
            font-family: 'nepaliNormal';
            src: url('assets/css/Kantipur.TTF');
            font-weight: normal;
            font-style: normal;
        }
        .nepaliText{
          font-family:'nepaliNormal';
          font-size:18px!important;
      }
       .table {
            border-collapse: collapse;
            background-color: transparent;
          }

        .tableClas{
                border-collapse: collapse;
        }
    .tableClas  td {
          // border: 1px solid black;
          border: 1px solid #eceeef;
        }
        ul{
          list-style-type: none
      }
      .print_show{
        display: inline-block !important;
    }

    .print_hide{
        display: none !important;
      }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  implementingAgencyChange(selectedEvent) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.agencyName = displayText;
      let agencyCode = selectedEvent.target.value;
      this.getAgencyType(agencyCode);
    } else {
      this.childType = '';
    }
  }


  getAgencyType(agencyCode = 0, index = 0, type = null) {
    // remove previously added form control if the parent select is changed.
    this.ats.show(agencyCode).subscribe(
      result => {
        if (result['length'] > 0) {
          this.childType = result;
        } else {
          this.childType = '';
        }
      },
      error => {
        console.log(error);
      }
    );
  }


}
