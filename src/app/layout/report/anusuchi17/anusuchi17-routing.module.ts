import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Anusuchi17Component } from './anusuchi17.component';

const routes: Routes = [
    {
        path: '',
        component: Anusuchi17Component,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class Anusuchi17RoutingModule { }
