import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Anusuchi17Component } from './anusuchi17.component';
import { SharedModule } from 'app/shared/modules/shared.module';
import { Anusuchi17RoutingModule } from './anusuchi17-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    Anusuchi17RoutingModule
  ],
  declarations: [Anusuchi17Component],
  schemas: [NO_ERRORS_SCHEMA]
})
export class Anusuchi17Module { }
