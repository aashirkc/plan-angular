import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportComponent } from './report.component';

const routes: Routes = [
    {
        path: '',
        component: ReportComponent,
        children: [
            {
                path: '',
                redirectTo: 'activity-detail-ledger',
                pathMatch: 'full'
            },
            {
                path: 'program-report',
                loadChildren:
                    './program-report/program-report.module#ProgramReportModule'
            },
            {
                path: 'activity-detail-ledger',
                loadChildren:
                    './activity-detail-ledger/activity-detail-ledger.module#ActivityDetailLedgerModule'
            },
            {
                path: 'activity-report',
                loadChildren:
                    './activity-report/activity-report.module#ActivityReportModule'
            },
            {
                path: 'consumer-committee-cost',
                loadChildren:
                    './consumer-committee-cost/consumer-committee-cost.module#ConsumerCommitteeCostModule'
            },
            {
                path: 'planning-progress-report',
                loadChildren:
                    './planning-progress-report/planning-progress-report.module#PlanningProgressReportModule'
            },
            {
                path: 'detailed-estimate-and-abstract-of-cost',
                loadChildren:
                    './detailed-estimate-and-abstract-of-cost/detailed-estimate-and-abstract-of-cost.module#DetailedEstimateAndAbstractOfCostModule'
            },
            {
                path: 'measurement-book-and-bill-of-qty-report',
                loadChildren:
                    './measurement-book-and-bill-of-qty-report/measurement-book-and-bill-of-qty-report.module#MeasurementBookAndBillOfQtyReportModule'
            },
            {
                path: 'work-aception-report-thaka-bill',
                loadChildren:
                    './work-aception-report-thaka-bill/work-aception-report-thaka-bill.module#WorkAceptionReportThakaBillModule'
            },
            {
                path: 'anusuchi17',
                loadChildren: './anusuchi17/anusuchi17.module#Anusuchi17Module'
            },
            {
                path: 'anusuchi172',
                loadChildren:
                    './anusuchi172/anusuchi172.module#Anusuchi172Module'
            },
            {
                path: 'karyabidhi',
                loadChildren: './karyabidhi/karyabidhi.module#KaryabidhiModule'
            },
            {
                path: 'anusuchi1',
                loadChildren: './anusuchi1/anusuchi1.module#Anusuchi1Module'
            },
            {
                path: 'anusuchi2',
                loadChildren: './anusuchi2/anusuchi2.module#Anusuchi2Module'
            },
            {
                path: 'anusuchi3',
                loadChildren: './anusuchi3/anusuchi3.module#Anusuchi3Module'
            },
            {
                path: 'anusuchi4',
                loadChildren: './anusuchi4/anusuchi4.module#Anusuchi4Module'
            },
            {
                path: 'anusuchi5',
                loadChildren: './anusuchi5/anusuchi5.module#Anusuchi5Module'
            },
            {
                path: 'anusuchi6',
                loadChildren: './anusuchi6/anusuchi6.module#Anusuchi6Module'
            },
            {
                path: 'sarbajanik-faram',
                loadChildren:
                    './sarbajanik-faram/sarbajanik-faram.module#SarbajanikFaramModule'
            }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class ReportRoutingModule {}
