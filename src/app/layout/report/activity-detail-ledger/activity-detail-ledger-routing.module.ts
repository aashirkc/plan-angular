import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityDetailLedgerComponent } from './activity-detail-ledger.component';

const routes: Routes = [
  {
    path: '',
    component: ActivityDetailLedgerComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityDetailLedgerRoutingModule { }
