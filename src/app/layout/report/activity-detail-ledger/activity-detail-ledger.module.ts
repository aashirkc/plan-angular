import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ActivityDetailLedgerComponent } from './activity-detail-ledger.component';
import { ActivityDetailLedgerRoutingModule } from './activity-detail-ledger-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ActivityDetailLedgerRoutingModule
  ],
  declarations: [
    ActivityDetailLedgerComponent,
  ]
})
export class ActivityDetailLedgerModule { }
