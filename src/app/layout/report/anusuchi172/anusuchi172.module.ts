import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Anusuchi172Component } from './anusuchi172.component';
import { SharedModule } from 'app/shared/modules/shared.module';
import { Anusuchi172RoutingModule } from './anusuchi172-routing.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        Anusuchi172RoutingModule
    ],
    declarations: [Anusuchi172Component],
    schemas: [NO_ERRORS_SCHEMA]
})
export class Anusuchi172Module { }
