import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Anusuchi172Component } from './anusuchi172.component';

const routes: Routes = [
    {
        path: '',
        component: Anusuchi172Component,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class Anusuchi172RoutingModule { }
