import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, RouterState, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { DateConverterService, AllReportService, UnicodeTranslateService,  CurrentUserService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';

@Component({
  selector: 'app-anusuchi172',
  templateUrl: './anusuchi172.component.html',
  styleUrls: ['./anusuchi172.component.scss']
})
export class Anusuchi172Component implements OnInit {

  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  transData: any;
  organizationMasterData:  any;

  lang: any;
  getReport: any;
  // <span *ngIf="data['status']=='Not Started'"> {{ 'NOT_STARTED' | translate }}</span>
  // <span *ngIf="data['status']=='Ongoing'"> {{ 'ONGOINGE' | translate }}</span>
  // <span *ngIf="data['status']=='Completed'"> {{ 'COMPLETEDE' | translate }}</span>
  // <span *ngIf="data['status']=='Suspended'"> {{ 'SUSPENDED' | translate }}</span>
  // <span *ngIf="data['status']=='Spill Over'"> {{ 'SPILLOVERE' | translate }}</span>
  // <span *ngIf="data['status']=='Abandoned'"> {{ 'ABANDONEDE' | translate }}</span>
  statusAdapter: Array<any> = [
    {
      name: "सुरु नभएकाे",
      value: "Not Started"
    },
    {
      name: "चलिरहेको योजना",
      value: "Ongoing"
    },
    {
      name: "पूर्ण भएको योजना",
      value: "Completed"
    },
    {
      name: "रोक्का गरिएको",
      value: "Suspended"
    },
    {
      name: "समयावधि थप्नु पर्ने योजना",
      value: "Spill Over"
    },
    {
      name: "त्याग गरेका योजना",
      value: "Abandoned"
    }
  ];


  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('myViewWindow') myViewWindow: jqxWindowComponent;
  @ViewChild('myEditWindow') myEditWindow: jqxWindowComponent;
  @ViewChild('myReportWindow') myReportWindow: jqxWindowComponent;
  @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;

  constructor(
    private fb: FormBuilder,
    private cus: CurrentUserService,
    private dfs: DateConverterService,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    @Inject('API_URL') API_URL: string,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private report: AllReportService,
    private translate: TranslateService,
    private unicode: UnicodeTranslateService,
  ) {
    // this.createForm();
    this.getTranslation();
  }


  ngOnInit() {
    this.report.getOrg().subscribe((res) => {
      this.organizationMasterData = res;

    }, (error) => {
      console.info(error);

    });
    this.lang = this.translate.currentLang.substr(0,2);
    
    
  }
  getTranslation() {
    this.translate.get(['SN', 'regNo', 'universityRoll', 'ADD', 'EDIT', 'NAME', 'nepali', 'english', 'mobile', 'VIEW', 'DETAIL', "ACTIONS", "UPDATE", "DELETESELECTED", "RELOAD"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

 

  ngAfterViewInit() {
    this.cdr.detectChanges();
    this.jqxLoader.open();
    this.report.getAnusuchi172().subscribe((res) => {
      this.getReport = res;
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
  /**
* Export Report Data to Excel
*/
  exportReport(): void {
    let htmltable = document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  /**
   * Open Print Window to print report
   */
  printReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .table-container table th{
              text-align: center;
          }
          
          .table-container table .tleft{
              text-align: left !important;
          }
          
          .table-container table .tright{
              text-align: right !important;
          }
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            position:relative;
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          @font-face {
            font-family: 'nepaliNormal';
            src: url('assets/css/Kantipur.TTF');
            font-weight: normal;
            font-style: normal;
        }
        .nepaliText{
          font-family:'nepaliNormal';
          font-size:18px!important;
      }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .last-td{
            display:none;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
