/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Anusuchi172Component } from './anusuchi172.component';

describe('Anusuchi172Component', () => {
  let component: Anusuchi172Component;
  let fixture: ComponentFixture<Anusuchi172Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Anusuchi172Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Anusuchi172Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
