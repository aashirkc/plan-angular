import { Component, OnInit, ViewChild, } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from "@angular/common";
import { jqxNotificationComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxnotification";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
range:any=new Array(25);
activity:any='';
phase:any='';
activityId:any;

 @ViewChild("msgNotification")
  msgNotification: jqxNotificationComponent;
  @ViewChild("errNotification")
  errNotification: jqxNotificationComponent;

  constructor(
    private activeRoute: ActivatedRoute,
    private location:Location,
    private router: Router,
  ) { 

  }
  ngOnInit() {
    let data = this.activeRoute.snapshot.params;
    this.activity = data["activityName"];
    let a=data["phase"];
    this.activityId=data['activityId'];
    let b=a.split(' ');
    this.phase = b[0];
  }
  close(){
    this.router.navigate(["/report/sarbajanik-faram", { activityId:this.activityId}]);
  }

  printReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .table-container table th{
              text-align: center;
          }
          
          .table-container table .tleft{
              text-align: left !important;
          }
          
          .table-container table .tright{
              text-align: right !important;
          }
          .hide_div{
            display:none !important;

          }
          .show_div{
            display:inline-block !important ;
          }
          .p{
            margin-bottom: 5px;
          }
          .td {
              border: 1px solid #eceeef;
          }
         
          @font-face {
            font-family: 'nepaliNormal';
            src: url('assets/css/Kantipur.TTF');
            font-weight: normal;
            font-style: normal;
        }
        .nepaliText{
          font-family:'nepaliNormal';
          font-size:18px!important;
      }
       .table {
            border-collapse: collapse;
            background-color: transparent;
          }
         
        .tableClas{
                border-collapse: collapse; 
        }
    .tableClas  td {
          // border: 1px solid black;
          border: 1px solid #eceeef;
        }
        .tableClas  th {
          // border: 1px solid black;
          border: 1px solid #eceeef;
        }
        ul{
          list-style-type: none
      }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
