import { Component, OnInit,Inject, } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ActivityOutputService, } from "../../../../shared";
import print from './print-js/src/index';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
id:any;
phase:any;
phasename:any;
actName:any;
dataId:any;
imgSrc:any=[];
docUrl:any='';
img:any=[];
pdf:any=[];
  constructor(
    private activityOutputService: ActivityOutputService,
    private activeRoute: ActivatedRoute,
    private router:Router,
    @Inject('API_URL_DOC') docUrl: string
  ) { 
    let data = this.activeRoute.snapshot.params;
    this.id=data['id'];
    this.dataId=data['dataId'];
    this.phase={phase:data['phase']};
    this.actName=data['activityName']
    this.phasename=data['phase']
    this.docUrl=docUrl;
    this.docUrl=this.docUrl.slice(0, -1);
  }

  ngOnInit() {
  
  this.getFiles()
  }
  getFiles(){
    this.activityOutputService.getDocsList(this.id,this.phase).subscribe((res) => {
      if ( res[0].error) {
        
        console.log(res)
       
      } else {
        let a=res[0]['images'];
        this.imgSrc=[];
        this.img=[];
        this.pdf=[];
        var c,d;
        a.map((data)=>{
          // b=data['location'].split('/')
          c=data['name']
          d=c.split('.')
          if(d[d.length-1]==='jpeg' || d[d.length-1]==='png' || d[d.length-1]==='jpg'){
            this.img.push(this.docUrl+data['location'])
          }else{
            this.pdf.push(this.docUrl+data['location'])
          }
          this.imgSrc.push({name:c,path:this.docUrl+data['location'],type:d[d.length-1],id:data['id'],location:data['location']})
          // console.log(c)
        }) 
   console.log(this.imgSrc)
      }
    
  },
  error => {
      console.info(error);
     
  }
);
  }

  fileClick(path){
    window.open(path);
  
  }
  printFile(){
    if(this.img.length>0){
  print({
      printable: this.img,
      type: 'image',
      imageStyle: 'page-break-after: always;width:100%;height:100%;object-fit:contain;'
     })
    }
    // if(this.pdf.length>0){
    //   this.pdf.map((data)=>{
    //     print({printable:data, type:'pdf', showModal:true})
    //   })
     
    // }
  }

  deleteFile(id,location){
    let post={
      location:location
    }
    this.activityOutputService.deleteDocs(id,post).subscribe((res) => {
      if ( res['message']==='Success') {
        this.getFiles()
        console.log(res)
       
      } 
    
  },
  error => {
      console.info(error);
     
  }
);
  }

  uploadFile(){
    this.router.navigate(["/report/sarbajanik-faram/upload", { activityId:this.id,id:this.dataId, activityName: this.actName,phase:this.phasename }]);
    
   
  }
  close(){
    this.router.navigate(["/report/sarbajanik-faram", { activityId:this.id}]);
  }


}
