import { Component, OnInit, ViewChild, } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from "@angular/router";
import { ActivityOutputService, } from "../../../../shared";
import { jqxNotificationComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxnotification";

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  
  public uploader:FileUploader = new FileUploader({url: 'https://evening-anchorage-3159.herokuapp.com/api/'});
  public hasBaseDropZoneOver:boolean = false;
  public hasAnotherDropZoneOver:boolean = false;
  fileVar:any=[];
  id:any;
  activityId:any;
  percentDone;
  swidth='0';

  @ViewChild("msgNotification")
  msgNotification: jqxNotificationComponent;
  @ViewChild("errNotification")
  errNotification: jqxNotificationComponent;

  constructor(
    private http: HttpClient,
    private activeRoute: ActivatedRoute,
    private activityOutputService: ActivityOutputService,
    private router: Router,
  ) { }

  ngOnInit() {
    let data = this.activeRoute.snapshot.params;
    this.id=data['id'];
    this.activityId=data['activityId'];
  }

  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
    console.log(e);
  }
  dropped(event){
    console.log(event);
    // console.log(this.uploader.queue)
  //  this.fileVar=this.uploader.queue[0].file.rawFile;
  //  console.log(this.fileVar)
this.fileVar=[]
   for (let item of this.uploader.queue){
     this.fileVar.push(item.file.rawFile);
   }
}
remove(item){
  item.remove();
  this.fileVar=[]
  for (let item of this.uploader.queue){
    this.fileVar.push(item.file.rawFile);
  }
}
removeAll(){
  this.uploader.clearQueue()
  this.fileVar=[]
}

uploadAll(){
  console.log(this.fileVar)
  let post=new FormData();
  post.append('id',this.id);
  for(let i=0;i<this.fileVar.length;i++){
    post.append('document',this.fileVar[i],this.fileVar[i]['name']);
  }

//   this.activityOutputService.uploadDocs(post).subscribe(
//     res => {
//       if(res['message']){
//         let messageDiv: any = document.getElementById("message");
//         messageDiv.innerText = res["message"];
//         this.msgNotification.open();
//         this.removeAll();
//       }
//       else{
   
//         let messageDiv: any = document.getElementById("error");
//         messageDiv.innerText = res["error"]["message"];
//         this.errNotification.open();
//       }
      
//     },
//     error => {
//         console.info(error);
     
//     }
// );
this.http.request(this.activityOutputService.uploadDocs(post)).subscribe(event => {
  // Via this API, you get access to the raw event stream.
  // Look for upload progress events.
  if (event.type === HttpEventType.UploadProgress) {
    // This is an upload progress event. Compute and show the % done:
     this.percentDone = Math.round(100 * event.loaded / event.total);
     this.swidth=this.percentDone.toString();
    console.log(`File is ${this.percentDone}% uploaded.`);
  } else if (event instanceof HttpResponse) {
    if(event.body['error']){
      let messageDiv: any = document.getElementById("error");
      messageDiv.innerText = event.body['error'].message;
      this.errNotification.open();
    }
    else{
    let messageDiv: any = document.getElementById("message");
        messageDiv.innerText = 'Success';
        this.msgNotification.open();
        this.removeAll();
     
    }
    this.swidth='0'
    console.log(event);
     
  }
});

}
close(){
  this.router.navigate(["/report/sarbajanik-faram", { activityId:this.activityId}]);
}
}
