import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SarbajanikFaramComponent } from './sarbajanik-faram.component';
import { FormComponent } from './form/form.component';
import { UploadComponent } from './upload/upload.component';
import { ViewComponent } from './view/view.component';
const routes: Routes = [
  {
    path: '',
    component: SarbajanikFaramComponent,
  },
  {
    path: 'form',
    component: FormComponent,
  },
  {
    path: 'upload',
    component: UploadComponent,
  },
  {
    path: 'view',
    component: ViewComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SarbajanikFaramRoutingModule { }
