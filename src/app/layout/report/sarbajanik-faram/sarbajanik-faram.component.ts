import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router,ActivatedRoute, RouterState, RouterStateSnapshot, ActivatedRouteSnapshot } from "@angular/router";
import { DateConverterService, ActivityOutputService, ToggleSidebarService,ActivityFocusAreaMasterService, PlanningYearService, InclusionGroupService, CurrentUserService, UnicodeTranslateService } from "../../../shared";
import { jqxNotificationComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxnotification";
import { jqxLoaderComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxloader";
import { jqxGridComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxgrid";
import { TranslateService } from "@ngx-translate/core";
import { jqxWindowComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxwindow";
import { DataService } from "../../../shared/data-service";


@Component({
  selector: 'app-sarbajanik-faram',
  templateUrl: './sarbajanik-faram.component.html',
  styleUrls: ['./sarbajanik-faram.component.scss']
})
export class SarbajanikFaramComponent implements OnInit {
  sarbajanikForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  transData: any;
  investigationStageAdapter: Array<any> = [];
  activityAdapter:any= [];
  activityFocusAreaAdapter: any = [];
  activityCategoryAdapter: any = [];
  userData: any = {};
  SearchDatas: Array<any> = [];
  API_URL_DOC: any;
  API_URL: any;
  columngroups: any[];
  userType: any;
  message: string;
  lang: any;
  phaseId='';
  activityId='';
  activityDatas:any;
  check:boolean=true;

  @ViewChild("msgNotification")
  msgNotification: jqxNotificationComponent;
  @ViewChild("errNotification")
  errNotification: jqxNotificationComponent;
  @ViewChild("jqxLoader")
  jqxLoader: jqxLoaderComponent;
  @ViewChild("jqxEditLoader")
  jqxEditLoader: jqxLoaderComponent;
  @ViewChild("myGrid")
  myGrid: jqxGridComponent;

  constructor(
      private fb: FormBuilder,
      private cus: CurrentUserService,
      private dfs: DateConverterService,
      @Inject("API_URL_DOC") API_URL_DOC: string,
      @Inject("API_URL") API_URL: string,
      private cdr: ChangeDetectorRef,
      private router: Router,
      private activityOutputService: ActivityOutputService,
      private translate: TranslateService,
      private unicode: UnicodeTranslateService,
      private ts:ToggleSidebarService,
      private activeRoute: ActivatedRoute,
  ) {
      this.createForm();
      this.getTranslation();
      this.userData = this.cus.getTokenData();
      this.API_URL_DOC = API_URL_DOC;
      this.API_URL = API_URL;
  }

  ngOnInit() {

    this.ts.currentMessage.subscribe((data) => {
        if(data=="on"){
        this.check=false;
        }
        if(data=="off")
        this.check=true;

      });
      this.lang = this.translate.currentLang.substr(0,2);
      this.getTranslation();
      this.activityOutputService.getInvestigationStage().subscribe((res) => {
        this.investigationStageAdapter = res && res['Record'];
      }, (error) => {
        console.info(error);
      });
      this.activityOutputService.getActivity().subscribe((res) => {
        this.activityAdapter = res;
      }, (error) => {
        console.info(error);
      });

  }
  getPhase(phase){
      console.log(phase);
      this.investigationStageAdapter.map((data)=>{
          if(phase===data['name']){
              console.log(data['id'])
            this.phaseId=data['id'].toString();
          }

      })
  }
  getTranslation() {
      this.translate
          .get(["SN", "regNo", "STATUS", "चरण", 'Approved', 'Approve', 'APPROVE', "ADD", "EDIT", "NAME", "nepali", "english", "mobile", "VIEW", "DETAIL", "ACTION", "UPDATE", "DELETESELECTED", "RELOAD", "ACTIVITY_NAME", "ACTIVITY_FOCUS_AREA", "ACTIVITY_CATEGORY", "ACTIVITY_FOR", "TECHNICAL_APPROVAL", "viewEdit"]).subscribe((translation: [string]) => {
              this.transData = translation;
              this.loadGrid();
          });
  }


  loadGridData(id) {


    //   let post = {};
      this.jqxLoader.open();
    //   post = this.sarbajanikForm.value;

      this.activityId=id;

      this.activityOutputService.getPublicExaminationList(id).subscribe((res) => {
              if ( res[0].error) {
                  this.source.localdata = [];

              } else {
                  this.source.localdata = res;

              }
              this.myGrid.updatebounddata();
              this.jqxLoader.close();
          },
          error => {
              console.info(error);
              this.jqxLoader.close();
          }
      );

      this.myGrid.updatebounddata();
      this.jqxLoader.close();

  }

  createForm() {
      this.sarbajanikForm = this.fb.group({
          activityId: ["", Validators.required],
          phase: ["", Validators.required],
      });
  }

  ngAfterViewInit() {
    //   this.loadGridData();
    let data = this.activeRoute.snapshot.params;
    if(data['activityId']){
        this.sarbajanikForm.get('activityId').patchValue(data['activityId'])
     this.loadGridData(data['activityId']);
    }
      this.unicode.initUnicode();
      this.cdr.detectChanges();
      this.activityDatas=JSON.parse(localStorage.getItem('ActivityDatas'));
      if(this.activityDatas){
        this.sarbajanikForm.get('activityId').patchValue(this.activityDatas['id'])
       this.loadGridData(this.activityDatas['id'])
       }

  }


  loadGrid() {
      this.source = {
          datatype: "json",
          datafields: [
              { name: "id", type: "string" },
              { name: "phase", type: "string" },
              { name: "activity", type: "string" },

          ],
          id: "id",
          localdata: [],
          pagesize: 100
      };

      this.dataAdapter = new jqx.dataAdapter(this.source);
      this.columns = [
          {
              text: this.transData["SN"], sortable: false, filterable: false, editable: false, groupable: false, draggable: false, resizable: false, datafield: "id", columntype: "number", width: 50,
              cellsrenderer: function (row, column, value) {
                  return "<div style='margin:4px;'>" + (value + 1) + "</div>";
              }
          },
          { text: this.transData['ACTIVITY_NAME'], datafield: "activity", columntype: "textbox", editable: false },
          { text: "चरण", datafield: "phase", width: 100, columntype: "textbox", editable: false },
          {
              text: 'फारम', datafield: "taStatus", columngroup: "action", sortable: false, filterable: false, width: 110, columntype: "button",
              cellsrenderer: (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
                  return 'प्रिन्ट गर्नुहोस'
              },
              buttonclick: (row: number): void => {
                this.editrow = row;
                let dataRecord = this.myGrid.getrowdata(this.editrow);
                this.router.navigate(["/report/sarbajanik-faram/form", { activityId:this.activityId,id: dataRecord["id"], activityName: dataRecord["activity"],phase:dataRecord["phase"] }]);
              }

          },
          {
              text: 'फाईल', datafield: "View", columngroup: "action", sortable: false, filterable: false, width: 90, columntype: "button",
              cellsrenderer: (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
                  return 'अप्लोड गर्नुहोस';
              },
              buttonclick: (row: number): void => {
                this.editrow = row;
                let dataRecord = this.myGrid.getrowdata(this.editrow);
                this.router.navigate(["/report/sarbajanik-faram/upload", { activityId:this.activityId,id: dataRecord["id"], activityName: dataRecord["activity"],phase:dataRecord["phase"] }]);
              }
          },
          {
              text: 'फाईल', datafield: "ViewData", columngroup: "action", sortable: false, filterable: false, width: 60, columntype: "button",
              cellsrenderer: (): string => {
                  return 'हेर्नुहोस';
              },
              buttonclick: (row: number): void => {
                this.editrow = row;
                let dataRecord = this.myGrid.getrowdata(this.editrow);
                this.getPhase(dataRecord["phase"]);

                this.router.navigate(["/report/sarbajanik-faram/view", { id:this.activityId, dataId:dataRecord["id"],activityName: dataRecord["activity"],phase:this.phaseId }]);
              }
          }
      ];
      this.columngroups = [
          { text: this.transData['ACTION'], align: "center", name: "action" }
      ];
  }

  SaveData(post) {
      this.jqxLoader.open();
      this.activityOutputService.saveActivity(post).subscribe(
          res => {
              if(res['message']){
                let messageDiv: any = document.getElementById("message");
                messageDiv.innerText = res["message"];
                this.msgNotification.open();
            this.jqxLoader.close();
            this.loadGridData(this.sarbajanikForm.get('activityId').value)
              }
              else{
                this.jqxLoader.close();
                let messageDiv: any = document.getElementById("error");
                messageDiv.innerText = res["error"]["message"];
                this.errNotification.open();
              }
          },
          error => {
              console.info(error);
              this.jqxLoader.close();
          }
      );
  }

}
