import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SarbajanikFaramComponent } from './sarbajanik-faram.component';
import { SarbajanikFaramRoutingModule } from './sarbajanik-faram-routing.module';
import { SharedModule } from '../../../shared/modules/shared.module';
import { FormComponent } from './form/form.component';
import { UploadComponent } from './upload/upload.component';
import { ViewComponent } from './view/view.component';

@NgModule({
  imports: [
    CommonModule,
    SarbajanikFaramRoutingModule,
    SharedModule
  ],
  declarations: [SarbajanikFaramComponent, FormComponent, UploadComponent, ViewComponent],
  schemas : [NO_ERRORS_SCHEMA]
})
export class SarbajanikFaramModule { }
