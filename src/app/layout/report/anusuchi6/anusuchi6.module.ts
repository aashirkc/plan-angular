import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Anusuchi6Component } from './anusuchi6.component';
import { SharedModule } from 'app/shared/modules/shared.module';
import { Anusuchi6RoutingModule } from './anusuchi6-routing.module';
import { AnusuchiSearchBarModule } from '../anusuchi-search-bar/anusuchi-search-bar.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AnusuchiSearchBarModule,
    Anusuchi6RoutingModule
  ],
  declarations: [Anusuchi6Component],
  schemas: [NO_ERRORS_SCHEMA]
})
export class Anusuchi6Module { }
