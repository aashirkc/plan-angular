import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Anusuchi6Component } from './anusuchi6.component';

const routes: Routes = [
    {
        path: '',
        component: Anusuchi6Component,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class Anusuchi6RoutingModule { }
