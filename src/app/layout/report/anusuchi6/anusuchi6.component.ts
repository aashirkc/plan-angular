import { Component,ViewChild, OnInit } from '@angular/core';
import { PlanningYearService, UnicodeTranslateService,AgencyTypeService,AdministrativeApprovalService, NepaliTextareaComponent, AllReportService,KataSanchalanService, DateConverterService,NepaliInputComponent,ToggleSidebarService, ProgressReportingTippaniAadeshService } from '../../../shared';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-anusuchi6',
  templateUrl: './anusuchi6.component.html',
  styleUrls: ['./anusuchi6.component.scss']
})
export class Anusuchi6Component implements OnInit {

  organizationMasterData:any;
  yearAdapter: Array<any> = [];
  anusuchi6: FormGroup;
  SearchDatas:any;
  dataLists:any;
  anusuchiReport: FormGroup;
  activityAdapter: Array<any> = [];
  activityDatas:any;
  check:boolean=true;
  childType: any = [];
  agencyName: any;
  agencyType: any;
  lang: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  @ViewChild('act') act: jqxComboBoxComponent;

  @ViewChild('nepin1') nepin1: NepaliInputComponent;
  @ViewChild('nepin2') nepin2: NepaliInputComponent;
  @ViewChild('nepin3') nepin3: NepaliInputComponent;
  @ViewChild('nepin4') nepin4: NepaliInputComponent;
  @ViewChild('nepin5') nepin5: NepaliInputComponent;
  @ViewChild('nepin6') nepin6: NepaliInputComponent;
  @ViewChild('nepin7') nepin7: NepaliInputComponent;
  @ViewChild('nepin8') nepin8: NepaliTextareaComponent;
  @ViewChild('nepin9') nepin9: NepaliTextareaComponent;

  constructor(
    private fb: FormBuilder,
    private pys: PlanningYearService,
    private rs: AllReportService,
    private ks: KataSanchalanService,
    private aas: AdministrativeApprovalService,
    private unicode: UnicodeTranslateService,
    private ts:ToggleSidebarService,
    private ats:AgencyTypeService,
    private prtas: ProgressReportingTippaniAadeshService
  ) {
    this.createForm();
  }

   // --- search bar emitter functions
   search = ({value, id}) => this.SearchData(value, id)

   changeLoadingStatus($event){
     !$event && this.jqxLoader.close();
     $event && this.jqxLoader.open();
   }

   showError($event){
     let messageDiv: any = document.getElementById('error');
     messageDiv.innerText = $event;
     this.errNotification.open();
   }
   // when sidebar is opened
  searchWithActivityDatas = () => {
    try{
        const activityDatas = JSON.parse(localStorage.getItem("ActivityDatas"));
        if(activityDatas){
            const requestParams = {
                planYear: activityDatas.planYear,
                activity: activityDatas.id,
                activityType: 3
            }
            this.prtas.getOrganizationName().subscribe((res) => {
                this.organizationMasterData = res;
                this.search({ value: requestParams, id: activityDatas.id });
            });
        }
    }catch(e){
        debugger;
        console.warn(e);
    }
}
   // --- end

  createForm() {
    this.anusuchi6 = this.fb.group({
      'planYear': ['', Validators.required],
      'child': [''],
      'parent': ['']
    });
    this.anusuchiReport=this.fb.group({
      'activity': [''],
      'kharcha1':[''],
      'kharcha2':[''],
      'kharcha3':[''],
      'kharcha4':[''],
      'kharcha5':[''],
      'kharcha6':[''],
      'kharcha7':[''],
      'kharcha8':[''],
      'kharcha9':[''],
      'kharcha10':[''],
      'kharcha11':[''],
      'kharcha12':[''],
      'uskharcha':[''],
      'samasya1':[''],
      'samasya2':[''],
      'samasya3':[''],
      'upaaya1':[''],
      'upaaya2':[''],
      'upaaya3':[''],
      'upaaya4':[''],
      'sujhaab':[''],
      'maag':[''],
      'mainkharcha':[''],
      'anudaan':[''],
      'chandaa':[''],
      'janasahabhagita':[''],
      'totalLaagat':[''],
      'tillKharca':[''],
      'fromProgram':[''],
    });
  }
  // ngAfterViewInit() {
  //   this.unicode.initUnicode();
  // }
ngOnInit(){
  this.ats.show(0).subscribe(
    result => {
      if (result['length'] > 0) {

        this.agencyType = result;
      }
    },
    error => {
      console.log(error);
    }
  );
  this.ts.currentMessage.subscribe((data) => {
    if (data == "on"){
      this.check = false;
      // local storage gets set when sidebar is opened taking current activity
      this.searchWithActivityDatas();
    }
    if (data == "off"){
        this.check = true;
    }
    console.log(this.check);
});
  this.rs.getOrg().subscribe((res) => {
    this.organizationMasterData = res;
  }, (error) => {
    console.info(error);

  });
  this.activityDatas=JSON.parse(localStorage.getItem('ActivityDatas'));


  this.pys.index({}).subscribe((res) => {
    if (res.length == 1 && res[0].error) {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = res[0].error;
      this.errNotification.open();
      this.yearAdapter = [];
    } else {
      this.yearAdapter = res;
      if(this.activityDatas){
        this.anusuchi6.get('planYear').patchValue(this.activityDatas['planYear']);
        this.ChangePlanYear1(this.activityDatas['planYear']);
       }else{
      this.anusuchi6.get('planYear').patchValue(res[0]['yearCode']);
      this.ChangePlanYear(res[0]['yearCode']);
       }
    }
    this.jqxLoader.close();
  }, (error) => {
    console.info(error);
    this.jqxLoader.close();
  });
}

ChangePlanYear1(id){
  console.log(id);
  this.ks.indexActivity({planYear:id, activityType: 3 }).subscribe((res) => {
    if (res.length == 1 && res[0].error) {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = res[0].error;
      this.errNotification.open();
      this.activityAdapter = [];
    } else {
      this.activityAdapter = res;
      console.log(this.activityDatas['id']);
      setTimeout(() => {
        if(this.check){
          this.act.selectItem(this.activityDatas['id']);
          }
        // this.act.selectItem(this.activityDatas['id']);
        this.SearchData(this.anusuchi6.value,this.activityDatas['id']);
      }, 100);

    }
    this.jqxLoader.close();
  }, (error) => {
    console.info(error);
    this.jqxLoader.close();
  });

}

clearNepin(){
  this.nepin1.clearInput();
  this.nepin2.clearInput();
  this.nepin3.clearInput();
  this.nepin4.clearInput();
  this.nepin5.clearInput();
  this.nepin6.clearInput();
  this.nepin7.clearInput();
  this.nepin8.clearInput();
  this.nepin9.clearInput();
}
SearchData(post,val) {
  post['activity'] = val;
  // post['activity'] = this.act.val();
  this.jqxLoader.open();
  if (val) {
    this.rs.getanusuchi6Report(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.SearchDatas = [];
      } else {
        this.SearchDatas = res;
        console.log(this.SearchDatas)

        this.anusuchiReport.controls['tillKharca'].patchValue(this.SearchDatas[0]['totalExpenditure']);

        this.anusuchiReport.reset();
        this.clearNepin();
      let id=val;
      this.aas.getApprovalByid(id).subscribe((result: any) => {
        this.dataLists = result;
        // this.anusuchiReport.controls['anudaan'].patchValue(res['anudaan']);
        this.anusuchiReport.controls['anudaan'].patchValue(this.dataLists['proposedCost'])
        this.anusuchiReport.controls['chandaa'].patchValue(this.dataLists['RemainingPlan'] && this.dataLists['RemainingPlan'][0] && this.dataLists['RemainingPlan'][0].grantedFromOthers || '');
        this.anusuchiReport.controls['janasahabhagita'].patchValue(this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromConsumerCommitte || '');
        this.anusuchiReport.controls['fromProgram'].patchValue(this.dataLists['PlanAgreement'] && this.dataLists['PlanAgreement'][0] && this.dataLists['PlanAgreement'][0].grantedFromLocalLevel);

      });
      setTimeout(()=>{
        this.rs.getanusuchi6ReportData(id).subscribe((result) => {
            console.log(result);
            if(result['error']){
            //   this.anusuchiReport.reset();
            //   this.clearNepin();
            } else {
              this.anusuchiReport.reset();
              this.clearNepin();
              let a=JSON.parse(result['data'])
              let res=a['data']
              console.log('saved data')
              this.anusuchiReport.controls['kharcha1'].patchValue(res['kharcha1']);
              this.anusuchiReport.controls['kharcha2'].patchValue(res['kharcha2']);
              this.anusuchiReport.controls['kharcha3'].patchValue(res['kharcha3']);
              this.anusuchiReport.controls['kharcha4'].patchValue(res['kharcha4']);
              this.anusuchiReport.controls['kharcha5'].patchValue(res['kharcha5']);
              this.anusuchiReport.controls['kharcha6'].patchValue(res['kharcha6']);
              this.anusuchiReport.controls['kharcha7'].patchValue(res['kharcha7']);
              this.anusuchiReport.controls['kharcha8'].patchValue(res['kharcha8']);
              this.anusuchiReport.controls['kharcha9'].patchValue(res['kharcha9']);
              this.anusuchiReport.controls['kharcha10'].patchValue(res['kharcha10']);
              this.anusuchiReport.controls['kharcha11'].patchValue(res['kharcha11']);
              this.anusuchiReport.controls['kharcha12'].patchValue(res['kharcha12']);
              this.anusuchiReport.controls['uskharcha'].patchValue(res['uskharcha']);
              this.anusuchiReport.controls['samasya1'].patchValue(res['samasya1']);
              this.anusuchiReport.controls['samasya2'].patchValue(res['samasya2']);
              this.anusuchiReport.controls['samasya3'].patchValue(res['samasya3']);
              this.anusuchiReport.controls['upaaya1'].patchValue(res['upaaya1']);
              this.anusuchiReport.controls['upaaya2'].patchValue(res['upaaya2']);
              this.anusuchiReport.controls['upaaya3'].patchValue(res['upaaya3']);
              this.anusuchiReport.controls['upaaya4'].patchValue(res['upaaya4']);
              this.anusuchiReport.controls['sujhaab'].patchValue(res['sujhaab']);
              this.anusuchiReport.controls['maag'].patchValue(res['maag']);
              this.anusuchiReport.controls['mainkharcha'].patchValue(res['mainkharcha']);
              this.anusuchiReport.controls['anudaan'].patchValue(res['anudaan']);
              this.anusuchiReport.controls['tillKharca'].patchValue(res['tillKharca']);
              this.anusuchiReport.controls['chandaa'].patchValue(res['chandaa']);
              this.anusuchiReport.controls['janasahabhagita'].patchValue(res['janasahabhagita']);
              this.anusuchiReport.controls['totalLaagat'].patchValue(res['totalLaagat']);
              this.anusuchiReport.controls['fromProgram'].patchValue(res['fromProgram']);
            }
            this.anusuchiReport.controls['activity'].patchValue(id);
          });
      },500)

      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
  else {
    this.jqxLoader.close();
    let messageDiv: any = document.getElementById('error');
    messageDiv.innerText = 'कृपया याेजना छान्नुहाेस्';
    this.errNotification.open();
  }
}
ChangePlanYear(id){
  this.ks.indexActivity({planYear:id, activityType: 3}).subscribe((res) => {
    if (res.length == 1 && res[0].error) {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = res[0].error;
      this.errNotification.open();
      this.activityAdapter = [];
    } else {
      this.activityAdapter = res;
    }
    this.jqxLoader.close();
  }, (error) => {
    console.info(error);
    this.jqxLoader.close();
  });

}
  /**
* Export Report Data to Excel
*/
  exportReport(): void {

    let htmltable = document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    let fhtml = `
    <html>
      <head>
        <style>
        .borderTable{
          border: 1px solid #757879;
        }
        .hideDiv{
          display: none !important;
      }
        @font-face {
          font-family: 'preeti';
          src: url('assets/css/Preeti.TTF');
          font-weight: normal;
          font-style: normal;
      }
         .preeti{
          font-family:'preeti' !important;
          font-size:18px !important;
         }
        </style>
      </head>
  <body onload="window.print();window.close()">${html}</body>
    </html>`
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(fhtml));

  }

  /**
   * Open Print Window to print report
   */
  printReport(): void {
    console.log(this.anusuchiReport.value);
    let post=this.anusuchiReport.value;
    // if(this.check)
    // post['activity'] = this.act.val();
    // else
    // post['activity'] =this.activityDatas['id'];
    // console.log(this.act.val());
    this.rs.sendanusuchi6Report(post).subscribe((res) => {
      console.log(res);
      if(res['message']==='success'){

      }
    });

    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .table-container table th{
              text-align: center;
          }

          .table-container table .tleft{
              text-align: left !important;
          }

          .table-container table .tright{
              text-align: right !important;
          }
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .hideDiv{
            display: none !important;
        }
          .table {
            position:relative;
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          @font-face {
            font-family: 'nepaliNormal';
            src: url('assets/css/Kantipur.TTF');
            font-weight: normal;
            font-style: normal;
        }
        .nepaliText{
          font-family:'nepaliNormal';
          font-size:18px!important;
      }
      #printShow{
        display: inline-block !important;
    }
    #printHide{
        display: none !important;
      }
      .print_show{
        display: inline-block !important;
    }
    .p-control{
      display: none !important;
    }
    .print_hide{
        display: none !important;
      }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close();">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
  implementingAgencyChange(selectedEvent) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.agencyName = displayText;
      let agencyCode = selectedEvent.target.value;
      this.getAgencyType(agencyCode);
    } else {
      this.childType = '';
    }
  }


  getAgencyType(agencyCode = 0, index = 0, type = null) {
    // remove previously added form control if the parent select is changed.
    this.ats.show(agencyCode).subscribe(
      result => {
        if (result['length'] > 0) {
          this.childType = result;
        } else {
          this.childType = '';
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
