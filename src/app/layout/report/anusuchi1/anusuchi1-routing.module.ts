import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Anusuchi1Component } from './anusuchi1.component';

const routes: Routes = [
    {
        path: '',
        component: Anusuchi1Component
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class Anusuchi1RoutingModule {}
