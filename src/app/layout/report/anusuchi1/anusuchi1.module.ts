import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Anusuchi1Component } from './anusuchi1.component';
import { SharedModule } from 'app/shared/modules/shared.module';
import { Anusuchi1RoutingModule } from './anusuchi1-routing.module';
import { AnusuchiSearchBarModule } from '../anusuchi-search-bar/anusuchi-search-bar.module';

@NgModule({
    imports: [CommonModule, SharedModule, Anusuchi1RoutingModule, AnusuchiSearchBarModule],
    declarations: [Anusuchi1Component]
})
export class Anusuchi1Module {}
