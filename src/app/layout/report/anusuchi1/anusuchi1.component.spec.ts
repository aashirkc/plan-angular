import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Anusuchi1Component } from './anusuchi1.component';

describe('Anusuchi1Component', () => {
  let component: Anusuchi1Component;
  let fixture: ComponentFixture<Anusuchi1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Anusuchi1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Anusuchi1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
