import { Component, OnInit, ViewChild } from '@angular/core';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { UnicodeTranslateService, AllReportService } from '../../../shared';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { NgStyle } from '@angular/common';

@Component({
    selector: 'app-anusuchi1',
    templateUrl: './anusuchi1.component.html',
    styleUrls: ['./anusuchi1.component.scss']
})
export class Anusuchi1Component implements OnInit {
    check: boolean = true;
    anusuchi1Report: FormGroup;
    empty21Array = [...new Array(21)];

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

    constructor(
        private unicode: UnicodeTranslateService,
        private fb: FormBuilder,
        private all: AllReportService,

    ) { }

    ngOnInit() {
        this.createForm();
        // this.all.getAnusuchi1().subscribe(d=>{
        //     console.log(d)
        // })
    }

    search = ({ value, id }) => this.SearchData(value, id)

    changeLoadingStatus($event) {
        !$event && this.jqxLoader.close();
        $event && this.jqxLoader.open();
    }

    showError($event) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = $event;
        this.errNotification.open();
    }

    // ngAfterViewInit() {
    //     this.unicode.initUnicode();
    // }
    startingData: any
    activityId: any
    anusuchi1: any

    SearchData(value, id) {
        this.activityId = id
        let p = {
            ...value,
            activity: id
        }
        this.all.getAnusuchi1(p).subscribe(res => {
            this.startingData = res[0]

        })
        let q = {
            id: id
        }
        this.all.getAllAnusuchi1(q).subscribe(res => {
            let a = JSON.parse(res['anusuch1'])
            this.anusuchi1 = a
            this.setData(this.anusuchi1);
        })

    }

    setData(post) {
        this.anusuchi1Report.get('programNam').patchValue(post['programNam'])
        this.anusuchi1Report.get('unit').patchValue(post['unit'])
        this.anusuchi1Report.get('totalQty').patchValue(post['totalQty'])
        this.anusuchi1Report.get('extLoan').patchValue(post['extLoan'])
        this.anusuchi1Report.get('anudan').patchValue(post['anudan'])
        this.anusuchi1Report.get('aayojanaNepalGov').patchValue(post['aayojanaNepalGov'])
        this.anusuchi1Report.get('aayojanaOrg').patchValue(post['aayojanaOrg'])
        this.anusuchi1Report.get('aayojanaPeople').patchValue(post['aayojanaPeople'])
        this.anusuchi1Report.get('aayojanaLoan').patchValue(post['aayojanaLoan'])
        this.anusuchi1Report.get('aayojanaAnudan').patchValue(post['aayojanaAnudan'])
        this.anusuchi1Report.get('currentNepalGov').patchValue(post['currentNepalGov'])
        this.anusuchi1Report.get('currentOrg').patchValue(post['currentOrg'])
        this.anusuchi1Report.get('currentPeople').patchValue(post['currentPeople'])
        this.anusuchi1Report.get('currentLoan').patchValue(post['currentLoan'])
        this.anusuchi1Report.get('currentAnudan').patchValue(post['currentAnudan'])
        this.anusuchi1Report.get('dathiSanstha').patchValue(post['dathiSanstha'])
        this.anusuchi1Report.get('district').patchValue(post['district'])
        this.anusuchi1Report.get('yearlyBudget').patchValue(post['yearlyBudget'])
        this.anusuchi1Report.get('nepalGov').patchValue(post['nepalGov'])
        this.anusuchi1Report.get('intOrg').patchValue(post['intOrg'])
        this.anusuchi1Report.get('intPeople').patchValue(post['intPeople'])
        this.anusuchi1Report.get('programHeadName').patchValue(post['programHeadName'])
        this.anusuchi1Report.get('districtDivision').patchValue(post['districtDivision'])
        this.anusuchi1Report.get('programCompletionDate').patchValue(post['programCompletionDate'])
        this.anusuchi1Report.get('programName').patchValue(post['programName'])
        this.anusuchi1Report.get('division').patchValue(post['division'])
        this.anusuchi1Report.get('ministry').patchValue(post['ministry'])
        this.anusuchi1Report.get('totalInvest').patchValue(post['totalInvest'])
        this.anusuchi1Report.get('totalWeight').patchValue(post['totalWeight'])
        this.anusuchi1Report.get('finishedQty').patchValue(post['finishedQty'])
        this.anusuchi1Report.get('progress').patchValue(post['progress'])
        this.anusuchi1Report.get('yrlyIndex').patchValue(post['yrlyIndex'])
        this.anusuchi1Report.get('yrlyWeight').patchValue(post['yrlyWeight'])
        this.anusuchi1Report.get('yrlyBudget').patchValue(post['yrlyBudget'])
        this.anusuchi1Report.get('sndIndex').patchValue(post['sndIndex'])
        this.anusuchi1Report.get('sndWeight').patchValue(post['sndWeight'])
        this.anusuchi1Report.get('sndBudget').patchValue(post['sndBudget'])
        this.anusuchi1Report.get('trdIndex').patchValue(post['trdIndex'])
        this.anusuchi1Report.get('trdWeight').patchValue(post['trdWeight'])
        this.anusuchi1Report.get('trdBudget').patchValue(post['trdBudget'])


    }

    createForm() {
        this.anusuchi1Report = this.fb.group({
            extLoan: [''],
            anudan: [''],
            aayojanaNepalGov: [''],
            aayojanaOrg: [''],
            aayojanaPeople: [''],
            aayojanaLoan: [''],
            aayojanaAnudan: [''],
            currentNepalGov: [''],
            currentOrg: [''],
            currentPeople: [''],
            currentLoan: [''],
            currentAnudan: [''],
            dathiSanstha: [''],
            district: [''],
            yearlyBudget: [''],
            nepalGov: [''],
            intOrg: [''],
            intPeople: [''],
            programHeadName: [''],
            districtDivision: [''],
            programCompletionDate: [''],
            programName: [''],
            division: [''],
            ministry: [''],
            tableData: this.fb.array([
                this.initTableData(),
            ]),
        })
        console.log(this.anusuchi1Report)
    }

    /**
    * Export Report Data to Excel
    */
    exportReport(): void {
        let htmltable = document.getElementById('reportContainer');
        let html = htmltable.outerHTML;
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
    }

    initTableData() {
        return this.fb.group({
            programNam: [''],
            unit: [''],
            totalQty: [''],
            totalInvest: [''],
            totalWeight: [''],
            finishedQty: [''],
            progress: [''],
            yrlyIndex: [''],
            yrlyWeight: [''],
            yrlyBudget: [''],
            fstIndex: [''],
            fstWeight: [''],
            fstBudget: [''],
            sndIndex: [''],
            sndWeight: [''],
            sndBudget: [''],
            trdIndex: [''],
            trdWeight: [''],
            trdBudget: [''],
        });
    }

    addRow() {
        const control = <FormArray>this.anusuchi1Report.controls['tableData'];
        control.push(this.initTableData());
    }

    deleteRow(i) {
        const control = <FormArray>this.anusuchi1Report.controls['tableData'];
        control.removeAt(i);
    }

    /**
     * Open Print Window to print report
     */
    printReport(): void {

        let post = this.anusuchi1Report.value;
        let p = {
            anusuchi1: post
        }
        this.all.postAnusuchi1(this.activityId, post).subscribe(res => {
            console.log(res)
        })
        let printContents, popupWin;
        printContents = document.getElementById('reportContainer').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          table.invoice-table {
            border-collapse: collapse;
            width: 100%;
        }

        table.invoice-table, .invoice-table th, .invoice-table td {
            border: 1px solid black;
            padding: 2px 4px;
        }

        .invoice-table th {
            font-size: 12px;
        }

          .p{
            margin-bottom: 5px;
          }


          @font-face {
            font-family: 'nepaliNormal';
            src: url('assets/css/Kantipur.TTF');
            font-weight: normal;
            font-style: normal;
        }
        .nepaliText{
          font-family:'nepaliNormal';
          font-size:18px!important;
      }

      .invoice-footer-info {
        font-size: 12px;
        margin-top: 15px;
        border-bottom: 1px solid #000;
        padding-bottom: 10px;
        line-height: 24px;
    }

    .invoice-footer{
        display: block;
        padding-top: 30px;
        margin-bottom: 15px;
    }

    .invoice-footer .head{
        display: inline-block;
        width: 32%;
        float: left;
    }

    .invoice-footer-field{

    }
    .px-control1{
        display: none
    }
    .px-control{
        display: none
    }

    .invoice-footer-signature div:nth-child(2){
        margin-top: 4px
    }

      .print_show{
        display: inline-block !important;
    }

    .print_hide{
        display: none !important;
      }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
    }
}
