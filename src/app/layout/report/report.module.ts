import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportComponent } from './report.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { ReportRoutingModule } from './report-routing.module';
import { AnusuchiSearchBarComponent } from './anusuchi-search-bar/anusuchi-search-bar.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReportRoutingModule
  ],
  declarations: [ReportComponent],
  schemas:[NO_ERRORS_SCHEMA]
})
export class ReportModule { }
