import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Anusuchi3Component } from './anusuchi3.component';
import { SharedModule } from 'app/shared/modules/shared.module';
import { Anusuchi3RoutingModule } from './anusuchi3-routing.module';
import { AnusuchiSearchBarModule } from '../anusuchi-search-bar/anusuchi-search-bar.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AnusuchiSearchBarModule,
    Anusuchi3RoutingModule
  ],
  declarations: [Anusuchi3Component],
//   schemas: [NO_ERRORS_SCHEMA]
})
export class Anusuchi3Module { }
