import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Anusuchi3Component } from './anusuchi3.component';

const routes: Routes = [
    {
        path: '',
        component: Anusuchi3Component,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class Anusuchi3RoutingModule { }
