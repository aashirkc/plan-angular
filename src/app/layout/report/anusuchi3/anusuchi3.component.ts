import { Component, ViewChild, QueryList, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { PlanningYearService, UnicodeTranslateService, NepaliInputComponent, ToggleSidebarService, AllReportService, CustomValidators, KataSanchalanService, DateConverterService, AgencyTypeService, ProgressReportingTippaniAadeshService } from '../../../shared';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-anusuchi3',
  templateUrl: './anusuchi3.component.html',
  styleUrls: ['./anusuchi3.component.scss']
})
export class Anusuchi3Component implements OnInit {

  organizationMasterData: any;
  yearAdapter: Array<any> = [];
  anusuchi3: FormGroup;
  anusuchiReport: FormGroup;
  SearchDatas: any;
  amdaaniArray: any;
  kharchaArray: any;
  maujdaatArray: any;
  bhuktaaniArray: any;
  pragatiArray: any;
  activityAdapter: Array<any> = [];
  activityDatas: any;
  check: boolean = true;
  childType: any = [];
  agencyName: any;
  agencyType: any;
  lang: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  @ViewChild('act') act: jqxComboBoxComponent;

  @ViewChild('nepin1') nepin1: NepaliInputComponent;
  @ViewChild('nepin2') nepin2: NepaliInputComponent;
  @ViewChild('nepin3') nepin3: NepaliInputComponent;
  @ViewChild('nepin4') nepin4: NepaliInputComponent;
  @ViewChild('nepin5') nepin5: NepaliInputComponent;
  @ViewChild('nepin6') nepin6: NepaliInputComponent;
  @ViewChild('nepin7') nepin7: NepaliInputComponent;
  @ViewChild('nepin8') nepin8: NepaliInputComponent;
  @ViewChild('nepin9') nepin9: NepaliInputComponent;
  @ViewChild('nepin10') nepin10: NepaliInputComponent;
  @ViewChild('nepin11') nepin11: NepaliInputComponent;
  @ViewChild('nepin12') nepin12: NepaliInputComponent;
  @ViewChild('nepin13') nepin13: NepaliInputComponent;
  @ViewChild('nepin14') nepin14: NepaliInputComponent;
  @ViewChild('nepin15') nepin15: NepaliInputComponent;
  @ViewChild('nepin16') nepin16: NepaliInputComponent;
  @ViewChild('nepin17') nepin17: NepaliInputComponent;
  @ViewChild('nepin18') nepin18: NepaliInputComponent;
  @ViewChild('nepin19') nepin19: NepaliInputComponent;
  @ViewChild('nepin20') nepin20: NepaliInputComponent;
  @ViewChild('nepin21') nepin21: NepaliInputComponent;
  @ViewChild('nepin22') nepin22: NepaliInputComponent;
  @ViewChild('nepin23') nepin23: NepaliInputComponent;
  @ViewChild('nepin24') nepin24: NepaliInputComponent;


  constructor(
    private fb: FormBuilder,
    private pys: PlanningYearService,
    private rs: AllReportService,
    private ks: KataSanchalanService,
    private unicode: UnicodeTranslateService,
    private ts: ToggleSidebarService,
    private ats: AgencyTypeService,
    private translate: TranslateService,
    private prtas: ProgressReportingTippaniAadeshService
  ) {
    this.createForm();
    // this.addMore();
  }

  // --- search bar emitter functions
  search = ({ value, id }) => this.SearchData(value, id)

  changeLoadingStatus($event) {
    !$event && this.jqxLoader.close();
    $event && this.jqxLoader.open();
  }

  showError($event) {
    let messageDiv: any = document.getElementById('error');
    messageDiv.innerText = $event;
    this.errNotification.open();
  }

  // when sidebar is opened
  searchWithActivityDatas = () => {
    try {
      const activityDatas = JSON.parse(localStorage.getItem("ActivityDatas"));
      if (activityDatas) {
        const requestParams = {
          planYear: activityDatas.planYear,
          activity: activityDatas.id,
          activityType: 3
        }
        this.prtas.getOrganizationName().subscribe((res) => {
          this.organizationMasterData = res;
          this.search({ value: requestParams, id: activityDatas.id });
        });
      }
    } catch (e) {
      debugger;
      console.warn(e);
    }
  }
  // --- end

  createForm() {
    this.anusuchi3 = this.fb.group({
      'planYear': ['', Validators.required],
      'child': [''],
      'parent': [''],
    });
    // this.anusuchiReport=this.fb.group({
    //   aamdaani: this.fb.array([
    //     this.aamdaaniForm()
    //   ]),
    //   kharcha: this.fb.array([
    //     this.kharchaForm()
    //   ]),
    //   maujdaat: this.fb.array([
    //     this.maujdaatForm()
    //   ]),
    //   bhuktaani: this.fb.array([
    //     this.bhuktaaniForm()
    //   ]),
    //   pragati: this.fb.array([
    //     this.pragatiForm()
    //   ]),

    // })
    this.anusuchiReport = this.fb.group({
      'activity': [''],
      'aamdaani1': [''],
      'mahila': [''],
      'total': [''],
      'purus': [''],
      'aamdaani2': [''],
      'aamdaani3': [''],
      'aamdaani4': [''],
      'aamdaani5': [''],
      'aamdaani6': [''],
      'kharcha7': [''],
      'kharcha8': [''],
      'samagri': this.fb.array([
        this.initgeneralArray(),
      ]),
      'kharcha9': [''],
      'kharcha10': [''],
      'kharcha11': [''],
      'kharcha12': [''],
      'kharcha13': [''],
      'kharcha14': [''],
      'kharcha15': [''],
      'kharcha16': [''],
      'kharcha17': [''],
      'kharcha18': [''],
      'kharcha19': [''],
      'kharcha20': [''],
      'kharcha21': [''],
      'kharcha22': [''],
      'kharcha23': [''],
      'kharcha24': [''],
      'kharcha25': [''],
      'kharcha26': [''],
      'kharcha27': [''],
      'kharcha28': [''],
      'kharcha29': [''],
      'kharcha30': [''],
      'kharcha31': [''],
      'kharcha32': [''],
      'kharcha33': [''],
      'kharcha34': [''],
      'kharcha35': [''],
      'kharcha36': [''],
      'kharcha37': [''],
      'kharcha38': [''],
      'maujdaat39': [''],
      'maujdaat40': [''],
      'maujdaat41': [''],
      'maujdaat42': [''],
      'maujdaat43': [''],
      'maujdaat44': [''],
      'maujdaat45': [''],
      'maujdaat46': [''],
      'bhuktani47': [''],
      'bhuktani48': [''],
      'pragati49': [''],
      'pragati50': [''],
      'pragati51': [''],
      'upasthiti52': [''],
      'upasthiti53': [''],
      'upasthiti54': [''],
      'upasthiti55': [''],
      'upasthiti56': [''],
      'rohabarName': [''],
      'rohabarPost': [''],
      'rohabarDate': ['', Validators.compose([Validators.required, CustomValidators.checkDate])],
    });
  }
  initgeneralArray() {
    return this.fb.group({
      'name': [''],
      'qty': ['',],
      'rate': ['',],
      'total': ['',],

    })
  }
  addgeneralArray() {
    const control1 = <FormArray>this.anusuchiReport.controls['samagri'];
    control1.push(this.initgeneralArray());
  }
  removeRow(i) {
    const control1 = <FormArray>this.anusuchiReport.controls['samagri'];
    control1.removeAt(i);

  }

  // private aamdaaniForm() {
  //   return this.fb.group({
  //     srot: [''],
  //     rakam: [''],
  //     kaifiyat: [''],
  //   });
  // }
  // private kharchaForm() {
  //   return this.fb.group({
  //     bibaran: [''],
  //     dar:[''],
  //     pariman: [''],
  //     jamma: [''],
  //   });
  // }
  // private maujdaatForm() {
  //   return this.fb.group({
  //     rakam: [''],
  //     kaifiyat: [''],
  //   });
  // }
  // private bhuktaaniForm() {
  //   return this.fb.group({
  //     bibaran: [''],
  //     rakam: [''],
  //   });
  // }
  // private pragatiForm() {
  //   return this.fb.group({
  //     bibaran: [''],
  //     laxya: [''],
  //     pragati:[''],
  //   });
  // }


  // ngAfterViewInit() {
  //   this.unicode.initUnicode();
  // }

  // addMore() {
  //   this.amdaaniArray=this.anusuchiReport.controls["aamdaani"];
  //   for(let i=0;i<1;i++){
  //   this.amdaaniArray.push(this.aamdaaniForm());
  //   }
  //   this.kharchaArray=this.anusuchiReport.controls["kharcha"];
  //   for(let i=0;i<8;i++){
  //   this.kharchaArray.push(this.kharchaForm());
  //   }
  //   this.maujdaatArray=this.anusuchiReport.controls["maujdaat"];
  //   for(let i=0;i<3;i++){
  //   this.maujdaatArray.push(this.maujdaatForm());
  //   }
  //   // this.bhuktaaniArray=this.anusuchiReport.controls["bhuktaani"];
  //   // for(let i=0;i<1;i++){
  //   // this.bhuktaaniArray.push(this.bhuktaaniForm());
  //   // }
  //   // this.pragatiArray=this.anusuchiReport.controls["pragati"];
  //   // for(let i=0;i<1;i++){
  //   // this.pragatiArray.push(this.pragatiForm());
  //   // }
  // }

  ngOnInit() {

    this.ats.show(0).subscribe(
      result => {
        if (result['length'] > 0) {

          this.agencyType = result;
        }
      },
      error => {
        console.log(error);
      }
    );

    this.ts.currentMessage.subscribe((data) => {
      if (data == "on") {
        this.check = false;
        // local storage gets set when sidebar is opened taking current activity
        this.searchWithActivityDatas();
      }
      if (data == "off") {
        this.check = true;
      }
      console.log(this.check);
    });
    this.rs.getOrg().subscribe((res) => {
      this.organizationMasterData = res;
    }, (error) => {
      console.info(error);

    });
    this.activityDatas = JSON.parse(localStorage.getItem('ActivityDatas'));

    this.pys.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.yearAdapter = [];
      } else {
        this.yearAdapter = res;
        if (this.activityDatas) {
          this.anusuchi3.get('planYear').patchValue(this.activityDatas['planYear']);
          this.ChangePlanYear1(this.activityDatas['planYear']);
        } else {
          this.anusuchi3.get('planYear').patchValue(res[0]['yearCode']);
          this.ChangePlanYear(res[0]['yearCode']);
        }
        // this.anusuchi3.get('planYear').patchValue(res[0]['yearCode']);
        // this.ChangePlanYear(res[0]['yearCode']);
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
  ChangePlanYear1(id) {
    console.log(id);
    this.ks.indexActivity1({ planYear: id }).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
        console.log(this.activityDatas['id']);
        setTimeout(() => {
          if (this.check) {
            this.act.selectItem(this.activityDatas['id']);
          }
          // this.act.selectItem(this.activityDatas['id']);
          this.SearchData(this.anusuchi3.value, this.activityDatas['id']);
        }, 100);

      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }
  add1() {
    let a = Number(this.anusuchiReport.get('kharcha7').value)
    let b = Number(this.anusuchiReport.get('kharcha8').value)
    let c = a * b
    this.anusuchiReport.get('kharcha9').patchValue(c)
  }
  add2(i) {
    let a = Number(this.anusuchiReport.get('samagri')['controls'][i].get('rate').value)
    let b = Number(this.anusuchiReport.get('samagri')['controls'][i].get('qty').value)
    let c = a * b
    this.anusuchiReport.get('samagri')['controls'][i].get('total').patchValue(c)
  }
  add3() {
    let a = Number(this.anusuchiReport.get('kharcha10').value)
    let b = Number(this.anusuchiReport.get('kharcha11').value)
    let c = a * b
    this.anusuchiReport.get('kharcha12').patchValue(c)
  }
  add4() {
    let a = Number(this.anusuchiReport.get('kharcha14').value)
    let b = Number(this.anusuchiReport.get('kharcha15').value)
    let c = a * b
    this.anusuchiReport.get('kharcha16').patchValue(c)
  }
  add5() {
    let a = Number(this.anusuchiReport.get('kharcha18').value)
    let b = Number(this.anusuchiReport.get('kharcha19').value)
    let c = a * b
    this.anusuchiReport.get('kharcha20').patchValue(c)
  }
  add6() {
    let a = Number(this.anusuchiReport.get('kharcha21').value)
    let b = Number(this.anusuchiReport.get('kharcha22').value)
    let c = a * b
    this.anusuchiReport.get('kharcha23').patchValue(c)
  }
  add7() {
    let a = Number(this.anusuchiReport.get('kharcha25').value)
    let b = Number(this.anusuchiReport.get('kharcha26').value)
    let c = a * b
    this.anusuchiReport.get('kharcha27').patchValue(c)
  }
  add8() {
    let a = Number(this.anusuchiReport.get('kharcha29').value)
    let b = Number(this.anusuchiReport.get('kharcha30').value)
    let c = a * b
    this.anusuchiReport.get('kharcha31').patchValue(c)
  }
  add9() {
    let a = Number(this.anusuchiReport.get('kharcha32').value)
    let b = Number(this.anusuchiReport.get('kharcha33').value)
    let c = a * b
    this.anusuchiReport.get('kharcha34').patchValue(c)
  }
  add10() {
    let a = Number(this.anusuchiReport.get('kharcha36').value)
    let b = Number(this.anusuchiReport.get('kharcha37').value)
    let c = a * b
    this.anusuchiReport.get('kharcha38').patchValue(c)
  }


  clearNepin() {
    this.nepin1.clearInput();
    this.nepin2.clearInput();
    this.nepin3.clearInput();
    this.nepin4.clearInput();
    this.nepin5.clearInput();
    this.nepin6.clearInput();
    this.nepin7.clearInput();
    this.nepin8.clearInput();
    this.nepin9.clearInput();
    this.nepin10.clearInput();
    this.nepin11.clearInput();
    this.nepin12.clearInput();
    this.nepin13.clearInput();
    this.nepin14.clearInput();
    this.nepin15.clearInput();
    this.nepin16.clearInput();
    this.nepin17.clearInput();
    this.nepin18.clearInput();
    this.nepin19.clearInput();
    this.nepin20.clearInput();
    this.nepin21.clearInput();
    this.nepin22.clearInput();
    this.nepin23.clearInput();
    this.nepin24.clearInput();
  }
  SearchData(post, val) {
    // post['activity'] = this.act.val();
    post['activity'] = val;
    this.jqxLoader.open();
    if (val) {
      this.rs.getanusuchi3Report(post).subscribe((res) => {
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.SearchDatas = [];
        } else {
          this.SearchDatas = res;
          this.anusuchiReport.reset();
          this.anusuchiReport.get('samagri')['controls'] = [];
          this.addgeneralArray();
          this.clearNepin();
          let id = val;

          // get already saved record
          this.rs.getanusuchi3ReportData(id).subscribe((result) => {
            //   console.log(res);
            if (result['error']) {
              this.anusuchiReport.reset();
              this.clearNepin();
            } else {
              let a = JSON.parse(result['data']);
              let res = a['data'];
              this.anusuchiReport.reset();
              this.clearNepin();
              for (let i = 0; i < res['samagri'].length - 1; i++) {
                this.addgeneralArray();
              }
              for (let i = 0; i < res['samagri'].length; i++) {
                this.anusuchiReport.get('samagri')['controls'][i].get('name').patchValue(res['samagri'][i].name)
                this.anusuchiReport.get('samagri')['controls'][i].get('rate').patchValue(res['samagri'][i].rate)
                this.anusuchiReport.get('samagri')['controls'][i].get('qty').patchValue(res['samagri'][i].qty)
                this.anusuchiReport.get('samagri')['controls'][i].get('total').patchValue(res['samagri'][i].total)
              }
              this.anusuchiReport.controls['aamdaani1'].patchValue(res['aamdaani1']);
              this.anusuchiReport.controls['mahila'].patchValue(res['mahila']);
              this.anusuchiReport.controls['purus'].patchValue(res['purus']);
              this.anusuchiReport.controls['total'].patchValue(res['total']);
              this.anusuchiReport.controls['aamdaani2'].patchValue(res['aamdaani2']);
              this.anusuchiReport.controls['aamdaani3'].patchValue(res['aamdaani3']);
              this.anusuchiReport.controls['aamdaani4'].patchValue(res['aamdaani4']);
              this.anusuchiReport.controls['aamdaani5'].patchValue(res['aamdaani5']);
              this.anusuchiReport.controls['aamdaani6'].patchValue(res['aamdaani6']);
              this.anusuchiReport.controls['kharcha7'].patchValue(res['kharcha7']);
              this.anusuchiReport.controls['kharcha8'].patchValue(res['kharcha8']);
              this.anusuchiReport.controls['kharcha9'].patchValue(res['kharcha9']);
              this.anusuchiReport.controls['kharcha10'].patchValue(res['kharcha10']);
              this.anusuchiReport.controls['kharcha11'].patchValue(res['kharcha11']);
              this.anusuchiReport.controls['kharcha12'].patchValue(res['kharcha12']);
              this.anusuchiReport.controls['kharcha13'].patchValue(res['kharcha13']);
              this.anusuchiReport.controls['kharcha14'].patchValue(res['kharcha14']);
              this.anusuchiReport.controls['kharcha15'].patchValue(res['kharcha15']);
              this.anusuchiReport.controls['kharcha16'].patchValue(res['kharcha16']);
              this.anusuchiReport.controls['kharcha17'].patchValue(res['kharcha17']);
              this.anusuchiReport.controls['kharcha18'].patchValue(res['kharcha18']);
              this.anusuchiReport.controls['kharcha19'].patchValue(res['kharcha19']);
              this.anusuchiReport.controls['kharcha20'].patchValue(res['kharcha20']);
              this.anusuchiReport.controls['kharcha21'].patchValue(res['kharcha21']);
              this.anusuchiReport.controls['kharcha22'].patchValue(res['kharcha22']);
              this.anusuchiReport.controls['kharcha23'].patchValue(res['kharcha23']);
              this.anusuchiReport.controls['kharcha24'].patchValue(res['kharcha24']);
              this.anusuchiReport.controls['kharcha25'].patchValue(res['kharcha25']);
              this.anusuchiReport.controls['kharcha26'].patchValue(res['kharcha26']);
              this.anusuchiReport.controls['kharcha27'].patchValue(res['kharcha27']);
              this.anusuchiReport.controls['kharcha28'].patchValue(res['kharcha28']);
              this.anusuchiReport.controls['kharcha29'].patchValue(res['kharcha29']);
              this.anusuchiReport.controls['kharcha30'].patchValue(res['kharcha30']);
              this.anusuchiReport.controls['kharcha31'].patchValue(res['kharcha31']);
              this.anusuchiReport.controls['kharcha32'].patchValue(res['kharcha32']);
              this.anusuchiReport.controls['kharcha33'].patchValue(res['kharcha33']);
              this.anusuchiReport.controls['kharcha34'].patchValue(res['kharcha34']);
              this.anusuchiReport.controls['kharcha35'].patchValue(res['kharcha35']);
              this.anusuchiReport.controls['kharcha36'].patchValue(res['kharcha36']);
              this.anusuchiReport.controls['kharcha37'].patchValue(res['kharcha37']);
              this.anusuchiReport.controls['kharcha38'].patchValue(res['kharcha38']);
              this.anusuchiReport.controls['maujdaat39'].patchValue(res['maujdaat39']);
              this.anusuchiReport.controls['maujdaat40'].patchValue(res['maujdaat39']);
              this.anusuchiReport.controls['maujdaat41'].patchValue(res['maujdaat39']);
              this.anusuchiReport.controls['maujdaat42'].patchValue(res['maujdaat39']);
              this.anusuchiReport.controls['maujdaat43'].patchValue(res['maujdaat39']);
              this.anusuchiReport.controls['maujdaat44'].patchValue(res['maujdaat39']);
              this.anusuchiReport.controls['maujdaat45'].patchValue(res['maujdaat39']);
              this.anusuchiReport.controls['maujdaat46'].patchValue(res['maujdaat39']);
              this.anusuchiReport.controls['bhuktani47'].patchValue(res['maujdaat39']);
              this.anusuchiReport.controls['bhuktani48'].patchValue(res['maujdaat39']);
              this.anusuchiReport.controls['pragati49'].patchValue(res['pragati49']);
              this.anusuchiReport.controls['pragati50'].patchValue(res['pragati50']);
              this.anusuchiReport.controls['pragati51'].patchValue(res['pragati51']);
              this.anusuchiReport.controls['upasthiti52'].patchValue(res['upasthiti52']);
              this.anusuchiReport.controls['upasthiti53'].patchValue(res['upasthiti53']);
              this.anusuchiReport.controls['upasthiti54'].patchValue(res['upasthiti54']);
              this.anusuchiReport.controls['upasthiti55'].patchValue(res['upasthiti55']);
              this.anusuchiReport.controls['upasthiti56'].patchValue(res['upasthiti56']);
              this.anusuchiReport.controls['rohabarName'].patchValue(res['rohabarName']);
              this.anusuchiReport.controls['rohabarPost'].patchValue(res['rohabarPost']);
              this.anusuchiReport.controls['rohabarDate'].patchValue(res['rohabarDate']);
            }
            this.anusuchiReport.controls['activity'].patchValue(id);
          });
        }
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
    }
    else {
      this.jqxLoader.close();
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'कृपया याेजना छान्नुहाेस्';
      this.errNotification.open();
    }
  }
  ChangePlanYear(id) {
    this.ks.indexActivity({ planYear: id, activityType: 3 }).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }
  /**
* Export Report Data to Excel
*/
  exportReport(): void {
    let htmltable = document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  /**
   * Open Print Window to print report
   */
  printReport(): void {
    let post = this.anusuchiReport.value;
    // post['activity'] = this.act.val();
    // if (this.check)
    //   post['activity'] = this.act.val();
    // else
    //   post['activity'] = this.activityDatas['id'];
    this.rs.sendanusuchi3Report(post).subscribe((res) => {
      if (res['message'] === 'success') {

      }
    });
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .table-container table th{
              text-align: center;
          }

          .table-container table .tleft{
              text-align: left !important;
          }

          .table-container table .tright{
              text-align: right !important;
          }
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }

          @font-face {
            font-family: 'nepaliNormal';
            src: url('assets/css/Kantipur.TTF');
            font-weight: normal;
            font-style: normal;
        }
        .nepaliText{
          font-family:'nepaliNormal';
          font-size:18px!important;
      }
       .table {
            border-collapse: collapse;
            background-color: transparent;
          }


        .tableClas{
                border-collapse: collapse;
        }
    .tableClas  td {
          // border: 1px solid black;
          border: 1px solid #eceeef;
        }
        ul{
          list-style-type: none
      }

      .print_show{
        display: inline-block !important;
    }

    .print_hide{
        display: none !important;
      }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  implementingAgencyChange(selectedEvent) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.agencyName = displayText;
      let agencyCode = selectedEvent.target.value;
      this.getAgencyType(agencyCode);
    } else {
      this.childType = '';
    }
  }


  getAgencyType(agencyCode = 0, index = 0, type = null) {
    // remove previously added form control if the parent select is changed.
    this.ats.show(agencyCode).subscribe(
      result => {
        if (result['length'] > 0) {
          this.childType = result;
        } else {
          this.childType = '';
        }
      },
      error => {
        console.log(error);
      }
    );
  }

}
