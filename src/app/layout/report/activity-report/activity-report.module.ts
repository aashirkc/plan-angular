import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityReportComponent } from './activity-report.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ActivityReportRoutingModule } from './activity-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ActivityReportRoutingModule
  ],
  declarations: [ActivityReportComponent],
  schemas:[ NO_ERRORS_SCHEMA ]
})
export class ActivityReportModule { }
