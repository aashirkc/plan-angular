import {
    Component,
    ViewChild,
    ViewContainerRef,
    Inject,
    ChangeDetectorRef,
    OnInit
} from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
    AllReportService,
    DateConverterService,
    AgencyTypeService,
    UnicodeTranslateService,
    ActivityOutputService,
    ActivityFocusAreaMasterService,
    PlanningYearService,
    InclusionGroupService,
    CurrentUserService,
    PrintingService
} from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { TranslateService } from '@ngx-translate/core';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';

@Component({
    selector: 'app-activity-report',
    templateUrl: './activity-report.component.html',
    styleUrls: ['./activity-report.component.scss']
})
export class ActivityReportComponent implements OnInit {
    activityForm: FormGroup;
    source: any;
    dataAdapter: any;
    columns: any[];
    editrow: number = -1;
    transData: any;
    planYearAdapter: Array<any> = [];
    agencyType: any = [];
    activityAdapter: Array<any> = [];
    activityFocusAreaAdapter: any = [];
    activityCategoryAdapter: any = [];
    userData: any = {};
    SearchDatas: Array<any> = [];
    API_URL_DOC: any;
    API_URL: any;
    lang: any;
    childType: any = [];
    agencyName: any;
    workServiceTypeAdapter: any = [
        {
            name: 'Consumer Committe',
            id: 'उपभोक्ता समिति'
        },
        {
            name: 'Contract',
            id: 'ठेक्का मार्फत'
        },
        {
            name: 'Amanathan Marfat',
            id: 'अमानत मार्फत'
        },
        {
            name: 'Sahakari',
            id: 'श्रम सहकारी / संस्था मार्फत'
        }
    ];

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
    @ViewChild('myViewWindow') myViewWindow: jqxWindowComponent;
    @ViewChild('myEditWindow') myEditWindow: jqxWindowComponent;
    @ViewChild('myReportWindow') myReportWindow: jqxWindowComponent;
    @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;

    constructor(
        private fb: FormBuilder,
        private cus: CurrentUserService,
        private dfs: DateConverterService,
        @Inject('API_URL_DOC') API_URL_DOC: string,
        @Inject('API_URL') API_URL: string,
        private cdr: ChangeDetectorRef,
        private pys: PlanningYearService,
        private igs: InclusionGroupService,
        private ats: AgencyTypeService,
        private afams: ActivityFocusAreaMasterService,
        private activityOutputService: ActivityOutputService,
        private translate: TranslateService,
        private ars: AllReportService,
        private router: Router,
        private unicode: UnicodeTranslateService,
        private printingService: PrintingService
    ) {
        this.createForm();
        this.getTranslation();
        this.userData = this.cus.getTokenData();
        this.API_URL_DOC = API_URL_DOC;
        this.API_URL = API_URL;
    }

    ngOnInit() {
        this.lang = this.translate.currentLang.substr(0, 2);
        this.pys.index({}).subscribe(
            res => {
                this.planYearAdapter = res;
                if (this.planYearAdapter.length > 0) {
                    let planCode = this.planYearAdapter[0].yearCode || '';
                    this.activityForm.get('planYear').patchValue(planCode);
                }
            },
            error => {
                console.info(error);
            }
        );
        this.igs.index({}).subscribe(
            res => {
                this.activityAdapter = res;
            },
            error => {
                console.info(error);
            }
        );
        this.afams.show(0).subscribe(
            res => {
                this.activityFocusAreaAdapter = res;
            },
            error => {
                console.info(error);
            }
        );
        this.ats.show(0).subscribe(
            result => {
                if (result['length'] > 0) {
                    this.agencyType = result;
                }
            },
            error => {
                console.log(error);
            }
        );
    }
    getTranslation() {
        this.translate
            .get([
                'SN',
                'regNo',
                'universityRoll',
                'ADD',
                'EDIT',
                'NAME',
                'nepali',
                'english',
                'mobile',
                'VIEW',
                'DETAIL',
                'ACTIONS',
                'UPDATE',
                'DELETESELECTED',
                'RELOAD'
            ])
            .subscribe((translation: [string]) => {
                this.transData = translation;
            });
    }
    implementingAgencyChange(selectedEvent) {
        if (
            selectedEvent &&
            selectedEvent.target &&
            selectedEvent.target.value
        ) {
            let displayText = selectedEvent.target.selectedOptions[0].text;
            this.agencyName = displayText;
            let agencyCode = selectedEvent.target.value;
            this.getAgencyType(agencyCode);
        } else {
            this.childType = '';
        }
    }
    getAgencyType(agencyCode = 0, index = 0, type = null) {
        // remove previously added form control if the parent select is changed.
        this.ats.show(agencyCode).subscribe(
            result => {
                if (result['length'] > 0) {
                    this.childType = result;
                } else {
                    this.childType = '';
                }
            },
            error => {
                console.log(error);
            }
        );
    }
    createForm() {
        this.activityForm = this.fb.group({
            planYear: ['', Validators.required],
            activityFor: [''],
            activityFocusArea: [''],
            activityCategory: [''],
            activityName: [''],
            parent: [''],
            child: [''],
            workServiceType: ['']
        });
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.unicode.initUnicode();
        }, 200);
        this.cdr.detectChanges();
    }

    LoadActivityCategory() {
        let data = this.activityForm.value;
        if (data['activityFocusArea']) {
            this.jqxLoader.open();
            this.afams.show(data['activityFocusArea']).subscribe(
                res => {
                    this.activityCategoryAdapter = res;
                    this.activityForm.get('activityCategory').patchValue('');
                    this.jqxLoader.close();
                },
                error => {
                    console.info(error);
                    this.activityForm.get('activityCategory').patchValue('');
                    this.jqxLoader.close();
                }
            );
        }
    }

    SearchData(post) {
        if (this.childType.length <= 0) {
            post['child'] = '';
        }
        this.jqxLoader.open();
        this.ars.getActivityReport(post).subscribe(
            res => {
                if (res.length == 1 && res[0].error) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = res[0].error;
                    this.errNotification.open();
                    this.SearchDatas = [];
                } else {
                    this.SearchDatas = res;
                }
                this.jqxLoader.close();
            },
            error => {
                console.info(error);
                this.jqxLoader.close();
            }
        );
    }

    /**
     * Export Report Data to Excel
     */
    exportReport(): void {
        let htmltable = document.getElementById('reportContainer');
        let html = htmltable.outerHTML;
        window.open(
            'data:application/vnd.ms-excel,' + encodeURIComponent(html)
        );
    }

    /**
     * Open Print Window to print report
     */
    printReport(): void {
        const printContents = document.getElementById('reportContainer')
            .innerHTML;
        this.printingService.printContents(printContents, 'Print Tab');
    }
}
