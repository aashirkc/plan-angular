import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { PrintingService, AllReportService } from '../../../shared';

@Component({
    selector: 'app-program-report',
    templateUrl: './program-report.component.html',
    styleUrls: ['./program-report.component.scss']
})
export class ProgramReportComponent implements OnInit {
    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    SearchDatas: any;
    total: any = 0;
    constructor(
        private printingService: PrintingService,
        private ars: AllReportService,
    ) { }

    ngOnInit() { }


    search = (value) => {
        let post = value['value'];
        post['programID'] = value['program'];
        this.ars.getActivityDetails(post).subscribe((res) => {
            if (res.length == 1 && res[0].error) {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = res[0].error;
                this.errNotification.open();
                this.SearchDatas = [];
            } else {
                this.SearchDatas = res[0];
                this.total = this.SearchDatas['program'][0]['budget'];
                for (let data of this.SearchDatas['activites']) {
                    this.total = this.total - Number(data.budget);
                }
            }
            this.jqxLoader.close();
        }, (error) => {
            console.info(error);
            this.jqxLoader.close();
        });
    };

    changeLoadingStatus = $event => {
        if ($event) {
            this.jqxLoader.open();
        } else {
            this.jqxLoader.close();
        }
    };

    showError = $event => {
        console.log($event);
    };

    printReport(): void {
        const printContents = document.getElementById('reportContainer')
            .innerHTML;
        this.printingService.printContents(
            printContents,
            'Print tab',
            false,
            `
            th{
                text-align: center !important;
            }
        `
        );
    }

    exportReport(): void {
        let htmltable = document.getElementById('reportContainer');
        let html = htmltable.outerHTML;
        let fhtml = `
        <html>
        <head>
            <style>
            .borderTable{
            border: 1px solid #757879;
            }
           
            </style>
        </head>
        <body onload="window.print();window.close()">${html}</body>
        </html>`;
        window.open(
            'data:application/vnd.ms-excel,' + encodeURIComponent(fhtml)
        );
    }
}
