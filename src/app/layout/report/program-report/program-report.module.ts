import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ProgramReportRoutingModule } from './program-report-routing.module';
import { ProgramReportComponent } from './program-report.component';
import { AnusuchiSearchBarModule } from '../anusuchi-search-bar/anusuchi-search-bar.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ProgramReportRoutingModule,
        AnusuchiSearchBarModule
    ],
    declarations: [ProgramReportComponent]
})
export class ProgramReportModule {}
