import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgramReportComponent } from './program-report.component';

const routes: Routes = [
    {
        path: '',
        component: ProgramReportComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProgramReportRoutingModule {}
