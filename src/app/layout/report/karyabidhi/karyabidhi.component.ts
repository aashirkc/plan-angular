import {
    Component,
    ViewChild,
    ViewContainerRef,
    Inject,
    ChangeDetectorRef,
    OnInit
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
    DateConverterService,
    AgencyTypeService,
    ProgressReportingTippaniAadeshService,
    AllReportService,
    UnicodeTranslateService,
    PlanningYearService,
    CurrentUserService,
    PrintingService
} from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { ProgramsService } from '../../../shared/services/programs.service';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

@Component({
    selector: 'app-karyabidhi',
    templateUrl: './karyabidhi.component.html',
    styleUrls: ['./karyabidhi.component.scss']
})
export class KaryabidhiComponent implements OnInit {
    anusuchi17Report: FormGroup;
    source: any;
    dataAdapter: any;
    columns: any[];
    editrow: number = -1;
    transData: any;
    planYear: any;
    planYearAdapter: Array<any> = [];
    activityAdapter: Array<any> = [];
    activityFocusAreaAdapter: any = [];
    activityCategoryAdapter: any = [];
    programAdapter: any = [];
    childType: any = [];
    userData: any = {};
    SearchDatas: Array<any> = [];
    API_URL_DOC: any;
    API_URL: any;
    columngroups: any[];
    agencyType: any = [];
    lang: any;
    agencyName: any;
    organizationMasterData: any;
    // <span *ngIf="data['status']=='Not Started'"> {{ 'NOT_STARTED' | translate }}</span>
    // <span *ngIf="data['status']=='Ongoing'"> {{ 'ONGOINGE' | translate }}</span>
    // <span *ngIf="data['status']=='Completed'"> {{ 'COMPLETEDE' | translate }}</span>
    // <span *ngIf="data['status']=='Suspended'"> {{ 'SUSPENDED' | translate }}</span>
    // <span *ngIf="data['status']=='Spill Over'"> {{ 'SPILLOVERE' | translate }}</span>
    // <span *ngIf="data['status']=='Abandoned'"> {{ 'ABANDONEDE' | translate }}</span>

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
    @ViewChild('myGrid') myGrid: jqxGridComponent;
    @ViewChild('myViewWindow') myViewWindow: jqxWindowComponent;
    @ViewChild('myEditWindow') myEditWindow: jqxWindowComponent;
    @ViewChild('myReportWindow') myReportWindow: jqxWindowComponent;
    @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;
    @ViewChild('list') list: jqxComboBoxComponent;
    isRul: boolean;
    distType: any;

    constructor(
        private fb: FormBuilder,
        private cus: CurrentUserService,
        private dfs: DateConverterService,
        private ats: AgencyTypeService,
        private ps: ProgramsService,
        @Inject('API_URL_DOC') API_URL_DOC: string,
        @Inject('API_URL') API_URL: string,
        @Inject('DIST_TYPE') distType,
        private cdr: ChangeDetectorRef,
        private pys: PlanningYearService,
        private report: AllReportService,
        private translate: TranslateService,
        private unicode: UnicodeTranslateService,
        private printingService: PrintingService,
        private prtas: ProgressReportingTippaniAadeshService
    ) {
        this.distType = distType;
        this.createForm();
        this.getTranslation();
        this.userData = this.cus.getTokenData();
        this.API_URL_DOC = API_URL_DOC;
        this.API_URL = API_URL;
    }

    // --- search bar emitter functions
    search = ({ value, id }) => this.SearchData(value, id);

    changeLoadingStatus($event) {
        !$event && this.jqxLoader.close();
        $event && this.jqxLoader.open();
    }

    showError($event) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = $event;
        this.errNotification.open();
    }
    // --- end

    ngOnInit() {
        this.lang = this.translate.currentLang.substr(0,2);
        this.isRul = false;
        const orgMaster = JSON.parse(localStorage.getItem('org'));
        if (orgMaster && orgMaster[0]) {
            const orgType = orgMaster[0]['orgType'];
            this.isRul = orgType === 'RUL';
        }
        this.prtas.getOrganizationName().subscribe(
            res => {
                this.organizationMasterData = res;
            },
            error => {
                console.info(error);
            }
        );
        this.pys.index({}).subscribe(
            res => {
                this.planYearAdapter = res;
                if (this.planYearAdapter.length > 0) {
                    let planCode = this.planYearAdapter[0].yearCode || '';
                    this.anusuchi17Report.get('planYear').patchValue(planCode);
                    this.planChange(planCode);
                }
            },
            error => {
                console.info(error);
            }
        );
        this.ats.show(0).subscribe(
            result => {
                if (result['length'] > 0) {
                    this.agencyType = result;
                }
            },
            error => {
                console.log(error);
            }
        );
    }
    implementingAgencyChange(selectedEvent) {
        if (
            selectedEvent &&
            selectedEvent.target &&
            selectedEvent.target.value
        ) {
            let displayText = selectedEvent.target.selectedOptions[0].text;
            this.agencyName = displayText;
            let agencyCode = selectedEvent.target.value;
            this.getAgencyType(agencyCode);
        } else {
            console.log('entered');
            this.childType = '';
            this.anusuchi17Report.get('child').patchValue('');
        }
    }
    getAgencyType(agencyCode = 0, index = 0, type = null) {
        // remove previously added form control if the parent select is changed.
        this.ats.show(agencyCode).subscribe(
            result => {
                if (result['length'] > 0) {
                    this.childType = result;
                } else {
                    this.anusuchi17Report.get('child').patchValue('');
                    this.childType = '';
                }
            },
            error => {
                console.log(error);
            }
        );
    }
    planChange(value) {
        this.ps
            .getPrograms({
                planYear: value,
                focusAreaLevel1: '',
                implementingAgencyType1: ''
            })
            .subscribe(res => {
                this.programAdapter = res;
            });
        this.pys.index({}).subscribe(
            res => {
                this.planYearAdapter = res;
                if (this.planYearAdapter.length > 0) {
                    for (let i = 0; i < this.planYearAdapter.length; i++) {
                        if (value == this.planYearAdapter[i].yearCode) {
                            this.planYear = this.planYearAdapter[i].yearName;
                            break;
                        }
                    }
                }
            },
            error => {
                console.info(error);
            }
        );
    }
    getTranslation() {
        this.translate
            .get([
                'SN',
                'regNo',
                'universityRoll',
                'ADD',
                'EDIT',
                'NAME',
                'nepali',
                'english',
                'mobile',
                'VIEW',
                'DETAIL',
                'ACTIONS',
                'UPDATE',
                'DELETESELECTED',
                'RELOAD'
            ])
            .subscribe((translation: [string]) => {
                this.transData = translation;
            });
    }

    createForm() {
        this.anusuchi17Report = this.fb.group({
            planYear: ['', Validators.required],
            child: [''],
            parent: ['']
        });
    }

    ngAfterViewInit() {
        this.cdr.detectChanges();
        // this.unicode.initUnicode();
    }
    SearchData(post, id) {
        console.log(post, id);
        post['program'] = id;
        if (post['program']) {
            this.jqxLoader.open();
            this.report.getKaryabidhi(post).subscribe(
                res => {
                    if (res && res.length == 1 && res[0].error) {
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = res[0].error;
                        this.errNotification.open();
                        this.SearchDatas = [];
                    } else {
                        this.SearchDatas = res;
                    }
                    this.jqxLoader.close();
                },
                error => {
                    console.info(error);
                    this.jqxLoader.close();
                }
            );
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'कृपया कार्यक्रम छनाेट गर्नुहाेला';
            this.errNotification.open();
        }
    }

    /**
     * Export Report Data to Excel
     */
    exportReport(): void {
        let htmltable = document.getElementById('reportContainer');
        let html = htmltable.outerHTML;
        let fhtml = `
    <html>
      <head>
        <style>
        .borderTable{
          border: 1px solid #757879;
        }
        @font-face {
          font-family: 'preeti';
          src: url('../assets/css/Preeti.TTF');
          font-weight: normal;
          font-style: normal;
      }
         .preeti{
          font-family:'preeti' !important;
          font-size:18px ;
         }
        </style>
      </head>
  <body onload="window.print();window.close()">${html}</body>
    </html>`;
        window.open(
            'data:application/vnd.ms-excel,' + encodeURIComponent(fhtml)
        );
    }

    /**
     * Open Print Window to print report
     */
    printReport(): void {
        const printContents = document.getElementById('reportContainer')
            .innerHTML;
        this.printingService.printContents(
            printContents,
            'Print tab',
            false,
            `
            th{
                text-align: center !important;
            }
        `
        );
        //     popupWin = window.open(
        //         '',
        //         '_blank',
        //         'top=0,left=0,height=100%,width=auto'
        //     );
        //     popupWin.document.open();
        //     popupWin.document.write(`
        //   <html>
        //     <head>
        //       <title>Print tab</title>
        //       <style>
        //       @font-face {
        //         font-family: 'preeti';
        //         src: url('../assets/css/Preeti.TTF');
        //         font-weight: normal;
        //         font-style: normal;
        //     }

        //     .preeti{
        //         font-family:'preeti';
        //         font-size:18px;
        //     }
        //       .table-container table th{
        //           text-align: center;
        //       }

        //       .table-container table .tleft{
        //           text-align: left !important;
        //       }

        //       .table-container table .tright{
        //           text-align: right !important;
        //       }
        //       .p{
        //         margin-bottom: 5px;
        //       }
        //       .table-bordered {
        //           border: 1px solid #eceeef;
        //       }
        //       .table {
        //         position:relative;
        //         width: 100%;
        //         max-width: 100%;
        //         margin-top: 20px;
        //         margin-bottom: 1rem;
        //         font-size: smaller;
        //       }
        //       .table {
        //         border-collapse: collapse;
        //         background-color: transparent;
        //       }
        //       .table-bordered th, .table-bordered td {
        //           border: 1px solid #eceeef;
        //       }
        //       @font-face {
        //         font-family: 'nepaliNormal';
        //         src: url('assets/css/Kantipur.TTF');
        //         font-weight: normal;
        //         font-style: normal;
        //     }
        //     .nepaliText{
        //       font-family:'nepaliNormal';
        //       font-size:18px!important;
        //   }
        //       .table th, .table td {
        //           padding: 0.55rem;
        //           vertical-align: top;
        //           border-top: 1px solid #eceeef;
        //           text-align:left;
        //       }
        //       .last-td{
        //         display:none;
        //       }
        //       //........Customized style.......
        //       </style>
        //     </head>
        // <body onload="window.print();window.close()">${printContents}</body>
        //   </html>`);
        //     popupWin.document.close();
    }
}
