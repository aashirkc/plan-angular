import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KaryabidhiComponent } from './karyabidhi.component';

const routes: Routes = [
    {
        path: '',
        component: KaryabidhiComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class KaryabidhiRoutingModule { }
