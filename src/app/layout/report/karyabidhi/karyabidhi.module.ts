import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KaryabidhiComponent } from './karyabidhi.component';
import { SharedModule } from 'app/shared/modules/shared.module';
import { KaryabidhiRoutingModule } from './karyabidhi-routing.module';
import { AnusuchiSearchBarModule } from '../anusuchi-search-bar/anusuchi-search-bar.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AnusuchiSearchBarModule,
    KaryabidhiRoutingModule
  ],
  declarations: [KaryabidhiComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class KaryabidhiModule { }
