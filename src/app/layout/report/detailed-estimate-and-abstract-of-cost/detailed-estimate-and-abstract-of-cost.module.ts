import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailedEstimateAndAbstractOfCostComponent } from './detailed-estimate-and-abstract-of-cost.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { DetailedEstimateAndAbstractOfCostRoutingModule } from './detailed-estimate-and-abstract-of-cost-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DetailedEstimateAndAbstractOfCostRoutingModule
  ],
  declarations: [DetailedEstimateAndAbstractOfCostComponent],
  schemas:[ NO_ERRORS_SCHEMA ]
})
export class DetailedEstimateAndAbstractOfCostModule { }
