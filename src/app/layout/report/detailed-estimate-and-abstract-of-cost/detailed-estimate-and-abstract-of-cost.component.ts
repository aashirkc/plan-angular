import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { TranslateService } from '@ngx-translate/core';
import { EstimateAbstractOfCostService, PlanningYearService, } from '../../../shared';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

@Component({
  selector: 'app-detailed-estimate-and-abstract-of-cost',
  templateUrl: './detailed-estimate-and-abstract-of-cost.component.html',
  styleUrls: ['./detailed-estimate-and-abstract-of-cost.component.scss']
})
export class DetailedEstimateAndAbstractOfCostComponent implements OnInit {

  searchForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  aboutFinancialOperationForm: FormGroup;
  foForm: FormGroup;
  transData: any;
  yearAdapter: Array<any> = [];
  activityAdapter: Array<any> = [];
  userData: any = {};
  SearchDatas: Array<any> = [];
  organizationDta: any;
  date: any
  fullDate: any;
  planYearAdapter: Array<any> = [];

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;
  @ViewChild('myComboBox') myComboBox: jqxComboBoxComponent;

  constructor(
    private fb: FormBuilder,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    private pys: PlanningYearService,
    @Inject('API_URL') API_URL: string,
    private estimateCostService: EstimateAbstractOfCostService) {
    this.createForm();

  }

  ngOnInit() {
    this.pys.index({}).subscribe((res) => {
      this.planYearAdapter = res;
      if (this.planYearAdapter.length > 0) {
        let planCode = this.planYearAdapter[0].yearCode || '';
        this.searchForm.get('planYear').patchValue(planCode);
      }
    }, (error) => {
      console.info(error);
    });
    this.estimateCostService.indexActivity({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

    this.estimateCostService.indexOrganizationMaster({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.organizationDta = {};
      } else {
        this.organizationDta = res && res[0];
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  createForm() {
    this.searchForm = this.fb.group({
      'planYear': ['',],
    });

  }

  Subtotal: number;
  SubtotalWord: string;
  comboBoxOnChange(event: any) {
    let post = {};
    post['activity'] = this.myComboBox.val() || '';
    this.estimateCostService.getTableData(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.SearchDatas = [];
      } else {
        this.SearchDatas = res['Record'];
        this.Subtotal = res['subTotal'];
        this.SubtotalWord = res['inWord'];
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }

  SearchData(post) {
    post['activity'] = this.myComboBox.val() || '';
    this.estimateCostService.getTableData(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.SearchDatas = [];
      } else {
        this.SearchDatas = res['Record'];
        this.Subtotal = res['subTotal'];
        this.SubtotalWord = res['inWord'];
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }





  printData(): void {
    let printContents, popupWin;
    printContents = '';
    printContents = document.getElementById('printContent').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title></title>
          <style>
          .exam-triplicate{
            background: #fff;
            margin-bottom: 20px;
          }
          .univ-title {
              padding-top: 20px;
          }
          .form-style-4 {
            display: none !important;
        }
        .print_show{
            display: inline-block !important;
        }
          .table{
            width:100%;
            border-collapse:collapse;
          }
           .col-md-4{
             display:inline-block;
             width:33%;
           }
          .univ-title p {
              margin: 0 0 0px;      
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
              padding: 3px;
          }
          .logo-top{
            padding-top: 25px;
          }
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
}
