import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailedEstimateAndAbstractOfCostComponent } from './detailed-estimate-and-abstract-of-cost.component';

const routes: Routes = [
  {
    path: '',
    component: DetailedEstimateAndAbstractOfCostComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailedEstimateAndAbstractOfCostRoutingModule { }
