import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeasurementBookAndBillOfQtyReportComponent } from './measurement-book-and-bill-of-qty-report.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { MeasurementBookAndBillOfQtyReportRoutingModule } from './measurement-book-and-bill-of-qty-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MeasurementBookAndBillOfQtyReportRoutingModule
  ],
  declarations: [MeasurementBookAndBillOfQtyReportComponent],
  schemas:[ NO_ERRORS_SCHEMA ]
})
export class MeasurementBookAndBillOfQtyReportModule { }
