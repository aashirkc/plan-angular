import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MeasurementBookAndBillOfQtyReportComponent } from './measurement-book-and-bill-of-qty-report.component';

const routes: Routes = [
  {
    path: '',
    component: MeasurementBookAndBillOfQtyReportComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MeasurementBookAndBillOfQtyReportRoutingModule { }
