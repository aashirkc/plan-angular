import { Component, OnInit, ViewChild } from '@angular/core';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { UnicodeTranslateService, AllReportService } from '../../../shared';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-anusuchi2',
    templateUrl: './anusuchi2.component.html',
    styleUrls: ['./anusuchi2.component.scss']
})
export class Anusuchi2Component implements OnInit {
    check: boolean = true;
    anusuchi2Report: FormGroup;

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

    constructor(
        private unicode: UnicodeTranslateService,
        private fb: FormBuilder,
        private all: AllReportService 

    ) { }

    userData: any;

    ngOnInit() {
        this.userData = JSON.parse(localStorage.getItem('pcUser'))
        this.createForm();
    }

    search = ({ value, id }) => this.SearchData(value, id)

    changeLoadingStatus($event) {
        !$event && this.jqxLoader.close();
        $event && this.jqxLoader.open();
    }

    showError($event) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = $event;
        this.errNotification.open();
    }

    // ngAfterViewInit() {
    //     this.unicode.initUnicode();
    // }
    activityId: any
    anusuchi2: any

    SearchData(value: any, id: any) {
        this.activityId = id;
        let q = {
            id:id
        }

        this.all.getAllAnusuchi1(q).subscribe(res=>{
            let a = JSON.parse(res['anusuchi2'])
               this.anusuchi2 = a
               this.setData(this.anusuchi2);
         
          })
          
    }

    setData(post){
       
        this.anusuchi2Report.get('programName').patchValue(post['programName'])
        this.anusuchi2Report.get('unit').patchValue(post['unit'])
        this.anusuchi2Report.get('yrlyIndex').patchValue(post['yrlyIndex'])
        this.anusuchi2Report.get('yrlyWeight').patchValue(post['yrlyWeight'])
        this.anusuchi2Report.get('yrlyBudget').patchValue(post['yrlyBudget'])
        this.anusuchi2Report.get('fstIndex').patchValue(post['fstIndex'])
        this.anusuchi2Report.get('fstWeight').patchValue(post['fstWeight'])
        this.anusuchi2Report.get('fstBudget').patchValue(post['fstBudget'])
        this.anusuchi2Report.get('sndIndex').patchValue(post['sndIndex'])
        this.anusuchi2Report.get('sndWeight').patchValue(post['sndWeight'])
        this.anusuchi2Report.get('sndpercentage').patchValue(post['sndpercentage'])
        this.anusuchi2Report.get('fnlAmt').patchValue(post['fnlAmt'])
        this.anusuchi2Report.get('fnlpercentage').patchValue(post['fnlpercentage'])
    }

    createForm() {
        this.anusuchi2Report = this.fb.group({
            programName: [''],
            unit: [''],
            yrlyIndex: [''],
            yrlyWeight: [''],
            yrlyBudget: [''],
            fstIndex: [''],
            fstWeight: [''],
            fstBudget: [''],
            sndIndex: [''],
            sndWeight: [''],
            sndpercentage: [''],
            fnlAmt: [''],
            fnlpercentage: ['']

        })
    }

    /**
    * Export Report Data to Excel
    */
    exportReport(): void {
        let htmltable = document.getElementById('reportContainer');
        let html = htmltable.outerHTML;
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
    }

    /**
     * Open Print Window to print report
     */
    printReport(): void {
        let post = this.anusuchi2Report.value;
        this.all.postAnusuchi2(this.activityId,post).subscribe((data)=>{
            console.log(data)
        })
        let printContents, popupWin;
        printContents = document.getElementById('reportContainer').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .header{
            display: inline-block;
            width: 212px;
            float: left;
            padding-left: 66px;
          }
          table.invoice-table {
            border-collapse: collapse;
            width: 100%;
        }

        table.invoice-table, .invoice-table th, .invoice-table td {
            border: 1px solid black;
            padding: 2px 4px;
        }

        .invoice-table th {
            font-size: 12px;
        }
          .table-container table th{
              text-align: center;
              font: 12px
          }

          .table-container table .tleft{
              text-align: left !important;
          }

          .table-container table .tright{
              text-align: right !important;
          }
          .px-control{
              display:none
          }
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }

          @font-face {
            font-family: 'nepaliNormal';
            src: url('assets/css/Kantipur.TTF');
            font-weight: normal;
            font-style: normal;
        }
        .nepaliText{
          font-family:'nepaliNormal';
          font-size:18px!important;
      }
      .table {
            border-collapse: collapse;
            background-color: transparent;
          }

          .footer .footer-feild{
            display: inline-block;
            width:  170px;
            float: left;
        }
        .footer .footer-col{
            display: inline-block;
            width: 32%;
            float: left;
        }
        .invoice-footer .invoice-footer-col{
            display: inline-block;
            width: 326px;
            float: left;
        }

        .tableClas{
                border-collapse: collapse;
        }
    .tableClas  td {
          // border: 1px solid black;
          border: 1px solid #eceeef;
        }
        ul{
          list-style-type: none
      }



      .print_show{
        display: inline-block !important;
    }

    .print_hide{
        display: none !important;
      }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
    }

}
