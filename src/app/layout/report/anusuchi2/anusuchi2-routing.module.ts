import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Anusuchi2Component } from './anusuchi2.component';

const routes: Routes = [
    {
        path: '',
        component: Anusuchi2Component
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class Anusuchi2RoutingModule {}
