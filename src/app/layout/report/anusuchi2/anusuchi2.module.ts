import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Anusuchi2RoutingModule } from './anusuchi2-routing.module';
import { Anusuchi2Component } from './anusuchi2.component';
import { SharedModule } from 'app/shared/modules/shared.module';
import { AnusuchiSearchBarModule } from '../anusuchi-search-bar/anusuchi-search-bar.module';

@NgModule({
    imports: [CommonModule, SharedModule, Anusuchi2RoutingModule, AnusuchiSearchBarModule],
    declarations: [Anusuchi2Component]
})
export class Anusuchi2Module {}
