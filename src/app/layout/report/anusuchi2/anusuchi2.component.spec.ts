import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Anusuchi2Component } from './anusuchi2.component';

describe('Anusuchi2Component', () => {
  let component: Anusuchi2Component;
  let fixture: ComponentFixture<Anusuchi2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Anusuchi2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Anusuchi2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
