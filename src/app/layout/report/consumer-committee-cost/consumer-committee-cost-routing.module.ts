import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsumerCommitteeCostComponent } from './consumer-committee-cost.component';

const routes: Routes = [
  {
    path: '',
    component: ConsumerCommitteeCostComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsumerCommitteeCostRoutingModule { }
