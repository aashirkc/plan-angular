import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsumerCommitteeCostComponent } from './consumer-committee-cost.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ConsumerCommitteeCostRoutingModule } from './consumer-committee-cost-routing.module';
import { AnusuchiSearchBarModule } from '../anusuchi-search-bar/anusuchi-search-bar.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AnusuchiSearchBarModule,
    ConsumerCommitteeCostRoutingModule
  ],
  declarations: [ConsumerCommitteeCostComponent],
  schemas:[ NO_ERRORS_SCHEMA ]
})
export class ConsumerCommitteeCostModule { }
