import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { PlanningProgressReportComponent } from './planning-progress-report.component';
import { PlanningProgressReportRoutingModule } from './planning-progress-report-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PlanningProgressReportRoutingModule
  ],
  declarations: [
    PlanningProgressReportComponent,
  ]
})
export class PlanningProgressReportModule { }
