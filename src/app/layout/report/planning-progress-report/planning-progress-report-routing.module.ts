import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanningProgressReportComponent } from './planning-progress-report.component';

const routes: Routes = [
  {
    path: '',
    component: PlanningProgressReportComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanningProgressReportRoutingModule { }
