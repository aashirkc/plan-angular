import {
    Component,
    ViewChild,
    ViewContainerRef,
    Inject,
    ChangeDetectorRef,
    OnInit
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
    Router,
    RouterState,
    RouterStateSnapshot,
    ActivatedRouteSnapshot
} from '@angular/router';
import {
    DateConverterService,
    AgencyTypeService,
    AllReportService,
    UnicodeTranslateService,
    ActivityOutputService,
    ActivityFocusAreaMasterService,
    PlanningYearService,
    InclusionGroupService,
    CurrentUserService,
    PrintingService
} from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';

@Component({
    selector: 'app-planning-progress-report',
    templateUrl: './planning-progress-report.component.html',
    styleUrls: ['./planning-progress-report.component.scss']
})
export class PlanningProgressReportComponent implements OnInit {
    activityDetailLedger: FormGroup;
    source: any;
    dataAdapter: any;
    columns: any[];
    editrow: number = -1;
    transData: any;
    planYearAdapter: Array<any> = [];
    activityAdapter: Array<any> = [];
    activityFocusAreaAdapter: any = [];
    activityCategoryAdapter: any = [];
    agencyType: any = [];
    childType: any = [];
    userData: any = {};
    SearchDatas: Array<any> = [];
    API_URL_DOC: any;
    API_URL: any;
    columngroups: any[];
    lang: any;
    agencyName: any;
    planYear: any;
    year: any;
    wardSelect: boolean = false;
    wSelect: boolean = false;
    officeType: string = '';
    workServiceTypeAdapter: any = [
        {
            name: 'Consumer Committe',
            id: 'उपभोक्ता समिति'
        },
        {
            name: 'Contract',
            id: 'ठेक्का मार्फत'
        },
        {
            name: 'Amanathan Marfat',
            id: 'अमानत मार्फत'
        },
        {
            name: 'Sahakari',
            id: 'श्रम सहकारी / संस्था मार्फत'
        }
    ];
    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
    @ViewChild('myGrid') myGrid: jqxGridComponent;
    @ViewChild('myViewWindow') myViewWindow: jqxWindowComponent;
    @ViewChild('myEditWindow') myEditWindow: jqxWindowComponent;
    @ViewChild('myReportWindow') myReportWindow: jqxWindowComponent;
    @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;
    isRul: boolean;

    constructor(
        private fb: FormBuilder,
        private cus: CurrentUserService,
        private dfs: DateConverterService,
        private ats: AgencyTypeService,
        @Inject('API_URL_DOC') API_URL_DOC: string,
        @Inject('API_URL') API_URL: string,
        private cdr: ChangeDetectorRef,
        private pys: PlanningYearService,
        private igs: InclusionGroupService,
        private router: Router,
        private afams: ActivityFocusAreaMasterService,
        private activityOutputService: ActivityOutputService,
        private report: AllReportService,
        private translate: TranslateService,
        private unicode: UnicodeTranslateService,
        private printingService: PrintingService
    ) {
        this.createForm();
        this.getTranslation();
        this.userData = this.cus.getTokenData();
        this.API_URL_DOC = API_URL_DOC;
        this.API_URL = API_URL;
    }

    ngOnInit() {
        this.lang = this.translate.currentLang.substr(0, 2);
        this.isRul = false;
        const orgMaster = JSON.parse(localStorage.getItem('org'));
        if (orgMaster && orgMaster[0]) {
            const orgType = orgMaster[0]['orgType'];
            this.isRul = orgType === 'RUL';
        }
        this.pys.index({}).subscribe(
            res => {
                this.planYearAdapter = res;
                if (this.planYearAdapter.length > 0) {
                    let planCode = this.planYearAdapter[0].yearCode || '';
                    this.activityDetailLedger
                        .get('planYear')
                        .patchValue(planCode);
                    this.planYear = planCode;
                }
            },
            error => {
                console.info(error);
            }
        );
        this.igs.index({}).subscribe(
            res => {
                this.activityAdapter = res;
            },
            error => {
                console.info(error);
            }
        );
        this.afams.show(0).subscribe(
            res => {
                this.activityFocusAreaAdapter = res;
            },
            error => {
                console.info(error);
            }
        );
        this.ats.show(0).subscribe(
            result => {
                if (result['length'] > 0) {
                    this.agencyType = result;
                }
            },
            error => {
                console.log(error);
            }
        );
    }
    getTranslation() {
        this.translate
            .get([
                'SN',
                'regNo',
                'universityRoll',
                'ADD',
                'EDIT',
                'NAME',
                'nepali',
                'english',
                'mobile',
                'VIEW',
                'DETAIL',
                'ACTIONS',
                'UPDATE',
                'DELETESELECTED',
                'RELOAD'
            ])
            .subscribe((translation: [string]) => {
                this.transData = translation;
            });
    }

    loadGridData() {
        let post = {};
        post = this.activityDetailLedger.value;
        if (post['status']) {
            this.SearchData(post);
        }
    }

    createForm() {
        this.activityDetailLedger = this.fb.group({
            planYear: ['', Validators.required],
            activityFor: [''],
            activityFocusArea: [''],
            activityCategory: [''],
            activityName: [''],
            parent: [''],
            child: [''],
            workServiceType: ['']
        });
    }

    ngAfterViewInit() {
        this.cdr.detectChanges();
        this.activityDetailLedger.valueChanges.subscribe(val => {
            // console.info(val);
            if (val['parent'] == '' && val['child'] == '') {
                this.officeType = 'नगर';
            }
            if (val['parent'] != '' && val['child'] == '') {
                this.officeType = 'नगर';
            }
            if (val['parent'] != '' && val['child'] != '') {
                this.officeType = 'वडा';
            }
        });
        this.unicode.initUnicode();
    }

    ChangePlanYear(value) {
        this.planYear = value;
    }

    makeSticky() {
        const container = document.getElementById('yojanaTable');
        container.scrollTop = 0;
        const th = container.querySelectorAll('th');
        for (let i = 0; i < th.length; i++) {
            const cont = th[i];
            cont.style.setProperty(
                'top',
                cont.offsetTop - container.offsetTop + 'px'
            );
        }
    }

    findTotal(source, index) {
        try {
            let total = 0;
            for (let d of source) {
                if (index in d) {
                    total += Number(d[index]);
                }
            }
            return Number(total.toFixed(2));
        } catch (e) {
            return 0;
        }
    }

    toNumber(data) {
        const ret = Number(data);
        if (ret) {
            return ret;
        }
        return 0;
    }

    LoadActivityCategory() {
        let data = this.activityDetailLedger.value;
        if (data['activityFocusArea']) {
            this.jqxLoader.open();
            this.afams.show(data['activityFocusArea']).subscribe(
                res => {
                    this.activityCategoryAdapter = res;
                    this.activityDetailLedger
                        .get('activityCategory')
                        .patchValue('');
                    this.jqxLoader.close();
                },
                error => {
                    console.info(error);
                    this.activityDetailLedger
                        .get('activityCategory')
                        .patchValue('');
                    this.jqxLoader.close();
                }
            );
        }
    }
    implementingAgencyChange(selectedEvent) {
        if (
            selectedEvent &&
            selectedEvent.target &&
            selectedEvent.target.value
        ) {
            let displayText = selectedEvent.target.selectedOptions[0].text;
            this.agencyName = displayText;
            let agencyCode = selectedEvent.target.value;
            this.getAgencyType(agencyCode);
        } else {
            this.childType = '';
            this.activityDetailLedger.get('child').patchValue('');
        }
    }
    getAgencyType(agencyCode = 0, index = 0, type = null) {
        // remove previously added form control if the parent select is changed.
        this.ats.show(agencyCode).subscribe(
            result => {
                if (result['length'] > 0) {
                    this.childType = result;
                } else {
                    this.childType = '';
                }
            },
            error => {
                console.log(error);
            }
        );
    }
    SearchData(post) {
        if (this.childType.length <= 0) {
            post['child'] = '';
        }
        this.jqxLoader.open();
        this.report.getPlanningProgressReport(post).subscribe(
            res => {
                if (res.length == 1 && res[0].error) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = res[0].error;
                    this.errNotification.open();
                    this.SearchDatas = [];
                    this.year = '';
                } else {
                    this.SearchDatas = res;
                    this.year =
                        this.SearchDatas &&
                        this.SearchDatas[0] &&
                        this.SearchDatas[0]['wardData'][0].planYear;
                    if (post['child'] != '') {
                        this.wardSelect = true;
                    } else {
                        this.wardSelect = false;
                    }
                    setTimeout(this.makeSticky, 100);
                }
                this.jqxLoader.close();
            },
            error => {
                console.info(error);
                this.jqxLoader.close();
            }
        );
    }

    /**
     * Export Report Data to Excel
     */
    exportReport(): void {
        let htmltable = document.getElementById('reportContainer');
        let html = htmltable.outerHTML;
        window.open(
            'data:application/vnd.ms-excel,' + encodeURIComponent(html)
        );
    }

    /**
     * Open Print Window to print report
     */
    printReport(): void {
        const printContents = document.getElementById('reportContainer')
            .innerHTML;
        this.printingService.printContents(
            printContents,
            'Print tab',
            true,
            `
                .table-container{
                    font-size: 11px;
                }
                td, th {
                    padding: 3px;
                }
            `
        );
    }
}
