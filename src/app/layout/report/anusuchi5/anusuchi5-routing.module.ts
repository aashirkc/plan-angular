import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Anusuchi5Component } from './anusuchi5.component';

const routes: Routes = [
    {
        path: '',
        component: Anusuchi5Component,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class Anusuchi5RoutingModule { }
 