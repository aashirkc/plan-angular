import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { PlanningYearService, UnicodeTranslateService, AgencyTypeService, AdministrativeApprovalService, AllReportService, KataSanchalanService, DateConverterService, ToggleSidebarService, OrganizedByMasterService, ProgressReportingTippaniAadeshService } from '../../../shared';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-anusuchi5',
  templateUrl: './anusuchi5.component.html',
  styleUrls: ['./anusuchi5.component.scss']
})
export class Anusuchi5Component implements OnInit {
  organizationMasterData: any;
  yearAdapter: Array<any> = [];
  anusuchi5: FormGroup;
  foForm: FormGroup;
  SearchDatas: any;
  dataLists: any;
  activityAdapter: Array<any> = [];
  activityDatas: any;
  check: boolean = true;
  childType: any = [];
  agencyName: any;
  agencyType: any;
  lang: any;
  techCost: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  @ViewChild('act') act: jqxComboBoxComponent;


  // --- search bar emitter functions
  search = ({value, id}) => this.SearchData(value, id)

  changeLoadingStatus($event){
    !$event && this.jqxLoader.close();
    $event && this.jqxLoader.open();
  }

  showError($event){
    let messageDiv: any = document.getElementById('error');
    messageDiv.innerText = $event;
    this.errNotification.open();
  }
  // when sidebar is opened
  searchWithActivityDatas = () => {
    try{
        const activityDatas = JSON.parse(localStorage.getItem("ActivityDatas"));
        if(activityDatas){
            const requestParams = {
                planYear: activityDatas.planYear,
                activity: activityDatas.id,
                activityType: 3
            }
            this.prtas.getOrganizationName().subscribe((res) => {
                this.organizationMasterData = res;
                this.search({ value: requestParams, id: activityDatas.id });
            });
        }
    }catch(e){
        debugger;
        console.warn(e);
    }
}
  // --- end


  constructor(
    private fb: FormBuilder,
    private pys: PlanningYearService,
    private rs: AllReportService,
    private aas: AdministrativeApprovalService,
    private ks: KataSanchalanService,
    private unicode: UnicodeTranslateService,
    private org: OrganizedByMasterService,
    private ts: ToggleSidebarService,
    private ats: AgencyTypeService,
    private prtas:ProgressReportingTippaniAadeshService

  ) {
    this.createForm();
  }
  createForm() {
    this.anusuchi5 = this.fb.group({
      'planYear': ['', Validators.required],
      'child': [''],
      'parent': [''],
    });
    this.foForm = this.fb.group({
      'janasahavagita': [''],
    });
  }

  // ngAfterViewInit() {
  //   this.unicode.initUnicode();
  // }

  ngOnInit() {
    this.ats.show(0).subscribe(
      result => {
        if (result['length'] > 0) {

          this.agencyType = result;
        }
      },
      error => {
        console.log(error);
      }
    );

    this.ts.currentMessage.subscribe((data) => {
        if (data == "on"){
          this.check = false;
          // local storage gets set when sidebar is opened taking current activity
          this.searchWithActivityDatas();
        }
        if (data == "off"){
            this.check = true;
        }
        console.log(this.check);
    });
    this.rs.getOrg().subscribe((res) => {
      this.organizationMasterData = res && res[0];
      console.info(res);
    }, (error) => {
      console.info(error);

    });
    this.activityDatas = JSON.parse(localStorage.getItem('ActivityDatas'));

    this.pys.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.yearAdapter = [];
      } else {
        this.yearAdapter = res;
        if (this.activityDatas) {
          this.anusuchi5.get('planYear').patchValue(this.activityDatas['planYear']);
          this.ChangePlanYear1(this.activityDatas['planYear']);
        } else {
          this.anusuchi5.get('planYear').patchValue(res[0]['yearCode']);
          this.ChangePlanYear(res[0]['yearCode']);
        }
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
  ChangePlanYear1(id) {
    console.log(id);
    this.ks.indexActivity({ planYear: id, activityType: 3 }).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
        console.log(this.activityDatas['id']);
        setTimeout(() => {
          if (this.check) {
            this.act.selectItem(this.activityDatas['id']);
          }
          // this.act.selectItem(this.activityDatas['id']);
          this.SearchData(this.anusuchi5.value, this.activityDatas['id']);
        }, 100);

      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }

  SearchData(post, val) {
    // post['activity'] = this.act.val();
    post['activity'] = val;
    this.jqxLoader.open();
    if (val) {
      this.rs.getanusuchi5Report(post).subscribe((res) => {
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.SearchDatas = [];
        } else {
          this.SearchDatas = res;
          let id = val;
          this.aas.getApprovalByid(id).subscribe((result: any) => {
            this.dataLists = result;
            this.techCost = this.dataLists['techCost'];
            this.foForm.controls['janasahavagita'].patchValue(this.dataLists['PlanAgreement'][0].grantedFromConsumerCommitte)
          });
        }
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });
    }
    else {
      this.jqxLoader.close();
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'कृपया याेजना छान्नुहाेस्';
      this.errNotification.open();
    }
  }
  ChangePlanYear(id) {
    this.ks.indexActivity({ planYear: id, activityType: 3 }).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }


  /**
* Export Report Data to Excel
*/
  exportReport(): void {
    let htmltable = document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  /**
   * Open Print Window to print report
   */
  printReport(): void {
    // console.log(this.foForm.value);
    let post=this.foForm.value;
    this.rs.sendanusuchi5Report(post).subscribe((res) => {
      console.log(res);
      if (res['message'] === 'success') {

      }
    });
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .table-container table th{
              text-align: center;
          }

          .table-container table .tleft{
              text-align: left !important;
          }

          .table-container table .tright{
              text-align: right !important;
          }
          .hide_div{
            display:none !important;

          }
          .show_div{
            display:inline-block !important ;
          }
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }

          @font-face {
            font-family: 'nepaliNormal';
            src: url('assets/css/Kantipur.TTF');
            font-weight: normal;
            font-style: normal;
        }
        .nepaliText{
          font-family:'nepaliNormal';
          font-size:18px!important;
      }
       .table {
            border-collapse: collapse;
            background-color: transparent;
          }

        .tableClas{
                border-collapse: collapse;
        }
    .tableClas  td {
          // border: 1px solid black;
          border: 1px solid #eceeef;
        }
        ul{
          list-style-type: none
      }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  implementingAgencyChange(selectedEvent) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.agencyName = displayText;
      let agencyCode = selectedEvent.target.value;
      this.getAgencyType(agencyCode);
    } else {
      this.childType = '';
    }
  }

  getAgencyType(agencyCode = 0, index = 0, type = null) {
    // remove previously added form control if the parent select is changed.
    this.ats.show(agencyCode).subscribe(
      result => {
        if (result['length'] > 0) {
          this.childType = result;
        } else {
          this.childType = '';
        }
      },
      error => {
        console.log(error);
      }
    );
  }



}
