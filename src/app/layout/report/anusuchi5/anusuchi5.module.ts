import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Anusuchi5Component } from './anusuchi5.component';
import { SharedModule } from 'app/shared/modules/shared.module';
import { Anusuchi5RoutingModule } from './anusuchi5-routing.module';
import { AnusuchiSearchBarModule } from '../anusuchi-search-bar/anusuchi-search-bar.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    Anusuchi5RoutingModule,
    AnusuchiSearchBarModule
  ],
  declarations: [Anusuchi5Component],
  schemas: [NO_ERRORS_SCHEMA]
})
export class Anusuchi5Module { }
