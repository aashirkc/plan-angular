import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/modules/shared.module';
import { AnusuchiSearchBarComponent } from './anusuchi-search-bar.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [ AnusuchiSearchBarComponent ],
  exports: [
      AnusuchiSearchBarComponent
    ]
})
export class AnusuchiSearchBarModule { }
