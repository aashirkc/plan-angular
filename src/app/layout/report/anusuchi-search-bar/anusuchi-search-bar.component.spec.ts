import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnusuchiSearchBarComponent } from './anusuchi-search-bar.component';

describe('AnusuchiSearchBarComponent', () => {
  let component: AnusuchiSearchBarComponent;
  let fixture: ComponentFixture<AnusuchiSearchBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnusuchiSearchBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnusuchiSearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
