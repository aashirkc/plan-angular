import {
    Component,
    OnInit,
    ViewChild,
    Output,
    EventEmitter,
    Input,
    AfterViewInit,
    OnDestroy
} from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import {
    AgencyTypeService,
    PlanningYearService,
    KataSanchalanService,
    UnicodeTranslateService
} from 'app/shared';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { ProgramsService } from 'app/shared/services/programs.service';

// TODO: dont make api request for activity when noActivity is present

@Component({
    selector: 'app-anusuchi-search-bar',
    templateUrl: './anusuchi-search-bar.component.html',
    styleUrls: ['./anusuchi-search-bar.component.scss']
})
export class AnusuchiSearchBarComponent implements OnInit, AfterViewInit {
    anusuchiSearch: FormGroup;
    check = true;
    childType: any = [];
    agencyName: string;
    yearAdapter: any;
    activityDatas: any;
    activityAdapter: any;
    agencyType: any;
    @Input() showProgram = false;
    @Input() showActivity = true;
    @Input() onlyConsumerCommittee = false;
    @Input() showPrint = false;
    @Input() searchBarTitle = 'अनुसुची ३ रिपोर्ट ';
    @Output() error: EventEmitter<string> = new EventEmitter<string>();
    @Output() loading: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() search: EventEmitter<any> = new EventEmitter<any>();
    @Output() print: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('act') act: jqxComboBoxComponent;
    @ViewChild('prog') prog: jqxComboBoxComponent;
    @Input() showSearchBar = true;
    programAdapter: any[];

    constructor(
        private fb: FormBuilder,
        private ats: AgencyTypeService,
        private pys: PlanningYearService,
        private ks: KataSanchalanService,
        private programsService: ProgramsService,
        private unicode: UnicodeTranslateService
    ) { }

    ngOnInit() {
        this.activityDatas = JSON.parse(localStorage.getItem('ActivityDatas'));
        this.createForm();
        this.getPlanYear();
        this.getAgencyType(0);
        if (this.showProgram) {
            this.getPrograms();
        }
    }

    ngAfterViewInit(): void {
        this.unicode.initUnicode();
    }

    getPrograms() {
        const post = {
            planYear: this.anusuchiSearch.value.planYear,
            implementingAgencyType1: this.anusuchiSearch.value.parent,
            implementingAgencyType2: this.anusuchiSearch.value.child
        };
        this.loading.emit(true);
        this.programsService.getYearlyPrograms(post).subscribe(
            res => {
                if (res && res.length == 1 && res[0].error) {
                    this.showErrorMessage(res[0].error);
                    this.programAdapter = [];
                } else {
                    this.programAdapter = res['data'];
                }
                this.loading.emit(false);
            },
            error => {
                console.log(error);
                // tslint:disable-next-line: quotemark
                console.log("Can't get programs");
                this.loading.emit(false);
            }
        );
    }

    getActOrProg() {
        if (this.showProgram) {
            this.getPrograms();
        } else {
            this.getActivity();
        }
    }

    createForm = () => {
        this.anusuchiSearch = this.fb.group({
            planYear: ['', Validators.required],
            child: '',
            parent: ''
        });
    };

    implementingAgencyChange = $event => {
        if ($event && $event.target && $event.target.value) {
            let displayText = $event.target.selectedOptions[0].text;
            this.agencyName = displayText;
            let agencyCode = $event.target.value;
            this.getAgencyType(agencyCode);
            this.anusuchiSearch.get('child').setValue('');
        } else {
            this.childType = '';
            this.anusuchiSearch.get('child').setValue('');
        }
    };

    getAgencyType = code => {
        this.ats.show(code).subscribe(
            result => {
                if (code == 0) {
                    if (result['length'] > 0) {
                        this.agencyType = result;
                    }
                } else {
                    if (result['length'] > 0) {
                        this.childType = result;
                    } else {
                        this.childType = '';
                    }
                }
            },
            error => {
                console.log(error);
            }
        );
    };

    getPlanYear = () =>
        this.pys.index({}).subscribe(res => {
            if (res && res.length == 1 && res[0].error) {
                this.showErrorMessage(res[0].error);
            } else {
                this.yearAdapter = res;
                if (this.activityDatas) {
                    this.anusuchiSearch.patchValue({
                        planYear: this.activityDatas['planYear']
                    });
                    this.ChangePlanYear1(this.activityDatas['planYear']);
                } else {
                    this.anusuchiSearch.patchValue({
                        planYear: res[0]['yearCode']
                    });
                    this.ChangePlanYear(res[0]['yearCode']);
                }
            }
        });

    showErrorMessage = (message: string) => this.error.emit(message);

    ChangePlanYear1 = id => {
        this.loading.emit(true);
        let serviceFunction = this.ks.indexActivity; // all data
        if (this.onlyConsumerCommittee) {
            serviceFunction = this.ks.indexActivity1; // only consumer committee data
        }
        serviceFunction({ planYear: id }).subscribe(
            res => {
                if (res && res.length == 1 && res[0].error) {
                    this.showErrorMessage(res[0].error);
                    this.activityAdapter = [];
                } else {
                    this.activityAdapter = res;
                    setTimeout(() => {
                        if (this.check) {
                            this.act &&
                                this.act.selectItem(this.activityDatas['id']);
                        }
                        // this.SearchData();
                    }, 100);
                }
                this.loading.emit(false);
            },
            error => {
                console.info(error);
                this.loading.emit(false);
            }
        );
    };

    getActivity() {
        const params = this.anusuchiSearch.value;
        let serviceFunction = this.ks.indexActivity; // all data
        if (this.onlyConsumerCommittee) {
            serviceFunction = this.ks.indexActivity1; // only consumer committee data
        }
        serviceFunction(params).subscribe(
            res => {
                if (res && res.length == 1 && res[0].error) {
                    this.showErrorMessage(res[0].error);
                    this.activityAdapter = [];
                } else {
                    this.activityAdapter = res;
                    setTimeout(() => {
                        if (this.check) {
                            const datas =
                                this.activityDatas && this.activityDatas['id'];
                            this.act && this.act.selectItem(datas);
                        }
                        // this.SearchData();
                    }, 100);
                }
                this.loading.emit(false);
            },
            error => {
                console.info(error);
                this.loading.emit(false);
            }
        );
    }

    SearchData = () => {
        this.search.emit({
            value: this.anusuchiSearch.value,
            id: this.act && this.act.val(),
            program: this.prog && this.prog.val()
        });
    };

    ChangePlanYear = id => {
        this.loading.emit(true);
        const params = this.anusuchiSearch.value;
        let serviceFunction = this.ks.indexActivity; // all data
        if (this.onlyConsumerCommittee) {
            serviceFunction = this.ks.indexActivity1; // only consumer committee data
        }
        serviceFunction(params).subscribe(
            res => {
                if (res && res.length == 1 && res[0].error) {
                    this.showErrorMessage(res[0].error);
                    this.activityAdapter = [];
                } else {
                    this.activityAdapter = res;
                }
                this.loading.emit(false);
            },
            error => {
                console.info(error);
                this.loading.emit(false);
            }
        );
    };
}
