import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { TranslateService } from '@ngx-translate/core';
import { EstimateAbstractOfCostService, PlanningYearService, } from '../../../shared';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

@Component({
  selector: 'app-work-aception-report-thaka-bill',
  templateUrl: './work-aception-report-thaka-bill.component.html',
  styleUrls: ['./work-aception-report-thaka-bill.component.scss']
})
export class WorkAceptionReportThakaBillComponent implements OnInit {
  searchForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  aboutFinancialOperationForm: FormGroup;
  foForm: FormGroup;
  transData: any;
  yearAdapter: Array<any> = [];
  activityAdapter: Array<any> = [];
  userData: any = {};
  SearchDatas: Array<any> = [];
  organizationDta: any;
  date: any
  fullDate: any;
  ActivityName: any;
  planYearAdapter: Array<any> = [];

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
  @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;
  @ViewChild('myComboBox') myComboBox: jqxComboBoxComponent;

  constructor(
    private fb: FormBuilder,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    @Inject('API_URL') API_URL: string,
    private pys: PlanningYearService,
    private estimateCostService: EstimateAbstractOfCostService) {
    this.createForm();

  }

  ngOnInit() {
    this.pys.index({}).subscribe((res) => {
      this.planYearAdapter = res;
      if (this.planYearAdapter.length > 0) {
        let planCode = this.planYearAdapter[0].yearCode || '';
        this.searchForm.get('planYear').patchValue(planCode);
      }
    }, (error) => {
      console.info(error);
    });
    
    this.estimateCostService.indexActivity({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.activityAdapter = [];
      } else {
        this.activityAdapter = res;
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

    this.estimateCostService.indexOrganizationMaster({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.organizationDta = {};
      } else {
        this.organizationDta = res && res[0];
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  createForm() {
    this.searchForm = this.fb.group({
      'planYear': ['',],
    });

  }



  Subtotal: number;
  SubtotalWord: string;
  extraDatas: any;
  comboBoxOnChange(event: any) {
    let post = {};
    post['activity'] = this.myComboBox.val() || '';
    this.ActivityName = event.args.item.label;
    this.estimateCostService.getTableThirdData(post).subscribe((res) => {
      if (res['error'] && res['error'].message) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res['error'].message;
        this.errNotification.open();
        this.SearchDatas = [];
      } else {
        this.SearchDatas = res && res['data'];
        this.extraDatas = res;
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });

  }


  SearchData(post) {
    post['activity'] = this.myComboBox.val() || '';
    this.estimateCostService.getTableThirdData(post).subscribe((res) => {
      if (res['error'] && res['error'].message) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res['error'].message;
        this.errNotification.open();
        this.SearchDatas = [];
      } else {
        this.SearchDatas = res && res['data'];
        this.extraDatas = res;
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }




  printData(): void {
    let printContents, popupWin;
    printContents = '';
    printContents = document.getElementById('printContent').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>कार्य सम्पन्न प्रतिबेदन</title>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
          <style>
          .exam-triplicate{
            background: #fff;
            margin-bottom: 20px;
          }
          .univ-title {
              padding-top: 20px;
          }
          .form-style-4 {
            display: none !important;
        }
        .print_show{
            display: inline-block !important;
        }
          .table{
            width:100%;
            border-collapse:collapse;
          }
           .col-md-4{
             display:inline-block;
             width:33%;
           }
          .univ-title p {
              margin: 0 0 0px;      
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
              padding: 3px;
          }
          .logo-top{
            padding-top: 25px;
          }
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
}
