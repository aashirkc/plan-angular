import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkAceptionReportThakaBillComponent } from './work-aception-report-thaka-bill.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { WorkAceptionReportThakaBillRoutingModule } from './work-aception-report-thaka-bill-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    WorkAceptionReportThakaBillRoutingModule
  ],
  declarations: [WorkAceptionReportThakaBillComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class WorkAceptionReportThakaBillModule { }
