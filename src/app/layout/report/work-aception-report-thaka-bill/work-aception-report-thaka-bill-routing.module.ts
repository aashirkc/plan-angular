import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkAceptionReportThakaBillComponent } from './work-aception-report-thaka-bill.component';

const routes: Routes = [
  {
    path: '',
    component: WorkAceptionReportThakaBillComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkAceptionReportThakaBillRoutingModule { }
