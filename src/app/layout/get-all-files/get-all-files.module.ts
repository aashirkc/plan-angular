import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetAllFilesComponent } from './get-all-files.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { GetAllFilesRoutingModule } from './get-all-files-routing.module';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GetAllFilesRoutingModule
  ],
  declarations: [GetAllFilesComponent]
})
export class GetAllFilesModule { }
