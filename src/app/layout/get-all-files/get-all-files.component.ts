import { Component, OnInit } from '@angular/core';
import { ActivityOutputService, } from "../../shared";

@Component({
  selector: 'app-get-all-files',
  templateUrl: './get-all-files.component.html',
  styleUrls: ['./get-all-files.component.scss']
})
export class GetAllFilesComponent implements OnInit {
  activityDatas:any;
  constructor(
    private activityOutputService: ActivityOutputService,
  ) {
    this.activityDatas=JSON.parse(localStorage.getItem('ActivityDatas'))
    this.activityOutputService.getAllFiles(this.activityDatas['id']).subscribe((res) => {
      if ( res[0].error) {
        
        console.log(res)
       
      } else {
       
   console.log(res)
      }
    
  },
  error => {
      console.info(error);
     
  }
);
   }

  ngOnInit() {

  }

}
