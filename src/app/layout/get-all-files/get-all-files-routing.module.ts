import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetAllFilesComponent } from './get-all-files.component';

const routes: Routes = [
    { path: '', component: GetAllFilesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GetAllFilesRoutingModule { }
