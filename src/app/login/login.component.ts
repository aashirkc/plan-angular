import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../router.animations';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthenticateService, OrganizedByMasterService } from '../shared';
import { JwtHelperService } from '@auth0/angular-jwt';

import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    @ViewChild('loginLoader') loginLoader: jqxLoaderComponent;

    loginForm: FormGroup;
    estateSelected: boolean = false;

    msgType: string;
    msgText: string;

    jwtHelper: JwtHelperService = new JwtHelperService();

    constructor(
        private org: OrganizedByMasterService,
        private route: ActivatedRoute,
        public router: Router,
        private as: AuthenticateService,
        private fb: FormBuilder,
    ) {
        this.loginForm = this.fb.group({
            'userId': [null, Validators.required],
            'password': [null, Validators.required],
        });
    }

    ngOnInit() {
        let currentToken = this.as.getToken();
        let currentUser = this.as.getUser();
        if (currentToken && this.jwtHelper.isTokenExpired(currentToken)) {
            this.as.logout();
            this.router.navigate(['/login'], { queryParams: { type: 'error', msg: 'Session Expired Please login to continue' } });
        } else if (currentToken && currentUser && !this.jwtHelper.isTokenExpired(currentToken)) {
            this.router.navigate(['/']);
        } else {
            this.as.logout();
        }

        this.route.queryParams.subscribe(params => {
            // Defaults to 0 if no query param provided.
            this.msgType = params['type'] || null;
            this.msgText = params['msg'] || null;
        });

    }

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }

    getLogin(post) {
        // console.log(post);
        let FData = new FormData();
        for (let data in post) {
            FData.append(data, post[data]);
          }

        this.loginLoader.open();


        this.as.login(FData)
            .subscribe(result => {
                if (result['token']) {
                    localStorage.setItem('pcToken', result['token']);
                    localStorage.setItem('userType', result['userType']);
                    localStorage.setItem('pcUser', JSON.stringify(result));
                    this.loginLoader.close();
                    this.getOrganization();
                    this.router.navigate(['/dashboard']);
                } else {
                    this.loginLoader.close();
                    this.msgText = result['error'] && result['error']['message'];
                }

                this.loginLoader.close();
            },
                error => {
                    this.loginLoader.close();
                });



    }

    getOrganization(){
        this.org.indexOrgMaster({}).subscribe(res=>{
            if(res.length > 0){
                localStorage.setItem('org',  JSON.stringify(res))
                    console.log('hey')
            }else{
                this.router.navigate(['/setup/organization-master']);
                alert('please fill the information below first')
                console.log('hey you')
            }


        })
    }

}
