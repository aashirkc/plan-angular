import { CommonModule } from '@angular/common';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../shared/modules/shared.module';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        LoginRoutingModule
    ],
    declarations: [LoginComponent],
    schemas: [ NO_ERRORS_SCHEMA]
})
export class LoginModule {
}
