if [ "$1" = "build" ]
then 

echo "--------building file-----";
node --max_old_space_size=8192 node_modules/@angular/cli/bin/ng b --prod --aot --output-hashing none;

fi;

cd dist;

echo "--- send to client ----";
smbclient //192.168.100.199/Application -U guest -c 'deltree *; prompt; recurse; mput *';

cd ..;